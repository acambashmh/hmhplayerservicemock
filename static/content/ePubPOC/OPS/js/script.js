/*Foot note opening*/
function popup() {
  var p = document.getElementById(this.id + '-popup');
  
  var top_of_el = $(this).position().top+$(this).parent().position().top-$(p).height()-30;
  var left_of_el = $(this).position().left+$(this).parent().position().left;
  $(".popup").css("opacity", "0");
  p.style.opacity = 1;
  var new_y =  top_of_el;
  p.style.webkitTransform = 'translateY(' + new_y + 'px) translateX(' + left_of_el + 'px)';
  
}
/*Foot note closing*/
function dismiss() {
  this.style.opacity = 0;
  var el = this;
  setTimeout(function () {
    el.style.webkitTransform = null;
  }, 1000);
}

/* Register the events */
var terms = document.getElementsByClassName('term');
for (var i=0; i < terms.length; i++) {
  terms[i].addEventListener('click', popup);
}
var popups = document.getElementsByClassName('popup');
for (var i=0; i < popups.length; i++) {
  popups[i].addEventListener('click', dismiss);

}

/*Read foot note*/
function popup_read() {
  var p = document.getElementById(this.id + '-popupread');
  
  var top_of_el = $(this).position().top+$(this).parent().position().top+40;
  var left_of_el = $(this).position().left+$(this).parent().position().left;

  p.style.opacity = 1;
  var new_y =  top_of_el;
  p.style.webkitTransform = 'translateY(' + new_y + 'px) translateX(' + left_of_el + 'px)';
  
}

var terms = document.getElementsByClassName('term');
for (var i=0; i < terms.length; i++) {
  terms[i].addEventListener('click', popup_read);
}

/* Video closing*/

$(document).ready(function(){
  $("#page33video1").bind('ended', function(){
 document.getElementsByClassName("page33fig1")[0].style.display = "block";
 document.getElementById("page33video1").style.display = "none";
document.getElementById("page33video1").setAttribute("src", " ");
  });

  $('#impairment').on('click', function(){
   $('#impairment-popup').css('margin-top','-60px');
 $('#impairment-popup').css('left','0px');

  });
});

/* Video function */
function videodisp()
{
 document.getElementsByClassName("page33fig1")[0].style.display = "none";
 document.getElementById("page33video1").style.display = "block";


var myVideo=document.getElementById("page33video1"); 
if (myVideo.paused) 
	{
	 document.getElementById("page33video1").style.display = "block";
	 document.getElementById("page33video1").setAttribute("src", "video/page0033.mp4");
	
  myVideo.play();
   myVideo.ended();
  

	}
else {
	
 document.getElementsByClassName("page33fig1")[0].style.display = "block";
 document.getElementById("page33video1").style.display = "none";
document.getElementById("page33video1").setAttribute("src", " ");

}



}

/* Audio Function */
function audioplay()
{
if($('#A1').length)
{
$('#A1').remove();
}
else
{
$('#sec001').append('<audio src="audio/0978.mp3" id="A1"><div class="err"><p>Sorry, it appears your system does not support audio playback.</p></div></audio>');

}

var myAudio=document.getElementById("A1"); 
if (myAudio.paused){ 
  myAudio.play(); 
}
}

