console.log("Global model is " + GlobalModel);
GlobalModel.HMHPlayerEmbedded = true;
window.USE_UNMINNED_COREJS = true; // debug purposes
(function() {
	if (!GlobalModel.HMHPlayerEmbedded) {
		return;
	}
	$(document).ready(function() {
		$('body').addClass('hmhPlayerEmbeded');
	});

	var getOperator = function (operatorName) {
		// objOperatorsArray is an object, not an array
		return OperatorManager.objOperatorsArray[operatorName];
	};

	var playerApi = {
		/**
		 * Change the ebook view type, 
		 * @param viewType - one of 'scroll', 'single', 'double'
		 */
		changePageView : function(viewType) {
			switch ((viewType || "").toLowerCase()) {
			case 'single':
				$('.singlePageLauncher.changePageViewBtn').click();
				break;
			case 'double':
				$('.doublePageLauncher.changePageViewBtn').click();
				break;
			default:
				// default to scroll
				$('.scrollPageLauncher.changePageViewBtn').click();
				break;
			}
		},
		/**
		 * Change the ebook zoom factor
		 * @param value float between 0 and 1
		 */
		setPageViewZoom : function(value) {
			var val = Math.max(0, Math.min(1, value));
			val = isNaN(val) ? .5 : val;
			// 2.5 is the max zoom of the ebook
			$("#bookContentContainer").data('scrollviewcomp').setZoom(val * 2.5);
		},
		/**
		 * paginate to the next page in the reader
		 */
		paginateNext : function() {
			$("#nextBtnRight").click();
		},
		/**
		 *  paginate to the previous page in the reader
		 */
		paginatePrevious : function() {
			$("#prevBtnLeft").click();
		},
		/**
		 * goto a specific page
		 * @param page - the page number (human redable) to navigate to (non-zero index)
		 */
		gotoPage : function(page) {
			$('#tocGotoInputBox').val(page);
			// trigger enter key up
			$('#tocGotoInputBox').trigger({
				type : 'keydown',
				which : 13
			});
			$('#tocGotoInputBox').trigger({
				type : 'keyup',
				which : 13
			});
			$('#tocGotoInputBox').trigger({
				type : 'keypress',
				which : 13
			});
		},
		/**
		 * got to a specific page index (0 based index, as referenced from TOC.xhtml)
		 */
		gotoPageIndex : function(pageIndex) {
			var objComp = {
				getClickedLinkIndex : function() {
					return pageIndex;
				}
			};
			MG.NavigationOperator.prototype.tocLinkItemClickHandler(objComp);
		},
		toggleAudioPlayer: function () {
			// TODO: instead of using $() to find the native audio button, see if we can modify
			// the method attachComponent in MG.MediaPlayerOperator.js so we can call a method to trigger the click event
			$("#audioLauncherBtn").click();
		},
		loadQuestionXML: function (fileName) {
			$.ajax({
				type: "GET",
				url: getPathManager().getIntractivitiesXMLPath(fileName),
				processData: false,
				contentType: "plain/text",
				success: function (objData) {
					var questionJSON = $.xml2json(new XMLSerializer().serializeToString(objData));
					ebookBridge.updateQuestion(questionJSON, fileName);
				},
				error: function (e) {
					//todo: handle error
				}
			});
		},
		saveAnnotation : function(data) {
			var noteOperator = getOperator("NoteOperator");

			if (data.isNew) {
				data.id = GlobalModel.totalStickyNotes.toString();

				if (data.isNote) {
					noteOperator.createNewNote(data);
				} else {
					data.selectionClass = ".selection" + data.selection;
					noteOperator.createNewHighlight(data);
				}


				if( !data.isSwapping )
				{
					GlobalModel.totalStickyNotes++;
				}
			} else {
				noteOperator.modifyNote(data);
			}

			// Dismiss the hidden popup to remove the click mask
			PopupManager.removePopup();
		},
		cancelAnnotation : function () {
			console.log("HMHPlayer cancelAnnotation called");
			var noteOperator = getOperator("NoteOperator");
			noteOperator.exitAnnotationMode();
		},
		updateHighlightStateForCurrentSelection: function (data) {
			var noteOperator = getOperator("NoteOperator");

			if (!data.isNote) {
				noteOperator.changeHighlightColor(data);
			} else {
				noteOperator.switchToNoteMode();
			}

		}

	};

	var parentWindow;

	var pendingMessageQueue = [];
	
	var sendNativeMessage = function(data) {
		if (parentWindow) {
			parentWindow.postMessage(data, "*");			
		} else {
			pendingMessageQueue.push(data);
		}
	};

	//CPA bridge
	$(window).on("message", function(evt) {
		if (evt.originalEvent.origin.indexOf("chrome-extension:") !== 0) {
			// not a message from the shell
			return;
		}
		var payload = evt.originalEvent.data;
		switch (payload.evt) {
			case "initializeBridge":
				parentWindow = evt.originalEvent.source;
				for (var i = 0; i < pendingMessageQueue.length; i++) {
					sendNativeMessage(pendingMessageQueue[i]);
				}
				pendingMessageQueue = [];
				break;
			case "changePageMode":
				playerApi.changePageView(payload.data.mode);
				break;
			case "paginatePrevious":
				playerApi.paginatePrevious();
				break;
			case "paginateNext":
				playerApi.paginateNext();
				break;
			case "setZoom":
				playerApi.setPageViewZoom(payload.data.zoom);
				break;
			case "gotoPageIndex":
				playerApi.gotoPageIndex(payload.data.pageIndex);
				break;
			case "toggleAudioPlayer":
				playerApi.toggleAudioPlayer();
				break;
			case "loadQuestionXML":
				playerApi.loadQuestionXML(payload.data.fileName);
				break;
			case "saveAnnotation":
				playerApi.saveAnnotation(payload.data);
				break;
			case "cancelAnnotation":
				playerApi.cancelAnnotation();
				break;
			case "updateHighlightStateForCurrentSelection":
				playerApi.updateHighlightStateForCurrentSelection(payload.data);
				break;
		}
	});

	var ebookBridge = {
		onNavigateToPage : function(pageIdx, lessonTitle) {
			sendNativeMessage({
				event : "NavToPage",
				page : pageIdx + "",
				lessonTitle : lessonTitle
			});
		},
		onSaveBookmarkForPage: function (bookmarkObj) {
			sendNativeMessage({
				event: "saveBookmarkForPage",
				bookmarkObj: bookmarkObj
			});
		},
		onDeleteBookmarkForPage: function (bookmarkId) {
			sendNativeMessage({
				event: "deleteBookmarkForPage",
				bookmarkId: bookmarkId
			});
		},
		onEnableAudioButton: function () {
			sendNativeMessage({
				event: "enableAudioButton"
			});
		},
		onDisableAudioButton: function () {
			sendNativeMessage({
				event: "disableAudioButton"
			});
		},
		showQuestionGroup: function (groupData, imageGalleryData) {
			sendNativeMessage({
				event: "questionWidget:showGroup",
				groupData: {
					groupXMLData: groupData.groupXMLData,
					groupId: groupData.currentGrp,
					currQuesClicked: groupData.currQuesClicked,
					currentGrpArray: groupData.currentGrpArray,
					currentQuestion: groupData.currentQuestion,
					correctResponse: groupData.correctResponse,
					prevQuesClicked: groupData.prevQuesClicked,
					questionXMLData: groupData.questionXMLData,
					questionArrData: groupData.questionArrData,
					currInteractionType: groupData.currInteractionType.widgetName,
					questionPageName: groupData.questionPageName,
					imageGalleryArray: groupData.imageGalleryArray,
					imgFolderPath: groupData.imgFolderPath,
					imageGalleryData: imageGalleryData
				}
			});
		},
		updateQuestion: function (questionData, qid) {
			sendNativeMessage({
				event: "questionWidget:updateQuestion",
				questionData: questionData,
				qid: qid
			});
		},
		onNewAnnotation: function (annotationText, pageNumber) {
			sendNativeMessage({
				event: "newAnnotation",
				annotationText: annotationText,
				pageNumber: pageNumber
			});
		},
		editAnnotation: function (noteModel) {
			var noteOperator = getOperator("NoteOperator");
			sendNativeMessage({
				event: "editAnnotation",
				annotationModel: noteModel,
				selectedText: noteOperator.getTextFromRangeXml(noteModel.range)
			});
		}
	};

	window.hmhPlayerApiBridge = ebookBridge;
	window.hmhPlayerApi = playerApi;

})();
