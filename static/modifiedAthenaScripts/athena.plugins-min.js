Function.prototype.extend = function(a) {
    if (a.constructor === Function) {
        this.prototype = new a();
        this.prototype.constructor = this;
        this.prototype.parent = a.prototype;
    } else {
        this.prototype = a;
        this.prototype.constructor = this;
        this.prototype.parent = a;
    }
    return this;
};
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
};
Athena.BaseClass = function() {
    this.name = "BaseCreator";
    this.speed = 300;
    this.animeType = "linear";
    this.items = [];
    this.data = null;
    this.prefix = "";
    this.selectedIds = {};
    this.removeValidationError = 0;
};
Athena.BaseClass.prototype.simpleList = function(c, j, m, k, f, o, e) {
    var d = j.length,
        b, h, n = m,
        a, g, l;
    g = m.attr("class").split(" ");
    for (h = 0; h < g.length; h++) {
        if (/default/.exec(g[h])) {
            a = g[h];
        }
    }
    this.data = j;
    this.items[c] = [];
    this.selectedIds[c] = -1;
    l = this.randomise(j.length, (e !== undefined && e === true) ? e : false);
    for (h = 0; h < d; h++) {
        b = m.clone().insertBefore(m);
        b.attr("id", k[0] + "-" + this.prefix + l[h]);
        if (k[1] === undefined) {
            b.find("span").empty().append(j[l[h]][k[0]]);
        } else {
            b.find("span").empty().append(j[l[h]][k[0]][k[1]]);
        }
        b.removeClass(a);
        this.items[c].push(b);
        if (f !== undefined && f === true) {
            b.show();
        }
    }
    if (o !== undefined && o) {
        o();
    }
    m.remove();
    return l;
};
Athena.BaseClass.prototype.randomise = function(d, c) {
    var a, e = [],
        b;
    b = Athena.ArrayFunctions();
    for (a = 0; a < d; a++) {
        e.push(a);
    }
    if (c) {
        e = b.shuffle(e);
    }
    return e;
};
Athena.BaseClass.prototype.fadeItemsIn = function(c, h) {
    var f = 0,
        a = (this.items[c] instanceof Array) ? true : false,
        e = (a) ? this.items[c].length : 1,
        b = this,
        d;

    function g() {
        if (a) {
            d = b.items[c][f];
        } else {
            d = b.items[c];
        }
        if (f < e) {
            b.items[c][f].fadeIn(b.speed, b.animeType, g);
            f++;
        } else {
            if (h !== undefined) {
                h();
            }
        }
    }
    g();
};
Athena.BaseClass.prototype.fadeItemsOut = function(c, h) {
    var a = (this.items[c] instanceof Array) ? true : false,
        f = (a) ? this.items[c].length : 1,
        b = this,
        e = f - 1,
        d;

    function g() {
        if (a) {
            d = b.items[c][e];
        }
        if (e === -1) {
            if (h !== undefined) {
                h();
            }
        } else {
            d.fadeOut(b.speed, b.animeType, g);
            e--;
        }
    }
    g();
};
Athena.BaseClass.prototype.cloneElement = function(a, b, h, g, k, j, c, d) {
    var e, l, f;
    if (a !== undefined && a !== null) {
        e = [];
        l = null;
        for (f = 0; f < a; f++) {
            if (k !== null && k !== undefined) {
                if (String(k) === "before") {
                    l = h.clone().insertBefore(j);
                } else {
                    if (String(k) === "after") {
                        l = h.clone().insertAfter(j);
                    }
                }
                if (String(k) === "prepend") {
                    l = h.clone().prependTo(b);
                } else {
                    if (String(k) === "append") {
                        l = h.clone().appendTo(b);
                    }
                }
            } else {
                l = h.clone().appendTo(b);
            }
            if (g !== null && g !== undefined) {
                l.attr("id", g + f);
            }
            if (c) {
                l.hide();
            }
            e[f] = l;
        }
        h.remove();
        if (d !== null && d !== undefined) {
            d.call(this, e);
        } else {
            return e;
        }
    }
};
Athena.BaseClass.prototype.addDragEvent = function(e, h, c, g, b, f, d) {
    var a = this;
    c = typeof c === "undefined" ? "x" : c;
    h = typeof h === "undefined" ? null : h;
    b = typeof b === "undefined" ? function() {
        this.removeValidationError++;
    } : b;
    f = typeof f === "undefined" ? function() {
        this.removeValidationError++;
    } : f;
    d = typeof d === "undefined" ? function() {
        this.removeValidationError++;
    } : d;
    g = typeof g === "undefined" ? function() {
        this.removeValidationError++;
    } : g;
    e.draggable({
        revert: function() {
            g.call(this, $(this));
        },
        revertDuration: 200,
        containment: h,
        axis: c,
        start: function(i, j) {
            b.call(this, $(this));
        },
        drag: function() {
            f.call(this);
        },
        stop: function() {
            d.call(this);
        }
    });
};
Athena.BaseClass.prototype.addDropEvent = function(c, b) {
    var a = this;
    c.droppable({
        tolerance: "intersect",
        drop: function(e, g) {
            var f, d;
            f = g.draggable;
            d = $(this);
            b.call(this, a, f, d, d.attr("id"));
        }
    });
};
Athena.BaseClass.prototype.addSemantics = function(h, n, p) {
    var a, m, d, l, c, b, o, k, f, e, g;
    p = typeof p !== "undefined" ? p : "19px";
    l = "";
    h = h.replace(new RegExp("<script[a-z0-9_ '\"-=]*>", "gi"), "").replace(new RegExp("<\/script>", "gi"), "");
    h = h.replace(new RegExp("<mstyle[a-z0-9_ '\"-=]*>", "gi"), "").replace(new RegExp("</mstyle>", "gi"), "");
    a = h.split(new RegExp("</math>", "ig"));
    k = "(<mrow[a-z0-9_ '\"-=]*>|<mo[a-z0-9_ '\"-=]*>|<mi[a-z0-9_ '\"-=]*>|<mn[a-z0-9_ '\"-=]*>|<mtext[a-z0-9_ '\"-=]*>)[a-z0-9_ '\"-=]*(" + n + ")[a-z0-9_ '\"-=]*(</mrow>|</mo>|</mi>|</mn>|</mtext>)";
    d = new RegExp(k, "ig");
    c = new RegExp("^(<mrow[a-z0-9_ '\"-=]*>|<mo[a-z0-9_ '\"-=]*>|<mi[a-z0-9_ '\"-=]*>|<mn[a-z0-9_ '\"-=]*>|<mtext[a-z0-9_ '\"-=]*>)", "ig");
    b = new RegExp("(</mrow>|</mo>|</mi>|</mn>|</mtext>)$", "ig");
    g = new RegExp("<math[a-z0-9_ '\"-=]*>(\r\n|\n|\t|\r| )?<mstyle", "ig");
    for (f = 0; f < a.length; f++) {
        if (f !== a.length - 1) {
            a[f] += "</math>";
        }
        if (!(g).test(a[f])) {
            a[f] = a[f].replace(new RegExp("<math[a-z0-9_ '\"-=]*>", "ig"), '<math><mstyle displaystyle="true" mathsize="' + p + '">').replace(new RegExp("</math>", "ig"), "</mstyle></math>");
        }
        d.compile(k, "ig");
        if (d.test(a[f])) {
            m = a[f].match(d);
            for (e = 0; e < m.length; e++) {
                o = m[e].replace(c, '<semantics><annotation-xml encoding="application/xhtml+xml">').replace(b, "</annotation-xml></semantics>");
                a[f] = a[f].replace(m[e], o);
            }
            l += a[f];
        } else {
            l += a[f];
        }
    }
    return l;
};
Athena.BaseClass.prototype.checkDevices = function() {
    var a = "";
    if ((((String(navigator.userAgent.match(/iPhone/i)) === "iPhone") || (String(navigator.userAgent.match(/Android/i)) === "Android") || (String(navigator.userAgent.match(/Android/i)) === "Tablet")) && (String(navigator.userAgent.match(/Mobile/i)) === "Mobile")) && (String(navigator.userAgent.match(/iPad/i)) !== "iPad")) {
        a = "mobile";
    }
    if ($(window).innerWidth() < 700) {
        a = "mobile";
    }
    if ((String(navigator.userAgent.match(/iPad/i)) === "iPad" || String(navigator.userAgent.match(/Android/i)) === "Android") && String(navigator.userAgent.match(/mobile/i)) !== "mobile") {
        if ($(window).innerWidth() < $(window).innerHeight()) {
            a = "mobile";
        }
    }
    return a;
};
Athena.BaseClass.prototype.addMathSizeAttributeinMathML = function(e, d) {
    var b, a, c;
    c = this.checkDevices();
    if (e.value !== undefined) {
        a = e.value;
    } else {
        a = e;
    }
    if (c === "mobile") {
        d = typeof d !== "undefined" ? d : "19px";
    } else {
        d = typeof d !== "undefined" ? d : "19px";
    }
    if ((new RegExp("<mstyle[a-z0-9_ '\"-=]*>", "g").test(e))) {
        b = a.replace(new RegExp("<mstyle[a-z0-9_ '\"-=]*>", "gi"), '<mstyle displaystyle="true" mathsize="' + d + '">');
    }
    if (!(new RegExp("<mstyle[a-z0-9_ '\"-=]*>", "g").test(e))) {
        b = a.replace(new RegExp("<math[a-z0-9_ '\"-=]*>", "ig"), '<math><mstyle displaystyle="true" mathsize="' + d + '">').replace(new RegExp("</math>", "ig"), "</mstyle></math>");
    }
    return b;
};
Athena.BaseClass.prototype.setMathFontSize = function(d) {
    var b, c, a;
    for (b in d.contents) {
        if (d.contents.hasOwnProperty(b)) {
            if (d.contents[b].value === undefined) {
                d.contents[b] = this.addMathSizeAttributeinMathML(d.contents[b]);
            } else {
                d.contents[b].value = this.addMathSizeAttributeinMathML(d.contents[b].value);
            }
        }
    }
    for (b in d.lists) {
        if (d.lists.hasOwnProperty(b)) {
            for (c in d.lists[b]) {
                if (d.lists[b].hasOwnProperty(c)) {
                    for (a in d.lists[b][c]) {
                        if (d.lists[b][c].hasOwnProperty(a)) {
                            d.lists[b][c][a].value = this.addMathSizeAttributeinMathML(d.lists[b][c][a].value);
                        }
                    }
                }
            }
        }
    }
    return d;
};
Athena.BaseClass.prototype.checkMathHasPlaceholder = function(b, c) {
    var d, a;
    d = typeof c === "undefined" ? "(dp)" : c;
    a = "(<mrow[a-z0-9_ '\"-=]*>|<mo[a-z0-9_ '\"-=]*>|<mi[a-z0-9_ '\"-=]*>|<mn[a-z0-9_ '\"-=]*>|<mtext[a-z0-9_ '\"-=]*>)[a-z0-9_ '\"-=]*(" + d + ")[a-z0-9_ '\"-=]*(</mrow>|</mo>|</mi>|</mn>|</mtext>)";
    return new RegExp(a, "ig").test(b);
};
Athena.MultipleChoiceQuestion = function() {
    this.name = "MultipleChoiceQuestion";
    this.data = null;
    this.container = null;
    this.cloneContainer = null;
    this.cloneElementArray = [];
    this.checkButton = null;
    this.tryAgainButton = null;
    this.showMeButton = null;
    this.myAnswerButton = null;
    this.selectedArray = [];
    this.currentObjectArray = [];
    this.correctArray = [];
    this.selectionType = null;
    this.contentType = null;
    this.choiceContentType = null;
    this.activeClassName = null;
    this.htmlObjects = null;
    this.select = null;
    this.selected = null;
    this.unselect = null;
    this.correct = null;
    this.correctTryCount = null;
    this.wrong = null;
    this.showme = null;
    this.answerBoxSelect = null;
    this.answerBoxUnSelect = null;
    this.answerBoxCorrect = null;
    this.answerBoxWrong = null;
    this.answerBoxShowMe = null;
    this.resetAll = null;
    this.tryCount = null;
    this.feedBack = null;
    this.hint = null;
    this.successMsg = null;
    this.tryAgainMsg = null;
    this.showmeMsg = null;
    this.hintMsg = null;
    this.failMsg = null;
    this.answerBoxWidth = null;
    this.imagePath = null;
    this.buttonCheckText = null;
    this.buttonTryAgainText = null;
    this.buttonShowMeText = null;
    this.buttonMyAnswerText = null;
    this.pluginEvent = null;
    this.refId = "";
    this.deviceType = "";
    this.myanswerFlag = true;
    this.choiceFlag = true;
    this.mobileChoiceWidth = 0;
    this.storage = {};
    this.persistOnSelect = false;
};
Athena.MultipleChoiceQuestion.extend(Athena.BaseClass);
Athena.MultipleChoiceQuestion.prototype.options = function(e, j, h, f, k, g, c) {
    var b, a, d, l = this;
    b = null;
    this.deviceType = this.checkDevices();
    if (h.pluginEvent) {
        this.pluginEvent = h.pluginEvent;
    }
    if (j !== null && j !== undefined) {
        this.htmlObjects = j;
    }
    if (j.container !== null && j.container !== undefined) {
        this.container = j.container;
    }
    if (j.cloneContainer !== null && j.cloneContainer !== undefined) {
        this.cloneContainer = j.container.find("." + j.cloneContainer);
    }
    if (j.defaultItem !== null && j.defaultItem !== undefined) {
        this.defaultItem = j.container.find("." + j.defaultItem);
    }
    if (j.checkButtonObject !== null && j.checkButtonObject !== undefined) {
        this.checkButton = j.container.find("." + j.checkButtonObject);
    }
    if (j.tryButtonObject !== null && j.tryButtonObject !== undefined) {
        this.tryAgainButton = j.container.find("." + j.tryButtonObject);
    }
    if (j.showMeButtonObject !== null && j.showMeButtonObject !== undefined) {
        this.showMeButton = j.container.find("." + j.showMeButtonObject);
    }
    if (j.myAnswersButtonObject !== null && j.myAnswersButtonObject !== undefined) {
        this.myAnswerButton = j.container.find("." + j.myAnswersButtonObject);
    }
    if (j.checkBtnActive !== null && j.checkBtnActive !== undefined) {
        this.activeClassName = j.checkBtnActive;
    }
    if (j.feedBackTextClass !== null && j.feedBackTextClass !== undefined) {
        this.feedBack = j.feedBackTextClass;
    }
    if (j.hint !== null && j.hint !== undefined) {
        this.hint = j.container.find("." + j.hint);
    }
    if (j.refId !== null && j.refId !== undefined) {
        this.refId = j.refId;
    }
    if (j.persistOnSelect !== null && j.persistOnSelect !== undefined) {
        this.persistOnSelect = j.persistOnSelect;
    }
    if (e !== null && e !== undefined) {
        this.data = e;
    }
    if (e.contents.correct_option !== null && e.contents.correct_option !== undefined) {
        this.correctArray = e.contents.correct_option.value.split(",");
    }
    if (e.contents.total_attempts !== null && e.contents.total_attempts !== undefined) {
        this.tryCount = Number(e.contents.total_attempts.value);
    }
    if (e.contents.reset_all !== null && e.contents.reset_all !== undefined) {
        this.resetAll = e.contents.reset_all.value;
    }
    if (h.imagePath !== null && h.imagePath !== undefined) {
        this.imagePath = h.imagePath;
    }
    if (h.buttonCheckText !== null && h.buttonCheckText !== undefined) {
        this.buttonCheckText = h.buttonCheckText;
    }
    if (h.buttonTryAgainText !== null && h.buttonTryAgainText !== undefined) {
        this.buttonTryAgainText = h.buttonTryAgainText;
    }
    if (h.buttonShowMeText !== null && h.buttonShowMeText !== undefined) {
        this.buttonShowMeText = h.buttonShowMeText;
    }
    if (h.buttonMyAnswerText !== null && h.buttonMyAnswerText !== undefined) {
        this.buttonMyAnswerText = h.buttonMyAnswerText;
    }
    if (j.successMsg !== null && j.successMsg !== undefined) {
        this.successMsg = j.successMsg;
    }
    if (j.failMsg !== null && j.failMsg !== undefined) {
        this.failMsg = j.failMsg;
    }
    if (j.tryAgainMsg !== null && j.tryAgainMsg !== undefined) {
        this.tryAgainMsg = j.tryAgainMsg;
    }
    if (j.showmeMsg !== null && j.showmeMsg !== undefined) {
        this.showmeMsg = j.showmeMsg;
    }
    if (e.contents.hint !== null && e.contents.hint !== undefined) {
        this.hintMsg = e.contents.hint.value;
        this.hint.find("span").html(this.hintMsg);
        if (e.contents.hint.objectId !== undefined) {
            this.hint.attr("data-objectid", e.contents.hint.objectId);
        }
    }
    if (String(e.contents.clonable.value) === "true") {
        this.createClone(this.defaultItem, k, this.cloneContainer, g, c);
    } else {
        this.renderDropDown(null);
    }
    b = l.htmlObjects.mainContainer.parents(".interactivity-container");
    $(window).on("orientationchange resize", function(n) {
        var m, i;
        l.deviceType = l.checkDevices();
        setTimeout(function() {
            if (l.deviceType === "mobile") {
                if (String(l.choiceContentType).toLowerCase() !== "dropdown") {
                    if (String(l.contentType) !== "image") {
                        l.setContentWidth();
                    }
                    l.addScrollToTable();
                }
            } else {
                m = l.container.find(".question_container").find("table");
                if (Number(m.length) !== 0) {
                    for (i = 0; i < m.length; i++) {
                        l.container.find(".question_container").find(".scroll-container-" + i).mCustomScrollbar("destroy");
                    }
                }
                if (String(l.choiceContentType).toLowerCase() !== "dropdown") {
                    if (String(l.contentType) !== "image") {
                        l.setContentWidth();
                    }
                }
            }
            l.htmlObjects.mainContainer.find(".single-container").find(".centerAlign").css("width", l.answerBoxWidth + "px");
            l.htmlObjects.mainContainer.find(".single-image-container").find(".centerAlign").css("width", l.answerBoxWidth + "px");
        }, 1000);
    });
    if (String(l.choiceContentType).toLowerCase() !== "dropdown") {
        this.addScrollToTable();
    }
};
Athena.MultipleChoiceQuestion.prototype.addScrollToTable = function() {
    var a, b, c;
    if (this.deviceType === "mobile") {
        c = this.container.find(".question_container").find("table");
        if (Number(c.length) !== 0) {
            for (b = 0; b < c.length; b++) {
                if (this.container.find(".question_container").find(".scroll-container-" + b).length <= 0) {
                    $(c[b]).wrapAll("<div class='scroll-container-" + b + "'></div>");
                }
                this.container.find(".question_container").find(".scroll-container-" + b).find("table").css({
                    "margin-left": "0px",
                    width: "329px"
                });
                this.container.find(".question_container").find(".scroll-container-" + b).css({
                    margin: "0 auto",
                    width: "90%",
                    overflow: "hidden",
                    position: "relative",
                    height: "auto"
                });
                this.container.find(".question_container").find(".scroll-container-" + b).find("table").css("width", "100%");
                this.container.find(".question_container").find(".scroll-container-" + b).mCustomScrollbar({
                    horizontalScroll: true,
                    autoHideScrollbar: true,
                    scrollInertia: 100
                });
                this.container.find(".question_container").find(".scroll-container-" + b).find(".mCSB_container").css("margin-bottom", "10px");
                this.container.find(".question_container").find(".scroll-container-" + b).find(".mCSB_container").css("width", this.container.find(".question_container").find(".scroll-container-" + b).find(".mCSB_container").width() + 40);
            }
        }
    }
};
Athena.MultipleChoiceQuestion.prototype.removeLineBreakandTrim = function(a) {
    a = a.replace(new RegExp("\n", "gm"), "").trim();
    return a;
};
Athena.MultipleChoiceQuestion.prototype.createClone = function(a, b, d, c, e) {
    var f;
    if (String(this.data.lists.list1) !== "" && this.data.lists.list1 !== null && this.data.lists.list1 !== undefined) {
        f = this.data.lists.list1.length;
    } else {
        if (this.data.contents.choiceContentArray !== null && String(this.data.contents.choiceContentArray) !== "" && this.data.contents.choiceContentArray !== undefined) {
            f = this.data.contents.choiceContentArray.length;
        }
    }
    if (e !== null && e !== undefined) {
        this.cloneElement(f, this.cloneContainer, a, null, b, null, c, e);
    } else {
        this.cloneElement(f, this.cloneContainer, a, null, b, null, c, this.renderChoiceCreator);
    }
};
Athena.MultipleChoiceQuestion.prototype.renderChoiceCreator = function(b) {
    var a;
    this.cloneElementArray = b;
    a = this;
    a.choiceCreate(a.data, a.cloneElementArray, a.htmlObjects.choiceIdPrefix, a.htmlObjects.choiceBtnClassName, a.htmlObjects.choiceTextClassName, a.htmlObjects.questionObject, null);
    this.setStyleClassNames(this.htmlObjects.styleClassNames);
};
Athena.MultipleChoiceQuestion.prototype.choiceCreate = function(A, o, w, d, b, g, p) {
    var z, r, k, c, h, y, f, v, u, s, t, a, x, l, e, q;
    r = this;
    z = [];
    k = A.contents.question.value;
    if (A.contents.choiceContentArray !== undefined && A.contents.choiceContentArray !== null) {
        c = A.contents.choiceContentArray.value;
    } else {
        c = [];
    }
    h = A.lists.list1;
    this.contentType = A.contents.content_type.value;
    f = this.cloneElementArray.length;
    this.choiceContentType = A.contents.contentChoiceType !== undefined ? A.contents.contentChoiceType.value : undefined;
    this.selectionType = A.contents.choice_type.value;
    if (k !== null && k !== undefined) {
        if (g !== null && g !== undefined) {
            g = this.container.find("." + g);
            g.html(k);
            if (A.contents.question.objectId !== undefined) {
                g.attr("data-objectid", A.contents.question.objectId);
            }
        }
    }
    if (f !== undefined && f !== null) {
        x = null;
        q = [];
        q = r.container.find("img");
        this.countImages = 0;
        for (v = 0; v < f; v++) {
            x = this.cloneElementArray[v];
            if (h.length !== 0 && this.contentType === "text") {
                l = x.find("." + b);
                l.empty().append(h[v].option.value);
                if (h[v].option.objectId !== null) {
                    x.attr("data-objectid", h[v].option.objectId);
                }
            } else {
                if (h.length !== 0 && this.contentType === "image") {
                    l = x.find("." + b);
                    this.calculateImageWidth(l, h, v, q);
                    l.css("background-size", "100% 100%");
                    if (h[v].option.objectId !== null) {
                        x.attr("data-objectid", h[v].option.objectId);
                    }
                }
            }
            a = x.find("." + d);
            a.attr("id", w + v);
            if (c.length !== 0 && this.choiceContentType === "text") {
                a.empty().append(c[v].option.value);
                if (c[v].option.objectId !== null) {
                    a.attr("data-objectid", c[v].option.objectId);
                }
            } else {
                if (c.length !== 0 && this.choiceContentType === "image") {
                    a.attr("src", this.imagePath + h[v].option.value);
                    a.css("background-size", "100% 100%");
                } else {
                    if (c.length !== 0 && this.choiceContentType.toLowerCase() === "dropdown") {
                        for (u = 0; u < c[v].length; u++) {
                            a.empty().append(new Option(c[v][u].option.value, u));
                        }
                    }
                }
            }
            z[v] = a;
        }
        this.currentObjectArray = z;
        this.setEvents(z, null);
        if (A !== null && A !== undefined) {
            if (this.checkButton !== null && this.checkButton !== undefined) {
                this.checkButton.find("span").empty().html(this.buttonCheckText);
            }
            if (this.tryAgainButton !== null && this.tryAgainButton !== undefined) {
                this.tryAgainButton.find("span").empty().html(this.buttonTryAgainText);
            }
            if (this.resetButton !== null && this.resetButton !== undefined) {
                this.resetButton.find("span").empty().html("Reset");
            }
            if (this.showMeButton !== null && this.showMeButton !== undefined) {
                this.showMeButton.find("span").empty().html(this.buttonShowMeText);
            }
            if (this.myAnswerButton !== null && this.myAnswerButton !== undefined) {
                this.myAnswerButton.find("span").empty().html(this.buttonMyAnswerText);
            }
        }
        if (p !== null && p !== undefined) {
            p(z);
        } else {
            return z;
        }
    }
};
Athena.MultipleChoiceQuestion.prototype.calculateImageWidth = function(e, d, b, c) {
    var a = this;
    e.load(function() {
        a.countImages++;
        if (Number(c.length) === Number(a.countImages)) {
            a.setContentWidth();
        }
    }).attr("src", this.imagePath + d[b].option.value);
};
Athena.MultipleChoiceQuestion.prototype.setContentWidth = function() {
    var l, y, L, a, d, Q, s, A, q, g, O, b, R, I, S, f, B, k, P, F, u, c, o, D, r, E, e, C, x, w, N, M, J, K, h, G, p, z, v, t, H;
    l = this;
    g = true;
    A = true;
    q = 0;
    s = 0;
    O = null;
    b = [];
    I = [];
    S = [];
    O = l.htmlObjects.mainContainer.parents(".interactivity-container");
    for (N = 0; N < l.cloneElementArray.length; N++) {
        if ((l.data.contents.reduce_font !== undefined && String(l.data.contents.reduce_font.value) === "true")) {
            if (l.data.contents.target_font_size !== undefined && l.data.contents.target_font_size !== null && String(l.data.contents.target_font_size.value) !== "") {
                l.cloneElementArray[N].find("." + l.htmlObjects.choiceTextClassName).css("font-size", l.data.contents.target_font_size + "px");
            }
        }
    }
    for (N = 0; N < l.cloneElementArray.length; N++) {
        I[N] = l.cloneElementArray[N].width();
        u = l.cloneElementArray[N].parent().width();
        r = l.cloneElementArray[N].find("." + l.htmlObjects.choiceBtnClassName).width();
        E = parseInt(l.cloneElementArray[N].find("." + l.htmlObjects.choiceBtnClassName).css("margin-left"), 10);
        e = parseInt(l.cloneElementArray[N].find("." + l.htmlObjects.choiceBtnClassName).css("margin-right"), 10);
    }
    k = Math.max.apply(Math, I);
    if (l.data.contents.maxHorizontal_options !== null && l.data.contents.maxHorizontal_options !== undefined && Number(l.data.contents.maxHorizontal_options.value) !== 0) {
        if ((k * Number(l.data.contents.maxHorizontal_options.value)) > u) {
            F = (u / Number(l.data.contents.maxHorizontal_options.value));
        } else {
            F = k;
        }
    }
    if (l.cloneContainer.find(".groupParent").width() !== null) {
        w = l.cloneContainer.find(".groupParent").width();
    } else {
        w = F;
    }
    for (N = 0; N < l.cloneElementArray.length; N++) {
        if (String(l.contentType) !== "image") {
            l.cloneElementArray[N].css("width", ((l.answerBoxWidth / l.cloneElementArray[N].parent().width()) * 100) + "%");
        }
    }
    if (l.data !== null && l.data !== undefined && l.data.contents.maxHorizontal_options !== null && l.data.contents.maxHorizontal_options !== undefined && Number(l.data.contents.maxHorizontal_options.value) !== 0) {
        for (J = 0; J < l.cloneElementArray.length; h++) {
            G = null;
            for (K = 0; K < Number(l.data.contents.maxHorizontal_options.value); K++) {
                if (Number(K) === 0) {
                    G = l.cloneElementArray[J];
                } else {
                    G = G.add(l.cloneElementArray[J]);
                }
                J++;
            }
            G.wrapAll('<div class="centerAlign"></div>');
            p = G.parent();
            if (l.deviceType === "mobile") {
                p.css("margin", "0 auto");
            }
            if (!g) {
                p.css("width", "272px");
            }
            if (String(l.contentType) !== "image") {
                f = w * Number(l.data.contents.maxHorizontal_options.value) + Number(l.data.contents.maxHorizontal_options.value) * 2;
                l.answerBoxWidth = f;
                p.css("width", f + "px");
                p.wrapAll('<div class="groupParent"></div>');
            } else {
                z = l.cloneElementArray[0].find("." + l.htmlObjects.choiceTextClassName);
                f = ((w * Number(l.data.contents.maxHorizontal_options)) - (Number(l.data.contents.maxHorizontal_options) * (2 + parseInt(z.css("margin-right"), 10) + parseInt(z.css("margin-left"), 10))) + 1);
                l.answerBoxWidth = f;
                p.css("width", f + "px");
                p.wrapAll('<div class="groupParent"></div>');
            }
        }
        for (N = 0; N < l.cloneElementArray.length; N++) {
            if (l.cloneElementArray[N].find(".choice-text").children().length !== 0) {
                y = String(l.cloneElementArray[N].find(".choice-text").text());
                if (Number(y.indexOf("<math>")) !== -1) {
                    A = false;
                }
            }
        }
        for (N = 0; N < l.cloneElementArray.length; N++) {
            z = l.cloneElementArray[N].find("." + l.htmlObjects.choiceTextClassName);
            C = parseInt(z.css("margin-right"), 10);
            x = parseInt(z.css("margin-left"), 10);
            B = ((l.cloneElementArray[N].parent().width()) / Number(l.data.contents.maxHorizontal_options.value)) - 2;
            if (String(l.contentType) !== "image") {
                l.cloneElementArray[N].css("width", ((B / l.cloneElementArray[N].parent().width()) * 100) + "%");
                l.cloneElementArray[N].parent().css("width", (l.cloneElementArray[N].parent().width() + 55) + "px");
                b[N] = l.cloneElementArray[N].width();
            } else {
                l.cloneElementArray[N].css("width", (z.width() + C + x));
            }
            D = Number(r + e + E + C + x);
            o = Number(l.cloneElementArray[N].width() - 5);
            if (String(l.contentType) !== "image") {
                if (A) {
                    z.css("width", (((o - D + 2) / z.parent().width()) * 100) + "%");
                }
            }
            S[N] = l.cloneElementArray[N].height();
        }
        P = Math.max.apply(Math, S);
        l.answerBoxHeight = P;
        t = 0;
        H = 0;
        for (N = 0; N < l.cloneElementArray.length; N++) {
            l.cloneElementArray[N].attr("curWidth", l.cloneElementArray[N].width() + "px");
            l.cloneElementArray[N].attr("curHeight", l.cloneElementArray[N].height() + "px");
            t = t > l.cloneElementArray[N].height() ? t : l.cloneElementArray[N].height();
            H = H > l.cloneElementArray[N].width() ? H : l.cloneElementArray[N].height();
            $(l.cloneElementArray[N]).find(".choice-text").attr("curWidth", $(l.cloneElementArray[N]).find(".choice-text").width() + "px");
        }
        if (String(l.contentType) === "image") {
            O.find(".choice-element").css("height", t);
            O.find(".choice-element").css("width", H);
            O.find(".choice-element").parents(".groupParent").css("width", "auto");
        }
        if (l.deviceType === "mobile") {
            if (typeof MathJax !== "undefined") {
                MathJax.Hub.Queue(["Typeset", MathJax.Hub, l.htmlObjects.mainContainer.toArray()[0].querySelector(".inner-Content"), function() {
                    if ((String(l.selectionType).toLowerCase() === "single") && (String(l.contentType) === "text")) {
                        if (!A) {
                            a = document.createElement("table");
                            for (N = 0; N < l.cloneElementArray.length; N++) {
                                s = parseInt($(l.cloneElementArray[N]).find(".choice-text").attr("curWidth"), 10);
                                if (q > s) {
                                    q = Number(q);
                                } else {
                                    q = s;
                                }
                            }
                            for (N = 0; N < l.cloneElementArray.length; N++) {
                                Q = document.createElement("tr");
                                d = document.createElement("td");
                                $(l.cloneElementArray[N]).detach().appendTo(d);
                                Q.appendChild(d);
                                a.appendChild(Q);
                                $(l.cloneElementArray[N]).attr("curWidth", $(l.cloneElementArray[N]).width() + "px");
                                $(l.cloneElementArray[N]).css("clear", "both");
                            }
                            O.find(".msq-resp-container").append($(a));
                            O.find(".msq-resp-container").css("width", "95%");
                            O.find(".msq-resp-container").css({
                                width: "100%",
                                overflow: "hidden",
                                margin: "0 auto",
                                height: "auto"
                            });
                            O.find(".msq-resp-container").find("table").find(".choice-element").css("width", q + 200 + "px");
                            O.find(".msq-resp-container").find("table").css("width", "100%");
                            O.find(".msq-resp-container").mCustomScrollbar({
                                horizontalScroll: true,
                                autoHideScrollbar: true,
                                scrollInertia: 100
                            });
                            O.find(".msq-resp-container").find(".mCSB_container").css("margin-bottom", "10px");
                            for (N = 0; N < l.cloneElementArray.length; N++) {
                                l.cloneElementArray[N] = O.find(".msq-resp-container").find(".choice-element").eq(N);
                                l.currentObjectArray[N] = O.find(".msq-resp-container").find(".choice-element").eq(N).find(".choice-btn");
                            }
                        }
                    }
                }]);
            }
        }
        if (l.deviceType === "mobile") {
            if ((String(l.selectionType).toLowerCase() === "multiple") && (String(l.contentType) === "text")) {
                if ($(window).innerWidth() < $(window).innerHeight()) {
                    L = 0;
                    for (N = 0; N < l.cloneElementArray.length; N++) {
                        $(l.cloneElementArray[N]).detach().appendTo(O.find(".msq-resp-container"));
                        $(l.cloneElementArray[N]).css({
                            width: $(l.cloneElementArray[N]).attr("curWidth")
                        });
                        L = $(l.cloneElementArray[N]).attr("curWidth");
                    }
                    O.find(".msq-resp-container").css({
                        width: L,
                        margin: "0 auto"
                    });
                    O.find(".msq-resp-container").attr("curWidth", L);
                    O.find(".groupParent").remove();
                    O.find(".msq-resp-container").find(".groupParent").remove();
                } else {
                    O.find(".msq-resp-container").css({
                        width: "90%",
                        margin: "0 auto"
                    });
                    L = 0;
                    for (N = 0; N < l.cloneElementArray.length; N++) {
                        $(l.cloneElementArray[N]).detach().appendTo(O.find(".msq-resp-container"));
                        $(l.cloneElementArray[N]).css({
                            width: $(l.cloneElementArray[N]).attr("curWidth")
                        });
                        L = $(l.cloneElementArray[N]).attr("curWidth");
                    }
                    O.find(".msq-resp-container").attr("curWidth", L);
                    O.find(".groupParent").remove();
                    O.find(".msq-resp-container").find(".groupParent").remove();
                }
            } else {
                if (String(l.contentType) === "image") {
                    if ($(window).innerWidth() < $(window).innerHeight()) {
                        for (N = 0; N < l.cloneElementArray.length; N++) {
                            R = Number($(l.cloneElementArray[N]).width());
                        }
                        O.find(".msq-resp-container").css({
                            width: R + "px",
                            margin: "0 auto"
                        });
                        for (N = 0; N < l.cloneElementArray.length; N++) {
                            R = Number($(l.cloneElementArray[N]).width());
                            $(l.cloneElementArray[N]).detach().appendTo(O.find(".msq-resp-container"));
                        }
                    } else {
                        for (N = 0; N < l.cloneElementArray.length; N++) {
                            R = Number($(l.cloneElementArray[N]).width());
                        }
                        v = 0;
                        if (l.cloneElementArray.length >= 4) {
                            v = Number(l.cloneElementArray.length) / 2;
                        }
                        if (v === 0) {
                            R = R * Number(l.cloneElementArray.length) + Number(8);
                            O.find(".msq-resp-container").css({
                                width: R + "px",
                                margin: "0 auto"
                            });
                        } else {
                            R = Math.round(Number(v)) * R + Number(8);
                            O.find(".msq-resp-container").css({
                                width: R + "px",
                                margin: "0 auto"
                            });
                        }
                        for (N = 0; N < l.cloneElementArray.length; N++) {
                            $(l.cloneElementArray[N]).detach().appendTo(O.find(".msq-resp-container"));
                        }
                    }
                }
            }
        }
    }
};
Athena.MultipleChoiceQuestion.prototype.reCalculateWidth = function() {
    var b, d, c, k, j, f, g, e, a, h = this;
    b = this.data.lists.list1;
    if (String(this.choiceContentType).toLowerCase() !== "dropdown" && String(this.contentType) !== "image") {
        h.setContentWidth();
    }
};
Athena.MultipleChoiceQuestion.prototype.setStyleClassNames = function(a) {
    this.answerBoxSelect = a.answerBoxSelect;
    this.answerBoxUnSelect = a.answerBoxUnSelect;
    this.answerBoxCorrect = a.answerBoxCorrect;
    this.answerBoxWrong = a.answerBoxWrong;
    this.answerBoxShowMe = a.answerBoxShowMe;
    this.select = a.select;
    this.selected = a.selected;
    this.unselect = a.unselect;
    this.correct = a.correct;
    this.correctTryCount = a.correctTryCount;
    this.wrong = a.wrong;
    this.showme = a.showme;
};
Athena.MultipleChoiceQuestion.prototype.setEvents = function(b, e) {
    var c, d, a;
    d = b.length;
    a = this;
    for (c = 0; c < d; c++) {
        a.setClickEvents(b[c], e);
        b[c].css({
            cursor: "pointer"
        });
    }
    a.setTryOption();
    a.setShowMeOption();
    a.setMyAnswersOption();
};
Athena.MultipleChoiceQuestion.prototype.setClickEvents = function(a, d) {
    var b, c, e;
    b = this;
    a.parent().unbind("click");
    a.parent().bind("click", function() {
        c = $(this).find(".choice-btn").attr("id").split("-");
        e = c[c.length - 1];
        b.onSelectItems(e, null, null, null, null, b.activeClassName, null);
        if (d !== null && d !== undefined) {
            d(e, b);
        } else {
            return e;
        }
    });
};
Athena.MultipleChoiceQuestion.prototype.removeEvents = function(a) {
    var b, c;
    c = a.length;
    for (b = 0; b < c; b++) {
        a[b].parent().unbind("click");
        if (String(this.choiceContentType).toLowerCase() === "dropdown") {
            a[b].find("." + this.clickClassName).unbind("click");
        } else {
            if (String(a[b].get(0).tagName).toLowerCase() === "input") {
                a[b].attr("disabled", true);
            }
        }
    }
};
Athena.MultipleChoiceQuestion.prototype.setCheckEvent = function(c, f) {
    var b, d, e, a;
    b = this;
    e = 0;
    if (f !== null && f !== undefined) {
        this.tryCount = f;
        this.storage._trylimit = this.tryCount;
    }
    b.checkButton.unbind("click");
    b.checkButton.bind("click", function() {
        b.storage._checkClicked = "true";
        b.storage._tryClicked = "false";
        for (d = 0; d < b.currentObjectArray.length; d++) {
            b.currentObjectArray[d].css("cursor", "default");
        }
        b.container.find("." + b.unselect).unbind("click");
        e = b.selectedArray.length;
        if (Number(e) === 0) {
            alert("Please select the answers");
        } else {
            a = null;
            a = b.validation(null, b.correctArray, null, null);
            if (String(a).toLowerCase() === "pass") {
                if (b.data.contents.feedback_correct.objectId !== undefined) {
                    b.container.find("." + b.feedBack).attr("data-objectid", b.data.contents.feedback_correct.objectId);
                }
                b.container.find("." + b.feedBack).show().empty().html(b.successMsg);
                b.container.find("." + b.feedBack).parent().addClass("hasText");
                b.feedbackAutoHide();
            } else {
                if (String(a).toLowerCase() === "fail") {
                    if (Number(b.tryCount) > 0) {
                        b.tryAgainButton.show();
                        b.hint.parent().show();
                        b.hint.find("span").html(b.hintMsg);
                        if (typeof MathJax !== "undefined") {
                            MathJax.Hub.Queue(["Typeset", MathJax.Hub, b.htmlObjects.mainContainer.toArray()[0].querySelector("." + b.htmlObjects.hint), function() {
                                var g;
                            }]);
                        }
                        b.container.find("." + b.feedBack).show().empty().html(b.tryAgainMsg);
                        b.container.find("." + b.feedBack).parent().addClass("hasText");
                        if (b.data.contents.feedback_first_incorrect.objectId !== undefined) {
                            b.container.find("." + b.feedBack).attr("data-objectid", b.data.contents.feedback_first_incorrect.objectId);
                        }
                    } else {
                        if (String(b.selectionType).toLowerCase() === "multiple") {
                            b.showMeButton.show();
                            b.hint.find("span").empty();
                            b.container.find("." + b.feedBack).show().empty().html(b.failMsg);
                            b.container.find("." + b.feedBack).parent().addClass("hasText");
                            b.feedbackAutoHide();
                            if (b.data.contents.feedback_second_incorrect.objectId !== undefined) {
                                b.container.find("." + b.feedBack).attr("data-objectid", b.data.contents.feedback_second_incorrect.objectId);
                            }
                        } else {
                            b.showCorrectAnswers(null, null);
                            b.hint.find("span").empty();
                            b.container.find("." + b.feedBack).parent().addClass("hasText");
                            b.container.find("." + b.feedBack).show().empty().html(b.showmeMsg);
                            b.feedbackAutoHide();
                            if (b.data.contents.show_answer.objectId !== undefined) {
                                b.container.find("." + b.feedBack).attr("data-objectid", b.data.contents.show_answer.objectId);
                            }
                        }
                    }
                }
            }
            $(this).hide();
        }
        b.persistData();
    });
};
Athena.MultipleChoiceQuestion.prototype.setTryOption = function() {
    var g, c, e, l = this,
        h, k, f, b, a, d;
    l.tryAgainButton.unbind("click");
    l.tryAgainButton.bind("click", function() {
        l.storage._tryClicked = "true";
        if (Number(l.tryCount) > 0) {
            l.container.find("." + l.feedBack).empty();
            l.container.find("." + l.feedBack).parent().removeClass("hasText");
            l.hint.parent().hide();
            l.hint.find("span").empty();
            $(this).hide();
            l.checkButton.show();
            l.checkButton.unbind("click");
            l.checkButton.removeClass(l.activeClassName);
            l.container.find("." + l.feedBack).removeClass("hasText").empty();
            l.container.find("." + l.wrongTextClassName).hide();
            if (String(l.choiceContentType).toLowerCase() === "dropdown") {
                g = 0;
                l.container.find("." + l.dropDownClassName).each(function() {
                    if ($(this).hasClass("dropdown-wrong")) {
                        d = $(this).attr("id");
                        a = $("#" + d + "-body").find("li:eq(0) div").html();
                        $(this).find("ul li:eq(0) span").html(a);
                        $(this).find("ul li:eq(0) span").css("font-size", "20px");
                        l.selectedArray[g] = undefined;
                    }
                    $(this).removeClass("DisableMode");
                    g++;
                });
                l.setDropDownEvents(l.currentObjectArray, l.optionsClass, l.replaceText, l.clickClassName, null);
                l.container.find(".dropdown-correct").addClass(l.correctTryCount);
                l.container.find(".dropdown-correct").addClass(l.correctTryCount);
                l.container.find(".dropdown-wrong").removeClass("dropdown-wrong");
            } else {
                l.setEvents(l.currentObjectArray, null);
                l.tryAgainReset(null, null);
            }
            for (g = 0; g < l.currentObjectArray.length; g++) {
                l.currentObjectArray[g].css("cursor", "pointer");
            }
            if (String(l.choiceContentType).toLowerCase() === "dropdown") {
                l.container.find(".dropdown-correct").addClass(l.correctTryCount);
            }
        } else {
            l.hint.find("span").empty();
            l.container.find("." + l.feedBack).parent().addClass("hasText");
            l.container.find("." + l.feedBack).show().empty().html(l.showmeMsg);
        }
        l.tryCount--;
        l.storage._trylimit = l.tryCount;
        l.persistData();
    });
};
Athena.MultipleChoiceQuestion.prototype.setShowMeOption = function() {
    var a = this;
    a.showMeButton.unbind("click");
    this.showMeButton.bind("click", function() {
        a.storage._showClicked = "true";
        delete a.storage._myanswerClicked;
        a.showCorrectAnswers(null, null);
        $(this).hide();
        a.myAnswerButton.show();
        a.container.find("." + a.feedBack).parent().addClass("hasText");
        a.container.find("." + a.feedBack).show().empty().html(a.showmeMsg);
        a.persistData();
    });
};
Athena.MultipleChoiceQuestion.prototype.setMyAnswersOption = function() {
    var a = this;
    a.myAnswerButton.unbind("click");
    this.myAnswerButton.bind("click", function() {
        a.storage._myanswerClicked = "true";
        delete a.storage._showClicked;
        if (String(a.choiceContentType).toLowerCase() === "dropdown") {
            a.dropDownvalidation(a.currentObjectArray, a.correctArray, a.selectedArray, null);
        } else {
            a.validation(null, null, null, null);
        }
        $(this).hide();
        a.showMeButton.show();
        a.container.find("." + a.feedBack).parent().removeClass("hasText");
        a.container.find("." + a.feedBack).empty();
        a.persistData();
    });
};
Athena.MultipleChoiceQuestion.prototype.feedbackAutoHide = function() {
    var a = this;
    setTimeout(function() {
        a.container.find("." + a.feedBack).fadeIn();
    }, 0);
    setTimeout(function() {
        a.container.find("." + a.feedBack).parent().removeClass("hasText");
        a.container.find("." + a.feedBack).fadeOut();
        a.container.find("." + a.feedBack).empty();
    }, 2000);
};
Athena.MultipleChoiceQuestion.prototype.onSelectItems = function(h, f, d, c, g, a, e) {
    var b;
    if (a !== null && a !== undefined) {
        this.activeClassName = a;
    }
    if (f !== null && f !== undefined && Number(f.length) !== 0) {
        this.currentObjectArray = f;
    }
    if (g !== null && g !== undefined && Number(g.length) !== 0) {
        this.selectedArray = g;
    }
    if (d !== null && d !== undefined) {
        this.selectionType = d;
    }
    if (c !== null && c !== undefined) {
        this.choiceContentType = c;
    }
    if (h !== undefined && h !== null) {
    	
    	Collab.multiChoiceSelect(this.container.selector + " .choice-btn:eq(" + h +")");
    	
        if (String(this.selectionType).toLowerCase() === "multiple") {
            if (this.selectedArray[h] === undefined || String(this.selectedArray[h]) === "") {
                this.selectedArray[h] = h;
                this.storeUserSelectedItem();
                if (String(this.choiceContentType).toLowerCase() !== "dropdown") {
                    if (this.unselect !== null && this.unselect !== undefined) {
                        this.currentObjectArray[h].removeClass(this.unselect);
                        this.cloneElementArray[h].removeClass(this.answerBoxUnSelect);
                    }
                    if (this.select !== null && this.select !== undefined) {
                        this.currentObjectArray[h].addClass(this.select);
                        this.cloneElementArray[h].addClass(this.answerBoxSelect);
                    }
                    this.currentObjectArray[h].prop("checked", true);
                }
            } else {
                this.selectedArray[h] = undefined;
                if (String(this.choiceContentType).toLowerCase() !== "dropdown") {
                    if (this.unselect !== null && this.unselect !== undefined) {
                        this.currentObjectArray[h].addClass(this.unselect);
                        this.cloneElementArray[h].addClass(this.answerBoxUnSelect);
                    }
                    if (this.select !== null && this.select !== undefined) {
                        this.currentObjectArray[h].removeClass(this.select);
                        this.cloneElementArray[h].removeClass(this.answerBoxSelect);
                    }
                    this.currentObjectArray[h].prop("checked", false);
                }
            }
        } else {
            if (String(this.selectionType).toLowerCase() === "single") {
                this.selectedArray[0] = h;
                this.storeUserSelectedItem();
                if (String(this.choiceContentType).toLowerCase() !== "dropdown") {
                    this.setInitialBackground(null, null);
                    if (this.unselect !== null && this.unselect !== undefined) {
                        this.currentObjectArray[h].removeClass(this.unselect);
                        this.cloneElementArray[h].removeClass(this.answerBoxUnSelect);
                    }
                    if (this.select !== null && this.select !== undefined) {
                        this.currentObjectArray[h].addClass(this.select);
                        this.cloneElementArray[h].addClass(this.answerBoxSelect);
                    }
                    this.currentObjectArray[h].prop("checked", true);
                }
            }
        }
        this.changeCheckButtonStatus(null, a, null, null);
        if (e !== null && e !== undefined) {
            e(this.selectedArray);
        } else {
            return this.selectedArray;
        }
    }
};
Athena.MultipleChoiceQuestion.prototype.changeCheckButtonStatus = function(e, a, c, d) {
    var b, f;
    if (e !== null && e !== undefined) {
        this.checkButton = e;
    }
    if (a !== null && a !== undefined) {
        this.activeClassName = a;
    }
    if (c !== null && c !== undefined) {
        this.selectionType = c;
    }
    if (d !== null && d !== undefined) {
        this.selectedArray = d;
    }
    if (String(this.selectionType).toLowerCase() === "single") {
        this.setCheckEvent();
        this.checkButton.addClass(this.activeClassName);
    } else {
        if (String(this.selectionType).toLowerCase() === "multiple") {
            b = 0;
            for (f = 0; f < this.selectedArray.length; f++) {
                if (this.selectedArray[f] === undefined) {
                    b++;
                }
            }
            b = this.selectedArray.length - b;
            if (b !== null && b !== undefined && Number(b) !== 0) {
                this.setCheckEvent();
                this.checkButton.addClass(this.activeClassName);
            } else {
                this.checkButton.removeClass(this.activeClassName);
            }
        }
    }
};
Athena.MultipleChoiceQuestion.prototype.showCorrectAnswers = function(e, c) {
    var f, d, b, l, g, k, h;
    l = this;
    if (e !== null && e !== undefined && Number(e.length) !== 0) {
        l.correctArray = e;
    }
    if (String(l.selectionType).toLowerCase() === "multiple" || String(l.choiceContentType).toLowerCase() === "dropdown") {
        this.setInitialBackground(null, null);
        for (f = 0; f < l.currentObjectArray.length; f++) {
            l.currentObjectArray[f].prop("checked", false);
        }
    }

    function a(j) {
        setTimeout(function() {
            if (typeof MathJax !== "undefined") {
                MathJax.Hub.Queue(["Typeset", MathJax.Hub, l.container.toArray()[0].querySelector(("#drop-down-id-" + l.refId + "-" + j)), function() {
                    var i;
                }]);
            }
        }, 0);
    }
    for (f = 0; f < l.currentObjectArray.length; f++) {
        for (d = 0; d < l.correctArray.length; d++) {
            if (Number(f) === Number(l.correctArray[d])) {
                if (l.showme !== null && l.showme !== undefined) {
                    l.currentObjectArray[f].addClass(l.showme);
                    l.cloneElementArray[f].addClass(l.answerBoxShowMe);
                    if (String(l.choiceContentType).toLowerCase() === "dropdown") {
                        l.container.find("#drop-down-id-" + l.refId + "-" + f).addClass(l.showme);
                        l.container.find("#drop-down-id-" + l.refId + "-" + f).addClass(l.answerBoxShowMe);
                    }
                }
                l.currentObjectArray[f].prop("checked", true);
            }
        }
        g = null;
        if (String(l.choiceContentType).toLowerCase() === "dropdown") {
            if (String(l.currentObjectArray[f].get(0).tagName).toUpperCase() === "SELECT") {
                g = l.currentObjectArray[f].find("." + l.clickClassName);
                for (b = 0; b < g.length; b++) {
                    k = g[b];
                    if (Number($(k).val()) === Number(l.correctArray[f])) {
                        l.currentObjectArray[f].val($(k).val());
                        l.currentObjectArray[f].addClass(l.showme);
                        l.cloneElementArray[f].addClass(l.answerBoxShowMe);
                        if (String(l.choiceContentType).toLowerCase() === "dropdown") {
                            l.container.find("#drop-down-id-" + l.refId + "-" + f).addClass(l.showme);
                            l.container.find("#drop-down-id-" + l.refId + "-" + f).addClass(l.answerBoxShowMe);
                        }
                    }
                }
            } else {
                h = l.container.find("#drop-down-id-" + l.refId + "-" + f + "-body").length;
                g = h > 0 ? l.container.find("#drop-down-id-" + l.refId + "-" + f + "-body").find("." + l.clickClassName) : l.container.find("#drop-down-id-" + l.refId + "-" + f).find("." + l.clickClassName);
                for (b = 0; b < g.length; b++) {
                    k = g[b];
                    if (Number($(k).val()) === Number(l.correctArray[f])) {
                        l.currentObjectArray[f].prop("checked", true);
                        if (l.showme !== null && l.showme !== undefined) {
                            l.container.find("#drop-down-id-" + l.refId + "-" + f).addClass(l.showme);
                            l.container.find("#drop-down-id-" + l.refId + "-" + f).addClass(l.answerBoxShowMe);
                        }
                        l.container.find("#drop-down-id-" + l.refId + "-" + f).find("span.selectedValue").empty().append($(k).find("div").html());
                    }
                }
            }
            if (typeof l.container.find("#drop-down-id-" + l.refId + "-" + f).attr("answer") !== "undefined" && l.container.find("#drop-down-id-" + l.refId + "-" + f).attr("answer") !== "true") {
                a(f);
            }
        }
    }
    if (c !== null && c !== undefined) {
        c(this.selectedArray);
    } else {
        return this.selectedArray;
    }
};
Athena.MultipleChoiceQuestion.prototype.validation = function(o, h, e, c) {
    var s = null,
        l, g, p, b, d, f, a, r, k, q, m;
    r = this;
    this.storage._trylimit = this.tryCount;
    this.storeUserSelectedItem();
    if (h !== null && h !== undefined && Number(h.length) !== 0) {
        r.correctArray = h;
    }
    if (e !== null && e !== undefined && Number(e.length) !== 0) {
        r.selectedArray = e;
    }
    if (o !== null && o !== undefined && Number(o.length) !== 0) {
        r.currentObjectArray = o;
    }
    if (Number(r.selectedArray.length) === 0 || r.selectedArray === null || r.selectedArray === undefined) {
        s = "notselected";
        if (c !== null && c !== undefined) {
            c(s);
        } else {
            return s;
        }
    } else {
        if (String(r.selectionType).toLowerCase() === "multiple") {
            for (l = 0; l < r.currentObjectArray.length; l++) {
                r.currentObjectArray[l].prop("checked", false);
                d = r.selectedArray[l];
                f = false;
                if (String(r.choiceContentType) !== "dropdown") {
                    for (g = 0; g < r.correctArray.length; g++) {
                        if (Number(r.selectedArray[l]) === Number(r.correctArray[g])) {
                            f = true;
                            break;
                        }
                    }
                } else {
                    if (Number(r.selectedArray[l]) === Number(r.correctArray[l])) {
                        f = true;
                    }
                }
                a = 0;
                for (p = 0; p < r.selectedArray.length; p++) {
                    if (r.selectedArray[p] === undefined || r.selectedArray[p] === null) {
                        a++;
                    }
                }
                a = r.selectedArray.length - a;
                if (Number(r.correctArray.length) !== Number(a)) {
                    s = "fail";
                }
                if (d !== null && d !== undefined) {
                    if (String(r.choiceContentType) !== "dropdown") {
                        r.setInitialBackground(d, null);
                    } else {
                        r.setInitialBackground(l, null);
                    }
                    if (String(r.choiceContentType).toLowerCase() === "dropdown") {
                        if (String(r.currentObjectArray[l].get(0).tagName).toUpperCase() === "SELECT") {
                            k = null;
                            k = r.currentObjectArray[l].find("." + r.clickClassName);
                            for (b = 0; b < k.length; b++) {
                                q = k[b];
                                if (Number($(q).val()) === Number(r.selectedArray[l])) {
                                    r.currentObjectArray[l].val($(q).val());
                                }
                            }
                        } else {
                            k = null;
                            m = r.container.find("#drop-down-id-" + r.refId + "-" + l + "-body").length;
                            k = m > 0 ? r.container.find("#drop-down-id-" + r.refId + "-" + l + "-body").find("." + r.clickClassName) : r.container.find("#drop-down-id-" + r.refId + "-" + l).find("." + r.clickClassName);
                            for (b = 0; b < k.length; b++) {
                                q = k[b];
                                if (Number($(q).val()) === Number(r.selectedArray[l])) {
                                    r.container.find("#drop-down-id-" + r.refId + "-" + l).find("span.selectedValue").empty();
                                    r.container.find("#drop-down-id-" + r.refId + "-" + l).find("span.selectedValue").append($(q).find("div").html());
                                }
                            }
                        }
                    }
                    if (f) {
                        if (String(r.choiceContentType) !== "dropdown") {
                            if (r.correct !== null && r.correct !== undefined) {
                                r.currentObjectArray[d].addClass(r.correct);
                                r.cloneElementArray[d].addClass(r.answerBoxCorrect);
                            }
                            r.currentObjectArray[d].prop("checked", true);
                        } else {
                            if (r.correct !== null && r.correct !== undefined) {
                                r.container.find("#drop-down-id-" + r.refId + "-" + l).attr("answer", "true");
                                r.container.find("#drop-down-id-" + r.refId + "-" + l).addClass(r.correct);
                                r.container.find("#drop-down-id-" + r.refId + "-" + l).addClass(r.answerBoxCorrect);
                            }
                        }
                    } else {
                        s = "fail";
                        if (String(r.choiceContentType) !== "dropdown") {
                            if (r.wrong !== null && typeof r.wrong !== undefined) {
                                r.currentObjectArray[d].addClass(r.wrong);
                                r.cloneElementArray[d].addClass(r.answerBoxWrong);
                            }
                            r.currentObjectArray[d].prop("checked", true);
                        } else {
                            if (r.wrong !== null && r.wrong !== undefined) {
                                r.container.find("#drop-down-id-" + r.refId + "-" + l).addClass(r.wrong);
                                r.container.find("#drop-down-id-" + r.refId + "-" + l).addClass(r.answerBoxWrong);
                            }
                        }
                    }
                } else {
                    r.setInitialBackground(l, null);
                    if (String(r.choiceContentType) !== "dropdown") {
                        if (r.unselect !== null && r.unselect !== undefined) {
                            r.currentObjectArray[l].addClass(r.unselect);
                            r.cloneElementArray[l].addClass(r.answerBoxUnSelect);
                        }
                    } else {
                        k = null;
                        m = r.container.find("#drop-down-id-" + r.refId + "-" + l + "-body").length;
                        k = m > 0 ? r.container.find("#drop-down-id-" + r.refId + "-" + l + "-body").find("." + r.clickClassName) : r.container.find("#drop-down-id-" + r.refId + "-" + l).find("." + r.clickClassName);
                        for (b = 0; b < k.length; b++) {
                            q = k[b];
                            if (Number($(q).val()) === Number(0)) {
                                r.container.find("#drop-down-id-" + r.refId + "-" + l).addClass("DisableMode");
                                r.container.find("#drop-down-id-" + r.refId + "-" + l).find("span.selectedValue").html($(q).find("div").html());
                            }
                        }
                    }
                }
            }
        } else {
            if (String(r.selectionType).toLowerCase() === "single") {
                if (Number(r.selectedArray.length) !== 0 && r.selectedArray.length !== undefined && Number(r.correctArray.length) !== 0 && r.correctArray.length !== undefined) {
                    if (Number(r.selectedArray[0]) === Number(r.correctArray[0])) {
                        r.setInitialBackground(r.selectedArray[0], null);
                        if (r.correct !== null && r.correct !== undefined) {
                            r.currentObjectArray[r.selectedArray[0]].addClass(r.correct);
                            r.cloneElementArray[r.selectedArray[0]].addClass(r.answerBoxCorrect);
                        }
                        r.currentObjectArray[r.selectedArray[0]].prop("checked", true);
                    } else {
                        s = "fail";
                        r.setInitialBackground(r.selectedArray[0], null);
                        if (r.wrong !== null && r.wrong !== undefined) {
                            r.currentObjectArray[r.selectedArray[0]].addClass(r.wrong);
                            r.cloneElementArray[r.selectedArray[0]].addClass(r.answerBoxWrong);
                        }
                        r.currentObjectArray[r.selectedArray[0]].prop("checked", true);
                    }
                }
            }
        }
        r.removeEvents(r.currentObjectArray);
        if (s === null || s === undefined) {
            s = "pass";
        }
        if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
            this.pluginEvent(null, s, "started", "instructionq", this.data.objectId);
        }
        if (c !== null && c !== undefined) {
            c(s);
        } else {
            return s;
        }
    }
};
Athena.MultipleChoiceQuestion.prototype.tryAgainReset = function(h) {
    var f, d, c, l = this,
        g, e, a, k, b;
    if (h !== null && h !== undefined && Number(h.length) !== 0) {
        this.currentObjectArray = h;
    }
    if (this.currentObjectArray !== undefined && this.currentObjectArray !== null) {
        this.setInitialBackground(null, null);
    }
    if (String(l.choiceContentType).toLowerCase() === "dropdown") {
        b = [];
        for (f = 0; f < l.currentObjectArray.length; f++) {
            e = false;
            a = null;
            g = null;
            g = l.container.find("#drop-down-id-" + l.refId + "-" + f).find("." + l.clickClassName);
            for (c = 0; c < g.length; c++) {
                k = g[c];
                if (Number($(k).val()) === 0) {
                    a = $(k);
                }
                if (String(l.resetAll) === "false") {
                    if ((Number($(k).val()) === Number(l.selectedArray[f])) && (Number(l.correctArray[f]) === Number(l.selectedArray[f]))) {
                        if (Number(b.length) === 0) {
                            b[0] = l.container.find("#drop-down-id-" + l.refId + "-" + f);
                        } else {
                            b[b.length + 1] = l.container.find("#drop-down-id-" + l.refId + "-" + f);
                        }
                        e = true;
                        break;
                    }
                }
            }
            if (!e) {
                l.selectedArray[f] = undefined;
                if (String(l.container.find("#drop-down-id-" + l.refId + "-" + f).get(0).tagName).toUpperCase() === "SELECT") {
                    l.container.find("#drop-down-id-" + l.refId + "-" + f).val(0);
                } else {
                    l.container.find("#drop-down-id-" + l.refId + "-" + f).find("span.selectedValue").empty().append(a.find("div").html());
                }
            } else {
                if (l.unselect !== null && l.unselect !== undefined) {
                    l.container.find("#drop-down-id-" + l.refId + "-" + f).removeClass(l.unselect);
                    l.container.find("#drop-down-id-" + l.refId + "-" + f).removeClass(l.answerBoxUnSelect);
                }
                if (l.correct !== null && l.correct !== undefined) {
                    l.container.find("#drop-down-id-" + l.refId + "-" + f).addClass(l.correct);
                    l.container.find("#drop-down-id-" + l.refId + "-" + f).addClass(l.answerBoxCorrect);
                }
            }
        }
    } else {
        if (String(l.resetAll) === "false") {
            for (f = 0; f < l.currentObjectArray.length; f++) {
                e = false;
                for (d = 0; d < l.correctArray.length; d++) {
                    if (Number(l.selectedArray[f]) === Number(l.correctArray[d])) {
                        e = true;
                        break;
                    }
                }
                if (e) {
                    if (l.unselect !== null && l.unselect !== undefined) {
                        l.currentObjectArray[f].removeClass(l.unselect);
                        l.cloneElementArray[f].removeClass(l.unselect);
                    }
                    if (l.correct !== null && l.correct !== undefined) {
                        l.currentObjectArray[f].addClass(l.correct);
                        l.cloneElementArray[f].addClass(l.answerBoxCorrect);
                    }
                } else {
                    l.selectedArray[f] = undefined;
                }
            }
        }
    }
    if (String(this.resetAll) === "true") {
        this.selectedArray = [];
        for (f = 0; f < l.currentObjectArray.length; f++) {
            l.currentObjectArray[f].prop("checked", false);
        }
    }
};
Athena.MultipleChoiceQuestion.prototype.setInitialBackground = function(d, c) {
    var b, a;
    a = this;
    if (c !== null && c !== undefined && Number(c.length) !== 0) {
        a.currentObjectArray = c;
    }
    if (d !== null && d !== undefined) {
        if (this.select !== null && this.select !== undefined) {
            a.currentObjectArray[d].removeClass(a.select);
            a.cloneElementArray[d].removeClass(a.answerBoxSelect);
            if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                a.container.find("#drop-down-id-" + a.refId + "-" + d).removeClass(a.select);
            }
        }
        if (this.correct !== null && this.correct !== undefined) {
            a.currentObjectArray[d].removeClass(a.correct);
            a.cloneElementArray[d].removeClass(a.answerBoxCorrect);
            if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                a.container.find("#drop-down-id-" + a.refId + "-" + d).removeClass(a.correct);
            }
        }
        if (this.wrong !== null && this.wrong !== undefined) {
            a.currentObjectArray[d].removeClass(a.wrong);
            a.cloneElementArray[d].removeClass(a.answerBoxWrong);
            if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                a.container.find("#drop-down-id-" + a.refId + "-" + d).removeClass(a.wrong);
            }
        }
        if (this.showme !== null && this.showme !== undefined) {
            a.currentObjectArray[d].removeClass(a.showme);
            a.cloneElementArray[d].removeClass(a.answerBoxShowMe);
            if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                a.container.find("#drop-down-id-" + a.refId + "-" + d).removeClass(a.showme);
            }
        }
        if (this.unselect !== null && this.unselect !== undefined) {
            a.currentObjectArray[d].addClass(a.unselect);
            a.cloneElementArray[d].removeClass(a.answerBoxUnSelect);
            if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                a.container.find("#drop-down-id-" + a.refId + "-" + d).removeClass(a.unselect);
            }
        }
        if (this.correctTryCount !== null && this.correctTryCount !== undefined) {
            a.container.find("#drop-down-id-" + a.refId + "-" + d).removeClass(a.correctTryCount);
        }
        if (this.selected !== null && this.selected !== undefined) {
            a.currentObjectArray[d].removeClass(a.selected);
            if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                a.container.find("#drop-down-id-" + a.refId + "-" + d).removeClass(a.selected);
                a.container.find("#drop-down-id-" + a.refId + "-" + d).addClass(a.unselect);
            }
        }
    } else {
        if (a.currentObjectArray !== undefined && a.currentObjectArray !== null) {
            for (b = 0; b < a.currentObjectArray.length; b++) {
                if (this.select !== null && this.select !== undefined) {
                    a.currentObjectArray[b].removeClass(a.select);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.select);
                    }
                }
                if (this.answerBoxSelect !== null && this.answerBoxSelect !== undefined) {
                    a.cloneElementArray[b].removeClass(a.answerBoxSelect);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.answerBoxSelect);
                    }
                }
                if (this.selected !== null && this.selected !== undefined) {
                    a.currentObjectArray[b].removeClass(a.selected);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.selected);
                    }
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.selected);
                    }
                }
                if (this.correct !== null && this.correct !== undefined) {
                    a.currentObjectArray[b].removeClass(a.correct);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.correct);
                    }
                }
                if (this.answerBoxCorrect !== null && this.answerBoxCorrect !== undefined) {
                    a.cloneElementArray[b].removeClass(a.answerBoxCorrect);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.answerBoxCorrect);
                    }
                }
                if (this.wrong !== null && this.wrong !== undefined) {
                    a.currentObjectArray[b].removeClass(a.wrong);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.wrong);
                    }
                }
                if (this.answerBoxWrong !== null && this.answerBoxWrong !== undefined) {
                    a.cloneElementArray[b].removeClass(a.answerBoxWrong);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.answerBoxWrong);
                    }
                }
                if (this.showme !== null && this.showme !== undefined) {
                    a.currentObjectArray[b].removeClass(a.showme);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.showme);
                    }
                }
                if (this.answerBoxShowMe !== null && this.answerBoxShowMe !== undefined) {
                    a.cloneElementArray[b].removeClass(a.answerBoxShowMe);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.answerBoxShowMe);
                    }
                }
                if (this.unselect !== null && this.unselect !== undefined) {
                    a.currentObjectArray[b].addClass(a.unselect);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).addClass(a.unselect);
                    }
                }
                if (this.answerBoxUnSelect !== null && this.answerBoxUnSelect !== undefined) {
                    a.cloneElementArray[b].removeClass(a.answerBoxUnSelect);
                    if (String(a.choiceContentType).toLowerCase() === "dropdown") {
                        a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.answerBoxUnSelect);
                    }
                }
                if (this.correctTryCount !== null && this.correctTryCount !== undefined) {
                    a.container.find("#drop-down-id-" + a.refId + "-" + b).removeClass(a.correctTryCount);
                }
            }
        }
    }
};
Athena.MultipleChoiceQuestion.prototype.reset = function() {
    var g, f, c, l = this,
        h, b, k, d, a, e;
    l.container.find("." + l.feedBack).empty();
    l.container.find("." + l.feedBack).parent().removeClass("hasText");
    l.hint.parent().hide();
    l.tryCount = Number(this.data.contents.total_attempts.value);
    l.hint.find("span").empty();
    l.tryAgainButton.hide();
    l.showMeButton.hide();
    l.myAnswerButton.hide();
    l.checkButton.show();
    l.checkButton.unbind("click");
    l.checkButton.removeClass(l.activeClassName);
    l.container.find("." + l.feedBack).removeClass("hasText").empty();
    l.container.find("." + l.wrongTextClassName).hide();
    if (l.currentObjectArray !== undefined && l.currentObjectArray !== null) {
        l.setInitialBackground(null, null);
    }
    l.selectedArray = [];
    if (String(l.choiceContentType).toLowerCase() === "dropdown") {
        d = [];
        l.container.find("." + l.dropDownClassName).each(function() {
            e = $(this).attr("id");
            a = $("#" + e + "-body").find("li:eq(0) div").html();
            if (!(new RegExp("<math", "gi").test(a))) {
                $(this).find("ul li:eq(0) span.selectedValue").css("font-size", "20px");
            }
            if (typeof a === "undefined") {
                a = $(this).find("ul li ul li:eq(0) div").html();
            }
            $(this).find("ul li:eq(0) span.selectedValue").html(a);
            $(this).removeClass("DisableMode");
        });
        l.setDropDownEvents(l.currentObjectArray, l.optionsClass, l.replaceText, l.clickClassName, null);
    } else {
        for (g = 0; g < l.currentObjectArray.length; g++) {
            l.currentObjectArray[g].prop("checked", false);
        }
        l.setEvents(l.currentObjectArray, null);
    }
    for (g = 0; g < l.currentObjectArray.length; g++) {
        l.currentObjectArray[g].css("cursor", "pointer");
    }
    if (String(l.choiceContentType).toLowerCase() === "dropdown") {
        l.container.find(".dropdown-correct").addClass(l.correctTryCount);
    }
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        this.pluginEvent(null, null, null, null, {
            id: this.data.objectId,
            value: undefined
        });
    }
};
Athena.MultipleChoiceQuestion.prototype.storeUserSelectedItem = function() {
    this.storage.userChoice = JSON.stringify(this.selectedArray);
    if (this.persistOnSelect) {
        this.persistData();
    }
};
Athena.MultipleChoiceQuestion.prototype.persistData = function() {
    var a = this;
    this.storage.tryCount = this.tryCount;
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        a.pluginEvent(null, null, null, null, {
            id: a.data.objectId,
            value: a.storage
        });
    }
};
Athena.MultipleChoiceQuestion.prototype.preset = function(d) {
    var c, b, a = this;
    if (d !== undefined && d.value !== undefined) {
        this.storage = d.value;
    }
    if (String(this.storage._tryClicked) === "true") {
        delete this.storage._checkClicked;
        if (String(this.choiceContentType).toLowerCase() === "dropdown") {
            this.tryCount = 1;
            b = JSON.parse(this.storage.userChoice);
            this.selectedArray = b;
            this.setCheckEvent();
            this.checkButton.trigger("click");
            this.setTryOption();
            this.tryAgainButton.trigger("click");
            this.tryAgainReset(null, null);
        } else {
            this.tryCount = 0;
        }
    } else {
        if (String(this.storage._checkClicked) === "true") {
            setTimeout(function() {
                if (a.storage._trylimit !== null) {
                    a.tryCount = Number(a.storage._trylimit);
                } else {
                    a.tryCount = 1;
                }
                b = [];
                b = JSON.parse(a.storage.userChoice);
                if (a.currentObjectArray !== null) {
                    for (c = 0; c < b.length; c++) {
                        if (String(b[c]) !== "null") {
                            if (String(a.choiceContentType).toLowerCase() !== "dropdown") {
                                a.currentObjectArray[b[c]].parent().trigger("click");
                            } else {
                                a.currentObjectArray[c].find("li").eq(Number(b[c]) + 1).trigger("click");
                                $("#" + a.currentObjectArray[c].find("li").eq(Number(b[c]) + 1).parents(".selectBox").attr("id") + "-body").hide();
                            }
                        }
                    }
                }
                a.selectedArray = b;
                a.setCheckEvent();
                a.checkButton.trigger("click");
                if (String(a.storage._showClicked) === "true") {
                    a.myanswerFlag = false;
                    a.showMeButton.trigger("click");
                }
                if (String(a.storage._myanswerClicked) === "true") {
                    a.myAnswerButton.trigger("click");
                }
            }, 100);
        } else {
            b = JSON.parse(this.storage.userChoice);
            this.selectedArray = b;
            this.setCheckEvent();
            if (a.currentObjectArray !== null) {
                for (c = 0; c < b.length; c++) {
                    if (String(b[c]) !== "null") {
                        if (String(a.choiceContentType).toLowerCase() !== "dropdown") {
                            a.currentObjectArray[b[c]].parent().trigger("click");
                        } else {
                            a.currentObjectArray[c].find("li").eq(Number(b[c]) + 1).trigger("click");
                            $("#" + a.currentObjectArray[c].find("li").eq(Number(b[c]) + 1).parents(".selectBox").attr("id") + "-body").hide();
                        }
                    }
                }
            }
        }
    }
};
Athena.DropDownCreator = function() {
    this.name = "DropDownCreator";
    this.currentObjectArray = [];
    this.optionsClass = null;
    this.clickClassName = null;
    this.dropDownClassName = null;
    this.replaceText = null;
    this.selectedArray = [];
    this.contentArray = [];
    this.labelArray = [];
    this.dropDownWidth = 0;
    this.deviceType = "";
    this.labelContent = [];
};
Athena.DropDownCreator.extend(Athena.MultipleChoiceQuestion);
Athena.DropDownCreator.prototype.setCloneItems = function(b, g, f, d, h, e, a) {
    var c, k, j, l;
    j = this;
    this.deviceType = this.checkDevices();
    if (g.optionsClass !== null && g.optionsClass !== undefined) {
        this.optionsClass = g.optionsClass;
    }
    if (g.clickClassName !== null && g.clickClassName !== undefined) {
        this.clickClassName = g.clickClassName;
    }
    if (g.dropDownClassName !== null && g.dropDownClassName !== undefined) {
        this.dropDownClassName = g.dropDownClassName;
    }
    if (g.replaceText !== null && g.replaceText !== undefined) {
        this.replaceText = g.replaceText;
    }
    this.options(b, g, f, d, h, e, this.renderDropDown);
    $(window).on("orientationchange	resize", function(m) {
        var i = true;
        j.deviceType = j.checkDevices();
        if (String(j.deviceType) === "mobile") {
            if ($(this).innerWidth() < $(this).innerHeight()) {
                j.container.find(".interaction_container").find(".mdq-break").show();
                j.container.find(".interaction_container").find("label").each(function(n) {
                    $(this).css("top", j.labelArray[n] + "px");
                    j.labelContent[n] = $(this).html();
                });
                j.container.find(".interaction_container").find(".selectBox").each(function() {
                    if (typeof $(this).attr("data-addClass") !== "undefined") {
                        $(this).find(".replace").css("width", "230px");
                        $(this).css("width", "230px");
                    }
                });
            } else {
                j.container.find(".interaction_container").find("label").each(function(n) {
                    $(this).css("top", j.labelArray[n] + "px");
                    j.labelContent[n] = $(this).html();
                });
                for (c = 0; c <= j.labelContent.length; c++) {
                    j.container.find(".interaction_container").find("label").eq(c).empty();
                    j.container.find(".interaction_container").find("label").eq(c).append("<span style='padding-right:5px;'>" + j.labelContent[c] + "</span>");
                }
                j.container.find(".interaction_container").find(".mdq-break").hide();
                j.container.find(".interaction_container").find(".selectBox").find(".replace").css("width", j.dropDownWidth + "px");
                j.container.find(".interaction_container").find(".selectBox").css("width", "230px").css("width", j.dropDownWidth + "px");
            }
        }
        j.toggleScrollToTable();
        j.reAdjustHeight();
    });
};
Athena.DropDownCreator.prototype.renderDropDown = function(d) {
    var e, a, b, c, f;
    b = [];
    e = [];
    f = this.data.contents.mdq_seperate_mode;
    f = typeof f === "undefined" ? false : f;
    if (this.data.contents.content_type === "text") {
        if (f || this.checkSeperateFromText()) {
            this.container.find(".interaction_container").addClass("mdq-text");
        }
    }
    this.data.contents.option_text.value = this.addSemantics(this.data.contents.option_text.value, this.data.contents.place_holder.value);
    this.data.contents.option_text.value = this.data.contents.option_text.value.replace(new RegExp("\\" + this.data.contents.place_holder.value, "g"), this.htmlObjects.replaceHTML);
    this.cloneContainer.html(this.data.contents.option_text.value);
    if (this.data.contents.option_text.objectId !== undefined) {
        this.cloneContainer.attr("data-objectid", this.data.contents.option_text.objectId);
    }
    e = this.cloneContainer.find("." + this.dropDownClassName);
    this.cloneElementArray = [];
    for (c = 0; c < e.length; c++) {
        this.cloneElementArray[c] = $(e[c]);
    }
    if (typeof this.data.contents.adj_interaction_width !== "undefined" && this.data.contents.adj_interaction_width.value === "true") {
        this.cloneContainer.css("width", "740px");
    }
    this.create(this.data, this.cloneElementArray, this.dropDownClassName, this.htmlObjects.questionObject, this.clickClassName, this.htmlObjects.choiceIdPrefix, null);
    this.setStyleClassNames(this.htmlObjects.styleClassNames);
};
Athena.DropDownCreator.prototype.checkSeperateFromText = function() {
    var a = this.data.contents.place_holder;
    if (this.data.contents.option_text.value.trim() === a || this.data.contents.option_text.value.trim() === "<label>" + a + "</label>") {
        return true;
    }
    return false;
};
Athena.DropDownCreator.prototype.create = function(w, g, o, d, x, r, k) {
    var l, f, e, v, b, t, s, p, q, u, a, h, c;
    l = this;
    this.choiceContentType = w.contents.content_choice_type.value;
    f = w.contents.question.value;
    e = [];
    for (t = 1; t <= Object.keys(w.lists).length; t++) {
        e[t - 1] = w.lists["d_option_" + t];
    }
    this.contentArray = e;
    v = w.contents.content_type.value;
    b = g.length;
    l.selectionType = w.contents.choice_type.value;
    if (f !== null && f !== undefined) {
        if (d !== null && d !== undefined) {
            d = this.container.find("." + d);
            d.html(f);
            if (w.contents.question.objectId !== undefined) {
                d.attr("data-objectid", w.contents.question.objectId);
            }
        }
    }
    if (b !== undefined && b !== null) {
        u = null;
        for (t = 0; t < b; t++) {
            u = g[t];
            if (Number(e.length) !== 0) {
                for (s = 0; s < e[t].length; s++) {
                    if (Number(s) === 0) {
                        a = u.find("." + x);
                    } else {
                        a = a.clone().insertAfter(a);
                    }
                    if (Number(s) === 0) {
                        h = u.get(0).tagName;
                        if (String(h) !== "SELECT") {
                            u.find("span").empty().append(e[t][s].option.value);
                        }
                    }
                    a.attr("value", s);
                    if (e[t][s].option.objectId !== undefined) {
                        a.attr("data-objectid", e[t][s].option.objectId);
                    }
                    if (Number(e[t].length) !== 0 && String(v) === "text") {
                        if (!(new RegExp("<math|Select an answer", "gi").test(e[t][s].option.value))) {
                            $(u).attr("data-addClass", "textOnlyDropdown");
                        }
                        a.find("div").empty().append(e[t][s].option.value);
                    } else {
                        if (Number(e[t].length) !== 0 && String(v) === "image") {
                            a.find("div").css("background", 'url("' + this.imagePath + e[t][s].option.value.trim() + '")');
                        }
                    }
                }
            }
            u.attr("id", "drop-down-id-" + this.refId + "-" + t);
            c = u.find("." + l.clickClassName);
            for (p = 0; p < c.length; p++) {
                if (!(new RegExp("<math", "gi").test($(c[p]).find("div").html()))) {
                    $(c[p]).find("div").css("font-size", "20px");
                } else {
                    $(c[p]).find("div").css("font-size", "17px");
                }
            }
            l.currentObjectArray[t] = u;
            u.find("." + l.optionsClass).css("visibility", "visible");
        }
        if (w !== null && w !== undefined) {
            if (this.checkButton !== null && this.checkButton !== undefined) {
                this.checkButton.find("span").empty().html(this.buttonCheckText);
            }
            if (this.tryAgainButton !== null && this.tryAgainButton !== undefined) {
                this.tryAgainButton.find("span").empty().html(this.buttonTryAgainText);
            }
            if (this.showMeButton !== null && this.showMeButton !== undefined) {
                this.showMeButton.find("span").empty().html(this.buttonShowMeText);
            }
            if (this.myAnswerButton !== null && this.myAnswerButton !== undefined) {
                this.myAnswerButton.find("span").empty().html(this.buttonMyAnswerText);
            }
            l.hint.find("span").html(l.hintMsg);
        }
        l.setDropDownEvents(l.currentObjectArray, l.optionsClass, l.replaceText, l.clickClassName, null);
        l.setTryOption();
        l.setShowMeOption();
        l.setMyAnswersOption();
        if (k !== null && k !== undefined) {
            k(l.currentObjectArray);
        } else {
            return l.currentObjectArray;
        }
    }
};
Athena.DropDownCreator.prototype.setStyles = function(a, c, b, d, e) {
    return this.setStyleClassNames(a, c, b, d, e);
};
Athena.DropDownCreator.prototype.setDropDownEvents = function(a, g, f, c, e) {
    var d, j, h, b, k;
    d = a.length;
    j = this;
    for (h = 0; h < d; h++) {
        j.setDropDownClick(a[h], g, f, c, e);
    }
    if (navigator.userAgent.indexOf("iPad") !== -1 || navigator.userAgent.indexOf("Android") !== -1) {
        k = "touchstart";
    } else {
        k = "click";
    }
    $(document).unbind(k);
    $(document).bind(k, function(i) {
        j.hideOtherDropdowns(i);
    });
};
Athena.DropDownCreator.prototype.displayUserInput = function(a) {};
Athena.DropDownCreator.prototype.hideOtherDropdowns = function(a) {
    var b;
    b = a.srcElement || a.target;
    if (!this.findMouseClickedElementWithinRange(b, ["selectBox", "interactivity-mdq-dd-body", "mrow", "mn", "mi", "mo", "mfrac", "math", "MathJax"], null, 5)) {
        this.hideAllOpendDropDown();
    }
};
Athena.DropDownCreator.prototype.findMouseClickedElementWithinRange = function(a, e, c, g) {
    var l, f = 1,
        b, j = false,
        d, k, m, h;
    b = typeof g === undefined || g === null ? -1 : g;
    l = a;
    e = e instanceof Array ? e.join("|") : e;
    if (parent === null) {
        return false;
    }
    do {
        d = l.id;
        k = l.className;
        m = new RegExp(e, "g");
        h = new RegExp(c, "g");
        if (e && m.test(k)) {
            j = true;
            return j;
        }
        if (c && h.test(d)) {
            j = true;
            return j;
        }
        if (b > 0 && b === f) {
            return j;
        }
        f++;
        l = l.parentNode;
    } while (l);
    return false;
};
Athena.DropDownCreator.prototype.hideAllOpendDropDown = function(f) {
    
    var c, g, b = this,
        d, a;
    if ($(document).find(".opended") !== null && $(document).find(".opended") !== undefined) {
        if ($(document).find(".opended").attr("class") !== undefined) {
        	Collab.closeOpenDropdowns();
            $(document).find(".opended").attr("opended", "false");
            d = $(".selectBox.opended").attr("id") + "-body";
            a = $(".selectBox.opended").closest(".drop-down-container");
            $(document).find(".opended").find("." + this.optionsClass).hide();
            $(document).find(".opended").find("." + this.optionsClass).css("visibility", "visible");
            $(a).find("#" + d).hide();
            $(document).find(".opended").removeClass(this.select);
            $(document).find(".opended").addClass(this.unselect);
            $(document).find(".opended").removeClass("opended");
            $(document).find(".opended").removeClass(this.unselect);
            $(document).find(".opended").addClass(this.select);
            for (c = 0; c < this.currentObjectArray.length; c++) {
                if (Number(this.currentObjectArray[c].val()) !== 0) {
                    this.currentObjectArray[c].addClass(this.selected);
                }
            }
        }
    }
};
Athena.DropDownCreator.prototype.getElementPosition = function(d, c) {
    var a = 0,
        b = 0;
    while (d.offsetParent) {
        if (new RegExp(c, "gi").test(d.className)) {
            break;
        }
        a += d.offsetLeft;
        b += d.offsetTop;
        d = d.offsetParent;
    }
    return {
        left: a,
        top: b
    };
};
Athena.DropDownCreator.prototype.setDropDownClick = function(f, c, e, r, h) {
    var j, l, p, i, a, k, m, q, o, g, b, n, d;
    i = this;
    g = f.get(0).tagName;
    if (String(g) === "SELECT") {
        this.container.delegate(f, "click", function(s) {
            f.removeClass(i.unselect);
            f.addClass(i.select);
        });
        this.container.delegate(f, "change", function(s) {
            f.unbind("click");
            f.removeClass(i.select);
            f.addClass(i.unselect);
            l = $(this).val();
            q = f.attr("id").split("-");
            j = q[q.length - 1];
            i.selectedArray[j] = l;
            if (!s) {
                s = window.event;
            }
            s.cancelBubble = true;
            if (s.stopPropagation) {
                s.stopPropagation();
            }
            i.changeCheckButtonStatus(null, null, null, i.selectedArray);
            f.bind("click", function(t) {
                f.removeClass(i.unselect);
                f.addClass(i.select);
            });
            if (h !== null && h !== undefined) {
                h(j, l, i.selectedArray, i);
            }
        });
    } else {
        n = "#" + f.attr("id") + "." + this.dropDownClassName;
        this.container.undelegate(n, "click");
        f.removeClass(i.selected);
        this.container.delegate(n, "click", function(s) {
            if (!$(this).hasClass("DisableMode") && !$(this).hasClass(i.wrong) && !$(this).hasClass(i.correct) && !$(this).hasClass(i.showme)) {
                b = $(this).attr("opended");
                i.hideAllOpendDropDown();
                if (String(b) === "true" && b !== undefined && b !== null) {
                    $(this).attr("opended", "false");
                    Collab.dropdownClose();
                } else {
                	Collab.dropdownOpen(n);
                    o = $(this).addClass("opended");
                    if (i.container.find("#" + f.attr("id") + "-body").length <= 0) {
                        var t = document.createElement("div");
                        $(t).addClass("interactivity-mdq-dd-body");
                        $(t).attr("id", f.attr("id") + "-body");
                        $(t).attr("data-ref-dd", n);
                        if (typeof $(this).attr("data-addClass") !== "undefined") {
                            $(t).addClass($(this).attr("data-addClass"));
                        }
                        d = i.getElementPosition(this, "interaction_container");
                        $(t).css({
                            position: "absolute",
                            left: d.left - parseInt($(this).css("margin-left"), 10),
                            top: d.top + $(this).height() - 10
                        });
                        $(t).append($(this).find("." + c).wrap("<div>").parent().html());
                        $(this).find("." + c).unwrap();
                        $(t).find("." + c).hide();
                        $(t).find("." + c).show();
                        $(t).find("." + c).css("visibility", "visible");
                        if (i.deviceType === "mobile") {
                            p = i.container.find(".interaction_container #" + f.attr("id")).width();
                            $(t).find("ul").css("width", (Number(p) + Number(45)) + "px");
                        }
                        i.container.find(".interaction_container").append(t);
                        if (i.deviceType === "mobile") {
                            i.container.find(".interaction_container #" + f.attr("id") + "-body").css("margin-left", "0px");
                        }
                    } else {
                        d = i.getElementPosition(this, "interaction_container");
                        k = d.left - parseInt($(this).css("margin-left"), 10);
                        a = d.top + $(this).height() - 10;
                        if (String(i.deviceType) === "mobile") {
                            p = i.container.find(".interaction_container #" + f.attr("id")).width();
                            i.container.find(".interaction_container #" + f.attr("id") + "-body").find("ul").css("width", (Number(p) + Number(45)) + "px");
                            i.container.find(".interaction_container #" + f.attr("id") + "-body").css({
                                position: "absolute",
                                left: k,
                                top: a
                            });
                            i.container.find(".interaction_container #" + f.attr("id") + "-body").show();
                        } else {
                            i.container.find(".interaction_container #" + f.attr("id") + "-body").css({
                                position: "absolute",
                                left: k,
                                top: a
                            });
                            i.container.find(".interaction_container #" + f.attr("id") + "-body").show();
                        }
                    }
                    $(this).removeClass(i.unselect);
                    $(this).addClass(i.select);
                    $(this).attr("opended", "true");
                    $(this).removeClass(i.selected);
                }
                s.stopPropagation();
            }
        });
        i.onSelected(f, c, e, r, h);
    }
};
Athena.DropDownCreator.prototype.onSelected = function(j, i, f, c, e) {
    var k, l, d, b, a, g, h;
    l = this;
    g = ".interactivity-mdq-dd-body ." + c;
    this.container.undelegate(g, "click");
    j.removeClass(l.selected);
    this.container.delegate(g, "click", function(m) {
        h = $(this).closest(".interactivity-mdq-dd-body").attr("id");
        a = $("#" + h).attr("data-ref-dd");
        l.container.find("#" + h).hide();
        j = l.container.find(a);
        j.removeClass("opended");
        j.attr("opended", "false");
        j.find("." + i).hide();
        j.find("." + i).css("visibility", "visible");
        j.removeClass(l.select + " " + l.selected);
        if (!(new RegExp("<math", "gi").test($(this).find("div").html()))) {
            j.find("span").css("font-size", "20px");
        } else {
            if (String(l.data.contents.reduce_font) === "true") {
                if (l.data.contents.target_font_size !== undefined && l.data.contents.target_font_size !== null && String(l.data.contents.target_font_size) !== "") {
                    if ((new RegExp("<math", "gi").test($(this).find("div").html()))) {
                        j.find("span").css("font-size", l.data.contents.target_font_size + "px");
                    }
                }
            } else {
                j.find("span").css("font-size", "16px");
            }
        }
        j.find("span").empty().append($(this).find("div").html());
        if (j.find("span").html() === "Select an answer") {
            j.addClass(l.unselect);
        } else {
            j.addClass(l.selected);
        }
        k = $(this).val();
        
        Collab.dropDownItemSelected(a, k)
        
        d = j.attr("id").split("-");
        b = d[d.length - 1];
        l.selectedArray[b] = k;
        if (!m) {
            m = window.event;
        }
        m.cancelBubble = true;
        if (m.stopPropagation) {
            m.stopPropagation();
        }
        l.changeCheckButtonStatus(null, null, null, null);
        if (e !== null && e !== undefined) {
            e(b, k, l.selectedArray, l);
        }
    });
};
Athena.DropDownCreator.prototype.reAdjustHeight = function() {
    var j, e = [],
        b, c, a, d = [],
        f, h = this,
        g;
    j = this.cloneContainer.find("." + this.dropDownClassName);
    this.cloneElementArray = [];
    for (c = 0; c < j.length; c++) {
        this.cloneElementArray[c] = $(j[c]);
    }
    for (c = 0; c < this.cloneElementArray.length; c++) {
        f = this.cloneElementArray[c];
        if (typeof f.attr("data-addClass") !== "undefined") {
            f.find(".replace").find("ul").css("width", "230px").show();
        } else {
            f.find(".replace").find("ul").show();
        }
        d = f.find("." + h.clickClassName);
        for (a = 0; a < d.length; a++) {
            e[a] = Number($(d[a]).height());
        }
        b = Math.max.apply(Math, e);
        for (a = 0; a < d.length; a++) {
            $(d[a]).find("div").css("height", b + "px");
        }
        f.find(".replace").parent().css("height", (Number(b) + 20) + "px");
        f.find(".replace").css("height", (Number(b) + 20) + "px");
        f.find(".replace").find("ul").hide();
    }
};
Athena.DropDownCreator.prototype.reCalculateWidth = function() {
    var s, q, p, j, d, k, o, l, c, h, a, f, r, g, b, e;
    r = this;
    r.deviceType = r.checkDevices();
    a = [];
    k = [];
    l = [];
    h = [];
    s = this.cloneContainer.find("." + this.dropDownClassName);
    this.cloneElementArray = [];
    for (g = 0; g < s.length; g++) {
        this.cloneElementArray[g] = $(s[g]);
    }
    for (g = 0; g < r.cloneElementArray.length; g++) {
        q = r.cloneElementArray[g];
        h = q.find("." + r.clickClassName);
        for (b = 0; b < h.length; b++) {
            if (String(r.data.contents.reduce_font) === "true") {
                if (r.data.contents.target_font_size !== undefined && r.data.contents.target_font_size !== null && String(r.data.contents.target_font_size) !== "") {
                    if ((new RegExp("<math", "gi").test($(h[b]).find("div").html()))) {
                        $(h[b]).find("div").css("font-size", r.data.contents.target_font_size + "px");
                    }
                }
            }
        }
        for (b = 0; b < h.length; b++) {
            k[b] = $(h[b]).width();
        }
        o = Math.max.apply(Math, k);
        o = o + 1;
        q.attr("data-maxwidth", o);
        this.dropDownWidth = (Number(o) + Number(q.find("." + r.replaceText).css("padding-left").split("px")[0]) + Number(q.find("." + r.replaceText).css("padding-right").split("px")[0]));
        if (this.deviceType === "mobile") {
            if ($(window).innerWidth() < $(window).innerHeight()) {
                if (typeof q.find("." + r.replaceText).parents(".selectBox").attr("data-addClass") !== "undefined") {
                    q.find("." + r.replaceText).parent().css("width", "230px");
                    q.find("." + r.replaceText).parent().parent().parent().css("margin-left", "0px");
                } else {
                    q.find("." + r.replaceText).parent().css("width", (Number(o) + Number(q.find("." + r.replaceText).css("padding-left").split("px")[0]) + Number(q.find("." + r.replaceText).css("padding-right").split("px")[0])) + "px");
                }
            } else {
                q.find("." + r.replaceText).parent().css("width", (Number(o) + Number(q.find("." + r.replaceText).css("padding-left").split("px")[0]) + Number(q.find("." + r.replaceText).css("padding-right").split("px")[0])) + "px");
            }
        } else {
            q.find("." + r.replaceText).parent().css("width", (Number(o) + Number(q.find("." + r.replaceText).css("padding-left").split("px")[0]) + Number(q.find("." + r.replaceText).css("padding-right").split("px")[0])) + "px");
        }
        q.find("." + this.optionsClass).css("width", (Number(q.width()) + Number(q.css("padding-right").split("px")[0]) - 2) + "px");
        q.find("." + this.optionsClass).find("." + this.clickClassName).eq(0).hide();
        for (b = 0; b < h.length; b++) {
            l[b] = Number($(h[b]).height());
        }
        c = Math.max.apply(Math, l);
        for (b = 0; b < h.length; b++) {
            $(h[b]).find("div").css("height", c + "px");
        }
        q.find("." + r.replaceText).css("font-size", "20px");
        q.find("." + r.replaceText).parent().css("height", (Number(c) + 20) + "px");
        q.find("." + r.replaceText).css("height", (Number(c) + 20) + "px");
        a = q.parent().find("label");
        f = q.css("margin-bottom").split("px")[0];
        for (e = 0; e < a.length; e++) {
            this.labelArray[e] = ((((Number(c - 3) / 2) + Number(f) - 3) + 10) * -1);
        }
        if (this.deviceType === "mobile") {
            if ($(window).innerWidth() < $(window).innerHeight()) {
                for (e = 0; e < a.length; e++) {
                    $(a[e]).css("top", ((((Number(c - 3) / 2) + Number(f) - 3) + 10) * -1) + "px");
                    $(a[e]).css("top", ((((Number(c - 3) / 2) + Number(f) - 3) + 10) * -1) + "px");
                }
            }
        } else {
            for (e = 0; e < a.length; e++) {
                $(a[e]).css("top", ((((Number(c - 3) / 2) + Number(f) - 3) + 10) * -1) + "px");
            }
        }
        q.find("." + r.optionsClass).css("display", "none");
        this.toggleScrollToTable();
    }
};
Athena.DropDownCreator.prototype.toggleScrollToTable = function() {
    var a, d, e, c, b = this;
    if (this.deviceType === "mobile") {
        d = b.container.find(".interaction_container").find("table");
        a = d.width();
        if (d.length !== 0) {
            if (b.container.find(".interaction_container .dropdown-table-scroll").length <= 0) {
                e = document.createElement("div");
                e.className = "dropdown-table-scroll";
                b.container.find(".interaction_container").append(e);
                c = b.container.find(".interaction_container").find("table").height() + Number(5);
                b.container.find(".interaction_container").find(".dropdown-table-scroll").css({
                    width: "100%",
                    height: c + "px",
                    overflow: "hidden"
                });
                b.container.find(".interaction_container").find("table").detach().appendTo(b.container.find(".interaction_container").find(".dropdown-table-scroll"));
                b.container.find(".interaction_container").find(".dropdown-table-scroll").mCustomScrollbar({
                    horizontalScroll: true,
                    autoHideScrollbar: true,
                    scrollInertia: 100
                });
                b.container.find(".interaction_container").find(".mCSB_container").css("width", b.container.find(".interaction_container").find(".mCSB_container").width() + 30);
            } else {
                b.container.find(".interaction_container").find(".mCSB_container").css("width", a + 30);
                if (b.container.find(".interaction_container").find(".mCSB_container").length > 0) {
                    b.container.find(".interaction_container").find(".dropdown-table-scroll").mCustomScrollbar("update");
                } else {
                    b.container.find(".interaction_container").find(".dropdown-table-scroll").mCustomScrollbar({
                        horizontalScroll: true,
                        autoHideScrollbar: true,
                        scrollInertia: 100
                    });
                }
            }
        }
    } else {
        if (b.container.find(".interaction_container").find(".dropdown-table-scroll").length > 0) {
            b.container.find(".interaction_container").find(".dropdown-table-scroll").mCustomScrollbar("destroy");
        }
    }
};
Athena.DropDownCreator.prototype.disableDropDown = function(a) {
    return this.removeEvents(a);
};
Athena.DropDownCreator.prototype.dropDownvalidation = function(d, a, c, b) {
    this.storeUserSelectedItem();
    return this.validation(d, a, c, b);
};
Athena.DropDownCreator.prototype.resetDropdown = function(a) {
    return this.tryAgainReset(a, null);
};
Athena.ClickToHighlight = function() {
    this.type = "slide";
    this.replaceClickedText = "View";
    this.originalClickedText = "Hide";
    this.changeContentType = "none";
    this.clickClass = "clickcontent";
    this.clickClickedClass = "clickedclass";
    this.revealClass = "slidecontent";
    this.runningTextClass = "running_text";
    this.runningObject = null;
    this.activityType = 1;
    this.showOneAtTime = false;
    this.highlightColors = [];
    this.highlightColorsRgb = [];
    this.modelContent = null;
    this.activityContainer = $(".interactivity-ch");
    this.previousClickIndex = 0;
    this.currentClickIndex = 0;
    this.maxCount = 0;
    this.clickContainer = "";
    this.revealContainer = "";
    this.numberOfButtons = 0;
    this.imageSourcePath = "assets/images/";
    this.pluginEvent = null;
    this.storedThis = undefined;
};
Athena.ClickToHighlight.extend(Athena.BaseClass);
Athena.ClickToHighlight.prototype.preset = function(b) {
    var a, c;
    this.currentClickIndex = b.value;
    if (Number(this.activityType) === 1) {
        this.clickEvent(this.clickContainer.eq(this.currentClickIndex));
    } else {
        if (String(b.value).length > 0) {
            c = String(b.value).split(",");
            for (a = 0; a < c.length; a++) {
                this.clickEvent(this.activityContainer.find(".interactivity-ch-text").find(".button-text").eq(c[a]));
            }
        }
    }
};
Athena.ClickToHighlight.prototype.persistData = function() {
    if (String(this.modelContent.contents.content_type.value) === "text") {
        var a, b = [];
        a = -1;
        this.activityContainer.find(".interactivity-ch-text").find(".button-text").each(function() {
            a++;
            if ($(this).hasClass("button-clicked")) {
                b.push(a);
            }
        });
        if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
            this.pluginEvent(null, null, null, null, {
                id: this.activityContainer.attr("data-objectid"),
                value: b
            });
        }
    } else {
        if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
            this.pluginEvent(null, null, null, null, {
                id: this.activityContainer.attr("data-objectid"),
                value: this.currentClickIndex
            });
        }
    }
};
Athena.ClickToHighlight.prototype.options = function(a) {
    var b;
    for (b in a) {
        if (a.hasOwnProperty(b)) {
            this[b] = a[b];
        }
    }
};
Athena.ClickToHighlight.prototype.reset = function() {
    if (String(this.modelContent.contents.content_type.value) === "text") {
        this.activityContainer.find(".interactivity-ch-image").hide();
        this.activityContainer.find(".buttons").html('<div class="button-container"><div class="button button-text active button-bottom"><span> </span></div></div>');
    } else {
        this.activityContainer.find(".interactivity-ch-text").hide();
        this.activityContainer.find(".image").html('<img class="img" src="" alt="img"/>');
        this.activityContainer.find(".buttons").html('<div class="button-container"><div class="button-desc button button-text active button-bottom"> <span> </span> </div></div>');
    }
    this.init();
};
Athena.ClickToHighlight.prototype.setEvents = function() {
    var a;
    a = this;
    this.currentClickIndex = this.maxCount = this.activityContainer.find("." + this.clickClass).length;
    this.create();
    this.clickContainer.unbind("click");
    this.clickContainer.bind("click", function() {
        a.clickEvent(this);
    });
    $(window).on("orientationchange", function() {
        setTimeout(function() {
            a.activityContainer.find(".image").each(function() {
                a.imageLoad($(this).find("img"));
            });
        }, 500);
    });
};
Athena.ClickToHighlight.prototype.create = function() {
    this.clickContainer = this.activityContainer.find("." + this.clickClass);
    this.revealContainer = this.activityContainer.find("." + this.revealClass);
    this.revealContainerRoot = this.activityContainer.find("." + this.revealClass + "_root");
};
Athena.ClickToHighlight.prototype.imageLoad = function(d) {
    var b = this,
        a, c;
    if ($(d).data("loaded") !== "loaded") {
        d.bind("load", function() {
            a = b.activityContainer.find(".image").attr("cur-width");
            c = b.activityContainer.find(".image").attr("cur-height");
            a = typeof a === "undefined" ? 0 : a;
            c = typeof c === "undefined" ? 0 : c;
            if (this.width > a) {
                b.activityContainer.find(".image").css("width", this.width);
                b.activityContainer.find(".image").attr("cur-width", this.width);
            }
            if ($(this).outerHeight() > c) {
                b.activityContainer.find(".image").css({
                    height: $(this).outerHeight()
                });
                b.activityContainer.find(".image").attr("cur-height", $(this).outerHeight());
            }
            $(d).data("loaded", "loaded");
        });
    } else {
        a = b.activityContainer.find(".image").attr("cur-width");
        c = b.activityContainer.find(".image").attr("cur-height");
        a = typeof a === "undefined" ? 0 : a;
        c = typeof c === "undefined" ? 0 : c;
        if (d.toArray()[0].width > a) {
            b.activityContainer.find(".image").css("width", d.toArray()[0].width);
            b.activityContainer.find(".image").attr("cur-width", d.toArray()[0].width);
        }
        if ($(d).outerHeight() > c) {
            b.activityContainer.find(".image").css({
                height: $(d).outerHeight()
            });
            b.activityContainer.find(".image").attr("cur-height", $(d).outerHeight());
        }
    }
};
Athena.ClickToHighlight.prototype.init = function() {
    var b, c, a, d;
    b = this;
    b.runningObject = b.activityContainer.find("." + b.runningTextClass);
    b.runningObject.empty().html(b.modelContent.contents.running_text.value);
    if (b.modelContent.contents.running_text.objectId !== undefined) {
        b.runningObject.attr("data-objectid", b.modelContent.contents.running_text.objectId);
    }
    b.runningObject.addClass("runningAnimation");
    if (Number(this.activityType) === 1) {
        this.createContent(this.activityContainer.find(".image"), this.modelContent.lists.highlight_image, "img", "image", false, function(e) {
            for (c = 0; c < e.length; c++) {
                e[c].attr("src", this.imageSourcePath + b.modelContent.lists.highlight_image[c].image_name.value).attr("class", "imageclass");
                if (b.modelContent.lists.highlight_image[c].image_name.objectId !== undefined) {
                    e[c].attr("data-objectid", b.modelContent.lists.highlight_image[c].image_name.objectId);
                }
                this.imageLoad(e[c]);
            }
            if (b.modelContent.contents.image_shape.value !== "") {
                a = document.createElement("img");
                a.src = this.imageSourcePath + b.modelContent.contents.image_shape.value;
                $(a).addClass("imageclass_root");
                if (b.modelContent.contents.image_shape.objectId !== undefined) {
                    $(a).attr("data-objectid", b.modelContent.contents.image_shape.objectId);
                }
                this.imageLoad($(a));
                e[0].parent().prepend($(a));
            }
        });
        this.createContent(this.activityContainer.find(".buttons").eq(0), this.modelContent.lists.button_text, "button-container", "text", false, function(e) {
            b.numberOfButtons = e.length;
            for (c = 0; c < e.length; c++) {
                e[c].find(".button-text span").html(b.modelContent.lists.button_text[c].button_name.value);
                if (b.modelContent.lists.button_text[c].button_name.objectId !== undefined) {
                    e[c].find(".button-text span").attr("data-objectid", b.modelContent.lists.button_text[c].button_name.objectId);
                }
            }
            var g, f = 0;
            for (c = 0; c <= b.numberOfButtons; c++) {
                g = parseInt(b.activityContainer.find("#button-container" + c).width(), 10);
                if (f < g) {
                    f = g;
                } else {
                    f = 0;
                }
            }
            b.activityContainer.find(".button-container").css("width", Number(f + 20) + "px");
            b.activityContainer.find(".button-container").find(".button-desc").css({
                "float": "none",
                margin: "0 auto"
            });
        });
    } else {
        if (Number(this.activityType) === 2) {
            this.createContent(this.activityContainer.find(".buttons").eq(1), this.modelContent.lists.button_text, "button-container", "text", false, function(e) {
                for (c = 0; c < e.length; c++) {
                    e[c].find(".button-text span").html(this.modelContent.lists.button_text[c].button_name.value);
                    if (this.modelContent.lists.button_text[c].button_name.objectId !== undefined) {
                        e[c].find(".button-text span").attr("data-objectid", b.modelContent.lists.button_text[c].button_name.objectId);
                    }
                }
            });
            for (c = 1; c < 10; c++) {
                this.modelContent.contents.highlight_text.value = this.modelContent.contents.highlight_text.value.replace(new RegExp("<" + c + ">", "gi"), '<span class = "' + c + '">').replace(new RegExp("<" + c + "/>", "gi"), "</span>");
            }
            this.activityContainer.find(".text").html(this.modelContent.contents.highlight_text.value);
            if (b.modelContent.contents.highlight_text.objectId !== undefined) {
                this.activityContainer.find(".text").attr("data-objectid", b.modelContent.contents.highlight_text.objectId);
            }
        }
    }
};
Athena.ClickToHighlight.prototype.createContent = function(d, e, c, g, a, f) {
    var b;
    b = this;
    c = d.find("." + c);
    if (String((typeof f)) !== "undefined" || String(f) === "null") {
        this.cloneElement(e.length, d, c, c.attr("class"), "append", "", a, f);
    } else {
        this.cloneElement(e.length, d, c, c.attr("class"), "append", "", a, function(h) {
            b.loadContent(h, e, g);
        });
    }
    this.setEvents();
};
Athena.ClickToHighlight.prototype.loadContent = function(g, f, h, a, e, d) {
    var c, b;
    for (b = 0; b < g.length; b++) {
        if (String(h) === "text") {
            c = g[b].find("span").length > 0 ? g[b].data(a, e[b]).find("span") : g[b].data(a, e[b]);
            g[b].data(a, e[b]).find("span").html(d[b]);
        } else {
            if (String(h) === "image") {
                g[b].find("img").attr("src", this.imageSourcePath + f[b]);
            }
        }
    }
};
Athena.ClickToHighlight.prototype.clickEvent = function(d) {
    var i, b, e, j, c, f, k, g, a, h, l;
    i = $(d);
    a = (String(this.modelContent.contents.content_type.value) === "text") ? this.activityContainer.find(".interactivity-ch-text") : this.activityContainer.find(".interactivity-ch-image");
    b = a.find("." + this.clickClass).index(d);
    this.currentClickIndex = b;
    e = this.revealContainer.eq(b);
    j = false;
    g = i.find("span").length > 0 ? i.find("span") : i;
    if (String(this.modelContent.contents.content_type.value) === "image") {
        h = typeof this.modelContent.lists.highlight_image[b].running_text === "undefined" ? "" : this.modelContent.lists.highlight_image[b].running_text.value;
    }
    l = this.modelContent.contents.running_text.value;
    if (String(e.css("display")) === "none" || String(e.css("opacity")) === "0") {
        j = false;
    } else {
        if (String(e.css("display")) === "block" || String(e.css("display")) === "table" || String(e.css("display")) === "inline" || String(e.css("opacity")) === "1") {
            j = true;
        }
    }
    if (this.showOneAtTime) {
        if (i.attr("clicked") !== this.activityContainer.find("." + this.clickClickedClass).attr("clicked")) {
            this.activityContainer.find("." + this.clickClickedClass).removeAttr("style clicked");
        }
        this.clickContainer.removeClass(this.clickClickedClass);
        this.runningObject.empty().html(l);
    }
    i.toggleClass(this.clickClickedClass);
    switch (this.type) {
        case "show":
            i.toggleClass("button-bottom");
            if (i.parent().find(".button-desc").attr("clicked") === undefined) {
                i.parent().find(".button-desc").attr("clicked", "true");
                i.parent().find(".button-desc").css("background-color", this.modelContent.lists.colour_value[b].colour.value);
            } else {
                i.parent().find(".button-desc").removeAttr("style");
                i.parent().find(".button-desc").removeAttr("clicked");
                i.removeClass(this.clickClickedClass);
            }
            this.activityContainer.find(".button-container").find(".button-desc").css({
                "float": "none"
            });
            if (this.showOneAtTime) {
                this.revealContainer.hide();
            }
            if (!j) {
                if (this.showOneAtTime) {
                    this.revealContainerRoot.hide();
                }
                if (typeof this.modelContent.lists.highlight_image[b].running_text !== "undefined") {
                    this.runningObject.empty().html(h);
                }
                e.show();
            } else {
                if (this.showOneAtTime) {
                    this.revealContainerRoot.show();
                }
                e.hide();
            }
            break;
        case "slide":
            if (this.showOneAtTime) {
                this.revealContainer.hide();
                e.slideToggle("slow");
            } else {
                e.slideToggle("slow");
            }
            break;
        case "fade":
            if (this.showOneAtTime) {
                this.revealContainer.hide();
            }
            if (!j) {
                e.fadeIn(1000);
            } else {
                e.fadeOut(1000);
            }
            break;
        case "texthighlight":
            f = b + 1;
            k = (String(this.activityContainer.find("." + f).css("color")) === String("rgb(0, 0, 0)")) ? "rgb(" + this.highlightColorsRgb[f - 1] + ")" : String("rgb(0, 0, 0)");
            this.activityContainer.find("." + f).toggleClass("boldclass");
            i.toggleClass("button-bottom");
            if (i.parent().find(".button").attr("clicked") === undefined) {
                i.parent().find(".button").attr("clicked", "true");
                i.parent().find(".button").css({
                    "border-top": "2px solid #5B5B5B",
                    "background-color": k
                });
            } else {
                i.parent().find(".button").removeAttr("style");
                i.parent().find(".button").removeAttr("clicked");
            }
            if (this.changeContentType === "same") {
                c = g.html() === this.originalClickedText ? this.replaceClickedText : this.originalClickedText;
                g.html(c);
            } else {
                if (this.changeContentType === "different" && String(typeof this.replaceClickedText) === "object" && String(typeof this.originalClickedText) === "object") {
                    c = g.html() === this.originalClickedText[b].button_name.value ? this.replaceClickedText[b].button_name.value : this.originalClickedText[b].button_name.value;
                    g.html(c);
                }
            }
            break;
    }
    if (typeof MathJax !== "undefined") {
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, a.toArray()[0].querySelector(".main-Container")], function() {
            var m;
        });
    }
    this.persistData();
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        this.pluginEvent(null, null, "started", "instructionq", this.activityContainer.attr("data-objectid"));
    }
};
Athena.ImageHotSpot = function() {
    this.imageContent = "";
    this.question = "";
    this.parentContainer = "";
    this.correctAnswerFeedback = "Correct answer is shown.";
    this.hintMsg = "";
    this.incompleteAnswerFeedback = "Your answer is incomplete.";
    this.question_container = "";
    this.hotspotAnswers = [];
    this.correctIndexes = [];
    this.minimumClickCount = 1;
    this.noOfAttempts = 2;
    this.attemptNo = 0;
    this.xCount = 0;
    this.yCount = 0;
    this.parentDom = null;
    this.feedbackContainer = null;
    this.checkbutton = null;
    this.questionContainer = null;
    this.dragContainer = null;
    this.activityContainer = null;
    this.model = null;
    this.hotspotAtIntersection = true;
    this.resetAll = true;
    this.partiallyAnswered = false;
    this.dragClass = "drag";
    this.dropClass = "drop";
    this.hoverClass = "point";
    this.stableClass = "stablepoint";
    this.partialAnswer = "You have partially answered.";
    this.selectCount = "many";
    this.checkClass = "check-box";
    this.resetClass = "reset-box";
    this.showMeClass = "showmeClass";
    this.myAnswerClass = "correctAnswerClass";
    this.transparentBorder = "1px solid rgb(176,147,1)";
    this.transpatentBackground = "none";
    this.checkbox_EnableState_Class = "active";
    this.hotspotType = "hover";
    this.hotspotBackground = "none";
    this.feedbackContainer = "feedback_container";
    this.hintContainer = "hint_container";
    this.successMsg = "Correct!";
    this.firstAttemptFailMsg = "That's not it";
    this.secondAttemptFailMsg = "That's not it";
    this.correctClass = "correct-green";
    this.incorrectClass = "incorrect-red";
    this.checkText = "Check";
    this.tryagainText = "Try again";
    this.showMeText = "Show me";
    this.myAnswerText = "My answer";
    this.myanswerCorrectCls = "correct-blue";
    this.button = "";
    this.type = "";
    this.imagesPath = "assets/images/";
    this.pluginEvent = null;
    this.key = "";
    this.storage = {};
    this.isMobile = "";
    this.mobileType = "";
};
Athena.ImageHotSpot.extend(Athena.BaseClass);
Athena.ImageHotSpot.prototype.save = function() {
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        this.pluginEvent(null, null, null, null, {
            id: this.model.objectId,
            value: this.storage
        });
    }
};
Athena.ImageHotSpot.prototype.preset = function(d) {
    var a = this,
        b, c;
    if (d !== undefined && d.value !== undefined) {
        this.storage = d.value;
        this.attemptNo = this.storage.attemptNo;
        c = (this.hotspotType === "hover") ? this.dragClass : this.dropClass;
        if (this.storage.clickedIndexes !== undefined) {
            for (b = 0; b < this.storage.clickedIndexes.length; b++) {
                this.parentContainer.find("." + c).eq(this.storage.clickedIndexes[b]).trigger("click");
            }
            this.validateAnswer();
            if (this.storage.checkButtonText !== undefined) {
                if (String(this.storage.checkButtonText) === this.tryagainText) {
                    this.checkbutton.trigger("click");
                } else {
                    if (String(this.storage.checkButtonText) === this.showMeText) {
                        this.toggle(this.checkbutton);
                    }
                }
            }
        }
    }
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        this.pluginEvent(null, null, null, null, {
            id: this.model.objectId,
            value: undefined
        });
    }
};
Athena.ImageHotSpot.prototype.calculateImageWidthandHeight = function(e) {
    var f, b, g, c, a, d;
    c = this;
    g = "";
    b = document.getElementsByTagName("body");
    f = document.createElement("img");
    f.src = this.imagesPath + e;
    $(f).bind("load", function() {
        a = Number($(this).height()) + 5;
        d = Number($(this).width()) + 5;
        c.parentContainer.find(".image-container").css({
            height: a,
            width: d
        });
        c.parentContainer.find("." + c.imageContent).css({
            height: a,
            width: d,
            background: "url('" + c.imagesPath + c.model.contents.image.value.trim() + "') no-repeat"
        });
        if (c.model.contents.image.objectId !== undefined) {
            c.parentContainer.find("." + c.imageContent).attr("data-objectid", c.model.contents.image.objectId);
        }
        $(this).remove();
    });
    b[0].appendChild(f);
    if (this.checkDevices() === "mobile") {
        if (typeof MathJax !== "undefined") {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.activityContainer.toArray()[0].querySelector(".main-Container"), function() {
                setTimeout(function() {
                    c.parentContainer.find(".image-container").wrapAll("<div class='image-scroll-content'></div>");
                    c.parentContainer.find(".image-scroll-content").css({
                        width: "100%",
                        overflow: "hidden",
                        position: "relative"
                    });
                    c.parentContainer.find(".mCSB_container").css({
                        width: "100%"
                    });
                    c.parentContainer.find(".image-scroll-content").mCustomScrollbar({
                        horizontalScroll: true,
                        autoHideScrollbar: true,
                        scrollInertia: 100
                    });
                    c.parentContainer.find(".image-scroll-content").find(".mCSB_container").css("margin-bottom", "10px");
                    c.parentContainer.find(".image-scroll-content").find(".ddspan").css("font-size", "14px");
                }, 1000);
            }]);
        }
        $(window).on("orientationchange", function(h) {
            c.parentContainer.find(".image-scroll-content").mCustomScrollbar("update");
        });
    }
};
Athena.ImageHotSpot.prototype.options = function(a) {
    var b;
    for (b in a) {
        if (a.hasOwnProperty(b)) {
            this[b] = a[b];
        }
    }
};
Athena.ImageHotSpot.prototype.init = function() {
    var b, d, a, c;
    b = this;
    if (typeof this.parentDom === "string") {
        this.parentContainer = this.activityContainer.find("." + this.parentDom);
    }
    if (typeof this.dragContainer === "string") {
        this.dragContainer = this.activityContainer.find("." + this.dragContainer);
    }
    if (typeof this.feedbackContainer === "string") {
        this.feedbackContainer = this.activityContainer.find("." + this.feedbackContainer);
    }
    if (typeof this.hintContainer === "string") {
        this.hintContainer = this.activityContainer.find("." + this.hintContainer);
        if (this.model.contents.hint.objectId !== undefined) {
            this.hintContainer.attr("data-objectid", this.model.contents.hint.objectId);
        }
        this.hintContainer.find(".hint span").html(this.model.contents.hint.value);
    }
    if (typeof this.checkClass === "string") {
        this.checkbutton = this.activityContainer.find("." + this.checkClass);
    }
    this.hintMsg = this.model.contents.hint.value;
    this.question = this.model.contents.question.value;
    this.type = this.model.contents.image_hotspot_type.value;
    this.parentContainer.find("." + this.button).show().removeClass("active").find("span").html(this.checkText);
    d = this.calculateImageWidthandHeight(this.model.contents.image.value.trim());
    this.key = window.location.href + "-" + this.activityContainer.parent().parent().attr("id") + "-" + this.activityContainer.attr("class");
};
Athena.ImageHotSpot.prototype.reset = function() {
    this.attemptNo = 0;
    this.hintContainer.hide();
    this.feedbackContainer.removeClass("hasText").find("span").html("");
    this.parentContainer.find("." + this.imageContent).html("");
    this.init();
    if (String(this.model.contents.image_hotspot_area.value.trim()) === "intersection") {
        this.createIntersectionImageHotSpots();
    } else {
        if (String(this.model.contents.image_hotspot_area.value.trim()) === "specific") {
            this.createImageHotSpots();
        }
    }
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        this.pluginEvent(null, null, null, null, {
            id: this.model.objectId,
            value: undefined
        });
    }
};
Athena.ImageHotSpot.prototype.createIntersectionImageHotSpots = function() {
    var q, c, n, o, h, g, d, p, k, f, r, b, e, l, m, a = "";
    q = this;
    f = this.model.contents.startingPoint.value;
    if (!this.isMobile) {
        r = this.model.contents.xDisplacement.value;
        b = this.model.contents.yDisplacement.value;
    } else {
        if (this.mobileType === "Android" && this.model.lists.xDisplacementAndroid !== undefined && this.model.lists.yDisplacementAndroid !== undefined) {
            r = this.model.contents.xDisplacementAndroid.value;
            b = this.model.contents.yDisplacementAndroid.value;
        } else {
            if (this.mobileType === "iPhone" && this.model.lists.xDisplacementiPhone !== undefined && this.model.lists.yDisplacementiPhone !== undefined) {
                r = this.model.contents.xDisplacementiPhone.value;
                b = this.model.contents.yDisplacementiPhone.value;
            } else {
                r = this.model.contents.xDisplacement.value;
                b = this.model.contents.yDisplacement.value;
            }
        }
    }
    e = this.model.contents.noOfHotspotX.value;
    l = this.model.contents.noOfHotspotY.value;
    m = this.model.contents.hoverclass.value;
    c = f.split(",")[0];
    n = f.split(",")[1];
    o = "";
    this.xCount = e;
    this.yCount = l;
    this.hoverClass = m;
    if (this.hotspotType === "hover") {
        o = '<div class="' + this.dragClass + "\" style='position:absolute;top:" + n + "px;left:" + c + "px;'></div>";
    } else {
        if (this.hotspotType === "drop") {
            o = '<div class="' + this.dropClass + "\" style='position:absolute;top:" + n + "px;left:" + c + "px;'></div>";
        }
    }
    this.parentContainer.append(o);
    for (h = 1; h <= l; h++) {
        for (g = 1; g <= e; g++) {
            d = parseInt(c, 10) + (r * g);
            p = parseInt(n, 10) + (b * (h - 1));
            if (this.hotspotType === "hover") {
                o = '<div class="' + this.dragClass + "\" style='position:absolute;top:" + p + "px;left:" + d + "px;'></div>";
            } else {
                if (this.hotspotType === "drop") {
                    o = '<div class="' + this.dropClass + "\" style='position:absolute;top:" + p + "px;left:" + d + "px;'></div>";
                }
            }
            this.parentContainer.find("." + this.imageContent).append(o);
        }
    }
    if (this.hotspotType === "hover") {
        if (navigator.userAgent.indexOf("iPad") !== -1 || navigator.userAgent.indexOf("Android") !== -1) {
            k = "do nothing";
        } else {
            this.parentContainer.find("." + this.dragClass).unbind("mouseenter");
            this.parentContainer.find("." + this.dragClass).bind("mouseenter", function() {
                q.hotSpotMouseEnter(this);
            });
            this.parentContainer.find("." + this.dragClass).unbind("mouseleave");
            this.parentContainer.find("." + this.dragClass).bind("mouseleave", function() {
                q.hotSpotMouseLeave(this);
            });
        }
        this.parentContainer.find("." + this.dragClass).unbind("click");
        this.parentContainer.find("." + this.dragClass).bind("click", function() {
            q.hotSpotClick(this);
        });
    } else {
        if (this.hotspotType === "drop") {
            this.addDraggable(this.activityContainer.find("." + this.dragClass));
            this.addDroppable(this.parentContainer.find("." + this.dropClass));
        }
    }
    this.setEvents();
};
Athena.ImageHotSpot.prototype.createImageHotSpots = function() {
    var k, h, d, b, j, e, f, g, c, a = "";
    k = this;
    if (!this.isMobile) {
        f = this.model.lists.positions;
    } else {
        if (this.mobileType === "Android" && this.model.lists.positionsAndroid !== undefined) {
            f = this.model.lists.positionsAndroid;
        } else {
            if (this.mobileType === "iPhone" && this.model.lists.positionsiPhone !== undefined) {
                f = this.model.lists.positionsiPhone;
            } else {
                f = this.model.lists.positions;
            }
        }
    }
    g = this.model.contents.hoverclass.value;
    c = this.model.lists.correctAnswers;
    this.hoverClass = g;
    for (d = 0; d < f.length; d++) {
        b = f[d].positionsValue.value.split(",")[0];
        j = f[d].positionsValue.value.split(",")[1];
        if (f[d].positionsValue.objectId !== undefined) {
            a = f[d].positionsValue.objectId;
        } else {
            a = "";
        }
        if (this.hotspotType === "hover") {
            h = "<div data-objectid='" + a + "' class=\"" + this.dragClass + "\" style='position:absolute;top:" + j + "px;left:" + b + "px;'></div>";
        } else {
            if (this.hotspotType === "drop") {
                h = "<div data-objectid='" + a + "' class=\"" + this.dropClass + "\" style='position:absolute;top:" + j + "px;left:" + b + "px;'></div>";
            }
        }
        this.parentContainer.find("." + this.imageContent).append(h);
    }
    if (this.hotspotType === "hover") {
        if (navigator.userAgent.indexOf("iPad") !== -1 || navigator.userAgent.indexOf("Android") !== -1) {
            e = "do nothing";
        } else {
            this.parentContainer.find("." + this.dragClass).bind("mouseenter", function() {
                k.hotSpotMouseEnter(this);
            });
            this.parentContainer.find("." + this.dragClass).bind("mouseleave", function() {
                k.hotSpotMouseLeave(this);
            });
        }
        this.parentContainer.find("." + this.dragClass).bind("click", function() {
            k.hotSpotClick(this);
        });
    } else {
        if (this.hotspotType === "drop") {
            this.addDraggable(this.activityContainer.find("." + this.dragClass));
            this.addDroppable(this.parentContainer.find("." + this.dropClass));
        }
    }
    this.setEvents();
};
Athena.ImageHotSpot.prototype.setEvents = function() {
    var a, c, b;
    a = this;
    this.parentContainer.find("." + this.question_container).html(this.question);
    if (this.model.contents.question.objectId !== undefined) {
        this.parentContainer.find("." + this.question_container).attr("data-objectid", this.model.contents.question.objectId);
    }
    this.activityContainer.find("." + this.checkClass).unbind("click");
    this.activityContainer.find("." + this.checkClass).bind("click", function() {
        c = $(this).find("span").html();
        if (c === a.checkText && $(this).hasClass(a.checkbox_EnableState_Class)) {
            a.validateAnswer();
            a.save();
        } else {
            if (c === a.tryagainText) {
                if (navigator.userAgent.indexOf("iPad") !== -1 || navigator.userAgent.indexOf("Android") !== -1) {
                    b = "do nothing";
                } else {
                    a.parentContainer.find("." + a.dragClass).unbind("mouseenter");
                    a.parentContainer.find("." + a.dragClass).bind("mouseenter", function() {
                        a.hotSpotMouseEnter(this);
                    });
                    a.parentContainer.find("." + a.dragClass).unbind("mouseleave");
                    a.parentContainer.find("." + a.dragClass).bind("mouseleave", function() {
                        a.hotSpotMouseLeave(this);
                    });
                }
                a.parentContainer.find("." + a.dragClass).unbind("click");
                a.parentContainer.find("." + a.dragClass).bind("click", function() {
                    a.hotSpotClick(this);
                });
                a.hintContainer.hide();
                a.feedbackContainer.find(".feedback").hide();
                a.checkbutton.find("span").html(a.checkText);
                a.feedbackContainer.removeClass("hasText");
                a.resetAll = true;
                a.resetHotspots();
                a.storage.checkButtonText = c;
                a.save();
            } else {
                if (c === a.showMeText || c === a.myAnswerText) {
                    a.storage.checkButtonText = c;
                    a.save();
                    a.feedbackContainer.find(".feedback").show();
                    a.toggle($(this));
                }
            }
        }
    });
};
Athena.ImageHotSpot.prototype.addDraggable = function(b) {
    var a = this,
        c;
    b.draggable({
        start: function(d, e) {
            c = typeof $(this).parent().attr("id") === "undefined" ? $(this).parent().attr("class") : $(this).parent().attr("id");
            if ($(this).parent().find("div").hasClass(a.stableClass)) {
                $(this).parent().find("." + a.stableClass).css({
                    background: "none",
                    border: "none"
                });
            }
            a.parentContainer.find(".drop").css("border", "1px solid #B09301");
        },
        stop: function() {
            if ($(this).parent().find("div").hasClass(a.stableClass)) {
                $(this).parent().find("." + a.stableClass).css({
                    background: a.transpatentBackground,
                    border: a.transparentBorder
                });
            }
            a.parentContainer.find(".drop").css("border", "none");
        },
        revert: "invalid"
    });
};
Athena.ImageHotSpot.prototype.addDroppable = function(b) {
    var a = this;
    b.droppable({
        drop: function(d, e) {
            if (a.type === "multiple") {
                var c = e.draggable.clone();
                a.addDraggable(c.css({
                    left: "5px",
                    top: "5px"
                }).appendTo($(a.dragContainer)));
                $(this).html("");
            }
            if (!($(this).find("div").hasClass(a.stableClass))) {
                $(this).append('<div class="' + a.stableClass + '"></div>');
            }
            e.draggable.css({
                position: "absolute",
                left: "0px",
                top: "0px"
            }).appendTo($(this));
            a.CheckBoxState();
        },
        tolerance: "pointer"
    });
};
Athena.ImageHotSpot.prototype.hotSpotMouseEnter = function(b) {
    var a = document.createElement("div");
    a.className = this.hoverClass;
    if (!$(b).find("div").hasClass(this.stableClass)) {
        $(b).css("background", this.hotspotBackground).append(a);
    }
};
Athena.ImageHotSpot.prototype.hotSpotMouseLeave = function(a) {
    if (!$(a).find("div").hasClass(this.stableClass)) {
        $(a).css("background", "none").html("");
    }
};
Athena.ImageHotSpot.prototype.hotSpotClick = function(a) {
    if (this.type === "single") {
        this.parentContainer.find("." + this.dragClass).css("background", "none").html("");
        $(a).css("background", this.hotspotBackground).html('<div class="' + this.stableClass + '"></div>');
    } else {
        if (this.type === "multiple") {
            if (!($(a).find("div").hasClass(this.stableClass))) {
                if (typeof this.selectCount === "number") {
                    if (this.parentContainer.find("." + this.stableClass).length < (this.selectCount)) {
                        $(a).css("background", this.hotspotBackground).html('<div class="' + this.stableClass + '"></div>');
                    }
                } else {
                    $(a).css("background", this.hotspotBackground).html('<div class="' + this.stableClass + '"></div>');
                }
            } else {
                $(a).css("background", "none").html("");
            }
        }
    }
    this.CheckBoxState();
};
Athena.ImageHotSpot.prototype.CheckBoxState = function() {
    if (this.parentContainer.find("." + this.stableClass).length >= this.minimumClickCount) {
        this.activityContainer.find("." + this.checkClass).addClass(this.checkbox_EnableState_Class);
    } else {
        this.activityContainer.find("." + this.checkClass).removeClass(this.checkbox_EnableState_Class);
    }
};
Athena.ImageHotSpot.prototype.resetHotspots = function() {
    var a;
    a = this;
    this.activityContainer.find("." + this.checkClass).removeClass(this.checkbox_EnableState_Class);
    this.parentContainer.find("." + this.stableClass).each(function() {
        if (!$(this).hasClass(a.correctClass)) {
            $(this).detach();
        } else {
            if (!a.resetAll) {
                $(this).parent().unbind("click");
            }
        }
    });
    if (this.resetAll) {
        this.parentContainer.find("." + this.correctClass).removeClass(this.correctClass).removeClass(this.stableClass);
    }
    if (String(this.hotspotType) !== "hover") {
        if (((String(this.type) === "single") && (this.parentContainer.find("." + this.dropClass).find("." + this.dragClass).length > 0))) {
            this.addDraggable(this.parentContainer.find("." + this.dropClass).find("." + this.dragClass + ":eq(0)").css({
                left: "5px",
                top: "5px"
            }).appendTo($(this.dragContainer)));
        }
        this.parentContainer.find("." + this.dropClass).find("." + this.dragClass).detach();
    }
};
Athena.ImageHotSpot.prototype.validateAnswer = function(g) {
    var o, h, f, e, c, k, a, n, l, d, b, m;
    n = this;
    h = [];
    b = [];
    e = 0;
    d = false;
    l = (this.hotspotType === "hover") ? this.dragClass : this.dropClass;
    for (f = 0; f < this.hotspotAnswers.length; f++) {
        if (this.hotspotAtIntersection) {
            c = parseInt(this.hotspotAnswers[f].answer.value.replace("(", "").split(",")[0], 10);
            k = parseInt(this.hotspotAnswers[f].answer.value.replace(")", "").split(",")[1], 10);
            h[f] = parseInt((parseInt(c, 10) * parseInt(this.yCount, 10)), 10) + (k - 1);
            a = this.parentContainer.find("." + l + ":eq(" + h[f] + ")");
        } else {
            h[f] = Number(this.hotspotAnswers[f].answer.value);
            a = this.parentContainer.find("." + l + ":eq(" + this.hotspotAnswers[f].answer.value + ")");
        }
        if (a.find("div").hasClass(this.stableClass)) {
            a.find("div").addClass(this.correctClass);
        }
    }
    this.correctIndexes = h;
    this.parentContainer.find("." + this.stableClass).each(function() {
        m = n.parentContainer.find("." + l).index($(this).parent());
        b.push(m);
        if (!$(this).hasClass(n.correctClass)) {
            d = true;
            $(this).addClass(n.incorrectClass);
            $(this).parent().find("." + n.dragClass).css("background", "none");
            $(this).css("background", "rgba(255,0,0,0.4)");
        } else {
            $(this).addClass(n.correctClass);
            $(this).parent().find("." + n.dragClass).css("background", "none");
            $(this).css("background", "rgba(0,255,0,0.4)");
        }
    });
    this.storage.clickedIndexes = b;
    if (this.hotspotAnswers.length !== this.parentContainer.find("." + this.stableClass).length) {
        if (!d) {
            this.partiallyAnswered = true;
        } else {
            this.partiallyAnswered = false;
        }
        d = true;
    }
    this.showFeedbackMsg(d);
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        o = true;
        if (d === true) {
            o = false;
        }
        this.pluginEvent(null, o, "started", "instructionq", n.model.objectId);
    }
};
Athena.ImageHotSpot.prototype.showFeedbackMsg = function(a) {
    var b = this,
        c;
    this.storage.attemptNo = this.attemptNo;
    this.attemptNo++;
    this.save();
    if (!a) {
        this.feedbackContainer.addClass("hasText").find(".feedback span").html(this.successMsg).show();
        this.feedbackAutoHide();
        this.checkbutton.hide();
        this.activityContainer.find("." + this.dragClass).unbind("click mouseenter mouseleave").css("cursor", "inherit");
        if (this.model.contents.feedback_correct.objectId !== undefined) {
            this.feedbackContainer.attr("data-objectid", this.model.contents.feedback_correct.objectId);
        }
    } else {
        if (this.attemptNo === 1 && parseInt(this.noOfAttempts, 10) > 1) {
            this.hintContainer.show();
            this.checkbutton.find("span").html(this.tryagainText);
            this.activityContainer.find("." + this.dragClass).unbind("click mouseenter mouseleave").css("cursor", "inherit");
            this.feedbackContainer.addClass("hasText").find(".feedback ").show();
            if (this.partiallyAnswered) {
                this.feedbackContainer.addClass("hasText").find(".feedback span").html(this.partialAnswer).show();
            } else {
                this.feedbackContainer.addClass("hasText").find(".feedback span").html(this.firstAttemptFailMsg).show();
            }
            if (this.model.contents.feedback_first_incorrect.objectId !== undefined) {
                this.feedbackContainer.attr("data-objectid", this.model.contents.feedback_first_incorrect.objectId);
            }
        } else {
            if ((this.attemptNo > 1 && parseInt(this.noOfAttempts, 10) > 1) || (this.attemptNo === 1 && parseInt(this.noOfAttempts, 10) === 1)) {
                this.checkbutton.addClass(this.showMeClass).find("span").html(this.showMeText);
                if (this.type === "single") {
                    this.checkbutton.hide();
                    setTimeout(function() {
                        b.feedbackContainer.find(".feedback span").fadeOut(1000);
                    }, 2000);
                    this.feedbackContainer.find(".feedback").show();
                    this.activityContainer.find("." + this.dragClass).eq(this.correctIndexes[0]).addClass(this.myanswerCorrectCls);
                    this.feedbackContainer.addClass("hasText").find(".feedback span").html(this.secondAttemptFailMsg).show();
                    setTimeout(function() {
                        b.feedbackContainer.removeClass("hasText");
                    }, 3000);
                } else {
                    this.feedbackContainer.find(".feedback").show();
                    if (this.partiallyAnswered) {
                        this.feedbackContainer.addClass("hasText").find(".feedback span").html(this.partialAnswer).show();
                    } else {
                        this.feedbackContainer.addClass("hasText").find(".feedback span").html(this.firstAttemptFailMsg).show();
                    }
                    this.myAnswers = [];
                    c = -1;
                    this.parentContainer.find("." + this.stableClass).each(function() {
                        b.myAnswers[++c] = b.activityContainer.find("." + b.dragClass).index($(this).parent());
                    });
                }
                if (this.model.contents.feedback_second_incorrect.objectId !== undefined) {
                    this.feedbackContainer.attr("data-objectid", this.model.contents.feedback_second_incorrect.objectId);
                }
            }
        }
    }
    if (this.attemptNo === 2) {
        this.activityContainer.find("." + this.dragClass).unbind("click mouseenter mouseleave").css("cursor", "inherit");
    }
};
Athena.ImageHotSpot.prototype.feedbackAutoHide = function() {
    var a = this;
    setTimeout(function() {
        a.feedbackContainer.find(".feedback").fadeIn();
    }, 0);
    setTimeout(function() {
        a.feedbackContainer.find(".feedback").fadeOut("medium", function() {
            a.feedbackContainer.removeClass("hasText");
        });
    }, 2000);
};
Athena.ImageHotSpot.prototype.toggle = function(b) {
    var d, a, c;
    a = this;
    d = -1;
    this.activityContainer.find("." + this.dragClass).unbind("click");
    this.activityContainer.find("." + this.stableClass).detach();
    if (b.hasClass(this.showMeClass)) {
        b.removeClass(this.showMeClass).addClass(this.myAnswerClass).find("span").html(this.myAnswerText);
        this.feedbackContainer.addClass("hasText").find(".feedback span").html(this.correctAnswerFeedback).show();
        if (this.model.contents.show_answer.objectId !== undefined) {
            this.feedbackContainer.attr("data-objectid", this.model.contents.show_answer.objectId);
        }
        for (c = 0; c < this.correctIndexes.length; c++) {
            this.activityContainer.find("." + this.dragClass).eq(this.correctIndexes[c]).append('<div class="' + this.stableClass + '"></div>');
            this.activityContainer.find("." + this.dragClass).eq(this.correctIndexes[c]).find("." + this.stableClass).addClass(this.myanswerCorrectCls);
        }
    } else {
        if (b.hasClass(this.myAnswerClass)) {
            this.feedbackContainer.addClass("hasText").find(".feedback span").html(this.incompleteAnswerFeedback).hide();
            b.removeClass(this.myAnswerClass).addClass(this.showMeClass).find("span").html(this.showMeText);
            for (c = 0; c < this.myAnswers.length; c++) {
                this.activityContainer.find("." + this.dragClass).eq(this.myAnswers[c]).append('<div class="' + this.stableClass + '"></div>');
                if (this.correctIndexes.indexOf(this.myAnswers[c]) === -1) {
                    this.activityContainer.find("." + this.dragClass).eq(this.myAnswers[c]).find("." + this.stableClass).addClass(this.incorrectClass);
                } else {
                    this.activityContainer.find("." + this.dragClass).eq(this.myAnswers[c]).find("." + this.stableClass).addClass(this.correctClass);
                }
            }
        }
    }
};
Athena.InstructionalText = function() {
    this.activityContainer = null;
    this.model = "";
    this.tableClass = "";
    this.tableHeadClass = "";
    this.tableRowClass = "";
    this.tableCellClass = "";
    this.mapSection = {
        previewLabel: "",
        lessonPerformanceTaskHead: "",
        runningText: "",
        screenimage: "",
        techtextart: "",
        datatable: ""
    };
    this.imageTextRightSideContent = "line-text right-side-content";
    this.imageTextLeftSideContent = "line-text left-side-content";
    this.textHighlightercallBack = null;
    this.audioClicked = null;
    this.videoCounter = 0;
    this.objectsId = [];
    this.imageSourcePath = "assets/images/";
    this.objIdParent = null;
    this.deviceType = "";
    this.isOnline = true;
    this.pluginEvent = null;
};
Athena.InstructionalText.extend(Athena.BaseClass);
Athena.InstructionalText.prototype.options = function(a) {
    var b;
    for (b in a) {
        if (a.hasOwnProperty(b)) {
            this[b] = a[b];
        }
    }
    this.deviceType = this.checkDevices();
};
Athena.InstructionalText.prototype.formDataTable = function(e, a) {
    var f, c, d, h, k, l, g, b;
    d = [
        []
    ];
    h = 0;
    l = 0;
    for (f in e) {
        if (e.hasOwnProperty(f)) {
            if (h++ > 0) {
                k = 0;
                d[l] = [];
                for (c in e[f]) {
                    if (e[f].hasOwnProperty(c)) {
                        d[l][k++] = e[f][c];
                    }
                }
                l++;
            } else {
                g = e[f].position.value;
                if (typeof e[f].colspan !== "undefined") {
                    b = e[f].colspan.value;
                }
            }
        }
    }
    this.createTable(d, this.tableHeadClass, this.tableRowClass, this.tableCellClass, g, a, b);
};
Athena.InstructionalText.prototype.updateTableScrollbar = function() {
    var a, d, f, c, g, e, b = this;
    if (this.deviceType === "mobile") {
        a = Number(this.activityContainer.find(".table").width());
        c = Number(this.activityContainer.find(".table").height()) + 5;
        this.activityContainer.find(".table").wrapAll("<div class='table-scroll-container'></div>");
        this.activityContainer.find(".table-scroll-container").css({
            height: "auto",
            margin: "0 auto",
            width: "90%",
            overflow: "hidden"
        });
        if (this.activityContainer.find(".table-scroll-container").width() < this.activityContainer.find(".table").width()) {
            this.activityContainer.find(".table-scroll-container").find(".table").css("margin-left", "0px");
            this.activityContainer.find(".table-scroll-container").mCustomScrollbar({
                horizontalScroll: true,
                autoHideScrollbar: true,
                scrollInertia: 100
            });
            this.activityContainer.find(".table-scroll-container").find(".mCSB_container").css("margin-bottom", "10px");
            this.activityContainer.find(".table-scroll-container").find(".table").css("width", "100%");
        }
        f = this.activityContainer.find("table").not(".table");
        if (Number(f.length) !== 0) {
            for (d = 0; d < f.length; d++) {
                e = Number($(f[d]).height());
                if (this.activityContainer.find(".scroll-container-" + d).height() < e) {
                    $(f[d]).wrapAll("<div class='scroll-container-" + d + "'></div>");
                    this.activityContainer.find(".scroll-container-" + d).find("table").css("margin-left", "0px");
                    this.activityContainer.find(".scroll-container-" + d).css({
                        margin: "0 auto",
                        width: "90%",
                        overflow: "hidden",
                        position: "relative",
                        height: "auto"
                    });
                    this.activityContainer.find(".scroll-container-" + d).mCustomScrollbar({
                        horizontalScroll: true,
                        autoHideScrollbar: true,
                        scrollInertia: 100
                    });
                    this.activityContainer.find(".scroll-container-" + d).find(".mCSB_container").css("margin-bottom", "10px");
                    this.activityContainer.find(".scroll-container-" + d).find("table").css("width", "100%");
                }
            }
        }
    } else {
        setTimeout(function() {
            b.activityContainer.find(".table-scroll-container").mCustomScrollbar("destroy");
            b.activityContainer.find(".table-scroll-container").find(".table").removeAttr("style");
        }, 500);
    }
};
Athena.InstructionalText.prototype.createTable = function(h, m, g, e, u, f, b, a) {
    var o, n, l, p, d, s, c, q, t, r;
    p = document.createElement("table");
    $(p).attr({
        cellspacing: 0,
        cellpadding: 0
    });
    p.className = "table";
    p.id = f;
    d = document.createElement("th");
    d.className = m;
    q = document.createElement("div");
    q.className = this.tableClass;
    for (n = 0; n < h[0].length; n++) {
        s = document.createElement("tr");
        s.className = g;
        for (l = 0; l < h.length; l++) {
            c = document.createElement("td");
            c.className = e;
            c.innerHTML = h[l][n].value;
            s.appendChild(c);
            if (h[l][n].objectId !== undefined) {
                $(c).attr("data-objectid", h[l][n].objectId);
            }
            if (n === 0 && typeof b !== "undefined") {
                c.colSpan = b;
                break;
            }
        }
        if (n === 0) {
            d.appendChild(s);
            p.appendChild(d);
        } else {
            p.appendChild(s);
        }
    }
    q.innerHTML = p.outerHTML;
    this.activityContainer.append(p);
    t = this.activityContainer.find("#" + this.objectsId[u]);
    this.activityContainer.find("#" + f).insertBefore(t);
    this.objectsId.push("");
    for (l = this.objectsId.length - 1; l > u; l--) {
        this.objectsId[l] = this.objectsId[l - 1];
    }
    this.objectsId[u] = f;
};
Athena.InstructionalText.prototype.createImageTextTypeA = function(c, h) {
    var e, a, g, d, b, f;
    b = document.createElement("div");
    b.className = "section-2";
    f = document.createElement("div");
    f.className = "main-container-2";
    e = document.createElement("div");
    e.className = this.imageTextRightSideContent;
    a = document.createElement("div");
    a.className = this.imageTextLeftSideContent;
    g = document.createElement("p");
    d = document.createElement("img");
    d.src = this.imageSourcePath + c.image.value;
    d.className = "imageTextimg";
    if (c.image.objectId !== undefined) {
        $(d).attr("data-objectid", c.image.objectId);
    }
    if (c.passage.objectId !== undefined) {
        $(g).attr("data-objectid", c.passage.objectId);
    }
    g.innerHTML = c.passage.value;
    a.appendChild(g);
    e.appendChild(d);
    f.appendChild(a);
    f.appendChild(e);
    b.appendChild(f);
    b.id = h;
    this.activityContainer.append(b);
};
Athena.InstructionalText.prototype.createImageTextTypeB = function(c, h) {
    var e, a, g, d, b, f;
    b = document.createElement("div");
    b.className = "section-2";
    f = document.createElement("div");
    f.className = "main-container-2";
    e = document.createElement("div");
    e.className = this.imageTextRightSideContent;
    a = document.createElement("div");
    a.className = this.imageTextLeftSideContent;
    g = document.createElement("p");
    d = document.createElement("img");
    d.src = this.imageSourcePath + c.image.value;
    d.className = "imageTextimg";
    g.innerHTML = c.passage.value;
    if (c.image.objectId !== undefined) {
        $(d).attr("data-objectid", c.image.objectId);
    }
    if (c.passage.objectId !== undefined) {
        $(g).attr("data-objectid", c.passage.objectId);
    }
    e.appendChild(g);
    a.appendChild(d);
    f.appendChild(a);
    f.appendChild(e);
    b.appendChild(f);
    b.id = h;
    this.activityContainer.append(b);
};
Athena.InstructionalText.prototype.createScreenImage = function(d, h) {
    var c, g, a, e, f, b;
    c = document.createElement("div");
    c.className = "section-2";
    g = document.createElement("div");
    g.className = "main-container-2";
    a = document.createElement("div");
    a.className = "image-bottom-content";
    a.innerHTML = d.caption.value;
    e = document.createElement("img");
    e.src = this.imageSourcePath + d.image.value;
    if (d.image.objectId !== undefined) {
        e.setAttribute("data-objectid", d.image.objectId);
    }
    if (d.caption.objectId !== undefined) {
        a.setAttribute("data-objectid", d.caption.objectId);
    }
    g.appendChild(e);
    g.appendChild(a);
    c.appendChild(g);
    c.id = h;
    this.activityContainer.append(c);
};
Athena.InstructionalText.prototype.createText = function(f, b, g) {
    var i, d, e, a, c, h;
    h = null;
    c = this.textHighlightercallBack;
    i = f;
    a = 0;
    d = document.createElement("p");
    e = i.replace(/<hi/g, '<span class="underlineText glossary-item"');
    e = e.split("</hi>").join("</span>");
    e = e.split("<runningSubhead>").join('<span class="runningSubhead">');
    e = e.split("</runningSubhead>").join("</span>");
    d.innerHTML = e;
    $(d).find("span").each(function() {
        $(this).attr("id", "highlight-" + (++a));
    });
    $(d).find("span").on("mousedown", function() {
        h = $(this).attr("id");
    });
    d.id = b;
    if (g !== undefined) {
        d.setAttribute("data-objectid", g);
    }
    this.activityContainer.append(d);
};
Athena.InstructionalText.prototype.createPreviewText = function(b, e, a) {
    var d, c;
    d = b;
    c = document.createElement("h3");
    c.innerHTML = d;
    if (a !== undefined) {
        c.setAttribute("data-objectid", a);
    }
    c.id = e;
    this.activityContainer.append(c);
};
Athena.InstructionalText.prototype.audio = function(h, d, i) {
    var g, a, f, b, c, e;
    b = this.audioClicked;
    c = null;
    f = 1;
    g = document.createElement("div");
    a = document.createElement("div");
    g.className = "play-btn";
    a.className = "audio-play";
    if (i !== undefined) {
        $(a).attr("data-objectid", i);
    }
    $(a).on("mousedown", function() {
        f++;
        if (!$(this).hasClass("audio-stop")) {
            $(".audio-stop").removeClass("audio-stop");
            $(this).addClass("audio-stop");
            c = "play";
        } else {
            $(this).removeClass("audio-stop");
            c = "stop";
        }
        if (b !== undefined) {
            b.call(this, c, h);
        }
    });
    g.appendChild(a);
    e = h.substr(0, h.lastIndexOf("."));
    g.id = e;
    this.activityContainer.append(g);
};
Athena.InstructionalText.prototype.completedAudioHandlers = function(a) {};
Athena.InstructionalText.prototype.lessonPerformanceTaskHeadFunction = function(b, e, a) {
    var d, c;
    d = b;
    c = document.createElement("div");
    c.className = "title";
    c.innerHTML = d;
    c.id = e;
    if (a !== undefined) {
        c.setAttribute("data-objectid", a);
    }
    this.activityContainer.append(c);
};
Athena.InstructionalText.prototype.subheadFunction = function(c, d, b) {
    var a;
    a = document.createElement("div");
    a.className = "sub-title";
    a.innerHTML = c;
    a.id = d;
    if (b !== undefined) {
        $(a).attr("data-objectid", b);
    }
    this.activityContainer.append(a);
};
Athena.InstructionalText.prototype.createGreenStrip = function(b, c) {
    var a;
    a = document.createElement("div");
    a.className = "greenStrip";
    a.id = c;
    this.activityContainer.append(a);
    if (b.objectId !== undefined) {
        $(a).attr("data-objectid", b.objectId);
    }
};
Athena.InstructionalText.prototype.createblock = function(b) {
    var a;
    a = document.createElement("div");
    a.className = "block";
    a.id = b;
    this.activityContainer.append(a);
};
Athena.InstructionalText.prototype.createVideo = function(e, c) {
    var f, i, d, b, h, g, a;
    b = null;
    h = null;
    g = null;
    a = null;
    f = document.createElement("div");
    f.className = "video-player";
    d = document.createElement("div");
    d.className = "video-container";
    if (e.playerId.objectId !== undefined) {
        d.setAttribute("data-objectid", e.playerId.objectId);
    }
    if (e.videoType.value === "legacy") {
        b = 400;
        h = 300;
        $(d).addClass("legacy");
    } else {
        if (e.videoType.value === "current") {
            if (this.deviceType === "mobile") {
                b = 280;
                h = 180;
                $(d).addClass("legacy");
            } else {
                b = 680;
                h = 383;
                $(d).addClass("current");
            }
        }
    }
    f.appendChild(d);
    this.activityContainer.append(f);
    this.videoCounter++;
    if (this.isOnline) {
        this["playerObj_" + this.videoCounter] = new Athena.BrightCovePlayer();
        this["playerObj_" + this.videoCounter].options({
            playerId: e.playerId.value,
            playerKey: e.playerKey.value,
            token: e.token.value,
            width: b,
            height: h,
            bgColour: "#ffffff",
            useButtons: false
        });
        a = f;
        this["playerObj_" + this.videoCounter].init("video-player-" + this.videoCounter, $(a), e.videoReference.value);
    } else {
        this["playerObj_" + this.videoCounter] = new Athena.JPlayer();
        this["playerObj_" + this.videoCounter].options({
            width: b,
            height: h,
            bgColour: "#ffffff",
            useButtons: false,
            jPlayerbuttons: true,
            videoDownloadDir: this.offlineSettings.videoDownloadDir,
            videoFileSuffix: this.offlineSettings.videoFileSuffix,
            captionFileSuffix: this.offlineSettings.captionFileSuffix,
            posterFileSuffix: this.offlineSettings.posterFileSuffix,
            ENCODE: true,
            pluginEvent: this.pluginEvent
        });
        a = f;
        this["playerObj_" + this.videoCounter].init("video-player-" + this.videoCounter, $(a), e.videoReference.value);
    }
};
Athena.InstructionalText.prototype.stopAllVideo = function() {
    var a;
    for (a in this) {
        if (this.hasOwnProperty(a)) {
            if (a.match(/playerObj_/g)) {
                this[a].stop();
            }
        }
    }
};
Athena.InstructionalText.prototype.create = function(d) {
    var e, c, f, a, g, b = this;
    f = null;
    d.completedAudioHandlers = function(h) {
        b.activityContainer.find("#" + h + " .audio-play").removeClass("audio-stop");
    };
    a = this.model.length;
    g = [];
    for (e in this.model.lists.values) {
        if (this.model.lists.values.hasOwnProperty(e)) {
            for (c in this.model.lists.values[e]) {
                if (this.model.lists.values[e].hasOwnProperty(c)) {
                    g.push(c);
                }
            }
        }
    }
    this.objectsId = g;
    for (e in this.model.lists.values) {
        if (this.model.lists.values.hasOwnProperty(e)) {
            for (c in this.model.lists.values[e]) {
                if (this.model.lists.values[e].hasOwnProperty(c)) {
                    switch (this.model.lists.values[e][c].param) {
                        case "preview_label":
                            this.createPreviewText(this.model.lists.values[e][c].value, c, this.model.lists.values[e][c].objectId);
                            break;
                        case "has_audio":
                            this.audio(this.model.lists.values[e][c].value, c, this.model.lists.values[e][c].objectId);
                            break;
                        case "lesson_performance_task_head":
                            this.lessonPerformanceTaskHeadFunction(this.model.lists.values[e][c].value, c, this.model.lists.values[e][c].objectId);
                            break;
                        case "subhead":
                            this.subheadFunction(this.model.lists.values[e][c].value, c, this.model.lists.values[e][c].objectId);
                            break;
                        case "running_text":
                            this.createText(this.model.lists.values[e][c].value, c, this.model.lists.values[e][c].objectId);
                            break;
                        case "datatable":
                            this.createTable(this.model.lists.values[e][c].value, this.tableHeadClass, this.tableRowClass, this.tableCellClass, this.model.lists.values[e][c].objectId);
                            break;
                        case "screenimage":
                            this.createScreenImage(this.model.lists.values[e], c, this.model.lists.values[e][c].objectId);
                            break;
                        case "techtextart":
                            f = this.model.lists.values[e][c].value;
                            if (String(f) === "typeA") {
                                this.createImageTextTypeA(this.model.lists.values[e], c);
                            } else {
                                this.createImageTextTypeB(this.model.lists.values[e], c);
                            }
                            break;
                        case "green_strip":
                            this.createGreenStrip(this.model.lists.values[e][c], c);
                            break;
                        case "block":
                            this.createblock(c);
                            break;
                        case "video":
                            this.createVideo(this.model.lists.values[e], c);
                            break;
                    }
                }
            }
        }
    }
    for (e in this.model.lists) {
        if (this.model.lists.hasOwnProperty(e)) {
            if (/datatable/.test(e)) {
                this.formDataTable(this.model.lists[e], e);
            }
        }
    }
    this.updateTableScrollbar();
    $(window).on("orientationchange", function(h) {
        setTimeout(function() {
            b.deviceType = b.checkDevices();
            b.updateTableScrollbar();
        }, 500);
    });
};
Athena.Accordion = function() {
    this.type = "slide";
    this.replaceClickedText = "View";
    this.originalClickedText = "Hide";
    this.changeContentType = "none";
    this.clickClass = "clickcontent";
    this.clickClickedClass = "clickedclass";
    this.revealClass = "slidecontent";
    this.modelContent = "";
    this.activityType = "";
    this.showOneAtTime = false;
    this.activityContainer = null;
    this.previousClickIndex = 0;
    this.currentClickIndex = 0;
    this.maxCount = 0;
    this.clickContainer = "";
    this.revealContainer = "";
    this.imageSourcePath = "assets/images/";
    this.elements = null;
    this.showInitialType = "none";
    this.tableClass = "";
    this.tableHeadClass = "";
    this.tableRowClass = "";
    this.tableCellClass = "";
    this.accordionFlag = true;
    this.pluginEvent = null;
    this.removeAptanaValidationError = 1;
    this.storage = null;
};
Athena.Accordion.extend(Athena.BaseClass);
Athena.Accordion.prototype.options = function(a) {
    var b;
    for (b in a) {
        if (a.hasOwnProperty(b)) {
            this[b] = a[b];
        }
    }
};
Athena.Accordion.prototype.removeLineBreakandTrim = function(a) {
    a = a.replace(new RegExp("\n", "gm"), "").trim();
    return a;
};
Athena.Accordion.prototype.setEvents = function() {
    var a;
    a = this;
    this.currentClickIndex = this.maxCount = this.activityContainer.find("." + this.clickClass).length;
    this.activityContainer.find("." + this.clickClass).parent().attr("opened", true);
    this.activityContainer.find("#clickelement0").removeAttr("opened");
    this.create();
    this.clickContainer.bind("click", function() {
        if (a.type === "slideOneByOne") {
            a.toggleOneByOne(this);
        } else {
            a.toggle(this);
        }
        if (Number(a.activityType) === 3) {
            if ($(this).parent().attr("opened")) {
                $(this).find(".arrow").addClass("uparrow");
                $(this).parent().removeAttr("opened");
            } else {
                $(this).find(".arrow").removeClass("uparrow");
                $(this).parent().attr("opened", true);
            }
        }
    });
};
Athena.Accordion.prototype.init = function() {
    var b, c, e, d, a;
    b = this;
    this.storage = {};
    this.storage.tabs = [];
    e = "false";
    if (Number(this.modelContent.lists.lists.length) === 1) {
        this.activityType = 3;
    }
    this.activityContainer.find(".question_container").html(this.modelContent.contents.question.value);
    if (this.modelContent.contents.question.value === "") {
        this.activityContainer.find(".question_container").hide();
    }
    if (this.modelContent.contents.question.objectId !== undefined) {
        this.activityContainer.find(".question_container").attr("data-objectid", this.modelContent.contents.question.objectId);
    }
    if (Number(this.activityType) === 1) {
        this.createContent(this.activityContainer, this.modelContent.lists.lists, "clickelement", "text", false, function(f) {
            this.elements = f;
        });
        this.loadContent(this.activityContainer.find(".textcontent"), this.modelContent.lists.lists, "text", false, true, "panelContent");
        this.loadContent(this.activityContainer.find(".clickcontent"), this.modelContent.lists.lists, "text", true, false, "label");
        this.loadImages(".imagecontent");
        this.loadDatatables(".imagecontent");
    } else {
        if (Number(this.activityType) === 2) {
            this.createContent(this.activityContainer.find(".accordian-container"), this.modelContent.lists.lists, "clickelement", "text", false, function(f) {
                this.elements = f;
            });
            this.loadContent(this.activityContainer.find(".textcontent"), this.modelContent.lists.lists, "text", false, true, "panelContent");
            this.loadContent(this.activityContainer.find(".clickcontent"), this.modelContent.lists.lists, "text", true, false, "label");
            this.loadImages(".imagecontent");
            this.loadDatatables(".imagecontent");
        } else {
            if (Number(this.activityType) === 3) {
                this.createContent(this.activityContainer.find(".accordian-container"), this.modelContent.lists.lists, "clickelement", "text", false, function(f) {
                    this.elements = f;
                });
                this.loadContent(this.activityContainer.find(".textcontent"), this.modelContent.lists.lists, "text", false, true, "panelContent");
                this.loadContent(this.activityContainer.find(".clickcontent"), this.modelContent.lists.lists, "text", true, false, "label");
                this.loadImages(".imagecontent");
                this.loadDatatables(".imagecontent");
            }
        }
    }
};
Athena.Accordion.prototype.create = function() {
    this.clickContainer = this.activityContainer.find("." + this.clickClass);
    this.revealContainer = this.activityContainer.find("." + this.revealClass);
};
Athena.Accordion.prototype.createContent = function(d, e, c, g, a, f) {
    var b;
    b = this;
    c = d.find("." + c);
    if (String((typeof f)) !== "undefined" || String(f) === "null") {
        this.cloneElement(e.length, d, c, c.attr("class"), "append", "", a, f);
    } else {
        this.cloneElement(e.length, d, c, c.attr("class"), "append", "", a, function(h) {
            b.loadContent(h, e, g);
        });
    }
    this.setEvents();
    if (typeof this.showInitialType === "number" && Number(e.length) !== 1) {
        setTimeout(function() {
            b.clickContainer.eq((b.showInitialType - 1)).toggleClass(b.clickClickedClass);
            b.revealContainer.eq((b.showInitialType - 1)).slideDown(1000);
            b.clickContainer.eq((b.showInitialType - 1)).find(".arrow").addClass("uparrow");
        }, 100);
    }
    if (this.showInitialType === "All") {
        setTimeout(function() {
            b.clickContainer.toggleClass(b.clickClickedClass);
            b.revealContainer.slideDown("slow");
        }, 100);
    }
};
Athena.Accordion.prototype.loadImages = function(a) {
    var b, c, e, d;
    if (typeof this.modelContent.lists.images !== "undefined") {
        for (b = 0; b < this.modelContent.lists.images.length; b++) {
            e = (this.modelContent.lists.images[b].position.value - 1);
            c = this.modelContent.lists.images[b].image.value;
            if (c.trim() !== "") {
                d = this.activityContainer.find(a).eq(e).find("img");
                d.attr("src", this.imageSourcePath + c);
                if (this.modelContent.lists.images[b].image.objectId !== undefined) {
                    d.attr("data-objectid", this.modelContent.lists.images[b].image.objectId);
                }
            } else {
                this.activityContainer.find(a).eq(e).find("img").remove();
            }
        }
    } else {
        this.activityContainer.find("img").each(function() {
            if (!$(this).hasClass("do_not_remove")) {
                $(this).remove();
            }
        });
    }
};
Athena.Accordion.prototype.loadDatatables = function(a) {
    var b, c, d;
    for (b in this.modelContent.lists) {
        if (this.modelContent.lists.hasOwnProperty(b)) {
            if (/datatable/.test(b)) {
                this.formDataTable(a, this.modelContent.lists[b], b);
            }
        }
    }
};
Athena.Accordion.prototype.formDataTable = function(b, e, a) {
    var f, c, d, k, l, m, h, g;
    d = [
        []
    ];
    k = 0;
    m = 0;
    for (f in e) {
        if (e.hasOwnProperty(f)) {
            if (k++ > 0) {
                l = 0;
                d[m] = [];
                for (c in e[f]) {
                    if (e[f].hasOwnProperty(c)) {
                        d[m][l++] = e[f][c];
                    }
                }
                m++;
            } else {
                h = e[f].position.value;
                g = e[f].header.value;
            }
        }
    }
    this.createTable(b, d, this.tableHeadClass, this.tableRowClass, this.tableCellClass, h - 1, a, g);
};
Athena.Accordion.prototype.createTable = function(o, h, m, g, e, w, f, p) {
    var q, n, l, r, d, u, a, s, v, t, b, c;
    r = document.createElement("table");
    r.className = "table";
    r.id = f;
    if (p !== "") {
        b = document.createElement("tr");
        c = document.createElement("th");
        c.setAttribute("colspan", h.length);
        c.innerHTML = p;
        b.appendChild(c);
        r.appendChild(b);
    }
    d = document.createElement("div");
    d.className = m;
    s = document.createElement("div");
    s.className = this.tableClass;
    for (n = 0; n < h[0].length; n++) {
        u = document.createElement("tr");
        u.className = g;
        for (l = 0; l < h.length; l++) {
            if (String(typeof h[l][n].ignorecell) === "undefined") {
                a = document.createElement("td");
                if (String(typeof h[l][n].colspan) !== "undefined") {
                    $(a).attr("colspan", h[l][n].colspan);
                    if (h[l][n].colspan.objectId !== undefined) {
                        $(a).attr("data-objectid", h[l][n].colspan.objectId);
                    }
                }
                if (String(typeof h[l][n].rowspan) !== "undefined") {
                    $(a).attr("rowspan", h[l][n].rowspan);
                    if (h[l][n].rowspan.objectId !== undefined) {
                        $(a).attr("data-objectid", h[l][n].rowspan.objectId);
                    }
                }
                a.className = e;
                a.innerHTML = h[l][n].value;
                if (h[l][n].objectId !== undefined) {
                    $(a).attr("data-objectid", h[l][n].objectId);
                }
                u.appendChild(a);
            }
        }
        if (n === 0) {
            d.appendChild(u);
            r.appendChild(d);
        } else {
            r.appendChild(u);
        }
    }
    s.innerHTML = r.outerHTML;
    this.activityContainer.find(o).eq(w).html(s.innerHTML);
};
Athena.Accordion.prototype.loadContent = function(e, f, j, c, g, b) {
    var d, h, a;
    for (d = 0; d < e.length; d++) {
        if (c) {
            h = e.eq(d).find("span");
        } else {
            h = e.eq(d);
        }
        if (d === 0) {
            a = 1;
        } else {
            a = 0;
        }
        h.html(f[d][b].value);
        this.storage.tabs[d] = {
            state: a
        };
        if (f[d][b].objectId !== undefined) {
            h.attr("data-objectid", f[d][b].objectId);
        }
    }
};
Athena.Accordion.prototype.update = function(g, c, b) {
    var d, f, a, h, e;
    d = false;
    e = c.find("span").length > 0 ? c.find("span") : c;
    if (this.changeContentType === "same") {
        f = e.html() === this.originalClickedText ? this.replaceClickedText : this.originalClickedText;
        e.html(f);
    } else {
        if (this.changeContentType === "different" && String(typeof this.replaceClickedText) === "object" && String(typeof this.originalClickedText) === "object") {
            f = e.html() === this.originalClickedText[g] ? this.replaceClickedText[g] : this.originalClickedText[g];
            e.html(f);
        }
    }
    if (String(b.css("display")) === "none" || String(b.css("opacity")) === "0") {
        d = false;
    } else {
        if (String(b.css("display")) === "block" || String(b.css("display")) === "table" || String(b.css("display")) === "inline" || String(b.css("opacity")) === "1") {
            d = true;
        }
    }
};
Athena.Accordion.prototype.toggleArrowState = function(d) {
    if (Number(this.activityType) === 2) {
        var c, b, a;
        a = this.clickContainer.find(".arrow");
        for (c = 0; c <= a.length; c++) {
            $(a[c]).removeClass("uparrow");
        }
        b = this.revealContainer.eq(d);
        if ($(b).is(":visible")) {
            $(a[d]).removeClass("uparrow");
        } else {
            $(a[d]).addClass("uparrow");
        }
    }
};
Athena.Accordion.prototype.toggle = function(f) {
    var c, b, e, a, d;
    c = $(f);
    a = this;
    e = this.clickContainer.index(f);
    a.toggleArrowState(e);
    b = this.revealContainer.eq(e);
    this.currentClickIndex = e;
    this.update(e, c, b);
    this.storage.showOneAtTime = this.showOneAtTime;
    if (this.showOneAtTime) {
        this.clickContainer.removeClass(this.clickClickedClass);
    }
    d = c.hasClass(this.clickClickedClass) ? 0 : 1;
    c.toggleClass(this.clickClickedClass);
    if (this.showOneAtTime && this.currentClickIndex !== this.previousClickIndex) {
        this.revealContainer.slideUp("slow");
        if (this.activityType === 2) {
            if (this.accordionFlag) {
                this.accordionFlag = false;
                if (e !== 0) {
                    b.slideDown("slow");
                }
            } else {
                b.slideDown("slow");
            }
        } else {
            b.slideDown("slow");
        }
        this.storage.tabs[this.currentClickIndex] = {
            state: 1
        };
        this.storage.tabs[this.previousClickIndex] = {
            state: 0
        };
    } else {
        this.storage.tabs[this.currentClickIndex] = {
            state: d
        };
        b.slideToggle("slow");
    }
    this.previousClickIndex = this.currentClickIndex;
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        this.pluginEvent(null, null, null, null, {
            id: this.activityContainer.attr("data-objectid"),
            value: this.storage
        });
    }
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        this.pluginEvent(null, null, "started", "performance", a.activityContainer.attr("data-objectid"));
    }
    this.reRenderMath(b.parent(), b.attr("class"));
};
Athena.Accordion.prototype.toggleOneByOne = function(d) {
    var a, b, c;
    a = $(d);
    b = this.clickContainer.index(a);
    c = this.revealContainer.eq(b);
    this.update(b, a, c);
    this.previousClickIndex = this.currentClickIndex;
    this.currentClickIndex = b;
    if (String(c.css("display")) === "none") {
        c.slideDown();
        this.clickContainer.eq((b + 1)).slideDown(1000);
    } else {
        this.slideContentOneByOne(b, this.maxCount - 1, true);
    }
};
Athena.Accordion.prototype.slideContentOneByOne = function(d, b, a) {
    var c, e, g, f;
    c = this;
    f = parseFloat(1000 / parseInt((this.previousClickIndex - this.currentClickIndex), 10), 10);
    e = (a) ? this.revealContainer.eq(b) : this.clickContainer.eq(b);
    g = (a) ? f : 0;
    e.slideUp(g, function() {
        if (!a) {
            b--;
            a = true;
        } else {
            a = false;
        }
        if (b > d) {
            c.slideContentOneByOne(d, b, a);
        } else {
            if (Number(b) === Number(d)) {
                c.revealContainer.eq(d).slideUp();
            }
        }
    });
};
Athena.Accordion.prototype.reset = function() {
    var c, b, a;
    b = this.activityContainer.find(this.clickContainer).size();
    if (this.showOneAtTime === true) {
        c = this.revealContainer.eq(0).css("display");
        if (c !== "block") {
            for (a = 1; a < b; a++) {
                this.revealContainer.eq(a).slideUp(1000);
                this.clickContainer.eq(a).find(".arrow").removeClass("uparrow");
            }
            this.revealContainer.eq(0).slideDown(1000);
            this.clickContainer.eq(0).find(".arrow").addClass("uparrow");
        }
    } else {
        for (a = 1; a < b; a++) {
            this.revealContainer.eq(a).slideUp(1000);
            this.clickContainer.eq(a).find(".arrow").removeClass("uparrow");
        }
        this.revealContainer.eq(0).slideDown(1000);
        this.clickContainer.eq(0).find(".arrow").addClass("uparrow");
    }
};
Athena.Accordion.prototype.preset = function(g) {
    var d, f, e = this.activityContainer.find(this.clickContainer).size(),
        b, h, c, a;
    h = g.value.showOneAtTime;
    c = g.value.tabs;
    if (typeof h !== "undefined") {
        this.storage = g.value;
        if (h) {
            for (d = 0; d < c.length; d++) {
                this.revealContainer.eq(d).slideUp(1000);
                this.clickContainer.eq(d).find(".arrow").removeClass("uparrow");
                this.clickContainer.eq(d).removeClass(this.clickClickedClass);
                if (c[d].state === "1") {
                    b = d;
                }
            }
            this.revealContainer.eq(b).slideDown(1000);
            this.clickContainer.eq(b).find(".arrow").addClass("uparrow");
            a = this.revealContainer.eq(b);
            this.reRenderMath(a.parent(), a.attr("class"));
        } else {
            for (d = 0; d < c.length; d++) {
                if (c[d].state === 1) {
                    this.revealContainer.eq(d).slideDown(1000);
                    this.clickContainer.eq(d).addClass(this.clickClickedClass);
                    this.clickContainer.eq(d).find(".arrow").addClass("uparrow");
                    this.clickContainer.eq(d).parent().removeAttr("opened");
                    a = this.revealContainer.eq(d);
                    this.reRenderMath(a.parent(), a.attr("class"));
                } else {
                    this.revealContainer.eq(d).slideUp(1000);
                    this.clickContainer.eq(d).removeClass(this.clickClickedClass);
                    this.clickContainer.eq(d).find(".arrow").removeClass("uparrow");
                    this.clickContainer.eq(d).parent().attr("opened", true);
                }
            }
        }
    }
};
Athena.Accordion.prototype.reRenderMath = function(a, b) {
    if (typeof MathJax !== "undefined") {
        MathJax.Hub.Queue(["Reprocess", MathJax.Hub, a.toArray()[0].querySelector("." + b)], function() {
            var c;
        });
    }
};
Athena.BrightCovePlayer = function() {
    this.id = null;
    this.playerContainer = null;
    this.player = null;
    this.videoPlayer = null;
    this.experienceId = null;
    this.playerKey = null;
    this.playerId = null;
    this.width = 0;
    this.height = 0;
    this.bgColour = "#ffffff";
    this.token = null;
    this.useButtons = false;
    this.videoId = null;
    this.pluginEvent = null;
    this.objIdParent = null;
    this.isDevice = null;
};
Athena.BrightCovePlayer.prototype.options = function(a) {
    var b;
    for (b in a) {
        if (a.hasOwnProperty(b)) {
            this[b] = a[b];
        }
    }
};
Athena.BrightCovePlayer.prototype.init = function(f, d, e, a) {
    var c, b;
    c = this;
    this.playerContainer = d;
    this.experienceId = f;
    this.videoId = e;
    b = d.find(".video-container");
    b.width(this.width);
    if (a !== undefined) {
        b.attr("data-objectid", a);
    }
    b.html(this.create(f, this.bgColour, this.width, this.height, this.playerId, this.playerKey, true, e));
    if (this.useButtons) {
        d.find("nav").addClass("active");
        this.setEvents();
    }
    if ((((String(navigator.userAgent.match(/iPhone/i)) === "iPhone") || (String(navigator.userAgent.match(/Android/i)) === "Android")) && (String(navigator.userAgent.match(/Mobile/i)) === "Mobile")) && (String(navigator.userAgent.match(/iPad/i)) !== "iPad")) {
        setTimeout(function() {
            c.setVideoPlayerDimension();
        }, 500);
    }
    this.isDevice = new RegExp("ios|ipad|iphone|ipod|android", "ig").test(navigator.userAgent);
};
Athena.BrightCovePlayer.prototype.setVideoPlayerDimension = function() {
    var a;
    a = Number(this.width / this.height) * 88;
    this.playerContainer.find(".BrightcoveExperience").css("height", a + "px");
    this.playerContainer.find(".BrightcoveExperience").css("width", this.playerContainer.find(".video-container").width() - 5 + "px");
    this.playerContainer.css("margin", "0 auto");
};
Athena.BrightCovePlayer.prototype.create = function(a, e, b, i, d, c, h, f) {
    this.id = a;
    var g = '<object id="' + a + '" class="BrightcoveExperience">';
    g += '<param name="bgcolor" value="' + e + '" />';
    g += '<param name="width" value="' + b + '" />';
    g += '<param name="height" value="' + i + '" />';
    g += '<param name="playerID" value="' + d + '" />';
    g += '<param name="playerKey" value="' + c + '" />';
    g += '<param name="isVid" value="true" />';
    g += '<param name="isUI" value="true" />';
    g += '<param name="wmode" value="transparent" /> ';
    g += '<param name="dynamicStreaming" value="' + h + '" />';
    if (document.location.protocol === "https:") {
        g += '<param name="secureConnections" value="true" /><param name="secureHTMLConnections" value="true" />';
    }
    g += '<param name="seamlessTabbing" value="false" />';
    g += '<param name="@videoPlayer" value="ref:' + f + '" />';
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        g += '<param name="includeAPI" value="true" />';
        g += '<param name="templateLoadHandler" value="' + this.pluginEvent + '" />';
        g += '<param name="templateReadyHandler" value="' + this.pluginEvent + 'Ready" />';
    }
    g += "</object>";
    if (this.isDevice) {
        g += '<script type="text/javascript">brightcove.api.createExperiences();<\/script>';
    } else {
        g += '<script type="text/javascript">brightcove.createExperiences();<\/script>';
    }
    return g;
};
Athena.BrightCovePlayer.prototype.destroy = function(b) {
    var a = (b !== undefined) ? document.getElementById(b) : document.getElementById(this.experienceId);
    this.playerContainer.find("nav").removeClass("active");
    this.clearEvents();
    if (a) {
        a.parentNode.removeChild(a);
    }
};
Athena.BrightCovePlayer.prototype.pause = function(a) {
    if (this.isDevice) {
        this.player = (a !== undefined) ? brightcove.api.getExperience(a) : brightcove.api.getExperience(this.experienceId);
    } else {
        this.player = (a !== undefined) ? brightcove.getExperience(a) : brightcove.getExperience(this.experienceId);
    }
    if (this.player) {
        this.videoPlayer = this.player.getModule(APIModules.VIDEO_PLAYER);
        if (this.videoPlayer.isPlaying()) {
            this.videoPlayer.pause(true);
        } else {
            this.videoPlayer.pause(false);
        }
    }
};
Athena.BrightCovePlayer.prototype.isPlaying = function(b) {
    var a = false;
    if (this.isDevice) {
        this.player = (b !== undefined) ? brightcove.api.getExperience(b) : brightcove.api.getExperience(this.experienceId);
    } else {
        this.player = (b !== undefined) ? brightcove.getExperience(b) : brightcove.getExperience(this.experienceId);
    }
    if (this.player) {
        this.videoPlayer = this.player.getModule(APIModules.VIDEO_PLAYER);
        if (this.videoPlayer.isPlaying()) {
            a = true;
        } else {
            a = false;
        }
    }
    return a;
};
Athena.BrightCovePlayer.prototype.update = function(b, a) {
    if (this.isDevice) {
        this.player = (b !== undefined) ? brightcove.api.getExperience(b) : brightcove.api.getExperience(this.experienceId);
    } else {
        this.player = (b !== undefined) ? brightcove.getExperience(b) : brightcove.getExperience(this.experienceId);
    }
    this.videoPlayer = this.player.getModule(APIModules.VIDEO_PLAYER);
};
Athena.BrightCovePlayer.prototype.stop = function(a) {
    if (this.isDevice) {
        this.player = (a !== undefined) ? brightcove.api.getExperience(a) : brightcove.api.getExperience(this.experienceId);
    } else {
        this.player = (a !== undefined) ? brightcove.getExperience(a) : brightcove.getExperience(this.experienceId);
    }
    if (this.player) {
        this.videoPlayer = this.player.getModule(APIModules.VIDEO_PLAYER);
        if (this.isDevice) {
            this.videoPlayer.pause(true);
            this.videoPlayer.seek(0);
        } else {
            this.videoPlayer.stop();
        }
    }
};
Athena.BrightCovePlayer.prototype.play = function(a) {
    if (this.isDevice) {
        this.player = (a !== undefined) ? brightcove.api.getExperience(a) : brightcove.api.getExperience(this.experienceId);
    } else {
        this.player = (a !== undefined) ? brightcove.getExperience(a) : brightcove.getExperience(this.experienceId);
    }
    if (this.player) {
        this.videoPlayer = this.player.getModule(APIModules.VIDEO_PLAYER);
        this.videoPlayer.play();
    }
};
Athena.BrightCovePlayer.prototype.seek = function(b, a) {
    if (this.isDevice) {
        this.player = (b !== undefined) ? brightcove.api.getExperience(b) : brightcove.api.getExperience(this.experienceId);
    } else {
        this.player = (b !== undefined) ? brightcove.getExperience(b) : brightcove.getExperience(this.experienceId);
    }
    if (this.player) {
        this.videoPlayer = this.player.getModule(APIModules.VIDEO_PLAYER);
        this.videoPlayer.seek(a);
    }
};
Athena.BrightCovePlayer.prototype.property = function(d, b) {
    var c, a = "http://api.brightcove.com/services/library?command=find_video_by_id&video_id=" + d + "&video_fields=name,length&token=" + b;
    $.ajax({
        type: "GET",
        url: a,
        dataType: "jsonp",
        crossDomain: true,
        processData: true,
        success: function(e) {
            c = e;
        },
        error: function() {
            c = "error";
        }
    });
    return c;
};
Athena.BrightCovePlayer.prototype.setEvents = function() {
    var a = this;
    this.playerContainer.find(".player-button").click(function() {
        var b = $(this).attr("class").split(" ");
        if (b[0] === "button-play") {
            a.play(a.experienceId);
        }
        if (b[0] === "button-stop") {
            a.stop(a.experienceId);
        }
        if (b[0] === "button-pause") {
            a.pause(a.experienceId);
        }
        if (b[0] === "button-restart") {
            a.restart(a.experienceId);
        }
        if (b[0] === "button-seek") {
            a.seek(a.experienceId, Number(this.playerContainer.find(".seek-value").val()));
        }
    });
};
Athena.BrightCovePlayer.prototype.clearEvents = function() {
    this.playerContainer.find(".player-button").unbind("click");
};
Athena.BrightCovePlayer.prototype.reset = function() {
    this.stop();
};
Athena.JPlayer = function() {
    this.id = null;
    this.playerContainer = null;
    this.jplayer = null;
    this.player = null;
    this.videoContainer = null;
    this.videoPlayer = null;
    this.experienceId = null;
    this.playerKey = null;
    this.playerId = null;
    this.width = 0;
    this.height = 0;
    this.bgColour = "#ffffff";
    this.token = null;
    this.useButtons = false;
    this.jPlayerbuttons = true;
    this.videoId = null;
    this.pluginEvent = null;
    this.objIdParent = null;
    this.videoDownloadDir = null;
    this.videoFileSuffix = null;
    this.captionFileSuffix = null;
    this.posterFileSuffix = null;
    this.comingSoonText = null;
    this.noFlashPlayer = null;
    this.ENCODE = false;
};
Athena.JPlayer.prototype.options = function(a) {
    var b;
    for (b in a) {
        if (a.hasOwnProperty(b)) {
            this[b] = a[b];
        }
    }
};
Athena.JPlayer.prototype.init = function(d, b, c, a) {
    this.id = d;
    this.containerId = "jp_container_" + this.id;
    this.playerId = "jquery_player_" + this.id;
    this.playerContainer = b;
    this.videoId = c;
    this.videoContainer = b.find(".video-container");
    this.videoContainer.width(this.width);
    this.videoContainer.height(this.height);
    if (a !== undefined) {
        this.videoContainer.attr("data-objectid", a);
    }
    if (this.ENCODE) {
        this.load("video");
    } else {
        this.load("caption");
    }
};
Athena.JPlayer.prototype.base64ToBlob = function(c) {
    var g = atob(c),
        f = g.length,
        b = new ArrayBuffer(f),
        a = new Uint8Array(b),
        e, d;
    for (e = 0; e < f; e++) {
        a[e] = g.charCodeAt(e);
    }
    d = new Blob([a]);
    return d;
};
Athena.JPlayer.prototype.load = function(f) {
    var d = this,
        g = this.videoDownloadDir + this.videoId,
        b = null,
        a, e, c;
    a = function(i, h) {
        switch (h) {
            case "video":
                i = i + d.videoFileSuffix;
                c = "text";
                break;
            case "caption":
                i = i + d.captionFileSuffix;
                c = "xml";
                break;
            case "poster":
                i = i + d.posterFileSuffix;
                c = "text";
                break;
        }
        $.ajax({
            url: i,
            dataType: c,
            success: function(j) {
                e(j, h);
            },
            error: function(j) {
                e(null, h);
            }
        });
    };
    e = function(j, i) {
        alert(j);
        var h;
        if (i === "video") {
            h = "poster";
        } else {
            if (i === "poster") {
                h = "caption";
            } else {
                h = null;
            }
        }
        if (j) {
            if (i === "caption") {
                d["blob" + i] = j;
            } else {
                d["blob" + i] = d.base64ToBlob(j);
            }
        }
        if ((i === "video" && j === null)) {
            d.videoContainer.html(d.markup(true));
        } else {
            if (h) {
                a(g, h);
            } else {
                d.onVideoDataLoaded();
            }
        }
    };
    a(g, f);
};
Athena.JPlayer.prototype.onVideoDataLoaded = function() {
    var a = this,
        g = "",
        f = "",
        b = "",
        c = false;
    try {
        if (this.ENCODE) {
            if (this.blobvideo !== undefined) {
                f = window.URL.createObjectURL(this.blobvideo);
            } else {
                this.destroy();
                return;
            }
            if (this.blobposter !== undefined) {
                b = window.URL.createObjectURL(this.blobposter);
            }
            if (this.blobcaption !== undefined) {
                g = this.blobcaption;
            }
        } else {
            f = this.videoDownloadDir + this.videoId + ".mp4";
            b = this.videoDownloadDir + this.videoId + ".jpg";
            g = this.blobcaption;
            c = true;
        }
    } catch (d) {
        this.destroy();
        return;
    }
    if (c) {
        $.ajax({
            url: f,
            dataType: "text",
            success: function(e) {
                a.formatCaptions(g, f, b);
            },
            error: function(h) {
                a.videoContainer.html(a.markup(true));
            }
        });
    } else {
        this.formatCaptions(g, f, b);
    }
};
Athena.JPlayer.prototype.formatCaptions = function(g, f, d) {
    if (g === null || g === "") {
        this.create(f, [], d);
        return;
    }
    var e, a, b, c;
    b = $(g).find("p");
    a = b.length;
    c = [];
    for (e = 0; e < a; e++) {
        c.push({
            start: $(b[e]).attr("begin"),
            end: $(b[e]).attr("end"),
            text: b[e].innerHTML
        });
    }
    this.create(f, c, d);
};
Athena.JPlayer.prototype.create = function(f, d, c) {
    var b = this,
        e, a = this.markup(false);
    this.videoContainer.html(a);
    e = Popcorn.jplayer("#" + this.playerId, {
        media: {
            m4v: f
        },
        options: {
            ready: function() {
                if (c && c !== undefined) {
                    $("#" + b.playerId).find("img").attr("src", c);
                }
            },
            play: function() {
                if (b.pluginEvent === "Athena.onVideoLoaded") {
                    Athena.trackingEventHandler(Athena.VIDEO_PLAY);
                }
            },
            ended: function() {
                if (b.pluginEvent === "Athena.onVideoLoaded") {
                    Athena.trackingEventHandler(Athena.VIDEO_COMPLETE);
                }
            },
            supplied: "m4v",
            backgroundColor: this.bgColour,
            size: {
                width: this.width + "px",
                height: this.height + "px",
                cssClass: "jp-video-flexi"
            },
            swfPath: "css",
            cssSelectorAncestor: "#" + this.containerId,
            smoothPlayBar: true,
            keyEnabled: false
        }
    });
    this.jplayer = $("#" + this.playerId);
};
Athena.JPlayer.prototype.markup = function(a) {
    var b;
    if (a) {
        b = '<div id="videoError" style="display: table;height:100%;width:100%;background-color: #000000;color: #ffffff;text-align: center;overflow: hidden">';
        b += ' <div style="display: table-cell; vertical-align: middle;">';
        b += "<div>";
        b += this.comingSoonText;
        b += "</div>";
        b += "</div>";
        b += "</div>";
    } else {
        b = '<div id="' + this.containerId + '" class="jp-video">';
        b += '<div class="jp-type-single" style="position:relative;">';
        b += '<div id="' + this.playerId + '" class="jp-jplayer"></div>';
        if (this.jPlayerbuttons) {
            b += '<div class="jp-gui">';
            b += '<div class="jp-video-play">';
            b += '<a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>';
            b += "</div>";
            b += '<div class="jp-interface">';
            b += '<div class="jp-progress">';
            b += '<div class="jp-seek-bar">';
            b += '<div class="jp-play-bar"></div>';
            b += "</div>";
            b += "</div>";
            b += '<div class="jp-current-time"></div>';
            b += '<div class="jp-duration"></div>';
            b += '<div class="jp-controls-holder">';
            b += '<ul class="jp-controls">';
            b += "<li>";
            b += '<a href="javascript:;" class="jp-play" tabindex="1">play</a>';
            b += "</li>";
            b += "<li>";
            b += '<a href="javascript:;" class="jp-pause" tabindex="1">pause</a>';
            b += "</li>";
            b += "<li>";
            b += '<a href="javascript:;" class="jp-stop" tabindex="1">stop</a>';
            b += "</li>";
            b += "<li>";
            b += '<a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a>';
            b += "</li>";
            b += "<li>";
            b += '<a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a>';
            b += "</li>";
            b += "<li>";
            b += '<a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a>';
            b += "</li>";
            b += "</ul>";
            b += '<div class="jp-volume-bar">';
            b += '<div class="jp-volume-bar-value"></div>';
            b += "</div>";
            b += '<ul class="jp-toggles">';
            b += "<li>";
            b += '<a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen">full screen</a>';
            b += "</li>";
            b += "<li>";
            b += '<a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a>';
            b += "</li>";
            b += "</ul>";
            b += "</div>";
            b += "</div>";
            b += "</div>";
        }
        b += '<div class="jp-no-solution">';
        b += "<span>Update Required</span>";
        b += this.noFlashPlayer;
        b += "</div>";
        b += "</div>";
        b += "</div>";
    }
    return b;
};
Athena.JPlayer.prototype.setEvents = function() {
    var a = this;
    this.playerContainer.find(".player-button").click(function() {
        var b = $(this).attr("class").split(" ");
        if (b[0] === "button-play") {
            a.play();
        }
        if (b[0] === "button-stop") {
            a.stop();
        }
        if (b[0] === "button-pause") {
            a.pause();
        }
        if (b[0] === "button-restart") {
            a.restart();
        }
        if (b[0] === "button-seek") {
            a.seek(Number(this.playerContainer.find(".seek-value").val()));
        }
    });
};
Athena.JPlayer.prototype.clearEvents = function() {
    this.playerContainer.find(".player-button").unbind("click");
};
Athena.JPlayer.prototype.destroy = function(a) {
    this.clearEvents();
};
Athena.JPlayer.prototype.pause = function(a) {
    if (this.jplayer) {
        this.jplayer.jPlayer("pause");
    }
};
Athena.JPlayer.prototype.isPlaying = function(a) {
    if (this.jplayer && !this.jplayer.data().jPlayer.status.paused) {
        return true;
    }
    return false;
};
Athena.JPlayer.prototype.update = function(a) {
    this.destory();
    this.init(this.playerContainer, a, this.bgColour, this.width, this.height);
};
Athena.JPlayer.prototype.stop = function() {
    if (this.jplayer) {
        this.jplayer.jPlayer("stop");
    }
};
Athena.JPlayer.prototype.play = function() {
    if (this.jplayer) {
        this.jplayer.jPlayer("play");
    }
};
Athena.JPlayer.prototype.seek = function(a) {
    if (this.jplayer) {
        this.jplayer.jPlayer("play", a);
    }
};
Athena.JPlayer.prototype.restart = function() {
    if (this.jplayer) {
        this.jplayer.jPlayer("stop", 0);
    }
};
Athena.JPlayer.prototype.reset = function() {
    this.restart(this.id);
};
Athena.DragDrop = function() {
    this.type = "text";
    this.revertDuration = 300;
    this.reduceSize = 15;
    this.activityType = "order";
    this.attemptNo = 0;
    this.noOfAttempts = 2;
    this.checkbutton = null;
    this.dndType = "";
    this.contentType = "";
    this.myAnswers = null;
    this.correctAnswers = null;
    this.startDivHeight = "";
    this.modelData = "";
    this.correctClass = "correct-answer";
    this.incorrectClass = "wrong-answer";
    this.checkClass = "check-box";
    this.resetClass = "reset-box";
    this.enableClass = "active";
    this.showMeClass = "showmeClass";
    this.myAnswerClass = "correctAnswerClass";
    this.myanswerCorrectCls = "default-correct";
    this.afterDropClass = "after-drop";
    this.dragClass = "drag";
    this.dropClass = "drop";
    this.checkText = "Check";
    this.resetText = "Reset";
    this.tryagainText = "Try Again";
    this.showMeText = "Show Me";
    this.myAnswerText = "My Answer";
    this.incorrectMsg = "That's not it";
    this.hintMsg = "";
    this.showmeMsg = "Correct answer is shown";
    this.partialMessage = "You have partially answered.";
    this.successMsg = "Correct!";
    this.failMsg = "That's not it";
    this.dragContainer = "drag-container";
    this.dropContainer = "drop-container";
    this.feedbackContainer = "feedback_container";
    this.hintContainer = "hint_container";
    this.questionContainer = "question_container";
    this.parentContainer = null;
    this.dropClonable = true;
    this.toggleAnswers = true;
    this.resetAll = true;
    this.partiallyAnswered = false;
    this.allowMultiple = false;
    this.isMultipleCopies = false;
    this.dropinMath = false;
    this.showNumberInTable = false;
    this.reduceFontSize = false;
    this.imageSourcePath = "assets/images/";
    this.storedThis = undefined;
    this.randomArray = undefined;
    this.storage = {};
    this.deviceType = "";
    this.removeAptanaValidationError = 1;
    this.offsetPaddingAdj = 20;
    var a;
    this.validOptionCount = 0;
    this.order_unique = false;
    this.resetButton = false;
};
Athena.DragDrop.extend(Athena.BaseClass);
Athena.DragDrop.prototype.persistData = function() {
    var a = [],
        c, b, d;
    b = this;
    d = this.dropContainer.find("." + this.dropClass);
    if (b.storage === undefined) {
        b.storage = {};
    }
    if (String(this.dndType) === "dds" && Number(this.contentType) === 1) {
        d = this.parentContainer.find("." + this.dropClass);
    } else {
        if (this.dndType === "ddo-ole") {
            d = this.parentContainer.find("." + this.dropClass + ".ui-droppable");
        }
    }
    if (b.storage.attemptOver === undefined) {
        d.each(function() {
            c = [];
            if (String(b.activityType) === "group") {
                if ($(this).find("." + b.dragClass).length > 0) {
                    $(this).find("." + b.dragClass).each(function() {
                        c.push(Number($(this).data("actual-order")));
                    });
                    a.push("[" + c + "]");
                    a.push("delimit");
                } else {
                    a.push(-1);
                }
            } else {
                if ($(this).find("." + b.dragClass).length > 0) {
                    a.push(Number($(this).find("." + b.dragClass).eq(0).data("actual-order")));
                } else {
                    a.push(-1);
                }
            }
        });
        this.storage.draginDropActualOrder = a;
    }
    this.storage.randomArray = this.randomArray;
    this.storage.attemptNo = this.attemptNo;
    if ((b.pluginEvent !== undefined) && b.pluginEvent !== null) {
        b.pluginEvent(null, null, null, null, {
            id: b.parentContainer.attr("data-objectid"),
            value: b.storage
        });
    }
};
Athena.DragDrop.prototype.assignPersistedDataforMathMl = function(i) {
    var g, f, b, d, c, h, e, a;
    g = this;
    h = [];
    e = -1;
    if (String(g.activityType) === "order") {
        h = String(g.storage.draginDropActualOrder).split(",");
        i.each(function() {
            e++;
            if (Number(h[e]) !== -1) {
                d = g.dragContainer.find("." + g.dragClass).eq(h[e]).data(g.activityType);
                b = g.dragContainer.find("." + g.dragClass).eq(h[e]).data("actual-order");
                c = g.dragContainer.find("." + g.dragClass + ":eq(" + h[e] + ")").clone();
                c.data(g.activityType, d);
                c.data("actual-order", b);
                c.appendTo($(this));
                g.dragContainer.find("." + g.dragClass).eq(h[e]).addClass("disableDrag").html("&nbsp;");
            }
        });
    }
    g.validateAnswer(g.dropContainer, "." + g.dragClass, g.correctClass, g.incorrectClass, g.activityType);
    g.postPreset();
};
Athena.DragDrop.prototype.preset = function(e) {
    var m, c, k, g, f, d, n, a, h, o, b, l;
    m = this;
    n = [];
    h = -1;
    m.storage = e.value;
    if (m.storage === undefined) {
        m.storage = {};
    }
    if (m.storage !== undefined && m.storage.attemptNo !== undefined) {
        o = this.dropContainer.find("." + this.dropClass);
        if (String(this.dndType) === "dds" && Number(this.contentType) === 1) {
            o = this.parentContainer.find("." + this.dropClass);
        } else {
            if (this.dndType === "ddo-ole") {
                o = this.parentContainer.find("." + this.dropClass + ".ui-droppable");
                setTimeout(function() {
                    o = m.parentContainer.find("." + m.dropClass + ".ui-droppable");
                    m.assignPersistedDataforMathMl(o);
                }, 2000);
            }
        }
        if ((this.dndType !== "ddo-ole")) {
            if (String(m.activityType) === "order") {
                n = String(this.storage.draginDropActualOrder).split(",");
                o.each(function() {
                    h++;
                    if (Number(n[h]) !== -1) {
                        k = m.dragContainer.find("." + m.dragClass).eq(n[h]).data(m.activityType);
                        c = m.dragContainer.find("." + m.dragClass).eq(n[h]).data("actual-order");
                        g = m.dragContainer.find("." + m.dragClass + ":eq(" + n[h] + ")").clone();
                        g.data(m.activityType, k);
                        g.data("actual-order", c);
                        g.appendTo($(this));
                        m.dragContainer.find("." + m.dragClass).eq(n[h]).addClass("disableDrag").html("&nbsp;");
                    }
                });
            } else {
                n = String(this.storage.draginDropActualOrder).split(",delimit,");
                for (f = 0; f < n.length; f++) {
                    if (String(n[f]) !== "-1") {
                        a = String(n[f]).replace("]", "").replace("[", "").split(",");
                        b = o.eq(f);
                        for (d = 0; d < a.length; d++) {
                            k = m.dragContainer.find("." + m.dragClass).eq(a[d]).data(m.activityType);
                            c = m.dragContainer.find("." + m.dragClass).eq(a[d]).data("actual-order");
                            g = m.dragContainer.find("." + m.dragClass + ":eq(" + a[d] + ")").clone();
                            g.data(m.activityType, k);
                            g.data("actual-order", c);
                            g.appendTo(b);
                            m.dragContainer.find("." + m.dragClass).eq(a[d]).addClass("disableDrag").html("&nbsp;");
                        }
                    }
                }
            }
            m.validateAnswer(m.dropContainer, "." + m.dragClass, m.correctClass, m.incorrectClass, m.activityType);
            m.postPreset();
        }
    }
};
Athena.DragDrop.prototype.postPreset = function() {
    var a = this,
        b;
    this.attemptNo = this.storage.attemptNo;
    if (this.storage.checkButtonText !== undefined) {
        if (String(this.storage.checkButtonText) === this.checkText) {
            this.checkbutton.addClass(this.enableClass);
        } else {
            if (String(this.storage.checkButtonText) === this.tryagainText) {
                this.checkbutton.trigger("click");
                this.attemptNo = 1;
                this.attemptNo = this.storage.attemptNo = 1;
            }
        }
    }
    if (this.storage.attemptOver !== undefined || (String(this.storage.checkButtonText) === this.checkText && Number(this.storage.attemptNo) !== 0)) {
        this.checkbutton.addClass(this.enableClass);
        this.hintContainer.hide();
        this.checkbutton.find("span").html(this.showMeText);
        this.checkbutton.addClass(this.showMeClass);
        if (this.storage.checkButtonText !== undefined && String(this.storage.checkButtonText) === this.showMeText) {
            this.checkbutton.find("span").html(this.myAnswerText);
            this.checkbutton.addClass(this.myAnswerClass);
            this.toggle(this.checkbutton);
        }
    }
};
Athena.DragDrop.prototype.reset = function() {
    this.attemptNo = 0;
    this.hintContainer.hide();
    this.feedbackContainer.removeClass("hasText").find("span").html("");
    this.dragContainer.html('<div class="drag"><span class="ddspan"> </span></div>');
    if (this.dndType === "ddo-osp") {
        if (String(this.modelData.contents.contentType.value) === "1") {
            this.dropContainer.html('<div class="drop" xmlns="http://www.w3.org/1999/xhtml"> </div>');
            this.parentContainer.find(".index-container").html('<span class="index-text"> </span>');
        } else {
            this.parentContainer.find(".prooftable").html('<tr class="proofrow"><td class="prooflabel"> </td><td class="proofcontent"> </td><td class="prooflabel"> </td><td class="proofcontent"> </td></tr>');
        }
    } else {
        if (this.dndType === "dds") {
            if (String(this.modelData.contents.type.value) === "dds-sfg") {
                this.dragContainer.html('<div class="drag"><img src=""/></div>');
                this.dropContainer.html('<div class="drop"><label> </label></div>');
            } else {
                if (String(this.modelData.contents.type.value) === "dds-sp") {
                    this.dragContainer.html('<div class="drag"><span class="ddspan"> </span></div>');
                    this.dropContainer.parent().html('<div class="drop"><label> </label><div class="inner-container"></div></div>');
                } else {
                    this.dragContainer.html('<div class="drag"><span class="ddspan"> </span></div>');
                    this.dropContainer.parent().html('<div class="drop" xmlns="http://www.w3.org/1999/xhtml"><label> </label><div class="inner-container"></div></div>');
                }
            }
        } else {
            this.dropContainer.html("");
        }
    }
    this.create();
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        this.pluginEvent(null, null, null, null, {
            id: this.modelData.objectId,
            value: undefined
        });
    }
    if (typeof this.storage !== "undefined") {
        delete this.storage.attemptOver;
        delete this.storage.secondCheck;
        delete this.storage.draginDropActualOrder;
    }
};
Athena.DragDrop.prototype.calculateMaximumHeight = function(k) {
    var b, c, h, d, j, f, g, e, a;
    d = document.getElementsByTagName("body");
    d[0].style.fontFamily = '"MyriadPro Regular", Helvetica, sans-serif';
    h = 0;
    f = this.parentContainer.find("." + this.dragClass).eq(0).width() + "px";
    g = this.parentContainer.find("." + this.dragClass).css("font-size");
    e = this.parentContainer.find("." + this.dragClass).css("line-height");
    for (c = 0; c < k.length; c++) {
        j = document.createElement("div");
        b = document.createElement("span");
        b.className = "heightCalculation";
        j.style.width = f;
        j.style.height = "auto";
        j.style.fontSize = g;
        j.style.lineHeight = e;
        a = k[c].option.value;
        if (this.reduceFontSize) {
            a = this.addMathSizeAttributeinMathML(a, this.reduceSize + "px");
        } else {
            a = this.addMathSizeAttributeinMathML(a);
        }
        b.innerHTML = a;
        j.appendChild(b);
        d[0].appendChild(j);
        if (h < $(".heightCalculation").eq(0).height()) {
            h = $(".heightCalculation").eq(0).height();
        }
        j.outerHTML = "";
    }
    return h + 20;
};
Athena.DragDrop.prototype.checkNumberOfColoumn = function(e, j, b) {
    var f, d, c, g, h, a, i;
    g = Number(Number(e * b) + Number(e * 42));
    f = 0;
    if (g > j) {
        e = Number(e / 2);
        this.checkNumberOfColoumn(e, j, b);
    } else {
        d = Number(e) * Number(b + 42);
        c = Number(this.modelData.lists.options.length) * Number(b + 42);
        h = true;
        g = Number(Number(e * b) + Number(e * 42));
        this.dragContainer.find("." + this.dragClass).parent().css("width", d + "px");
        a = Number(parseInt(this.parentContainer.find(".content").css("width"), 10) / 2) - 20;
        i = parseInt(this.dragContainer.find("." + this.dragClass).parent().css("width"), 10) / 2;
        this.dragContainer.find("." + this.dragClass).parent().css("margin-left", "50%");
        if (!h) {
            f = parseInt(this.dragContainer.find("." + this.dragClass).parent().css("margin-left"), 10) - Number(c / 2);
        } else {
            f = parseInt(this.dragContainer.find("." + this.dragClass).parent().css("margin-left"), 10) - Number(d / 2);
        }
        this.dragContainer.find("." + this.dragClass).parent().css("margin-left", f + "px");
    }
};
Athena.DragDrop.prototype.setDragElementAligned = function(b) {
    var a, c;
    a = this.modelData.lists.options.length;
    this.dragContainer.find("." + this.dragClass).parent().css("width", "650px");
    c = parseInt(this.dragContainer.find("." + this.dragClass).parent().css("width"), 10);
    this.checkNumberOfColoumn(this.modelData.lists.options.length, c, b);
};
Athena.DragDrop.prototype.calculateMaximumWidth = function(n) {
    var c, d, j, e, h, l, b, g, k = this,
        f, m, a;
    e = $("body");
    e.css("font-family", '"MyriadPro Regular", Helvetica, sans-serif');
    j = 0;
    h = this.parentContainer.find("." + this.dragClass).css("line-height");
    f = function(o, q, p) {
        if (typeof MathJax !== "undefined") {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, "dragSimulator_" + o, function() {
                b = parseInt(k.dragContainer.attr("data-maxwidth"), 10);
                g = $(p).width() + k.offsetPaddingAdj;
                if ($(p).width() > b) {
                    if ((String(typeof k.modelData.contents.drag_width) !== "undefined")) {
                        g = parseInt(k.modelData.contents.drag_width.value, 10);
                    }
                    k.dragContainer.attr("data-maxwidth", g);
                    k.dragContainer.find("." + k.dragClass).css("width", g + "px");
                    if (k.dndType === "ddo-ole") {
                        k.setDragElementAligned(g);
                    } else {
                        if (k.dndType === "dds") {
                            k.setDragElementAligned(g);
                        }
                    }
                    k.dropContainer.find("." + k.dropClass).css("width", (g + k.offsetPaddingAdj) + "px");
                    k.dropContainer.find(".dropSimulator").css("width", (g - k.offsetPaddingAdj) + "px");
                } else {
                    if ((String(typeof k.modelData.contents.drag_width) !== "undefined")) {
                        b = parseInt(k.modelData.contents.drag_width.value, 10);
                    }
                    if (k.dndType === "ddo-ole") {
                        k.setDragElementAligned(b);
                    } else {
                        if (k.dndType === "dds") {
                            k.setDragElementAligned(b);
                        }
                    }
                    k.dragContainer.find("." + k.dragClass).css("width", b + "px");
                    if (k.isIEBrowser()) {
                        k.dropContainer.find("." + k.dropClass).css("width", (b + k.offsetPaddingAdj - 10) + "px");
                    } else {
                        k.dropContainer.find("." + k.dropClass).css("width", (b + k.offsetPaddingAdj) + "px");
                    }
                }
                $(p).detach();
            }]);
        }
    };
    for (d = 0; d < n.length; d++) {
        c = this.parentContainer.find("." + this.dragClass).eq(0).clone();
        l = Math.floor(Math.random() * 1000);
        $(c).attr("id", "dragSimulator_" + l);
        $(c).css({
            position: "absolute",
            left: "-40000px",
            top: "-40000px",
            "z-index": "10000"
        });
        $(c).find("span").css("display", "block");
        c.attr("class", "widthCalculation");
        c.css("width", "auto");
        c.css("border", "1px solid #115CA6");
        c.css("font-size", "20px");
        c.css("line-height", h);
        if (/<math>/.test(n[d].option.value)) {
            a = n[d].option.value;
            if (this.reduceFontSize) {
                a = this.addMathSizeAttributeinMathML(a, this.reduceSize + "px");
            } else {
                a = this.addMathSizeAttributeinMathML(a);
            }
            c.find("span").html(a);
        } else {
            if (this.reduceFontSize) {
                c.find("span").css("font-size", this.reduceSize + "px");
            }
            c.find("span").html(n[d].option.value.replace(/\s+/g, "&nbsp;"));
            e.append(c);
        }
        if (j < $("#dragSimulator_" + l).find("span").width()) {
            j = $("#dragSimulator_" + l).find("span").width();
        }
        m = typeof this.dragContainer.attr("data-maxwidth") === "undefined" ? 0 : parseInt(this.dragContainer.attr("data-maxwidth"), 10);
        if ((j + this.offsetPaddingAdj) > m) {
            this.dragContainer.attr("data-maxwidth", (j + this.offsetPaddingAdj));
        }
        this.dropContainer.find("." + this.dropClass).css("width", (j + this.offsetPaddingAdj) + "px");
        if (/<math>/.test(n[d].option.value)) {
            e.append(c);
            f(l, n[d].option.value, c);
        } else {
            c.detach();
        }
        if (this.dndType === "ddo-osp") {
            k.adjustDragheight();
        }
    }
    return j + this.offsetPaddingAdj;
};
Athena.DragDrop.prototype.adjustDragheight = function() {
    var b, c = 0,
        a = 0;
    for (a = 0; a <= this.dragContainer.find(".drag").length; a++) {
        this.dragContainer.find("#drag" + a).css("vertical-align", "top");
        b = parseInt(this.dragContainer.find("#drag" + a).css("height"), 10);
        if (b > c) {
            c = b;
        }
    }
    this.dragContainer.find("." + this.dragClass).css("line-height", c + "px");
    this.dragContainer.find("." + this.dragClass).css("height", c + "px");
    this.dropContainer.find("." + this.dropClass).css("height", c + "px");
};
Athena.DragDrop.prototype.shuffle = function(a, c) {
    var e, d, f, b;
    e = a.find(c).length;
    d = this.createRandom(e);
    f = [];
    for (b = 0; b < e; b++) {
        f[b] = a.find(c + ":eq(" + b + ")").clone(true);
    }
    a.find(c).detach();
    for (b = 0; b < e; b++) {
        a.append(f[d[b]].data("actual-order", b));
    }
    this.loadCorrectAnswers();
};
Athena.DragDrop.prototype.escapeRegEx = function(a) {
    return a.replace(/[\-\[\]\{}()*+?.,\\\^$|#\s]/g, "\\$&");
};
Athena.DragDrop.prototype.removeLineBreakandTrim = function(a) {
    a = a.replace(new RegExp("\n", "gm"), "").trim();
    return a;
};
Athena.DragDrop.prototype.create = function() {
    if (typeof this.dragContainer === "string") {
        this.dragContainer = this.parentContainer.find("." + this.dragContainer);
    }
    if (typeof this.dropContainer === "string") {
        this.dropContainer = this.parentContainer.find("." + this.dropContainer);
    }
    if (typeof this.feedbackContainer === "string") {
        this.feedbackContainer = this.parentContainer.find("." + this.feedbackContainer);
    }
    if (typeof this.hintContainer === "string") {
        this.hintContainer = this.parentContainer.find("." + this.hintContainer);
        if (this.modelData.contents.hint_text.objectId !== undefined) {
            this.hintContainer.attr("data-objectid", this.modelData.contents.hint_text.objectId);
        }
    }
    if (typeof this.checkClass === "string") {
        this.checkbutton = this.parentContainer.find("." + this.checkClass);
    }
    this.hintMsg = this.modelData.contents.hint_text.value;
    this.hintContainer.hide().find(".hint span").html(this.hintMsg);
    this.isMultipleCopies = (String(typeof this.modelData.contents.multiple_copies) === "undefined") ? false : this.removeLineBreakandTrim(this.modelData.contents.multiple_copies.value);
    this.isMultipleCopies = this.isMultipleCopies === "true" ? true : false;
    this.showNumberInTable = (String(typeof this.modelData.contents.show_list_number) === "undefined") ? true : this.removeLineBreakandTrim(this.modelData.contents.show_list_number.value);
    this.showNumberInTable = this.showNumberInTable === "false" ? false : true;
    this.reduceFontSize = (String(typeof this.modelData.contents.reduce_font) === "undefined") ? false : this.removeLineBreakandTrim(this.modelData.contents.reduce_font.value);
    this.reduceFontSize = this.reduceFontSize === "true" ? true : false;
    this.reduceSize = (String(typeof this.modelData.contents.font_size) === "undefined") ? this.reduceSize : this.removeLineBreakandTrim(this.modelData.contents.font_size.value);
    this.adjInteractWidth = (String(typeof this.modelData.contents.adj_interaction_width) === "undefined") ? false : this.removeLineBreakandTrim(this.modelData.contents.adj_interaction_width.value);
    this.adjInteractWidth = this.adjInteractWidth === "true" ? true : false;
    this.dndType = this.removeLineBreakandTrim(this.dndType);
    this.order_unique = (String(typeof this.modelData.contents.order_unique) === "undefined") ? false : this.removeLineBreakandTrim(this.modelData.contents.order_unique.value);
    this.parentContainer.find(".check-box").removeClass("active").show().find("span").html(this.checkText);
    this.parentContainer.find("." + this.questionContainer).html(this.modelData.contents.question.value);
    if (this.modelData.contents.question.objectId !== undefined) {
        this.parentContainer.find("." + this.questionContainer).attr("data-objectid", this.modelData.contents.question.objectId);
    }
    var M, J, q, ai, S, y, aa, Z, F, v, a, H, U, c, m, K, k, ag, E, e, l, ac, Q, aj, z, T, af, B, s, C, G, X, al, r, b, Y, d, t, g, P, ae, p, ab, o, x, u, ad, A, f, D, O, I, W, n, ak, R, N;
    k = this;
    G = "container";
    ac = typeof this.modelData.contents.place_holder === "undefined" ? "(dp)" : this.modelData.contents.place_holder.value.trim();
    if (new RegExp("ddo-osp").test(this.dndType)) {
        if (new RegExp("1").test(String(this.modelData.contents.contentType.value))) {
            this.contentCreator(this.modelData.lists.options, "text", "order", this.modelData.lists.options, this.modelData.lists.options.length);
            this.cloneElement(this.modelData.lists.options.length, this.parentContainer.find(".index-container"), this.parentContainer.find(".index-text"), "index", "append", "", false, function(h) {
                for (ag = 0; ag < h.length; ag++) {
                    h[ag].html(ag + 1);
                }
            });
            q = this.calculateMaximumHeight(this.modelData.lists.options);
            this.dragContainer.find("." + this.dragClass).css("height", q - 4 + "px");
            this.dropContainer.find("." + this.dropClass).css("height", q + "px");
            this.parentContainer.find(".index-text").css("height", q + "px");
            y = this.parentContainer.find(".interaction_container").height();
            v = this.dragContainer.height();
            a = (v > y) ? parseFloat(parseInt(v - y, 10) / 2) : 0;
            if (a !== 0) {
                this.dragContainer.css("margin-top", "-" + a + "px");
                this.dropContainer.css("margin-top", "-" + a + "px");
                this.dropContainer.find(".index-container").css("margin-top", "-" + a + "px");
            }
            this.addDroppable(this.dropContainer);
        } else {
            if (new RegExp("2").test(String(this.modelData.contents.contentType.value))) {
                k = this;
                e = this.modelData.lists.proof_header;
                this.contentCreator(this.modelData.lists.options, "text", "order", this.modelData.lists.options, this.modelData.lists.options.length);
                this.cloneElement(this.modelData.lists.proof.length, this.parentContainer.find(".prooftable"), this.parentContainer.find(".proofrow"), "table", "append", "", false, function(h) {
                    for (ag = 0; ag < k.modelData.lists.proof.length; ag++) {
                        W = h[ag].find(".prooflabel").eq(0).clone();
                        n = h[ag].find(".proofcontent").eq(0).clone();
                        if (String(typeof k.modelData.lists.proof[ag].statement) !== "undefined") {
                            h[ag].find(".prooflabel").html((ag + 1) + ". ");
                            R = k.modelData.lists.proof[ag].statement.value;
                            if (k.reduceFontSize) {
                                R = k.addMathSizeAttributeinMathML(R, k.reduceSize + "px");
                            } else {
                                R = k.addMathSizeAttributeinMathML(R);
                            }
                            if (k.checkMathHasPlaceholder(R, ac)) {
                                R = k.addSemantics(R, ac);
                                k.dropinMath = true;
                                R = R.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                R = R.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            h[ag].find(".proofcontent").eq(0).html(R);
                            if (String(typeof k.modelData.lists.proof[ag].statement.colspan) !== "undefined") {
                                h[ag].find(".proofcontent").eq(0).attr("colspan", ((Number(k.modelData.lists.proof[ag].statement.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].statement.rowspan) !== "undefined") {
                                h[ag].find(".proofcontent").eq(0).attr("rowspan", ((Number(k.modelData.lists.proof[ag].statement.rowspan))));
                                h[ag].find(".prooflabel").eq(0).attr("rowspan", ((Number(k.modelData.lists.proof[ag].statement.rowspan))));
                            }
                            if (k.modelData.lists.proof[ag].statement.objectId !== undefined) {
                                h[ag].find(".proofcontent").eq(0).attr("data-objectid", k.modelData.lists.proof[ag].statement.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason) !== "undefined") {
                            N = k.modelData.lists.proof[ag].reason.value;
                            if (k.reduceFontSize) {
                                N = k.addMathSizeAttributeinMathML(N, k.reduceSize + "px");
                            } else {
                                N = k.addMathSizeAttributeinMathML(N);
                            }
                            if (k.checkMathHasPlaceholder(N, ac)) {
                                N = k.addSemantics(N, ac);
                                k.dropinMath = true;
                                N = N.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                N = N.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            h[ag].find(".proofcontent").eq(1).html(N);
                            if (String(typeof k.modelData.lists.proof[ag].reason.colspan) !== "undefined") {
                                h[ag].find(".proofcontent").eq(1).attr("colspan", ((Number(k.modelData.lists.proof[ag].reason.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason.rowspan) !== "undefined") {
                                h[ag].find(".proofcontent").eq(1).attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason.rowspan))));
                                h[ag].find(".prooflabel").eq(1).attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason.rowspan))));
                            }
                            if (k.modelData.lists.proof[ag].reason.objectId !== undefined) {
                                h[ag].find(".proofcontent").eq(1).attr("data-objectid", k.modelData.lists.proof[ag].reason.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason1) !== "undefined") {
                            X = W.clone().appendTo(h[ag].find(".prooflabel").eq(0).parent());
                            al = n.clone().appendTo(h[ag].find(".proofcontent").eq(0).parent());
                            if (String(typeof k.modelData.lists.proof[ag].reason1.colspan) !== "undefined") {
                                al.attr("colspan", ((Number(k.modelData.lists.proof[ag].reason1.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason1.rowspan) !== "undefined") {
                                X.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason1.rowspan))));
                                al.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason1.rowspan))));
                            }
                            if (k.reduceFontSize) {
                                k.modelData.lists.proof[ag].reason1.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason1.value, k.reduceSize + "px");
                            } else {
                                k.modelData.lists.proof[ag].reason1.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason1.value);
                            }
                            if (k.checkMathHasPlaceholder(k.modelData.lists.proof[ag].reason1.value, ac)) {
                                k.modelData.lists.proof[ag].reason1.value = k.addSemantics(k.modelData.lists.proof[ag].reason1.value, ac);
                                k.dropinMath = true;
                                k.modelData.lists.proof[ag].reason1.value = k.modelData.lists.proof[ag].reason1.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                k.modelData.lists.proof[ag].reason1.value = k.modelData.lists.proof[ag].reason1.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            al.html(k.modelData.lists.proof[ag].reason1.value);
                            X.css({
                                "border-left": "2px solid rgb(17, 92, 166)"
                            });
                            if (k.modelData.lists.proof[ag].reason1.objectId !== undefined) {
                                al.attr("data-objectid", k.modelData.lists.proof[ag].reason1.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason2) !== "undefined") {
                            r = W.clone().appendTo(h[ag].find(".prooflabel").eq(0).parent());
                            b = n.clone().appendTo(h[ag].find(".proofcontent").eq(0).parent());
                            if (String(typeof k.modelData.lists.proof[ag].reason2.colspan) !== "undefined") {
                                b.attr("colspan", ((Number(k.modelData.lists.proof[ag].reason2.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason2.rowspan) !== "undefined") {
                                r.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason2.rowspan))));
                                b.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason2.rowspan))));
                            }
                            r.css({
                                "border-left": "2px solid rgb(17, 92, 166)"
                            });
                            if (k.reduceFontSize) {
                                k.modelData.lists.proof[ag].reason2.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason2.value, k.reduceSize + "px");
                            } else {
                                k.modelData.lists.proof[ag].reason2.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason2.value);
                            }
                            if (k.checkMathHasPlaceholder(k.modelData.lists.proof[ag].reason2.value, ac)) {
                                k.modelData.lists.proof[ag].reason2.value = k.addSemantics(k.modelData.lists.proof[ag].reason2.value, ac);
                                k.dropinMath = true;
                                k.modelData.lists.proof[ag].reason2.value = k.modelData.lists.proof[ag].reason2.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                k.modelData.lists.proof[ag].reason2.value = k.modelData.lists.proof[ag].reason2.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            b.html(k.modelData.lists.proof[ag].reason2.value);
                            if (k.modelData.lists.proof[ag].reason2.objectId !== undefined) {
                                b.attr("data-objectid", k.modelData.lists.proof[ag].reason2.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason3) !== "undefined") {
                            Y = W.clone().appendTo(h[ag].find(".prooflabel").eq(0).parent());
                            d = n.clone().appendTo(h[ag].find(".proofcontent").eq(0).parent());
                            if (String(typeof k.modelData.lists.proof[ag].reason2.colspan) !== "undefined") {
                                d.attr("colspan", ((Number(k.modelData.lists.proof[ag].reason3.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason3.rowspan) !== "undefined") {
                                Y.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason3.rowspan))));
                                d.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason3.rowspan))));
                            }
                            Y.css({
                                "border-left": "2px solid rgb(17, 92, 166)"
                            });
                            if (k.reduceFontSize) {
                                k.modelData.lists.proof[ag].reason3.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason3.value, k.reduceSize + "px");
                            } else {
                                k.modelData.lists.proof[ag].reason3.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason3.value);
                            }
                            if (k.checkMathHasPlaceholder(k.modelData.lists.proof[ag].reason3.value, ac)) {
                                k.modelData.lists.proof[ag].reason3.value = k.addSemantics(k.modelData.lists.proof[ag].reason3.value, ac);
                                k.dropinMath = true;
                                k.modelData.lists.proof[ag].reason3.value = k.modelData.lists.proof[ag].reason3.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                k.modelData.lists.proof[ag].reason3.value = k.modelData.lists.proof[ag].reason3.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            d.html(k.modelData.lists.proof[ag].reason3.value);
                            if (k.modelData.lists.proof[ag].reason3.objectId !== undefined) {
                                d.attr("data-objectid", k.modelData.lists.proof[ag].reason3.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason4) !== "undefined") {
                            t = W.clone().appendTo(h[ag].find(".prooflabel").eq(0).parent());
                            g = n.clone().appendTo(h[ag].find(".proofcontent").eq(0).parent());
                            if (String(typeof k.modelData.lists.proof[ag].reason4.colspan) !== "undefined") {
                                g.attr("colspan", ((Number(k.modelData.lists.proof[ag].reason4.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason4.rowspan) !== "undefined") {
                                t.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason4.rowspan))));
                                g.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason4.rowspan))));
                            }
                            t.css({
                                "border-left": "2px solid rgb(17, 92, 166)"
                            });
                            if (k.reduceFontSize) {
                                k.modelData.lists.proof[ag].reason4.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason4.value, k.reduceSize + "px");
                            } else {
                                k.modelData.lists.proof[ag].reason4.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason4.value);
                            }
                            if (k.checkMathHasPlaceholder(k.modelData.lists.proof[ag].reason4.value, ac)) {
                                k.modelData.lists.proof[ag].reason4.value = k.addSemantics(k.modelData.lists.proof[ag].reason4.value, ac);
                                k.dropinMath = true;
                                k.modelData.lists.proof[ag].reason4.value = k.modelData.lists.proof[ag].reason4.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                k.modelData.lists.proof[ag].reason4.value = k.modelData.lists.proof[ag].reason4.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            g.html(k.modelData.lists.proof[ag].reason4.value);
                            if (k.modelData.lists.proof[ag].reason4.objectId !== undefined) {
                                g.attr("data-objectid", k.modelData.lists.proof[ag].reason4.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason5) !== "undefined") {
                            ab = W.clone().appendTo(h[ag].find(".prooflabel").eq(0).parent());
                            o = n.clone().appendTo(h[ag].find(".proofcontent").eq(0).parent());
                            if (String(typeof k.modelData.lists.proof[ag].reason5.colspan) !== "undefined") {
                                o.attr("colspan", ((Number(k.modelData.lists.proof[ag].reason5.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason5.rowspan) !== "undefined") {
                                ab.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason5.rowspan))));
                                o.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason5.rowspan))));
                            }
                            ab.css({
                                "border-left": "2px solid rgb(17, 92, 166)"
                            });
                            if (k.reduceFontSize) {
                                k.modelData.lists.proof[ag].reason5.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason5.value, k.reduceSize + "px");
                            } else {
                                k.modelData.lists.proof[ag].reason5.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason5.value);
                            }
                            if (k.checkMathHasPlaceholder(k.modelData.lists.proof[ag].reason5.value, ac)) {
                                k.modelData.lists.proof[ag].reason5.value = k.addSemantics(k.modelData.lists.proof[ag].reason5.value, ac);
                                k.dropinMath = true;
                                k.modelData.lists.proof[ag].reason5.value = k.modelData.lists.proof[ag].reason5.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                k.modelData.lists.proof[ag].reason5.value = k.modelData.lists.proof[ag].reason5.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            o.html(k.modelData.lists.proof[ag].reason5.value);
                            if (k.modelData.lists.proof[ag].reason5.objectId !== undefined) {
                                o.attr("data-objectid", k.modelData.lists.proof[ag].reason5.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason6) !== "undefined") {
                            x = W.clone().appendTo(h[ag].find(".prooflabel").eq(0).parent());
                            u = n.clone().appendTo(h[ag].find(".proofcontent").eq(0).parent());
                            if (String(typeof k.modelData.lists.proof[ag].reason6.colspan) !== "undefined") {
                                u.attr("colspan", ((Number(k.modelData.lists.proof[ag].reason6.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason6.rowspan) !== "undefined") {
                                x.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason6.rowspan))));
                                u.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason6.rowspan))));
                            }
                            x.css({
                                "border-left": "2px solid rgb(17, 92, 166)"
                            });
                            if (k.reduceFontSize) {
                                k.modelData.lists.proof[ag].reason6.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason6.value, k.reduceSize + "px");
                            } else {
                                k.modelData.lists.proof[ag].reason6.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason6.value);
                            }
                            if (k.checkMathHasPlaceholder(k.modelData.lists.proof[ag].reason6.value, ac)) {
                                k.modelData.lists.proof[ag].reason6.value = k.addSemantics(k.modelData.lists.proof[ag].reason6.value, ac);
                                k.dropinMath = true;
                                k.modelData.lists.proof[ag].reason6.value = k.modelData.lists.proof[ag].reason6.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                k.modelData.lists.proof[ag].reason6.value = k.modelData.lists.proof[ag].reason6.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            u.html(k.modelData.lists.proof[ag].reason6.value);
                            if (k.modelData.lists.proof[ag].reason6.objectId !== undefined) {
                                u.attr("data-objectid", k.modelData.lists.proof[ag].reason6.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason7) !== "undefined") {
                            ad = W.clone().appendTo(h[ag].find(".prooflabel").eq(0).parent());
                            A = n.clone().appendTo(h[ag].find(".proofcontent").eq(0).parent());
                            if (String(typeof k.modelData.lists.proof[ag].reason7.colspan) !== "undefined") {
                                A.attr("colspan", ((Number(k.modelData.lists.proof[ag].reason7.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason7.rowspan) !== "undefined") {
                                ad.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason7.rowspan))));
                                A.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason7.rowspan))));
                            }
                            ad.css({
                                "border-left": "2px solid rgb(17, 92, 166)"
                            });
                            if (k.reduceFontSize) {
                                k.modelData.lists.proof[ag].reason7.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason7.value, k.reduceSize + "px");
                            } else {
                                k.modelData.lists.proof[ag].reason7.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason7.value);
                            }
                            if (k.checkMathHasPlaceholder(k.modelData.lists.proof[ag].reason7.value, ac)) {
                                k.modelData.lists.proof[ag].reason7.value = k.addSemantics(k.modelData.lists.proof[ag].reason7.value, ac);
                                k.dropinMath = true;
                                k.modelData.lists.proof[ag].reason7.value = k.modelData.lists.proof[ag].reason7.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                k.modelData.lists.proof[ag].reason7.value = k.modelData.lists.proof[ag].reason7.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            A.html(k.modelData.lists.proof[ag].reason7.value);
                            if (k.modelData.lists.proof[ag].reason7.objectId !== undefined) {
                                A.attr("data-objectid", k.modelData.lists.proof[ag].reason7.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason8) !== "undefined") {
                            f = W.clone().appendTo(h[ag].find(".prooflabel").eq(0).parent());
                            D = n.clone().appendTo(h[ag].find(".proofcontent").eq(0).parent());
                            if (String(typeof k.modelData.lists.proof[ag].reason8.colspan) !== "undefined") {
                                D.attr("colspan", ((Number(k.modelData.lists.proof[ag].reason8.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason8.rowspan) !== "undefined") {
                                f.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason8.rowspan))));
                                D.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason8.rowspan))));
                            }
                            f.css({
                                "border-left": "2px solid rgb(17, 92, 166)"
                            });
                            if (k.reduceFontSize) {
                                k.modelData.lists.proof[ag].reason8.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason8.value, k.reduceSize + "px");
                            } else {
                                k.modelData.lists.proof[ag].reason8.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason8.value);
                            }
                            if (k.checkMathHasPlaceholder(k.modelData.lists.proof[ag].reason8.value, ac)) {
                                k.modelData.lists.proof[ag].reason8.value = k.addSemantics(k.modelData.lists.proof[ag].reason8.value, ac);
                                k.dropinMath = true;
                                k.modelData.lists.proof[ag].reason8.value = k.modelData.lists.proof[ag].reason8.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                k.modelData.lists.proof[ag].reason8.value = k.modelData.lists.proof[ag].reason8.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            D.html(k.modelData.lists.proof[ag].reason8.value);
                            if (k.modelData.lists.proof[ag].reason8.objectId !== undefined) {
                                D.attr("data-objectid", k.modelData.lists.proof[ag].reason8.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason9) !== "undefined") {
                            O = W.clone().appendTo(h[ag].find(".prooflabel").eq(0).parent());
                            I = n.clone().appendTo(h[ag].find(".proofcontent").eq(0).parent());
                            if (String(typeof k.modelData.lists.proof[ag].reason9.colspan) !== "undefined") {
                                I.attr("colspan", ((Number(k.modelData.lists.proof[ag].reason9.colspan) * 2) - 1));
                            }
                            if (String(typeof k.modelData.lists.proof[ag].reason9.rowspan) !== "undefined") {
                                O.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason9.rowspan))));
                                I.attr("rowspan", ((Number(k.modelData.lists.proof[ag].reason9.rowspan))));
                            }
                            O.css({
                                "border-left": "2px solid rgb(17, 92, 166)"
                            });
                            if (k.reduceFontSize) {
                                k.modelData.lists.proof[ag].reason9.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason9.value, k.reduceSize + "px");
                            } else {
                                k.modelData.lists.proof[ag].reason9.value = k.addMathSizeAttributeinMathML(k.modelData.lists.proof[ag].reason9.value);
                            }
                            if (k.checkMathHasPlaceholder(k.modelData.lists.proof[ag].reason9.value, ac)) {
                                k.modelData.lists.proof[ag].reason9.value = k.addSemantics(k.modelData.lists.proof[ag].reason9.value, ac);
                                k.dropinMath = true;
                                k.modelData.lists.proof[ag].reason9.value = k.modelData.lists.proof[ag].reason9.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></span>');
                            } else {
                                k.modelData.lists.proof[ag].reason9.value = k.modelData.lists.proof[ag].reason9.value.replace(new RegExp(k.escapeRegEx(ac), "g"), '<span class="drop-Content"></span>');
                            }
                            I.html(k.modelData.lists.proof[ag].reason9.value);
                            if (k.modelData.lists.proof[ag].reason9.objectId !== undefined) {
                                I.attr("data-objectid", k.modelData.lists.proof[ag].reason9.objectId);
                            }
                        }
                        if (String(typeof k.modelData.lists.proof[ag].reason) === "undefined") {
                            h[ag].find(".prooflabel").eq(1).detach();
                            h[ag].find(".proofcontent").eq(1).detach();
                        }
                        if (String(typeof k.modelData.lists.proof[ag].statement) === "undefined") {
                            h[ag].find(".prooflabel").eq(0).detach();
                            h[ag].find(".proofcontent").eq(0).detach();
                        }
                    }
                });
                K = this.calculateMaximumWidth(this.modelData.lists.options);
                if (typeof this.dragContainer.attr("data-maxwidth") !== "undefined") {
                    if (parseInt(this.dragContainer.attr("data-maxwidth"), 10) > K) {
                        K = parseInt(this.dragContainer.attr("data-maxwidth"), 10);
                    }
                }
                this.dragContainer.find("." + this.dragClass).css("width", K + "px");
                this.dropContainer.find("." + this.dropClass).css("width", (K + this.offsetPaddingAdj) + "px");
                E = this.parentContainer.find(".proofrow").eq(0).removeAttr("colspan").removeAttr("rowspan").clone();
                P = E.find(".prooflabel").eq(0).removeAttr("colspan").removeAttr("rowspan").clone();
                E.find(".prooflabel").remove();
                E.find(".proofcontent").remove();
                for (ag = 0; ag < e.length; ag++) {
                    p = (String(typeof e[ag].header.colspan) === "undefined") ? 2 : Number(e[ag].header.colspan) * 2;
                    if (k.reduceFontSize) {
                        e[ag].header.value = k.addMathSizeAttributeinMathML(e[ag].header.value, k.reduceSize + "px");
                    } else {
                        e[ag].header.value = k.addMathSizeAttributeinMathML(e[ag].header.value);
                    }
                    P.clone().attr("colspan", p).css({
                        "border-left": "2px solid rgb(17, 92, 166)"
                    }).html(e[ag].header.value).appendTo(E);
                }
                this.parentContainer.find(".prooftable").prepend(E);
                this.addDroppable(this.dropContainer);
                if (!this.showNumberInTable) {
                    this.parentContainer.find("tr .prooflabel").css({
                        "padding-left": "10px",
                        "padding-right": "10px"
                    });
                    this.parentContainer.find("tr .proofcontent").css({
                        "padding-right": "10px"
                    });
                    this.parentContainer.find(".proofrow:gt(0) .prooflabel").html("");
                }
                if (this.reduceFontSize) {
                    this.parentContainer.find("tr .prooflabel,tr .proofcontent").css({
                        "font-size": "15px"
                    });
                    this.parentContainer.find(".drag-Area .drag").css({
                        "font-size": "15px"
                    });
                }
                if (this.adjInteractWidth) {
                    if (this.deviceType === "mobile") {
                        this.parentContainer.find(".interaction_container").removeClass("adjInteractionWidth");
                    } else {
                        this.parentContainer.find(".interaction_container").addClass("adjInteractionWidth");
                    }
                }
                if ((String(typeof this.modelData.contents.drag_width) !== "undefined")) {
                    this.dragContainer.find("." + this.dragClass).css("width", this.modelData.contents.drag_width.value + "px");
                    this.dropContainer.find("." + this.dropClass).css("width", (Number(this.modelData.contents.drag_width.value) + this.offsetPaddingAdj) + "px");
                }
            }
        }
    } else {
        if (new RegExp(/ddo-ole/mi).test(this.dndType)) {
            H = this.modelData.contents.expression.value;
            ak = "19px";
            H = this.addSemantics(H, ac, ak);
            af = 0;
            m = "";
            T = H.split("</math>");
            for (ag = 0; ag < T.length; ag++) {
                if (/<math>/.test(T[ag])) {
                    B = String(T[ag] + "</math>").split(new RegExp("(<math>.*</math>)", "g"));
                } else {
                    B = String(T[ag]).split(new RegExp("(<math>.*</math>)", "g"));
                }
                for (af = 0; af < B.length; af++) {
                    if (/<math>/.test(B[af])) {
                        m += B[af].replace(new RegExp(this.escapeRegEx(ac), "g"), "mathmlDrop");
                    } else {
                        m += B[af];
                    }
                }
            }
            s = [];
            c = m.split(new RegExp(this.escapeRegEx(ac), "g"));
            for (ag = 0; ag < c.length; ag++) {
                if (/<brk>/.test(c[ag])) {
                    M = c[ag].split(new RegExp(this.escapeRegEx("<brk>"), "g"));
                    for (af = 0; af < M.length; af++) {
                        if (/<br>/.test(M[af])) {
                            s.push('<div style="clear:both;"></div>');
                            M[af] = M[af].replace("<br>", "");
                        }
                        s.push('<div class="text"><span class="ddspan">' + M[af].replace(new RegExp("mathmlDrop", "g"), '<div class="drop" data-dropMode="mathMLDrop"  xmlns="http://www.w3.org/1999/xhtml"></div>') + "</span></div>");
                    }
                } else {
                    if (/<br>/.test(c[ag])) {
                        s.push('<div style="clear:both;"></div>');
                        c[ag] = c[ag].replace("<br>", "");
                    }
                    s.push('<div class="text"><span class="ddspan">' + c[ag].replace(new RegExp("mathmlDrop", "g"), '<div class="drop" data-dropMode="mathMLDrop" xmlns="http://www.w3.org/1999/xhtml"></div>') + "</span></div>");
                }
                if (ag !== c.length - 1) {
                    s.push('<div class="drop" xmlns="http://www.w3.org/1999/xhtml"></div>');
                }
            }
            m = "";
            for (ag = 0; ag < s.length; ag++) {
                m += s[ag];
            }
            if (/data\-dropMode=\"mathMLDrop\"/.test(m)) {
                this.dropinMath = true;
            }
            if (typeof this.browser !== "undefined" && (this.browser.toLowerCase() === "chrome" || this.browser.toLowerCase() === "safari")) {
                m = m.replace(/<math/g, '<script type="math/mml"><math').replace(/<\/math>/g, "</math><\/script>");
            }
            if (this.reduceFontSize) {
                m = this.addMathSizeAttributeinMathML(m, this.reduceSize + "px");
            } else {
                m = this.addMathSizeAttributeinMathML(m);
            }
            this.parentContainer.find("." + this.dropContainer.attr("class")).html(m);
            if (this.modelData.contents.expression.objectId !== undefined) {
                this.parentContainer.find("." + this.dropContainer.attr("class")).attr("data-objectid", this.modelData.contents.expression.objectId);
            }
            this.contentCreator(this.modelData.lists.options, "text", "order", this.modelData.lists.options, this.modelData.lists.options.length);
            K = this.calculateMaximumWidth(this.modelData.lists.options);
            if (typeof this.dragContainer.attr("data-maxwidth") !== "undefined") {
                if (parseInt(this.dragContainer.attr("data-maxwidth"), 10) > K) {
                    K = parseInt(this.dragContainer.attr("data-maxwidth"), 10);
                }
            }
            this.dragContainer.find("." + this.dragClass).css("width", K + "px");
            this.dropContainer.find("." + this.dropClass).css("width", (K + this.offsetPaddingAdj) + "px");
            this.setDragElementAligned(K);
            this.parentContainer.find("." + this.questionContainer).html(this.modelData.question);
            if (!this.dropinMath) {
                this.addDroppable(this.parentContainer.find("." + this.dropContainer.attr("class")), null, null, null);
            }
            if (this.reduceFontSize) {
                try {
                    this.parentContainer.find(".ddspan").css({
                        "font-size": k.reduceSize + "px"
                    });
                    this.parentContainer.find(".drag-Area .ddspan").css({
                        "font-size": k.reduceSize + "px"
                    });
                } catch (V) {
                    this.removeAptanaValidationError++;
                }
            }
            if (this.adjInteractWidth) {
                if (this.deviceType === "mobile") {
                    this.parentContainer.find(".interaction_container").removeClass("adjInteractionWidth");
                } else {
                    this.parentContainer.find(".interaction_container").addClass("adjInteractionWidth");
                }
            }
            if (String(typeof this.modelData.contents.drag_width) !== "undefined") {
                this.dragContainer.find("." + this.dragClass).css({
                    width: this.modelData.contents.drag_width.value + "px"
                });
                this.parentContainer.find(".drag-Area .drag").css({
                    width: this.modelData.contents.drag_width.value + "px"
                });
            }
        } else {
            if (new RegExp("dds").test(this.dndType)) {
                k = this;
                if (new RegExp("1").test(this.contentType)) {
                    this.cloneElement(this.modelData.lists.sections.length, this.parentContainer.find(".drop-container"), this.parentContainer.find("." + this.dropClass), "drop", "append", "", false, function(h) {
                        for (ag = 0; ag < k.modelData.lists.sections.length; ag++) {
                            h[ag].find("label").html(k.modelData.lists.sections[ag].section.value);
                            if (k.modelData.lists.sections[ag].section.objectId !== undefined) {
                                h[ag].attr("data-objectid", k.modelData.lists.sections[ag].section.objectId);
                            }
                        }
                    });
                    ag = -1;
                    this.dropContainer = this.parentContainer.find("." + this.dropContainer.attr("class"));
                    this.addDroppable(this.dropContainer);
                    this.parentContainer.find("." + this.dropClass).each(function() {
                        ag++;
                        $(this).data("group", ag);
                    });
                    this.contentCreator(this.modelData.lists.options, "text", "group", this.modelData.lists.options, this.modelData.lists.options.length);
                    K = this.calculateMaximumWidth(this.modelData.lists.options);
                    if (typeof this.dragContainer.attr("data-maxwidth") !== "undefined") {
                        if (parseInt(this.dragContainer.attr("data-maxwidth"), 10) > K) {
                            K = parseInt(this.dragContainer.attr("data-maxwidth"), 10);
                        }
                    }
                    this.dragContainer.find("." + this.dragClass).css("width", K + "px");
                    this.dropContainer.css("width", (K + this.offsetPaddingAdj) + "px");
                    ae = (String(typeof this.modelData.contents.contains_image) === "undefined") ? false : this.modelData.contents.contains_image.value;
                    ae = ae === "true" ? true : false;
                    if (ae) {
                        this.dragContainer.find("." + this.dragClass).css({
                            "padding-left": "0px",
                            "padding-right": "0px"
                        });
                        this.dragContainer.find("." + this.dragClass).addClass("after-no-drop");
                    }
                    if (String(typeof this.modelData.contents.drag_height) !== "undefined") {
                        this.dragContainer.find("." + this.dragClass).css({
                            height: this.modelData.contents.drag_height.value + "px"
                        });
                    }
                    if (String(typeof this.modelData.contents.drag_width) !== "undefined") {
                        this.dragContainer.find("." + this.dragClass).css({
                            width: this.modelData.contents.drag_width.value + "px"
                        });
                    }
                } else {
                    if (new RegExp("2").test(this.contentType)) {
                        if (k.modelData.lists.sections.length > 2) {
                            C = document.createElement("div");
                        }
                        if (k.modelData.lists.sections.length === 3) {
                            C.id = "container-Total";
                            G += "-3-";
                        } else {
                            if (k.modelData.lists.sections.length === 4) {
                                C.id = "container-4-Total";
                                G += "-4-";
                            } else {
                                if (k.modelData.lists.sections.length === 5) {
                                    C.id = "container-5-Total";
                                    G += "-5-";
                                }
                            }
                        }
                        this.cloneElement(this.modelData.lists.sections.length, this.parentContainer.find(".drop-container"), this.parentContainer.find("." + this.dropClass), "container", "append", "", false, function(h) {
                            for (ag = 0; ag < k.modelData.lists.sections.length; ag++) {
                                h[ag].attr("id", G + (ag + 1)).find("label").html(k.modelData.lists.sections[ag].section.value);
                                if (k.modelData.lists.sections[ag].section.objectId !== undefined) {
                                    h[ag].attr("data-objectid", k.modelData.lists.sections[ag].section.objectId);
                                }
                                if (k.modelData.lists.sections.length > 2 && ag > 0) {
                                    h[ag].detach().appendTo($(C));
                                }
                            }
                        });
                        if (k.modelData.lists.sections.length > 2) {
                            this.parentContainer.find(".drop-container").append($(C));
                        }
                        if (k.modelData.lists.sections.length === 5 && ((String(navigator.userAgent.match(/iPhone/i)) === "iPhone") || (String(navigator.userAgent.match(/Mobile/i)) === "Mobile") || (String(navigator.userAgent.match(/iPad/i)) !== "iPad"))) {
                            $(C).find("#container-5-4,#container-5-5").wrapAll('<div class="mobile-float">');
                        }
                        this.contentCreator(this.modelData.lists.options, "image", "group", this.modelData.lists.options, 2);
                        ag = 0;
                        this.dragContainer.attr("data-maxwidth", 0);
                        this.parentContainer.find(".drag img").bind("load", function() {
                            S = $(this).width();
                            ai = $(this).height();
                            aa = k.parentContainer.find(".drag").height();
                            if ((String(typeof k.modelData.contents.drag_width) !== "undefined")) {
                                Z = parseInt(k.modelData.contents.drag_width.value, 10);
                                k.parentContainer.find(".drag").css({
                                    width: Z
                                });
                                k.parentContainer.find(".drag").find("img").css({
                                    width: Z
                                });
                                k.dragContainer.attr("data-maxwidth", Z);
                            } else {
                                if (S > k.dragContainer.attr("data-maxwidth")) {
                                    if (k.modelData.lists.sections.length === 4) {
                                        Z = 44 * (1 + (S / 100));
                                        F = ai;
                                        k.parentContainer.find(".drag").css({
                                            width: Z
                                        });
                                        k.parentContainer.find(".drag").find("img").css({
                                            width: Z
                                        });
                                        k.dragContainer.attr("data-maxwidth", Z);
                                    } else {
                                        if (k.modelData.lists.sections.length === 5) {
                                            Z = 33 * (1 + (S / 100));
                                            F = ai;
                                            k.parentContainer.find(".drag").css({
                                                width: Z
                                            });
                                            k.parentContainer.find(".drag").find("img").css({
                                                width: Z
                                            });
                                            k.dragContainer.attr("data-maxwidth", Z);
                                        } else {
                                            k.parentContainer.find(".drag").css("width", S);
                                            k.dragContainer.attr("data-maxwidth", S);
                                        }
                                    }
                                }
                            }
                        });
                        this.parentContainer.find(".drop").each(function() {
                            $(this).data("group", ag++);
                        });
                        this.addDroppable(this.dropContainer);
                    }
                }
            }
        }
    }
    try {
        if (this.isIEBrowser()) {
            this.parentContainer.find(".drag").each(function() {
                var h = $(this).html();
                if (new RegExp("<math[a-z0-9_ '\"-=]*>", "ig").test(h)) {
                    $(this).html(h);
                }
            });
        }
        if (typeof MathJax !== "undefined") {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.parentContainer.toArray()[0].querySelector(".interaction_container")], function() {
                J = k.parentContainer.find(".interaction_container");
                k.toggleScroll(J);
            });
        }
        $(window).on("orientationchange resize", function(i) {
            var h = 500;
            setTimeout(function() {
                J = k.parentContainer.find(".interaction_container");
                k.toggleScroll(J);
                k.rePositionDroppable();
            }, h);
        });
        if (String(typeof this.modelData.contents.drag_height) !== "undefined") {
            this.dragContainer.find("." + this.dragClass).css("height", this.modelData.contents.drag_height.value + "px");
            this.dragContainer.find("." + this.dragClass).css("line-height", this.modelData.contents.drag_height.value + "px");
            if (String(k.dndType) === "ddo-ole") {
                this.dropContainer.find("." + this.dropClass).css("height", (Number(this.modelData.contents.drag_height.value) + this.offsetPaddingAdj) + "px");
            } else {
                this.dropContainer.find("." + this.dropClass).css("height", this.modelData.contents.drag_height.value + "px");
            }
            if (typeof MathJax !== "undefined") {
                MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.parentContainer.toArray()[0].querySelector(".interaction_container")], function() {
                    k.dragContainer.find("." + k.dragClass).css("height", k.modelData.contents.drag_height.value + "px");
                    k.dragContainer.find("." + k.dragClass).css("line-height", k.modelData.contents.drag_height.value + "px");
                    if (String(k.dndType) === "ddo-ole") {
                        k.dropContainer.find("." + k.dropClass).css("height", (Number(k.modelData.contents.drag_height.value) + this.offsetPaddingAdj) + "px");
                    } else {
                        k.dropContainer.find("." + k.dropClass).css("height", k.modelData.contents.drag_height.value + "px");
                    }
                });
            }
        }
    } catch (L) {
        k.removeAptanaValidationError++;
    }
    this.parentContainer.find("." + this.checkClass).find("span").html(this.modelData.checktext);
    this.addDraggable(this.dragContainer, null, null, null);
    try {
        if (typeof MathJax !== "undefined") {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.parentContainer.toArray()[0].querySelector(".interaction_container")], function() {
                setTimeout(function() {
                    k.addMathDroppable();
                }, 0);
            });
        }
    } catch (ah) {
        debug(ah);
    }
};
Athena.DragDrop.prototype.toggleScroll = function(b) {
    var a;
    a = this;
    a.deviceType = a.checkDevices();
    if (a.deviceType === "mobile") {
        this.parentContainer.find(".interaction_container").removeClass("adjInteractionWidth");
        if (a.dndType === "ddo-ole") {
            if (b.find(".ole-scroll-content").length === 0) {
                b.find(".drop-Area").wrapAll("<div class='ole-scroll-content'></div>");
            }
            if (!b.find(".ole-scroll-content").hasClass("mCustomScrollbar")) {
                b.find(".ole-scroll-content").css({
                    width: "100%",
                    overflow: "hidden",
                    position: "relative"
                });
                b.find(".ole-scroll-content").mCustomScrollbar({
                    horizontalScroll: true,
                    autoHideScrollbar: true,
                    scrollInertia: 100
                });
                b.find(".ole-scroll-content").find(".mCSB_container").css("margin-bottom", "10px");
                if (new RegExp("android", "gi").test(navigator.userAgent)) {
                    b.find(".ole-scroll-content").find(".mCSB_container").css("padding-right", "5px");
                }
            } else {
                b.find(".ole-scroll-content").mCustomScrollbar("update");
            }
        } else {
            if (a.dndType === "ddo-osp") {
                if (b.find(".pt-scroll-content").length === 0) {
                    b.find(".prooftable").wrapAll("<div class='pt-scroll-content'></div>");
                }
                if (!b.find(".pt-scroll-content").hasClass("mCustomScrollbar")) {
                    b.find(".pt-scroll-content").css({
                        height: "auto",
                        width: "100%",
                        overflow: "hidden",
                        position: "relative"
                    });
                    b.find(".pt-scroll-content").mCustomScrollbar({
                        horizontalScroll: true,
                        autoHideScrollbar: true,
                        scrollInertia: 100
                    });
                    b.find(".pt-scroll-content").find(".mCSB_container").css("margin-bottom", "10px");
                    if (new RegExp("android", "gi").test(navigator.userAgent)) {
                        b.find(".pt-scroll-content").find(".mCSB_container").css("padding-right", "5px");
                    }
                } else {
                    b.find(".pt-scroll-content").mCustomScrollbar("update");
                }
            }
        }
    } else {
        if (this.adjInteractWidth) {
            this.parentContainer.find(".interaction_container").addClass("adjInteractionWidth");
        }
        setTimeout(function() {
            if (a.dndType === "ddo-ole") {
                b.find(".ole-scroll-content").mCustomScrollbar("destroy");
            } else {
                b.find(".pt-scroll-content").mCustomScrollbar("destroy");
            }
        }, 500);
    }
};
Athena.DragDrop.prototype.options = function(a) {
    var b;
    for (b in a) {
        if (a.hasOwnProperty(b)) {
            this[b] = a[b];
        }
    }
    this.deviceType = this.checkDevices();
    if (this.deviceType === "mobile") {
        this.offsetPaddingAdj = 20;
        if (this.dndType === "ddo-osp") {
            this.offsetPaddingAdj = 20;
        }
    } else {
        this.offsetPaddingAdj = 20;
    }
};
Athena.DragDrop.prototype.isIEBrowser = function() {
    var a = navigator.userAgent;
    return new RegExp("msie", "ig").test(a) || (a.indexOf("compatible") < 0 && !!a.match(/Trident.*rv[ :]*11\./));
};
Athena.DragDrop.prototype.contentCreator = function(d, f, a, c, e) {
    var b;
    b = this;
    this.activityType = a;
    if (this.dropClonable) {
        this.cloneElement(e, this.dropContainer, this.dropContainer.find("." + this.dropClass), this.dropClass, "append", "", false, null);
    }
    this.cloneElement(d.length, this.dragContainer, this.dragContainer.find("." + this.dragClass), this.dragClass, "append", "", false, function(g) {
        b.loadContent(g, d, f, a, c);
    });
};
Athena.DragDrop.prototype.loadCorrectAnswers = function() {
    var d, f, c, b, a, g;
    d = this;
    this.correctAnswers = [];
    f = 0;
    if (this.isMultipleCopies) {
        this.dragContainer.find("." + this.dragClass).each(function() {
            c = $(this).data(d.activityType);
            b = c.split(",");
            if (d.order_unique) {
                d.correctAnswers.push($(this).clone());
                g = d.correctAnswers.length;
                d.correctAnswers[g - 1].data("order", b[0]);
            } else {
                for (f = 0; f < b.length; f++) {
                    d.correctAnswers[b[f]] = $(this).clone();
                    d.correctAnswers[b[f]].data("order", b[f]);
                }
            }
        });
    } else {
        if (!this.isMultipleCopies) {
            if (String(typeof this.storage.attemptNo) !== "undefined") {
                this.dragContainer.find("." + this.dragClass).each(function() {
                    d.correctAnswers[f++] = $(this).clone().data("actual-order", $(this).data("actual-order")).data(d.activityType, $(this).data(d.activityType));
                });
            } else {
                this.parentContainer.find("." + this.dragClass).each(function() {
                    d.correctAnswers[f++] = $(this).clone().data("actual-order", $(this).data("actual-order")).data(d.activityType, $(this).data(d.activityType));
                });
            }
            try {
                d.validOptionCount = 0;
                this.dragContainer.find("." + this.dragClass).each(function() {
                    if ($(this).data(d.activityType) > -1 && $(this).data(d.activityType) < d.parentContainer.find("." + d.dropClass).length) {
                        d.validOptionCount++;
                    }
                });
            } catch (e) {
                d.validOptionCount = d.modelData.lists.options.length;
                debug(e);
            }
        }
    }
};
Athena.DragDrop.prototype.loadContent = function(f, e, h, a, d) {
    var c, b, g;
    for (b = 0; b < f.length; b++) {
        d[b][a].value = d[b][a].value === "" ? -1 : d[b][a].value;
        if (String(h) === "text") {
            if (/<math>/.test(e[b].option.value)) {
                this.dragHasMath = true;
            } else {
                this.dragHasMath = false;
            }
            c = f[b].data(a, d[b][a].value).find("span").length > 0 ? f[b].data(a, d[b][a].value).find("span") : f[b].data(a, d[b][a].value);
            g = e[b].option.value;
            if (this.reduceFontSize) {
                g = this.addMathSizeAttributeinMathML(g, this.reduceSize + "px");
            } else {
                g = this.addMathSizeAttributeinMathML(g);
            }
            f[b].data(a, d[b][a].value).find("span").html(g);
            f[b].data("drag-index", b);
        } else {
            if (String(h) === "image") {
                f[b].data(a, d[b][a].value).find("img").attr("src", this.imageSourcePath + e[b].option.value);
            }
        }
        if (e[b].option.objectId !== undefined) {
            f[b].attr("data-objectid", e[b].option.objectId);
        }
    }
    this.shuffle(this.dragContainer, "." + this.dragClass);
    this.setEvents();
};
Athena.DragDrop.prototype.calculateDragBoxWidthAndHeight = function(c) {
    var f = 0,
        d, b, a, e;
    a = this.modelData.lists.options[c].option.value.length;
    for (d = 0; d < a; d++) {
        b = this.modelData.lists.options[c].option.value.length;
        f = b > f ? b : f;
    }
    e = f;
    return {
        maxLength: f,
        width: e
    };
};
Athena.DragDrop.prototype.setEvents = function() {
    var a;
    a = this;
    this.parentContainer.find("." + this.checkClass).unbind("click");
    this.parentContainer.find("." + this.checkClass).bind("click", function() {
        if (a.storage === undefined) {
            a.storage = {};
        }
        var b;
        b = $(this).find("span").text();
        a.storage.checkButtonText = b;
        if ((a.pluginEvent !== undefined) && a.pluginEvent !== null) {
            a.pluginEvent(null, null, "started", "instructionq", a.parentContainer.attr("data-objectid"));
        }
        if (b === a.checkText && $(this).hasClass(a.enableClass)) {
            a.dropContainer.find(".gray-shade").removeClass("gray-shade");
            if (a.attemptNo > 1 && a.storage.checkButtonText !== a.tryagainText) {
                a.storage.attemptOver = true;
            }
            a.storage.attemptNo = 1;
            a.persistData();
            a.validateAnswer(a.dropContainer, "." + a.dragClass, a.correctClass, a.incorrectClass, a.activityType);
        } else {
            if (b === a.tryagainText) {
                a.storage.attemptNo = 1;
                a.persistData();
                a.feedbackContainer.removeClass("hasText").find(".feedback").fadeOut(1000);
                a.hintContainer.hide();
                $(this).removeClass(a.enableClass).find("span").html(a.checkText);
                a.resetAll = false;
                a.resetWrongAnswers(a.dropContainer, a.dragContainer, a.activityType);
                a.storage.secondCheck = true;
            } else {
                if (b === a.showMeText || b === a.myAnswerText) {
                    a.attemptNo++;
                    a.storage.attemptNo = a.attemptNo;
                    a.storage.attemptOver = true;
                    a.persistData();
                    a.toggle($(this));
                }
            }
        }
    });
    this.parentContainer.find("." + this.resetClass).unbind("click");
    this.parentContainer.find("." + this.resetClass).bind("click", function() {
        a.resetWrongAnswers(a.dropContainer, a.dragContainer, a.activityType);
    });
};
Athena.DragDrop.prototype.createRandom = function(d) {
    var c, b, a;
    c = [d];
    c[0] = Math.round(Math.random() * (d - 1));
    c[0] = 0;
    for (b = 1; b < d; b++) {
        a = Math.round(Math.random() * (d - 1));
        while ($.inArray(a, c) >= 0) {
            a = Math.round(Math.random() * (d - 1));
        }
        c[b] = a;
        c[b] = b;
    }
    if (this.storage !== "undefined" && String(typeof this.storage.attemptNo) !== "undefined") {
        c = String(this.storage.randomArray).split(",");
    }
    this.randomArray = c;
    return c;
};
Athena.DragDrop.prototype.addDraggable = function(g, c, l, q) {
    var j, o, k, f, i, e, b, h, p, r, d, a, n, m;
    j = this;
    m = true;
    var me = this;
    g.find("." + this.dragClass).each(function() {
        if (!$(this).hasClass("disableDrag") && (!$(this).hasClass("ui-draggable") || !j.isMultipleCopies)) {
            $(this).draggable({
                scroll: true,
                refreshPositions: true,
                start: function() {
                    this.startDivHeight = $(this).attr("id");
                    j.parentContainer.find("div").css("z-index", 0);
                    $(this).parent().css("z-index", 100);
                    $(this).parent().parent().css("z-index", 100);
                    $(this).css("z-index", 100);
                    r = $(this).parent();
                    a = false;
                    n = false;
                    if (String(typeof $(this).next().html()) !== "undefined") {
                        d = $(this).next();
                        a = true;
                    } else {
                        d = $(this).prev();
                        n = true;
                    }
                    b = $(this).parent().attr("class").split("-")[0];
                    if (b === "drag") {
                        try {
                            if (e) {
                                e.remove();
                            }
                        } catch (s) {
                            debug(s);
                        }
                        e = $(this).clone().appendTo($(this).parent());
                        e.css({
                            position: "absolute",
                            "z-index": "0"
                        });
                        if (!j.isMultipleCopies) {
                            e.css("box-shadow", "none");
                        }
                        h = $(this).attr("id").split("drag")[1];
                        if (!j.isMultipleCopies) {
                            e.html("&nbsp;");
                        }
                        e.attr("id", "dropBg-" + h);
                        e.offset({
                            top: $(this).offset().top,
                            left: $(this).offset().left
                        });
                        e.css("width", $(this).width() + "px");
                    }
                    $(this).attr("cloned", "true");
                    if (String(c) !== "null" && String(typeof c) !== "undefined") {
                        c($(this));
                    }
                },
                stop: function() {
                    e.remove();
                    if (j.isMultipleCopies && !r.hasClass(j.dropClass)) {
                        p = $(this).clone();
                        p.data("order", $(this).data("order")).data("group", $(this).data("group")).data("actual-order", $(this).data("actual-order")).data("drag-index", $(this).data("drag-index"));
                        p.removeClass("ui-draggable").removeClass("ui-draggable-dragging");
                        if (a && !m) {
                            p.appendTo(r);
                            p.insertBefore(d);
                        } else {
                            if (n && !m) {
                                p.appendTo(r);
                                p.insertAfter(d);
                            }
                        }
                        j.addDraggable(j.dragContainer, null, null, null);
                    }
                    j.dragContainer.find("> div.drag:eq(" + $(this).data("drag-index") + ")").css("z-index", 0);
                    j.dropContainer.css("z-index", 0);
                    if (String(l) !== "null" && String(typeof l) !== "undefined") {
                        l($(this));
                    }
                    j.parentContainer.find(".hide-container").css({
                        "z-index": "101"
                    });
                },
                containment: j.parentContainer.find(".interaction_container"),
                revert: function(z) {
                    var A, x, u, w, v, B, s, y, t;
                    m = false;
                    if (String(l) !== "null" && String(typeof l) !== "undefined") {
                        l($(this));
                    }
                    if (!z) {
                        m = true;
                        w = $(this).parent().attr("class");
                        if (new RegExp(j.dropClass, "g").test(String(w))) {
                            v = $(this).data("actual-order");
                            u = j.dragContainer.find("." + j.dragClass).eq(v);
                            m = false;
                            $(this).css({
                                top: "0px",
                                left: "0px"
                            });
                            if (v === 0) {
                                $(this).insertBefore(u);
                            } else {
                                $(this).insertAfter(u);
                            }
                            u.detach();
                        }
                        j.checkChecButtonEnableStatus();
                        
                        var $drag = $(this);
                        var dragId = $drag.attr('id');
        	    		var datId = me.parentContainer.attr('data-objectid');
        	    		var sectionId = me.parentContainer.parents("section.content").attr('id');
        	    		var activityId = me.parentContainer.parents("div.activity").attr('class').match(/[^\s]+-[^\s]/)[0];
                        
        	    		Collab.onDragDrop(sectionId, activityId, datId, -1, dragId);
                    } else {
                        if (z.find("." + j.dragClass).length > 0 && (!j.allowMultiple)) {
                            if (z.find("." + j.dragClass).eq(0).hasClass("gray-shade")) {
                                m = true;
                            } else {
                                A = z.find("." + j.dragClass);
                                x = A.data("actual-order");
                                if (!j.isMultipleCopies) {
                                    if (j.dropContainer.find("#" + $(this).attr("id")).length <= 0) {
                                        j.dragContainer.find("#drag" + x).remove();
                                    }
                                    u = j.dragContainer.find("div.drag:eq(" + x + ")");
                                    A.insertBefore(u);
                                    if (j.dropContainer.find("#" + $(this).attr("id")).length <= 0) {
                                        B = $(this).clone();
                                        $(B).css({
                                            top: "0px",
                                            left: "0px"
                                        }).addClass(j.afterDropClass).find("span").html("&nbsp;");
                                        s = parseInt($(this).data("actual-order"), 10);
                                        if (s - 1 < 0) {
                                            B.insertBefore(j.dragContainer.find("div.drag:eq(" + (s + 1) + ")"));
                                        } else {
                                            B.insertAfter(j.dragContainer.find("div.drag:eq(" + (s - 1) + ")"));
                                        }
                                        setTimeout(function() {
                                            if (j.dragContainer.find("#" + $(B).attr("id")).length > 1) {
                                                j.dragContainer.find("#" + $(B).attr("id")).eq(1).remove();
                                            }
                                        }, 0);
                                    }
                                }
                                if (j.isMultipleCopies) {
                                    y = j.dropContainer.find("." + j.dropClass).index(z);
                                    t = j.dropContainer.find("." + j.dropClass).index($(this).parent());
                                    if (y !== t) {
                                        A.remove();
                                    }
                                }
                                if (!j.isMultipleCopies && $(this).parent().hasClass(j.dropClass)) {
                                    u.detach();
                                }
                                $(this).css({
                                    top: "0px",
                                    left: "0px"
                                }).appendTo(z);
                                if (j.isMultipleCopies) {
                                    $(this).css({
                                        top: "0px",
                                        left: "0px"
                                    }).appendTo(z);
                                }
                                m = false;
                            }
                        } else {
                            j.parentContainer.find("." + j.checkClass).addClass(j.enableClass);
                            k = j.dragContainer.find("." + j.dragClass).index($(this));
                            if ((!$(this).parent().hasClass(j.dropClass)) || j.allowMultiple) {
                                if (k > -1) {
                                    o = $(this).clone();
                                    o.data("order", $(this).data("order")).data("group", $(this).data("group")).data("actual-order", $(this).data("actual-order"));
                                    if (String(typeof o.find("span").html()) !== "undefined") {
                                        o.addClass(j.afterDropClass).css({
                                            left: "0px",
                                            top: "0px"
                                        }).find("span").html("&nbsp;");
                                    } else {
                                        o.addClass(j.afterDropClass).css({}).html("");
                                    }
                                    f = $(this).parent();
                                    if (!j.isMultipleCopies) {
                                        if (String(typeof $(this).next().html()) !== "undefined") {
                                            o.insertBefore($(this).next());
                                        } else {
                                            o.insertAfter($(this).prev());
                                        }
                                    }
                                    o.css({
                                        top: "0px",
                                        left: "0px"
                                    });
                                }
                            }
                            $(this).css({
                                top: "0px",
                                left: "0px"
                            }).appendTo(z);
                            m = false;
                        }
                    }
                    return m;
                },
                revertDuration: this.revertDuration
            });
        }
    });
};
Athena.DragDrop.prototype.checkChecButtonEnableStatus = function() {
    var d, b, a = this;
    try {
        d = this.parentContainer.find(".ui-droppable").find(".drag").length;
        if (Number(d) === 0) {
            this.parentContainer.find(".check-box").removeClass("active");
        } else {
            b = 0;
            this.parentContainer.find(".ui-droppable").find(".drag").each(function() {
                if (!$(this).hasClass("gray-shade")) {
                    b++;
                }
            });
            if (Number(b) === 0) {
                this.parentContainer.find(".check-box").removeClass("active");
            }
        }
    } catch (c) {
        a.removeAptanaValidationError++;
    }
};
Athena.DragDrop.prototype.addDroppable = function(l, n) {
    var q, o = this,
        e, j, b, h, g, f, d, k, p, c, m, a;
    q = Number(l.find("." + this.dropClass).length) > 0 ? l.find("." + this.dropClass) : l;
    g = this.modelData.lists.dropAdjustOffset;
    c = this.modelData.lists.dropAdjustOffsetMoz;
    p = this.modelData.lists.dropAdjustOffsetWebKit;
    m = this.modelData.lists.dropAdjustOffsetIE;
    a = this.modelData.lists.MobdropAdjustOffset;
    n = typeof n === "undefined" ? false : n;
    f = 0;
    var me = this;
    var registerDrop = function (elem, idx) {
	    $(elem).droppable({
	    	drop: function (evt) {
	    		console.log(idx + " drop index " + me);
	    		var tag = $(elem);
	    		var $drag = $(evt.toElement);
	    		if (!$drag.hasClass(me.dragClass)) {
	    			$drag = $drag.parents("." + me.dragClass);
	    		}
	    		var dragId = $drag.attr('id');
	    		var datId = me.parentContainer.attr('data-objectid');
	    		var sectionId = me.parentContainer.parents("section.content").attr('id');
	    		var activityId = me.parentContainer.parents("div.activity").attr('class').match(/[^\s]+-[^\s]/)[0];
	    		Collab.onDragDrop(sectionId, activityId, datId, idx, dragId);
	    	}
	    });
    };
    
    q.each(function() {
        e = o.dropContainer.offset();
        if (n && typeof $(this).attr("data-dropMode") !== "undefined" && $(this).attr("data-dropMode") === "mathMLDrop") {
            h = $(this).clone();
            j = $(this).offset().top - e.top - parseInt($(this).css("margin-bottom"), 10) + 10;
            b = ($(this).offset().left - e.left - 10);
            if ((typeof p !== "undefined" && p[f] !== undefined) && typeof o.browser !== "undefined" && (o.browser.toLowerCase() === "chrome" || o.browser.toLowerCase() === "safari")) {
                if (p[f].offsetL !== undefined && p[f].offsetT !== undefined) {
                    d = parseInt(p[f].offsetL.value, 10);
                    k = parseInt(p[f].offsetT.value, 10);
                }
            } else {
                if ((typeof c !== "undefined" && c[f] !== undefined) && typeof o.browser !== "undefined" && (o.browser.toLowerCase() === "firefox")) {
                    if (c[f].offsetL !== undefined && c[f].offsetT !== undefined) {
                        d = parseInt(c[f].offsetL.value, 10);
                        k = parseInt(c[f].offsetT.value, 10);
                    }
                } else {
                    if ((typeof m !== "undefined" && m[f] !== undefined) && o.isIEBrowser()) {
                        if (m[f].offsetL !== undefined && m[f].offsetT !== undefined) {
                            d = parseInt(m[f].offsetL.value, 10);
                            k = parseInt(m[f].offsetT.value, 10);
                        }
                    } else {
                        if ((typeof g !== "undefined") && g[f] !== undefined) {
                            if (g[f].offsetL !== undefined && g[f].offsetT !== undefined) {
                                d = parseInt(g[f].offsetL.value, 10);
                                k = parseInt(g[f].offsetT.value, 10);
                            }
                        } else {
                            d = 0;
                            k = 0;
                        }
                    }
                }
            }
            if (o.deviceType === "mobile") {
                if ((typeof a !== "undefined") && a[f] !== undefined) {
                    if (a[f].offsetL !== undefined && a[f].offsetT !== undefined) {
                        d = parseInt(a[f].offsetL.value, 10);
                        k = parseInt(a[f].offsetT.value, 10);
                    }
                }
            }
            b += d;
            j += k;
            $(h).removeAttr("data-dropmode");
            $(h).css({
                position: "absolute",
                left: b,
                top: j,
                visibility: "visible"
            });
            if (o.dropinMath) {
                if (o.dndType === "ddo-osp" && new RegExp("2").test(o.modelData.contents.contentType.value)) {
                    o.dropContainer.css("position", "relative");
                    $(this).closest("td").append($(h));
                } else {
                    $(this).data("dropRef", "drop-ref-" + f);
                    $(h).addClass("drop-ref-" + f);
                    o.dropContainer.append($(h));
                }
            } else {
                $(h).insertAfter($(this).closest(".text"));
            }
            if (o.dndType === "ddo-osp" && new RegExp("2").test(o.modelData.contents.contentType.value)) {
                $(this).css({
                    visibility: "hidden",
                    display: "inline-block"
                }).removeClass("drop-Content").addClass("dropSimulator");
            } else {
                $(this).css({
                    visibility: "hidden"
                }).removeClass("drop").addClass("dropSimulator");
            }
            registerDrop(h, f);
        } else {
        	registerDrop(this, f);
        }
        f++;
    });
};
Athena.DragDrop.prototype.rePositionDroppable = function() {
    var l, f, k, b, e, m, d, h = 0,
        o = this,
        j = this.modelData.lists.dropAdjustOffset,
        c = this.modelData.lists.dropAdjustOffsetMoz,
        p = this.modelData.lists.dropAdjustOffsetWebKit,
        n = this.modelData.lists.dropAdjustOffsetIE,
        a = this.modelData.lists.MobdropAdjustOffset,
        g = this.modelData.lists.MobdropAdjustOffsetPort;
    l = o.deviceType === "mobile" ? 1 : 0;
    setTimeout(function() {
        o.deviceType = o.checkDevices();
        l = o.deviceType !== "mobile" ? 1 : 0;
        o.dropContainer.find(".dropSimulator").each(function() {
            f = o.dropContainer.offset();
            k = $(this).offset().top - f.top - parseInt($(this).css("margin-bottom"), 10) + 10;
            b = ($(this).offset().left - f.left - 10);
            if (!l && (typeof p !== "undefined" && p[h] !== undefined) && typeof o.browser !== "undefined" && (o.browser.toLowerCase() === "chrome" || o.browser.toLowerCase() === "safari")) {
                if (p[h].offsetL !== undefined && p[h].offsetT !== undefined) {
                    e = parseInt(p[h].offsetL.value, 10);
                    m = parseInt(p[h].offsetT.value, 10);
                }
            } else {
                if ((typeof c !== "undefined" && c[h] !== undefined) && typeof o.browser !== "undefined" && (o.browser.toLowerCase() === "firefox")) {
                    if (c[h].offsetL !== undefined && c[h].offsetT !== undefined) {
                        e = parseInt(c[h].offsetL.value, 10);
                        m = parseInt(c[h].offsetT.value, 10);
                    }
                } else {
                    if ((typeof n !== "undefined" && n[h] !== undefined) && o.isIEBrowser()) {
                        if (n[h].offsetL !== undefined && n[h].offsetT !== undefined) {
                            e = parseInt(n[h].offsetL.value, 10);
                            m = parseInt(n[h].offsetT.value, 10);
                        }
                    } else {
                        if ((typeof j !== "undefined") && j[h] !== undefined) {
                            if (j[h].offsetL !== undefined && j[h].offsetT !== undefined) {
                                e = parseInt(j[h].offsetL.value, 10);
                                m = parseInt(j[h].offsetT.value, 10);
                            }
                        } else {
                            e = 0;
                            m = 0;
                        }
                    }
                }
            }
            if (o.deviceType === "mobile") {
                if (typeof g !== "undefined" && g[h].offsetL !== undefined && g[h].offsetT !== undefined) {
                    e = parseInt(g[h].offsetL.value, 10);
                    m = parseInt(g[h].offsetT.value, 10);
                } else {
                    if ((typeof a !== "undefined") && a[h] !== undefined) {
                        if (a[h].offsetL !== undefined && a[h].offsetT !== undefined) {
                            e = parseInt(a[h].offsetL.value, 10);
                            m = parseInt(a[h].offsetT.value, 10);
                        }
                    }
                }
            }
            b += e;
            k += m;
            o.dropContainer.find("." + $(this).data("dropRef")).css({
                left: b,
                top: k
            });
            h++;
        });
        if (new RegExp(/ddo-ole/mi).test(o.dndType)) {
            d = o.calculateMaximumWidth(o.modelData.lists.options);
            if (typeof o.dragContainer.attr("data-maxwidth") !== "undefined") {
                if (parseInt(o.dragContainer.attr("data-maxwidth"), 10) > d) {
                    d = parseInt(o.dragContainer.attr("data-maxwidth"), 10);
                }
            }
            o.dragContainer.find("." + o.dragClass).css("width", d);
            o.dropContainer.find("." + o.dropClass).css("width", (d + o.offsetPaddingAdj) + "px");
            o.setDragElementAligned(d);
        }
    }, 500);
};
Athena.DragDrop.prototype.addMathDroppable = function() {
    var d, c, b, g, n, e, j, l, k = this,
        h, f, m, a;
    a = function() {
        if (k.dropinMath) {
            if (k.dndType === "ddo-ole") {
                k.addDroppable(k.parentContainer.find("." + k.dropContainer.attr("class")), true);
            } else {
                k.addDroppable(k.dropContainer, true);
            }
        }
    };
    if (this.dndType === "ddo-ole") {
        b = this.calculateMaximumWidth(this.modelData.lists.options);
        if (typeof this.dragContainer.attr("data-maxwidth") !== "undefined") {
            if (parseInt(this.dragContainer.attr("data-maxwidth"), 10) > b) {
                b = parseInt(this.dragContainer.attr("data-maxwidth"), 10);
            }
        }
        if ((String(typeof this.modelData.contents.drag_width) !== "undefined")) {
            b = parseInt(this.modelData.contents.drag_width.value, 10);
        }
        this.dragContainer.find("." + this.dragClass).css("width", b + "px");
        this.dropContainer.find("." + this.dropClass).css("width", (b + this.offsetPaddingAdj) + "px");
        this.dropContainer.find(".dropSimulator").css("width", (b + this.offsetPaddingAdj) + "px");
        if (typeof MathJax !== "undefined") {
            f = MathJax.Hub.getAllJax(this.parentContainer.toArray()[0].querySelector(".interaction_container")).length;
            if (f > 0) {
                for (h = 0; h < f; h++) {
                    m = MathJax.Hub.getAllJax(this.parentContainer.toArray()[0].querySelector(".interaction_container"))[h].SourceElement().text;
                    if (/width:\s*\d+px;/g.test(MathJax.Hub.getAllJax(this.parentContainer.toArray()[0].querySelector(".interaction_container"))[h].SourceElement().text)) {
                        MathJax.Hub.getAllJax(this.parentContainer.toArray()[0].querySelector(".interaction_container"))[h].SourceElement().text = MathJax.Hub.getAllJax(this.parentContainer.toArray()[0].querySelector(".interaction_container"))[h].SourceElement().text.replace(/width:\s*\d+px;/g, "width: " + (b + this.offsetPaddingAdj + "px;visibility:hidden;"));
                    } else {
                        MathJax.Hub.getAllJax(this.parentContainer.toArray()[0].querySelector(".interaction_container"))[h].SourceElement().text = MathJax.Hub.getAllJax(this.parentContainer.toArray()[0].querySelector(".interaction_container"))[h].SourceElement().text.replace(/data\-dropMode="mathMLDrop"/g, 'data-dropMode="mathMLDrop" style="width: ' + (b + this.offsetPaddingAdj + 'px;visibility:hidden;"'));
                    }
                }
                MathJax.Hub.Queue(["Reprocess", MathJax.Hub, this.parentContainer.toArray()[0].querySelector(".interaction_container")], function() {
                    k.removeAptanaValidationError++;
                    a();
                });
            }
        } else {
            a();
        }
    }
};
Athena.DragDrop.prototype.validateAnswer = function(n, k, b, i, j) {
    var l, q, p, g, e, o, d, a, h, f, m, c;
    g = this.parentContainer.find(".interaction_container").css("width");
    e = this.parentContainer.find(".interaction_container").css("height");
    this.addHideContainer();
    n.find("." + b).removeClass(b);
    n.find("." + i).removeClass(i);
    p = false;
    o = this;
    this.partiallyAnswered = false;
    this._orderUserEntered = [];
    o.parentContainer.find("." + o.dropClass).each(function() {
        if (!$(this).hasClass("ui-droppable")) {
            $(this).addClass("drop-prop").removeClass(o.dropClass);
        }
    });
    n.find(k).each(function() {
        l = o.parentContainer.find("." + o.dropClass).index($(this).parent());
        q = document.createElement("div");
        l = (String(j) === "group") ? $(this).parent().data("group") : l;
        if (!o.isMultipleCopies) {
            a = (Number(l) === Number($(this).data(j)));
        } else {
            h = $(this).data(j);
            f = h.split(",");
            a = ($.inArray(String(l), f) >= 0) ? true : false;
            if (o.order_unique) {
                if (o._orderUserEntered.length > 0) {
                    a = ($.inArray($(this).data("drag-index"), o._orderUserEntered) >= 0) ? false : a;
                }
                if (a) {
                    o._orderUserEntered.push($(this).data("drag-index"));
                }
            }
        }
        if (String(l) === String($(this).find("div").data(j)) || a) {
            q.className = b;
            if ($(this).find("div").length > 0) {
                $(this).addClass(b);
            } else {
                $(this).addClass(b);
            }
        } else {
            if ($(this).find("div").length === 0) {
                q.className = i;
                $(this).addClass(i);
                p = true;
            } else {
                q.className = i;
                $(this).addClass(i);
                p = true;
            }
        }
    });
    if (String(this.activityType) !== "group") {
        q = document.createElement("div");
        q.className = i;
        if (!p && n.find("." + this.dropClass).not(":has(." + this.dragClass + ")").length) {
            this.partiallyAnswered = true;
        }
        n.find("." + this.dropClass).not(":has(." + this.dragClass + ")").each(function() {
            p = true;
        });
    } else {
        if ((Number(this.validOptionCount) !== Number(this.dropContainer.find("." + this.dragClass).length)) && !p) {
            this.partiallyAnswered = true;
            p = true;
        }
    }
    this.dragContainer.find("." + this.dragClass).each(function() {
        if (!$(this).hasClass(o.afterDropClass)) {
            try {
                $(this).draggable("destroy");
            } catch (r) {
                debug("exception in destoying drag");
            }
        }
    });
    this.showFeedbackMsg(p);
};
Athena.DragDrop.prototype.resetWrongAnswers = function(e, h, g) {
    var i, k, c, b, l, f, j, a, d;
    k = this;
    this.removeHideContainer();
    if (String(g) === "order") {
        f = e.find("." + this.dropClass).find("." + this.dragClass);
    } else {
        f = e.find("." + this.dragClass);
    }
    this._answerOrder = [];
    f.each(function() {
        if (String(g) === "group") {
            i = $(this).parent().data(g);
        } else {
            i = k.parentContainer.find("." + k.dropClass).index($(this).parent());
        }
        c = $(this).data(g);
        b = $(this).data("actual-order");
        if (!k.isMultipleCopies) {
            j = (Number(c) !== Number(i));
        } else {
            d = $(this).data(g);
            a = d.split(",");
            j = ($.inArray(String(i), a) >= 0) ? false : true;
            if (k.order_unique) {
                if (k._answerOrder.length > 0) {
                    j = ($.inArray($(this).data("drag-index"), k._answerOrder) >= 0) ? true : j;
                }
                if (!j) {
                    k._answerOrder.push($(this).data("drag-index"));
                }
            }
        }
        if (k.resetAll || j) {
            l = $(this).clone(true);
            h.find("> div.drag:eq(" + b + ")").data("actual-order", b).removeClass(k.afterDropClass).html(l.html());
            $(this).detach();
        } else {
            try {
                $(this).draggable("destroy");
                if (!k.isMultipleCopies) {
                    h.find("> div.drag:eq(" + $(this).data("drag-index") + ")").addClass("disableDrag");
                }
            } catch (m) {
                k.removeAptanaValidationError++;
            }
        }
    });
    this.dropContainer.find("." + this.incorrectClass).removeClass(this.incorrectClass);
    this.dropContainer.find("." + this.correctClass).addClass("gray-shade");
    this.addDraggable(h, null, null, null);
};
Athena.DragDrop.prototype.showFeedbackMsg = function(a) {
    var b = this,
        c;
    this.attemptNo++;
    if (!a) {
        if (this.modelData.contents.correct_feedback.objectId !== undefined) {
            this.feedbackContainer.show().addClass("hasText").find(".feedback").attr("data-objectid", this.modelData.contents.correct_feedback.objectId);
        }
        this.feedbackContainer.show().addClass("hasText").find(".feedback").show().find("span").show().html(this.successMsg);
        this.checkbutton.hide();
        this.feedbackAutoHide();
    } else {
        if (this.attemptNo === 1 && parseInt(this.noOfAttempts, 10) > 1) {
            this.checkbutton.find("span").html(this.tryagainText);
            if (this.partiallyAnswered) {
                this.feedbackContainer.addClass("hasText").show().find(".feedback").show().find("span").html(this.partialMessage);
            } else {
                if (this.modelData.contents.tryagain_feedback.objectId !== undefined) {
                    this.feedbackContainer.show().addClass("hasText").find(".feedback").attr("data-objectid", this.modelData.contents.tryagain_feedback.objectId);
                }
                this.feedbackContainer.addClass("hasText").show().show().find(".feedback").show().find("span").html(this.failMsg);
            }
            this.hintContainer.show();
        } else {
            if ((this.attemptNo > 1 && parseInt(this.noOfAttempts, 10) > 1) || (this.attemptNo === 1 && parseInt(this.noOfAttempts, 10) === 1)) {
                if (!this.toggleAnswers) {
                    this.checkbutton.hide();
                }
                this.checkbutton.addClass(this.showMeClass).find("span").html(this.showMeText);
                if (this.partiallyAnswered) {
                    this.incorrectMsg = this.partialMessage;
                }
                if (this.modelData.contents.incorrect_feedback.objectId !== undefined) {
                    this.feedbackContainer.show().addClass("hasText").find(".feedback").attr("data-objectid", this.modelData.contents.incorrect_feedback.objectId);
                }
                this.feedbackContainer.addClass("hasText").find(".feedback").show().find("span").html(this.incorrectMsg);
                this.myAnswers = [];
                c = -1;
                this.dropContainer.find("." + this.dragClass).each(function() {
                    c = b.parentContainer.find("." + b.dropClass).index($(this).parent());
                    b.myAnswers.push($(this).data("index", c));
                });
            }
        }
    }
    this.myAnswers = [];
    if (this.storage.attemptNo !== undefined) {
        this.dropContainer.find("." + this.dragClass).each(function() {
            c = b.parentContainer.find("." + b.dropClass).index($(this).parent());
            b.myAnswers.push($(this).data("index", c));
        });
    }
};
Athena.DragDrop.prototype.feedbackAutoHide = function() {
    var a = this;
    setTimeout(function() {
        a.feedbackContainer.find(".feedback").show();
    }, 0);
    setTimeout(function() {
        a.feedbackContainer.find(".feedback").hide();
        a.feedbackContainer.removeClass("hasText");
    }, 2000);
};
Athena.DragDrop.prototype.toggle = function(c) {
    var e, b, d, a;
    b = this;
    e = -1;
    this.removeHideContainer();
    this.dropContainer.find("." + this.dragClass).detach();
    if (c.hasClass(this.showMeClass)) {
        c.removeClass(this.showMeClass).addClass(this.myAnswerClass).find("span").html(this.myAnswerText);
        this.feedbackContainer.addClass("hasText").find(".feedback").show().find("span").html(this.showmeMsg);
        if (this.modelData.contents.showme_text.objectId !== undefined) {
            this.feedbackContainer.show().addClass("hasText").find(".feedback").attr("data-objectid", this.modelData.contents.showme_text.objectId);
        }
        if (this.activityType === "order") {
            for (e = 0; e < this.correctAnswers.length; e++) {
                d = this.correctAnswers[e].attr("class").replace("_append", "");
                if (this.correctAnswers[e].data("order") !== "" && Number(this.correctAnswers[e].data("order")) >= 0) {
                    this.correctAnswers[e].attr("class", d.replace(this.incorrectClass, this.incorrectClass + "_append").replace(this.correctClass, this.correctClass + "_append"));
                    if (!this.correctAnswers[e].parent().hasClass(this.dragContainer.attr("class"))) {
                        this.correctAnswers[e].appendTo(this.parentContainer.find("." + this.dropClass).eq(this.correctAnswers[e].data("order")));
                        if (b.isIEBrowser()) {
                            this.parentContainer.find("." + this.dropClass).eq(this.correctAnswers[e].data("order")).find("." + this.dragClass).html(this.correctAnswers[e].html());
                        }
                    } else {
                        this.correctAnswers[e].clone().appendTo(this.parentContainer.find("." + this.dropClass).eq(this.correctAnswers[e].data("order")));
                        if (b.isIEBrowser()) {
                            this.parentContainer.find("." + this.dropClass).eq(this.correctAnswers[e].data("order")).find("." + this.dragClass).html(this.correctAnswers[e].html());
                        }
                    }
                }
            }
        } else {
            if (this.activityType === "group") {
                for (e = 0; e < this.correctAnswers.length; e++) {
                    d = this.correctAnswers[e].attr("class").replace("_append", "");
                    if (this.correctAnswers[e].data("group") !== "" && Number(this.correctAnswers[e].data("group")) >= 0) {
                        this.correctAnswers[e].attr("class", d.replace(this.incorrectClass, this.incorrectClass + "_append").replace(this.correctClass, this.correctClass + "_append"));
                        if (!this.correctAnswers[e].parent().hasClass(this.dragContainer.attr("class"))) {
                            this.correctAnswers[e].appendTo(this.parentContainer.find("." + this.dropClass).eq(this.correctAnswers[e].data("group")));
                        } else {
                            this.correctAnswers[e].clone().appendTo(this.parentContainer.find("." + this.dropClass).eq(this.correctAnswers[e].data("group")));
                        }
                    }
                }
            }
        }
        this.dropContainer.find("." + this.dragClass).addClass(this.myanswerCorrectCls);
    } else {
        if (c.hasClass(this.myAnswerClass)) {
            this.feedbackContainer.removeClass("hasText").find(".feedback").show().find("span").html("");
            c.removeClass(this.myAnswerClass).addClass(this.showMeClass).find("span").html(this.showMeText);
            if (this.activityType === "order") {
                for (e = 0; e < this.myAnswers.length; e++) {
                    b.myAnswers[e].attr("class", b.myAnswers[e].attr("class").replace("_append", ""));
                    b.myAnswers[e].appendTo(this.parentContainer.find("." + this.dropClass).eq(b.myAnswers[e].data("index")));
                }
            }
            if (this.activityType === "group") {
                for (e = 0; e < this.myAnswers.length; e++) {
                    b.myAnswers[e].attr("class", b.myAnswers[e].attr("class").replace("_append", ""));
                    b.myAnswers[e].appendTo(this.parentContainer.find("." + this.dropClass).eq(b.myAnswers[e].data("index")));
                }
            }
            this.dropContainer.find("." + this.myanswerCorrectCls).removeClass(this.myanswerCorrectCls);
        }
    }
    this.dropContainer.find("." + this.dragClass).each(function() {
        try {
            $(this).draggable("disable");
        } catch (f) {
            debug("exception in disabling drag");
        }
    });
    if (typeof MathJax !== "undefined" && c.hasClass(this.myAnswerClass)) {
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.parentContainer.toArray()[0].querySelector(".interaction_container")], function() {
            b.removeAptanaValidationError++;
        });
    }
};
Athena.DragDrop.prototype.addHideContainer = function() {
    var g, b, a, d, f, e, c, h;
    c = this;
    g = this.parentContainer.find(".hide-container");
    if (c.dndType === "dds") {
        b = g.clone();
        b.addClass("hideDrag");
        a = this.parentContainer.find(".drop-container").height();
        d = this.parentContainer.find(".drop-container").width();
        b.css({
            position: "absolute",
            height: a + "px",
            width: d + "px",
            "margin-left": "0px",
            background: "none",
            "z-index": "10000"
        });
        this.parentContainer.find(".drop-container").append(b);
    } else {
        this.parentContainer.find("." + this.dragClass).each(function() {
            b = g.clone();
            b.addClass("hideDrag");
            a = $(this).height();
            d = $(this).width();
            f = Number($(this).offset().top);
            e = Number($(this).offset().left);
            b.css({
                position: "absolute",
                height: a + "px",
                width: d + "px",
                "margin-left": "0px",
                background: "none",
                "z-index": "10000"
            });
            if (c.dndType === "ddo-ole") {
                b.css("top", "0px");
            } else {
                b.css("margin-top", "0px");
            }
            $(this).css({
                "z-index": "1"
            });
            $(this).parent().append(b);
            b.show();
            if (new RegExp("ddo-osp").test(c.dndType)) {
                if (new RegExp("2").test(String(c.modelData.contents.contentType.value))) {
                    b.css("margin-top", "-" + a + "px");
                }
            }
        });
    }
};
Athena.DragDrop.prototype.removeHideContainer = function() {
    this.parentContainer.find(".hide-container.hideDrag").remove();
};
Athena.FillInBlankQuestion = function() {
    this.parentContainer = null;
    this.noOfAttempts = 2;
    this.attemptNo = 1;
    this.contentClass = null;
    this.checkbuttonClass = null;
    this.markWidth = null;
    this.feedback = null;
    this.hint = null;
    this.input = null;
    this.checkEnableAllValue = false;
    this.contentType = "text";
    this.successMsg = "";
    this.tryAgainMsg = "";
    this.hintMsg = "";
    this.failMsg = "";
    this.showMeMsg = "";
    this.incompleteMsg = "";
    this.content = "";
    this.tryAgainBtnTxt = "";
    this.showMeBtnTxt = "";
    this.myAnswerBtnTxt = "";
    this.checkBtnTxt = "";
    this.textBox = [];
    this.correctAnswers = [];
    this.textBoxValue = [];
    this.textBoxStatus = [];
    this.imagesPath = "assets/images/";
    this.reduceSize = 15;
    this.reduceFontSize = false;
    this.mathFontSize = "";
    this.pluginEvent = null;
    this.deviceType = "";
    this.removeAptanaValidationError = 0;
    this.storage = {};
    this.autoScrollTo = true;
};
Athena.FillInBlankQuestion.extend(Athena.BaseClass);
Athena.FillInBlankQuestion.prototype.options = function(b, a) {
    var c;
    for (c in b) {
        if (b.hasOwnProperty(c) && this.hasOwnProperty(c)) {
            this[c] = b[c];
        }
    }
    this.input = a;
};
Athena.FillInBlankQuestion.prototype.init = function(b) {
    var a = this;
    this.deviceType = this.checkDevices();
    if (typeof b !== "undefined") {
        this.parentContainer = b;
    }
    this.parentContainer.css("position", "relative");
    if (typeof this.contentClass === "string") {
        this.contentClass = this.parentContainer.find("." + this.contentClass);
    }
    if (typeof this.checkbuttonClass === "string") {
        this.checkbuttonClass = this.parentContainer.find("." + this.checkbuttonClass);
    }
    if (typeof this.feedback === "string") {
        this.feedback = this.parentContainer.find("." + this.feedback);
    }
    if (typeof this.hint === "string") {
        this.hint = this.parentContainer.find("." + this.hint);
        if (this.input.contents.hint.objectId !== undefined) {
            this.hint.attr("data-objectid", this.input.contents.hint.objectId);
        }
        this.hint.find(".hint span").html(this.hintMsg);
    }
    this.checkbuttonClass.removeClass("active").find("span").html(this.checkBtnTxt);
    this.parseInput();
    if (this.deviceType === "mobile") {
        if (typeof MathJax !== "undefined") {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.parentContainer.toArray()[0].querySelector(".main-Container"), function() {
                var c = false;
                a.parentContainer.find(".MathJax, table").each(function() {
                    if ($(this).width() > 265) {
                        c = true;
                        return;
                    }
                });
                if (c) {
                    a.toggleScroll();
                }
            }]);
        }
    }
    $(window).on("orientationchange", function(d) {
        var c = 500;
        setTimeout(function() {
            a.toggleScroll();
        }, c);
    });
};
Athena.FillInBlankQuestion.prototype.toggleScroll = function() {
    var a = this;
    a.deviceType = a.checkDevices();
    if (a.deviceType === "mobile") {
        if (!a.parentContainer.find(".scroll-content").hasClass("mCustomScrollbar")) {
            a.parentContainer.find(".scroll-content").mCustomScrollbar({
                horizontalScroll: true,
                autoHideScrollbar: true,
                scrollInertia: 100
            });
            a.parentContainer.find(".scroll-content").find(".mCSB_container").css("margin-bottom", "10px");
            if (new RegExp("android", "gi").test(navigator.userAgent)) {
                a.parentContainer.find(".scroll-content").find(".mCSB_container").css("padding-right", "5px");
            }
        } else {
            a.parentContainer.find(".scroll-content").mCustomScrollbar("update");
        }
    } else {
        setTimeout(function() {
            if (a.parentContainer.find(".scroll-content").hasClass("mCustomScrollbar")) {
                a.parentContainer.find(".scroll-content").mCustomScrollbar("destroy");
            }
        }, 500);
    }
};
Athena.FillInBlankQuestion.prototype.reset = function() {
    this.resetInputs(true);
};
Athena.FillInBlankQuestion.prototype.parseInput = function() {
    var p = this,
        e, c, h, g, d, o, f, k, j, b, n, m, l, a;
    c = this.input.contents.question.value;
    h = this.input.contents.description.value;
    g = this.input.contents.content_type.value;
    k = this.input.contents.place_holder.value;
    if (this.input.lists.data_table !== undefined) {
        m = this.input.lists.data_table;
    }
    if (this.input.contents.fiq_seperate_mode !== undefined) {
        l = this.input.contents.fiq_seperate_mode.value;
    }
    l = typeof l === "undefined" ? false : l;
    this.reduceFontSize = (String(typeof this.input.contents.reduce_font) === "undefined") ? false : this.input.contents.reduce_font.value;
    this.reduceFontSize = this.reduceFontSize === "true" ? true : false;
    if (this.deviceType === "mobile") {
        this.targetFont = 14;
    } else {
        this.targetFont = (String(typeof this.input.contents.target_font_size) === "undefined") ? "22" : this.input.contents.target_font_size.value;
    }
    this.targetFont = parseInt(this.targetFont, 10);
    if (this.reduceFontSize) {
        this.mathFontSize = this.targetFont < 15 ? "15px" : this.targetFont > 19 ? "19px" : this.targetFont + "px";
    } else {
        this.mathFontSize = "19px";
    }
    p.contentType = typeof g === "undefined" ? "text" : g;
    p.parentContainer.find(".question_container").html(c).attr("data-objectid", this.input.contents.question.objectId);
    if (p.contentType === "text") {
        if (!l && !this.checkSeperateFromText(h)) {
            p.parentContainer.find(".interaction_container").addClass("fib-text");
        }
        if (this.input.contents.description.objectId !== undefined) {
            this.parentContainer.find(".interaction_container").attr("data-objectid", this.input.contents.description.objectId);
        }
    }
    j = new RegExp(k, "gi");
    b = h.match(j);
    h = this.input.contents.description.value;
    h = this.addSemantics(h, k, this.mathFontSize);
    h = h.replace(/<math/g, '<script type="math/mml"><math').replace(/<\/math>/g, "</math><\/script>");
    h = h.replace(/<label/g, '<div class="label"').replace(/<\/label>/g, "</div>");
    o = this.input.lists.correct_answers;
    if (b !== null) {
        n = this.calculateMaxWidthAndLenOfTextBox();
        for (f = 0; f < b.length; f++) {
            this.correctAnswers[f] = o[f].answer.value;
            this.splitAndTrimCorrectAnswer(f);
            h = h.replace(b[f], '<input xmlns="http://www.w3.org/1999/xhtml" type="text" maxlength="' + n.maxLength + '" style="width:' + n.width + 'px" />');
        }
    }
    if (p.contentType === "image") {
        p.buildImageTemplate($(this));
    } else {
        if (this.reduceFontSize) {
            h = this.addMathSizeAttributeinMathML(h, this.mathFontSize);
        }
        p.parentContainer.find(".scroll-content").html(h);
    }
    if (typeof m !== "undefined") {
        this.buildTable(m, k);
    } else {
        if (this.reduceFontSize) {
            a = (this.targetFont - 15) + 1;
            this.parentContainer.find(".interaction_container").addClass("reduceFont_Level" + a);
        }
    }
};
Athena.FillInBlankQuestion.prototype.checkSeperateFromText = function(a) {
    var b = this.input.contents.place_holder !== undefined ? this.input.contents.place_holder.value : "<FIQ>";
    if (a.trim() === b || a.trim() === "<label>" + b + "</label>") {
        this.input.contents.description = b;
        return true;
    }
    return false;
};
Athena.FillInBlankQuestion.prototype.buildTable = function(r) {
    var o, e, m, l, t, q, b, g, p, d, s, n, c, f, h, u, a;
    c = this.input.lists.correct_answers;
    f = this.input.contents.place_holder.value;
    q = document.createElement("table");
    n = this.calculateMaxWidthAndLenOfTextBox();
    for (o = 0, m = 0; o < r.length; o++) {
        b = document.createElement("tr");
        for (t in r[o]) {
            if (r[o].hasOwnProperty(t)) {
                u = new RegExp(f, "gi");
                g = document.createElement("td");
                p = typeof r[o][t].mode === "undefined" ? "" : r[o][t].mode;
                d = typeof r[o][t].colspan === "undefined" ? "" : r[o][t].colspan;
                s = typeof r[o][t].rowspan === "undefined" ? "" : r[o][t].rowspan;
                $(g).addClass(p);
                if (d !== "") {
                    $(g).attr("colspan", d);
                } else {
                    if (s !== "") {
                        $(g).attr("rowspan", s);
                    }
                }
                if (r[o][t].objectId !== undefined) {
                    $(g).attr("data-objectid", r[o][t].objectId);
                }
                if (u.test(r[o][t].value)) {
                    h = r[o][t].value.match(u);
                    for (l = 0; l < h.length; l++, m++) {
                        this.correctAnswers[m] = c[m].answer.value;
                        this.splitAndTrimCorrectAnswer(m);
                        r[o][t].value = this.addSemantics(r[o][t].value, f, this.mathFontSize);
                        r[o][t].value = r[o][t].value.replace(/<math/g, '<script type="math/mml"><math').replace(/<\/math>/g, "</math><\/script>");
                        r[o][t].value = r[o][t].value.replace(new RegExp(f, "i"), '<input xmlns="http://www.w3.org/1999/xhtml" type="text" maxlength="' + n.maxLength + '" style="width:' + n.width + 'px" />');
                    }
                    g.innerHTML = r[o][t].value;
                } else {
                    r[o][t].value = this.addMathSizeAttributeinMathML(r[o][t].value, this.mathFontSize);
                    $(g).html(r[o][t].value);
                }
                b.appendChild(g);
            }
        }
        q.appendChild(b);
    }
    this.parentContainer.find(".scroll-content").append($(q));
    if (this.reduceFontSize) {
        a = (this.targetFont - 15) + 1;
        this.parentContainer.find(".scroll-content table").addClass("reduceFont_Level" + a);
    }
    this.setEqualHeight();
};
Athena.FillInBlankQuestion.prototype.setEqualHeight = function() {
    var b, a;
    b = this.parentContainer.find(".scroll-content").find("table").find("td").map(function() {
        if ($(this).attr("class") === undefined) {
            return $(this).height();
        }
    }).get();
    a = Math.max.apply(Math, b);
    this.parentContainer.find(".scroll-content").find("table").find("td:not(.header)").css("height", a);
};
Athena.FillInBlankQuestion.prototype.removeLineBreakandTrim = function(a) {
    a = a.replace(new RegExp("\n", "gm"), "").trim();
    return a;
};
Athena.FillInBlankQuestion.prototype.buildImageTemplate = function(a) {
    var o = this,
        g, l, k, d, m, e, n, c, h, b, f, j;
    g = this.removeLineBreakandTrim(this.input.contents.image_path.value.trim());
    this.parentContainer.find(".scroll-content").append('<div class="image-Area"><img src="' + this.imagesPath + g + '" /></div>');
    this.contentClass = this.parentContainer.find(".content .image-Area");
    if (this.input.contents.image_path.objectId !== undefined) {
        this.contentClass.attr("data-objectid", this.input.contents.image_path.objectId);
    }
    j = this.input.lists.input;
    if (typeof j !== "undefined") {
        m = o.calculateMaxWidthAndLenOfTextBox();
        for (d = 0; d < j.length; d++) {
            l = j[d].coordX.value;
            k = j[d].coordY.value;
            e = j[d].hasArrow.value;
            b = j[d].degreeFlag.value;
            if (typeof e !== "undefined" && e.toUpperCase() === "Y") {
                c = parseInt(j[d].arrow_width.value, 10);
                h = j[d].arrow_coordX.value;
                f = j[d].arrow_coordY.value;
                n = j[d].arrow_angle.value;
                n = typeof n !== "undefined" ? n : 0;
            }
            o.correctAnswers[d] = this.input.lists.correct_answers[d].answer.value;
            o.splitAndTrimCorrectAnswer(d);
            if (e.toUpperCase() === "Y") {
                $('<div class="arrow_holder deg' + n + '" style="width:' + c + "px;left:" + h + "px;top:" + f + "px;transform:rotate(" + n + "deg);-webkit-transform:rotate(" + n + 'deg)"><span></span></div><input xmlns="http://www.w3.org/1999/xhtml" type="text" maxlength="' + m.maxLength + '" style="width:' + m.width + "px;position:absolute;left:" + (parseInt(l, 10)) + "px;top:" + k + 'px;" />').appendTo(o.contentClass);
            } else {
                $('<input xmlns="http://www.w3.org/1999/xhtml" type="text" maxlength="' + m.maxLength + '" style="width:' + m.width + "px;position:absolute;left:" + l + "px;top:" + k + 'px;" />').appendTo(o.contentClass);
            }
            if (b === "Y") {
                this.parentContainer.find("input").after('<span style="position:absolute;left:' + (c + parseInt(l, 10) + m.width * 1.5) + "px;top:" + (k - 14) + 'px;">&#176;</span>');
            }
        }
    }
};
Athena.FillInBlankQuestion.prototype.calculateTextBoxWidthAndLen = function(c) {
    var g = 0,
        d, b, a, e, f;
    a = this.correctAnswers[c].length;
    f = this.targetFont - 7;
    for (d = 0; d < a; d++) {
        b = this.correctAnswers[c][d].length;
        g = b > g ? b : g;
    }
    g += 2;
    e = g * f;
    return {
        maxLength: g,
        width: e
    };
};
Athena.FillInBlankQuestion.prototype.calculateMaxWidthAndLenOfTextBox = function() {
    var b = 0,
        f, e, c, g, d, a, k, h;
    g = this.input.lists.correct_answers.length;
    h = this.targetFont - 7;
    for (f = 0; f < g; f++) {
        k = this.input.lists.correct_answers[f].answer.value.split("or");
        d = k.length;
        for (e = 0; e < d; e++) {
            c = k[e].length;
            b = c > b ? c : b;
        }
    }
    b += 2;
    a = b * h;
    return {
        maxLength: b,
        width: a
    };
};
Athena.FillInBlankQuestion.prototype.create = function() {
    var a, c;
    a = this;
    this.myAnswers = [];
    c = 0;
    var me = this;
    try {
        this.parentContainer.find("input").each(function() {
            c++;
            if (a.input.contents["input_" + c] !== undefined && a.input.contents["input_" + c].objectId !== undefined) {
                $(this).attr("data-objectId", a.input.contents["input_" + c].objectId);
            }
        });
    } catch (b) {
        this.removeAptanaValidationError++;
    }
    this.checkbuttonClass.bind("click", function(d) {
        if ((new RegExp("check", "igm")).test(a.checkbuttonClass.html())) {
            a.parentContainer.find(".disable").removeClass("disable");
        }
        a.validate();
    });
    this.contentClass.delegate('input[type="text"]', "keyup", function() {
    	a.allValuesEntered(a);
    });
    
    this.contentClass.delegate('input[type="text"]', "input", function() {
    	var myIdx = $(me.contentClass.selector + ' input[type="text"]').index(this);
    	Collab.fillInTheBlankTextUpdated(me.contentClass.selector + ' input[type="text"]:eq(' + myIdx + ')', $(this).val());
    });
    
    $(window).resize(function() {
        a.positionFeedbackIcons(a);
    });
};
Athena.FillInBlankQuestion.prototype.splitAndTrimCorrectAnswer = function(a) {
    var b, c;
    c = this.correctAnswers[a].split("or");
    for (b = 0; b < c.length; b++) {
        c[b] = c[b].trim();
    }
    this.correctAnswers[a] = c;
};
Athena.FillInBlankQuestion.prototype.toggleHint = function(a) {
    var b = this;
    if (a) {
        b.hint.find(".hint").show();
    } else {
        b.hint.find(".hint").hide();
    }
};
Athena.FillInBlankQuestion.prototype.positionFeedbackIcons = function(a) {
    var b, c, e, d;
    this.parentContainer.find(".mark").each(function() {
        b = $(this).data("index");
        d = a.contentClass.find("input[type=text]:eq(" + b + ")");
        c = d.position().left;
        e = d.position().top;
        $(this).css({
            left: c,
            top: e
        });
    });
};
Athena.FillInBlankQuestion.prototype.validate = function() {
    var n, f, b, d, e, h, m, l, c, g, j, k, a;
    d = this.checkbuttonClass.val();
    if (this.checkbuttonClass.hasClass("active") && String(this.checkbuttonClass.find("span").html()) !== String(this.tryAgainBtnTxt)) {
        this.storage._checkClicked = "true";
        this.storage._tryClicked = "false";
        b = true;
        n = this.contentClass.find("input[type=text]");
        k = this.parentContainer.offset().left;
        for (j = 0; j < n.length; j++) {
            e = this.contentClass.find("input[type=text]:eq(" + j + ")").val();
            this.myAnswers[j] = e;
            this.contentClass.find("input[type=text]:eq(" + j + ")").attr("readonly", "true");
            if ($.inArray(e, this.correctAnswers[j]) !== -1) {
                if (e !== "") {
                    this.contentClass.find("input[type=text]:eq(" + j + ")").addClass("mark correct-answer");
                }
            } else {
                if (e !== "") {
                    this.contentClass.find("input[type=text]:eq(" + j + ")").addClass("mark wrong-answer");
                }
                b = false;
            }
        }
        this.storeUserInputs();
        this.storage._trylimit = Number(this.attemptNo);
        if (b) {
            this.checkbuttonClass.hide();
            this.showFeedbackMsg(false);
        } else {
            if (Number(this.noOfAttempts) === 1) {
                this.checkbuttonClass.hide();
                this.showFeedbackMsg(true);
            } else {
                if (this.noOfAttempts > 1 && Number(this.attemptNo) === 1) {
                    this.showFeedbackMsg(true);
                    this.checkbuttonClass.find("span").html(this.tryAgainBtnTxt);
                } else {
                    if (this.attemptNo > 1) {
                        this.showFeedbackMsg(true);
                        this.checkbuttonClass.removeClass("active").addClass("show-me defaultBg").find("span").html(this.showMeBtnTxt);
                    }
                }
            }
        }
        if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
            this.pluginEvent(null, b, "started", "instructionq", this.parentContainer.attr("data-objectid"));
        }
    } else {
        if (this.checkbuttonClass.find("span").html() === this.tryAgainBtnTxt) {
            this.storage._tryClicked = "true";
            this.hint.hide();
            this.attemptNo++;
            this.storage._trylimit = Number(this.attemptNo);
            this.resetInputs(false);
            this.persistData();
        } else {
            this.toggleButtons();
            this.persistData();
        }
    }
};
Athena.FillInBlankQuestion.prototype.resetInputs = function(c) {
    var b = this,
        a = 0;
    this.contentClass.find("input[type=text]").removeAttr("readonly");
    if (c) {
        this.hint.find(".hint").hide();
        this.parentContainer.find(".mark").removeClass("mark wrong-answer correct-answer default-correct my-answer");
        this.contentClass.find("input[type=text]").val("");
        this.attemptNo = 1;
        this.feedback.removeClass("hasText").find(".feedback span").html("");
        this.checkbuttonClass.show().removeClass("active show-me defaultBg my-answer");
        this.myAnswers = [];
        delete this.storage._checkClicked;
        delete this.storage._myanswerClicked;
        delete this.storage._showClicked;
        delete this.storage._tryClicked;
        delete this.storage._trylimit;
        this.storeUserInputs();
    } else {
        this.contentClass.find(".wrong-answer").each(function(d, e) {
            b.contentClass.find(e).val("");
        });
        this.contentClass.find(".correct-answer").each(function() {
            a = $(this).index(".mark");
            $(this).attr("readonly", "readonly");
            $(this).addClass("disable");
        });
        this.contentClass.find(".wrong-answer").removeAttr("class");
        b.feedback.find(".feedback").hide();
        b.feedback.removeClass("hasText");
    }
    this.checkbuttonClass.removeClass("active").find("span").html(this.checkBtnTxt);
};
Athena.FillInBlankQuestion.prototype.toggleButtons = function() {
    var f, j, d, g, h, b, e, a, c;
    if (this.checkbuttonClass.hasClass("show-me")) {
        this.storage._showClicked = "true";
        delete this.storage._myanswerClicked;
        this.checkbuttonClass.removeClass("show-me").addClass("my-answer defaultBg").find("span").html(this.myAnswerBtnTxt);
        j = this.contentClass.find("input[type=text]");
        for (f = 0; f < j.length; f++) {
            c = this.correctAnswers[f][0];
            d = this.contentClass.find("input[type=text]:eq(" + f + ")").position().left;
            g = this.contentClass.find("input[type=text]:eq(" + f + ")").position().top;
            h = this.contentClass.find("input[type=text]:eq(" + f + ")").outerWidth(true);
            a = this.contentClass.find("input[type=text]:eq(" + f + ")").outerHeight(true);
            b = this.contentClass.find("input[type=text]:eq(" + f + ")").parent();
            this.contentClass.find("input[type=text]:eq(" + f + ")").val(this.correctAnswers[f][0]);
            this.contentClass.find("input[type=text]:eq(" + f + ")").addClass("mark default-correct");
            this.contentClass.find("input[type=text]:eq(" + f + ")").val(c);
        }
        this.feedback.addClass("hasText").find(".feedback span").html(this.showMeMsg).show();
        if (this.input.contents.show_answer.objectId !== undefined) {
            this.feedback.attr("data-objectid", this.input.contents.show_answer.objectId);
        }
        this.feedback.find(".feedback").show();
    } else {
        if (this.checkbuttonClass.hasClass("my-answer")) {
            this.storage._myanswerClicked = "true";
            delete this.storage._showClicked;
            this.feedback.removeClass("hasText").find(".feedback span").html("");
            this.checkbuttonClass.removeClass("my-answer").addClass("show-me").find("span").html(this.showMeBtnTxt);
            j = this.contentClass.find("input[type=text]");
            for (f = 0; f < j.length; f++) {
                c = this.myAnswers[f];
                d = this.contentClass.find("input[type=text]:eq(" + f + ")").position().left;
                g = this.contentClass.find("input[type=text]:eq(" + f + ")").position().top;
                h = this.contentClass.find("input[type=text]:eq(" + f + ")").outerWidth(true);
                a = this.contentClass.find("input[type=text]:eq(" + f + ")").outerHeight(true);
                b = this.contentClass.find("input[type=text]:eq(" + f + ")").parent();
                this.contentClass.find("input[type=text]:eq(" + f + ")").val(this.myAnswers[f]);
                this.contentClass.find("input[type=text]:eq(" + f + ")").val(c);
                if ($.inArray(this.myAnswers[f], this.correctAnswers[f]) !== -1) {
                    this.contentClass.find("input[type=text]:eq(" + f + ")").removeAttr("class");
                    this.contentClass.find("input[type=text]:eq(" + f + ")").addClass("mark correct-answer");
                } else {
                    this.contentClass.find("input[type=text]:eq(" + f + ")").removeAttr("class");
                    if (this.contentClass.find("input[type=text]:eq(" + f + ")").val().trim().length !== 0) {
                        this.contentClass.find("input[type=text]:eq(" + f + ")").addClass("mark wrong-answer");
                    } else {
                        this.contentClass.find("input[type=text]:eq(" + f + ")").removeAttr("class");
                    }
                }
            }
        }
    }
    this.persistData();
};
Athena.FillInBlankQuestion.prototype.isNoneIncorrect = function() {
    var c, d, b, a = true;
    c = this.contentClass.find("input[type=text]");
    for (d = 0; d < c.length; d++) {
        b = this.contentClass.find("input[type=text]:eq(" + d + ")").val();
        if (b !== "" && $.inArray(b, this.correctAnswers[d]) === -1) {
            a = false;
            break;
        }
    }
    return a;
};
Athena.FillInBlankQuestion.prototype.showFeedbackMsg = function(a) {
    if (this.autoScrollTo !== undefined && this.autoScrollTo) {
        window.scrollTo(0, 0);
    }
    var b = this,
        c;
    if (!a) {
        if (this.input.contents.feedback_correct.objectId !== undefined) {
            this.feedback.attr("data-objectid", this.input.contents.feedback_correct.objectId);
        }
        this.feedback.addClass("hasText").find(".feedback span").html(this.successMsg);
        this.feedbackAutoHide();
    } else {
        if (this.attemptNo === 1 && parseInt(this.noOfAttempts, 10) > 1) {
            if (this.input.contents.feedback_first_incorrect.objectId !== undefined) {
                this.feedback.attr("data-objectid", this.input.contents.feedback_first_incorrect.objectId);
            }
            c = this.isNoneIncorrect() === false ? this.tryAgainMsg : this.incompleteMsg;
            this.feedback.addClass("hasText").find(".feedback").show();
            this.feedback.addClass("hasText").find(".feedback span").html(c).show();
            this.hint.find(".hint span").show();
            this.hint.show();
            this.toggleHint(true);
        } else {
            if ((this.attemptNo > 1 && parseInt(this.noOfAttempts, 10) > 1) || (this.attemptNo === 1 && parseInt(this.noOfAttempts, 10) === 1)) {
                if (this.input.contents.feedback_second_incorrect.objectId !== undefined) {
                    this.feedback.attr("data-objectid", this.input.contents.feedback_second_incorrect.objectId);
                }
                c = this.isNoneIncorrect() === false ? this.failMsg : this.incompleteMsg;
                this.feedback.addClass("hasText").find(".feedback span").html(c).show();
                this.feedbackAutoHide();
            }
        }
    }
};
Athena.FillInBlankQuestion.prototype.feedbackAutoHide = function() {
    var a = this;
    setTimeout(function() {
        a.feedback.find(".feedback").show();
    }, 0);
    setTimeout(function() {
        a.feedback.find(".feedback").hide();
        a.feedback.removeClass("hasText");
    }, 2000);
};
Athena.FillInBlankQuestion.prototype.allValuesEntered = function() {
    var d, a, b = true,
        c = false;
    a = this;
    d = 0;
    this.contentClass.find("input[type=text]").each(function() {
        a.myAnswers[d] = $(this).val();
        d++;
        if (Number($(this).val().trim().length) === 0) {
            b = false;
        } else {
            c = true;
        }
    });
    if ((this.checkEnableAllValue && b) || (!this.checkEnableAllValue && c)) {
        this.checkbuttonClass.addClass("active");
    } else {
        this.checkbuttonClass.removeClass("active");
    }
};
Athena.FillInBlankQuestion.prototype.storeUserInputs = function() {
    this.storage._userInput = JSON.stringify(this.myAnswers);
    this.persistData();
};
Athena.FillInBlankQuestion.prototype.persistData = function() {
    var a = this;
    if ((a.pluginEvent !== undefined) && a.pluginEvent !== null) {
        a.pluginEvent(null, null, null, null, {
            id: a.parentContainer.attr("data-objectid"),
            value: a.storage
        });
    }
};
Athena.FillInBlankQuestion.prototype.preset = function(d) {
    var b, c, a = this;
    if (d !== undefined && d.value !== undefined) {
        this.storage = d.value;
        c = 0;
        b = JSON.parse(this.storage._userInput);
        this.parentContainer.find("input").each(function() {
            if (typeof b[c] !== "undefined") {
                this.value = b[c];
            }
            c++;
        });
    }
    if (String(this.storage._tryClicked) === "true") {
        a.attemptNo = 2;
        delete this.storage._checkClicked;
    } else {
        if (String(this.storage._checkClicked) === "true") {
            setTimeout(function() {
                if (a.storage._trylimit !== null && a.storage._trylimit !== undefined) {
                    a.attemptNo = Number(a.storage._trylimit);
                } else {
                    a.attemptNo = 1;
                }
                b = JSON.parse(a.storage._userInput);
                a.checkbuttonClass.addClass("active");
                a.checkbuttonClass.trigger("click");
                if (String(a.storage._showClicked) === "true") {
                    a.checkbuttonClass.addClass("show-me");
                    a.toggleButtons();
                }
            }, 500);
        }
    }
};
Athena.FreeTextQuestion = function() {
    this.parentContainer = null;
    this.headerClass = null;
    this.questionClass = null;
    this.qImageClass = null;
    this.saveBtnClass = null;
    this.sendBtnClass = null;
    this.symbolBtnClass = null;
    this.inputBoxClass = null;
    this.symbolBtnContainerClass = null;
    this.textAreaComp = null;
    this.palletteObject = null;
    this.scrollbarClass = null;
    this.intScroll = null;
    this.scrollbar = -1;
    this.paletteCallback = null;
    this.resizeCallback = null;
    this.paletteHideCallback = null;
    this.paletteShowCallback = null;
    this.userInput = "";
    this.header = "";
    this.question = "";
    this.questionImageName = "";
    this.saveBtnTxt = "";
    this.sendToNoteBookTxt = "";
    this.symbolImage = [];
    this.imageSourcePath = "assets/images/";
    this.pluginEvent = null;
    this.model = null;
    this.deviceType = "";
    this.removeValidationError = 0;
    this.iPadTabletMode = "";
    this.focus = false;
    this.orienatation = true;
    this.paletteOpened = true;
    this.autoScrollTo = true;
};
Athena.FreeTextQuestion.extend(Athena.BaseClass);
Athena.FreeTextQuestion.prototype.options = function(b, e, f) {
    var d, a, c;
    this.deviceType = this.checkDevices();
    if ((String(navigator.userAgent.match(/iPad/i)) === "iPad" || String(navigator.userAgent.match(/Android/i)) === "Android") && String(navigator.userAgent.match(/mobile/i)) !== "mobile") {
        if ($(window).innerWidth() < $(window).innerHeight()) {
            this.deviceType = "mobile";
            this.iPadTabletMode = "portrait";
        } else {
            this.iPadTabletMode = "landscape";
        }
    }
    this.palletteObject = f;
    for (d in b) {
        if (b.hasOwnProperty(d) && this.hasOwnProperty(d)) {
            this[d] = b[d];
        }
    }
    this.model = e;
    this.question = e.contents.question.value;
    this.questionImageName = e.contents.question_has_image.value.toUpperCase() === "N" ? null : e.contents.question_image.value;
    a = e.lists.symbols;
    for (c = 0; c < a.length; c++) {
        this.symbolImage.push(e.lists.symbols[c].symbol);
    }
};
Athena.FreeTextQuestion.prototype.reset = function() {
    this.parentContainer.find(".text-container").text(" ");
    if (this.paletteHideCallback !== undefined && this.paletteHideCallback !== null) {
        this.paletteHideCallback.call(null);
    }
    Collab.freeTextHideSymbolPopup(this);
};
Athena.FreeTextQuestion.prototype.init = function(n) {
    var o = this,
        e, b, d, h, a, j, m, l, g, f;
    var me = this;
    this.parentContainer.find("." + this.headerClass).html(this.header);
    this.parentContainer.find("." + this.questionClass).html(this.question);
    if (this.model.contents.question.objectId !== undefined) {
        this.parentContainer.find("." + this.questionClass).attr("data-objectid", this.model.contents.question.objectId);
    }
    this.parentContainer.find("." + this.saveBtnClass).html(this.saveBtnTxt);
    this.parentContainer.find("." + this.sendBtnClass).html(this.sendToNoteBookTxt);
    this.symbolBtnClassObj = this.parentContainer.find("." + this.symbolBtnClass);
    this.saveBtnClass = this.parentContainer.find("." + this.saveBtnClass);
    this.sendBtnClass = this.parentContainer.find("." + this.sendBtnClass);
    this.inputBoxClass = this.parentContainer.find("." + this.inputBoxClass);
    this.textAreaComp = this.parentContainer.find("." + this.textAreaComp);
    this.scrollbarClass = this.parentContainer.find("." + this.scrollbarClass);
    if (this.questionImageName !== null) {
        this.parentContainer.find("." + this.questionClass).hide();
        this.parentContainer.find("." + this.qImageClass).css("background", "url(" + this.imageSourcePath + this.questionImageName + ") no-repeat");
        this.parentContainer.find(".ftq_question_container").show();
        this.parentContainer.find(".ftq_question_container ." + this.questionClass).show();
        if (this.model.contents.question_image.objectId !== undefined) {
            this.parentContainer.find("." + this.qImageClass).attr("data-objectid", this.model.contents.question_image.objectId);
        }
    } else {
        this.parentContainer.find("." + this.questionClass).show();
        this.parentContainer.find(".ftq_question_container").hide();
    }
    if (this.userInput !== "") {
        this.inputBoxClass.html(this.userInput);
        o.valuesEntered();
    }
    m = this.parentContainer.find(this.symbolBtnClassObj);
    for (e = 0; e < this.symbolImage.length; e++) {
        l = m.clone();
        l.html(this.symbolImage[e].value);
        if (this.symbolImage[e].objectId !== undefined) {
            l.attr("data-objectid", this.symbolImage[e].objectId);
        }
        l.insertAfter(this.parentContainer.find(this.symbolBtnClassObj));
    }
    m.remove();
    this.saveBtnClass.bind("mousedown", function() {
        o.sendNote();
    });
    this.sendBtnClass.bind("mousedown", function() {
        o.sendNote();
    });
    if ((new RegExp("mobile", "igm")).test(navigator.userAgent) && !(new RegExp("ipad", "igm")).test(navigator.userAgent)) {
        this.parentContainer.find("." + this.symbolBtnClass).hide();
    }
    this.inputBoxClass.bind("focus", function() {
        o.trackInteraction();
    });
    if (this.deviceType !== "mobile") {
        this.parentContainer.find("." + this.symbolBtnClass).click(function() {
            o.showPalette();
        });
    } else {
        if (!(new RegExp("mobile", "igm")).test(navigator.userAgent)) {
            this.parentContainer.find("." + this.symbolBtnClass).click(function() {
                o.showPalette();
            });
        } else {
            if ((new RegExp("ipad", "igm")).test(navigator.userAgent)) {
                this.parentContainer.find("." + this.symbolBtnClass).click(function() {
                    o.showPalette();
                });
            }
        }
    }
    f = this.parentContainer.find(".text-container");
    
    f.on('input', function () {
    	Collab.freeTextInputChanged(me.parentContainer.attr('data-objectid'), me.parentContainer.find('.text-container').html());
    });
    
    f.focusout(function() {
        $("footer").show();
        if (o.iPadTabletMode === "landscape" || o.iPadTabletMode === "portrait") {
            if (o.autoScrollTo !== undefined && o.autoScrollTo) {
                window.scrollTo(0, 0);
            }
        }
        if (o.deviceType === "mobile") {
            o.focus = false;
            if (o.paletteHideCallback !== undefined && o.paletteHideCallback !== null) {
                o.paletteHideCallback.call(null);
            }
            Collab.freeTextShowSymbolPopup(me);
            $("footer").show();
        }
    });
    this.parentContainer.find(".text-container").focus(function() {
        var c = o.parentContainer.position().top;
        o.deviceType = o.checkDevices();
        if (o.resizeCallback !== undefined && o.resizeCallback !== null) {
            o.resizeCallback.call(null);
        }
        o.focus = true;
        if (o.paletteCallback !== undefined && o.paletteCallback !== null) {
            o.paletteCallback.call(null, o.parentContainer.find(".overview_contenteditable"), true);
        }
        if (((new RegExp("mobile", "igm")).test(navigator.userAgent) && !(new RegExp("ipad", "igm")).test(navigator.userAgent)) && $(window).width() < 650) {
            $(".math-palette").find("section").css("overflow-x", "scroll");
            setTimeout(function() {
                if (o.paletteShowCallback !== undefined && o.paletteShowCallback !== null) {
                    o.paletteShowCallback.call(null);
                }
                $("html, body").animate({
                    scrollTop: $(document).height()
                }, 200);
                $("footer").hide();
                $(".math-palette").css({
                    bottom: "0px",
                    "z-index": 20
                });
                $("#activity-" + Athena.dlolocaleModel.selectedActivity).animate({
                    scrollTop: c - 100
                }, 200);
            }, 500);
        }
    });
    if (n !== undefined) {
        this.callback();
    }
    this.addScroll();
    try {
        if (this.model.contents.user_input !== undefined && this.model.contents.user_input.objectId !== undefined) {
            this.parentContainer.find(".tinyscrollbar_container").find(".overview_contenteditable").attr("data-objectid", this.model.contents.user_input.objectId);
        }
    } catch (k) {
        this.removeValidationError++;
    }
    if ((String(navigator.userAgent.match(/iPad/i)) === "iPad" || String(navigator.userAgent.match(/Android/i)) === "Android") && String(navigator.userAgent.match(/mobile/i)) !== "mobile") {
        $(window).on("orientationchange", function(i) {
            try {
                o.parentContainer.find(".text-container").blur();
                if (o.resizeCallback !== undefined && o.resizeCallback !== null) {
                    o.resizeCallback.call(null);
                }
                $("footer").show();
            } catch (c) {
                debug("");
            }
        });
    }
    $(window).resize(function() {
        o.fixPosPallette("true");
    });
    o.fixPosPallette("false");
};
Athena.FreeTextQuestion.prototype.fixPosPallette = function(a) {
    if (((new RegExp("mobile", "igm")).test(navigator.userAgent) && !(new RegExp("ipad", "igm")).test(navigator.userAgent))) {
        if (a) {
            $(".math-palette").addClass("mobile").css("display", "none");
        } else {
            $(".math-palette").addClass("mobile").removeAttr("style").css({
                opacity: 1,
                display: "block"
            });
        }
    } else {
        $(".math-palette").removeClass("mobile");
        $(".math-palette section").css("overflow-x", "hidden");
    }
};
Athena.FreeTextQuestion.prototype.trackInteraction = function(a) {
    if ((this.pluginEvent !== undefined) && this.pluginEvent !== null) {
        this.pluginEvent(null, null, "started", "performance", this.parentContainer.attr("data-objectid"));
    }
};
Athena.FreeTextQuestion.prototype.showPalette = function(h) {
    if (this.iPadTabletMode === "landscape") {
        this.paletteOpened = false;
    }
    var a, b = this,
        f, g, c, i, d;
    if (b.paletteCallback !== undefined && b.paletteCallback !== null) {
        b.paletteCallback.call(null, b.parentContainer.find(".overview_contenteditable"), false);
    }
    a = b.parentContainer.find(".tinyscrollbar_container").position().left + b.parentContainer.find(".tinyscrollbar_container").width() + 32;
    f = b.parentContainer.find(".tinyscrollbar_container").offset().top;
    g = $(".math-palette").height();
    
    if (!window.__hasRegisteredPaletteClose) {
    	window.__hasRegisteredPaletteClose = true;
    	$(".math-palette .button-close").on('click', function () {
    		Collab.freeTextHideSymbolPopup();
    	});
    }
    
    c = b.parentContainer.find(".main-Container").height();
    d = b.parentContainer.find(".main-Container").offset().top;
    d = d + (c - g);
    $(".math-palette").css({
        left: a,
        top: d
    });
    i = a + $(".math-palette").width();
    if (i > $(window).width()) {
        $(".math-palette").css({
            left: a - $(".math-palette").width()
        });
    }
    if ($(".math-palette").offset().top < 30) {
        $(".math-palette").css("top", d + Math.abs($(".math-palette").offset().top) + 70);
    }
    
    Collab.freeTextShowSymbolPopup(this);

};
Athena.FreeTextQuestion.prototype.addScroll = function() {
    var b, f, a, e, g, d, c;
    b = this;
    this.parentContainer.find(".tinyscrollbar_container").tinyscrollbar();
    this.parentContainer.find(".tinyscrollbar_container").find(".text-container").bind("keyup", function(h) {
        b.updateScroll();
    });
};
Athena.FreeTextQuestion.prototype.updateScroll = function() {
    var b, f, a, e, g, d, c;
    b = this;
    g = b.parentContainer.find(".tinyscrollbar_container").data("plugin_tinyscrollbar");
    a = parseInt(b.parentContainer.find(".tinyscrollbar_container .thumb").css("height"), 10);
    e = parseInt(b.parentContainer.find(".tinyscrollbar_container .thumb").css("top"), 10);
    d = parseInt(b.parentContainer.find(".tinyscrollbar_container .track").css("height"), 10);
    c = a + e;
    if (c < d && c > d - 75) {
        g.update("bottom");
    } else {
        g.update("relative");
    }
};
Athena.FreeTextQuestion.prototype.valuesEntered = function(b) {
    var a;
    a = this;
    if (this.inputBoxClass.html() !== "") {
        this.sendBtnClass.addClass("active");
        this.saveBtnClass.addClass("active");
    } else {
        this.sendBtnClass.removeClass("active");
        this.saveBtnClass.removeClass("active");
    }
    a.updateScrollBar();
};
Athena.FreeTextQuestion.prototype.sendNote = function() {
    var a = this.parentContainer.find(".overview_contenteditable");
    this.pluginEvent(null, null, null, null, {
        id: this.parentContainer.attr("data-objectid"),
        value: a.find(".text-container").html()
    });
};
Athena.FreeTextQuestion.prototype.updateScrollBar = function() {
    if (navigator.userAgent.indexOf("iPad") !== -1 || navigator.userAgent.indexOf("Android") !== -1) {
        this.scrollbarClass.css({
            zIndex: 0
        });
        this.inputBoxClass.css({
            zIndex: 1
        });
    } else {
        this.scrollbarClass.css({
            zIndex: 1
        });
        this.inputBoxClass.css({
            zIndex: 0
        });
    }
    this.textAreaComp.tinyscrollbar_update("relative");
    if (!this.intScroll) {
        this.textAreaComp.tinyscrollbar_update("bottom");
    }
};
Athena.FreeTextQuestion.prototype.preset = function(a) {
    this.parentContainer.find(".overview_contenteditable").html('<div class="text-container" contenteditable="true">' + a.value + "</div>");
};
Athena.SliderRevealItems = function() {
    this.name = "SliderRevealItems";
    this.content_array = [];
    this.fadeTime = 0;
    this.itemCount = 0;
    this.adjustedOffset = 0;
    this.sliderDropPoint = 0;
    this.type = 0;
    this.contentContainerDiv = "";
    this.contentContainerClass = "";
    this.axis = "";
    this.instructionTextDiv = "";
    this.contentTextDiv = "";
    this.contentPrefix = "";
    this.sliderContainerDiv = "";
    this.sliderContentDiv = "";
    this.orderedItems = "";
    this.draggablePointer = "";
    this.sliderPanelDiv = "";
    this.imageContainerDiv = "";
    this.imageContentDiv = "";
    this.imageContent = null;
    this.useFade = null;
    this.sliderPrefix = null;
    this.location = null;
    this.visibility = null;
    this.parentContainer = null;
    this.dropObjId = null;
    this.clonedSliderObject = null;
    this.imagePath = "assets/images/";
    this.pluginEvent = null;
    this.reduce_font = null;
    this.font_size = null;
    this.tableMode = null;
    this.deviceType = "";
};
Athena.SliderRevealItems.extend(Athena.BaseClass);
Athena.SliderRevealItems.prototype.options = function(a) {
    var b;
    for (b in a) {
        if (a.hasOwnProperty(b)) {
            this[b] = a[b];
        }
    }
};
Athena.SliderRevealItems.prototype.init = function() {
    this.deviceType = this.checkDevices();
    this.content_array = this.model.lists.content_list;
    this.adjustedOffset = this.model.contents.offSet !== undefined ? Number(this.model.contents.offSet.value) : 0;
    this.axis = this.model.contents.axis.value;
    if (this.model.contents.firstEnable !== undefined) {
        this.tableMode = this.model.contents.firstEnable.value;
    }
    this.orderedItems = this.model.contents.selectionType.value;
    this.useFade = this.model.contents.useFade.value;
    this.fadeTime = Number(this.model.contents.fadeTime.value);
    this.sliderDropPoint = Number(this.model.contents.sliderDropPoint.value);
    this.type = Number(this.model.contents.type.value);
    this.reduce_font = typeof this.model.contents.reduce_font === "undefined" ? false : this.model.contents.reduce_font.value;
    var q = this,
        c, f, e, l, h, a, b, g, o, p, m, d, k, n;
    this.itemCount = this.content_array.length;
    if (typeof this.contentContainerDiv === "string") {
        this.contentContainerDiv = this.parentContainer.find("." + this.contentContainerDiv);
    }
    if (typeof this.contentContainerClass === "string") {
        this.contentContainerClass = this.parentContainer.find("." + this.contentContainerClass);
    }
    if (typeof this.instructionTextDiv === "string") {
        this.instructionTextDiv = this.parentContainer.find("." + this.instructionTextDiv);
    }
    if (typeof this.sliderContainerDiv === "string") {
        this.sliderContainerDiv = this.parentContainer.find("." + this.sliderContainerDiv);
    }
    if (typeof this.sliderContentDiv === "string") {
        this.sliderContentDiv = this.parentContainer.find("." + this.sliderContentDiv);
    }
    if (typeof this.draggablePointer === "string") {
        this.draggablePointer = this.parentContainer.find("." + this.draggablePointer);
    }
    if (typeof this.imageContainerDiv === "string") {
        this.imageContainerDiv = this.parentContainer.find("." + this.imageContainerDiv);
    }
    if (typeof this.sliderPanelDiv === "string") {
        this.sliderPanelDiv = this.parentContainer.find("." + this.sliderPanelDiv);
    }
    this.visibility = false;
    this.location = "append";
    if ($(window).innerWidth() > 700) {
        this.deviceType = "";
    }
    this.cloneElement(this.sliderDropPoint, this.sliderContainerDiv, this.sliderContentDiv, this.sliderPrefix, this.location, null, this.visibility, function(i) {
        q.clonedSliderObject = i;
        for (f = 0; f < q.clonedSliderObject.length; f++) {
            if (Number(this.sliderDropPoint) === 3) {
                if (this.deviceType === "mobile") {
                    if (Number(f) === 0) {
                        q.clonedSliderObject[f].attr("dropTop", 0);
                    } else {
                        q.clonedSliderObject[f].attr("dropTop", (150 * f) + (Number(f - 1) * 10));
                    }
                } else {
                    if (Number(f) === 0) {
                        q.clonedSliderObject[f].attr("dropTop", 0);
                    } else {
                        if (Number(f) === 1) {
                            q.clonedSliderObject[f].attr("dropTop", 225);
                        } else {
                            q.clonedSliderObject[f].attr("dropTop", 450);
                        }
                    }
                }
            } else {
                if (Number(this.sliderDropPoint) === 2) {
                    if (this.deviceType === "mobile") {
                        if (Number(f) === 0) {
                            q.clonedSliderObject[f].attr("dropTop", 0);
                        } else {
                            q.clonedSliderObject[f].attr("dropTop", (310 * f) + (Number(f - 1) * 10));
                        }
                    } else {
                        if (Number(f) === 0) {
                            q.clonedSliderObject[f].attr("dropTop", 0);
                        } else {
                            q.clonedSliderObject[f].attr("dropTop", 450);
                        }
                    }
                } else {
                    if (Number(this.sliderDropPoint) === 4) {
                        if (this.deviceType === "mobile") {
                            if (Number(f) === 0) {
                                q.clonedSliderObject[f].attr("dropTop", 0);
                            } else {
                                q.clonedSliderObject[f].attr("dropTop", (96 * f) + (Number(f - 1) * 10));
                            }
                        } else {
                            if (Number(f) === 0) {
                                q.clonedSliderObject[f].attr("dropTop", 0);
                            } else {
                                q.clonedSliderObject[f].attr("dropTop", (150 * f));
                            }
                        }
                    } else {
                        if (Number(this.sliderDropPoint) === 5) {
                            if (this.deviceType === "mobile") {
                                if (Number(f) === 0) {
                                    q.clonedSliderObject[f].attr("dropTop", 0);
                                } else {
                                    q.clonedSliderObject[f].attr("dropTop", (70 * f) + (Number(f - 1) * 10));
                                }
                            } else {
                                if (Number(f) === 0) {
                                    q.clonedSliderObject[f].attr("dropTop", 0);
                                } else {
                                    if (Number(f) === 4) {
                                        q.clonedSliderObject[f].attr("dropTop", 450);
                                    } else {
                                        q.clonedSliderObject[f].attr("dropTop", (113 * f));
                                    }
                                }
                            }
                        } else {
                            if (Number(this.sliderDropPoint) === 6) {
                                if (this.deviceType === "mobile") {
                                    if (Number(f) === 0) {
                                        q.clonedSliderObject[f].attr("dropTop", 0);
                                    } else {
                                        q.clonedSliderObject[f].attr("dropTop", (54 * f) + (Number(f - 1) * 10));
                                    }
                                } else {
                                    if (Number(f) === 0) {
                                        q.clonedSliderObject[f].attr("dropTop", 0);
                                    } else {
                                        q.clonedSliderObject[f].attr("dropTop", (87 * f));
                                    }
                                }
                            } else {
                                if (Number(this.sliderDropPoint) === 7) {
                                    if (this.deviceType === "mobile") {
                                        if (Number(f) === 0) {
                                            q.clonedSliderObject[f].attr("dropTop", 0);
                                        } else {
                                            q.clonedSliderObject[f].attr("dropTop", (43 * f) + (Number(f - 1) * 10));
                                        }
                                    } else {
                                        if (Number(f) === 0) {
                                            q.clonedSliderObject[f].attr("dropTop", 0);
                                        } else {
                                            q.clonedSliderObject[f].attr("dropTop", (75 * f));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    });
    if (String(this.model.contents.state.value) === "fullframe") {
        this.cloneElement(this.itemCount, this.imageContainerDiv, this.parentContainer.find("." + this.imageContentDiv), this.contentPrefix, this.location, null, this.visibility, function(i) {
            this.countImages = 0;
            d = 0;
            for (f = 0; f < this.content_array.length; f++) {
                for (e = 1; e <= Object.keys(this.content_array[f]).length; e++) {
                    if (this.content_array[f]["image_" + e] !== undefined) {
                        d++;
                    }
                }
            }
            for (f = 0; f < this.content_array.length; f++) {
                n = 1;
                for (e = 1; e <= Object.keys(this.content_array[f]).length; e++) {
                    this.imageContainerDiv.find("#" + this.contentPrefix + f).attr("class", "contentText");
                    if (this.content_array[f]["content_" + e] !== undefined) {
                        this.imageContainerDiv.find("#" + this.contentPrefix + f).find("label").empty().append(this.content_array[f]["content_" + e].value);
                        if (this.content_array[f]["content_" + e].objectId !== undefined) {
                            this.imageContainerDiv.find("#" + this.contentPrefix + f).attr("data-objectid", this.content_array[f]["content_" + e].objectId);
                        }
                    }
                    if (this.content_array[f]["image_" + e] !== undefined) {
                        this.calculateImageWidth(this.imageContainerDiv.find("#" + this.contentPrefix + f).find("img"), this.content_array, f, e, d);
                        n = 0;
                    }
                }
                if (n) {
                    if (!this.imageContainerDiv.find("#" + this.contentPrefix + f).find("img").hasClass("dontRemove")) {
                        this.imageContainerDiv.find("#" + this.contentPrefix + f).find("img").remove();
                    }
                }
            }
            q.setEvents();
        });
    } else {
        if (String(q.model.contents.state.value) === "table") {
            if (typeof q.content_array !== "undefined") {
                q.buildTable(q.content_array);
                q.contentContainerDiv.find("img").unbind("load");
                q.contentContainerDiv.find("img").bind("load", function() {
                    q.create();
                });
                q.setEvents();
                this.reduce_font_size = (String(typeof this.model.contents.reduce_table_font) === "undefined") ? false : this.model.contents.reduce_table_font.value;
                this.reduce_font_size = this.reduce_font_size === "true" ? true : false;
                if (this.reduce_font_size) {
                    this.font_size = (String(typeof this.model.contents.font_size) === "undefined") ? 15 : this.model.contents.font_size.value;
                    this.contentContainerDiv.find("table td,table th").css({
                        "font-size": this.font_size + "px"
                    });
                    if (typeof MathJax !== "undefined") {
                        MathJax.Hub.Queue(["Typeset", MathJax.Hub, this.parentContainer.parent().toArray()[0].querySelector(".slider-container-all"), function() {
                            q.contentContainerDiv.find("table td,table th").find("td,th,.mi,.mtext,.mo,.mn").css({
                                "font-size": q.font_size + "px"
                            });
                        }]);
                    }
                }
            }
        } else {
            this.cloneElement(this.itemCount, this.contentContainerDiv, this.contentContainerClass, this.contentPrefix, this.location, null, this.visibility, function(i) {
                this.countImages = 0;
                d = 0;
                for (f = 0; f < this.content_array.length; f++) {
                    for (e = 1; e <= Object.keys(this.content_array[f]).length; e++) {
                        if (this.content_array[f]["image_" + e] !== undefined && this.content_array[f]["image_" + e].value !== undefined && this.content_array[f]["image_" + e].value !== "") {
                            d++;
                        }
                    }
                }
                for (f = 0; f < this.content_array.length; f++) {
                    h = 0;
                    a = 0;
                    p = false;
                    for (e = 1; e <= Object.keys(this.content_array[f]).length; e++) {
                        this.imageContainerDiv.find("#" + this.contentPrefix + f).attr("class", "contentText");
                        if (this.content_array[f]["content_" + e] !== undefined) {
                            if (this.reduce_font) {
                                this.content_array[f]["content_" + e].value = this.addMathSizeAttributeinMathML(this.content_array[f]["content_" + e].value, "15px");
                            }
                            if (Number(h) === 0) {
                                this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.contentTextDiv).append(this.content_array[f]["content_" + e].value);
                                if (this.reduce_font) {
                                    this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.contentTextDiv).css("font-size", "15px");
                                }
                                if (this.content_array[f]["content_" + e].objectId !== undefined) {
                                    this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.contentTextDiv).attr("data-objectid", this.content_array[f]["content_" + e].objectId);
                                }
                            } else {
                                b = this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.contentTextDiv).clone().appendTo(this.contentContainerDiv.find("#" + this.contentPrefix + f));
                                b.empty().append(this.content_array[f]["content_" + e].value);
                                if (this.reduce_font) {
                                    b.css("font-size", "15px");
                                }
                                if (this.content_array[f]["content_" + e].objectId !== undefined) {
                                    b.attr("data-objectid", this.content_array[f]["content_" + e].objectId);
                                }
                            }
                        } else {
                            if (Number(h) === 0) {
                                this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.contentTextDiv).remove();
                            }
                        }
                        if (this.content_array[f]["image_" + e] !== undefined && this.content_array[f]["image_" + e].value !== undefined && this.content_array[f]["image_" + e].value !== "") {
                            p = true;
                            if (Number(h) === 0) {
                                this.calculateImageWidth(this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.imageContent).find("img"), this.content_array, f, e, d);
                            } else {
                                g = this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.imageContent).clone().appendTo(this.contentContainerDiv.find("#" + this.contentPrefix + f));
                                this.calculateImageWidth(g.find("img"), this.content_array, f, e, d);
                            }
                        } else {
                            if (Number(h) === 0) {
                                this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.imageContent).remove();
                            }
                        }
                        h++;
                    }
                }
                if (!p) {
                    q.setEvents();
                }
            });
        }
    }
    this.contentContainerDiv.mCustomScrollbar({
        advanced: {
            updateOnContentResize: true
        }
    });
    this.imageContainerDiv.mCustomScrollbar({
        advanced: {
            updateOnContentResize: true
        }
    });
    if (this.deviceType === "mobile") {
        this.addTableScrollbar(0);
    }
    for (f = 0; f < this.model.lists.instruction_list.length; f++) {
        k = "";
        if (this.model.lists.instruction_list[f].option.objectId !== undefined) {
            this.imageContainerDiv.find("#" + this.contentPrefix + f).attr("data-objectid", this.model.lists.instruction_list[f].option.objectId);
            k = '<label class="instruction-text" data-objectid="' + this.model.lists.instruction_list[f].option.objectId + '">';
        } else {
            k = '<label class="instruction-text">';
        }
        this.instructionTextDiv.append(k + this.model.lists.instruction_list[f].option.value + "</label>");
        this.parentContainer.find(".drag-pointer").show();
    }
    $(window).on("resize", function(i) {
        q.reAlignTheContent();
    });
};
Athena.SliderRevealItems.prototype.reAlignTheContent = function() {
    var h, c, b, f, g, e, a, d, k;
    h = this;
    if (h.deviceType === "mobile") {
        for (c = 0; c < h.content_array.length; c++) {
            f = "false";
            g = "false";
            for (b = 1; b <= Object.keys(h.content_array[c]).length; b++) {
                if (h.content_array[c]["content_" + b] !== undefined) {
                    f = "true";
                }
                if (h.content_array[c]["image_" + b] !== undefined) {
                    g = "true";
                }
                e = Number(h.contentContainerDiv.find("#" + h.contentPrefix + c).width());
                if (h.contentContainerDiv.find("#" + h.contentPrefix + c).find("." + h.imageContent) !== undefined) {
                    a = Number(h.contentContainerDiv.find("#" + h.contentPrefix + c).find("." + h.imageContent).width());
                } else {
                    a = 0;
                }
                if (h.contentContainerDiv.find("#" + h.contentPrefix + c).find("." + h.imageContent).css("margin-left") !== undefined) {
                    k = Number(h.contentContainerDiv.find("#" + h.contentPrefix + c).find("." + h.imageContent).css("margin-left").split("px")[0]);
                } else {
                    k = 0;
                }
                if (String(f) === "true" && String(g) === "true") {
                    d = (e - (a + k));
                    h.contentContainerDiv.find("#" + h.contentPrefix + c).find("." + h.contentTextDiv).css("width", (d - 2) + "px");
                }
            }
        }
    }
};
Athena.SliderRevealItems.prototype.calculateImageWidth = function(d, a, e, c, f) {
    var b = this;
    d.load(function() {
        b.countImages++;
        if (b.reduce_font) {
            $(this).css("width", "90%");
            b.contentContainerDiv.mCustomScrollbar("update");
            b.imageContainerDiv.mCustomScrollbar("update");
        }
        if (Number(f) === Number(b.countImages)) {
            b.create();
            b.setEvents();
        }
        if (a[e]["image_" + c].objectId !== undefined) {
            $(this).attr("data-objectid", a[e]["image_" + c].objectId);
        }
    }).attr("src", this.imagePath + a[e]["image_" + c].value.trim());
};
Athena.SliderRevealItems.prototype.buildTable = function(b) {
    var g, d, f, e, n, p, m, c, q, l, o = this,
        a, h;
    h = typeof o.model.contents.showHeader === "undefined" ? "true" : o.model.contents.showHeader.value;
    p = document.createElement("table");
    l = 0;
    for (g = 0, f = 0; g < b.length; g++) {
        if (String(o.model.contents.headerAxis.value) === "y-axis") {
            l = 0;
        }
        if ((l === 0 && h === "true") || l !== 0) {
            m = document.createElement("tr");
            for (n in b[g]) {
                if (b[g].hasOwnProperty(n)) {
                    if (String(b[g][n].mode) !== "text") {
                        if (b[g].hasOwnProperty(n)) {
                            if (String(o.model.contents.headerAxis.value) === "x-axis") {
                                if (Number(l) === 0) {
                                    c = document.createElement("th");
                                } else {
                                    c = document.createElement("td");
                                }
                            } else {
                                if (String(o.model.contents.headerAxis.value) === "y-axis") {
                                    if (Number(l) === 0) {
                                        c = document.createElement("th");
                                    } else {
                                        c = document.createElement("td");
                                    }
                                    l++;
                                }
                            }
                            q = typeof b[g][n].mode === "undefined" ? "" : b[g][n].mode.value;
                            if (b[g][n].colspan !== undefined) {
                                $(c).attr("colspan", b[g][n].colspan.value);
                            }
                            $(c).addClass(q);
                            c.innerHTML = b[g][n].value;
                            m.appendChild(c);
                            if (Number(g) !== 0) {
                                $(m).attr("id", this.contentPrefix + (g - 1));
                            }
                            if (b[g][n].objectId !== undefined) {
                                $(c).attr("data-objectid", b[g][n].objectId);
                            }
                        }
                    }
                }
            }
        }
        if (String(o.model.contents.headerAxis.value) !== "y-axis") {
            l++;
        }
        if (m) {
            p.appendChild(m);
        }
    }
    if (this.deviceType === "mobile") {
        if (this.model.contents.state.value === "table") {
            if (this.contentContainerDiv.find(".table-scroll-container").attr("customScrollbar") !== "added") {
                this.contentContainerDiv.find(".table-scroll-container").append($(p));
                this.contentContainerDiv.find(".table-scroll-container").attr("customScrollbar", "added");
                this.contentContainerDiv.find(".table-scroll-container").mCustomScrollbar({
                    scrollInertia: 100,
                    horizontalScroll: true,
                    autoHideScrollbar: true
                });
                this.contentContainerDiv.find(".table-scroll-container").find("table").parent().css("width", "600px");
                this.contentContainerDiv.find(".table-scroll-container").mCustomScrollbar("update");
                this.contentContainerDiv.find(".table-scroll-container").css("height", this.contentContainerDiv.find(".table-scroll-container").find("table").height() + "px");
                this.sliderPanelDiv.addClass("hasCustomScrollbar");
            }
        }
    } else {
        this.contentContainerDiv.append($(p));
    }
    for (g = 0, f = 0; g < b.length; g++) {
        for (n in b[g]) {
            if (b[g].hasOwnProperty(n)) {
                if (String(b[g][n].mode) === "text") {
                    a = document.createElement("div");
                    $(a).html(b[g][n].value);
                    if (b[g][n].objectId !== undefined) {
                        $(a).attr("data-objectid", b[g][n].objectId);
                    }
                    if (Number(g) !== 0) {
                        $(a).attr("id", this.contentPrefix + (g - 1));
                        $(a).attr("class", "contentText");
                    }
                    this.contentContainerDiv.append($(a));
                }
            }
        }
    }
};
Athena.SliderRevealItems.prototype.create = function() {
    var m, b, d, f, k, e, a, g, l, n, h, c, p, o;
    b = this.itemCount;
    e = b - 1;
    a = this.parentContainer.find(".drag-bar").height();
    o = this;
    if (this.type === 1) {
        this.sliderContainerDiv.addClass("slide-bar-" + this.sliderDropPoint);
        for (d = 0; d <= e; d++) {
            this.sliderContainerDiv.find("#" + this.sliderPrefix + d).height(a / Number(this.sliderDropPoint));
        }
    }
    for (f = 0; f < this.content_array.length; f++) {
        l = "false";
        n = "false";
        for (d = 1; d <= Object.keys(this.content_array[f]).length; d++) {
            if (this.content_array[f]["content_" + d] !== undefined) {
                l = "true";
            }
            if (this.content_array[f]["image_" + d] !== undefined) {
                n = "true";
            }
            h = Number(this.contentContainerDiv.find("#" + this.contentPrefix + f).width());
            if (this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.imageContent) !== undefined) {
                c = Number(this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.imageContent).width());
            } else {
                c = 0;
            }
            if (this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.imageContent).css("margin-left") !== undefined) {
                p = Number(this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.imageContent).css("margin-left").split("px")[0]);
            } else {
                p = 0;
            }
            if (String(l) === "true" && String(n) === "true") {
                g = (h - (c + p));
                this.contentContainerDiv.find("#" + this.contentPrefix + f).find("." + this.contentTextDiv).css("width", (g - 2) + "px");
            }
        }
    }
};
Athena.SliderRevealItems.prototype.setEvents = function() {
    var b, a = this,
        e, d;
    this.create();
    e = function() {
        if ($(this).text() !== "") {
            $(this).css("color", "transparent");
        } else {
            $(this).css("opacity", "0");
        }
    };
    setTimeout(function() {
        a.parentContainer.find(".text-area").mCustomScrollbar("destroy");
    }, 0);

    function c(f) {
        if (typeof MathJax !== "undefined") {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub, a.parentContainer.parent().toArray()[0].querySelector(".slider-container-all"), function() {
                if (String(a.model.contents.state.value) !== "table") {
                    a.parentContainer.find("#" + a.contentPrefix + f).css("display", "none");
                    a.parentContainer.find("#" + a.contentPrefix + f).css("visibility", "inherit");
                    if (f === 0) {
                        a.parentContainer.find("#" + a.contentPrefix + f).css("display", "block");
                    }
                }
                if (f === a.content_array.length - 1) {
                    a.parentContainer.find(".text-area").mCustomScrollbar({
                        advanced: {
                            updateOnContentResize: true
                        }
                    });
                }
            }]);
        } else {
            if (String(a.model.contents.state.value) !== "table") {
                a.parentContainer.find("#" + a.contentPrefix + f).css("display", "none");
                a.parentContainer.find("#" + a.contentPrefix + f).css("visibility", "inherit");
                if (f === 0) {
                    a.parentContainer.find("#" + a.contentPrefix + f).css("display", "block");
                }
            }
            if (f === a.content_array.length - 1) {
                a.parentContainer.find(".text-area").mCustomScrollbar({
                    advanced: {
                        updateOnContentResize: true
                    }
                });
            }
        }
    }
    for (b = 0; b < this.content_array.length; b++) {
        if (String(this.model.contents.state.value) !== "table") {
            this.parentContainer.find("#" + this.contentPrefix + b).css("visibility", "hidden");
        } else {
            this.parentContainer.find("#" + this.contentPrefix + b).find("td").each(e);
            if (String(this.parentContainer.find("#" + this.contentPrefix + b).prop("tagName")) === "DIV") {
                this.parentContainer.find("#" + this.contentPrefix + b).css("display", "none");
            }
        }
        c(b);
    }
    if (String(this.model.contents.state.value) === "table") {
        this.parentContainer.find("#" + this.contentPrefix + "0").find("td").each(function() {
            $(this).removeAttr("style");
        });
        if (a.reduce_font_size) {
            a.contentContainerDiv.find("table td,table th").css({
                "font-size": a.font_size + "px"
            });
        }
        if (String(this.parentContainer.find("#" + this.contentPrefix + "0").prop("tagName")) === "DIV") {
            this.parentContainer.find("#" + this.contentPrefix + "0").css("display", "none");
        }
        if (this.tableMode === "false") {
            this.parentContainer.find("#" + this.contentPrefix + "0").find("td").css("color", "transparent");
        }
    } else {
        this.parentContainer.find("#" + this.contentPrefix + "0").css("display", "block");
    }
    this.parentContainer.find("#" + this.contentPrefix + "0").attr("isDropped", "true");
    this.addDragEvent(this.draggablePointer, this.sliderContainerDiv, this.axis, undefined, undefined, this.dragCallback, undefined);
    d = function(f, h, g, i) {
        f.dropCallBack(f, h, g, i);
    };
    for (b = 0; b < this.clonedSliderObject.length; b++) {
        this.addDropEvent(this.clonedSliderObject[b], d);
    }
};
Athena.SliderRevealItems.prototype.selectSliderType = function() {
    var a, c, b, h, d, f, g, e, k = this,
        l;
    g = null;
    l = function() {
        if ($(this).text() !== "") {
            $(this).css("color", "transparent");
        } else {
            $(this).css("opacity", "0");
        }
    };
    if (this.orderedItems === "all") {
        e = this.itemCount - (Number(this.dropObjId) + 1);
        for (c = 1; c <= e; c++) {
            h = this.parentContainer.find("#" + this.contentPrefix + (Number(this.dropObjId) + c));
            f = h.attr("isDropped");
            if (String(f) === "true") {
                if (String(this.model.contents.state.value) === "table") {
                    h.find("td").each(l);
                    if (String(h.prop("tagName")) === "DIV") {
                        h.hide();
                    }
                } else {
                    h.hide();
                }
            }
        }
        for (c = 0; c <= this.dropObjId; c++) {
            h = this.parentContainer.find("#" + this.contentPrefix + c);
            f = h.attr("isDropped");
            if (String(this.useFade) === "true") {
                if (Number(this.fadeTime) !== 0) {
                    if (f === undefined || f === null || String(f) === "false") {
                        if (String(this.model.contents.state.value) === "table") {
                            h.find("td").removeAttr("style");
                            if (k.reduce_font_size) {
                                k.contentContainerDiv.find("table td,table th").css({
                                    "font-size": k.font_size + "px"
                                });
                            }
                            if (String(h.prop("tagName")) === "DIV") {
                                h.hide().fadeIn(this.fadeTime);
                            }
                        } else {
                            h.hide().fadeIn(this.fadeTime);
                        }
                        h.attr("isDropped", "true");
                    }
                } else {
                    if (String(this.model.contents.state.value) === "table") {
                        h.find("td").removeAttr("style");
                        if (k.reduce_font_size) {
                            k.contentContainerDiv.find("table td,table th").css({
                                "font-size": k.font_size + "px"
                            });
                        }
                        if (String(h.prop("tagName")) === "DIV") {
                            h.hide().fadeIn(200);
                        }
                    } else {
                        h.hide().fadeIn(200);
                    }
                    h.attr("isDropped", "true");
                }
            } else {
                if (String(this.model.contents.state.value) === "table") {
                    h.find("td").removeAttr("style");
                    if (k.reduce_font_size) {
                        k.contentContainerDiv.find("table td,table th").css({
                            "font-size": k.font_size + "px"
                        });
                    }
                    if (String(h.prop("tagName")) === "DIV") {
                        h.show();
                    }
                } else {
                    h.show();
                }
                h.attr("isDropped", "true");
            }
            if (Number(c) === Number(this.dropObjId)) {
                this.moveScollToEnd(this.contentContainerDiv);
            }
        }
        e = this.itemCount - (Number(this.dropObjId) + 1);
        for (c = 1; c <= e; c++) {
            h = this.parentContainer.find("#" + this.contentPrefix + (Number(this.dropObjId) + c));
            h.attr("isDropped", "false");
        }
    } else {
        if (this.orderedItems === "single") {
            for (d = 0; d < this.itemCount; d++) {
                this.parentContainer.find("#" + this.contentPrefix + d).css("display", "none");
            }
            h = this.parentContainer.find("#" + this.contentPrefix + this.dropObjId);
            if (String(this.useFade) === "true") {
                if (Number(this.fadeTime) !== 0) {
                    h.hide().fadeIn(this.fadeTime);
                } else {
                    h.hide().fadeIn(200);
                }
            } else {
                h.show();
                if (h.height() > this.parentContainer.outerHeight()) {
                    this.moveScollToEnd(this.imageContainerDiv);
                } else {
                    if (navigator.userAgent.match(/iPhone/i) === null && navigator.userAgent.match(/Android/i) === null && navigator.userAgent.match(/mobile/i) === null) {
                        this.imageContainerDiv.mCustomScrollbar("scrollTo", "top");
                    }
                }
            }
        }
    }
    this.reAlignTheContent();
    if (k.deviceType === "mobile") {
        if (k.parentContainer.find(".text-area").is(":visible")) {
            g = k.parentContainer.find(".text-area");
        } else {
            g = k.imageContainerDiv;
        }
        g.mCustomScrollbar("destroy");
        g.mCustomScrollbar({
            scrollInertia: 100
        });
        if (k.model.contents.state.value !== "table") {
            for (d = 0; d <= 100; d++) {
                g.mCustomScrollbar("scrollTo", "bottom");
            }
        }
        this.addTableScrollbar(this.dropObjId);
    }
};
Athena.SliderRevealItems.prototype.addTableScrollbar = function(f) {
    var e, d, c, b, a;
    c = this;
    a = [];
    if (this.parentContainer.find(".text-area").is(":visible")) {
        d = this.parentContainer.find(".text-area");
    } else {
        d = c.imageContainerDiv;
    }
    a = d.find("#content-" + f).find("table");
    if (a.length !== 0) {
        if (d.find("#content-" + f).find("table").attr("customScrollbar") !== "added") {
            e = document.createElement("div");
            e.className = "table-scroll-" + f;
            d.find("#content-" + f).append(e);
            b = d.find("#content-" + f).find("table").height() + Number(10);
            d.find("#content-" + f).find(".table-scroll-" + f).css({
                width: "98%",
                overflow: "hidden",
                height: "auto"
            });
            d.find("#content-" + f).find("table").detach().appendTo(d.find("#content-" + f).find(".table-scroll-" + f));
            d.find("#content-" + f).find("table").attr("customScrollbar", "added");
            d.find("#content-" + f).find(".table-scroll-" + f).mCustomScrollbar({
                scrollInertia: 100,
                horizontalScroll: true,
                autoHideScrollbar: true
            });
            this.sliderPanelDiv.addClass("hasCustomScrollbar");
        }
    }
};
Athena.SliderRevealItems.prototype.dropCallBack = function(a, d, b, e) {
    var c;
    
    Collab.sliderRevealDropped(a.draggablePointer.selector, a.sliderContainerDiv.selector + ' #' +b.attr("id"));
    
    a.dropObjId = b.attr("id").split("-")[1];
    if (a.tableMode === "false") {
        a.dropObjId -= 1;
    }
    d.css("top", b.attr("dropTop") + "px");
    if ((a.pluginEvent !== undefined) && a.pluginEvent !== null) {
        a.pluginEvent(null, null, null, null, {
            id: this.model.objectId,
            value: {
                dropObj: this.dropObjId,
                top: b.attr("dropTop")
            }
        });
    }
    a.selectSliderType();
    if ((a.pluginEvent !== undefined) && a.pluginEvent !== null) {
        a.pluginEvent(null, null, "started", "performance", this.model.objectId);
    }
};
Athena.SliderRevealItems.prototype.moveScollToEnd = function(a) {
    if (navigator.userAgent.match(/iPhone/i) === null && navigator.userAgent.match(/Android/i) === null && navigator.userAgent.match(/mobile/i) === null) {
        setTimeout(function() {
            a.mCustomScrollbar("scrollTo", "bottom");
        }, 500);
    }
};
Athena.SliderRevealItems.prototype.reset = function() {
    var b, c, a;
    c = 1;
    b = this.content_array.length;
    this.draggablePointer.animate({
        top: "0px"
    }, 1000);
    if (this.tableMode === "false") {
        c = 0;
    }
    for (a = c; a < b; a++) {
        this.parentContainer.find("#" + this.contentPrefix + a).removeAttr("isDropped");
        if (String(this.model.contents.state.value) === "table") {
            this.parentContainer.find("#" + this.contentPrefix + a).find("td").css("color", "transparent");
            if (this.parentContainer.find(".contentText")) {
                this.parentContainer.find(".contentText").hide();
            }
        } else {
            this.parentContainer.find("#" + this.contentPrefix + a).fadeOut(500);
        }
    }
    this.parentContainer.find("#" + this.contentPrefix + "0").attr("isDropped", "true");
    this.parentContainer.find("#" + this.contentPrefix + "0").fadeIn(500);
    if (this.deviceType === "mobile") {
        this.contentContainerDiv.mCustomScrollbar("destroy");
        this.imageContainerDiv.mCustomScrollbar("destroy");
        this.contentContainerDiv.mCustomScrollbar({
            advanced: {
                updateOnContentResize: true
            }
        });
        this.imageContainerDiv.mCustomScrollbar({
            advanced: {
                updateOnContentResize: true
            }
        });
    }
};
Athena.SliderRevealItems.prototype.preset = function(a) {
    if (a !== undefined) {
        this.dropObjId = a.value.dropObj;
        this.parentContainer.find(".drag-pointer").hide();
        this.parentContainer.find(".drag-pointer").css("top", a.value.top + "px");
        this.parentContainer.find(".drag-pointer").show();
        this.selectSliderType();
    } else {
        this.parentContainer.find(".drag-pointer").show();
    }
};
Athena.MathPalette = function() {
    this.name = "MathPalette";
    this.palette = null;
    this.output = null;
    this.display = null;
    this.hidden = null;
    this.operators = null;
    this.isDraggable = false;
    this.isScrollable = false;
    this.undos = [];
    this.lastRestores = [];
};
Athena.MathPalette.prototype.setData = function() {
    this.operators = [{
        id: "Exp",
        value: "Exponent",
        className: "b-exp",
        markup: '<div class="equa-item equa-exp std"><span class="selectable active"></span><sup class="selectable"></sup></div>'
    }, {
        id: "Sub",
        value: "Subscript",
        className: "b-subscript",
        markup: '<div class="equa-item equa-sub std"><span class="selectable active"></span><sub class="selectable"></sub></div>'
    }, {
        id: "ExpSub",
        value: "ExponentSubscript",
        className: "b-expsub",
        markup: '<div class="equa-item equa-subexp std"><span class="selectable"></span><div class="right"><sup class="selectable"></sup><sub class="selectable"></sub></div></div>'
    }, {
        id: "Frac",
        value: "Fraction",
        className: "b-fraction",
        markup: '<div class="equa-item equa-frac dbl"><span class="numer selectable active"></span><span class="denom selectable"></span></div>'
    }, {
        id: "OBar",
        value: "OverBar",
        className: "b-overbar",
        markup: '<div class="equa-item equa-overbar std"><span class="selectable active"></span></div>'
    }, {
        id: "SqrR",
        value: "SquareRoot",
        className: "b-sqroot",
        markup: '<div class="equa-item equa-sqroot"><div class="right"><div class="content selectable active"></div></div></div>'
    }, {
        id: "NthR",
        value: "NthRoot",
        className: "b-nthroot",
        markup: '<div class="equa-item equa-nthroot"><sup class="nth">2</sup><div class="right"><div class="content selectable active"></div></div></div>'
    }, {
        id: "Abs",
        value: "AbsoluteValue",
        className: "b-absolute",
        markup: '<div class="equa-item equa-absol std"><span class="selectable active"></span></div>'
    }, {
        id: "Pi",
        value: "Pi",
        className: "b-pi",
        markup: '<div class="equa-symbol sym-pi"></div>'
    }, {
        id: "Deg",
        value: "Degree",
        className: "b-degree",
        markup: '<div class="equa-symbol sym-degree"></div>'
    }, {
        id: "Ang",
        value: "Angle",
        className: "b-angle",
        markup: '<div class="equa-symbol sym-angle"></div>'
    }, {
        id: "Tri",
        value: "Triangle",
        className: "b-triangle",
        markup: '<div class="equa-symbol sym-triangle"></div>'
    }, {
        id: "RAr",
        value: "RightArrow",
        className: "b-rightarrow",
        markup: '<div class="equa-symbol sym-rightarrow"></div>'
    }, {
        id: "Par",
        value: "Parallel",
        className: "b-parallel",
        markup: '<div class="equa-symbol sym-parallel"></div>'
    }, {
        id: "Perp",
        value: "Perpendicular",
        className: "b-perpendicular",
        markup: '<div class="equa-symbol sym-perpendicular"></div>'
    }, {
        id: "Inf",
        value: "Infinity",
        className: "b-infinity",
        markup: '<div class="equa-symbol sym-infinity"></div>'
    }, {
        id: "PlMi",
        value: "PlusMinus",
        className: "b-plusminus",
        markup: '<div class="equa-symbol sym-plusminus"></div>'
    }, {
        id: "Sim",
        value: "Sim",
        className: "b-sim",
        markup: '<div class="equa-symbol sym-sim"></div>'
    }, {
        id: "AproxEq",
        value: "ApproximatelyEqual",
        className: "b-approxequal",
        markup: '<div class="equa-symbol sym-approxeq"></div>'
    }, {
        id: "Cong",
        value: "Congruent",
        className: "b-congruent",
        markup: '<div class="equa-symbol sym-congruent"></div>'
    }, {
        id: "Eq",
        value: "Equals",
        className: "b-equals",
        markup: '<div class="equa-symbol sym-equals"></div>'
    }, {
        id: "Gteq",
        value: "GreaterThanEqualTo",
        className: "b-gthaneq",
        markup: '<div class="equa-symbol sym-gthaneq"></div>'
    }, {
        id: "Lteq",
        value: "LessThanEqualTo",
        className: "b-lthaneq",
        markup: '<div class="equa-symbol sym-lthaneq"></div>'
    }, {
        id: "Neq",
        value: "NoEqual",
        className: "b-neq",
        markup: '<div class="equa-symbol sym-notequal"></div>'
    }, {
        id: "Div",
        value: "Divide",
        className: "b-divide",
        markup: '<div class="equa-symbol sym-divide"></div>'
    }, {
        id: "Tim",
        value: "Times",
        className: "b-times",
        markup: '<div class="equa-symbol sym-times"></div>'
    }, {
        id: "Min",
        value: "Minus",
        className: "b-minus",
        markup: '<div class="equa-symbol sym-minus"></div>'
    }, {
        id: "Plu",
        value: "Plus",
        className: "b-plus",
        markup: '<div class="equa-symbol sym-plus"></div>'
    }];
};
Athena.MathPalette.prototype.init = function(c, b, e, k) {
    this.palette = c;
    this.output = b;
    if (this.output) {
        this.display = this.output.find(".text-container");
    }
    this.hidden = e;
    var d, a = this.operators.length,
        g = 0,
        f, h = c.find("#default-palette-button"),
        j = c.find("section div.containment");
    for (d = 0; d < a; d++) {
        f = h.clone();
        f.attr("id", "palette-button-" + g);
        f.addClass(this.operators[d].className);
        j.append(f);
        g++;
    }
    h.remove();
    this.setEvents(k);
    this.addDraggable();
    this.addScrollable();
    if (this.output) {
        this.reset();
        this.output.removeAttr("contenteditable");
    }
};
Athena.MathPalette.prototype.setSelectableEvents = function(b) {
    var a = this;
    this.output.find(".equa-item .selectable").unbind("mousedown");
    this.display.unbind("dblclick");
    this.output.find(".equa-item .selectable").mousedown(function(c) {
        c.stopPropagation();
        a.output.find(".equa-item .selectable").removeClass("active");
        $(this).addClass("active");
    });
    this.display.dblclick(function(c) {
        c.stopPropagation();
        a.output.find(".equa-item .selectable").removeClass("active");
    });
};
Athena.MathPalette.prototype.setDisplay = function() {
    var a = this.output.find(".text-container");
    if (a !== this.display) {
        this.display = a;
        this.setEvents();
        if (this.display !== undefined && this.display) {
            this.lastRestores = [];
            this.lastRestores.push(this.display.html());
        }
    }
};
Athena.MathPalette.prototype.setEvents = function(b) {
    var a = this;
    this.palette.find("button.small").unbind("click");
    this.palette.find(".button-close").unbind("click");
    this.palette.find("button.delete").unbind("click");
    this.palette.find("button.undo").unbind("click");
    this.palette.find("button.small").click(function() {
        a.lastRestores.push(a.display.html());
        var c = Number($(this).attr("id").split("-")[2]);
        a.updateOutput(c);
    });
    this.palette.find(".button-close").click(function() {
        a.palette.hide();
        a.clear();
    });
    this.palette.find("button.delete").click(function() {
        a.reset();
    });
    this.palette.find("button.undo").click(function() {
        if (a.lastRestores.length > 0) {
            a.display.html(a.lastRestores.pop());
        }
    });
};
Athena.MathPalette.prototype.updateOutput = function(f) {
    var d = this.output.find(".active"),
        c = this.output.find("span.mp-undo-level-" + (this.undos.length - 1)),
        a = $(this.getMarkup(f)),
        e, b;
    if (d.length === 0) {
        b = this.display;
        a.addClass("mp-undo-level-" + (this.undos.length));
        this.undos.push(a);
        this.insert(a, b);
        a.find(".active").focusin(function() {
            var g = 0;
        });
        a.find(".active").focus();
    } else {
        b = d;
        a.addClass("mp-undo-level-" + (this.undos.length));
        this.undos.push(a);
        this.insert(a, b);
        b.removeClass("active");
        a.find(".active").focus();
    }
    
    Collab.freeTextInputChanged(b.parents('.interactivity-container').attr('data-objectid'), b.html());
    
    this.setSelectableEvents();
};
Athena.MathPalette.prototype.insert = function(c, d) {
    var b, g, f, e, a;
    if (document.getSelection) {
        g = document.createElement("div");
        g.setAttribute("id", "replace-div");
        a = d;
        if (document.getSelection().type !== "None") {
            b = document.getSelection().getRangeAt(0);
            if (!$(a).hasClass("text-container")) {
                a = $(a).closest(".text-container");
            }
            if ($(b.startContainer).closest(".text-container").length > 0) {
                b.surroundContents(g);
                a.find("#replace-div").replaceWith(c.prop("outerHTML") + "&nbsp;");
            }
        } else {
            if (document.createRange) {
                b = document.createRange();
                e = d.toArray()[0];
                if (!(/text\-container/.test(e.className))) {
                    a = d.closest(".text-container");
                    e = a.toArray()[0];
                }
                b.selectNode(e);
                f = e.childNodes;
                b.setStartAfter(f[f.length - 1]);
                b.insertNode(g);
                a.find("#replace-div").replaceWith(c.prop("outerHTML") + "&nbsp;");
            }
        }
    }
};
Athena.MathPalette.prototype.getMarkup = function(e) {
    var a, d = this.operators,
        c = d.length,
        b;
    for (a = 0; a < c; a++) {
        if (a === e) {
            b = d[a].markup;
        }
    }
    return b;
};
Athena.MathPalette.prototype.addDraggable = function() {
    if (this.isDraggable) {
        this.palette.draggable({
            handle: this.palette.find("header")
        });
    }
};
Athena.MathPalette.prototype.addScrollable = function() {
    if (this.isScrollable) {
        this.palette.addClass("mobile");
        var b, a;
        b = -(this.palette.find("section div.containment").width() - this.palette.find("section").width());
        a = 0;
        this.palette.find("section div.containment").draggable({
            axis: "x",
            containment: [b, 0, a, 0],
            cancel: false
        });
    }
};
Athena.MathPalette.prototype.resetOutput = function() {
    if (this.display.text() === "") {
        this.reset();
    }
};
Athena.MathPalette.prototype.undoItem = function() {
    if (this.undos[this.undos.length - 1] !== undefined) {
        this.undos[this.undos.length - 1].remove();
        this.undos.pop();
    }
};
Athena.MathPalette.prototype.clear = function() {
    this.undos = [];
};
Athena.MathPalette.prototype.reset = function() {
    this.undos = [];
    this.display.empty().append("&nbsp;").focus();
    Collab.freeTextInputChanged(this.display.parents('.interactivity-container').attr('data-objectid'), this.display.html());
};