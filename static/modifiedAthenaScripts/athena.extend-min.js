Athena.HsmShellContainerView = Athena.ShellContainerView.extend({
	activateShell : function() {
		Athena.HsmShellContainerView.__super__.activateShell.call(this);
	},
	initSplashView : function() {
		this.splashView = new Athena.HsmSplashView();
		this.splashView.updateHeader = true;
		this.splashView.render();
	},
	initShell : function() {
		Athena.HsmShellContainerView.__super__.initShell.call(this);
		this.initTeacherPanel();
	},
	initTeacherPanel : function() {
		if (Athena.settingsModel.edition === "TE") {
			this.teacherView = new Athena.HsmTeacherPanelView();
		}
	},
	initGlossaryView : function() {
		if (Athena.dlolocaleModel.options.hasGlossary !== undefined) {
			if (Athena.dlolocaleModel.options.hasGlossary) {
				this.glossaryView = new Athena.HsmGlossaryView();
			}
		}
	},
	initSettingsView : function() {
		if (Athena.dlolocaleModel.options.hasMenuSettings !== undefined) {
			if (Athena.dlolocaleModel.options.hasMenuSettings) {
				this.settingsView = new Athena.HsmSettingsView();
				var a = navigator.userAgent, b = new RegExp("msie", "ig").test(a) || (a.indexOf("compatible") < 0 && !!navigator.userAgent.match(/Trident.*rv[ :]*11\./));
				if (Athena.browserDetails[0].Browser === "Safari" || Athena.browserDetails[0].Browser === "Chrome" || Athena.browserDetails[0].isMobile === 1 || b) {
					this.settingsView.buttonFullscreen.hide();
				}
			}
		}
	},
	initMainMenu : function() {
		if (Athena.dlolocaleModel.options.hasMenuActivity !== undefined) {
			if (Athena.dlolocaleModel.options.hasMenuActivity) {
				this.navView = new Athena.HsmNavigationView();
				this.menuView = new Athena.HsmMenuView();
				this.footerMenuView = new Athena.HsmFooterMenuView();
			}
		}
	},
	initArrowMenu : function() {
		if (Athena.dlolocaleModel.options.hasMenuArrows !== undefined) {
			if (Athena.dlolocaleModel.options.hasMenuArrows) {
				this.arrowMenuView = new Athena.HsmMenuArrowView();
			}
		}
	},
	initToolsView : function() {
		if (Athena.dlolocaleModel.options.hasTools !== undefined) {
			if (Athena.dlolocaleModel.options.hasTools) {
				this.toolsView = new Athena.HsmToolsView();
			}
		}
	},
	initInfoView : function() {
		if (Athena.dlolocaleModel.options.hasInfo !== undefined) {
			if (Athena.dlolocaleModel.options.hasInfo) {
				this.infoView = new Athena.HsmInfoView();
			}
		}
	},
	initAudioView : function() {
		this.dispatch(Athena.AUDIO_INIT);
	},
	initPalette : function() {
		Athena.mathPalette = new Athena.HsmMathPaletteView();
	},
	updateText : function() {
		if (Athena.dlolocaleModel.options.updateHeaders !== undefined && Athena.dlolocaleModel.options.updateHeaders) {
			var c, e, d, a, b = Athena.dlolocaleModel.getByCountId(Athena.dlolocaleModel.selectedActivity);
			e = b.attributes.contents.title;
			d = b.attributes.contents.subtitle;
			a = b.attributes.contents.intro;
			if (Athena.dlolocaleModel.shell.title !== undefined && Athena.dlolocaleModel.shell.title !== "") {
				if (this.shellTitle) {
					this.shellTitle.html(Athena.dlolocaleModel.shell.title);
				}
			} else {
				c = Athena.dlolocaleModel.data.customValues[Athena.settingsModel.region + "-" + Athena.settingsModel.param.category + "-title"];
				if (c !== undefined) {
					if (this.shellTitle) {
						this.shellTitle.html(c);
					}
				} else {
					if (e !== undefined && e !== "") {
						if (this.shellTitle) {
							this.shellTitle.html(e);
						}
					}
				}
			}
			if (Athena.dlolocaleModel.shell.subtitle !== undefined && Athena.dlolocaleModel.shell.subtitle !== "") {
				if (this.shellSubTitle) {
					this.shellSubTitle.html(Athena.dlolocaleModel.shell.subtitle);
				}
			} else {
				if (d !== undefined && d !== "") {
					if (this.shellSubTitle) {
						this.shellSubTitle.html(d);
					}
				}
			}
			if (Athena.dlolocaleModel.shell.intro !== undefined && Athena.dlolocaleModel.shell.intro !== "") {
				if (this.shellIntro) {
					this.shellIntro.html(Athena.dlolocaleModel.shell.intro);
				}
			} else {
				if (a !== undefined && a !== "") {
					if (this.shellIntro) {
						this.shellIntro.html(a);
					}
				}
			}
		}
	},
	wrapperEvent : function(b) {
		var a = this;
		if (b) {
			$("#wrapper").unbind("click");
			$("#wrapper").click(function(c) {
				debug("ClickEvent: Wrapper");
				c.stopPropagation();
				a.dispatch(Athena.MENUS_ALL_CLOSE);
				$("#wrapper").unbind("click");
			});
		} else {
			$("#wrapper").unbind("click");
		}
	},
	render : function() {
		Athena.ShellContainerView.__super__.render.call(this);
		this.headerContainer = $("header.main");
		this.footerContainer = this.$el.find("footer.main");
		this.shellTitle = $("#shell-title");
		this.shellSubTitle = $("#shell-subtitle");
		this.shellIntro = $("#shell-intro");
		Athena.pluginDataCollection.formatCustomData();
		this.initSplashView();
		this.initPalette();
		this.check();
		var a = this, b = Athena.settingsModel.get("preload-splash");
		if (b) {
			this.dispatch(Athena.PRELOADER_HIDE);
		}
		this.wrapperEvent(true);
	}
});
Athena.HsmFooterMenuView = Athena.CoreSettingsView.extend({
	render : function() {
		this.$el = $("#footer-menu");
		this.$el.addClass("visible");
		this.setVisibleButtons();
	}
});
Athena.HsmNavigationView = Athena.CoreView.extend({
	menuOpen : false,
	links : null,
	initialize : function() {
		this.register(Athena.MENUS_ALL_CLOSE, this.togglePanel, this);
		this.register(Athena.ACTIVITY_CHANGED, this.closeMenu, this);
		this.register(Athena.MENUS_NEED_SCROLL, this.setVerticalScroller, this);
		Athena.HsmNavigationView.__super__.initialize.call(this);
	},
	setTabs : function() {
		$("ul.tabs li:nth-of-type(1)").html(Athena.localeModel.get("nav-tab-1"));
		$("ul.tabs li:nth-of-type(2)").html(Athena.localeModel.get("nav-tab-2"));
		$("ul.tabs li:nth-of-type(3)").html(Athena.localeModel.get("nav-tab-3"));
	},
	setContent : function() {},
	closeMenu : function() {
		if (this.menuOpen) {
			this.buttonCollapse.addClass("open").removeClass("close");
			this.$el.slideUp();
		}
	},
	togglePanel : function(b) {
		var a = this;
		if (b) {
			if (this.menuOpen) {
				this.buttonCollapse.addClass("open").removeClass("close");
				this.$el.slideUp();
				Athena.shellView.wrapperEvent(false);
			} else {
				this.buttonCollapse.addClass("close").removeClass("open");
				this.$el.slideDown("medium", function() {
					a.setVerticalScroller();
				});
				Athena.shellView.wrapperEvent(true);
			}
			this.menuOpen = !this.menuOpen;
		} else {
			this.buttonCollapse.addClass("open").removeClass("close");
			this.$el.slideUp();
			this.menuOpen = false;
			Athena.shellView.wrapperEvent(false);
		}
	},
	setEvents : function() {
		var a = this;
		this.buttonCollapse.click(function(b) {
			b.stopPropagation();
			if (!a.menuOpen) {
				a.dispatch(Athena.MENUS_ALL_CLOSE);
			}
			a.togglePanel(true);
		});
		$(".dlo-menu ul.tabs li").click(function(b) {
			b.stopPropagation();
			var c = Number($(this).attr("id").split("-")[2]);
			$(this).data("tabNo", c);
			$("ul.tabs li").removeClass("active");
			$(".dlo-menu .tab-content").hide();
			$(".dlo-menu .content-" + c).fadeIn("medium", function() {
				a.setVerticalScroller();
			});
			$(this).addClass("active");
		});
		$(".dlo-menu ul.resources li").click(function() {
			var c = $(this).attr("id").split("-")[3], b = a.getLink(c);
			window.open(b, "_blank");
		});
		$(".dlo-menu .standards").find("li").click(function() {
			if ($(this).find(".desc").css("display") === "block" || $(this).find(".desc").css("display") === "") {
				$(this).find(".desc").slideUp("medium", function() {
					a.dispatch(Athena.MENUS_NEED_SCROLL);
				});
				$(this).removeClass("open");
			} else {
				$(this).find(".desc").slideDown("medium", function() {
					a.dispatch(Athena.MENUS_NEED_SCROLL);
				});
				$(this).addClass("open");
			}
			return false;
		});
		$(".dlo-menu .standards").find("li").hover(function() {
			$(this).addClass("hover");
		}, function() {
			$(this).removeClass("hover");
		});
		$(window).bind("resize orientationchange", function() {
			a.setVerticalScroller();
		});
	},
	setStandards : function() {
		var c, a = Athena.dlolocaleModel.options.standards, e = Athena.settingsModel.region.toLowerCase(), b, d, f;
		debug(b);
		a = a.replace(/\s/g, "");
		b = a.split(",");
		d = b.length;
		for (c = 0; c < d; c++) {
			f = b[c];
			if (e === "tx") {
				e = "texas";
			}
			$(".dlo-menu .standards").append('<li><div class="title">' + f + '</div><div class="desc">' + Athena.localeModel.get("standard-" + e + "-" + f) + "</div></li>");
		}
		if (!new RegExp(/iphone|android|ipad/gi).test(navigator.userAgent) && $(window).height() > 600) {
			$(".dlo-menu .standards li:nth-child(1)").addClass("open").find(".desc").slideDown();
		}
	},
	setVerticalScroller : function() {
		var e, b, a, c, d;
		c = Number($("ul.tabs li.active").data("tabNo"));
		c = isNaN(c) ? 1 : c;
		a = $(".dlo-menu .content-" + c);
		d = $(".splash").length > 0 ? 0 : $("footer").outerHeight();
		b = $(window).height() - $(a).offset().top - d;
		e = $(a).find("ul").outerHeight();
		if (new RegExp(/iphone|ipad|android/gi).test(navigator.userAgent)) {
			if (b < e) {
				$(a).css({
					height : b + "px",
					"overflow-y" : "scroll"
				});
			} else {
				$(a).css({
					height : "auto",
					"overflow-y" : "auto"
				});
			}
		}
	},
	setResources : function() {
		var d, b, e = Athena.dlolocaleModel.options.resource_labels, a = Athena.dlolocaleModel.options.resource_links, c;
		if (e === "" && a === "") {
			if (Athena.settingsModel.param.category === "alg1" || Athena.settingsModel.param.category === "alg2" || Athena.settingsModel.param.category === "geo") {
				d = Athena.settingsModel.param.category;
			} else {
				d = Athena.settingsModel.param.category;
			}
			if (Athena.dlolocaleModel.data.customValues.resources !== undefined) {
				if (Athena.dlolocaleModel.data.customValues.resources[Athena.settingsModel.region + "-" + d] !== undefined) {
					e = Athena.dlolocaleModel.data.customValues.resources[Athena.settingsModel.region + "-" + d].titles;
					a = Athena.dlolocaleModel.data.customValues.resources[Athena.settingsModel.region + "-" + d].links;
				}
			}
		}
		a = a.replace(/\s/g, "");
		e = e.split(",");
		a = a.split(",");
		c = e.length;
		for (b = 0; b < c; b++) {
			$(".dlo-menu .resources").append('<li id="resource-link-id-' + b + '">' + e[b] + "</li>");
		}
		this.links = a;
	},
	getLink : function(e) {
		var b, d = this.links.length, c, a = Athena.settingsModel.hostName;
		if (!String(a).match(/http:/g)) {
			a = "http://" + a;
		}
		for (b = 0; b < d; b++) {
			if (b === Number(e)) {
				c = a + this.links[b];
			}
		}
		return c;
	},
	render : function() {
		this.$el = $(".dlo-menu");
		this.buttonCollapse = $(".button-menu-collapse");
		this.setTabs();
		this.setStandards();
		this.setResources();
		this.setContent();
		this.setEvents();
	}
});
Athena.HsmToolsView = Athena.ToolsView.extend({
	menuList : null,
	initialize : function() {
		this.register(Athena.MENUS_ALL_CLOSE, this.menuClose, this);
		Athena.HsmToolsView.__super__.initialize.call(this);
	},
	setEvents : function() {
		var a = this;
		this.menuList.find("li").click(function() {
			var c, d = $(this).attr("id").split("-")[2], b = Athena.settingsModel.hostName;
			if (!String(b).match(/http:/g)) {
				b = "http://" + b;
			}
			c = $(this).find(":nth-child(2)").text();
			a.dispatch(Athena.EVENT_TRACK, {
				verb : "started",
				result : c,
				intType : "tool",
				objectId : "t" + d
			});
			a.dispatch(Athena.TOOLS_CLOSED, true);
			a.fadeOut();
			window.open(b + Athena.localeModel.get("tool-link-" + d), "_blank");
		});
	},
	setContent : function() {
		var c, d = 7, a = this.menuList.find("#default-tool-item"), b;
		for (c = 0; c < d; c++) {
			b = a.clone();
			b.attr("id", "menu-tool-" + (c + 1));
			b.find("span.label").html(Athena.localeModel.get("tool-" + (c + 1)));
			this.menuList.append(b);
		}
		a.remove();
	},
	menuClose : function() {
		if (Athena.shellView.settingsView.isToolsOpen) {
			this.fadeOut();
			this.dispatch(Athena.TOOLS_CLOSED, true);
		}
	},
	fadeIn : function() {
		this.$el.slideDown();
	},
	fadeOut : function() {
		this.$el.slideUp();
	},
	render : function() {
		Athena.HsmToolsView.__super__.render.call(this);
		this.menuList = this.$el.find("ul");
		this.setContent();
		this.setEvents();
	}
});
Athena.HsmInfoView = Athena.InfoView.extend({
	fadeIn : function() {
		this.$el.slideDown();
	},
	fadeOut : function() {
		this.$el.slideUp();
	}
});
Athena.HsmMenuView = Athena.MenuView.extend({
	initialize : function() {
		Athena.HsmMenuView.__super__.initialize.call(this);
		this.register(Athena.MENU_CHANGE, this.updateMenu, this);
	},
	generate : function() {
		var b, i, e, c, f, g, h, j, a, d, k = this;
		h = this.$el.find("#default-activity");
		j = this.$el.find("#default-activity-small");
		a = this.$el.find(".start");
		_.each(Athena.dlolocaleModel.models, function(l) {
			if (l.attributes.parentId === -1) {
				if (l.attributes.option.navType !== undefined) {
					b = j.clone().insertBefore(a);
				} else {
					b = h.clone().insertBefore(a);
				}
				b.removeAttr("id").removeClass("start");
				i = b.find("button").first();
				c = i.find("span.label");
				g = i.find("span.icon");
				if (l.attributes.option.navType !== undefined) {
					e = b.find("button:nth-child(2)");
					e.attr("title", l.attributes.option.navSmallLabel.value);
					f = e.find("span.label");
					f.append(l.attributes.option.navSmallLabel.value);
					e.attr("id", "menu-" + l.attributes.option.navSmallId.value);
				}
				i.attr("id", "menu-" + l.attributes.id);
				if (Athena.dlolocaleModel.options.useMainMenuText) {
					if (l.attributes.contents.title.value !== undefined) {
						c.append(l.attributes.contents.title.value);
						c.attr("data-objectid", l.attributes.contents.title.objectId);
					} else {
						c.append(l.attributes.contents.title);
					}
				} else {
					c.append(l.attributes.id + 1);
				}
				c.attr("class", "").addClass("label").addClass("text-" + l.attributes.option.colour.value);
				g.attr("class", "").addClass("icon").addClass(l.attributes.option.icon.value);
				if (l.attributes.id === 0) {
					if ($("section.splash").length === 0) {
						i.addClass("visited").addClass("active");
					}
				}
				d = k.setChildren(l.attributes.id);
				b.append(d);
				if (d !== "") {
					b.find(".collapse-menu").addClass("visible");
				} else {
					b.find(".collapse-menu").remove();
				}
			}
		});
		h.remove();
		j.remove();
		this.setEvents();
		this.$el.addClass("visible");
		if (Athena.browserDetails[0].isMobile === 1) {
			this.$el.find(".activity-sub-items").hide();
		} else {
			this.$el.find("span.collapse-menu").addClass("close").removeClass("open");
		}
	},
	setChildren : function(a) {
		var g = 0, f, j = "", n, o = this, k, l, m, d, c, e, b, h, i = Athena.browserDetails[0].isMobile;
		if (i === 0) {
			i = "";
		} else {
			i = "<br>";
		}
		_.each(Athena.dlolocaleModel.models, function(q) {
			var r = q.attributes.contents, p;
			if (r.title.value === undefined && r.subtitle.value === undefined && r.intro.value === undefined) {
				k = r.title;
				l = r.subtitle;
				m = r.intro;
			} else {
				k = r.title.value;
				l = r.subtitle.value;
				m = r.intro.value;
			}
			b = new RegExp("<math[a-z0-9_ '\"-=]*>", "ig").test(m);
			h = new RegExp("<[a-z0-9_ '\"-=]*>", "ig").test(m);
			if (q.attributes.id === a && q.attributes.option.showInSubMenu !== undefined) {
				n = o.checkMenuMerge(q.attributes.id);
				if (n !== undefined) {
					p = n.attributes.contents;
					if (p.title.value === undefined && p.subtitle.value === undefined && p.intro.value === undefined) {
						d = p.title;
						c = p.subtitle;
						e = p.intro;
					} else {
						d = p.title.value;
						c = p.subtitle.value;
						e = p.intro.value;
					}
					j += '<li data-name="1"><button id="sub-menu-' + q.attributes.id + '" type="button" class="sub-menu-item-small" title="' + (b || h ? "" : l + " " + m)
							+ '"><span class="label"><strong>' + l + "</strong> " + i + m + '</span></button><button id="sub-menu-' + n.attributes.id
							+ '" type="button" class="sub-menu-link" title="' + d + '"><span class="label">' + d + "</span></button></li>";
				} else {
					j += '<li data-name="2"><button id="sub-menu-' + q.attributes.id + '" type="button" class="sub-menu-item" title="' + (b || h ? "" : l + "  " + m)
							+ '"><span class="label"><strong>' + l + "</strong> " + i + m + "</span></button></li>";
				}
			}
			if (q.attributes.parentId === a && q.attributes.option.menuMergeWithParent === undefined) {
				n = o.checkMenuMerge(q.attributes.id);
				if (n !== undefined) {
					p = n.attributes.contents;
					if (p.title.value === undefined && p.subtitle.value === undefined && p.intro.value === undefined) {
						d = p.title;
						c = p.subtitle;
						e = p.intro;
					} else {
						d = p.title.value;
						c = p.subtitle.value;
						e = p.intro.value;
					}
					j += '<li data-name="3"><button id="sub-menu-' + q.attributes.id + '" type="button" class="sub-menu-item-small" title="' + (b || h ? "" : l + " " + m)
							+ '"><span class="label"><strong>' + l + "</strong> " + i + m + '</span></button><button id="sub-menu-' + n.attributes.id + '" type="button" title="'
							+ d + '" class="sub-menu-link"><span class="label">' + d + "</span></button></li>";
				}
			}
			if (q.attributes.parentId === a && q.attributes.option.showInSubMenu !== undefined) {
				j += '<li data-name="4"><button id="sub-menu-' + q.attributes.id + '" type="button" class="sub-menu-item"  title="' + (b || h ? "" : l + " " + m)
						+ '"><span class="label"><strong>' + l + "</strong> " + i + m + "</span></button></li>";
			}
		});
		if (j === "") {
			f = "";
		} else {
			f = '<ul class="activity-sub-items">' + j + "</ul>";
		}
		return f;
	},
	checkMenuMerge : function(b) {
		var a;
		_.each(Athena.dlolocaleModel.models, function(c) {
			if (c.attributes.parentId === b && c.attributes.option.menuMergeWithParent !== undefined) {
				a = c;
			}
		});
		return a;
	},
	setEvents : function() {
		var a = this;
		this.$el.find(".menu-item").click(function(b) {
			b.stopPropagation();
			var c = $(this).find("span.collapse-menu");
			if (c.length > 0) {
				a.toggleSubMenu($(this), c);
			} else {
				a.checkSplash();
				if ($(this).is(":not(.active)")) {
					a.handleItemClick(b);
				}
			}
		});
		this.$el.find(".sub-menu-item").click(function(b) {
			b.stopPropagation();
			if ($(this).is(":not(.active)")) {
				a.checkSplash();
				var c = $(this).attr("id").split("-")[2];
				a.handleSubItemClick(b, c);
			}
		});
		this.$el.find(".sub-menu-item-small").click(function(b) {
			b.stopPropagation();
			if ($(this).is(":not(.active)")) {
				a.checkSplash();
				var c = $(this).attr("id").split("-")[2];
				a.handleSubItemClick(b, c);
			}
		});
		this.$el.find(".sub-menu-link").click(function(b) {
			b.stopPropagation();
			a.checkSplash();
			if ($(this).is(":not(.active)")) {
				var c = $(this).attr("id").split("-")[2];
				a.handleSubItemClick(b, c);
			}
		});
		this.$el.find(".menu-item-small").click(function(b) {
			b.stopPropagation();
			a.checkSplash();
			if ($(this).is(":not(.active)")) {
				a.handleItemClick(b);
			}
		});
		this.$el.find(".menu-item-link").click(function(b) {
			b.stopPropagation();
			a.checkSplash();
			if ($(this).is(":not(.active)")) {
				a.handleItemClick(b);
			}
		});
	},
	toggleSubMenu : function(b, c) {
		var a = this;
		if (c.hasClass("close")) {
			c.addClass("open").removeClass("close");
			b.parent().find(".activity-sub-items").slideUp("medium", function() {
				a.dispatch(Athena.MENUS_NEED_SCROLL);
			});
		} else {
			if (c.hasClass("open")) {
				c.addClass("close").removeClass("open");
				b.parent().find(".activity-sub-items").slideDown("medium", function() {
					a.dispatch(Athena.MENUS_NEED_SCROLL);
				});
			}
		}
	},
	checkSplash : function() {
		if ($("section.splash").length !== 0) {
			Athena.shellView.splashView.hide();
			$("header.main").width(Athena.WINDOW_WIDTH);
		}
	},
	handleSubItemClick : function(e, d) {
		this.dispatch(Athena.AUDIO_STOP);
		this.dispatch(Athena.ACTIVITY_DISABLE, Athena.dlolocaleModel.selectedActivity);
		this.dispatch(Athena.IMAGE_HIDE);
		this.dispatch(Athena.EXCONTENT_HIDE);
		var c, b, a;
		c = e.currentTarget;
		b = d;
		Athena.dlolocaleModel.setSelected(Number(b));
		this.dispatch(Athena.ACTIVITY_CHANGED, 1);
		this.dispatch(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
		this.updateMenu(true);
		this.checkStates();
		this.dispatch(Athena.MENUARROW_UPDATE);
	},
	updateMenu : function(a) {
		var c, b;
		c = Athena.dlolocaleModel.getByCountId(Athena.dlolocaleModel.selectedActivity);
		b = c.attributes.parentId;
		$(".menu-item").removeClass("active");
		$(".menu-item-small").removeClass("active");
		$(".menu-item-link").removeClass("active");
		$(".sub-menu-item").removeClass("active");
		$(".sub-menu-item-small").removeClass("active");
		$(".sub-menu-link").removeClass("active");
		if (a) {
			$("#sub-menu-" + Athena.dlolocaleModel.selectedActivity).addClass("active").addClass("visited");
		} else {
			$("#sub-menu-" + Athena.dlolocaleModel.selectedActivity).addClass("active").addClass("visited");
			if ($("#sub-menu-" + Athena.dlolocaleModel.selectedActivity).length === 0) {
				$("#menu-" + Athena.dlolocaleModel.selectedActivity).addClass("active").addClass("visited");
			}
		}
		this.checkStates();
	}
});
Athena.HsmMenuArrowView = Athena.MenuArrowView.extend({
	initialize : function() {
		Athena.HsmMenuArrowView.__super__.initialize.call(this);
	},
	setEvents : function() {
		var a = this;
		this.buttonLeft.click(function(b) {
			if ($(this).is(":not(.disabled)")) {
				this.blur();
				a.handleLeftArrowClick(b);
			}
		});
		this.buttonRight.click(function(b) {
			if ($(this).is(":not(.disabled)")) {
				this.blur();
				a.handleRightArrowClick(b);
			}
		});
	},
	handleLeftArrowClick : function(a) {
		if (typeof window.Bridge === "undefined") {
			Athena.HsmMenuArrowView.__super__.handleLeftArrowClick.call(this);
		} else {
			Athena.eventBus.trigger(Athena.MEDIA_STOPALL);
			Athena.eventBus.trigger(Athena.NAVIGATION_CLICKED, {
				verb : "launched",
				result : "prev",
				intType : "navigation"
			}, null);
		}
	},
	handleRightArrowClick : function(a) {
		if (typeof window.Bridge === "undefined") {
			Athena.HsmMenuArrowView.__super__.handleRightArrowClick.call(this);
		} else {
			Athena.eventBus.trigger(Athena.MEDIA_STOPALL);
			Athena.eventBus.trigger(Athena.NAVIGATION_CLICKED, {
				verb : "launched",
				result : "next",
				intType : "navigation"
			}, null);
		}
	},
	setStatus : function() {
		var c, d, a = this, e, b;
		this.stopAnimation();
		if (this.menuType === 1) {
			if (Athena.dlolocaleModel.selectedActivity > 0) {
				this.buttonLeft.removeClass("disabled");
				e = Athena.dlolocaleModel.getByCountId(Athena.dlolocaleModel.selectedActivity - 1);
				if (e.attributes.contents.title.value === undefined && e.attributes.contents.subtitle.value === undefined) {
					if (e.attributes.contents.subtitle !== "") {
						b = "<strong>" + e.attributes.contents.title + "</strong> | " + e.attributes.contents.subtitle;
					} else {
						b = "<strong>" + e.attributes.contents.title + "</strong>";
					}
				} else {
					if (e.attributes.contents.subtitle.value !== "") {
						b = "<strong>" + e.attributes.contents.title.value + "</strong> | " + e.attributes.contents.subtitle.value;
					} else {
						b = "<strong>" + e.attributes.contents.title.value + "</strong>";
					}
				}
				this.buttonLeft.find("span.icon").attr("class", "").addClass("icon").addClass(e.attributes.option.icon.value + "-small");
				this.buttonLeft.find("span.label").html(b).attr("class", "").addClass("label").addClass("text-" + e.attributes.option.colour.value);
				this.buttonLeft.find("span.arrow").attr("class", "").addClass("arrow").addClass(e.attributes.option.colour.value);
				if (Athena.browserDetails[0].Browser === "Chrome" || Athena.browserDetails[0].Browser === "Safari") {
					$("img.bg").css({
						position : "absolute",
						top : "-" + $("#dlo").find("header").height() + "px",
						left : "0px"
					});
				}
			} else {
				this.buttonLeft.addClass("disabled");
				if (Athena.browserDetails[0].Browser === "Chrome" || Athena.browserDetails[0].Browser === "Safari") {
					setTimeout(function() {
						$("img.bg").css({
							position : "fixed",
							top : "0px"
						});
					}, 1000);
				}
			}
			if (Athena.dlolocaleModel.selectedActivity < Athena.dlolocaleModel.totalActivities - 1) {
				this.buttonRight.removeClass("disabled");
				e = Athena.dlolocaleModel.getByCountId(Athena.dlolocaleModel.selectedActivity + 1);
				if (e.attributes.contents.title.value === undefined && e.attributes.contents.subtitle.value === undefined) {
					if (e.attributes.contents.subtitle !== "") {
						b = "<strong>" + e.attributes.contents.title + "</strong> | " + e.attributes.contents.subtitle;
					} else {
						b = "<strong>" + e.attributes.contents.title + "</strong>";
					}
				} else {
					if (e.attributes.contents.subtitle.value !== "") {
						b = "<strong>" + e.attributes.contents.title.value + "</strong> | " + e.attributes.contents.subtitle.value;
					} else {
						b = "<strong>" + e.attributes.contents.title.value + "</strong>";
					}
				}
				this.buttonRight.find("span.icon").attr("class", "").addClass("icon").addClass(e.attributes.option.icon.value + "-small");
				this.buttonRight.find("span.label").html(b).attr("class", "").addClass("label").addClass("text-" + e.attributes.option.colour.value);
				this.buttonRight.find("span.arrow").attr("class", "").addClass("arrow").addClass(e.attributes.option.colour.value);
			} else {
				this.buttonRight.addClass("disabled");
			}
		}
	},
	render : function() {
		Athena.HsmMenuArrowView.__super__.render.call(this);
		this.setStatus();
	}
});
Athena.HsmTeacherPanelView = Athena.CoreView.extend({
	buttonTeacher : null,
	comboItems : null,
	comboItemsOptions : null,
	optionContent : null,
	isOpen : false,
	isComboOpen : false,
	initialize : function() {
		this.register(Athena.MENUS_ALL_CLOSE, this.togglePanel, this);
		Athena.HsmTeacherPanelView.__super__.initialize.call(this);
	},
	getContent : function(e) {
		var b = this, d, c = Athena.dlolocaleModel.teacher, a;
		for (d in c) {
			if (c.hasOwnProperty(d)) {
				if (d === "text_" + e) {
					a = c[d];
				}
			}
		}
		return a;
	},
	setContent : function() {
		this.buttonTeacher.find("span.label").html(Athena.localeModel.get("teacher-support"));
		var d, c = Athena.dlolocaleModel.teacher, e, a = this.comboItems.find(".options");
		for (d in c) {
			if (c.hasOwnProperty(d)) {
				if (d.match(/header/)) {
					e = d.split("_")[1];
					a.append('<div class="item-' + e + " " + c[d].colour + ' option">' + c[d].value + "</div>");
				}
			}
		}
		try {
			MathJax.Hub.Queue([ "Typeset", MathJax.Hub, this.$el.toArray()[0].querySelector(".te-content"), function() {
				var f;
			} ]);
		} catch (b) {
			debug(b);
		}
	},
	togglePanel : function(a) {
		if (a) {
			if (this.isOpen) {
				this.isOpen = false;
				this.fadeOut();
				this.buttonTeacher.removeClass("close").addClass("open");
				Athena.shellView.wrapperEvent(false);
			} else {
				this.isOpen = true;
				this.fadeIn();
				this.buttonTeacher.removeClass("open").addClass("close");
				Athena.shellView.wrapperEvent(true);
			}
		} else {
			this.isOpen = false;
			this.fadeOut();
			this.buttonTeacher.removeClass("close").addClass("open");
		}
	},
	setEvents : function() {
		var a = this;
		this.$el.click(function(b) {
			b.stopPropagation();
		});
		this.buttonTeacher.click(function(b) {
			b.stopPropagation();
			if (!a.isOpen) {
				a.dispatch(Athena.MENUS_ALL_CLOSE);
			}
			a.togglePanel(true);
		});
		this.comboItems.find(".selected").click(function(b) {
			b.stopPropagation();
			a.toggleCombo();
		});
		this.comboItems.find(".arrow").click(function(b) {
			b.stopPropagation();
			a.toggleCombo();
		});
		this.comboItems.find("div.option").click(function(b) {
			b.stopPropagation();
			var c = $(this).attr("class").split(" ")[0].split("-")[1];
			a.optionContent.html(a.getContent(c));
			a.optionContent.mCustomScrollbar();
			a.comboItems.find(".selected").html($(this).html());
			if (a.optionContent.find(".mCSB_container").hasClass("mCS_no_scrollbar")) {
				a.optionContent.find(".mCSB_container").addClass("no-scroll-margin");
			} else {
				a.optionContent.find(".mCSB_container").removeClass("no-scroll-margin");
			}
			a.toggleCombo();
		});
	},
	toggleCombo : function() {
		var a = this;
		if (this.isComboOpen) {
			this.isComboOpen = false;
			this.comboItemsOptions.slideUp();
			this.comboItems.find(".arrow").removeClass("close").addClass("open");
		} else {
			this.isComboOpen = true;
			this.comboItemsOptions.slideDown();
			this.comboItems.find(".arrow").removeClass("open").addClass("close");
		}
		try {
			MathJax.Hub.Queue([ "Typeset", MathJax.Hub, this.$el.toArray()[0].querySelector(".te-content"), function() {
				a.optionContent.mCustomScrollbar("update");
			} ]);
		} catch (b) {
			debug(b);
		}
	},
	fadeIn : function() {
		this.$el.slideDown();
	},
	fadeOut : function() {
		this.$el.slideUp();
	},
	setDefault : function() {
		this.toggleCombo();
	},
	render : function() {
		this.$el = $(".teacher-edition");
		this.buttonTeacher = $(".button-teacher-menu");
		this.comboItems = this.$el.find(".te-options");
		this.comboItemsOptions = this.comboItems.find(".options");
		this.optionContent = this.$el.find(".option-content-text");
		this.buttonTeacher.addClass("visible");
		this.comboItems.find("span.selected").text(Athena.localeModel.get("text-select-item"));
		this.setContent();
		this.setEvents();
		this.setDefault();
	}
});
Athena.HsmMathPaletteView = Athena.CoreView.extend({
	palette : null,
	output : null,
	initialize : function() {
		Athena.HsmMathPaletteView.__super__.initialize.call(this);
		this.register(Athena.PALETTE_HIDE, this.fadeOut, this);
		this.register(Athena.PALETTE_SHOW, this.fadeIn, this);
		this.register(Athena.PALETTE_UPDATE, this.update, this);
		this.render();
	},
	update : function(c) {
		var b = this, d, a;
		if (!c.is(this.output)) {
			this.output = c;
			this.palette.output = this.output;
			this.palette.setDisplay();
		}
	},
	fadeIn : function(a) {
		Athena.HsmMathPaletteView.__super__.fadeIn.call(this);
		if (a !== undefined) {
			this.update(a);
		}
	},
	fadeOut : function() {
		Athena.HsmMathPaletteView.__super__.fadeOut.call(this);
		this.palette.clear();
	},
	handlePaletteClick : function() {},
	render : function() {
		var a = this;
		this.$el = $(".math-palette");
		this.$el.find("#m-palette-undo span.label").text(Athena.localeModel.get("text-undo"));
		this.palette = new Athena.MathPalette();
		this.palette.isDraggable = (Athena.browserDetails[0].isMobile === 0) ? true : false;
		this.palette.isScrollable = (Athena.browserDetails[0].isMobile === 1) ? true : false;
		if (String(navigator.userAgent.match(/iPad/i)) === "iPad") {
			this.palette.isScrollable = false;
			this.palette.isDraggable = true;
		}
		this.palette.setData();
		this.palette.init(this.$el, this.output, undefined, function(b) {
			a.handlePaletteClick(b);
		});
		Athena.HsmMathPaletteView.__super__.render.call(this);
	}
});
Athena.HsmGlossaryView = Athena.GlossaryView.extend({
	iFrame : null,
	initialize : function() {
		Athena.HsmGlossaryView.__super__.initialize.call(this);
		this.render();
	},
	fadeIn : function(a) {
		this.$el.fadeIn();
		this.iFrame.attr("src", Athena.settingsModel.hostName + Athena.settingsModel.get("glossary-link") + a);
	},
	render : function() {
		this.iFrame = this.$el.find("iframe");
	}
});
Athena.HsmSettingsView = Athena.CoreSettingsView.extend({
	checkTools : function() {
		var a = this;
		if (Athena.dlolocaleModel.options.hasTools !== undefined) {
			if (Athena.dlolocaleModel.options.hasTools) {
				this.buttonTools = this.$el.find(".item-tools");
				this.buttonTools.addClass("visible");
				this.buttonTools.click(function(b) {
					b.stopPropagation();
					a.handleToolsClick();
				});
			}
		}
	},
	handleToolsClick : function(a) {
		if (a === undefined) {
			if (!this.isToolsOpen) {
				this.dispatch(Athena.MENUS_ALL_CLOSE);
				Athena.shellView.wrapperEvent(true);
			} else {
				Athena.shellView.wrapperEvent(false);
			}
		}
		Athena.HsmSettingsView.__super__.handleToolsClick.call(this, a);
	}
});
Athena.HsmSplashView = Athena.SplashView.extend({
	setEvents : function() {
		var a = this;
		this.buttonEnter.click(function(b) {
			a.dispatch(Athena.MENUS_ALL_CLOSE);
			$(this).unbind("click");
			a.handleEnterClicked();
			if (Athena.browserDetails[0].Browser === "Chrome" || Athena.browserDetails[0].Browser === "Safari") {
				$("img.bg").css({
					position : "absolute",
					top : "-" + $("#dlo").find("header").height() + "px",
					left : "0px"
				});
				setTimeout(function() {
					$("img.bg").css({
						position : "fixed",
						left : "0px",
						top : "0px"
					});
				}, 900);
			}
		});
	},
	hide : function() {
		Athena.HsmSplashView.__super__.hide.call(this);
		$("#settings-menu").find(".item-reset").show();
	},
	setContent : function() {
		$("#settings-menu").find(".item-reset").hide();
		if (this.title) {
			this.title.html(Athena.dlolocaleModel.splash.title);
		}
		if (this.subTitle) {
			this.subTitle.html(Athena.dlolocaleModel.splash.subTitle);
		}
		if (this.intro) {
			this.intro.html(Athena.dlolocaleModel.splash.intro);
		}
		if (this.buttonEnter) {
			if (Athena.dlolocaleModel.splash.enter !== undefined) {
				this.buttonEnter.find("span.label").html(Athena.dlolocaleModel.splash.enter);
			}
		}
		if (this.image) {
			if (Athena.dlolocaleModel.splash.background !== undefined) {
				if (Athena.dlolocaleModel.splash.background !== "") {
					var a = this.$el.find("div.splash-blur");
					this.image.css("background-image", "url(" + Athena.settingsModel.getImagePath() + Athena.dlolocaleModel.splash.background + ")");
				}
			}
		}
		this.animateElements();
	}
});
Athena.CustomPreloaderView = Athena.PreloaderView.extend({
	initialize : function() {
		Athena.CustomPreloaderView.__super__.initialize.call(this);
	},
	render : function() {
		Athena.CustomPreloaderView.__super__.render.call(this);
	},
	update : function(b, a) {
		Athena.CustomPreloaderView.__super__.update.call(this, b, a);
	}
});
Athena.CustomRouter = Athena.CoreRouter.extend({
	routes : {
		"" : "dlo_live_edition",
		"me/:edition" : "dlo_edition",
		"me/:edition/:region" : "dlo_edition",
		"me/:edition/:region/:id" : "dlo_edition",
		"me/:edition/:region/:id/:param" : "dlo_edition",
		"mx/:project/:discipline/:unit/:module/:dlo" : "dev_unit",
		"mx/:project/:discipline/:unit/:module/:dlo/:edition" : "dev_unit",
		"mx/:project/:discipline/:unit/:module/:dlo/:edition/:region" : "dev_unit",
		"mx/:project/:discipline/:unit/:module/:dlo/:edition/:region/:deep_link" : "dev_unit",
		"mx/:project/:discipline/:unit/:module/:dlo/:edition/:region/:deep_link/:param" : "dev_unit",
		"mcl/:params" : "dlo_live_edition",
		"mc/:project/:discipline/:unit/:module/:dlo" : "dev_editions",
		"mc/:project/:discipline/:unit/:module/:dlo/:params" : "dev_editions",
		"ds/:discipline/:grade/:lesson/:dlo" : "dev_grade",
		"ds/:discipline/:grade/:lesson/:dlo/:deep_link" : "dev_grade",
		"ds/:discipline/:grade/:lesson/:dlo/:deep_link/:param" : "dev_grade",
		"dm/:prefix/:discipline/:grade/:lesson/:dlo" : "dev_grade_p",
		"dm/:prefix/:discipline/:grade/:lesson/:dlo/:deep_link" : "dev_grade_p",
		"dm/:prefix/:discipline/:grade/:lesson/:dlo/:deep_link/:param" : "dev_grade_p",
		"dc/:project/:discipline/:category/:dlo" : "dev_project",
		"dc/:project/:discipline/:category/:dlo/:deep_link" : "dev_project",
		"dc/:project/:discipline/:category/:dlo/:deep_link/:param" : "dev_project",
		"da/:prefix/:project/:discipline/:category/:dlo" : "dev_project_p",
		"da/:prefix/:project/:discipline/:category/:dlo/:deep_link" : "dev_project_p",
		"da/:prefix/:project/:discipline/:category/:dlo/:deep_link/:param" : "dev_project_p"
	},
	formatQueryParams : function(d) {
		var b, a, c = {};
		if (d !== undefined) {
			for (b in d) {
				if (d.hasOwnProperty(b)) {
					this.parseParam(b, d[b], c);
				}
			}
		}
		return c;
	},
	parseParam : function(a, b, c) {
		if (a === "U") {
			if (b === "1efbbef0f49ec5327ffd389831cbe95e") {
				c.edition = "TE";
			} else {
				if (b === "3d3f0775297c02641fe5c0a653948c23") {
					c.edition = "SE";
				} else {
					c.edition = "SE";
				}
			}
		} else {
			if (a === "S") {
				c.state = b;
			} else {
				if (a === "L") {
					c.deeplink = Number(b);
				} else {
					if (a === "C") {
						c.category = b;
					} else {
						if (a === "H") {
							c.hideui = (Number(b) === 1) ? true : false;
							Athena.HIDE_UI = c.hideui;
						} else {
							if (a === "HS") {
								c.hidesplash = (Number(b) === 1) ? true : false;
								Athena.HIDE_SPLASH = c.hidesplash;
							} else {
								if (a === "HH") {
									c.hideheader = (Number(b) === 1) ? true : false;
									Athena.HIDE_HEADER = c.hideheader;
								} else {
									if (a === "HF") {
										c.hidefooter = (Number(b) === 1) ? true : false;
										Athena.HIDE_FOOTER = c.hidefooter;
									} else {
										if (a === "UID") {
											Athena.userModel.userId = b;
										} else {
											if (a === "username") {
												Athena.userModel.username = b;
											} else {
												if (a === "CID") {
													Athena.userModel.contentId = b;
												} else {
													if (a === "HMHID") {
														Athena.userModel.hmhId = b;
													} else {
														if (a === "OL") {
															c.isOnline = Boolean(Number(b));
														} else {
															c[a] = b;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	},
	parseParams : function(g) {
		var c, e, a, b = [], d = [], f = {};
		if (g !== undefined) {
			a = g.split("|");
			e = a.length;
			for (c = 0; c < e; c++) {
				if (c & 1) {
					d.push(a[c]);
				} else {
					b.push(a[c]);
				}
			}
			e = b.length;
			for (c = 0; c < e; c++) {
				this.parseParam(b[c], d[c], f);
			}
		}
		return f;
	},
	live_route : function() {
		this.setStandardValues();
	},
	setStandardValues : function() {
		Athena.settingsModel.loadData();
	},
	dlo_edition : function(b, c, a, d) {
		if (a !== undefined) {
			Athena.settingsModel.deeplink = a;
		}
		if (b !== undefined) {
			Athena.settingsModel.edition = b;
		}
		if (c !== undefined && c !== "NA") {
			Athena.settingsModel.region = c;
		}
		if (d !== undefined) {
			Athena.settingsModel.param = d;
		}
		this.setStandardValues();
	},
	dlo_live_edition : function(b) {
		var a;
		if (b !== undefined) {
			a = this.parseParams(b);
		} else {
			a = this.formatQueryParams(queryparams);
		}
		Athena.settingsModel.param = a;
		if (a.edition !== undefined) {
			Athena.settingsModel.edition = a.edition;
		} else {
			Athena.settingsModel.edition = "SE";
		}
		if (a.state !== undefined) {
			Athena.settingsModel.region = a.state;
		} else {
			Athena.settingsModel.region = "NA";
		}
		if (a.deeplink !== 0 || a.deeplink !== undefined) {
			Athena.settingsModel.deeplink = a.deeplink;
		}
		this.setStandardValues();
	},
	dev_editions : function(g, b, d, c, a, f) {
		var e;
		if (f !== undefined) {
			e = this.parseParams(f);
		} else {
			e = this.formatQueryParams(queryparams);
		}
		this.setCustomValuesAlt(g, b, d, c, a, e);
	},
	dev_unit : function(f, a, g, i, c, d, e, h, b) {
		this.setCustomValues(f, a, g, i, c, d, e, h, b);
	},
	setCustomValues : function(m, d, j, n, h, k, l, g, f, e, c, a) {
		Athena.LOAD_MODE = "DLO";
		var i, o, b;
		if (m) {
			Athena.settingsModel.project = m;
		}
		if (d) {
			Athena.settingsModel.category = d;
		}
		if (j) {
			Athena.settingsModel.grade = j;
		}
		if (n) {
			Athena.settingsModel.lesson = n;
		}
		if (h) {
			Athena.settingsModel.dlo = h;
		}
		if (k !== undefined) {
			Athena.settingsModel.edition = k;
		}
		if (l !== undefined && l !== "NA") {
			Athena.settingsModel.region = l;
		}
		if (g !== 0 || g !== undefined) {
			Athena.settingsModel.deeplink = g;
		}
		if (f !== undefined) {
			b = this.parseParams(f);
		} else {
			b = this.formatQueryParams(queryparams);
		}
		Athena.settingsModel.param = b;
		if (e !== undefined) {
			Athena.settingsModel.param2 = e;
		}
		if (c !== undefined) {
			Athena.settingsModel.param3 = c;
		}
		if (a !== undefined) {
			Athena.settingsModel.param4 = a;
		}
		Athena.settingsModel.path = m + "_" + Athena.settingsModel.path;
		o = "dlo_types/" + m + "/" + d + "/unit_" + j + "/module_" + n + "/lesson_" + h + "/";
		i = d + "_" + j + "_" + n + "_" + h;
		Athena.settingsModel.add({
			id : "dlo-path",
			value : o
		});
		Athena.dlolocaleModel.setName(i);
		Athena.settingsModel.loadData();
	},
	setCustomValuesAlt : function(g, c, f, d, b, e) {
		Athena.LOAD_MODE = "DLO";
		var h, a;
		if (g) {
			Athena.settingsModel.project = g;
		}
		if (c) {
			Athena.settingsModel.category = c;
		}
		if (f) {
			Athena.settingsModel.grade = f;
		}
		if (d) {
			Athena.settingsModel.lesson = d;
		}
		if (b) {
			Athena.settingsModel.dlo = b;
		}
		Athena.settingsModel.param = e;
		if (e !== undefined && e.length !== 0) {
			if (e.edition !== undefined) {
				Athena.settingsModel.edition = e.edition;
			} else {
				Athena.settingsModel.edition = "SE";
			}
			if (e.state !== undefined) {
				Athena.settingsModel.region = e.state;
			} else {
				Athena.settingsModel.region = "NA";
			}
			if (e.deeplink !== 0 || e.deeplink !== undefined) {
				Athena.settingsModel.deeplink = e.deeplink;
			}
		} else {
			Athena.settingsModel.edition = "SE";
			Athena.settingsModel.region = "NA";
		}
		Athena.settingsModel.path = g + "_" + Athena.settingsModel.path;
		a = "dlo_types/" + g + "/" + c + "/unit_" + f + "/module_" + d + "/lesson_" + b + "/";
		h = c + "_" + f + "_" + d + "_" + b;
		Athena.settingsModel.add({
			id : "dlo-path",
			value : a
		});
		Athena.dlolocaleModel.setName(h);
		Athena.settingsModel.loadData();
	}
});
Athena.PluginDataCollection = Athena.CoreCollection.extend({
	annotationIds : {},
	dataClass : null,
	initialize : function() {
		try {
			this.dataClass = new InstructionQuestion();
		} catch (a) {
			debug("InstructionQuestion: Could not be initialised");
		}
	},
	update : function(d, a) {
		if (queryparams.OL !== "0" || typeof (queryparams.OL) === "undefined") {
			if (Athena.userModel.userId !== undefined && Athena.userModel.contentId !== undefined && Athena.userModel.hmhId !== undefined) {
				var b = Athena.dlolocaleModel.selectedActivity, e = this.annotationIds[b], c;
				if (e !== undefined) {
					e = this.annotationIds[b][d.id];
				}
				if (e !== undefined) {
					c = e.annoId;
				}
				this.send(a, c, d.id, d.value, d);
				debug("UPDATE DATA: ", a, c, d.id, d.value, d);
			}
		}
	},
	send : function(j, e, c, g) {
		var l = Athena.userModel.userId, k = Athena.userModel.contentId, b = Athena.userModel.hmhId, d = Athena.xapi.actorTypes[Athena.settingsModel.discipline ? "teacher"
				: "student"], i = "#" + (Athena.dlolocaleModel.selectedActivity + 1) + ".1", h = {
			object : {
				definition : {}
			}
		}, f = 2, a = Athena.dlolocaleModel.selectedActivity;
		if (this.dataClass) {
			if (e === undefined) {
				this.dataClass.set(l, k, b, d, i, j, f, c, g, e, function(m) {
					debug("CAS: Create Complete", m);
				});
			} else {
				this.dataClass.set(l, k, b, d, i, j, f, c, g, e, function(m) {
					debug("CAS: Update Complete", m);
				});
			}
		} else {
			if (e === undefined) {
				this.set(l, k, b, d, i, j, f, c, g, e, function(m) {
					debug("CAS: Create Complete", m);
				});
			} else {
				this.set(l, k, b, d, i, j, f, c, g, e, function(m) {
					debug("CAS: Update Complete", m);
				});
			}
		}
	},
	set : function(e, n, c, b, d, a, m, l, j, i, g) {
		var k = {
			object : {
				definition : {}
			}
		}, f = {}, h;
		f.content_id = a;
		f.created_by_tool_id = 16;
		f.type_id = m;
		f.title = "";
		f.body_text = "";
		h = Athena.xapi.createStatement(e, b, Athena.xapi.verbTypes.finished, Athena.xapi.objectTypes.instructionq, l, n, c, d, k, j);
		f.data = JSON.stringify(h);
		f.visibility_id = 1;
		debug(e, n, c, b, d, a, m, l, j, i, g);
		if (i === undefined) {
			annService.createAnnotation(f, g);
		} else {
			annService.updateAnnotation(i, f, g);
		}
	},
	get : function(e) {
		var a = this, b = Athena.dlolocaleModel.selectedActivity, d;
		try {
			if (this.dataClass) {
				this.dataClass.get(function(f) {
					a.received(f);
					e();
				}, function(f) {
					a.error(f);
				});
			} else {
				if (Athena.dlolocaleModel.models[b].attributes.objectId !== undefined) {
					d = Athena.dlolocaleModel.models[b].attributes.objectId;
					annService.Annotations.find({
						content_id : d,
						type_id : 2
					}).done(function(f) {
						a.received(f);
						e();
					}).fail(function(f) {
						a.error(f);
					}).run();
				}
			}
		} catch (c) {
			debug(c);
		}
	},
	received : function(e) {
		debug("CAS: Plugin Data Received", e);
		var b, d, c = Athena.dlolocaleModel.selectedActivity, a;
		d = e.length;
		this.annotationIds[c] = {};
		for (b = 0; b < d; b++) {
			a = JSON.parse(e[b].data);
			this.annotationIds[c][a.object.definition.object_id] = {
				annoId : e[b].annotation_id,
				value : a.result.response
			};
		}
	},
	error : function(a) {
		debug("Error: An error has occurred whilst connecting to the CAS");
	},
	formatCustomData : function() {
		var b = Athena.dlolocaleModel.data.customValues, a = {};
		if (b !== undefined) {
			$(b).find("options").each(function() {
				$(this).find("option").each(function() {
					var d = $(this).attr("id"), c;
					if (d === Athena.settingsModel.region + "-" + Athena.settingsModel.param.category + "-HMHID") {
						c = $(this).text();
						if (Athena.settingsModel.edition !== "SE") {
							c = c.replace("ISE", "ITE");
						}
						queryparams.HMHID = c;
						Athena.userModel.hmhId = queryparams.HMHID;
					}
					if (d === Athena.settingsModel.region + "-" + Athena.settingsModel.param.category + "-CID") {
						queryparams.CID = $(this).text();
						Athena.userModel.contentId = queryparams.CID;
					}
				});
				Athena.userModel.userId = queryparams.UID;
				Athena.userModel.username = queryparams.username;
			});
			$(b).find("shell").each(function() {
				a[$(this).attr("id")] = $(this).text();
			});
			$(b).find("activity").each(function() {
				var c = $(this).attr("count");
				a[c] = {};
				$(this).find("wfid").each(function() {
					a[c][$(this).attr("id")] = $(this).text();
				});
			});
			$(b).find("resources").each(function() {
				if (a.resources === undefined) {
					a.resources = {};
				}
				var c = $(this).attr("id");
				a.resources[c] = {
					titles : "",
					links : ""
				};
				$(this).find("titles").each(function() {
					a.resources[c].titles = $(this).text();
				});
				$(this).find("links").each(function() {
					a.resources[c].links = $(this).text();
				});
			});
		}
		Athena.dlolocaleModel.data.customValues = a;
	}
});
Athena.XAPIDataCollection = Athena.CoreCollection
		.extend({
			objectIds : {},
			dataClass : null,
			initialize : function() {
				try {
					this.dataClass = new XAPIRequests();
					if (Athena.xapi === undefined || !Athena.xapi) {
						Athena.xapi = new XAPI();
					}
				} catch (a) {
					debug("XAPIRequests: Could not be initialised");
				}
			},
			getCredentials : function() {
				var a = this, b = "/api/identity/v1/token;contextId=hmof";
				if (XAPITEST !== undefined || XAPITEST === true) {
					b = "http://my-test.hrw.com/api/identity/v1/token;contextId=hmof";
				}
				window.Identity.getCredentials(b, function(c) {
					a.authenticateResponse(c);
				}, this.authenticateError);
			},
			authenticateResponse : function(b) {
				var a = JSON.parse(b);
				txm.XapiService.authorization = a.access_token;
				txm.XapiService.ref_id = a.ref_id;
				window.Identity.SIFID = a;
				if (window.Identity.SIFID.roles === undefined) {
					window.Identity.SIFID.roles = "Student";
				}
			},
			authenticateError : function(c, b, a) {
				debug("An error occurred with the request");
			},
			update : function(b, a) {
				this.send(a, b.id, b.value, b);
			},
			set : function(j, c, g, e) {
				if (new RegExp("ftq", "igm").test(c)) {
					this.dispatch(Athena.PRELOADER_SHOW);
					this.dispatch(Athena.PRELOADER_UPDATE, Athena.localeModel.get("preload-save-data"));
				}
				var m = this, l = txm.XapiService.ref_id, k = (queryparams.CID !== undefined) ? queryparams.CID : Athena.userModel.contentiId, b = (queryparams.HMHID !== undefined) ? queryparams.HMHID
						: Athena.userModel.hmhId, i = "#" + ((Athena.dlolocaleModel) ? Athena.dlolocaleModel.selectedActivity : 1) + ".1", h = {
					object : {
						definition : {}
					}
				}, f = 2, a, d;
				if (Athena.dlolocaleModel) {
					a = Athena.dlolocaleModel.selectedActivity;
				} else {
					a = 0;
				}
				if (window.Identity.SIFID === undefined || window.Identity.SIFID.roles === undefined) {
					if (queryparams.U !== undefined) {
						if (queryparams.U === "1efbbef0f49ec5327ffd389831cbe95e") {
							d = "Instructor";
						} else {
							d = "Student";
						}
					} else {
						d = "Student";
					}
				} else {
					d = window.Identity.SIFID.roles;
				}
				debug("SENDING TO SERVER", l, k, b, d, i, Athena.userModel.contentId + "!" + Athena.userModel.hmhId + "!" + c, f, j, g);
				if (this.dataClass) {
					this.dataClass.set(l, k, b, d, i, Athena.userModel.contentId + "!" + Athena.userModel.hmhId + "!" + c, f, j, g, function(n) {
						if (new RegExp("ftq", "igm").test(c)) {
							m.dispatch(Athena.PRELOADER_HIDE);
						}
						if (e !== undefined) {
							e(n);
						}
					});
				}
			},
			get : function(d, e, h) {
				var c = this, a, b, g = 0, f = e.length;
				if (this.dataClass) {
					a = function() {
						try {
							c.dataClass.get(function(j) {
								c.received(d, e[g], j);
								b(h);
							}, function(j) {
								b(h);
								debug("An error occurred and the data could not be retreived");
							}, Athena.userModel.contentId + "!" + Athena.userModel.hmhId + "!" + e[g]);
						} catch (i) {
							debug("An error occurred");
						}
					};
					b = function(i) {
						g++;
						if (g < f) {
							a();
						} else {
							if (i !== undefined) {
								i();
							}
						}
					};
					a();
				} else {
					debug("Class Loaded");
				}
			},
			received : function(c, a, b) {
				if (this.objectIds[c] === undefined) {
					this.objectIds[c] = {};
				}
				this.objectIds[c][a] = {
					value : b.result.response
				};
			},
			error : function(a) {
				debug("Error: An error has occurred whilst connecting to the XAPI service");
			}
		});
Athena.CustomInitialiser = function() {
	Athena.TRACK_EVENTS = true;
	Athena.PALETTE_HIDE = "palette:hide";
	Athena.PALETTE_SHOW = "palette:show";
	Athena.PALETTE_UPDATE = "palette:update";
	Athena.MENUS_ALL_CLOSE = "menus:allclose";
	Athena.MENUS_NEED_SCROLL = "menus:scroll";
	Athena.EVENT_TRACK = "event:track";
	Athena.MEDIA_STOPALL = "media:stopall";
	Athena.NAVIGATION_CLICKED = "navigation:clicked";
	Athena.TOGGLE_TEACHER_MENU = "teacherpanel:toggle";
	Athena.pluginDataCollection = new Athena.PluginDataCollection();
	Athena.onVideoLoaded = function(a) {
		Athena.experience = new RegExp("ios|ipad|iphone|ipod|android", "ig").test(navigator.userAgent) ? brightcove.api.getExperience(a) : brightcove.getExperience(a);
		Athena.videoPlayer = Athena.experience.getModule(APIModules.VIDEO_PLAYER);
		Athena.videoPlayer.addEventListener(BCMediaEvent.BEGIN, function(b) {
			Athena.eventBus.trigger(Athena.MEDIA_STOPALL);
			Athena.trackingEventHandler(Athena.VIDEO_PLAY, {
				objectId : $("#" + a).parent().parent().parent().data("objectid")
			});
		});
		Athena.videoPlayer.addEventListener(BCMediaEvent.PLAY, function(b) {
			Athena.eventBus.trigger(Athena.MEDIA_STOPALL);
			Athena.trackingEventHandler(Athena.VIDEO_PLAY, {
				objectId : $("#" + a).parent().parent().parent().data("objectid")
			});
		});
		Athena.videoPlayer.addEventListener(BCMediaEvent.COMPLETE, function(b) {
			Athena.trackingEventHandler(Athena.VIDEO_COMPLETE, {
				objectId : $("#" + a).parent().parent().parent().data("objectid")
			});
		});
	};
	Athena.onVideoLoadedReady = function(a) {
		Athena.experience = new RegExp("ios|ipad|iphone|ipod|android", "ig").test(navigator.userAgent) ? brightcove.api.getExperience(a) : brightcove.getExperience(a);
		Athena.videoPlayer = Athena.experience.getModule(APIModules.VIDEO_PLAYER);
		Athena.videoPlayer.addEventListener(BCMediaEvent.BEGIN, function(b) {
			Athena.eventBus.trigger(Athena.MEDIA_STOPALL);
			Athena.trackingEventHandler(Athena.VIDEO_PLAY, {
				objectId : $("#" + a).parent().parent().parent().data("objectid")
			});
		});
		Athena.videoPlayer.addEventListener(BCMediaEvent.PLAY, function(b) {
			Athena.eventBus.trigger(Athena.MEDIA_STOPALL);
			Athena.trackingEventHandler(Athena.VIDEO_PLAY, {
				objectId : $("#" + a).parent().parent().parent().data("objectid")
			});
		});
	};
	Athena.initCAS = function(h) {
		if (queryparams.UID !== undefined && queryparams.CID !== undefined && queryparams.HMHID !== undefined) {
			var g = new AnnModel(), f = (queryparams.server !== undefined) ? queryparams.server : "http://dubv-dpdiweb01.dubeng.local/", a = (queryparams.role !== undefined) ? queryparams.role
					: "Student", c = (queryparams.UID !== undefined) ? queryparams.UID : "student1", e = (queryparams.platform !== undefined) ? queryparams.platform : "HMOF", b = {}, d = document.location
					.toString();
			if (Athena.LOAD_MODE === "DLO") {
				b = {
					serverURL : f,
					usePostTunneling : false,
					token : null,
					clientId : "DLO",
					cors : true,
					successHandler : null
				};
				annService.setServerURL(f);
				annServiceTokenGenerator.getToken({
					userId : c,
					role : a,
					platform : e
				}, function(i) {
					b.token = i;
					b.successHandler = h;
					annService.init(b);
				}, function(i) {
					debug("## Error: could not retrieve token");
				});
			} else {
				b = {
					clientId : "DLO",
					successHandler : null
				};
				b.successHandler = h;
				annService.init(b);
			}
		}
	};
	Athena.initTracking = function() {
		Athena.xapi = new XAPI();
		Athena.xapi.registerAsStatementListener(this, Athena.xapiPlayerRequestHandler);
		Athena.eventBus.on(Athena.ACTIVITY_VISITED, function() {
			Athena.trackingEventHandler(Athena.ACTIVITY_VISITED);
		}, this);
		Athena.eventBus.on(Athena.APP_STARTUP, function() {
			Athena.trackingEventHandler(Athena.APP_STARTUP);
		}, this);
		Athena.eventBus.on(Athena.GLOSSARY_SHOW, function() {
			Athena.trackingEventHandler(Athena.GLOSSARY_SHOW);
		}, this);
		Athena.eventBus.on(Athena.GLOSSARY_HIDE, function() {
			Athena.trackingEventHandler(Athena.GLOSSARY_HIDE);
		}, this);
		Athena.eventBus.on(Athena.NAVIGATION_CLICKED, function(a) {
			Athena.trackingEventHandler(Athena.NAVIGATION_CLICKED, a);
		}, this);
		Athena.eventBus.on(Athena.EVENT_TRACK, function(a) {
			Athena.trackingEventHandler(Athena.EVENT_TRACK, a);
		}, this);
	};
	Athena.xapiPlayerRequestHandler = function(c) {
		if (c.object.id === Athena.xapi.objectTypes.command.object.id) {
			var b = jQuery.parseJSON(c.object.definition.data), d, a;
			switch (b.command) {
			case "navigateTo":
				d = 0;
				a = b.params.path.slice(1).split(".");
				d = ((a[0] === null) ? 0 : (Number(a[0]) - 1));
				Athena.shellView.menuView.checkSplash();
				Athena.eventBus.trigger(Athena.AUDIO_STOP);
				Athena.eventBus.trigger(Athena.MEDIA_STOPALL);
				Athena.eventBus.trigger(Athena.ACTIVITY_DISABLE, Athena.dlolocaleModel.selectedActivity);
				Athena.eventBus.trigger(Athena.IMAGE_HIDE);
				Athena.eventBus.trigger(Athena.EXCONTENT_HIDE);
				Athena.dlolocaleModel.selectedActivity = d;
				Athena.eventBus.trigger(Athena.ACTIVITY_CHANGED, 1);
				Athena.eventBus.trigger(Athena.MENUARROW_UPDATE);
				Athena.eventBus.trigger(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
				break;
			case "toggleTeacherPanel":
				Athena.shellView.teacherView.$el.css({
					top : 0
				});
				Athena.shellView.teacherView.togglePanel(true);
				break;
			}
		}
	};
	Athena.trackingEventHandler = function(c, d) {
		var j, e, p, i, h, q, o, b, n, f = Athena.xapi.actorTypes[Athena.settingsModel.discipline ? "teacher" : "student"], m = "#" + (Athena.dlolocaleModel.selectedActivity + 1)
				+ ".1", k = {
			object : {
				definition : {}
			}
		}, g = null;
		if (queryparams.CID !== undefined) {
			o = queryparams.CID;
		} else {
			o = "9780544221581";
		}
		if (queryparams.HMHID !== undefined) {
			b = queryparams.HMHID;
		} else {
			b = "GM_CA15E_ISE_G02U2C03L03S00_0025-001";
		}
		if (queryparams.UID !== undefined) {
			q = queryparams.UID;
		} else {
			q = "12345678";
		}
		if (d !== undefined) {
			if (d.objectId !== undefined || d.objectId !== null) {
				n = d.objectId;
			}
		} else {
			n = "1_0_1_1_0_1_0_1_p1";
		}
		if (Athena.isAssessmentStarted === true) {
			g = Athena.xapi.createStatement(q, f, Athena.xapi.verbTypes.finished, Athena.xapi.objectTypes.assessment, n, o, b, m, k);
			Athena.xapi.sendStatement(g, Athena.xapiRequestCallbackFunction);
			Athena.isAssessmentStarted = false;
		}
		switch (c) {
		case Athena.EVENT_TRACK:
			if (d.intType === "instructionq") {
				h = Athena.xapi.objectTypes.instructionq;
				if (d.result !== null) {
					k.object.definition.classid = "incorrect";
					if (d.result === true || d.result === "pass") {
						k.object.definition.classid = "correct";
					}
				}
			}
			if (d.intType === "assessment") {
				h = Athena.xapi.objectTypes.assessment;
				Athena.isAssessmentStarted = true;
			}
			if (d.intType === "performance") {
				h = Athena.xapi.objectTypes.performance;
			}
			if (d.intType === "tool") {
				h = Athena.xapi.objectTypes.tool;
				k.object.definition.classid = d.result;
			}
			g = Athena.xapi.createStatement(q, f, Athena.xapi.verbTypes.started, h, n, o, b, m, k);
			break;
		case Athena.APP_STARTUP:
			i = Athena.dlolocaleModel.selectedActivity;
			if (Athena.dlolocaleModel.models[i].attributes.objectId !== undefined) {
				n = Athena.dlolocaleModel.models[i].attributes.objectId;
			}
			g = Athena.xapi.createStatement(q, f, Athena.xapi.verbTypes.initialized, Athena.xapi.objectTypes.navigation, n, o, b, m, k);
			break;
		case Athena.ACTIVITY_VISITED:
			i = Athena.dlolocaleModel.selectedActivity;
			if (Athena.dlolocaleModel.models[i].attributes.objectId !== undefined) {
				n = Athena.dlolocaleModel.models[i].attributes.objectId;
			}
			g = Athena.xapi.createStatement(q, f, Athena.xapi.verbTypes.launched, Athena.xapi.objectTypes.navigation, n, o, b, m, k);
			break;
		case Athena.NAVIGATION_CLICKED:
			i = Athena.dlolocaleModel.selectedActivity;
			if (Athena.dlolocaleModel.models[i].attributes.objectId !== undefined) {
				n = Athena.dlolocaleModel.models[i].attributes.objectId;
			}
			e = Number(Athena.dlolocaleModel.selectedActivity) + 1;
			j = e;
			if (d.result === "next") {
				j = e + 1;
			}
			if (d.result === "prev") {
				j = e - 1;
			}
			k = {
				object : {
					definition : {
						data : '{"command": "navigateTo", "params": { "to": "#' + j + '.1","from": "' + m + '","direction": "' + d.result + '"} }'
					}
				}
			};
			g = Athena.xapi.createStatement(q, f, Athena.xapi.verbTypes.launched, Athena.xapi.objectTypes.command, n, o, b, m, k);
			break;
		case Athena.VIDEO_PLAY:
		case Athena.VIDEO_COMPLETE:
			p = (c === Athena.VIDEO_PLAY) ? Athena.xapi.verbTypes.started : Athena.xapi.verbTypes.finished;
			g = Athena.xapi.createStatement(q, f, p, Athena.xapi.objectTypes.video, n, o, b, m, k);
			break;
		default:
			break;
		}
		if (typeof window.Bridge === "undefined") {
			Athena.xapi.sendStatement(g, Athena.xapiRequestCallbackFunction);
		} else {
			window.Bridge.sendPlayerMessage(g, Athena.xapiRequestCallbackFunction);
		}
	};
	Athena.xapiRequestCallbackFunction = function(a) {};
};
Athena.QueryValidator = function() {
	function d() {
		Athena.init();
		if (Athena.TRACK_EVENTS) {
			Athena.initTracking();
		}
		if (queryparams.lang === undefined) {
			Athena.settingsModel.locale = "en-US";
		} else {
			Athena.settingsModel.locale = queryparams.lang;
		}
	}

	function c(i) {
		var f = 0, g = 0, j = "", h = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", e = h.split("");
		i.split("").forEach(function(k) {
			f = (f << 6) + e.indexOf(k);
			g += 6;
			while (g >= 8) {
				j += String.fromCharCode((f >>> (g -= 8)) & 255);
			}
		});
		return j;
	}

	function a() {
		var g, e, f;
		if (document.cookie.indexOf("SecurityToken=") > -1) {
			e = document.cookie.split("SecurityToken=");
			f = e[1].split("&");
			e = f[0].split(".");
			e = c(e[1]).split("uid=");
			f = e[1].split(",");
			g = f[0];
		} else {
			g = false;
		}
		return g;
	}

	function b() {
		var g, e, f;
		if (document.cookie.indexOf("SecurityToken=") > -1) {
			e = document.cookie.split("SecurityToken=");
			f = e[1].split("&");
			e = f[0].split(".");
			e = c(e[1]).split('DisplayName":"');
			f = e[1].split('"');
			g = f[0];
		} else {
			g = false;
		}
		return g;
	}
	this.checkBridge = function() {
		var e, f, g, h;
		if (typeof (queryparams.UID) === "undefined") {
			e = a();
			if (e !== false) {
				queryparams.UID = e;
			}
		}
		if (typeof (queryparams.username) === "undefined") {
			h = b();
			if (h !== false) {
				queryparams.username = h;
			}
		}
		if (typeof (window.Bridge) !== "undefined") {
			queryparams.HH = 1;
			queryparams.HS = 1;
			window.xapi = new XAPI();
			window.xapi.registerAsStatementListener(document, window.xapiPlayerRequestHandler);
			g = {
				object : {
					definition : {
						data : '{"command": "requestParams" }'
					}
				}
			};
			f = window.xapi.createStatement("1", "player", window.xapi.verbTypes.launched, window.xapi.objectTypes.command, "na", "0", "0", "0", g);
			window.Bridge.sendPlayerMessage(f, window.handlePlayerParams);
		} else {
			d();
		}
	};
	window.handlePlayerParams = function(e) {
		if (e.HMHID !== "undefined") {
			queryparams.HMHID = e.HMHID;
		}
		if (e.UID !== "undefined") {
			queryparams.UID = e.UID;
		}
		if (e.CID !== "undefined") {
			queryparams.CID = e.CID;
		}
		d();
	};
};