(function() {

	var sendCollabMessage = window.sendCollabMessage = window.sendCollabMessage || function(message) {
		console.log("MOCK Collab Message: " + JSON.stringify(message));
	};

	var _scrollTargetSection;
	var _scrollTargetPosition;

	var sectionScrollerInterval = setInterval(function() {
		if (!_scrollTargetSection) {
			return;
		}
		_scrollTargetSection.scrollTop(_scrollTargetPosition);
		if (Math.abs(_scrollTargetSection.scrollTop() - _scrollTargetPosition) < 10) {
			_scrollTargetSection = null;
		}
	}, 50);

	var handleSectionScroll = function(sectionId, position) {
		_scrollTargetPosition = position;
		_scrollTargetSection = $('section#' + sectionId);
		if (!_scrollTargetSection[0]) {
			_scrollTargetSection = null;
		}
	};

	var handleDragDrop = function(dragSelector, dropSelector) {
		var $drag = $(dragSelector);
		var $drop = $(dropSelector);

		$drag.simulate("drag", {
			dropTarget : $drop,
			duration : 250
		});
	};

	var dispatchScrollEvent = function(target) {
		var e = document.createEvent("HTMLEvents");
		e.initEvent("scroll", true, false);
		target.dispatchEvent(e);
	};

	var progressiveScroll = function(target, scrollTop, callback) {
		var initialPosition = target.scrollTop;
		// Some DLOs have a progressive disclosure feature that can only cope with a certain amount of
		// downward scrolling at a time, so don't exceed that.
		if (scrollTop - initialPosition <= 500) {
			target.scrollTop = scrollTop;
			dispatchScrollEvent(target);

			if (callback) {
				callback();
			}
		} else {
			target.scrollTop += 500;
			dispatchScrollEvent(target);
			window.setTimeout(function() {
				progressiveScroll(target, scrollTop);
			}, 50);
		}
	};

	var tryInitialScroll = function(targetId, scrollTop, maxTries) {
		var target = document.getElementById(targetId);
		var onFail = function() {
			if (maxTries > 1) {
				setTimeout(function() {
					tryInitialScroll(targetId, scrollTop, maxTries - 1);
				}, 500);
			} else {
				console.error("Giving up an attempt to scroll " + targetId + " to " + scrollTop);
			}
		};

		// Just because the DLO reported itself as initialized doesn't mean that it's initialized
		// to the point where scrolling will work. Keep trying until the target element exists and
		// scrolling it actually has an effect.
		if (target) {
			progressiveScroll(target, scrollTop, function() {
				if (target.scrollTop !== scrollTop) {
					onFail();
				}
			});
		} else {
			onFail();
		}
	};

	window.onCollabMessage = function(message) {

		switch (message.event) {
		case "scrollTo":
			var data = message.data;
			progressiveScroll(document.getElementById(data.targetId), data.scrollTop);
			break;
		case "dragDrop":
			handleDragDrop(message.data.dragSelector, message.data.dropSelector);
			break;
		case "dropdownOpen":
			$(message.data.selectorId).click();
			break;
		case "closeOpenDropdowns":
		case "dropdownClose":
			Athena.DropDownCreator.prototype.hideAllOpendDropDown();
			break;
		case "dropDownItemSelected":
			$(message.data.dropdownSelector).parent().parent().find(".option.drop-down-options [value='" + message.data.selectedValue + "']").click();
			break;
		case "freeTextInputChanged":
			$(message.data.selectorId).html(message.data.text || "");
			break;
		case "freeTextOpenMathSymbols":
			$(message.data.btnSelector).click();
			break;
		case "freeTextCloseMathSymbols":
			$('.math-palette .button-close').click();
			break;
		case "fillInTheBlankTextUpdated":
			$(message.data.selector).val(message.data.text || "");
			break;
		case "sliderRevealDropped":
			handleDragDrop(message.data.draggerSelector, message.data.dropTargetSelector);
			break;
		case "multiChoiceSelect":
			$(message.data.btnId).click();
			break;
		}

	};

	document.addEventListener("scroll", function(event) {
		if (event.target.id) {
			sendCollabMessage({
				event : "scrollTo",
				data : {
					targetId : event.target.id,
					scrollTop : event.target.scrollTop
				}
			});
		}

	}, true /* Important: run in the capturing phase so we get called before other handlers stop propagation */);

	window.Collab = {
		initialize : function() {},
		multiChoiceSelect : function(btnId) {
			sendCollabMessage({
				event : "multiChoiceSelect",
				data : {
					btnId : btnId,
				}
			});
		},
		sliderRevealDropped : function(draggerSelector, dropTargetSelector) {
			sendCollabMessage({
				event : "sliderRevealDropped",
				data : {
					draggerSelector : draggerSelector,
					dropTargetSelector : dropTargetSelector
				}
			});
		},

		freeTextShowSymbolPopup : function(ftWidget) {
			var btnSelector = 'div[data-objectid="' + ftWidget.parentContainer.attr('data-objectid') + '"] .formula-btn';
			sendCollabMessage({
				event : "freeTextOpenMathSymbols",
				data : {
					btnSelector : btnSelector
				}
			});
		},

		fillInTheBlankTextUpdated : function(selector, text) {
			sendCollabMessage({
				event : "fillInTheBlankTextUpdated",
				data : {
					selector : selector,
					text : text
				}
			});
		},

		freeTextHideSymbolPopup : function() {
			sendCollabMessage({
				event : "freeTextCloseMathSymbols"
			});
		},

		freeTextInputChanged : function(widgetDataId, text) {
			sendCollabMessage({
				event : "freeTextInputChanged",
				data : {
					selectorId : "div[data-objectid='" + widgetDataId + "'] .text-container",
					text : text
				}
			});
		},
		dropdownOpen : function(selectorId) {
			sendCollabMessage({
				event : "dropdownOpen",
				data : {
					selectorId : selectorId
				}
			});
		},
		dropDownItemSelected : function(dropdownSelector, selectedValue) {
			sendCollabMessage({
				event : "dropDownItemSelected",
				data : {
					dropdownSelector : dropdownSelector,
					selectedValue : selectedValue
				}
			});
		},
		closeOpenDropdowns : function() {
			sendCollabMessage({
				event : "closeOpenDropdowns"
			});
		},
		dropdownClose : function() {
			sendCollabMessage({
				event : "dropdownClose"
			});
		},
		onDragDrop : function(sectionId, activityId, dragDropObjectId, dropTargetIndex, dragElemId) {
			var containerSelector = 'section#' + sectionId + ' .' + activityId + ' div[data-objectid="' + dragDropObjectId + '"]';
			var dragSelector = containerSelector + " #" + dragElemId
			var dropSelector;
			if (dropTargetIndex == -1) {
				dropSelector = containerSelector + " #" + dragElemId + ".after-drop";
			} else {
				dropSelector = containerSelector + " .drop:eq(" + dropTargetIndex + ")";
			}
			sendCollabMessage({
				event : "dragDrop",
				data : {
					dragSelector : dragSelector,
					dropSelector : dropSelector
				}
			});
		}
	};

})();
