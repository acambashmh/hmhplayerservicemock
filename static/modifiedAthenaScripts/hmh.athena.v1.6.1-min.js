/*jslint nomen: true, plusplus:true, regexp:true, evil:true, newcap:true */
/*global opera, $, _, Detection, TransitionManager, Backbone, BackboneMVC, debug, console, Athena:true, TweenMax, Sine, Power0, Circ, Quad, Cubic, Quart, Quint, Back, Bounce, Elastic, Expo, Sine, ActiveXObject, $LAB, ATHENA_HIDESPLASH, ATHENA_HIDEUI, Debug, Element */
/*
 * HMH Athena Architecture
 * @description - Athena DLO architecture
 * @author Nikolas Stratis
 * @copyright (c) 2013 - 2014 Houghton Mifflin Harcourt Publish Company. All Rights Reserved.
 * No unauthorised copying or distribution of this codebase in part or as a whole is permitted.
 */
var Athena = Athena || {}, PluginObject = {};
(function() {
	"use strict";
	window.debug = function() {
		if (Athena.APP_MODE_DEBUG) {
			var c;
			try {
				console.log.apply(console, arguments);
			} catch (b) {
				try {
					opera.postError.apply(opera, arguments);
				} catch (d) {
					try {
						c = arguments;
						console.log(c[0]);
					} catch (a) {}
				}
			}
		}
	};
	if (window.addEventListener) {
		window.addEventListener("message", function(a) {
			Athena.handleMessage(a);
		}, false);
	} else {
		window.attachEvent("onmessage", function(a) {
			Athena.handleMessage(a);
		});
	}
	Athena = {
		TWEENER_TYPE : "TweenMax",
		ENTER_KEY : 13,
		TAB_KEY : 9,
		F11_KEY : 122,
		APP_LIVE : true,
		LOAD_MODE : "LIVE",
		APP_MODE_DEBUG : false,
		USE_COMPLEX_TRANSITIONS : false,
		USE_DATA_PERSISTENCE : false,
		SCORM_ACTIVE : false,
		USE_JPLAYER_AUDIO : false,
		USE_JPLAYER_VIDEO : false,
		USE_AUTO_SIZE : false,
		HIDE_UI : false,
		HIDE_HEADER : false,
		HIDE_FOOTER : false,
		HIDE_SPLASH : false,
		CLOSE_CAPTIONS : false,
		WINDOW_WIDTH : $(window).width(),
		WINDOW_HEIGHT : $(window).height(),
		eventBus : null,
		detector : null,
		browserDetails : null,
		transitions : null,
		arrayMethods : null,
		userModel : null,
		settingsModel : null,
		activityModel : null,
		localeModel : null,
		dlolocaleModel : null,
		preloaderView : null,
		alertView : null,
		preloadController : null,
		audioController : null,
		apiController : null,
		scormCollection : null,
		fullscreenController : null,
		initialiseController : null,
		startupController : null,
		router : null,
		shellView : null
	};
	Athena.PRELOAD_START = "preload:start";
	Athena.INIT_PRELOAD_UPDATE = "initpreload:update";
	Athena.SETTINGS_PRELOAD_COMPLETE = "settingspreload:complete";
	Athena.INIT_PRELOAD_COMPLETE = "initpreload:complete";
	Athena.DLO_PRELOAD_COMPLETE = "dlopreload:complete";
	Athena.ASSET_PRELOAD_COMPLETE = "assetpreload:complete";
	Athena.APP_STARTUP = "app:startup";
	Athena.SCORM_FINISH = "scorm:Finish";
	Athena.SCORM_START = "scorm:start";
	Athena.ACTIVITY_VISITED = "activity:visited";
	Athena.ACTIVITY_START = "activity:start";
	Athena.ACTIVITY_RESET = "activity:reset";
	Athena.ACTIVITY_REPLAY = "activity:replay";
	Athena.ACTIVITY_UPDATESTEP = "activity:updatestep";
	Athena.ACTIVITY_DISABLE = "disable:activity";
	Athena.ACTIVITY_ENABLE = "enable:activity";
	Athena.ACTIVITY_CHANGED = "activity:change";
	Athena.ACTIVITY_SWITCH = "activity:switch";
	Athena.APP_RESIZE = "app:resize";
	Athena.TOGGLE_UI = "toggle:ui";
	Athena.TOGGLE_HEADER = "toggle:shellheader";
	Athena.SHELL_INIT = "shell:init";
	Athena.SHELL_ACTIVATE = "shell:activate";
	Athena.SCROLL_DISABLE = "content:disablescroll";
	Athena.SCROLL_ENABLE = "content:enablescroll";
	Athena.SCROLL_ENABLED = "activity:scrollenabled";
	Athena.API_EXECUTE = "api:execute";
	Athena.UPDATE_SHELLTEXT = "shelltext:update";
	Athena.OFFSETS_RESET = "offsets:reset";
	Athena.SCORE_SHOW = "shellscore:show";
	Athena.SCORE_HIDE = "shellscore:hide";
	Athena.SCORE_UDPATE = "shellscore:update";
	Athena.ALERT_SHOW = "alert:show";
	Athena.ALERT_HIDE = "alert:hide";
	Athena.ALERT_ACCEPT = "alert:accept";
	Athena.EXCONTENT_SHOW = "extracontent:open";
	Athena.EXCONTENT_HIDE = "extracontent:close";
	Athena.EXCONTENT_UPDATE = "extracontent:update";
	Athena.PRELOADER_SHOW = "preloader:show";
	Athena.PRELOADER_HIDE = "preloader:hide";
	Athena.PRELOADER_UPDATE = "preloader:update";
	Athena.INFO_SHOW = "info:show";
	Athena.INFO_HIDE = "info:hide";
	Athena.INFO_UPDATE = "info:update";
	Athena.INFO_CLOSED = "info:closed";
	Athena.TOOLS_SHOW = "tools:show";
	Athena.TOOLS_HIDE = "tools:hide";
	Athena.TOOLS_CLOSED = "tools:closed";
	Athena.HELP_SHOW = "help:show";
	Athena.HELP_HIDE = "help:hide";
	Athena.HELP_UPDATE = "help:update";
	Athena.HELP_CLOSED = "help:closed";
	Athena.GLOSSARY_SHOW = "glossary:show";
	Athena.GLOSSARY_HIDE = "glossary:hide";
	Athena.RESOURCES_SHOW = "resources:show";
	Athena.RESOURCES_HIDE = "resources:hide";
	Athena.AUDIO_SHOW = "audio:show";
	Athena.AUDIO_HIDE = "audio:hide";
	Athena.IMAGE_SHOW = "image:show";
	Athena.IMAGE_HIDE = "image:hide";
	Athena.CAPTIONS_SHOW = "closedcaption:show";
	Athena.CAPTIONS_HIDE = "closedcaption:hide";
	Athena.CAPTIONS_UPDATE = "closedcaption:update";
	Athena.SUCCESS_SHOW = "successview:show";
	Athena.SUCCESS_HIDE = "successview:hide";
	Athena.SUCCESS_UPDATE = "successview:update";
	Athena.INSTRUCTIONS_SHOW = "instructionpanel:show";
	Athena.INSTRUCTIONS_HIDE = "instructionpanel:hide";
	Athena.INSTRUCTIONS_UPDATE = "instructionpanel:update";
	Athena.SIDEMENU_SHOW = "sidemenu:show";
	Athena.SIDEMENU_HIDE = "sidemenu:hide";
	Athena.MENUARROW_ENABLE = "menuArrow:enable";
	Athena.MENUARROW_DISABLE = "menuArrow:disable";
	Athena.MENUARROW_UPDATE = "menuArrow:update";
	Athena.MENUARROW_STARTANIME = "menuArrow:startanime";
	Athena.MENUARROW_STOPANIME = "menuArrow:stopanime";
	Athena.MENU_CHANGE = "menuview:change";
	Athena.MENU_STARTANIME = "menuview:startanime";
	Athena.MENU_STOPANIME = "menuview:stopanime";
	Athena.MENUSUB_CHANGE = "submenu:change";
	Athena.MENUSUB_SET = "submenu:set";
	Athena.MENUSUB_DESTROY = "submenu:destory";
	Athena.MENUSUB_ENABLE = "submenu:enable";
	Athena.MENUSUB_DISABLE = "submenu:disable";
	Athena.AUDIO_INIT = "audio:init";
	Athena.AUDIO_PLAY = "audio:play";
	Athena.AUDIO_STOP = "audio:stop";
	Athena.AUDIO_PAUSE = "audio:pause";
	Athena.AUDIO_MUTE = "audio:mute";
	Athena.AUDIO_VOLUME = "audio:volume";
	Athena.AUDIO_COMPLETE = "audio:complete";
	Athena.AUDIO_TIMEUPDATE = "audio:timeupdate";
	Athena.FULLSCREEN_TOGGLE = "fullscreen:toggle";
	Athena.F11 = "f11:pressed";
	Athena.ENTER = "enter:pressed";
	Athena.TAB = "tab:pressed";
	Athena.PRINT = "";
	Athena.PAUSE = "";
	Athena.PLAY = "";
	Athena.RELOAD = "dlo:reload";
	Athena.ArrayMethods = function() {};
	Athena.ArrayMethods.prototype.cleanDuplicates = function(d, b) {
		var a, c = [];
		if (b !== undefined) {
			d.sort(this.sort(b));
		} else {
			d.sort();
		}
		for (a = 0; a < d.length; a++) {
			if (b !== undefined) {
				if (a === 0) {
					if (d[a][b] !== "") {
						c[c.length] = d[a];
					}
				} else {
					if (d[a][b] !== d[a - 1][b]) {
						if (d[a][b] !== "") {
							c[c.length] = d[a];
						}
					}
				}
			} else {
				if (d[a] !== d[a + 1]) {
					if (d[a] !== "") {
						c[c.length] = d[a];
					}
				}
			}
		}
		return c;
	};
	Athena.ArrayMethods.prototype.sort = function(b) {
		var a = 1;
		if (b[0] === "-") {
			a = -1;
			b = b.substr(1);
		}
		return function(e, d) {
			var c = (e[b] < d[b]) ? -1 : (e[b] > d[b]) ? 1 : 0;
			return c * a;
		};
	};
	Athena.Detection = function() {};
	Athena.Detection.prototype.get = function() {
		var d, f, k, a = 0, l, c, m, n, b, g = false, j;
		n = window.navigator.userAgent;
		b = window.navigator.platform;
		a = 0;
		if (/MSIE/.test(n)) {
			f = "Internet Explorer";
			if (/IEMobile/.test(n)) {
				a = 1;
			}
			k = /MSIE \d+[.]\d+/.exec(n);
			if (k) {
				if (k[0] !== undefined) {
					k = k[0].split(" ")[1];
				}
			}
			try {
				d = new ActiveXObject("ChromeTab.ChromeFrame");
				if (d) {
					g = true;
				}
			} catch (h) {}
		} else {
			if (/Chrome/.test(n)) {
				f = "Chrome";
				k = /Chrome\/[\d\.]+/.exec(n);
				if (k) {
					if (k[0] !== undefined) {
						k = k[0].split("/")[1];
					}
				}
			} else {
				if (/Opera/.test(n)) {
					f = "Opera";
					if (/mini/.test(n) || /Mobile/.test(n)) {
						a = 1;
					}
				} else {
					if (/Android/.test(n)) {
						f = "Android Webkit Browser";
						a = 1;
						l = /Android\s[\.\d]+/.exec(n);
					} else {
						if (/Firefox/.test(n)) {
							f = "Firefox";
							if (/Fennec/.test(n)) {
								a = 1;
							}
							k = /Firefox\/[\.\d]+/.exec(n);
							if (k) {
								if (k[0] !== undefined) {
									k = k[0].split("/")[1];
								}
							}
						} else {
							if (/Safari/.test(n)) {
								f = "Safari";
								if ((/iPhone/.test(n)) || (/iPad/.test(n)) || (/iPod/.test(n))) {
									l = "iOS";
									a = 1;
								}
							}
						}
					}
				}
			}
		}
		if (!k) {
			k = /Version\/[\.\d]+/.exec(n);
			if (k) {
				if (k[0] !== undefined) {
					k = k[0].split("/")[1];
				}
			} else {
				k = /Opera\/[\.\d]+/.exec(n);
				if (k) {
					if (k[0] !== undefined) {
						k = k[0].split("/")[1];
					}
				}
			}
		}
		if (b === "MacIntel" || b === "MacPPC") {
			l = "Mac OS X";
			c = /10[\.\_\d]+/.exec(n);
			if (c) {
				if (c[0] !== undefined) {
					c = c[0];
					if (/[\_]/.test(c)) {
						c = c.split("_").join(".");
					}
				}
			}
		} else {
			if (b === "Win32" || b === "Win64") {
				l = "Windows";
				m = b.replace(/[^0-9]+/, "");
			} else {
				if (!l && /Linux/.test(b)) {
					l = "Linux";
				} else {
					if (!l && /Windows/.test(n)) {
						l = "Windows";
					}
				}
			}
		}
		j = [ {
			Browser : f,
			Version : k,
			isMobile : a,
			OS : l,
			OSVersion : c,
			BitRate : m,
			hasChromeFrame : g
		} ];
		return j;
	};
	Athena.TransitionManager = function() {};
	Athena.TransitionManager.prototype.animate = function(a, c, b, f, e) {
		var d;
		if (Athena.TWEENER_TYPE === "TweenMax") {
			d = (b > 50) ? (b / 1000) : b;
			if (f === undefined || f === null) {
				TweenMax.to(a, d, {
					css : c,
					ease : this.getTweenMaxEasing(e)
				});
			} else {
				TweenMax.to(a, d, {
					css : c,
					ease : this.getTweenMaxEasing(e),
					onComplete : f
				});
			}
		} else {
			if (Athena.TWEENER_TYPE === "Jquery") {
				d = (b < 100) ? (b * 1000) : b;
				if (f === undefined || f === null) {
					a.animate(c, d);
				} else {
					a.animate(c, d, f);
				}
			}
		}
	};
	Athena.TransitionManager.prototype.getTweenMaxEasing = function(b) {
		var a = Sine.easeInOut;
		if (b === 1) {
			a = Power0.easeIn;
		}
		if (b === 2) {
			a = Power0.easeInOut;
		}
		if (b === 3) {
			a = Power0.easeOut;
		}
		if (b === 4) {
			a = Circ.easeIn;
		}
		if (b === 5) {
			a = Circ.easeInOut;
		}
		if (b === 6) {
			a = Circ.easeOut;
		}
		if (b === 7) {
			a = Quad.easeIn;
		}
		if (b === 8) {
			a = Quad.easeInOut;
		}
		if (b === 9) {
			a = Quad.easeOut;
		}
		if (b === 10) {
			a = Cubic.easeIn;
		}
		if (b === 11) {
			a = Cubic.easeInOut;
		}
		if (b === 12) {
			a = Cubic.easeOut;
		}
		if (b === 13) {
			a = Quart.easeIn;
		}
		if (b === 14) {
			a = Quart.easeInOut;
		}
		if (b === 15) {
			a = Quart.easeOut;
		}
		if (b === 16) {
			a = Quint.easeIn;
		}
		if (b === 17) {
			a = Quint.easeInOut;
		}
		if (b === 18) {
			a = Quint.easeOut;
		}
		if (b === 19) {
			a = Back.easeIn;
		}
		if (b === 20) {
			a = Back.easeInOut;
		}
		if (b === 21) {
			a = Back.easeOut;
		}
		if (b === 22) {
			a = Bounce.easeIn;
		}
		if (b === 23) {
			a = Bounce.easeInOut;
		}
		if (b === 24) {
			a = Bounce.easeOut;
		}
		if (b === 25) {
			a = Elastic.easeIn;
		}
		if (b === 26) {
			a = Elastic.easeInOut;
		}
		if (b === 27) {
			a = Elastic.easeOut;
		}
		if (b === 28) {
			a = Expo.easeIn;
		}
		if (b === 29) {
			a = Expo.easeInOut;
		}
		if (b === 30) {
			a = Expo.easeOut;
		}
		if (b === 31) {
			a = Sine.easeIn;
		}
		if (b === 32) {
			a = Sine.easeInOut;
		}
		if (b === 33) {
			a = Sine.easeOut;
		}
		return a;
	};
	Athena.CoreRouter = Backbone.Router.extend({
		routes : {
			"" : "live_route",
			"p/:param" : "live_route_p",
			"av/:id" : "deep_link",
			"av/:id/:param" : "deep_link",
			"ds/:discipline/:grade/:lesson/:dlo" : "dev_grade",
			"ds/:discipline/:grade/:lesson/:dlo/:deep_link" : "dev_grade",
			"ds/:discipline/:grade/:lesson/:dlo/:deep_link/:param" : "dev_grade",
			"dm/:prefix/:discipline/:grade/:lesson/:dlo" : "dev_grade_p",
			"dm/:prefix/:discipline/:grade/:lesson/:dlo/:deep_link" : "dev_grade_p",
			"dm/:prefix/:discipline/:grade/:lesson/:dlo/:deep_link/:param" : "dev_grade_p",
			"dc/:project/:discipline/:category/:dlo" : "dev_project",
			"dc/:project/:discipline/:category/:dlo/:deep_link" : "dev_project",
			"dc/:project/:discipline/:category/:dlo/:deep_link/:param" : "dev_project",
			"da/:prefix/:project/:discipline/:category/:dlo" : "dev_project_p",
			"da/:prefix/:project/:discipline/:category/:dlo/:deep_link" : "dev_project_p",
			"da/:prefix/:project/:discipline/:category/:dlo/:deep_link/:param" : "dev_project_p"
		},
		initialize : function(a) {},
		live_route : function() {
			this.setStandardValues();
		},
		live_route_p : function(a) {
			Athena.settingsModel.param = a;
			this.setStandardValues();
		},
		deep_link : function(a, b) {
			Athena.settingsModel.deeplink = a;
			if (b !== undefined) {
				Athena.settingsModel.param = b;
			}
			this.setStandardValues();
		},
		dev_grade : function(c, e, d, a, b, f) {
			this.setValues(null, null, c, e, null, d, a, b, f);
		},
		dev_grade_p : function(e, c, f, d, a, b, g) {
			this.setValues(e, null, c, f, null, d, a, b, g);
		},
		dev_project : function(f, c, d, a, b, e) {
			this.setValues(null, f, c, null, d, null, a, b, e);
		},
		dev_project_p : function(e, g, c, d, a, b, f) {
			this.setValues(e, g, c, null, d, null, a, b, f);
		},
		setStandardValues : function() {
			Athena.settingsModel.loadData();
		},
		setValues : function(j, l, d, k, c, m, h, g, f, e, b, a) {
			Athena.LOAD_MODE = "DLO";
			var i, n;
			if (j) {
				Athena.settingsModel.prefix = j;
			}
			if (l) {
				Athena.settingsModel.project = l;
			}
			if (d) {
				Athena.settingsModel.discipline = d;
			}
			if (c) {
				Athena.settingsModel.category = c;
			}
			if (k) {
				Athena.settingsModel.grade = k;
			}
			if (m) {
				Athena.settingsModel.lesson = m;
			}
			if (h) {
				Athena.settingsModel.dlo = h;
			}
			if (g !== 0 || g !== undefined) {
				Athena.settingsModel.deeplink = g;
			}
			if (f !== undefined) {
				Athena.settingsModel.param = f;
			}
			if (e !== undefined) {
				Athena.settingsModel.param2 = e;
			}
			if (b !== undefined) {
				Athena.settingsModel.param3 = b;
			}
			if (a !== undefined) {
				Athena.settingsModel.param4 = a;
			}
			if (j) {
				if (l) {
					Athena.settingsModel.path = l + "_" + j + "_" + Athena.settingsModel.path;
				} else {
					Athena.settingsModel.path = j + "_" + Athena.settingsModel.path;
				}
			} else {
				if (l) {
					Athena.settingsModel.path = l + "_" + d + "_" + Athena.settingsModel.path;
				} else {
					Athena.settingsModel.path = d + "_" + Athena.settingsModel.path;
				}
			}
			if (l) {
				n = "dlo_types/" + l + "/" + d + "/" + c + "/" + h + "/";
				i = d + "_" + c + "_" + h;
			} else {
				n = "dlo_types/" + d + "/gr" + k + "/lessons_" + m + "/dlo_" + h + "/";
				i = k + "_" + m + "_" + h;
			}
			Athena.settingsModel.add({
				id : "dlo-path",
				value : n
			});
			Athena.dlolocaleModel.setName(i);
			Athena.settingsModel.loadData();
		}
	});
	Athena.CoreCollection = Backbone.Collection.extend({
		path : "",
		initialize : function() {},
		loader : function(a, c) {
			var b = this;
			$.ajax({
				type : "GET",
				url : a,
				dataType : "xml",
				success : function(d) {
					c.onResult(d);
				},
				error : function() {
					b.onFailed(a);
				}
			});
		},
		onFailed : function(a) {
			this.eventBus.trigger(Athena.ALERT_SHOW, "Could not find the XML file to load : " + a);
			this.eventBus.trigger(Athena.PRELOADER_HIDE);
		},
		dispatch : function(e, a, d, c, b) {
			this.eventBus.trigger(e, a, d, c, b);
		},
		register : function(c, b, a) {
			this.eventBus.on(c, b, a);
		}
	});
	Athena.UserCollection = Athena.CoreCollection.extend({
		initialize : function() {
			this.model.activity = [];
			this.eventBus.on(Athena.ACTIVITY_VISITED, this.logActivity, this);
		},
		logActivity : function(b) {
			var a = false;
			_.each(this.model.activity, function(c) {
				if (Number(c) === Number(b)) {
					a = true;
				}
			});
			if (!a) {
				this.model.activity.push(b);
			}
		},
		isAllVisited : function() {
			var a = false;
			if (this.model.activity.length >= Athena.dlolocaleModel.totalActivities) {
				a = true;
			}
			return a;
		},
		update : function(d, a, b) {
			var c = this.where({
				activity : d,
				name : a
			})[0];
			if (c !== undefined) {
				c.attributes.value = b;
			} else {
				this.add({
					activity : d,
					name : a,
					value : b
				});
			}
		},
		retrieve : function(d, a) {
			var b, c = this.where({
				activity : d,
				name : a
			})[0];
			b = (c !== undefined) ? c.attributes.value : "undefined";
			return b;
		}
	});
	Athena.ScormCollection = Athena.CoreCollection.extend({
		name : "scorm",
		cbtTesting : true,
		errorThrown : false,
		cbtStartTime : null,
		cbtScore : null,
		cbtsuspandData : null,
		cbtUserID : null,
		cbtUserName : null,
		cbtStatus : null,
		cbtLessonLocation : null,
		cnstLocation : "cmi.core.lesson_location",
		cnstSuspend : "cmi.suspend_data",
		cnstScore : "cmi.core.score.raw",
		cnstStatus : "cmi.core.lesson_status",
		cnstStuID : "cmi.core.student_id",
		cnstStuName : "cmi.core.student_name",
		cnstTime : "cmi.core.session_time",
		NoError : 0,
		GeneralException : 101,
		ServerBusy : 102,
		InvalidArgumentError : 201,
		ElementCannotHaveChildren : 202,
		ElementIsNotAnArray : 203,
		NotInitialized : 301,
		NotImplementedError : 401,
		InvalidSetValue : 402,
		ElementIsReadOnly : 403,
		ElementIsWriteOnly : 404,
		IncorrectDataType : 405,
		apiHandle : null,
		API : null,
		findAPITries : 0,
		initialize : function() {
			this.eventBus.on(Athena.SCORM_START, this.InitiateSCORM, this);
			this.eventBus.on(Athena.SCORM_FINISH, this.cbtScormFinish, this);
		},
		InitiateSCORM : function() {
			debug("Scrorm:: Start");
			this.cbtStartup();
		},
		cbtStartup : function() {
			debug("Dispatching Scorm CBT Startup Calls");
			var a = this.doLMSInitialize();
			if (a === "false") {
				this.errorHalt("001 API Connection");
			} else {
				if (a === "true") {
					this.cbtStartTimer();
					this.cbtStatus = this.cbtGetDataFromLMS(this.cnstStatus);
					this.cbtSetStatus();
				} else {
					this.errorHalt("002 API Connection");
				}
			}
		},
		doLMSInitialize : function() {
			var b, a, d, e, c;
			b = this.getAPIHandle();
			if (b === null || b === undefined) {
				c = "Unable to locate the LMS's API Implementation.\nLMSInitialize was not successful.";
				return false;
			}
			a = b.LMSInitialize("");
			if (a !== true) {
				d = this.ErrorHandler();
				e = b.LMSGetErrorString(d);
				c = d + " : " + e;
			}
			return a;
		},
		getAPIHandle : function() {
			if (this.apiHandle === null) {
				this.apiHandle = this.getAPI();
			}
			return this.apiHandle;
		},
		getAPI : function() {
			var a = this.findAPI(window.parent);
			if ((a === null) && (window.opener !== null) && (typeof (window.opener) !== undefined)) {
				a = this.findAPI(window.opener);
			}
			if (a === null) {
				alert("Unable to find an API adapter");
			}
			return a;
		},
		findAPI : function(a) {
			while ((a.API === undefined) && (a.parent !== null) && (a.parent !== a)) {
				this.findAPITries++;
				if (this.findAPITries > 7) {
					alert("Error finding API -- too deeply nested.");
					return null;
				}
			}
			return a.API;
		},
		ErrorHandler : function() {
			var b, a, c;
			b = this.getAPIHandle();
			if (b === null) {
				return;
			}
			a = b.LMSGetLastError().toString();
			if (a !== this.NoError) {
				c = b.LMSGetErrorString(a);
				if (Debug === true) {
					c += "\n";
					c += b.LMSGetDiagnostic(null);
				}
			}
			return a;
		},
		doLMSGetValue : function(b) {
			var c, d, a, e;
			d = "";
			c = this.getAPIHandle();
			if (c === null) {
				if (b === this.cnstLocation) {
					this.errGetBookmark = "Unable to locate the LMS's API Implementation.\nLMSGetValue was not successful.";
				} else {
					if (b === this.cnstSuspend) {
						this.errGetSuspendData = "Unable to locate the LMS's API Implementation.\nLMSGetValue was not successful.";
					} else {
						if (b === this.cnstScore) {
							this.errGetScore = "Unable to locate the LMS's API Implementation.\nLMSGetValue was not successful.";
						} else {
							if (b === this.cnstStatus) {
								this.errGetStatus = "Unable to locate the LMS's API Implementation.\nLMSGetValue was not successful.";
							}
						}
					}
				}
			} else {
				d = c.LMSGetValue(b);
				a = c.LMSGetLastError().toString();
				if (a !== this.NoError) {
					e = c.LMSGetErrorString(a);
					if (b === this.cnstLocation) {
						this.errGetBookmark = a + " : " + e;
					} else {
						if (b === this.cnstSuspend) {
							this.errGetSuspendData = a + " : " + e;
						} else {
							if (b === this.cnstScore) {
								this.errGetScore = a + " : " + e;
							} else {
								if (b === this.cnstStatus) {
									this.errGetStatus = a + " : " + e;
								}
							}
						}
					}
				}
				return d.toString();
			}
		},
		doLMSSetValue : function(b, e) {
			var c, a, d, f;
			c = this.getAPIHandle();
			if (c === null) {
				if (b === this.cnstLocation) {
					this.errSetBookmark = "Unable to locate the LMS's API Implementation.\nLMSSetValue was not successful.";
				} else {
					if (b === this.cnstSuspend) {
						this.errSetSuspendData = "Unable to locate the LMS's API Implementation.\nLMSSetValue was not successful.";
					} else {
						if (b === this.cnstScore) {
							this.errSetScore = "Unable to locate the LMS's API Implementation.\nLMSSetValue was not successful.";
						} else {
							if (b === this.cnstStatus) {
								this.errSetStatus = "Unable to locate the LMS's API Implementation.\nLMSSetValue was not successful.";
							}
						}
					}
				}
				return;
			}
			a = c.LMSSetValue(b, e);
			if (a.toString() !== "true") {
				d = this.ErrorHandler();
				f = c.LMSGetErrorString(d);
				if (b === this.cnstLocation) {
					this.errSetBookmark = d + " : " + f;
				} else {
					if (b === this.cnstSuspend) {
						this.errSetSuspendData = d + " : " + f;
					} else {
						if (b === this.cnstScore) {
							this.errSetScore = d + " : " + f;
						} else {
							if (b === this.cnstStatus) {
								this.errSetStatus = d + " : " + f;
							}
						}
					}
				}
			}
			return;
		},
		doLMSCommit : function() {
			var b, a, c, d;
			b = this.getAPIHandle();
			if (b === null) {
				this.errCommit = "Unable to locate the LMS's API Implementation.\nLMSCommit was not successful.";
				return "false";
			}
			a = b.LMSCommit("");
			if (a !== "true") {
				c = this.ErrorHandler();
				d = b.LMSGetErrorString(c);
				this.errCommit = c + " : " + d;
			}
			return a;
		},
		doLMSFinish : function() {
			var b, a, c, d;
			b = this.getAPIHandle();
			if (b === null) {
				this.errFinish = "Unable to locate the LMS's API Implementation.\nLMSFinish was not successful.";
				return "false";
			}
			b.LMSSetValue("cmi.core.exit", "");
			a = b.LMSFinish("");
			if (a.toString() !== "true") {
				c = this.ErrorHandler();
				d = b.LMSGetErrorString(c);
				this.errFinish = c + " : " + d;
			}
			return a;
		},
		cbtGetDataFromLMS : function(a) {
			var b = this.doLMSGetValue(a);
			return b;
		},
		cbtSetDataToLMS : function(a, b) {
			var c = this.doLMSSetValue(a, b);
			return c;
		},
		cbtSetStatus : function() {
			if (this.cbtStatus === "not attempted") {
				this.cbtSetDataToLMS(this.cnstStatus, "incomplete");
			}
		},
		cbtStartTimer : function() {
			this.cbtStartTime = new Date().getTime();
		},
		errorHalt : function(b) {
			var a;
			if (this.cbtTesting !== true) {
				if (this.errorThrown === false) {
					a = "LMS Error: There is an LMS Communication Error.\nUnable to continue.\n\nError Code: " + b;
					alert(a);
					this.errorThrown = true;
				}
			} else {
				if (this.errorThrown === false) {
					a = "LMS Error: There is an LMS Communication Error.\nUnable to continue.\n\nError Code: " + b;
					alert(a);
					this.errorThrown = true;
				}
			}
		},
		cbtStopTimer : function() {
			var a, c, b;
			if (this.cbtStartTime !== 0) {
				a = new Date().getTime();
				c = ((a - this.cbtStartTime) / 1000);
				b = this.cbtFormatTime(c);
			} else {
				b = "00:00:00.0";
			}
			this.cbtSetDataToLMS(this.cnstTime, b);
		},
		cbtFormatTime : function(c) {
			var h, d, b, f, a, i, g, e;
			h = (c % 60);
			c -= h;
			f = (c % 3600);
			c -= f;
			h = Math.round(h * 100) / 100;
			a = String(h);
			i = a;
			g = "";
			if (a.indexOf(".") !== -1) {
				i = a.substring(0, a.indexOf("."));
				g = a.substring(a.indexOf(".") + 1, a.length);
			}
			if (i.length < 2) {
				i = "0" + i;
			}
			a = i;
			if (g.length) {
				a = a + "." + g;
			}
			if ((c % 3600) !== 0) {
				b = 0;
			} else {
				b = (c / 3600);
			}
			if ((f % 60) !== 0) {
				d = 0;
			} else {
				d = (f / 60);
			}
			if ((String(b)).length < 2) {
				b = "0" + b;
			}
			if ((String(d)).length < 2) {
				d = "0" + d;
			}
			e = b + ":" + d + ":" + a;
			return e;
		},
		cbtScormFinish : function() {
			this.cbtStopTimer();
			var b, a = Athena.userModel.isAllVisited();
			if (a) {
				this.cbtSetDataToLMS(this.cnstStatus, "completed");
			}
			this.doLMSCommit();
			b = this.doLMSFinish();
			if (b === "false") {
				this.errorHalt("003 API Shut Down");
			}
		}
	});
	Athena.LocaleCollection = Athena.CoreCollection.extend({
		initialize : function() {},
		get : function(b) {
			var c, a = this.where({
				id : b
			})[0];
			c = (a !== undefined) ? a.attributes.value : "undefined";
			return c;
		},
		parse : function(b) {
			var a, c;
			a = this;
			c = b;
			$(c).find("locale").each(function(d) {
				$(this).find("shell").each(function(e) {
					$(this).find("lang").each(function() {
						a.add({
							id : $(this).attr("id"),
							value : $(this).text()
						});
					});
				});
			});
		}
	});
	Athena.LocaleActivityCollection = Athena.CoreCollection.extend({
		pathName : null,
		dloXmlLoaded : false,
		selectedActivity : -1,
		previousActivityId : -1,
		totalActivities : 0,
		selectedActivityStep : -1,
		totalActivitySteps : -1,
		loadItems : null,
		options : null,
		splash : null,
		shell : null,
		info : null,
		teacher : null,
		assets : [],
		subActivityRef : [],
		templates : {},
		data : {},
		initialize : function() {},
		setName : function(a) {
			this.pathName = a;
		},
		getActivity : function(b, d, c) {
			var a, e;
			a = this.where({
				id : Number(b),
				activityId : Number(d),
				type : c
			});
			e = (a[0] !== undefined) ? a[0].attributes.contents : null;
			return e;
		},
		getActivityIds : function() {
			var a = [];
			_.each(this.models, function(b) {
				a.push(b.attributes.activityId);
			});
			return a;
		},
		getData : function(d, g, e) {
			var c, h, b, a, f;
			c = this.where({
				id : Number(d),
				activityId : Number(g),
				type : Number(e)
			});
			if (c[0] !== undefined) {
				h = [];
				if (c[0].attributes.option !== undefined) {
					h.option = c[0].attributes.option;
				}
				if (c[0].attributes.contents !== undefined) {
					h.contents = c[0].attributes.contents;
				}
				if (c[0].attributes.lists !== undefined) {
					h.lists = c[0].attributes.lists;
				}
				if (c[0].attributes.questions !== undefined) {
					h.questions = c[0].attributes.questions;
				}
				if (c[0].attributes.answers !== undefined) {
					h.answers = c[0].attributes.answers;
				}
				if (c[0].attributes.interactivities !== undefined) {
					h.interactivities = c[0].attributes.interactivities;
				}
				if (Athena.settingsModel.get("parse-all-attributes")) {
					for (f in c[0].attributes) {
						if (c[0].attributes.hasOwnProperty(f)) {
							if (f !== "id" && f !== "type" && f !== "parent" && f !== "option" && f !== "contents" && f !== "lists" && f !== "questions" && f !== "answers"
									&& f !== "interactivities") {
								h[f] = c[0].attributes[f];
							}
						}
					}
				}
			} else {
				h = null;
			}
			return h;
		},
		getByCountId : function(b) {
			var a, c;
			a = this.where({
				id : Number(b)
			});
			c = (a !== undefined) ? a[0] : null;
			return c;
		},
		checkHasChildren : function(d) {
			var b, c, a = false;
			b = this.where({
				parentId : Number(d)
			});
			if (b.length > 0) {
				a = true;
			}
			return a;
		},
		checkIsChild : function(f) {
			var b, d, e, g, a = [], c = [];
			b = this.where({
				id : Number(f)
			});
			if (b.length > 0) {
				g = Number(b[0].attributes.parentId);
				a.push(g);
				if (g !== -1) {
					for (d = 0; d < this.models.length; d++) {
						if (this.models[d].attributes.parentId === g) {
							c.push(this.models[d].attributes.id);
						}
					}
				}
				a.push(c);
			}
			return a;
		},
		getAllParents : function() {
			var b, a = [];
			_.each(this.models, function(c) {
				if (c.attributes.parentId === -1) {
					a.push(c.attributes.id);
				}
			});
			return a;
		},
		getNextParent : function(e, f) {
			var c, b, d, a = this.getAllParents();
			for (c = 0; c < a.length; c++) {
				if (a[c] === f) {
					b = c;
				}
			}
			if (e) {
				d = (a[b + 1] !== undefined) ? a[b + 1] : -1;
			} else {
				d = (a[b - 1] !== undefined) ? a[b - 1] : -1;
			}
			return d;
		},
		setSelected : function(c) {
			var b, a;
			this.previousActivityId = this.selectedActivity;
			this.selectedActivity = c;
			a = this.checkIsChild(c);
			if (this.options.menuSubType !== undefined) {
				if (this.options.menuSubType === "1") {
					if (a[0] === -1) {
						b = this.checkHasChildren(this.selectedActivity);
						if (b) {
							this.eventBus.trigger(Athena.MENUSUB_SET, this.selectedActivity);
						} else {
							this.eventBus.trigger(Athena.MENUSUB_DESTROY);
						}
					} else {
						this.eventBus.trigger(Athena.MENU_CHANGE);
						if (a[1].length > 0) {
							if (a[1].indexOf(this.selectedActivity) === (a[1].length - 1)) {
								if (a[1].indexOf(this.previousActivityId) === -1) {
									this.eventBus.trigger(Athena.MENUSUB_SET, a[0]);
								}
							} else {
								this.eventBus.trigger(Athena.MENUSUB_CHANGE, this.selectedActivity);
							}
						}
					}
				}
			}
		},
		addAsset : function(a, c, b) {
			this.assets.push({
				name : a,
				path : c,
				type : b
			});
		},
		parse : function(c) {
			var d = $(c), i = this, h, f, e = 0, a, g;
			this.loadItems = [];
			this.options = {};
			this.splash = {};
			this.shell = {};
			this.info = {};
			this.teacher = {};
			a = Athena.settingsModel.get("image-path");
			g = Athena.settingsModel.getImagePath();
			d.find("locale").each(function(j) {
				d.find("dloxml").each(function(k) {
					var l = Athena.settingsModel.getXmlPath();
					$(this).find("xml").each(function(m) {
						h = $(this);
						i.loadItems.push({
							name : h.attr("name"),
							path : l + h.text(),
							type : "xml"
						});
					});
				});
				d.find("markup").each(function(k) {
					var l = Athena.settingsModel.get("template-path");
					$(this).find("html").each(function(m) {
						h = $(this);
						i.loadItems.push({
							name : h.attr("name"),
							path : l + h.text(),
							type : "html"
						});
					});
				});
				d.find("dlooptions").each(function(k) {
					$(this).find("option").each(function(l) {
						h = $(this);
						f = h.text();
						if (f === "Y") {
							f = true;
						} else {
							if (f === "N") {
								f = false;
							}
						}
						i.options[h.attr("name")] = f;
					});
				});
				d.find("splash").each(function(k) {
					$(this).find("content").each(function(l) {
						h = $(this);
						f = h.text();
						i.splash[h.attr("name")] = f;
						if (h.attr("type") === "image") {
							i.addAsset(h.attr("name"), a + f, h.attr("type"));
						}
					});
				});
				d.find("shell").each(function(k) {
					$(this).find("content").each(function(l) {
						h = $(this);
						f = h.text();
						i.shell[h.attr("name")] = f;
						if (h.attr("type") === "image") {
							i.addAsset(h.attr("name"), a + f, h.attr("type"));
						}
					});
				});
				d.find("teacher").each(function(k) {
					$(this).find("content").each(function(l) {
						h = $(this);
						f = i.checkImageContent(h.text());
						if (h.attr("colour") !== undefined) {
							i.teacher[h.attr("name")] = {
								value : f,
								colour : h.attr("colour")
							};
						} else {
							i.teacher[h.attr("name")] = f;
						}
						if (h.attr("type") === "image") {
							i.addAsset(h.attr("name"), a + f, h.attr("type"));
						}
					});
				});
				d.find("info").each(function(k) {
					$(this).find("content").each(function(l) {
						h = $(this);
						f = h.text();
						i.info[h.attr("name")] = f;
						if (h.attr("type") === "image") {
							i.addAsset(h.attr("name"), a + f, h.attr("type"));
						}
					});
				});
				d.find("activity").each(function(k) {
					i.parseActivity($(this), e, g);
					e++;
				});
			});
			this.totalActivities = this.models.length;
			try {
				if (ATHENA_HIDEUI !== undefined) {
					Athena.HIDE_UI = ATHENA_HIDEUI;
				}
				if (ATHENA_HIDESPLASH !== undefined) {
					Athena.HIDE_SPLASH = ATHENA_HIDESPLASH;
				}
			} catch (b) {}
		},
		parseActivity : function(c, d, o) {
			var n = this, b = c.attr("id"), l = c.attr("type"), g = c.attr("parent"), p = {}, m, i, h, f, e, k, a, j;
			c.find("options").each(function(q) {
				m = $(this);
				m.find("option").each(function(r) {
					m = $(this);
					i = m.text();
					p[m.attr("name")] = {
						value : i
					};
					$.each(this.attributes, function(t, v) {
						var s = v.name, u = v.value;
						p[m.attr("name")][s] = u;
					});
				});
			});
			f = this.parseContentTypes(c.find("contents"), o);
			e = {};
			e.id = Number(d);
			e.activityId = Number(b);
			e.type = Number(l);
			e.parentId = Number(g);
			if (Athena.settingsModel.get("parse-all-attributes")) {
				for (k in c[0].attributes) {
					if (c[0].attributes.hasOwnProperty(k)) {
						if (c[0].attributes[Number(k)] !== undefined) {
							a = String(c[0].attributes[Number(k)].name);
							j = c[0].attributes[Number(k)].value;
							if (a !== "id" && a !== "type" && a !== "parent") {
								e[a] = j;
							}
						}
					}
				}
			}
			if (p !== undefined) {
				e.option = p;
			}
			for (m in f) {
				if (f.hasOwnProperty(m)) {
					e[m] = f[m];
				}
			}
			this.add(e);
		},
		parseInteractivity : function(a, b) {},
		parseContentTypes : function(e, l, h) {
			var k = this, j, g, c, i, d, f, a, b;
			e.children().each(function(m) {
				j = $(this);
				g = k.checkImageContent(j.text());
				if (this.tagName === "content") {
					if (j.attr("type") === "text") {
						if (c === undefined) {
							c = {};
						}
						if (Athena.settingsModel.get("parse-all-attributes")) {
							if (this.attributes.length >= 1) {
								c[j.attr("name")] = {
									value : g
								};
								$.each(this.attributes, function(o, q) {
									var n = q.name, p = q.value;
									c[j.attr("name")][n] = p;
								});
							} else {
								c[j.attr("name")] = g;
							}
						} else {
							if (j.attr("selector") !== undefined) {
								c[j.attr("name")] = {
									value : g,
									selector : j.attr("selector"),
									type : "text"
								};
							} else {
								c[j.attr("name")] = g;
							}
						}
					} else {
						if (j.attr("type") === "audio" || j.attr("type") === "video") {
							if (c === undefined) {
								c = {};
							}
							c[j.attr("name")] = j.text();
						} else {
							if (j.attr("type") === "image") {
								if (c === undefined) {
									c = {};
								}
								if (j.attr("selector") !== undefined) {
									c[j.attr("name")] = {
										value : j.text(),
										selector : j.attr("selector"),
										type : "image"
									};
								} else {
									c[j.attr("name")] = j.text();
								}
								if (j.attr("type") === "image") {
									k.addAsset(j.attr("name"), l + j.text(), j.attr("type"));
								}
							} else {
								if ($(this).attr("type") === "options") {
									if (i === undefined) {
										i = {};
									}
									i[$(this).attr("name")] = k.parseOptions($(this), l);
								} else {
									if ($(this).attr("type") === "questions") {
										if (d === undefined) {
											d = {};
										}
										d[$(this).attr("name")] = k.parseQuestions($(this), l);
									} else {
										if ($(this).attr("type") === "answers") {
											if (f === undefined) {
												f = {};
											}
											f = k.parseAnswers($(this), l);
										} else {
											if ($(this).attr("type") === "interactivity") {
												if (a === undefined) {
													a = {};
												}
												a[$(this).attr("name")] = k.parseContentTypes($(this), l, "interactivity");
											}
										}
									}
								}
							}
						}
					}
				}
			});
			b = {};
			if (h === "interactivity") {
				b.objectId = e.attr("objectId");
				b.options = {};
				e.find("options").each(function(m) {
					$(this).find("option").each(function(n) {
						b.options[$(this).attr("name")] = $(this).text();
					});
				});
			}
			if (c !== undefined) {
				b.contents = c;
			}
			if (i !== undefined) {
				b.lists = i;
			}
			if (d !== undefined) {
				b.questions = d;
			}
			if (a !== undefined) {
				b.interactivities = a;
			}
			if (f !== undefined) {
				b.answers = f;
			}
			return b;
		},
		parseOptions : function(c, d) {
			var a = this, b = [];
			c.find("option").each(function(f) {
				var g = $(this), e = {};
				g.find("data").each(function(i) {
					var h = $(this), j = a.checkImageContent(h.text());
					e[h.attr("name")] = {
						value : j
					};
					if (Athena.settingsModel.get("parse-all-attributes")) {
						$.each(this.attributes, function(l, n) {
							var k = n.name, m = n.value;
							e[h.attr("name")][k] = m;
						});
					}
					if (h.attr("type") !== undefined) {
						if (h.attr("type") === "image") {
							if (j !== "") {
								a.addAsset(g.attr("name"), d + h.text(), h.attr("type"));
							}
						}
					}
				});
				b.push(e);
			});
			return b;
		},
		checkImageContent : function(b) {
			var c = Athena.settingsModel.getImagePath(), a = b.split("#IMAGEPATH#").join(c);
			return a;
		},
		parseQuestions : function(c, d) {
			var b = this, a = [];
			c.find("question").each(function(f) {
				var e = $(this), g = {};
				e.find("data").each(function(i) {
					var j = $(this), h = b.checkImageContent(j.text());
					g[j.attr("name")] = {
						value : h
					};
					if (Athena.settingsModel.get("parse-all-attributes")) {
						$.each(this.attributes, function(l, n) {
							var k = n.name, m = n.value;
							g[j.attr("name")][k] = m;
						});
					}
					if (j.attr("type") !== undefined) {
						if (j.attr("type") === "image" && j.text() !== "" && j.text() !== " ") {
							b.addAsset(j.attr("name"), d + j.text(), j.attr("type"));
						}
					}
				});
				a.push(g);
			});
			return a;
		},
		parseAnswers : function(a) {
			var b = [];
			a.find("data").each(function(c) {
				b.push({
					name : $(this).attr("name"),
					value : $(this).text()
				});
			});
			return b;
		}
	});
	Athena.ActivityCollection = Athena.CoreCollection
			.extend({
				initialize : function() {},
				getOption : function(a, b, g) {
					var f, e, c, d;
					f = this.where({
						activityId : Number(a)
					});
					if (f[0] !== undefined) {
						e = f[0].attributes.option;
						c = _.find(e, function(h) {
							return h.name === b;
						});
						if (g === undefined) {
							d = c.value;
						} else {
							d = c[g];
						}
					} else {
						d = null;
					}
					return d;
				},
				classes : function(e) {
					var i = [], a = Athena.settingsModel.get("activity-path"), c = Athena.settingsModel.get("plugin-path"), j = Athena.settingsModel.get("libs-path"), b = Athena.settingsModel
							.get("css-path"), g = Athena.settingsModel.get("js-path"), k, d, f, h;
					if (Athena.LOAD_MODE === "DLO") {
						_.each(this.models, function(l) {
							_.each(e, function(m) {
								if (l.attributes.activityId === m) {
									_.each(l.attributes.plugins, function(n) {
										i.push({
											name : n.name,
											ref : n.ref,
											value : c + n.value,
											type : "plugin"
										});
									});
									_.each(l.attributes.libs, function(n) {
										i.push({
											name : n.name,
											value : j + n.value,
											type : "lib"
										});
									});
									_.each(l.attributes.option, function(n) {
										if (n.name === "component") {
											i.push({
												name : n.name,
												value : a + n.value,
												type : "activity"
											});
										} else {
											if (n.name === "component-css") {
												i.push({
													name : n.name,
													value : b + n.value,
													type : "css"
												});
											}
										}
									});
								}
							});
						});
					} else {
						if (Athena.LOAD_MODE === "LIVE") {
							_.each(this.models, function(l) {
								_.each(e, function(m) {
									if (l.attributes.activityId === m) {
										_.each(l.attributes.libs, function(n) {
											i.push({
												name : n.name,
												value : j + n.value,
												type : "lib"
											});
										});
									}
								});
							});
							k = Athena.settingsModel.get("plugin-file-build");
							d = Athena.settingsModel.get("activity-file-build");
							f = Athena.settingsModel.get("activity-css-build");
							h = Athena.settingsModel.get("athena-extend-build");
							if (k !== "undefined") {
								i.push({
									name : "plugin-file-build",
									value : g + k,
									type : "plugin"
								});
							}
							if (d !== "undefined") {
								i.push({
									name : "activity-file-build",
									value : g + d,
									type : "activity"
								});
							}
							if (h !== "undefined") {
								i.push({
									name : "activity-extend-build",
									value : g + h,
									type : "plugin"
								});
							}
							if (f !== "undefined") {
								i.push({
									name : "activity-css-build",
									value : b + f,
									type : "css"
								});
							}
						}
					}
					return Athena.arrayMethods.cleanDuplicates(i, "value");
				},
				initClasses : function(aIds) {
					var ClassName, classArray = [];
					_.each(this.models, function(items) {
						_.each(aIds, function(activity) {
							if (items.attributes.activityId === activity) {
								_.each(items.attributes.plugins, function(classItem) {
									if (classItem.name !== undefined) {
										classArray.push({
											name : classItem.name,
											ref : classItem.ref
										});
									}
								});
							}
						});
					});
					classArray = Athena.arrayMethods.cleanDuplicates(classArray, "ref");
					_.each(classArray, function(classItem) {
						if (classItem.name !== undefined) {
							try {
								ClassName = eval(classItem.ref);
								Athena[classItem.name] = new ClassName();
								debug("Class initialised : " + classItem.name + " : " + classItem.ref);
							} catch (error) {
								debug("Error: The class could not be initialised : " + classItem.name + " : " + error);
							}
						}
					});
				},
				assets : function(b) {
					var a = [];
					_.each(this.models, function(c) {
						_.each(b, function(d) {
							if (c.attributes.activityId === d) {
								_.each(c.attributes.assets, function(e) {
									a.push(e);
								});
							}
						});
					});
					return Athena.arrayMethods.cleanDuplicates(a, "path");
				},
				parse : function(e) {
					var d = this, f, g, b, c, a = Athena.settingsModel.getDefaultImagePath();
					$(e).find("activities").each(function(h) {
						$(this).find("activity").each(function() {
							f = [];
							g = [];
							b = [];
							c = [];
							$(this).find("options").each(function() {
								$(this).find("option").each(function() {
									if ($(this).attr("ref") !== undefined) {
										f.push({
											name : $(this).attr("id"),
											ref : $(this).attr("ref"),
											value : $(this).text()
										});
									} else {
										f.push({
											name : $(this).attr("id"),
											value : $(this).text()
										});
									}
								});
							});
							$(this).find("plugins").each(function() {
								$(this).find("plugin").each(function() {
									if ($(this).text() !== "") {
										g.push({
											name : $(this).attr("id"),
											ref : $(this).attr("ref"),
											value : $(this).text()
										});
									}
								});
							});
							$(this).find("libs").each(function() {
								$(this).find("lib").each(function() {
									if ($(this).text() !== "") {
										b.push({
											name : $(this).attr("id"),
											ref : $(this).attr("ref"),
											value : $(this).text()
										});
									}
								});
							});
							$(this).find("assets").each(function() {
								$(this).find("asset").each(function() {
									if ($(this).text() !== "") {
										c.push({
											name : "activity",
											path : a + $(this).text(),
											type : $(this).attr("type")
										});
									}
								});
							});
							d.add({
								activityId : Number($(this).attr("id")),
								option : f,
								plugins : g,
								libs : b,
								assets : c
							});
						});
					});
				}
			});
	Athena.SettingsCollection = Athena.CoreCollection.extend({
		host : null,
		hostName : null,
		edition : null,
		prefix : null,
		project : null,
		discipline : null,
		grade : null,
		lesson : null,
		category : null,
		dlo : null,
		deeplink : null,
		param : null,
		param2 : null,
		param3 : null,
		param4 : null,
		templates : {},
		theme : "",
		locale : "",
		region : "",
		initialize : function() {
			this.setPath();
			this.host = window.location.host;
			this.hostName = window.location.hostname;
		},
		restore : function() {
			this.setPath();
			this.remove();
		},
		setPath : function() {
			this.path = "global_settings.xml";
		},
		loadData : function() {
			this.loader("xml/" + this.path, this);
		},
		saveDefaultActivity : function(a) {
			this.add({
				id : "DEFAULT_ACTIVITY",
				value : Number(a)
			});
		},
		addTemplate : function(a, b) {
			this.templates[a] = b;
		},
		getPreloadItems : function(b) {
			var a;
			if (b) {
				a = this.where({
					id : "preload-assets"
				})[0].attributes.value;
			} else {
				a = this.where({
					id : "preload-items"
				})[0].attributes.value;
			}
			return a;
		},
		addPreloadItems : function(c, e, d, b) {
			var a;
			if (b) {
				a = this.where({
					id : "preload-assets"
				})[0].attributes.value;
			} else {
				a = this.where({
					id : "preload-items"
				})[0].attributes.value;
			}
			return a;
		},
		randomTheme : function() {
			var c, a = this.where({
				id : "themes"
			})[0].attributes.value, b = this.where({
				id : "randomise-theme"
			})[0].attributes.value;
			if (a !== undefined) {
				if (b !== undefined && b) {
					a = a.split(",");
					c = Math.floor(Math.random() * a.length);
					this.theme = a[c];
					$("#wrapper").attr("class", "").addClass(this.theme);
				}
			} else {
				this.theme = "default-theme";
			}
		},
		getDefaultImagePath : function() {
			var a;
			a = this.where({
				id : "image-path"
			})[0].attributes.value;
			return a;
		},
		getXmlPath : function() {
			var a;
			if (Athena.LOAD_MODE === "DLO") {
				a = this.where({
					id : "dlo-path"
				})[0].attributes.value + "xml/";
			} else {
				if (Athena.LOAD_MODE === "LIVE") {
					a = this.where({
						id : "xml-path"
					})[0].attributes.value;
				}
			}
			return a;
		},
		getImagePath : function() {
			var a;
			if (Athena.LOAD_MODE === "DLO") {
				a = this.where({
					id : "dlo-path"
				})[0].attributes.value + "assets/images/";
			} else {
				if (Athena.LOAD_MODE === "LIVE") {
					a = this.where({
						id : "image-path"
					})[0].attributes.value;
				}
			}
			return a;
		},
		getAudioPath : function() {
			var a;
			if (Athena.LOAD_MODE === "DLO") {
				a = this.where({
					id : "dlo-path"
				})[0].attributes.value + "assets/audio/";
			} else {
				if (Athena.LOAD_MODE === "LIVE") {
					a = this.where({
						id : "audio-path"
					})[0].attributes.value;
				}
			}
			return a;
		},
		getVideoPath : function() {
			var a;
			if (Athena.LOAD_MODE === "DLO") {
				a = this.where({
					id : "dlo-path"
				})[0].attributes.value + "assets/video/";
			} else {
				if (Athena.LOAD_MODE === "LIVE") {
					a = this.where({
						id : "video-path"
					})[0].attributes.value;
				}
			}
			return a;
		},
		get : function(c) {
			var b, a = this.where({
				id : c
			})[0];
			b = (a !== undefined) ? a.attributes.value : "undefined";
			return b;
		},
		set : function(c, b) {
			var a = this.where({
				id : c
			})[0];
			if (a !== undefined) {
				a.attributes.value = b;
			}
		},
		onResult : function(d) {
			var j, k, h, n, c = "", g, f, b, m, e, o, a, l, i;
			e = d;
			o = this;
			$(e).find("settings").each(function(p) {
				$(this).find("setting").each(function() {
					var r = $(this).attr("id"), q = $(this).text();
					if (q === "true") {
						q = true;
					} else {
						if (q === "false") {
							q = false;
						}
					}
					o.add({
						id : r,
						value : q
					});
					if (r === "debug") {
						Athena.APP_MODE_DEBUG = q;
					}
					if (r === "scorm-active") {
						Athena.SCORM_ACTIVE = q;
					}
					if (r === "use-jplayer-audio") {
						Athena.USE_JPLAYER_AUDIO = q;
					}
					if (r === "use-jplayer-video") {
						Athena.USE_JPLAYER_VIDEO = q;
					}
					if (r === "complex-transitions") {
						Athena.USE_COMPLEX_TRANSITIONS = q;
					}
					if (r === "tweener-type") {
						if (Athena.browserDetails[0].Browser === "Android Webkit Browser") {
							Athena.TWEENER_TYPE = "Jquery";
						} else {
							Athena.TWEENER_TYPE = q;
						}
					}
					if (r === "xml-path") {
						j = q;
					}
					if (r === "image-path") {
						k = q;
					}
					if (r === "image-shell-path") {
						h = q;
					}
					if (r === "template-path") {
						n = q;
					}
					if (r === "locale") {
						if (o.locale === "") {
							o.locale = q;
						}
					}
					if (r === "region") {
						if (o.region === "") {
							o.region = q;
						}
					}
				});
			});
			this.randomTheme();
			m = this.getXmlPath();
			if (this.region !== "") {
				i = this.locale + "_" + this.region;
			} else {
				if (this.locale === "") {
					this.locale = "en-US";
				}
				i = this.locale;
			}
			if (Athena.LOAD_MODE === "LIVE") {
				b = "dlo_activity_" + i + ".xml";
			} else {
				if (Athena.LOAD_MODE === "DLO") {
					if (this.project) {
						c += this.project + "_";
					}
					if (this.prefix) {
						c += this.prefix + "_";
					}
					if (this.discipline) {
						c += this.discipline + "_";
					}
					b = "dlo_" + Athena.dlolocaleModel.pathName + "_activity_" + i + ".xml";
				}
			}
			a = [];
			l = [];
			a.push({
				name : "dlo-xml",
				path : m + b,
				type : "xml"
			});
			$(e).find("preloader").each(function(p) {
				$(e).find("preload").each(function(s) {
					var q, t = $(this).attr("type"), u = $(this).text(), r = $(this).attr("name");
					if ($(this).attr("type") === "image") {
						if ($(this).attr("theme") === undefined) {
							l.push({
								name : "shell",
								path : h + u,
								type : t
							});
						}
						if ($(this).attr("theme") === o.theme) {
							l.push({
								name : "shell",
								path : h + u,
								type : t
							});
						}
					} else {
						if (t === "html") {
							q = n;
						} else {
							if (t === "xml") {
								q = j;
							}
						}
						if (r === "locale-xml") {
							g = u;
							g = g.split(".");
							g = c + g[0] + "_" + i + "." + g[1];
							u = g;
						} else {
							if (r === "activity-xml") {
								f = u;
								f = f.split(".");
								f = c + f[0] + "." + f[1];
								u = f;
							}
						}
						a.push({
							name : r,
							path : q + u,
							type : t
						});
					}
				});
			});
			o.add({
				id : "preload-items",
				value : a
			});
			o.add({
				id : "preload-assets",
				value : l
			});
			debug(this.pluck("id"), this.pluck("value"));
			this.eventBus.trigger(Athena.SETTINGS_PRELOAD_COMPLETE);
		}
	});
	Athena.CoreController = BackboneMVC.Controller.extend({
		name : "corecontroller",
		dispatch : function(e, a, d, c, b) {
			Athena.eventBus.trigger(e, a, d, c, b);
		},
		register : function(c, b, a) {
			Athena.eventBus.on(c, b, a);
		}
	});
	Athena.FullscreenController = Athena.CoreController.extend({
		name : "fullscreen",
		initialize : function() {
			this.register(Athena.FULLSCREEN_TOGGLE, this.toggle, this);
		},
		toggle : function() {
			var a, b = document.body;
			a = (document.fullScreenElement && document.fullScreenElement !== null) || (document.mozFullScreen || document.webkitIsFullScreen);
			if (a) {
				Athena.fullscreenController.cancel(document);
			} else {
				Athena.fullscreenController.request(b);
			}
			return false;
		},
		cancel : function(a) {
			var d, c = a.cancelFullScreen || a.webkitCancelFullScreen || a.mozCancelFullScreen || a.exitFullscreen;
			if (c) {
				c.call(a);
			} else {
				try {
					if (typeof window.ActiveXObject !== "undefined" && typeof window.ActiveXObject !== undefined) {
						d = new ActiveXObject("WScript.Shell");
						if (d !== null) {
							d.SendKeys("{F11}");
						}
					}
				} catch (b) {
					this.dispatch(Athena.ALERT_SHOW, "ActiveX Objects are not enabled, press F11 on your keyboard for Fullscreen mode.");
				}
			}
		},
		request : function(a) {
			var d, c = a.requestFullScreen || a.webkitRequestFullScreen || a.mozRequestFullScreen || a.msRequestFullScreen;
			if (c) {
				if (c === a.webkitRequestFullScreen) {
					a.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
					if (!document.webkitCurrentFullScreenElement) {
						c.call(a);
						this.dispatch(Athena.ALERT_SHOW, "Warning: This Browser has known bugs that do not allow text input in fullscreen mode.");
					}
				} else {
					c.call(a);
				}
			} else {
				try {
					if (typeof window.ActiveXObject !== "undefined" && typeof window.ActiveXObject !== undefined) {
						d = new ActiveXObject("WScript.Shell");
						if (d !== null) {
							d.SendKeys("{F11}");
						}
					}
				} catch (b) {
					this.dispatch(Athena.ALERT_SHOW, "ActiveX Objects are not enabled, press F11 on your keyboard for Fullscreen mode.");
				}
			}
			return false;
		}
	});
	Athena.PreloaderController = Athena.CoreController.extend({
		name : "preloader",
		completeCount : 0,
		completePercent : 0,
		totalItems : 0,
		array : null,
		message : "",
		completeEvnt : null,
		updateEvnt : null,
		data : null,
		initialize : function() {
			this.register(Athena.PRELOAD_START, this.preload, this);
		},
		preload : function(d, c, b, a) {
			this.completeCount = 0;
			this.completePercent = 0;
			this.totalItems = d.length;
			this.array = d;
			this.data = null;
			this.message = (c !== undefined) ? c : null;
			this.updateEvnt = (b !== undefined) ? b : null;
			this.completeEvnt = (a !== undefined) ? a : null;
			this.start();
		},
		start : function() {
			var a = this.array[this.completeCount];
			if (a !== undefined) {
				if (a.type === "image") {
					this.loadImage(a.name, a.path, a.type);
				} else {
					this.load(a.name, a.path, a.type);
				}
			} else {
				if (this.completeEvnt !== undefined && this.completeEvnt) {
					this.dispatch(this.completeEvnt);
				}
			}
		},
		complete : function(b, c, a, d) {
			this.completeCount++;
			this.completePercent = (this.completeCount / this.totalItems) * 100;
			if (this.updateEvnt && this.updateEvnt !== undefined) {
				this.dispatch(this.updateEvnt, this.message, this.completePercent);
			}
			if (c !== "image") {
				if (!a) {
					if (!this.data) {
						this.data = {};
					}
					this.data[b] = {
						type : c,
						data : d
					};
				}
			}
			if (this.completeCount < this.totalItems) {
				this.start();
			} else {
				if (this.completeEvnt !== undefined && this.completeEvnt) {
					this.dispatch(this.completeEvnt, this.data);
				}
			}
		},
		load : function(b, d, c) {
			var a = this;
			$.ajax({
				type : "GET",
				url : d,
				dataType : c,
				success : function(e) {
					a.complete(b, c, false, e);
				},
				error : function() {
					a.complete(b, c, true);
				}
			});
		},
		loadImage : function(b, e, c) {
			var a = this, d = new Image();
			d.src = e;
			d.onload = function() {
				a.complete(b, c, false);
			};
			d.onerror = function() {
				a.complete(b, c, true);
			};
		},
		reset : function() {
			this.completeCount = 0;
			this.completePercent = 0;
			this.totalItems = 0;
			this.array = null;
			this.message = "";
			this.evnt = null;
			this.data = null;
		}
	});
	Athena.ApiController = Athena.CoreController.extend({
		name : "api",
		initialize : function() {
			this.register(Athena.API_EXECUTE, this.execute, this);
		},
		execute : function(b) {
			var a = this, d = Athena.settingsModel.getSetting(b.api), c = Athena.settingsModel.getSetting(b.api + "-format");
			switch (b.type) {
			case "post":
				this.post(d, b, c);
				break;
			case "get":
				break;
			case "ajax":
				this.ajax(d, b.type, b, c);
				break;
			}
			debug("Dispatching to the API : " + d, b);
		},
		post : function(c, b) {
			var a = this;
			$.post(c, b, function(d) {
				a.result(d);
			}, function(d) {
				a.result(d);
				a.result(d);
			});
		},
		get : function() {},
		ajax : function(e, b, c, d) {
			var a = this;
			$.ajax({
				type : "GET",
				url : e,
				cache : "false",
				dataType : d,
				data : (d === "xml") ? {
					request : c.request
				} : c,
				success : function(f) {
					if (f) {
						a.result(f);
					} else {
						a.result(f);
					}
				},
				error : function(f) {
					a.result(f);
				}
			});
		},
		dataType : function(a) {
			return JSON.stringify(a);
		},
		result : function(a) {
			if (a) {
				debug(a);
			}
		}
	});
	Athena.AudioController = Athena.CoreController.extend({
		name : "audio",
		audioMuted : false,
		audioVolume : 1,
		audioElement : null,
		isStopped : false,
		isPaused : false,
		sendTime : false,
		audioName : "",
		initialize : function() {
			this.register(Athena.AUDIO_INIT, this.activate, this);
			this.register(Athena.AUDIO_PLAY, this.play, this);
			this.register(Athena.AUDIO_STOP, this.stopAudio, this);
			this.register(Athena.AUDIO_PAUSE, this.pause, this);
			this.register(Athena.AUDIO_MUTE, this.mute, this);
			this.register(Athena.AUDIO_VOLUME, this.setVolume, this);
		},
		activate : function() {
			var a = this;
			debug("Audio: The default audio player has been initialised");
			if (Athena.USE_JPLAYER_AUDIO) {
				this.audioElement = $("#shell-jplayer-audio");
				this.audioElement.jPlayer({
					ready : function(b) {
						debug("Audio: Jplayer audio is ready");
						a.audioElement.bind($.jPlayer.event.ended + ".repeat", function() {
							if (!a.isStopped) {
								a.dispatch(Athena.AUDIO_COMPLETE, a.audioName, Number(Athena.dlolocaleModel.selectedActivity));
							}
						});
						a.audioElement.bind($.jPlayer.event.timeupdate, function(c) {
							if (a.sendTime) {
								a.dispatch(Athena.AUDIO_TIMEUPDATE, Number(Athena.dlolocaleModel.selectedActivity), c);
							}
						});
					},
					swfPath : "libs/jquery",
					supplied : "mp3,oga",
					wmode : "window"
				});
			} else {
				this.audioElement = $("#shell-audio");
				if (this.audioElement) {
					this.audioElement.bind("ended", function() {
						if (!a.isStopped) {
							a.dispatch(Athena.AUDIO_COMPLETE, a.audioName, Number(Athena.dlolocaleModel.selectedActivity));
						}
					});
				}
			}
		},
		play : function(c, a) {
			var d, g, f, e, b = this;
			debug("PlayEvent: Audio", c);
			if (Athena.USE_JPLAYER_AUDIO) {
				this.isStopped = false;
				if (c !== null || c !== undefined) {
					g = c.split(".");
					f = g[0];
				}
				b.audioName = f;
				e = {
					title : g[0],
					mp3 : Athena.settingsModel.getAudioPath() + g[0] + ".mp3",
					oga : Athena.settingsModel.getAudioPath() + g[0] + ".ogg"
				};
				if (this.audioElement) {
					if (a !== undefined && a === true) {
						b.sendTime = true;
					} else {
						b.sendTime = false;
					}
					this.audioElement.jPlayer("clearMedia");
					this.audioElement.jPlayer("setMedia", e).jPlayer("play");
					this.audioElement.jPlayer("option", "volume", (b.audioMuted) ? 0 : b.audioVolume);
				}
			} else {
				if (Athena.browserDetails[0].Browser === "Firefox" || Athena.browserDetails[0].Browser === "Opera") {
					d = ".ogg";
				} else {
					d = ".mp3";
				}
				if (Athena.browserDetails[0].Browser === "Internet Explorer" && Number(Athena.browserDetails[0].Version) < 9) {
					d = ".mp3";
				}
				if (Athena.browserDetails[0].Browser === "Internet Explorer" && Number(Athena.browserDetails[0].Version) < 9) {
					c = null;
				} else {
					if (c !== null || c !== undefined) {
						g = c.split(".");
						f = g[0];
						this.audioElement.attr("src", Athena.settingsModel.getAudioPath() + f + d);
						this.audioElement[0].load();
					}
					if (this.audioElement.attr("src") !== null || this.audioElement.attr("src") !== undefined) {
						this.audioElement[0].play();
						this.audioElement[0].volume = (this.audioMuted) ? 0 : this.audioVolume;
					}
				}
			}
		},
		pause : function() {
			this.isPaused = true;
			if (Athena.USE_JPLAYER_AUDIO) {
				this.audioElement.jPlayer("pause", 0);
			} else {
				if (Athena.browserDetails[0].Browser === "Internet Explorer" && Number(Athena.browserDetails[0].Version) < 9) {
					this.isPaused = true;
				} else {
					if (this.audioElement[0] !== undefined) {
						this.audioElement[0].pause();
					}
				}
			}
		},
		stopAudio : function() {
			if (this.audioElement) {
				this.wasStopped = true;
				if (Athena.USE_JPLAYER_AUDIO) {
					this.audioElement.jPlayer("stop");
				} else {
					if (Athena.browserDetails[0].Browser === "Internet Explorer" && Number(Athena.browserDetails[0].Version) < 9) {
						this.wasStopped = true;
					} else {
						if (this.audioElement !== undefined) {
							this.audioElement[0].pause();
						}
					}
				}
			}
		},
		mute : function(a) {
			if (this.audioElement) {
				if (Athena.USE_JPLAYER_AUDIO) {
					if (a === "on") {
						this.audioMuted = true;
						this.audioVolume = this.audioElement.jPlayer("option", "volume");
						this.audioElement.jPlayer("option", "volume", 0);
					} else {
						if (a === "off") {
							this.audioMuted = false;
							this.audioElement.jPlayer("option", "volume", this.audioVolume);
						}
					}
				} else {
					if (a === "on") {
						this.audioMuted = true;
						if (this.audioElement[0] !== undefined) {
							this.audioVolume = this.audioElement[0].volume;
							this.audioElement[0].volume = 0;
						}
					} else {
						if (a === "off") {
							this.audioMuted = false;
							if (this.audioElement[0] !== undefined) {
								this.audioElement[0].volume = this.audioVolume;
							}
						}
					}
				}
			}
		},
		setVolume : function(b) {
			var a = Math.round(b * Math.pow(10, 2)) / Math.pow(10, 2);
			this.audioVolume = a;
			if (this.audioElement) {
				if (Athena.USE_JPLAYER_AUDIO) {
					if (!this.audioMuted) {
						this.audioElement.jPlayer("option", "volume", this.audioVolume);
					}
				} else {
					if (!this.audioMuted) {
						this.audioElement[0].volume = this.audioVolume;
					}
				}
			}
		}
	});
	Athena.InitialiseController = Athena.CoreController
			.extend({
				name : "initialise",
				steps : 5,
				step : 1,
				initialize : function() {
					this.register(Athena.INIT_PRELOAD_UPDATE, this.updatePreload, this);
					this.register(Athena.SETTINGS_PRELOAD_COMPLETE, this.settingsData, this);
					this.register(Athena.INIT_PRELOAD_COMPLETE, this.storeInitData, this);
					this.register(Athena.DLO_PRELOAD_COMPLETE, this.storeDLOData, this);
					this.register(Athena.ASSET_PRELOAD_COMPLETE, this.assetsLoaded, this);
				},
				updatePreload : function(b, a) {
					var c = Math.ceil(((a / 100) * (100 / 5)) + (this.step * (100 / 5)));
					this.dispatch(Athena.PRELOADER_UPDATE, b, c);
				},
				settingsData : function(d) {
					this.updatePreload(Athena.settingsModel.get("preloader-loading"), 100);
					this.step = 2;
					var b = Athena.settingsModel.get("preload-shell-assets"), a = Athena.settingsModel.get("preload-activity-assets"), c = Athena.settingsModel
							.get("preload-content-assets"), e = Athena.settingsModel.get("preload-items");
					Athena.preloadController.preload(e, Athena.settingsModel.get("preloader-xml"), Athena.INIT_PRELOAD_UPDATE, Athena.INIT_PRELOAD_COMPLETE);
				},
				storeInitData : function(c) {
					this.step++;
					if (c["locale-xml"] !== undefined) {
						Athena.localeModel.parse(c["locale-xml"].data);
					}
					if (c["activity-xml"] !== undefined) {
						Athena.activityModel.parse(c["activity-xml"].data);
					}
					if (c["dlo-xml"] !== undefined) {
						Athena.dlolocaleModel.parse(c["dlo-xml"].data);
					}
					var b, d, a = Athena.settingsModel.get("preload-splash");
					for (b in c) {
						if (c.hasOwnProperty(b)) {
							if (c[b].type === "html") {
								Athena.settingsModel.addTemplate(b, c[b].data);
							}
						}
					}
					if (!c["locale-xml"] || c["locale-xml"].data === undefined || !c["activity-xml"] || c["activity-xml"].data === undefined || ! c["dlo-xml"] || c["dlo-xml"].data === undefined) {
						this.dispatch(Athena.ALERT_SHOW,
								"An error occured and the initial XML files did not load correctly, check the paths to the locale, activity and dlo xml files! " + c);
					} else {
						if (!a) {
							this.dispatch(Athena.APP_STARTUP);
						}
						d = Athena.dlolocaleModel.loadItems;
						if (d.length > 0) {
							Athena.preloadController.preload(d, Athena.localeModel.get("preload-alt"), Athena.INIT_PRELOAD_UPDATE, Athena.DLO_PRELOAD_COMPLETE);
						} else {
							this.storeDLOData({});
						}
					}
				},
				storeDLOData : function(b) {
					this.step++;
					var a, c;
					for (a in b) {
						if (b.hasOwnProperty(a)) {
							if (b[a].type === "html") {
								Athena.dlolocaleModel.templates[a] = b[a].data;
							}
							if (b[a].type === "xml") {
								Athena.dlolocaleModel.data[a] = b[a].data;
							}
						}
					}
					this.loadClasses();
				},
				loadClasses : function() {
					var j = this, a = Athena.settingsModel.get("auto-load-plugins"), h = Athena.settingsModel.get("auto-load-libs"), e = Athena.settingsModel
							.get("auto-load-activities"), b, g, f = 0, k = 0, c, d;
					g = Athena.arrayMethods.cleanDuplicates(Athena.dlolocaleModel.getActivityIds());
					b = Athena.activityModel.classes(g);
					d = function(i) {
						k++;
						if (k === f) {
							Athena.activityModel.initClasses(g);
							j.classesLoaded();
						} else {
							j.updatePreload(Athena.localeModel.get("preload-classes"), ((k / f) * 100));
						}
					};
					f = b.length;
					if (f === 0) {
						this.dispatch(Athena.PRELOADER_HIDE);
						this.dispatch(Athena.ALERT_SHOW, Athena.localeModel.get("no-classes"));
					} else {
						for (c = 0; c < f; c++) {
							if (b[c].type === "plugin") {
								if (a) {
									$LAB.script(b[c].value).wait(d);
								} else {
									d("plugin");
								}
							} else {
								if (b[c].type === "lib") {
									if (h) {
										$LAB.script(b[c].value).wait(d);
									} else {
										d("lib");
									}
								} else {
									if (b[c].type === "activity") {
										if (e) {
											$LAB.script(b[c].value).wait(d);
										} else {
											d("activity");
										}
									} else {
										if (b[c].type === "css") {
											if (e) {
												$("#theme-style").after('<link rel="stylesheet" href="' + b[c].value + '" type="text/css" media="screen" />');
												d();
											} else {
												d("css");
											}
										}
									}
								}
							}
						}
					}
				},
				classesLoaded : function() {
					var d, b, a, c, e;
					d = Athena.arrayMethods.cleanDuplicates(Athena.dlolocaleModel.getActivityIds());
					b = Athena.settingsModel.get("preload-shell-assets");
					a = Athena.settingsModel.get("preload-activity-assets");
					c = Athena.settingsModel.get("preload-content-assets");
					e = [];
					if (b) {
						e = e.concat(Athena.settingsModel.get("preload-assets"));
					}
					if (a) {
						e = e.concat(Athena.activityModel.assets(d));
					}
					if (c) {
						e.concat(Athena.arrayMethods.cleanDuplicates(Athena.dlolocaleModel.assets, "path"));
					}
					e = Athena.arrayMethods.cleanDuplicates(e, "path");
					Athena.preloadController.preload(e, Athena.localeModel.get("preload-assets"), Athena.INIT_PRELOAD_UPDATE, Athena.ASSET_PRELOAD_COMPLETE);
				},
				assetsLoaded : function() {
					var a = Athena.settingsModel.get("preload-splash");
					if (!a) {
						this.dispatch(Athena.PRELOADER_HIDE);
					} else {
						this.dispatch(Athena.APP_STARTUP);
					}
				}
			});
	Athena.StartUpController = Athena.CoreController.extend({
		name : "startup",
		initialize : function() {
			this.register(Athena.APP_STARTUP, this.execute, this);
		},
		execute : function() {
			var b, c = Athena.settingsModel.get("shell-view"), a;
			b = c.split(".");
			if (b.length === 3) {
				a = window[b[0]][b[1]][b[2]];
			} else {
				if (b.length === 2) {
					a = window[b[0]][b[1]];
				} else {
					if (b.length === 1) {
						a = window[b[0]];
					}
				}
			}
			Athena.shellView = new a();
			Athena.shellView.activate();
		}
	});
	Athena.CoreView = Backbone.View.extend({
		autoRender : true,
		html : null,
		ahtml : null,
		animeTime1 : 1,
		animeTime2 : 1,
		animeTime3 : 1,
		easing1 : 5,
		easing2 : 5,
		easing3 : 5,
		initialize : function() {
			this.animeTime1 = (Athena.settingsModel.get("anime-speed-1") !== "undefined") ? Number(Athena.settingsModel.get("anime-speed-1")) : 1;
			this.animeTime2 = (Athena.settingsModel.get("anime-speed-2") !== "undefined") ? Number(Athena.settingsModel.get("anime-speed-2")) : 1;
			this.animeTime3 = (Athena.settingsModel.get("anime-speed-3") !== "undefined") ? Number(Athena.settingsModel.get("anime-speed-3")) : 1;
			this.easing1 = (Athena.settingsModel.get("easing-type-1") !== "undefined") ? Number(Athena.settingsModel.get("easing-type-1")) : 5;
			this.easing2 = (Athena.settingsModel.get("easing-type-2") !== "undefined") ? Number(Athena.settingsModel.get("easing-type-2")) : 5;
			this.easing3 = (Athena.settingsModel.get("easing-type-3") !== "undefined") ? Number(Athena.settingsModel.get("easing-type-3")) : 5;
			if (this.autoRender) {
				this.render();
			}
		},
		getTemplate : function(a, b, c) {
			$.ajax({
				type : "GET",
				url : a,
				dataType : "html",
				success : function(d) {
					if (c === 1) {
						b.onTemplateLoaded(d);
					} else {
						if (c === 2) {
							b.onActivitiesLoaded(d);
						}
					}
				}
			});
		},
		resize : function(a) {},
		show : function() {
			this.$el.show();
		},
		hide : function() {
			this.$el.hide();
		},
		fadeIn : function() {
			this.$el.css({
				opacity : 0,
				display : "block"
			});
			Athena.transitions.animate(this.$el, {
				opacity : 1
			}, this.animeTime3);
		},
		fadeOut : function() {
			var a = this;
			this.$el.css("opacity", 1);
			Athena.transitions.animate(this.$el, {
				opacity : 0
			}, this.animeTime3, function() {
				a.$el.css({
					display : "none"
				});
			});
		},
		setEvents : function() {},
		clearEvents : function() {},
		dispatch : function(e, a, d, c, b) {
			this.eventBus.trigger(e, a, d, c, b);
		},
		register : function(c, b, a) {
			this.eventBus.on(c, b, a);
		},
		render : function() {}
	});
	Athena.AlertView = Athena.CoreView.extend({
		buttonOk : null,
		buttonCancel : null,
		action : null,
		initialize : function() {
			Athena.AlertView.__super__.initialize.call(this);
			this.register(Athena.ALERT_HIDE, this.hide, this);
			this.register(Athena.ALERT_SHOW, this.show, this);
		},
		show : function(a, b) {
			this.eventBus.trigger(Athena.PRELOADER_HIDE);
			if (b === undefined) {
				this.buttonCancel.css("display", "none");
			} else {
				this.action = b;
				this.buttonCancel.css("display", "inline");
			}
			this.$el.find("section").text(a);
			this.$el.css({
				opacity : 0,
				display : "block"
			});
			Athena.transitions.animate(this.$el, {
				opacity : 1
			});
		},
		hide : function() {
			if (this.action) {
				this.dispatch(Athena.ALERT_ACCEPT, this.action);
			}
			this.$el.hide();
		},
		setEvents : function() {
			var a = this;
			this.buttonOk.click(function() {
				a.hide();
			});
			this.buttonCancel.click(function() {
				a.hide();
			});
		},
		render : function() {
			this.$el = $("#alert-modal");
			this.buttonOk = $("#button-alert-ok");
			this.buttonCancel = $("#button-alert-cancel");
			this.setEvents();
		}
	});
	Athena.AudioView = Athena.CoreView.extend({
		el : "#panel-audio",
		buttonMute : null,
		buttonVolume : null,
		volumeBar : null,
		volumeBarColour : null,
		sliderPosition : 0,
		percentage : 100,
		barHeight : 0,
		buttonYStart : 0,
		buttonHeight : 0,
		defaultPanelHeight : 0,
		bindToElement : null,
		isOpen : false,
		initialize : function() {
			Athena.AudioView.__super__.initialize.call(this);
			var a = this;
			this.register(Athena.AUDIO_SHOW, this.show, this);
			this.register(Athena.AUDIO_HIDE, this.hide, this);
			_.bindAll(this);
			$(document).bind("keypress", function(b) {
				if (a.isOpen) {
					a.useKeys(b);
				}
			});
		},
		useKeys : function(d) {
			var a, c, b = this.buttonVolume.position().top;
			c = d.keyCode || d.which;
			a = {
				left : 37,
				up : 38,
				right : 39,
				down : 40
			};
			switch (c) {
			case a.up:
				if (this.percentage < 100) {
					this.buttonVolume.css("top", b - 1);
					this.handleSliderPosition();
				}
				break;
			case a.down:
				if (this.percentage > 0) {
					this.buttonVolume.css("top", b + 1);
					this.handleSliderPosition();
				}
				break;
			}
		},
		show : function() {
			this.bindPosition();
			var a = {
				top : -this.defaultPanelHeight,
				height : this.defaultPanelHeight
			};
			Athena.transitions.animate(this.$el, a, this.animeTime2);
			this.isOpen = true;
		},
		hide : function() {
			var a = {
				top : 0,
				height : 0
			};
			Athena.transitions.animate(this.$el, a, this.animeTime2);
			this.isOpen = false;
		},
		handleMuteClick : function(a) {
			if (this.isAudioMute) {
				this.buttonMute.removeClass("item-mute-on");
				this.isAudioMute = false;
				this.dispatch(Athena.AUDIO_MUTE, "off");
			} else {
				this.buttonMute.addClass("item-mute-on");
				this.isAudioMute = true;
				this.dispatch(Athena.AUDIO_MUTE, "on");
			}
		},
		setEvents : function() {
			var a = this;
			this.dispatch(Athena.AUDIO_INIT);
			if (this.buttonMute) {
				this.buttonMute.click(function(c) {
					a.handleMuteClick(c);
				});
			}
			try {
				a.buttonVolume.draggable({
					containment : a.volumeBar,
					axis : "y",
					stop : function() {
						$(document).unbind("mousemove");
					}
				});
				a.buttonVolume.mousedown(function() {
					$(document).mousemove(function() {
						a.handleSliderPosition();
					});
				}).mouseup(function() {
					$(document).unbind("mousemove");
				});
			} catch (b) {
				debug("%c AudioView Error: The Audio view volume slider is dependant on jQuery draggable : " + b, "background:#ffcc00");
			}
		},
		handleSliderPosition : function() {
			var d, c, b = this.buttonYStart, a = this.buttonYStart + this.barHeight - this.buttonHeight;
			this.sliderPosition = this.buttonVolume.position().top;
			d = -1 * ((((this.sliderPosition - b) / (a - b)).toFixed(2)) - 1);
			this.percentage = Math.round(d * 100);
			if (this.volumeBarColour) {
				c = (this.percentage / 100) * this.barHeight;
				this.volumeBarColour.height(c);
			}
			this.dispatch(Athena.AUDIO_VOLUME, d);
		},
		bindPosition : function() {
			if (this.bindToElement && this.bindToElement !== undefined) {
				if (this.bindToElement.length > 0) {
					var a = this.bindToElement.position().left + (this.bindToElement.width() / 2) - 5;
					this.$el.css("left", a);
				}
			}
		},
		render : function() {
			this.buttonMute = this.$el.find(".item-mute");
			this.buttonVolume = this.$el.find(".volume-drag-icon");
			this.volumeBar = this.$el.find(".volume-bar");
			this.volumeBarColour = this.$el.find(".inner");
			this.$el.show();
			this.barHeight = this.volumeBar.height();
			this.buttonYStart = this.buttonVolume.position().top;
			this.buttonHeight = this.buttonVolume.height();
			this.defaultPanelHeight = this.$el.height();
			this.$el.height(0).css("opacity", 1);
			this.setEvents();
		}
	});
	Athena.ClosedCaptionView = Athena.CoreView.extend({
		el : "#closed-caption-view",
		captionText : null,
		initialize : function() {
			Athena.ClosedCaptionView.__super__.initialize.call(this);
			this.register(Athena.CAPTIONS_SHOW, this.fadeIn, this);
			this.register(Athena.CAPTIONS_HIDE, this.fadeOut, this);
			this.register(Athena.CAPTIONS_UPDATE, this.update, this);
		},
		update : function(a) {
			if (a !== undefined) {
				this.captionText.empty().append(a);
			}
		},
		render : function() {
			this.captionText = this.$el.find(".caption-overview");
		}
	});
	Athena.CoreActivitiesView = Athena.CoreView.extend({
		contentContainer : null,
		activityContainers : null,
		displayMode : "standard",
		initialize : function() {
			this.displayMode = (Athena.settingsModel.get("dlo-mode") !== "undefined") ? Athena.settingsModel.get("dlo-mode") : "standard";
			this.register(Athena.APP_RESIZE, this.resize, this);
			this.register(Athena.ACTIVITY_CHANGED, this.next, this);
			this.register(Athena.ACTIVITY_SWITCH, this.switched, this);
			this.register(Athena.TOGGLE_UI, this.toggle, this);
			Athena.CoreActivitiesView.__super__.initialize.call(this);
		},
		generate : function(d) {
			var b = this, f, c, g, a, e;
			c = this.$el.find("#default-activity-content");
			_.each(Athena.dlolocaleModel.models, function(h) {
				f = c.clone().appendTo(b.contentContainer);
				f.attr("id", "activity-container-" + h.attributes.id);
				g = $(d).filter(function() {
					var l, j, k;
					l = $(this).attr("class");
					if (l !== undefined) {
						if (l.split(" ")[1] === "activity-" + h.attributes.activityId) {
							if (Number($(this).attr("data-type")) === Number(h.attributes.type)) {
								k = $(this);
							}
						}
					}
					return k;
				});
				if (g.length === 0) {
					g = $(d).filter(".activity-" + h.attributes.activityId + '[data-type="1"]');
				}
				f.find(".activity").replaceWith(g);
				f.attr("id", "activity-" + h.attributes.id);
				var i = Athena.activityModel.getOption(h.attributes.activityId, "component", "ref");
				if (i) {
					a = window[i];
					if (a) {
						e = new a(h.attributes.id, h.attributes.activityId, h.attributes.type);
					} else {
						debug("ERROR::The view component could not be initialised:: " + i);
					}
				}
			});
			this.activityContainers = $("section.content");
			c.remove();
			this.position();
			this.display();
		},
		switched : function(c) {
			var a, d, b = this;
			if (this.displayMode === "standard") {
				this.$el.find(".content").hide();
				this.$el.find("#activity-" + Athena.dlolocaleModel.selectedActivity).show();
				this.dispatch(Athena.ACTIVITY_START, Athena.dlolocaleModel.selectedActivity);
			} else {
				if (this.displayMode === "vertical") {
					a = Athena.dlolocaleModel.selectedActivity * this.$el.height();
					this.contentContainer.css({
						margin : -a + "px 0 0 0"
					});
					this.dispatch(Athena.ACTIVITY_START, Athena.dlolocaleModel.selectedActivity);
				} else {
					if (this.displayMode === "filmstrip") {
						a = Athena.dlolocaleModel.selectedActivity * Athena.WINDOW_WIDTH;
						this.contentContainer.css({
							margin : "0 0 0 " + -a + "px"
						});
						this.dispatch(Athena.ACTIVITY_START, Athena.dlolocaleModel.selectedActivity);
					}
				}
			}
		},
		next : function(c) {
			var a, d, b = this;
			if (this.displayMode === "standard") {
				this.$el.find(".content").hide();
				this.$el.find("#activity-" + Athena.dlolocaleModel.selectedActivity).fadeIn(this.animeTime1 * 1000);
				if (c !== true) {
					this.dispatch(Athena.ACTIVITY_START, Athena.dlolocaleModel.selectedActivity);
				}
			} else {
				if (this.displayMode === "vertical") {
					a = Athena.dlolocaleModel.selectedActivity * this.$el.height();
					d = {
						margin : -a + "px 0 0 0"
					};
					Athena.transitions.animate(this.contentContainer, d, this.animeTime1, function() {
						if (c !== true) {
							b.dispatch(Athena.ACTIVITY_START, Athena.dlolocaleModel.selectedActivity);
						}
					}, this.easing1);
				} else {
					if (this.displayMode === "filmstrip") {
						a = Athena.dlolocaleModel.selectedActivity * Athena.WINDOW_WIDTH;
						d = {
							margin : "0 0 0 " + -a + "px"
						};
						if (c !== true) {
							Athena.transitions.animate(this.contentContainer, d, this.animeTime1, function() {
								if (c !== true) {
									b.dispatch(Athena.ACTIVITY_START, Athena.dlolocaleModel.selectedActivity);
								}
							}, this.easing1);
						} else {
							this.contentContainer.css({
								margin : "0 0 0 " + -a + "px"
							});
						}
					}
				}
			}
		},
		display : function() {
			if (this.displayMode === "standard") {
				this.contentContainer.css("width", "100%");
				this.activityContainers.css({
					display : "none",
					"float" : "none",
					width : "100%"
				});
				$("section#activity-0").css("display", "block");
			} else {
				if (this.displayMode === "filmstrip") {
					this.activityContainers.css({
						"float" : "left"
					});
					this.contentContainer.width(Number(Athena.dlolocaleModel.totalActivities * Athena.WINDOW_WIDTH));
					this.activityContainers.width(this.$el.width());
				} else {
					if (this.displayMode === "vertical") {
						$("section#activity-0").css("display", "block");
						this.activityContainers.css("height", this.$el.height());
						this.contentContainer.css({
							position : "relative",
							display : "block",
							"float" : "none",
							width : "100%",
							height : Number(Athena.dlolocaleModel.totalActivities * Athena.WINDOW_WIDTH)
						});
					}
				}
			}
		},
		resize : function() {
			if (this.displayMode === "filmstrip") {
				this.contentContainer.width(Number(Athena.dlolocaleModel.models.length * Athena.WINDOW_WIDTH));
				this.activityContainers.width(this.$el.width());
				this.next(true);
			} else {
				if (this.displayMode === "vertical") {
					this.activityContainers.css({
						position : "relative",
						display : "block",
						"float" : "none",
						width : "100%",
						height : this.$el.height()
					});
					this.next(true);
				}
			}
		},
		toggle : function() {
			this.position();
			this.resize();
		},
		position : function() {
			if (!Athena.HIDE_UI) {
				if (!Athena.HIDE_HEADER) {
					if (Athena.dlolocaleModel.options.hasHeader !== undefined && Athena.dlolocaleModel.options.hasHeader) {
						this.$el.addClass("header");
					}
				}
				if (!Athena.HIDE_FOOTER) {
					if (Athena.dlolocaleModel.options.hasFooter !== undefined && Athena.dlolocaleModel.options.hasFooter) {
						this.$el.addClass("footer");
					}
				}
				if (Athena.dlolocaleModel.options.hasMenuActivity !== undefined && !Athena.dlolocaleModel.options.hasMenuActivity) {
					this.$el.addClass("nomenu");
				}
			} else {
				this.$el.removeClass("header");
				this.$el.removeClass("footer");
			}
		},
		animate : function() {},
		fadeIn : function() {
			var a = this;
			this.contentContainer.css({
				opacity : 0
			});
			Athena.transitions.animate(this.contentContainer, {
				opacity : 1
			}, this.animeTime3);
		},
		fadeOut : function() {
			Athena.transitions.animate(this.contentContainer, {
				opacity : 0
			}, this.animeTime3);
		},
		render : function() {
			this.$el = $("#module-container");
			this.contentContainer = this.$el.find("#module-content-container");
		}
	});
	Athena.CoreActivityView = Athena.CoreView.extend({
		ref : -1,
		aId : -1,
		type : 1,
		activated : false,
		scrollBarSpeed : 0,
		initialize : function(b, a, c) {
			this.$el = $("#activity-" + b);
			this.ref = Number(b);
			this.aId = Number(a);
			this.type = (c !== undefined) ? Number(c) : 1;
			this.scrollBarSpeed = Number(Athena.settingsModel.get("scroll-show-speed"));
			this.model = Athena.dlolocaleModel.getData(this.ref, a, this.type);
			this.register(Athena.APP_RESIZE, this.resize, this);
			this.register(Athena.ACTIVITY_START, this.activate, this);
			this.register(Athena.ACTIVITY_RESET, this.doreset, this);
			this.register(Athena.ACTIVITY_REPLAY, this.doReplay, this);
			this.register(Athena.ACTIVITY_UPDATESTEP, this.updateStep, this);
			this.register(Athena.ACTIVITY_DISABLE, this.disable, this);
			this.register(Athena.ACTIVITY_ENABLE, this.disable, this);
			this.register(Athena.AUDIO_COMPLETE, this.audioCompleteListener, this);
			this.register(Athena.AUDIO_STOP, this.audioStopListener, this);
			this.register(Athena.AUDIO_TIMEUPDATE, this.audioTimedListener, this);
			debug("Activity View initialised  Reference: " + this.ref + " Activity Id: " + this.aId + " Type: " + this.type);
		},
		activate : function(a) {
			if (!this.activated && Number(this.ref) === Number(a)) {
				this.render();
			} else {
				if (a === -100) {
					this.render();
				} else {
					if (this.activated && Number(this.ref) === Number(a)) {
						this.enableFunctions();
					}
				}
			}
		},
		doreset : function(a) {
			if (Athena.settingsModel.get("reset-all")) {
				this.reset();
			} else {
				if (Number(a) === this.ref) {
					this.reset();
				}
			}
		},
		reset : function(a) {},
		dispatchAudio : function(b, a) {
			if (a) {
				this.dispatch(Athena.AUDIO_PLAY, b, true);
			} else {
				this.dispatch(Athena.AUDIO_PLAY, b);
			}
		},
		stopAudio : function(a) {
			if (a) {
				this.dispatch(Athena.AUDIO_PAUSE);
			} else {
				this.dispatch(Athena.AUDIO_STOP);
			}
		},
		updateClosedCaptions : function(a) {
			if (a === -1) {
				this.dispatch(Athena.CAPTIONS_UPDATE, "");
			} else {
				this.dispatch(Athena.CAPTIONS_UPDATE, a);
			}
		},
		disable : function(a) {
			if (Number(this.ref) === Number(a)) {
				this.disableFunctions();
			}
		},
		disableFunctions : function() {
			this.toggleScroll(false);
		},
		enableFunctions : function() {
			var a = this;
			this.checkContent();
			setTimeout(function() {
				a.toggleScroll(true);
			}, this.scrollBarSpeed * 1000);
		},
		audioStopListener : function(a, b) {
			if (this.ref === b) {
				this.resetAudioHandlers(a);
			}
		},
		audioCompleteListener : function(a, b) {
			if (this.ref === b) {
				this.completedAudioHandlers(a);
			}
		},
		audioTimedListener : function(b, a) {
			if (this.ref === b) {
				this.timedAudioHandlers(a);
			}
		},
		timedAudioHandlers : function(a) {},
		resetAudioHandlers : function(a) {},
		completedAudioHandlers : function(a) {},
		setEvents : function() {},
		clearEvents : function() {},
		startActivity : function() {},
		checkContent : function() {
			var c, b, a, d;
			if (this.model.contents.tip !== undefined && this.model.contents.tip.value === undefined) {
				c = this.model.contents.tipHeader;
				b = this.model.contents.tip;
			} else {
				if (this.model.contents.tip !== undefined && this.model.contents.tip.value !== undefined) {
					c = this.model.contents.tipHeader.value;
					b = this.model.contents.tip.value;
				}
			}
			if (c !== undefined) {
				this.dispatch(Athena.INSTRUCTIONS_UPDATE, {
					content : b,
					header : c
				});
			} else {
				this.dispatch(Athena.INSTRUCTIONS_UPDATE, {
					content : b
				});
			}
			if (this.model.contents.extraContentLabel !== undefined && this.model.contents.extraContentLabel.value === undefined) {
				a = this.model.contents.extraContentLabel;
				d = this.model.contents.extraContent;
			} else {
				if (this.model.contents.extraContentLabel !== undefined && this.model.contents.extraContentLabel.value !== undefined) {
					a = this.model.contents.extraContentLabel.value;
					d = this.model.contents.extraContent.value;
				}
			}
			this.dispatch(Athena.EXCONTENT_UPDATE, {
				header : a,
				content : d
			});
		},
		setContent : function() {
			this.checkContent();
		},
		checkSteps : function() {
			var b, h, f, e = 1, d, g = 20, a = 0, c = 0;
			if (this.model.option.showSteps !== undefined) {
				if (this.model.option.showSteps.value !== undefined && this.model.option.showSteps.value === "Y") {
					b = this.model.interactivities;
					for (h in b) {
						if (b.hasOwnProperty(h)) {
							if (h === ("stepgroup" + e)) {
								a++;
								e++;
							}
						}
					}
					Athena.dlolocaleModel.selectedActivityStep = 1;
					Athena.dlolocaleModel.totalActivitySteps = Number(a);
					this.dispatch(Athena.MENUSUB_SET, a, true);
				}
			}
		},
		updateStep : function(b, a) {
			if (this.ref === b) {
				this.setStep(a);
			}
		},
		setStep : function(a) {},
		changeStep : function(b, a) {},
		doReplay : function(a) {
			if (this.ref === a) {
				this.replayAction();
			}
		},
		replayAction : function() {},
		toggleScroll : function(a) {
			var b = Athena.dlolocaleModel.options.scrollType;
			if (b === undefined) {
				b = Athena.settingsModel.get("scroll-type");
			}
			if (b === undefined) {
				b = false;
			}
			if (b === "A") {
				if (a) {
					this.$el.addClass("activescroll");
				} else {
					this.$el.removeClass("activescroll");
					this.$el.find(".activity").css({
						width : Athena.WINDOW_WIDTH
					});
				}
			}
		},
		resize : function() {
			var a = this;
			this.toggleScroll(false);
			if (this.ref === Athena.dlolocaleModel.selectedActivity) {
				setTimeout(function() {
					a.toggleScroll(true);
				}, this.scrollBarSpeed * 1000);
			}
		},
		render : function() {
			var a = this;
			this.activated = true;
			this.setContent();
			if (Athena.settingsModel.get("header-auto-open") !== undefined) {
				if (Athena.settingsModel.get("header-auto-open") === true) {
					this.dispatch(Athena.TOGGLE_HEADER, true);
				}
			}
			setTimeout(function() {
				a.toggleScroll(true);
			}, this.scrollBarSpeed * 1000);
		}
	});
	Athena.CoreSettingsView = Athena.CoreView.extend({
		isInfoOpen : false,
		isAudioOpen : false,
		isCaptionOpen : false,
		isToolsOpen : false,
		isGlossaryOpen : false,
		isResourcesOpen : false,
		isHelpOpen : false,
		buttonHome : null,
		buttonAudio : null,
		buttonReset : null,
		buttonReplay : null,
		buttonReload : null,
		buttonInfo : null,
		buttonFullscreen : null,
		buttonCC : null,
		buttonPause : null,
		buttonResources : null,
		buttonTools : null,
		buttonGlossary : null,
		buttonPrint : null,
		buttonHelp : null,
		initialize : function() {
			Athena.CoreSettingsView.__super__.initialize.call(this);
			this.register(Athena.F11, this.updateFullscreenButton, this);
			this.register(Athena.INFO_CLOSED, this.handleInfoClick, this);
			this.register(Athena.HELP_CLOSED, this.handleHelpClick, this);
			this.register(Athena.TOOLS_CLOSED, this.handleToolsClick, this);
		},
		handleHomeClick : function() {},
		handleAudioClick : function() {
			var b, a;
			a = this;
			if (this.isAudioOpen) {
				this.isAudioOpen = false;
				this.dispatch(Athena.AUDIO_HIDE);
				$(".item-audio").removeClass("active");
			} else {
				this.isAudioOpen = true;
				this.dispatch(Athena.AUDIO_SHOW);
				$(".item-audio").addClass("active");
			}
		},
		handleReloadClick : function() {
			window.location.reload();
		},
		handleReplayClick : function() {
			this.dispatch(Athena.ACTIVITY_REPLAY, Athena.dlolocaleModel.selectedActivity);
		},
		handleResourcesClick : function() {
			if (this.isResourcesOpen) {
				this.isResourcesOpen = false;
				this.buttonResources.removeClass("active");
				this.dispatch(Athena.RESOURCES_HIDE);
			} else {
				this.isResourcesOpen = true;
				this.buttonResources.addClass("active");
				this.dispatch(Athena.RESOURCES_SHOW);
			}
		},
		handleResetClick : function() {
			this.dispatch(Athena.ACTIVITY_RESET, Athena.dlolocaleModel.selectedActivity);
		},
		handleInfoClick : function(a) {
			if (a !== undefined && a) {
				this.isInfoOpen = false;
				this.buttonInfo.removeClass("active");
			} else {
				if (this.isInfoOpen) {
					this.isInfoOpen = false;
					this.buttonInfo.removeClass("active");
					this.dispatch(Athena.INFO_HIDE);
				} else {
					this.isInfoOpen = true;
					this.buttonInfo.addClass("active");
					this.dispatch(Athena.INFO_SHOW);
				}
			}
		},
		handleGlossaryClick : function() {
			if (this.isGlossaryOpen) {
				this.isGlossaryOpen = false;
				this.buttonGlossary.removeClass("active");
				this.dispatch(Athena.GLOSSARY_HIDE);
			} else {
				this.isGlossaryOpen = true;
				this.buttonGlossary.addClass("active");
				this.dispatch(Athena.GLOSSARY_SHOW);
			}
		},
		handlePrintClick : function() {},
		handleCaptionClick : function() {
			if (this.isCaptionOpen) {
				this.isCaptionOpen = Athena.CLOSE_CAPTIONS = false;
				this.dispatch(Athena.CAPTIONS_HIDE);
				this.buttonCC.removeClass("active");
			} else {
				this.isCaptionOpen = Athena.CLOSE_CAPTIONS = true;
				this.dispatch(Athena.CAPTIONS_SHOW);
				this.buttonCC.addClass("active");
			}
		},
		handlePauseClick : function() {},
		handleFullscreenClick : function() {
			var a = $(".item-fullscreen");
			this.dispatch(Athena.FULLSCREEN_TOGGLE);
		},
		handleToolsClick : function(a) {
			if (a !== undefined && a) {
				this.isToolsOpen = false;
				this.buttonTools.removeClass("active");
			} else {
				if (this.isToolsOpen) {
					this.isToolsOpen = false;
					this.dispatch(Athena.TOOLS_HIDE);
					this.buttonTools.removeClass("active");
				} else {
					this.isToolsOpen = true;
					this.dispatch(Athena.TOOLS_SHOW);
					this.buttonTools.addClass("active");
				}
			}
		},
		handleHelpClick : function() {
			if (this.isHelpOpen) {
				this.isHelpOpen = false;
				this.dispatch(Athena.HELP_HIDE);
				this.buttonHelp.removeClass("active");
			} else {
				this.isHelpOpen = true;
				this.dispatch(Athena.HELP_SHOW);
				this.buttonHelp.addClass("active");
			}
		},
		updateFullscreenButton : function(a) {
			var b = $(".item-fullscreen");
			if (!window.screenTop && !window.screenY) {
				if (a) {
					b.removeClass("active");
				} else {
					b.addClass("active");
				}
			} else {
				if (a) {
					b.addClass("active");
				} else {
					b.removeClass("active");
				}
			}
		},
		setVisibleButtons : function() {
			this.checkHome();
			this.checkAudio();
			this.checkPause();
			this.checkReload();
			this.checkReset();
			this.checkReplay();
			this.checkInfo();
			this.checkCaptions();
			this.checkFullscreen();
			this.checkPrint();
			this.checkResources();
			this.checkGlossary();
			this.checkTools();
			this.checkHelp();
		},
		checkHome : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasHome !== undefined) {
				if (Athena.dlolocaleModel.options.hasHome) {
					this.buttonHome = this.$el.find(".item-home");
					this.buttonHome.addClass("visible");
					this.buttonHome.click(function() {
						a.handleHomeClick();
					});
				}
			}
		},
		checkAudio : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasAudio !== undefined) {
				if (Athena.dlolocaleModel.options.hasAudio) {
					this.buttonAudio = this.$el.find(".item-audio");
					this.buttonAudio.addClass("visible");
					this.buttonAudio.click(function() {
						a.handleAudioClick();
					});
				}
			}
		},
		checkPause : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasPlayPause !== undefined) {
				if (Athena.dlolocaleModel.options.hasPlayPause) {
					this.buttonPause = this.$el.find(".item-pause");
					this.buttonPause.addClass("visible");
					this.buttonPause.click(function() {
						a.handlePauseClick();
					});
				}
			}
		},
		checkReload : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasReload !== undefined) {
				if (Athena.dlolocaleModel.options.hasReload) {
					this.buttonReload = this.$el.find(".item-reload");
					this.buttonReload.addClass("visible");
					this.buttonReload.click(function() {
						a.handleReloadClick();
					});
				}
			}
		},
		checkReset : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasReset !== undefined) {
				if (Athena.dlolocaleModel.options.hasReset) {
					this.buttonReset = this.$el.find(".item-reset");
					this.buttonReset.addClass("visible");
					this.buttonReset.click(function() {
						a.handleResetClick();
					});
				}
			}
		},
		checkReplay : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasReplay !== undefined) {
				if (Athena.dlolocaleModel.options.hasReplay) {
					this.buttonReplay = this.$el.find(".item-replay");
					this.buttonReplay.addClass("visible");
					this.buttonReplay.click(function() {
						a.handleReplayClick();
					});
				}
			}
		},
		checkInfo : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasInfo !== undefined) {
				if (Athena.dlolocaleModel.options.hasInfo) {
					this.buttonInfo = this.$el.find(".item-info");
					this.buttonInfo.addClass("visible");
					this.buttonInfo.click(function() {
						if ($(this).is(":not(.active)")) {
							a.handleInfoClick();
						}
					});
				}
			}
		},
		checkCaptions : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasCC !== undefined) {
				if (Athena.dlolocaleModel.options.hasCC) {
					this.buttonCC = this.$el.find(".item-cc");
					this.buttonCC.addClass("visible");
					this.buttonCC.click(function() {
						a.handleCaptionClick();
					});
				}
			}
		},
		checkFullscreen : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasFullscreen !== undefined) {
				if (Athena.dlolocaleModel.options.hasFullscreen) {
					this.buttonFullscreen = this.$el.find(".item-fullscreen");
					this.buttonFullscreen.addClass("visible");
					this.buttonFullscreen.click(function() {
						a.handleFullscreenClick();
					});
				}
			}
		},
		checkPrint : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasPrint !== undefined) {
				if (Athena.dlolocaleModel.options.hasPrint) {
					this.buttonPrint = this.$el.find(".item-print");
					this.buttonPrint.addClass("visible");
					this.buttonPrint.click(function() {
						a.handlePrintClick();
					});
				}
			}
		},
		checkResources : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasResources !== undefined) {
				if (Athena.dlolocaleModel.options.hasResources) {
					this.buttonResources = this.$el.find(".item-resource");
					this.buttonResources.addClass("visible");
					this.buttonResources.click(function() {
						a.handleResourcesClick();
					});
				}
			}
		},
		checkGlossary : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasGlossary !== undefined) {
				if (Athena.dlolocaleModel.options.hasGlossary) {
					this.buttonGlossary = this.$el.find(".item-glossary");
					this.buttonGlossary.addClass("visible");
					this.buttonGlossary.click(function() {
						a.handleGlossaryClick();
					});
				}
			}
		},
		checkTools : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasTools !== undefined) {
				if (Athena.dlolocaleModel.options.hasTools) {
					this.buttonTools = this.$el.find(".item-tools");
					this.buttonTools.addClass("visible");
					this.buttonTools.click(function() {
						a.handleToolsClick();
					});
				}
			}
		},
		checkHelp : function() {
			var a = this;
			if (Athena.dlolocaleModel.options.hasHelp !== undefined) {
				if (Athena.dlolocaleModel.options.hasHelp) {
					this.buttonHelp = this.$el.find(".item-help");
					this.buttonHelp.addClass("visible");
					this.buttonHelp.click(function() {
						a.handleHelpClick();
					});
				}
			}
		},
		render : function() {
			this.$el = $("#settings-menu");
			this.$el.addClass("visible");
			this.setVisibleButtons();
		}
	});
	Athena.CoreShellContainerView = Athena.CoreView.extend({
		activities : null,
		template : null,
		isHeaderActive : true,
		isFooterActive : true,
		collapseHeader : false,
		collapseFooter : false,
		headerContainer : null,
		footerContainer : null,
		contentContainer : null,
		moduleContainer : null,
		splashView : null,
		menuView : null,
		subMenuView : null,
		arrowMenuView : null,
		sideMenuView : null,
		settingsView : null,
		activityView : null,
		extraContentView : null,
		imageView : null,
		resourcesView : null,
		infoView : null,
		instructionalView : null,
		glossaryView : null,
		closedCaptionView : null,
		audioView : null,
		scoreView : null,
		successView : null,
		toolsView : null,
		helpView : null,
		buttonCollapse : null,
		themes : null,
		initialize : function() {
			this.autoRender = false;
			this.register(Athena.APP_RESIZE, this.resize, this);
			this.register(Athena.SHELL_INIT, this.initShell, this);
			this.register(Athena.SHELL_ACTIVATE, this.activateShell, this);
			this.register(Athena.ACTIVITY_CHANGED, this.activityLocale, this);
			Athena.CoreShellContainerView.__super__.initialize.call(this);
		},
		activate : function() {
			this.html = Athena.settingsModel.templates["html-shell"];
			this.ahtml = Athena.settingsModel.templates["html-activities"];
			this.render();
		},
		render : function() {
			$(this.html).insertBefore("#preload-modal");
			this.$el = $("section.shell");
			this.resize();
		},
		randomiseTheme : function() {
			var b, a = this;
			_.each(this.themes, function(c) {
				if (a.$el.hasClass(c)) {
					a.$el.removeClass(c);
				}
			});
			b = Math.floor(Math.random() * this.themes.length);
			this.$el.addClass(this.themes[b]);
		},
		initShell : function() {},
		activateShell : function() {},
		resize : function() {},
		tab : function() {}
	});
	Athena.CoreMenuView = Athena.CoreView.extend({
		buttonLeft : null,
		buttonRight : null,
		overlayLeft : null,
		overlayRight : null,
		animateLeft : false,
		animateRight : false,
		startAnimation : function(a) {
			if (a === 1) {
				this.animateLeft = true;
			} else {
				if (a === 2) {
					this.animateRight = true;
				}
			}
			this.animateButton();
		},
		stopAnimation : function(a) {
			if (a === 1) {
				this.animateLeft = false;
			} else {
				if (a === 2) {
					this.animateRight = false;
				} else {
					this.animateLeft = false;
					this.animateRight = false;
				}
			}
		},
		animateButton : function() {
			var c, e, d, b, a = this;
			if (this.animateLeft) {
				c = function() {
					a.overlayLeft.fadeIn(a.animeTime1, function() {
						e();
					});
				};
				e = function() {
					if (a.animateLeft) {
						a.overlayLeft.fadeOut(a.animeTime1, function() {
							c();
						});
					} else {
						a.overlayLeft.fadeOut(a.animeTime1);
					}
				};
				e();
			} else {
				if (this.animateRight) {
					d = function() {
						a.overlayRight.fadeIn(a.animeTime1, function() {
							b();
						});
					};
					b = function() {
						if (a.animateRight) {
							a.overlayRight.fadeOut(a.animeTime1, function() {
								d();
							});
						} else {
							a.overlayRight.fadeOut(a.animeTime1);
						}
					};
					b();
				}
			}
		}
	});
	Athena.CoreDialogView = Athena.CoreView.extend({
		dialogPanel : null,
		buttonOpen : null,
		buttonClose : null,
		contentContainer : null,
		headerContainer : null,
		headerTitle : null,
		isOpen : false,
		isDraggable : true,
		position : null,
		setEvents : function() {
			var a = this;
			if (this.buttonClose) {
				this.buttonClose.click(function(b) {
					a.isOpen = false;
					a.hide();
				});
			}
			if (this.buttonOpen) {
				this.buttonOpen.click(function(b) {
					a.fadeIn();
					a.isOpen = true;
				});
			}
		},
		update : function(a) {
			if (a !== undefined) {
				if (a.content !== undefined) {
					this.contentContainer.html(a.content);
				}
				if (a.header !== undefined) {
					this.headerTitle.html(a.header);
				}
				if (this.buttonOpen !== null && a.content !== undefined && a.header !== undefined) {
					this.buttonOpen.addClass("visible");
				}
			}
		},
		setContent : function() {},
		fadeIn : function(a) {
			if (!this.isOpen) {
				this.setPosition();
				this.update(a);
				Athena.CoreDialogView.__super__.fadeIn.call(this);
			}
		},
		checkDragStatus : function() {
			this.isDraggable = (Athena.settingsModel.get("draggable-dialogs") !== "undefined") ? Athena.settingsModel.get("draggable-dialogs") : false;
			if (this.isDraggable) {
				try {
					this.$el.find(".sys-dialog").draggable({
						handle : this.headerContainer
					});
				} catch (a) {
					debug("Draggable Error: Object not found");
				}
			}
		},
		setPosition : function(a) {
			if (a !== undefined) {
				this.position = {};
				this.position = a;
			}
			if (this.position) {
				this.dialogPanel.attr("style", "");
				this.dialogPanel.css(this.position);
			}
		},
		render : function() {
			this.dialogPanel = this.$el.find(".sys-dialog");
			this.buttonClose = this.$el.find(".button-close");
			this.headerContainer = this.$el.find("header");
			this.headerTitle = this.headerContainer.find("h1");
			this.contentContainer = this.$el.find(".dialog-content");
			this.setContent();
			this.setPosition({
				left : "50%",
				"margin-left" : -(this.dialogPanel.width() / 2) + "px",
				top : "50%",
				"margin-top" : -(this.dialogPanel.height() / 2) + "px"
			});
			this.setEvents();
			this.checkDragStatus();
		}
	});
	Athena.ExtraContentView = Athena.CoreDialogView.extend({
		initialize : function() {
			Athena.ExtraContentView.__super__.initialize.call(this);
			this.register(Athena.EXCONTENT_HIDE, this.fadeOut, this);
			this.register(Athena.EXCONTENT_UPDATE, this.update, this);
			this.register(Athena.ACTIVITY_RESET, this.restore, this);
		},
		update : function(a) {
			if (a !== undefined) {
				if (a.content !== undefined) {
					this.contentContainer.html(this.checkContent(a.content));
				}
				if (a.header !== undefined) {
					this.headerTitle.html(a.header);
				}
				if (this.buttonOpen !== null && a.content !== "" && a.header !== "") {
					this.buttonOpen.addClass("visible");
					this.buttonOpen.find("span.label").html(a.header);
				} else {
					if (this.buttonOpen) {
						this.buttonOpen.removeClass("visible");
					}
				}
			}
		},
		checkContent : function(c) {
			var b, a = this, d = String(c);
			b = d.replace(/\\<ch\>/g, '<input class="ec-checkbox" type="checkbox" value="" />');
			return b;
		},
		restore : function() {
			this.$el.find(".ec-checkbox").prop("checked", false);
		},
		fadeOut : function(a) {
			Athena.ExtraContentView.__super__.fadeOut.call(this, a);
			this.destroyScroll();
			this.isOpen = false;
		},
		fadeIn : function(a) {
			Athena.ExtraContentView.__super__.fadeIn.call(this, a);
			if (!this.isOpen) {
				this.setScroll();
			}
		},
		setScroll : function() {
			this.destroyScroll();
			try {
				this.contentContainer.mCustomScrollbar();
			} catch (a) {
				debug("Could not add scroll bar");
			}
		},
		destroyScroll : function() {
			try {
				this.contentContainer.mCustomScrollbar("destroy");
			} catch (a) {
				debug("Could not destroy scroll bar");
			}
		},
		render : function() {
			var a, b = this;
			this.$el = $("#extracontent-modal");
			this.buttonOpen = $("#button-extracontent");
			Athena.ExtraContentView.__super__.render.call(this);
		}
	});
	Athena.GlossaryView = Athena.CoreView.extend({
		el : "#panel-glossary",
		initialize : function() {
			Athena.GlossaryView.__super__.initialize.call(this);
			this.register(Athena.GLOSSARY_HIDE, this.fadeOut, this);
			this.register(Athena.GLOSSARY_SHOW, this.fadeIn, this);
		}
	});
	Athena.ImageView = Athena.CoreView.extend({
		el : "#imageviewer-modal",
		buttonClose : null,
		buttonAudio : null,
		imagePanel : null,
		imageContainer : null,
		audioReference : null,
		audioActive : false,
		isDraggabele : true,
		initialize : function() {
			Athena.ImageView.__super__.initialize.call(this);
			this.register(Athena.IMAGE_HIDE, this.fadeOut, this);
			this.register(Athena.IMAGE_SHOW, this.fadeIn, this);
			this.register(Athena.AUDIO_COMPLETE, this.resetAudio, this);
			this.register(Athena.AUDIO_STOP, this.resetAudio, this);
		},
		resetAudio : function() {
			this.audioActive = false;
			this.buttonAudio.removeClass("active");
		},
		handleAudio : function() {
			if (this.audioActive) {
				this.audioActive = false;
				this.dispatch(Athena.AUDIO_STOP, this.audioReference);
				this.buttonAudio.removeClass("active");
			} else {
				this.audioActive = true;
				this.dispatch(Athena.AUDIO_PLAY, this.audioReference);
				this.buttonAudio.addClass("active");
			}
		},
		fadeIn : function(a) {
			Athena.ImageView.__super__.fadeIn.call(this);
			this.dispatch(Athena.PRELOADER_HIDE);
			if (a !== undefined) {
				if (a.audio === undefined) {
					this.buttonAudio.hide();
					this.audioReference = null;
				} else {
					this.buttonAudio.show();
					this.audioReference = a.audio;
				}
				this.load(a.image);
			}
		},
		load : function(b) {
			var a = this, c = new Image();
			c.src = b;
			c.onload = function() {
				a.imageContainer.empty().append(c);
				a.imagePanel.width(c.width);
				a.imagePanel.height(c.height);
				a.imagePanel.css({
					left : "50%",
					"margin-left" : -c.width / 2
				});
			};
			if (Athena.browserDetails[0].Browser === "Internet Explorer" && Number(Athena.browserDetails[0].Version) < 9) {
				a.imageContainer.empty().append(c);
				a.imagePanel.width(c.width);
				a.imagePanel.height(c.height);
				a.imagePanel.css({
					left : "50%",
					"margin-left" : -c.width / 2
				});
			}
			a.fadeIn();
		},
		setEvents : function() {
			var a = this;
			this.buttonClose.click(function(b) {
				if (this.audioActive) {
					a.audioActive = false;
					a.dispatch(Athena.AUDIO_STOP, this.audioReference);
					a.buttonAudio.removeClass("active");
				}
				a.hide();
			});
			this.buttonAudio.click(function(b) {
				a.handleAudio();
			});
		},
		render : function() {
			this.buttonClose = this.$el.find(".button-close");
			this.buttonAudio = this.$el.find(".button-audio");
			this.imagePanel = this.$el.find(".imageviewer-main");
			this.imageContainer = this.$el.find(".image-container");
			if (this.isDraggabele) {
				this.imagePanel.draggable();
			}
			this.setEvents();
		}
	});
	Athena.InfoView = Athena.CoreView.extend({
		el : "#panel-info",
		panelHeader : null,
		panelContent : null,
		panelCopyright : null,
		panelVersion : null,
		buttonClose : null,
		bindToElement : null,
		leftOffset : 0,
		initialize : function() {
			Athena.InfoView.__super__.initialize.call(this);
			this.register(Athena.INFO_HIDE, this.fadeOut, this);
			this.register(Athena.INFO_SHOW, this.fadeIn, this);
			this.register(Athena.INFO_UPDATE, this.update, this);
		},
		setEvents : function() {
			var a = this;
			this.buttonClose.click(function(b) {
				a.fadeOut();
				a.dispatch(Athena.INFO_CLOSED, true);
			});
		},
		fadeIn : function() {
			if (this.bindToElement && this.bindToElement !== undefined) {
				if (this.bindToElement.length > 0) {
					this.$el.css("left", this.bindToElement.position().left + this.leftOffset);
				}
			}
			Athena.InfoView.__super__.fadeIn.call(this);
		},
		update : function(c, b, a) {
			if (b === "") {
				if (this.$el.find("hr").length > 0) {
					this.$el.find("hr").hide();
				}
				this.panelHeader.empty().css("display", "none");
				this.panelContent.empty().css("display", "none");
			} else {
				if (this.$el.find("hr").length > 0) {
					this.$el.find("hr").show();
				}
				this.panelHeader.empty().html(c).css("display", "block");
				this.panelContent.empty().html(b).css("display", "block");
			}
			if (a !== undefined) {
				this.panelVersion.empty().html(a);
			}
		},
		setContent : function() {
			var c, a, b;
			if (Athena.dlolocaleModel.info) {
				c = (Athena.dlolocaleModel.info.infoTitle !== undefined) ? Athena.dlolocaleModel.info.infoTitle : "";
				a = (Athena.dlolocaleModel.info.infoBody !== undefined) ? Athena.dlolocaleModel.info.infoBody : "";
				b = (Athena.dlolocaleModel.info.infoBody !== undefined) ? Athena.dlolocaleModel.info.infoVersion : "";
				this.update(c, a, b);
			}
			this.panelCopyright.html(Athena.localeModel.get("copyright-text"));
		},
		render : function() {
			this.panelHeader = this.$el.find(".info-header");
			this.panelContent = this.$el.find(".content-detail");
			this.panelCopyright = this.$el.find(".copyright-detail");
			this.panelVersion = this.$el.find(".version-number");
			this.buttonClose = this.$el.find(".button-close");
			this.setEvents();
			this.setContent();
		}
	});
	Athena.InstructionalView = Athena.CoreDialogView.extend({
		buttonOpen : null,
		itemHeight : 0,
		panelType : "S",
		initialize : function() {
			this.register(Athena.APP_RESIZE, this.resize, this);
			this.register(Athena.INSTRUCTIONS_SHOW, this.fadeIn, this);
			this.register(Athena.INSTRUCTIONS_HIDE, this.hide, this);
			this.register(Athena.INSTRUCTIONS_UPDATE, this.update, this);
			this.panelType = (Athena.dlolocaleModel.options.instructionPanelType !== "undefined") ? Athena.dlolocaleModel.options.instructionPanelType : "S";
			Athena.InstructionalView.__super__.initialize.call(this);
		},
		resize : function() {
			if (this.isOpen) {
				this.itemHeight = this.contentContainer.outerHeight();
				this.$el.height(this.itemHeight);
			}
		},
		update : function(a) {
			if (a !== undefined) {
				if (a.content !== undefined) {
					this.contentContainer.html(a.content);
				}
				if (a.header !== undefined) {
					if (this.headerTitle) {
						this.headerTitle.html(a.header);
					}
				}
				if (this.buttonOpen !== null && a.content !== "" && a.header !== "") {
					this.buttonOpen.addClass("visible");
				} else {
					if (this.buttonOpen) {
						this.buttonOpen.removeClass("visible");
					}
				}
			}
		},
		show : function(c) {
			this.update(c);
			if (this.panelType === "D") {
				this.fadeIn();
			} else {
				if (this.panelType === "S") {
					this.itemHeight = this.contentContainer.outerHeight();
					var a = this, b = {
						height : this.itemHeight + "px"
					};
					Athena.transitions.animate(a.$el, b, 500, function() {});
				}
			}
		},
		hide : function() {
			if (this.panelType === "S") {
				var a = this, b = {
					height : "0px"
				};
				Athena.transitions.animate(a.$el, b, 500, function() {
					a.isOpen = false;
				});
			} else {
				if (this.panelType === "D") {
					this.fadeOut();
					this.isOpen = false;
				}
			}
		},
		checkStatus : function() {
			if (this.isOpen) {
				this.isOpen = false;
				this.hide();
			} else {
				this.isOpen = true;
				this.show();
			}
			this.setButton();
		},
		setButton : function() {
			if (this.isOpen) {
				this.buttonOpen.addClass("active").removeClass("inactive");
			} else {
				this.buttonOpen.addClass("inactive").removeClass("active");
			}
		},
		setEvents : function() {
			var a = this;
			if (this.panelType === "D") {
				if (this.buttonClose) {
					this.buttonClose.click(function(b) {
						a.fadeOut();
					});
				}
				if (this.buttonOpen) {
					this.buttonOpen.click(function(b) {
						a.fadeIn();
					});
				}
			} else {
				if (this.panelType === "S") {
					if (this.buttonOpen) {
						this.buttonOpen.click(function(b) {
							a.checkStatus();
						});
					}
				}
			}
		},
		render : function() {
			this.buttonOpen = $("#button-instructions");
			var a = this.buttonOpen.find("span.label");
			a.text(Athena.localeModel.get("instruction-button-text"));
			if (this.panelType === "D") {
				this.$el = $("#instructional-modal");
				Athena.ExtraContentView.__super__.render.call(this);
			} else {
				if (this.panelType === "S") {
					this.$el = $("#instructional-view");
					this.$el.addClass("visible");
					this.contentContainer = this.$el.find(".dialog-content");
					this.setEvents();
				}
			}
		}
	});
	Athena.HelpView = Athena.CoreDialogView.extend({
		currentPage : 1,
		initialize : function() {
			this.register(Athena.HELP_SHOW, this.fadeIn, this);
			this.register(Athena.HELP_HIDE, this.hide, this);
			this.register(Athena.HELP_UPDATE, this.update, this);
			Athena.HelpView.__super__.initialize.call(this);
		},
		setContent : function() {
			var b = Athena.localeModel.get("help-title"), a = Athena.localeModel.get("help-content");
			this.headerTitle.append(b);
			this.contentContainer.html(a);
		},
		setEvents : function() {
			var a = this;
			this.buttonClose.click(function(b) {
				a.dispatch(Athena.HELP_CLOSED);
				a.hide();
			});
		},
		render : function() {
			this.$el = $("#help-modal");
			Athena.HelpView.__super__.render.call(this);
			if (!Athena.HIDE_UI) {
				this.$el.addClass("visible");
			}
		}
	});
	Athena.MenuView = Athena.CoreMenuView.extend({
		initialize : function() {
			Athena.MenuView.__super__.initialize.call(this);
			this.register(Athena.MENU_CHANGE, this.updateMenu, this);
			this.register(Athena.MENU_STARTANIME, this.startAnimation, this);
			this.register(Athena.MENU_STOPANIME, this.stopAnimation, this);
		},
		generate : function() {
			var c, e, g, b, f, a, d = this;
			b = this.$el.find("#default-activity");
			f = this.$el.find(".start");
			_.each(Athena.dlolocaleModel.models, function(h) {
				if (h.attributes.parentId === -1) {
					c = b.clone().insertBefore(f);
					c.removeAttr("id").removeClass("start");
					e = c.find("button");
					g = e.find("span.label");
					e.attr("id", "menu-" + h.attributes.id);
					if (Athena.dlolocaleModel.options.useMainMenuText) {
						if (h.attributes.contents.title.value === undefined) {
							g.append(h.attributes.contents.title);
						} else {
							g.append(h.attributes.contents.title.value);
						}
					} else {
						g.append(h.attributes.id + 1);
					}
					if (h.attributes.id === 0) {
						e.addClass("visited").addClass("active");
					}
				}
			});
			b.remove();
			this.setEvents();
			this.$el.addClass("visible");
		},
		setEvents : function() {
			var a = this;
			this.$el.find(".menu-item").click(function(b) {
				if ($(this).is(":not(.active)")) {
					a.handleItemClick(b);
				}
			});
			this.buttonLeft.click(function(b) {
				if ($(this).is(":not(.disabled)")) {
					a.handleArrowClick(false);
				}
			});
			this.buttonRight.click(function(b) {
				if ($(this).is(":not(.disabled)")) {
					a.handleArrowClick(true);
				}
			});
		},
		handleItemClick : function(d) {
			this.dispatch(Athena.AUDIO_STOP);
			this.dispatch(Athena.ACTIVITY_DISABLE, Athena.dlolocaleModel.selectedActivity);
			this.dispatch(Athena.IMAGE_HIDE);
			this.dispatch(Athena.EXCONTENT_HIDE);
			var c, b, a;
			c = d.currentTarget;
			b = $(c).attr("id").split("-")[1];
			Athena.dlolocaleModel.setSelected(Number(b));
			this.dispatch(Athena.ACTIVITY_CHANGED, 1);
			this.dispatch(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
			this.updateMenu();
			this.checkStates();
			this.dispatch(Athena.MENUARROW_UPDATE);
		},
		handleArrowClick : function(b) {
			this.dispatch(Athena.AUDIO_STOP);
			this.dispatch(Athena.ACTIVITY_DISABLE, Athena.dlolocaleModel.selectedActivity);
			this.dispatch(Athena.IMAGE_HIDE);
			this.dispatch(Athena.EXCONTENT_HIDE);
			var a = Athena.dlolocaleModel.getNextParent(b, Athena.dlolocaleModel.selectedActivity);
			if (a !== -1) {
				Athena.dlolocaleModel.setSelected(a);
				this.updateMenu();
				this.dispatch(Athena.ACTIVITY_CHANGED, 1);
				this.dispatch(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
			}
			this.updateMenu();
			this.dispatch(Athena.ACTIVITY_CHANGED, 1);
			this.dispatch(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
			this.dispatch(Athena.MENUARROW_UPDATE);
		},
		updateMenu : function() {
			var b, a;
			b = Athena.dlolocaleModel.getByCountId(Athena.dlolocaleModel.selectedActivity);
			a = b.attributes.parentId;
			if (a === -1) {
				$(".menu-item").removeClass("active");
				$("#menu-" + Athena.dlolocaleModel.selectedActivity).addClass("active").addClass("visited");
			}
			this.checkStates();
		},
		checkStates : function() {
			var a, b;
			this.stopAnimation();
			b = Athena.dlolocaleModel.getNextParent(false, Number(Athena.dlolocaleModel.selectedActivity));
			if (b === -1) {
				this.buttonLeft.addClass("disabled");
			} else {
				this.buttonLeft.removeClass("disabled");
			}
			a = Athena.dlolocaleModel.getNextParent(true, Number(Athena.dlolocaleModel.selectedActivity));
			if (a === -1) {
				this.buttonRight.addClass("disabled");
			} else {
				this.buttonRight.removeClass("disabled");
			}
		},
		render : function() {
			this.$el = $("#activity-menu");
			this.buttonLeft = this.$el.find("#menu-arrow-left");
			this.buttonRight = this.$el.find("#menu-arrow-right");
			this.overlayLeft = this.buttonLeft.find(".glow");
			this.overlayRight = this.buttonRight.find(".glow");
			this.generate();
		}
	});
	Athena.MenuSubView = Athena.CoreView.extend({
		defaultItem : null,
		listView : null,
		menuCount : 0,
		isSteps : false,
		initialize : function() {
			Athena.MenuSubView.__super__.initialize.call(this);
			this.register(Athena.APP_RESIZE, this.resize, this);
			this.register(Athena.MENUSUB_CHANGE, this.updateMenu, this);
			this.register(Athena.MENUSUB_SET, this.generate, this);
			this.register(Athena.MENUSUB_DESTROY, this.destroy, this);
			this.register(Athena.MENUSUB_ENABLE, this.setEvents, this);
			this.register(Athena.MENUSUB_DISABLE, this.clearEvents, this);
		},
		resize : function(a) {},
		generate : function(f, d) {
			var c, b = this, a = 0, e = 0;
			this.menuCount = 0;
			this.destroy();
			if (d !== undefined || d === true) {
				this.isSteps = true;
				e = 1;
				for (c = 1; c <= f; c++) {
					b.createItem(e, e, 1, f);
					e++;
				}
			} else {
				this.isSteps = false;
				_.each(Athena.dlolocaleModel.models, function(g) {
					if (g.attributes.id === f || g.attributes.parentId === f) {
						a++;
					}
				});
				a--;
				_.each(Athena.dlolocaleModel.models, function(g) {
					if (g.attributes.id === f || g.attributes.parentId === f) {
						e++;
						b.createItem(g.attributes.id, e, f, a);
						b.menuCount++;
					}
				});
			}
			this.setEvents();
			this.$el.addClass("visible");
		},
		createItem : function(h, f, g, a) {
			var c, b, e, d;
			c = this.defaultItem.clone();
			c.attr("id", "sub-menu-" + h);
			this.listView.append(c);
			c.show();
			b = c.find("button");
			if (e !== "undefined" && Athena.dlolocaleModel.options.useSubTextValuesStart) {
				f = f - 1;
			}
			if (b.length > 0) {
				if (h === g) {
					e = Athena.localeModel.get("sub-nav-start-text");
					b.addClass("active");
					if (e !== "undefined" && Athena.dlolocaleModel.options.useSubTextValuesStart) {
						b.append(e);
						b.addClass("sub-nav-type1");
					} else {
						b.append(f);
						b.addClass("sub-nav-type2");
					}
				} else {
					if (h === a) {
						d = Athena.localeModel.get("sub-nav-end-text");
						if (d !== "undefined" && Athena.dlolocaleModel.options.useSubTextValuesEnd) {
							b.append(d);
							b.addClass("sub-nav-type1");
						} else {
							b.append(f);
							b.addClass("sub-nav-type2");
						}
					} else {
						b.append(f);
						b.addClass("sub-nav-type2");
					}
				}
			} else {
				c.append(f);
			}
		},
		handleItemClick : function(c) {
			this.dispatch(Athena.AUDIO_STOP);
			this.dispatch(Athena.IMAGE_HIDE);
			this.dispatch(Athena.EXCONTENT_HIDE);
			var b, a;
			b = c.currentTarget;
			a = Number($(b).attr("id").split("-")[2]);
			if (this.isSteps) {
				this.dispatch(Athena.ACTIVITY_UPDATESTEP, Athena.dlolocaleModel.selectedActivity, a);
				$(".sub-menu-parent").find("button").removeClass("active");
				$(b).find("button").addClass("active");
				Athena.dlolocaleModel.selectedActivityStep = a;
				this.dispatch(Athena.MENUARROW_UPDATE);
			} else {
				this.dispatch(Athena.ACTIVITY_DISABLE, Athena.dlolocaleModel.selectedActivity);
				Athena.dlolocaleModel.setSelected(a);
				this.dispatch(Athena.ACTIVITY_CHANGED, 1);
				$(".sub-menu-parent").find("button").removeClass("active");
				$("#sub-menu-" + Athena.dlolocaleModel.selectedActivity).find("button").addClass("active");
				this.dispatch(Athena.MENUARROW_UPDATE);
			}
		},
		updateMenu : function(a) {
			$(".sub-menu-parent").find("button").removeClass("active");
			if (this.isSteps) {
				$("#sub-menu-" + a).find("button").addClass("active");
			} else {
				$("#sub-menu-" + Athena.dlolocaleModel.selectedActivity).find("button").addClass("active");
			}
		},
		destroy : function() {
			this.clearEvents();
			this.listView.empty();
			this.$el.removeClass("visible");
		},
		setEvents : function() {
			var a = this;
			$(".activity-sub-menu li").click(function(b) {
				if ($(this).find("button").is(":not(.active)")) {
					a.handleItemClick(b);
				} else {
					debug("SubMenuClick : Activity already loaded.");
				}
			});
		},
		clearEvents : function() {
			$(".activity-sub-menu li").unbind("click");
		},
		render : function() {
			this.$el = $("#activity-sub-menu-container");
			this.defaultItem = $("#default-sub-menu-element");
			this.listView = this.$el.find(".activity-sub-menu");
			this.defaultItem.hide();
		}
	});
	Athena.MenuArrowView = Athena.CoreMenuView.extend({
		menuType : 1,
		initialize : function() {
			Athena.MenuArrowView.__super__.initialize.call(this);
			this.register(Athena.MENUARROW_ENABLE, this.setEvents, this);
			this.register(Athena.MENUARROW_DISABLE, this.clearEvents, this);
			this.register(Athena.MENUARROW_UPDATE, this.update, this);
			this.register(Athena.MENUARROW_STARTANIME, this.startAnimation, this);
			this.register(Athena.MENUARROW_STOPANIME, this.stopAnimation, this);
			this.menuType = Number(Athena.dlolocaleModel.options.menuArrowType);
		},
		handleLeftArrowClick : function(b) {
			if (this.menuType === 1) {
				if (Athena.dlolocaleModel.selectedActivity > 0) {
					this.dispatch(Athena.AUDIO_STOP, Athena.dlolocaleModel.selectedActivity);
					this.dispatch(Athena.ACTIVITY_DISABLE, Athena.dlolocaleModel.selectedActivity);
					Athena.dlolocaleModel.setSelected(Athena.dlolocaleModel.selectedActivity - 1);
					this.dispatch(Athena.ACTIVITY_CHANGED, 1);
					this.dispatch(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
					this.dispatch(Athena.MENU_CHANGE);
					this.dispatch(Athena.MENUSUB_CHANGE);
				}
			} else {
				if (this.menuType === 2) {
					if (Athena.dlolocaleModel.selectedActivityStep > 1) {
						Athena.dlolocaleModel.selectedActivityStep = Athena.dlolocaleModel.selectedActivityStep - 1;
						this.dispatch(Athena.ACTIVITY_UPDATESTEP, Athena.dlolocaleModel.selectedActivity, Athena.dlolocaleModel.selectedActivityStep);
						this.dispatch(Athena.MENUSUB_CHANGE, Athena.dlolocaleModel.selectedActivityStep);
					} else {
						if (Athena.dlolocaleModel.selectedActivityStep === 1) {
							var a = Athena.dlolocaleModel.getNextParent(false, Number(Athena.dlolocaleModel.selectedActivity));
							if (a !== -1) {
								Athena.dlolocaleModel.setSelected(Athena.dlolocaleModel.selectedActivity - 1);
								this.dispatch(Athena.ACTIVITY_CHANGED, 1);
								this.dispatch(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
								this.dispatch(Athena.MENU_CHANGE);
								Athena.dlolocaleModel.selectedActivityStep = Athena.dlolocaleModel.totalActivitySteps;
								this.dispatch(Athena.MENUSUB_CHANGE, Athena.dlolocaleModel.totalActivitySteps);
							}
						}
					}
				}
			}
			this.setStatus();
		},
		handleRightArrowClick : function(b) {
			if (this.menuType === 1) {
				if (Athena.dlolocaleModel.selectedActivity < Athena.dlolocaleModel.totalActivities - 1) {
					this.dispatch(Athena.AUDIO_STOP, Athena.dlolocaleModel.selectedActivity);
					this.dispatch(Athena.ACTIVITY_DISABLE, Athena.dlolocaleModel.selectedActivity);
					Athena.dlolocaleModel.setSelected(Athena.dlolocaleModel.selectedActivity + 1);
					this.dispatch(Athena.ACTIVITY_CHANGED, 1);
					this.dispatch(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
					this.dispatch(Athena.MENU_CHANGE);
					this.dispatch(Athena.MENUSUB_CHANGE);
				}
			} else {
				if (this.menuType === 2) {
					if (Athena.dlolocaleModel.selectedActivityStep < Athena.dlolocaleModel.totalActivitySteps) {
						Athena.dlolocaleModel.selectedActivityStep = Athena.dlolocaleModel.selectedActivityStep + 1;
						this.dispatch(Athena.ACTIVITY_UPDATESTEP, Athena.dlolocaleModel.selectedActivity, Athena.dlolocaleModel.selectedActivityStep);
						this.dispatch(Athena.MENUSUB_CHANGE, Athena.dlolocaleModel.selectedActivityStep);
					} else {
						if (Athena.dlolocaleModel.selectedActivityStep === Athena.dlolocaleModel.totalActivitySteps) {
							var a = Athena.dlolocaleModel.getNextParent(true, Number(Athena.dlolocaleModel.selectedActivity));
							if (a !== -1) {
								Athena.dlolocaleModel.setSelected(Athena.dlolocaleModel.selectedActivity + 1);
								this.dispatch(Athena.ACTIVITY_CHANGED, 1);
								this.dispatch(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
								this.dispatch(Athena.MENU_CHANGE);
							}
						}
					}
				}
			}
			this.setStatus();
		},
		setEvents : function() {
			var a = this;
			this.buttonLeft.click(function(b) {
				if ($(this).is(":not(.disabled)")) {
					a.handleLeftArrowClick(b);
				}
			});
			this.buttonRight.click(function(b) {
				if ($(this).is(":not(.disabled)")) {
					a.handleRightArrowClick(b);
				}
			});
		},
		clearEvents : function() {
			var a = this;
			this.buttonLeft.unbind("click");
			this.buttonRight.unbind("click");
		},
		update : function() {
			this.setStatus();
		},
		setStatus : function() {
			var b, c, a = this;
			this.stopAnimation();
			if (this.menuType === 1) {
				if (Athena.dlolocaleModel.selectedActivity > 0) {
					this.buttonLeft.removeClass("disabled");
				} else {
					this.buttonLeft.addClass("disabled");
				}
				if (Athena.dlolocaleModel.selectedActivity < Athena.dlolocaleModel.totalActivities - 1) {
					this.buttonRight.removeClass("disabled");
				} else {
					this.buttonRight.addClass("disabled");
				}
			} else {
				if (this.menuType === 2) {
					c = Athena.dlolocaleModel.getNextParent(false, Number(Athena.dlolocaleModel.selectedActivity));
					if (c === -1) {
						if (Athena.dlolocaleModel.selectedActivityStep > 1) {
							this.buttonLeft.removeClass("disabled");
						} else {
							this.buttonLeft.addClass("disabled");
						}
					} else {
						this.buttonLeft.removeClass("disabled");
					}
					b = Athena.dlolocaleModel.getNextParent(true, Number(Athena.dlolocaleModel.selectedActivity));
					if (b === -1) {
						if (Athena.dlolocaleModel.selectedActivityStep < Athena.dlolocaleModel.totalActivitySteps) {
							this.buttonRight.removeClass("disabled");
						} else {
							this.buttonRight.addClass("disabled");
						}
					} else {
						this.buttonRight.removeClass("disabled");
					}
				}
			}
		},
		render : function() {
			this.$el = $("#activity-prev-next-menu");
			this.buttonLeft = this.$el.find("#menu-left-arrow");
			this.buttonRight = this.$el.find("#menu-right-arrow");
			this.overlayLeft = this.buttonLeft.find(".glow");
			this.overlayRight = this.buttonRight.find(".glow");
			this.setEvents();
			this.show();
		}
	});
	Athena.PreloaderView = Athena.CoreView.extend({
		el : "#preload-modal",
		bar : null,
		status : null,
		initialize : function() {
			Athena.PreloaderView.__super__.initialize.call(this);
			this.register(Athena.PRELOADER_HIDE, this.fadeOut, this);
			this.register(Athena.PRELOADER_SHOW, this.fadeIn, this);
			this.register(Athena.PRELOADER_UPDATE, this.update, this);
		},
		update : function(b, a) {
			this.status.text(b);
			this.bar.css("width", a + "%");
		},
		render : function() {
			this.bar = this.$el.find(".preloader-bar .inner");
			this.status = this.$el.find(".preloader-status");
		}
	});
	Athena.ResourcesView = Athena.CoreView.extend({
		el : "#panel-resources",
		initialize : function() {
			Athena.ResourcesView.__super__.initialize.call(this);
			this.register(Athena.RESOURCES_HIDE, this.fadeOut, this);
			this.register(Athena.RESOURCES_SHOW, this.fadeIn, this);
		}
	});
	Athena.SuccessView = Athena.CoreDialogView.extend({
		initialize : function() {
			Athena.SuccessView.__super__.initialize.call(this);
			this.register(Athena.SUCCESS_SHOW, this.show, this);
			this.register(Athena.SUCCESS_HIDE, this.hide, this);
			this.register(Athena.SUCCESS_UPDATE, this.update, this);
		},
		show : function(a, b) {
			this.update(a, b);
			Athena.SuccessView.__super__.show.call(this);
		},
		render : function() {
			this.$el = $("#success-modal");
			Athena.SuccessView.__super__.render.call(this);
		}
	});
	Athena.ScoreView = Athena.CoreView.extend({
		initialize : function() {
			Athena.ScoreView.__super__.initialize.call(this);
			this.register(Athena.SCORE_UDPATE, this.update, this);
		},
		update : function(a) {},
		render : function() {
			this.$el = $("#item-score");
			this.$el.addClass("visible");
		}
	});
	Athena.SideMenuView = Athena.CoreSettingsView.extend({
		buttonToggle : null,
		menuContent : null,
		menuWidth : 0,
		isOpen : false,
		directionLeft : true,
		initialize : function() {
			Athena.SideMenuView.__super__.initialize.call(this);
			this.register(Athena.SIDEMENU_HIDE, this.togglePanel, this);
			this.register(Athena.SIDEMENU_SHOW, this.togglePanel, this);
		},
		togglePanel : function() {
			if (this.isOpen) {
				this.slideOut();
				this.buttonToggle.removeClass("active");
				this.menuContent.removeClass("active");
			} else {
				this.slideIn();
				this.buttonToggle.addClass("active");
				this.menuContent.addClass("active");
			}
		},
		slideIn : function() {
			var a;
			this.isOpen = true;
			if (this.directionLeft) {
				a = {
					left : "0px"
				};
			} else {
				a = {
					right : "0px"
				};
			}
			Athena.transitions.animate(this.$el, a, this.animeTime2, null, this.easing2);
		},
		slideOut : function() {
			var b, a = this;
			this.isOpen = false;
			if (this.directionLeft) {
				b = {
					left : -this.menuWidth
				};
			} else {
				b = {
					right : -this.menuWidth
				};
			}
			Athena.transitions.animate(this.$el, b, this.animeTime2, function() {}, this.easing2);
		},
		setEvents : function() {
			var a = this;
			if (this.buttonToggle) {
				this.buttonToggle.click(function() {
					a.togglePanel();
				});
			}
		},
		render : function() {
			this.$el = $("#side-menu-bar");
			this.buttonToggle = $("#side-menu-button");
			this.menuContent = this.$el.find(".side-menu-content");
			if (!Athena.HIDE_UI) {
				this.$el.addClass("visible");
				this.buttonToggle.addClass("visible");
			}
			this.menuWidth = this.menuContent.outerWidth();
			this.setVisibleButtons();
			this.setEvents();
		}
	});
	Athena.ShellContainerView = Athena.CoreShellContainerView.extend({
		shellTitle : null,
		shellSubTitle : null,
		shellIntro : null,
		headerOpen : true,
		headerHeightOpen : 0,
		headerHeightClosed : 0,
		moduleTopOpen : 0,
		moduleTopClosed : 0,
		initialize : function() {
			Athena.ShellContainerView.__super__.initialize.call(this);
			this.register(Athena.UPDATE_SHELLTEXT, this.updateText, this);
			this.register(Athena.ACTIVITY_CHANGED, this.updateText, this);
			this.register(Athena.TOGGLE_UI, this.toggle, this);
			this.register(Athena.TOGGLE_HEADER, this.toggleHeader, this);
		},
		initShell : function() {
			this.initSubMenu();
			if (Athena.settingsModel.deeplink) {
				Athena.dlolocaleModel.selectedActivity = Number(Athena.settingsModel.deeplink);
			} else {
				Athena.dlolocaleModel.setSelected(0);
			}
			this.initSettingsView();
			this.initMainMenu();
			this.initArrowMenu();
			this.initSideMenu();
			this.initInstructionalView();
			this.initSuccesView();
			this.initScoreView();
			this.initInfoView();
			this.initImageView();
			this.initCaptionsView();
			this.initToolsView();
			this.initGlossaryView();
			this.initResourcesView();
			this.initExtraContentView();
			this.initHelpView();
			this.initAudioView();
			this.initActivitiesView();
			this.initCollapsibleView();
			this.updateText();
			this.activityView.resize();
		},
		activateShell : function() {
			var a = Athena.settingsModel.get("auto-render-activities");
			if (a !== undefined && a) {
				if (Athena.dlolocaleModel.options.autoRender !== undefined && Athena.dlolocaleModel.options.autoRender) {
					this.dispatch(Athena.ACTIVITY_START, -100);
				} else {
					this.dispatch(Athena.ACTIVITY_START, Athena.dlolocaleModel.selectedActivity);
				}
			} else {
				this.dispatch(Athena.ACTIVITY_START, Athena.dlolocaleModel.selectedActivity);
			}
			this.dispatch(Athena.ACTIVITY_VISITED, Athena.dlolocaleModel.selectedActivity);
			if (Athena.settingsModel.deeplink) {
				this.dispatch(Athena.ACTIVITY_SWITCH, 1);
				this.eventBus.trigger(Athena.MENUARROW_UPDATE);
				this.eventBus.trigger(Athena.MENU_CHANGE);
				this.eventBus.trigger(Athena.MENUSUB_CHANGE);
			}
		},
		resize : function() {},
		tab : function() {},
		initSplashView : function() {
			this.splashView = new Athena.SplashView();
			this.splashView.render();
		},
		initActivitiesView : function() {
			this.activityView = new Athena.CoreActivitiesView();
			this.activityView.generate(this.ahtml);
		},
		initMainMenu : function() {
			if (Athena.dlolocaleModel.options.hasMenuActivity !== undefined) {
				if (Athena.dlolocaleModel.options.hasMenuActivity) {
					this.menuView = new Athena.MenuView();
				}
			}
		},
		initSubMenu : function() {
			if (Athena.dlolocaleModel.options.hasMenuSub !== undefined) {
				if (Athena.dlolocaleModel.options.hasMenuSub) {
					this.subMenuView = new Athena.MenuSubView();
				}
			}
		},
		initArrowMenu : function() {
			if (Athena.dlolocaleModel.options.hasMenuArrows !== undefined) {
				if (Athena.dlolocaleModel.options.hasMenuArrows) {
					this.arrowMenuView = new Athena.MenuArrowView();
				}
			}
		},
		initSideMenu : function() {
			if (Athena.dlolocaleModel.options.hasMenuSide !== undefined) {
				if (Athena.dlolocaleModel.options.hasMenuSide) {
					this.arrowMenuView = new Athena.SideMenuView();
				}
			}
		},
		initInstructionalView : function() {
			if (Athena.dlolocaleModel.options.hasInstructionArea !== undefined) {
				if (Athena.dlolocaleModel.options.hasInstructionArea) {
					this.instructionalView = new Athena.InstructionalView();
				}
			}
		},
		initSuccesView : function() {
			if (Athena.dlolocaleModel.options.hasSuccessView !== undefined) {
				if (Athena.dlolocaleModel.options.hasSuccessView) {
					this.instructionalView = new Athena.SuccessView();
				}
			}
		},
		initSettingsView : function() {
			if (Athena.dlolocaleModel.options.hasMenuSettings !== undefined) {
				if (Athena.dlolocaleModel.options.hasMenuSettings) {
					this.settingsView = new Athena.CoreSettingsView();
				}
			}
		},
		initScoreView : function() {
			if (Athena.dlolocaleModel.options.hasShellScore !== undefined) {
				if (Athena.dlolocaleModel.options.hasShellScore) {
					this.scoreView = new Athena.ScoreView();
				}
			}
		},
		initInfoView : function() {
			if (Athena.dlolocaleModel.options.hasInfo !== undefined) {
				if (Athena.dlolocaleModel.options.hasInfo) {
					this.infoView = new Athena.InfoView();
					this.infoView.bindToElement = this.settingsView.buttonInfo;
				}
			}
		},
		initImageView : function() {
			if (Athena.dlolocaleModel.options.hasImageView !== undefined) {
				if (Athena.dlolocaleModel.options.hasImageView) {
					this.imageView = new Athena.ImageView();
				}
			}
		},
		initCaptionsView : function() {
			if (Athena.dlolocaleModel.options.hasCC !== undefined) {
				if (Athena.dlolocaleModel.options.hasCC) {
					this.closedCaptionView = new Athena.ClosedCaptionView();
				}
			}
		},
		initToolsView : function() {
			if (Athena.dlolocaleModel.options.hasTools !== undefined) {
				if (Athena.dlolocaleModel.options.hasTools) {
					this.toolsView = new Athena.ToolsView();
				}
			}
		},
		initGlossaryView : function() {
			if (Athena.dlolocaleModel.options.hasGlossary !== undefined) {
				if (Athena.dlolocaleModel.options.hasGlossary) {
					this.glossaryView = new Athena.GlossaryView();
				}
			}
		},
		initResourcesView : function() {
			if (Athena.dlolocaleModel.options.hasResources !== undefined) {
				if (Athena.dlolocaleModel.options.hasResources) {
					this.resourcesView = new Athena.ResourcesView();
				}
			}
		},
		initExtraContentView : function() {
			if (Athena.dlolocaleModel.options.hasExtraContent !== undefined) {
				if (Athena.dlolocaleModel.options.hasExtraContent) {
					this.extraContentView = new Athena.ExtraContentView();
				}
			}
		},
		initHelpView : function() {
			if (Athena.dlolocaleModel.options.hasHelp !== undefined) {
				if (Athena.dlolocaleModel.options.hasHelp) {
					this.helpView = new Athena.HelpView();
				}
			}
		},
		initAudioView : function() {
			if (Athena.dlolocaleModel.options.hasAudio !== undefined) {
				if (Athena.dlolocaleModel.options.hasAudio) {
					this.audioView = new Athena.AudioView();
					this.audioView.bindToElement = this.settingsView.buttonAudio;
				}
			}
		},
		initCollapsibleView : function() {
			var c = $("header.main .content-bottom"), b = $("#module-container"), a = this;
			this.headerHeightOpen = c.height();
			this.moduleTopOpen = b.position().top;
			this.headerHeightClosed = Number(Athena.settingsModel.get("header-close-height"));
			if (this.headerHeightOpen === this.headerContainer.height()) {
				this.moduleTopClosed = this.headerHeightClosed;
			} else {
				if (this.headerContainer.height() > this.headerHeightOpen || this.headerContainer.height() < this.headerHeightOpen) {
					this.moduleTopClosed = Math.ceil(this.headerHeightClosed + (this.headerContainer.height() - this.headerHeightOpen));
					if (Athena.settingsModel.get("module-close-top") !== "undefined") {
						if (!Athena.HIDE_UI) {
							if (!Athena.HIDE_HEADER) {
								this.moduleTopClosed = Number(Athena.settingsModel.get("module-close-top"));
							} else {
								this.moduleTopClosed = 0;
							}
						} else {
							this.moduleTopClosed = 0;
						}
					}
				}
			}
			if (Athena.dlolocaleModel.options.collapsibleHeader !== undefined && Athena.dlolocaleModel.options.collapsibleHeader) {
				this.buttonCollapse = $("#button-collapse");
				this.buttonCollapse.addClass("visible");
				this.buttonCollapse.addClass("close");
				this.buttonCollapse.find("span.label").html(Athena.localeModel.get("collapse-button-close"));
				this.buttonCollapse.click(function() {
					a.toggleHeader(false);
				});
				if (Athena.settingsModel.get("header-auto-close")) {
					a.headerContainer.addClass("close").removeClass("open");
					a.buttonCollapse.addClass("open").removeClass("close");
					c.height(a.headerHeightClosed);
					b.css({
						top : Number(a.moduleTopClosed)
					});
					a.headerOpen = false;
				} else {
					a.buttonCollapse.addClass("close").removeClass("open");
					a.headerContainer.addClass("open").removeClass("close");
					if (Athena.settingsModel.get("static-module-top")) {
						$("#module-container-overlay").css({
							display : "block"
						});
					}
				}
			}
		},
		toggleHeader : function(b) {
			var c = $("header.main .content-bottom"), a = $("#module-container");
			if (this.headerOpen && b === false) {
				this.buttonCollapse.addClass("open").removeClass("close");
				this.headerContainer.addClass("close").removeClass("open");
				this.headerOpen = false;
				Athena.transitions.animate(c, {
					height : this.headerHeightClosed
				}, this.animeTime2, function() {});
				if (!Athena.settingsModel.get("static-module-top")) {
					Athena.transitions.animate(a, {
						top : this.moduleTopClosed
					}, this.animeTime2);
				} else {
					$("#module-container-overlay").css({
						display : "none"
					});
				}
			} else {
				if ((!this.headerOpen)) {
					this.buttonCollapse.addClass("close").removeClass("open");
					this.headerOpen = true;
					this.headerContainer.addClass("open").removeClass("close");
					Athena.transitions.animate(c, {
						height : this.headerHeightOpen
					}, this.animeTime2, function() {});
					if (!Athena.settingsModel.get("static-module-top")) {
						Athena.transitions.animate(a, {
							top : this.moduleTopOpen
						}, this.animeTime2);
					} else {
						$("#module-container-overlay").css({
							display : "block",
							opacity : 0
						});
						Athena.transitions.animate($("#module-container-overlay"), {
							opacity : 1
						}, this.animeTime1);
					}
				}
			}
		},
		updateText : function() {
			if (Athena.dlolocaleModel.options.updateHeaders !== undefined && Athena.dlolocaleModel.options.updateHeaders) {
				var d, c, a, b = Athena.dlolocaleModel.getByCountId(Athena.dlolocaleModel.selectedActivity);
				d = (b.attributes.contents.title.value !== undefined) ? b.attributes.contents.title.value : b.attributes.contents.title;
				c = (b.attributes.contents.subtitle.value !== undefined) ? b.attributes.contents.subtitle.value : b.attributes.contents.subtitle;
				a = (b.attributes.contents.intro.value !== undefined) ? b.attributes.contents.intro.value : b.attributes.contents.intro;
				if (Athena.dlolocaleModel.shell.title !== undefined && Athena.dlolocaleModel.shell.title !== "") {
					if (this.shellTitle) {
						this.shellTitle.html(Athena.dlolocaleModel.shell.title);
					}
				} else {
					if (d !== undefined && d !== "") {
						if (this.shellTitle) {
							this.shellTitle.html(d);
						}
					}
				}
				if (Athena.dlolocaleModel.shell.subtitle !== undefined && Athena.dlolocaleModel.shell.subtitle !== "") {
					if (this.shellSubTitle) {
						this.shellSubTitle.html(Athena.dlolocaleModel.shell.subtitle);
					}
				} else {
					if (c !== undefined && c !== "") {
						if (this.shellSubTitle) {
							this.shellSubTitle.html(c);
						}
					}
				}
				if (Athena.dlolocaleModel.shell.intro !== undefined && Athena.dlolocaleModel.shell.intro !== "") {
					if (this.shellIntro) {
						this.shellIntro.html(Athena.dlolocaleModel.shell.intro);
					}
				} else {
					if (a !== undefined && a !== "") {
						if (this.shellIntro) {
							this.shellIntro.html(a);
						}
					}
				}
			}
		},
		toggle : function() {
			if (Athena.HIDE_UI) {
				Athena.HIDE_UI = false;
			} else {
				if (!Athena.HIDE_UI) {
					Athena.HIDE_UI = true;
				}
			}
			this.check();
		},
		check : function() {
			if (Athena.HIDE_UI) {
				if (this.headerContainer) {
					this.headerContainer.removeClass("visible");
				}
				if (this.footerContainer) {
					this.footerContainer.removeClass("visible");
				}
			} else {
				if (Athena.HIDE_HEADER) {
					if (this.headerContainer) {
						this.headerContainer.removeClass("visible");
					}
				} else {
					if (Athena.dlolocaleModel.options.hasHeader !== undefined && Athena.dlolocaleModel.options.hasHeader) {
						if (this.headerContainer) {
							this.headerContainer.addClass("visible");
						}
					}
				}
				if (Athena.HIDE_FOOTER) {
					if (this.footerContainer) {
						this.footerContainer.removeClass("visible");
					}
				} else {
					if (Athena.dlolocaleModel.options.hasFooter !== undefined && Athena.dlolocaleModel.options.hasFooter) {
						if (this.footerContainer) {
							this.footerContainer.addClass("visible");
						}
					}
				}
			}
		},
		render : function() {
			Athena.ShellContainerView.__super__.render.call(this);
			this.headerContainer = this.$el.find("header.main");
			this.footerContainer = this.$el.find("footer.main");
			this.shellTitle = $("#shell-title");
			this.shellSubTitle = $("#shell-subtitle");
			this.shellIntro = $("#shell-intro");
			this.initSplashView();
			this.check();
			var a = this, b = Athena.settingsModel.get("preload-splash");
			if (b) {
				this.dispatch(Athena.PRELOADER_HIDE);
			}
		}
	});
	Athena.SplashView = Athena.CoreView.extend({
		title : null,
		subTitle : null,
		intro : null,
		image : null,
		buttonEnter : null,
		updateHeader : false,
		initialize : function() {
			this.register(Athena.APP_RESIZE, this.resize, this);
		},
		setEvents : function() {
			var a = this;
			this.buttonEnter.click(function(b) {
				$(this).unbind("click");
				a.handleEnterClicked();
			});
		},
		handleEnterClicked : function() {
			this.hide();
		},
		resize : function() {},
		hide : function() {
			this.dispatch(Athena.PRELOADER_HIDE);
			var b = this, c, a = true;
			if (Athena.dlolocaleModel.options.splashTransition === "vertical") {
				c = {
					margin : "-" + Athena.WINDOW_HEIGHT + "px 0 0 0"
				};
			} else {
				if (Athena.dlolocaleModel.options.splashTransition === "horizontal") {
					c = {
						margin : "0 0 0 -" + Athena.WINDOW_WIDTH + "px"
					};
				} else {
					if (Athena.dlolocaleModel.options.splashTransition === "overlay") {
						c = {
							opacity : 0
						};
						a = false;
						Athena.shellView.$el.css({
							opacity : 0,
							display : "block"
						});
						this.dispatch(Athena.SHELL_INIT);
						if (Athena.settingsModel.get("auto-init-shell")) {
							b.dispatch(Athena.SHELL_ACTIVATE);
						}
						Athena.transitions.animate(Athena.shellView.$el, {
							opacity : 1
						}, this.animeTime1, function() {
							b.destroy();
							Athena.shellView.$el.css({
								position : "relative",
								"float" : "none"
							});
							if (!Athena.settingsModel.get("auto-init-shell")) {
								b.dispatch(Athena.SHELL_ACTIVATE);
							}
						});
						Athena.transitions.animate(b.$el, {
							opacity : 0
						}, this.animeTime1, function() {});
					}
				}
			}
			if (a) {
				if (!Athena.settingsModel.get("auto-init-shell")) {
					this.dispatch(Athena.SHELL_INIT);
				}
				Athena.transitions.animate(this.$el, c, this.animeTime1, function() {
					$("#wrapper").css({
						width : "100%"
					});
					b.destroy();
					if (!Athena.settingsModel.get("auto-init-shell")) {
						b.dispatch(Athena.SHELL_ACTIVATE);
					}
				}, 5);
			}
		},
		destroy : function() {
			this.$el.remove();
			$("#wrapper").css({
				width : "100%"
			});
			$(".shell-box-modal").css({
				width : "100%"
			});
			$(".shell-box-modal-inner").css({
				width : "100%"
			});
			$("section.shell").css({
				width : "100%"
			});
			if (this.updateHeader) {
				$("header.main").css({
					width : "100%"
				});
			}
		},
		end : function() {
			this.destroy();
			this.dispatch(Athena.SHELL_INIT);
			this.dispatch(Athena.SHELL_ACTIVATE);
		},
		animateElements : function(a) {},
		setContent : function() {
			if (this.title) {
				this.title.html(Athena.dlolocaleModel.splash.title);
			}
			if (this.subTitle) {
				this.subTitle.html(Athena.dlolocaleModel.splash.subTitle);
			}
			if (this.intro) {
				this.intro.html(Athena.dlolocaleModel.splash.intro);
			}
			if (this.buttonEnter) {
				if (Athena.dlolocaleModel.splash.enter !== undefined) {
					this.buttonEnter.find("span.label").html(Athena.dlolocaleModel.splash.enter);
				}
			}
			if (this.image) {
				if (Athena.dlolocaleModel.splash.background !== undefined) {
					if (Athena.dlolocaleModel.splash.background !== "") {
						this.image.attr("src", Athena.settingsModel.get("image-splash-path") + Athena.dlolocaleModel.splash.background);
					}
				}
			}
			this.animateElements();
		},
		render : function() {
			this.$el = $("section.splash");
			var a = Athena.settingsModel.get("show-splash");
			if (Athena.settingsModel.deeplink && Athena.settingsModel.get("deeplink-skip-splash")) {
				this.end();
			} else {
				if ((Athena.settingsModel.deeplink && !Athena.settingsModel.get("deeplink-skip-splash")) || !Athena.settingsModel.deeplink) {
					if (!Athena.HIDE_SPLASH) {
						if (a) {
							if (Athena.dlolocaleModel.options.useSplash !== undefined) {
								if (Athena.dlolocaleModel.options.useSplash) {
									this.title = this.$el.find(".title");
									this.subTitle = this.$el.find(".sub-title");
									this.intro = this.$el.find(".intro");
									this.buttonEnter = this.$el.find(".button-enter");
									this.image = this.$el.find(".splash-bg");
									this.setContent();
									this.show();
									this.setEvents();
									if (Athena.dlolocaleModel.options.splashTransition === "horizontal") {
										$("#wrapper").css({
											width : "200%"
										});
										$(".shell-box-modal").css({
											width : "50%"
										});
										$(".shell-box-modal-inner").css({
											width : "50%"
										});
										this.$el.css({
											width : "50%"
										});
										$("section.shell").css({
											width : "50%"
										});
										if (this.updateHeader) {
											$("header.main").css({
												width : "50%"
											});
										}
									} else {
										if (Athena.dlolocaleModel.options.splashTransition === "overlay") {
											this.$el.css({
												position : "absolute",
												top : 0,
												"z-index" : 1
											});
											$("section.shell").css({
												position : "absolute",
												top : 0,
												display : "none",
												"z-index" : 0
											});
										}
									}
									if (Athena.settingsModel.get("auto-init-shell") && Athena.dlolocaleModel.options.splashTransition !== "overlay") {
										this.dispatch(Athena.SHELL_INIT);
										this.dispatch(Athena.SHELL_ACTIVATE);
									}
								} else {
									this.end();
								}
							} else {
								this.end();
							}
						} else {
							this.end();
						}
					} else {
						this.end();
					}
				} else {
					this.end();
				}
			}
		}
	});
	Athena.ToolsView = Athena.CoreSettingsView.extend({
		initialize : function() {
			Athena.ToolsView.__super__.initialize.call(this);
			this.register(Athena.TOOLS_HIDE, this.fadeOut, this);
			this.register(Athena.TOOLS_SHOW, this.fadeIn, this);
		},
		render : function() {
			this.$el = $("#panel-tools");
			this.setVisibleButtons();
		}
	});
	Athena.handleMessage = function(d) {
		var c, a = [], b = d.data.data;
		for (c in b) {
			if (b.hasOwnProperty(c)) {
				a.push(b[c]);
			}
		}
		debug(d.data.evnt);
		Athena.eventBus.trigger(d.data.evnt, a.join());
	};
	Athena.init = function() {
		if (Athena.eventBus === null) {
			Athena.eventBus = _.extend({}, Backbone.Events);
		}
		Backbone.Router.prototype.eventBus = Backbone.Model.prototype.eventBus = Backbone.Collection.prototype.eventBus = Backbone.View.prototype.eventBus = Athena.eventBus;
		BackboneMVC.Controller.eventBus = Athena.eventBus;
		$(window).resize(function() {
			Athena.WINDOW_WIDTH = $(window).width();
			Athena.WINDOW_HEIGHT = $(window).height();
			Athena.eventBus.trigger(Athena.APP_RESIZE);
		});
		$(document).on("webkitfullscreenchange mozfullscreenchange fullscreenchange", function(a) {
			Athena.eventBus.trigger(Athena.F11, false);
		});
		$(document).keypress(function(c) {
			var b = c.keyCode || c.which, a = {
				f11 : Athena.F11_KEY,
				enter : Athena.ENTER_KEY,
				tab : Athena.TAB_KEY
			};
			switch (b) {
			case a.f11:
				Athena.eventBus.trigger(Athena.F11, true);
				break;
			case a.enter:
				Athena.eventBus.trigger(Athena.ENTER, true);
				break;
			case a.tab:
				Athena.eventBus.trigger(Athena.TAB, true);
				break;
			}
		});
		Athena.detector = new Athena.Detection();
		Athena.browserDetails = Athena.detector.get();
		if (Athena.browserDetails[0].Browser === "Internet Explorer" && Athena.browserDetails[0].hasChromeFrame === -1 && Number(Athena.browserDetails[0].Version) < 10) {
			$("#preload-modal").css("display", "none");
			alert("HMH Collections requires the Chrome Frame Plugin for Internet Explorer to run correctly, please visit http://www.google.com/chromeframe download and install the plugin before trying to run the site again.");
			throw new Error("Stop Execution and show alert this is a pseudo error");
		}
		Athena.transitions = new Athena.TransitionManager();
		Athena.arrayMethods = new Athena.ArrayMethods();
		Athena.userModel = (Athena.CustomUserCollection !== undefined) ? new Athena.CustomUserCollection() : new Athena.UserCollection();
		Athena.settingsModel = (Athena.CustomSettingsCollection !== undefined) ? new Athena.CustomSettingsCollection() : new Athena.SettingsCollection();
		Athena.activityModel = (Athena.CustomActivityCollection !== undefined) ? new Athena.CustomActivityCollection() : new Athena.ActivityCollection();
		Athena.localeModel = (Athena.CustomLocaleCollection !== undefined) ? new Athena.CustomLocaleCollection() : new Athena.LocaleCollection();
		Athena.dlolocaleModel = (Athena.CustomLocaleActivityCollection !== undefined) ? new Athena.CustomLocaleActivityCollection() : new Athena.LocaleActivityCollection();
		Athena.preloaderView = (Athena.CustomPreloaderView !== undefined) ? new Athena.CustomPreloaderView() : new Athena.PreloaderView();
		Athena.alertView = (Athena.CustomAlertView !== undefined) ? new Athena.CustomAlertView() : new Athena.AlertView();
		Athena.preloadController = (Athena.CustomPreloaderController !== undefined) ? new Athena.CustomPreloaderController() : new Athena.PreloaderController();
		Athena.audioController = (Athena.CustomAudioController !== undefined) ? new Athena.CustomAudioController() : new Athena.AudioController();
		Athena.apiController = (Athena.CustomApiController !== undefined) ? new Athena.CustomApiController() : new Athena.ApiController();
		Athena.scormCollection = (Athena.CustomScormCollection !== undefined) ? new Athena.CustomScormCollection() : new Athena.ScormCollection();
		Athena.fullscreenController = (Athena.CustomFullscreenController !== undefined) ? new Athena.CustomFullscreenController() : new Athena.FullscreenController();
		Athena.initialiseController = (Athena.CustomInitialiseController !== undefined) ? new Athena.CustomInitialiseController() : new Athena.InitialiseController();
		Athena.startupController = (Athena.CustomStartUpController !== undefined) ? new Athena.CustomStartUpController() : new Athena.StartUpController();
		Athena.CustomInitialiser();
		Athena.router = (Athena.CustomRouter !== undefined) ? new Athena.CustomRouter() : new Athena.Router();
		Backbone.history.start();
	};
}());