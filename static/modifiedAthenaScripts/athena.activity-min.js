var HsmActivity_1_View = Athena.CoreActivityView
		.extend({
			header : null,
			title : null,
			subtitle : null,
			intro : null,
			interactivities : [],
			useScroll : false,
			allVisible : false,
			headerHeight : 0,
			scrollMax : 0,
			totalInteractivities : 0,
			containerHeight : 0,
			activityHeight : 0,
			itemPosition : 0,
			objectIds : [],
			initialize : function(b, a, c) {
				HsmActivity_1_View.__super__.initialize.call(this, b, a, c);
				this.register(Athena.MEDIA_STOPALL, this.stopMedia, this);
				this.prerender();
			},
			resize : function() {
				HsmActivity_1_View.__super__.resize.call(this);
				if (this.model.option.page.value === "engage") {
					this.header.height(Athena.WINDOW_HEIGHT);
				}
			},
			disableFunctions : function() {
				HsmActivity_1_View.__super__.disableFunctions.call(this);
				this.dispatch(Athena.PALETTE_HIDE);
				this.stopAllVideos();
			},
			enableFunctions : function() {
				HsmActivity_1_View.__super__.enableFunctions.call(this);
				this.handleScrollEnabled();
			},
			timedAudioHandlers : function(a) {},
			resetAudioHandlers : function(a) {},
			completedAudioHandlers : function(a) {},
			stopMedia : function() {
				this.stopAudio();
				this.$el.find(".audio-play").removeClass("audio-stop");
			},
			setEvents : function() {
				var a = this, d, b, c;
				this.$el.find(".glossary-item").unbind("click");
				this.$el.find(".glossary-item").click(function() {
					if ($(this).attr("data-term") !== undefined) {
						d = $(this).attr("data-term");
					} else {
						d = $(this).text();
					}
					if (Athena.settingsModel.get("glossary-embedded")) {
						a.dispatch(Athena.GLOSSARY_SHOW, d);
					} else {
						b = Athena.settingsModel.hostName;
						c = Athena.settingsModel.get("glossary-link");
						if (!String(b).match(/http:/g)) {
							b = "http://" + b;
						}
						window.open(b + c + d, "_blank");
					}
				});
			},
			clearEvents : function() {},
			startActivity : function() {},
			setContent : function() {
				HsmActivity_1_View.__super__.setContent.call(this);
			},
			setStep : function(a) {},
			changeStep : function(b, a) {},
			replayAction : function() {},
			stopAllVideos : function() {
				var a;
				for (a in this) {
					if (this.hasOwnProperty(a)) {
						if (a.match(/player_/g)) {
							debug(a);
							this[a].stop();
						}
					}
				}
				for (a in this) {
					if (this.hasOwnProperty(a)) {
						if (a.match(/it_/g)) {
							debug(a);
							this[a].stopAllVideo();
						}
					}
				}
			},
			getMarkup : function(b, e) {
				var a, d = Athena.shellView.ahtml, c = $(d).filter(function() {
					var h, f, g;
					h = $(this).attr("class");
					if (h !== undefined) {
						if (h.split(" ")[0] === b) {
							g = h.split(" ")[0];
						}
					}
					return g;
				});
				a = b + "-" + e;
				c.addClass(a);
				this.$el.find(".activity").append(c);
				return c;
			},
			pluginEvent : function(d, a, f, c, e) {
				var b;
				if (typeof e !== "object") {
					Athena.eventBus.trigger(Athena.EVENT_TRACK, {
						verb : f,
						result : a,
						intType : c,
						objectId : e
					}, d);
				} else {
					e.activity = this.ref;
					b = Athena.xapiCollection.set(this.$el.attr("data-objectid"), e.id, e.value, function(g) {
						debug("SET COMPLETE", g);
					});
				}
				return b;
			},
			onlineCheck : function() {
				var a = true;
				if (Athena.settingsModel.param.isOnline !== undefined && !Athena.settingsModel.param.isOnline) {
					a = false;
				}
				return a;
			},
			initPlugins : function() {
				var e, a = this.model.interactivities, c, d = 0, b = this;
				debug("PLUGINS : ", a);
				for (e in a) {
					if (a.hasOwnProperty(e)) {
						this.objectIds.push(a[e].objectId);
						if (e.match(/IT/g)) {
							b.initIT(d, a[e]);
						} else {
							if (e.match(/EV/g)) {
								b.initEV(d, a[e]);
							} else {
								if (e.match(/MCQ/g)) {
									b.initMCQ(d, a[e]);
								} else {
									if (e.match(/MSQ/g)) {
										b.initMSQ(d, a[e]);
									} else {
										if (e.match(/FIQ/g)) {
											b.initFIQ(d, a[e]);
										} else {
											if (e.match(/MDQ/g)) {
												b.initMDQ(d, a[e]);
											} else {
												if (e.match(/FTQ/g)) {
													b.initFTQ(d, a[e]);
												} else {
													if (e.match(/DDM/g)) {
														b.initDDM(d, a[e]);
													} else {
														if (e.match(/DDS/g)) {
															b.initDDS(d, a[e]);
														} else {
															if (e.match(/DDO/g)) {
																b.initDDO(d, a[e]);
															} else {
																if (e.match(/SR/g)) {
																	b.initSR(d, a[e]);
																} else {
																	if (e.match(/CR/g)) {
																		b.initCR(d, a[e]);
																	} else {
																		if (e.match(/CH/g)) {
																			b.initCH(d, a[e]);
																		} else {
																			if (e.match(/IH/g)) {
																				b.initIH(d, a[e]);
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
						d++;
					}
				}
			},
			initIT : function(b, h) {
				try {
					try {
						$(document).unbind("contextmenu");
					} catch (i) {}
					var k = this, e, g, d, a, j;
					j = new Athena.BaseClass();
					h = j.setMathFontSize(h);
					a = function(m, l) {
						if (m === "play") {
							k.stopAllVideos();
							k.dispatchAudio(l);
						} else {
							k.stopAudio();
						}
					};
					g = this.getMarkup("interactivity-it", b);
					e = this.$el.find(".interactivity-it-" + b);
					e.attr("data-objectid", h.objectId);
					this["it_" + b] = new Athena.InstructionalText();
					this["it_" + b].options({
						model : h,
						tableClass : "table",
						tableHeadClass : "table-head",
						tableRowClass : "table-row",
						tableCellClass : "table-cell",
						activityContainer : e,
						textHighlightercallBack : d,
						audioClicked : a,
						imageSourcePath : Athena.settingsModel.getImagePath(),
						objIdParent : Athena.dlolocaleModel.options.dloDetails,
						isOnline : this.onlineCheck(),
						offlineSettings : {
							videoDownloadDir : Athena.settingsModel.get("video-download-dir"),
							videoFileSuffix : Athena.settingsModel.get("video-file-suffix"),
							captionFileSuffix : Athena.settingsModel.get("caption-file-suffix"),
							posterFileSuffix : Athena.settingsModel.get("poster-file-suffix")
						}
					});
					this["it_" + b].create(k);
					this.setEvents();
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-it-" + b) ]);
					} catch (f) {
						debug(f);
					}
				} catch (c) {
					debug("Exception in IT");
					debug(c);
				}
			},
			initEV : function(d, j) {
				try {
					var b = this.onlineCheck(), h = this.getMarkup("interactivity-ev", d), f = this.$el.find(".interactivity-ev-" + d), g = j.contents.player_1.value, i = j.contents.player_code_1.value, a = Number(j.contents.player_width.value), m = Number(j.contents.player_height.value), c = j.contents.player_background.value, l = j.contents.video_1.objectId, k = j.contents.video_1.value;
					f.attr("data-objectid", j.objectId);
					if (b) {
						this["player_" + d] = new Athena.BrightCovePlayer();
						this["player_" + d].options({
							playerId : g,
							playerKey : i,
							token : "cLsCcy1dDwolkR0UPKf7zJ8ZyU13B7EStFY1k-GqiAMxbQu1pk2Uug..",
							width : a,
							height : m,
							bgColour : c,
							useButtons : false,
							objIdParent : Athena.dlolocaleModel.options.dloDetails,
							pluginEvent : "Athena.onVideoLoaded"
						});
					} else {
						this["player_" + d] = new Athena.JPlayer();
						this["player_" + d].options({
							width : a,
							height : m,
							bgColour : c,
							useButtons : false,
							jPlayerbuttons : true,
							objIdParent : Athena.dlolocaleModel.options.dloDetails,
							videoDownloadDir : Athena.settingsModel.get("video-download-dir"),
							videoFileSuffix : Athena.settingsModel.get("video-file-suffix"),
							captionFileSuffix : Athena.settingsModel.get("caption-file-suffix"),
							posterFileSuffix : Athena.settingsModel.get("poster-file-suffix"),
							comingSoonText : Athena.localeModel.get("video-coming-soon"),
							noFlashPlayer : Athena.localeModel.get("no-flash-player"),
							ENCODE : false,
							pluginEvent : "Athena.onVideoLoaded"
						});
					}
					this["player_" + d].init("activity-player-" + this.ref + "-" + d, f, k, l);
				} catch (e) {
					debug("Exception in EV");
					debug(e);
				}
			},
			initMCQ : function(n, v) {
				try {
					var e, u, w, f, p, h, t, b, i, o, l, r, s, d, q, a, j, k, m = this;
					e = new Athena.BaseClass();
					v = e.setMathFontSize(v);
					w = this.getMarkup("interactivity-mcq", n);
					f = this.$el.find(".interactivity-mcq-" + n);
					f.attr("data-objectid", v.objectId);
					q = "choice-element-";
					j = false;
					p = {
						answerBoxSelect : "answer-box-select",
						answerBoxUnSelect : "answer-box-unselect",
						answerBoxCorrect : "answer-box-correct",
						answerBoxWrong : "answer-box-wrong",
						answerBoxShowMe : "answer-box-showme",
						select : "radio-btn-select",
						unselect : "radio-btn-unselect",
						correct : "radio-btn-correct",
						wrong : "radio-btn-wrong",
						showme : "radio-btn-showme"
					};
					if (Athena.MultipleChoiceQuestion.prototype.removeLineBreakandTrim(String(v.contents.content_type.value)) === "text") {
						f.find(".single-image-container").hide();
						k = f.find(".single-container");
					} else {
						f.find(".single-container").hide();
						k = f.find(".single-image-container");
					}
					if (Athena.settingsModel.getImagePath() !== null && Athena.settingsModel.getImagePath() !== undefined) {
						i = Athena.settingsModel.getImagePath();
					} else {
						i = "images/";
					}
					if (Athena.localeModel.get("button-check") !== null && Athena.localeModel.get("button-check") !== undefined) {
						o = Athena.localeModel.get("button-check");
					} else {
						o = "Check";
					}
					if (Athena.localeModel.get("button-tryagain") !== null && Athena.localeModel.get("button-tryagain") !== undefined) {
						l = Athena.localeModel.get("button-tryagain");
					} else {
						l = "Try again";
					}
					if (Athena.localeModel.get("button-showme") !== null && Athena.localeModel.get("button-showme") !== undefined) {
						r = Athena.localeModel.get("button-showme");
					} else {
						r = "Show me";
					}
					if (Athena.localeModel.get("button-myanswer") !== null && Athena.localeModel.get("button-myanswer") !== undefined) {
						s = Athena.localeModel.get("button-myanswer");
					} else {
						s = "My answer";
					}
					b = {
						imagePath : i,
						buttonCheckText : o,
						buttonTryAgainText : l,
						buttonShowMeText : r,
						buttonMyAnswerText : s,
						pluginEvent : function(z, x, B, y, A) {
							return m.pluginEvent(z, x, B, y, A);
						}
					};
					t = {
						container : k,
						mainContainer : f.find(".main-Container"),
						cloneContainer : "interaction_container",
						defaultItem : "choice-element",
						checkButtonObject : "check-btn",
						tryButtonObject : "try-again-btn",
						showMeButtonObject : "showme-btn",
						myAnswersButtonObject : "myanswer-btn",
						choiceBtnClassName : "choice-btn",
						choiceTextClassName : "choice-text",
						choiceIdPrefix : "choice-btn-id-",
						checkBtnActive : "active",
						questionObject : "question_container",
						feedBackTextClass : "feedback",
						styleClassNames : p,
						hint : "hint",
						replaceHTML : null,
						successMsg : Athena.localeModel.get("correct-msg"),
						failMsg : Athena.localeModel.get("fail-msg"),
						tryAgainMsg : Athena.localeModel.get("fail-msg"),
						showmeMsg : Athena.localeModel.get("showme-msg")
					};
					this["mcq_" + n] = new Athena.MultipleChoiceQuestion();
					this["mcq_" + n].options(v, t, b, q, "append", j, null);
					this.setEvents();
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-mcq-" + n), function() {
							m["mcq_" + n].reCalculateWidth();
						} ]);
					} catch (c) {
						debug(c);
					}
					this.setEvents();
				} catch (g) {
					debug("Exception in MCQ");
					debug(g);
				}
			},
			initMSQ : function(n, u) {
				try {
					var e, v, f, p, h, t, b, i, o, l, r, s, d, q, a, j, k, m = this;
					e = new Athena.BaseClass();
					u = e.setMathFontSize(u);
					v = this.getMarkup("interactivity-msq", n);
					f = this.$el.find(".interactivity-msq-" + n);
					f.attr("data-objectid", u.objectId);
					q = "choice-element-";
					j = false;
					p = {
						answerBoxSelect : "answer-box-select",
						answerBoxUnSelect : "answer-box-unselect",
						answerBoxCorrect : "answer-box-correct",
						answerBoxWrong : "answer-box-wrong",
						answerBoxShowMe : "answer-box-showme",
						select : "check-box-select",
						unselect : "check-box-unselect",
						correct : "check-box-correct",
						wrong : "check-box-wrong",
						showme : "check-box-showme"
					};
					if (Athena.settingsModel.getImagePath() !== null && Athena.settingsModel.getImagePath() !== undefined) {
						i = Athena.settingsModel.getImagePath();
					} else {
						i = "images/";
					}
					if (Athena.localeModel.get("button-check") !== null && Athena.localeModel.get("button-check") !== undefined) {
						o = Athena.localeModel.get("button-check");
					} else {
						o = "Check";
					}
					if (Athena.localeModel.get("button-tryagain") !== null && Athena.localeModel.get("button-tryagain") !== undefined) {
						l = Athena.localeModel.get("button-tryagain");
					} else {
						l = "Try again";
					}
					if (Athena.localeModel.get("button-showme") !== null && Athena.localeModel.get("button-showme") !== undefined) {
						r = Athena.localeModel.get("button-showme");
					} else {
						r = "Show me";
					}
					if (Athena.localeModel.get("button-myanswer") !== null && Athena.localeModel.get("button-myanswer") !== undefined) {
						s = Athena.localeModel.get("button-myanswer");
					} else {
						s = "My answer";
					}
					b = {
						imagePath : i,
						buttonCheckText : o,
						buttonTryAgainText : l,
						buttonShowMeText : r,
						buttonMyAnswerText : s,
						pluginEvent : function(y, w, A, x, z) {
							return m.pluginEvent(y, w, A, x, z);
						}
					};
					if (String(u.contents.content_type.value) === "text") {
						f.find(".multiple-image-container").hide();
						k = f.find(".multiple-container");
					} else {
						f.find(".multiple-container").hide();
						k = f.find(".multiple-image-container");
					}
					t = {
						container : k,
						mainContainer : f.find(".main-Container"),
						cloneContainer : "interaction_container",
						defaultItem : "choice-element",
						checkButtonObject : "check-btn",
						tryButtonObject : "try-again-btn",
						showMeButtonObject : "showme-btn",
						myAnswersButtonObject : "myanswer-btn",
						choiceBtnClassName : "choice-btn",
						choiceTextClassName : "choice-text",
						choiceIdPrefix : "choice-btn-id-",
						checkBtnActive : "active",
						questionObject : "question_container",
						feedBackTextClass : "feedback",
						styleClassNames : p,
						hint : "hint",
						replaceHTML : null,
						successMsg : Athena.localeModel.get("correct-msg"),
						failMsg : Athena.localeModel.get("fail-msg"),
						tryAgainMsg : Athena.localeModel.get("fail-msg"),
						showmeMsg : Athena.localeModel.get("showme-msg")
					};
					this["msq_" + n] = new Athena.MultipleChoiceQuestion();
					this["msq_" + n].options(u, t, b, q, "append", j, null);
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-msq-" + n), function() {
							m["msq_" + n].reCalculateWidth();
						} ]);
					} catch (c) {
						debug(c);
					}
					this.setEvents();
				} catch (g) {
					debug("Exception in MSQ");
					debug(g);
				}
			},
			initFIQ : function(a, f) {
				try {
					debug("FIQ", f);
					var h, i, c, e, g = this;
					h = new Athena.BaseClass();
					f = h.setMathFontSize(f);
					e = this.getMarkup("interactivity-fib", a);
					i = {
						markWidth : 12,
						successMsg : Athena.localeModel.get("correct-msg"),
						tryAgainMsg : Athena.localeModel.get("fail-msg"),
						hintMsg : f.contents.hint.value,
						failMsg : Athena.localeModel.get("fail-msg"),
						incompleteMsg : Athena.localeModel.get("partial-answer-msg"),
						showMeMsg : Athena.localeModel.get("showme-msg"),
						tryAgainBtnTxt : Athena.localeModel.get("button-tryagain"),
						showMeBtnTxt : Athena.localeModel.get("button-showme"),
						myAnswerBtnTxt : Athena.localeModel.get("button-myanswer"),
						checkBtnTxt : Athena.localeModel.get("button-check"),
						contentClass : "content",
						checkbuttonClass : "check-box",
						feedback : "feedback_container",
						hint : "hint_container",
						imagesPath : Athena.settingsModel.getImagePath(),
						pluginEvent : function(l, j, n, k, m) {
							return g.pluginEvent(l, j, n, k, m);
						},
						autoScrollTo : true
					};
					c = this.$el.find(".interactivity-fib-" + a);
					c.attr("data-objectid", f.objectId);
					this["fiq_" + a] = new Athena.FillInBlankQuestion();
					this["fiq_" + a].options(i, f);
					this["fiq_" + a].init(c);
					this["fiq_" + a].create();
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-fib-" + a) ]);
					} catch (d) {
						debug(d);
					}
					this.setEvents();
				} catch (b) {
					debug("Exception in FIQ");
					debug(b);
				}
			},
			initMDQ : function(n, u) {
				try {
					var e, v, f, p, h, t, b, i, o, l, r, s, d, q, a, j, k, m = this;
					e = new Athena.BaseClass();
					u = e.setMathFontSize(u);
					v = this.getMarkup("interactivity-mdq", n);
					f = this.$el.find(".interactivity-mdq-" + n);
					f.attr("data-objectid", u.objectId);
					q = "choice-element-";
					j = false;
					p = {
						answerBoxSelect : "answer-box-select",
						answerBoxUnSelect : "answer-box-unselect",
						answerBoxCorrect : "answer-box-correct",
						answerBoxWrong : "answer-box-wrong",
						answerBoxShowMe : "answer-box-showme",
						select : "dropdown-select",
						selected : "dropdown-selected",
						unselect : "dropdown-unselect",
						correct : "dropdown-correct",
						correctTryCount : "dropdown-correct-trycount",
						wrong : "dropdown-wrong",
						showme : "dropdown-showme"
					};
					if (Athena.settingsModel.getImagePath() !== null && Athena.settingsModel.getImagePath() !== undefined) {
						i = Athena.settingsModel.getImagePath();
					} else {
						i = "images/";
					}
					if (Athena.localeModel.get("button-check") !== null && Athena.localeModel.get("button-check") !== undefined) {
						o = Athena.localeModel.get("button-check");
					} else {
						o = "Check";
					}
					if (Athena.localeModel.get("button-tryagain") !== null && Athena.localeModel.get("button-tryagain") !== undefined) {
						l = Athena.localeModel.get("button-tryagain");
					} else {
						l = "Try again";
					}
					if (Athena.localeModel.get("button-showme") !== null && Athena.localeModel.get("button-showme") !== undefined) {
						r = Athena.localeModel.get("button-showme");
					} else {
						r = "Show me";
					}
					if (Athena.localeModel.get("button-myanswer") !== null && Athena.localeModel.get("button-myanswer") !== undefined) {
						s = Athena.localeModel.get("button-myanswer");
					} else {
						s = "My answer";
					}
					b = {
						imagePath : i,
						buttonCheckText : o,
						buttonTryAgainText : l,
						buttonShowMeText : r,
						buttonMyAnswerText : s,
						pluginEvent : function(y, w, A, x, z) {
							return m.pluginEvent(y, w, A, x, z);
						}
					};
					h = '<div class="selectBox dropdown-unselect" xmlns="http://www.w3.org/1999/xhtml"> <ul> <li class="replace" value="0"> <span class="selectedValue"> </span> <ul class="option drop-down-options"> <li value="0" class="selector"><div> </div></li> </ul> </li> </ul></div>';
					k = f.find(".drop-down-container");
					t = {
						refId : n,
						container : k,
						mainContainer : f.find(".main-Container"),
						cloneContainer : "interaction_container",
						defaultItem : "choice-element",
						checkButtonObject : "check-btn",
						tryButtonObject : "try-again-btn",
						showMeButtonObject : "showme-btn",
						myAnswersButtonObject : "myanswer-btn",
						choiceIdPrefix : null,
						optionsClass : "drop-down-options",
						clickClassName : "selector",
						dropDownClassName : "selectBox",
						replaceText : "selectedValue",
						checkBtnActive : "active",
						questionObject : "question_container",
						feedBackTextClass : "feedback",
						styleClassNames : p,
						hint : "hint",
						replaceHTML : h,
						successMsg : Athena.localeModel.get("correct-msg"),
						failMsg : Athena.localeModel.get("fail-msg"),
						tryAgainMsg : Athena.localeModel.get("fail-msg"),
						showmeMsg : Athena.localeModel.get("showme-msg")
					};
					this["mdq_" + n] = new Athena.DropDownCreator();
					this["mdq_" + n].setCloneItems(u, t, b, q, "append", j, null);
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-mdq-" + n), function() {
							m["mdq_" + n].reCalculateWidth();
						} ]);
					} catch (c) {
						debug(c);
					}
					this.setEvents();
				} catch (g) {
					debug("Exception in MDQ");
					debug(g);
				}
				this["mdq_" + n].displayUserInput();
			},
			initFTQ : function(a, g) {
				try {
					var i, f, d, j, b, h = this;
					i = new Athena.BaseClass();
					g = i.setMathFontSize(g);
					f = this.getMarkup("interactivity-ftq", a);
					d = this.$el.find(".interactivity-ftq-" + a);
					d.attr("data-objectid", g.objectId);
					if (g.contents.isReflect !== undefined) {
						if (g.contents.isReflect.value === "true") {
							d.addClass("reflect");
						}
					}
					if (g.contents.reflectTitle !== undefined) {
						d.find(".optional-header").html(g.contents.reflectTitle.value).show();
					}
					if (g.contents.user_input !== undefined) {
						d.find(".math-output").attr("data-objectid", g.contents.user_input.objectId);
					}
					j = {
						parentContainer : d,
						questionClass : "ftq_content",
						qImageClass : "image",
						headerClass : "questionHeader",
						saveBtnClass : "save-btn",
						saveBtnTxt : Athena.localeModel.get("save-button-text"),
						sendBtnClass : "sendToNoteBook-btn",
						sendToNoteBookTxt : "Send To NoteBook",
						symbolBtnContainerClass : "symbolContainer2",
						symbolBtnClass : "formula-btn",
						inputBoxClass : "overview",
						textAreaComp : "text-area-comp",
						scrollbarClass : "scrollbar",
						pluginEvent : function(m, k, o, l, n) {
							return h.pluginEvent(m, k, o, l, n);
						},
						imageSourcePath : Athena.settingsModel.getImagePath(),
						paletteCallback : function(k, l) {
							h.handleRequestPalette(k, l);
						},
						resizeCallback : function() {
							h.dispatch(Athena.APP_RESIZE);
						},
						paletteHideCallback : function() {
							h.dispatch(Athena.PALETTE_HIDE);
						},
						paletteShowCallback : function() {
							h.dispatch(Athena.PALETTE_SHOW);
						},
						autoScrollTo : true
					};
					this["ftq_" + a] = new Athena.FreeTextQuestion();
					this["ftq_" + a].options(j, g, this);
					this["ftq_" + a].init();
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-ftq-" + a) ]);
					} catch (e) {
						debug(e);
					}
					this.setEvents();
				} catch (c) {
					debug("Exception in FTQ");
					debug(c);
				}
			},
			initDDM : function(g, f) {
				try {
					var a, d, b, c = this;
					a = new Athena.BaseClass();
					f = a.setMathFontSize(f);
					d = this.getMarkup("interactivity-ddm", g);
					b = this.$el.find(".interactivity-ddm-" + g);
					b.attr("data-objectid", f.objectId);
					this["ddm_" + g] = new Athena.DragDrop();
					this["ddm_" + g].options({
						modelData : f,
						dndType : "ddo-ole",
						contentType : 1,
						dropClonable : false,
						dragClass : "drag",
						parentContainer : b,
						dragContainer : "drag-Area",
						dropContainer : "drop-Area",
						enableClass : "active",
						toggleAnswers : true,
						checkText : Athena.localeModel.get("button-check"),
						tryagainText : Athena.localeModel.get("button-tryagain"),
						showMeText : Athena.localeModel.get("button-showme"),
						myAnswerText : Athena.localeModel.get("button-myanswer"),
						imageSourcePath : Athena.settingsModel.getImagePath(),
						browser : Athena.browserDetails[0].Browser,
						pluginEvent : function(j, h, l, i, k) {
							return c.pluginEvent(j, h, l, i, k);
						},
						incorrectMsg : Athena.localeModel.get("fail-msg"),
						showmeMsg : Athena.localeModel.get("showme-msg"),
						partialMessage : Athena.localeModel.get("partial-answer-msg"),
						successMsg : Athena.localeModel.get("correct-msg"),
						failMsg : Athena.localeModel.get("fail-msg")
					});
					this["ddm_" + g].create();
					this.setEvents();
				} catch (e) {
					debug("Exception in DDM");
					debug(e);
				}
			},
			initDDS : function(g, f) {
				try {
					var a, d, b, c = this;
					a = new Athena.BaseClass();
					f = a.setMathFontSize(f);
//					debugger;
					this["dds_" + g] = new Athena.DragDrop();
					if (String(this["dds_" + g].removeLineBreakandTrim(f.contents.type.value)) === "dds-sfg") {
						d = this.getMarkup("interactivity-dds-sfg", g);
						b = this.$el.find(".interactivity-dds-sfg-" + g);
						b.attr("data-objectid", f.objectId);
						this["dds_" + g].options({
							modelData : f,
							dndType : "dds",
							contentType : 2,
							dragContainer : "drag-container",
							dropClonable : false,
							dropContainer : "drop-container",
							parentContainer : b,
							allowMultiple : true,
							checkText : Athena.localeModel.get("button-check"),
							tryagainText : Athena.localeModel.get("button-tryagain"),
							showMeText : Athena.localeModel.get("button-showme"),
							myAnswerText : Athena.localeModel.get("button-myanswer"),
							imageSourcePath : Athena.settingsModel.getImagePath(),
							browser : Athena.browserDetails[0].Browser,
							pluginEvent : function(j, h, l, i, k) {
								return c.pluginEvent(j, h, l, i, k);
							},
							incorrectMsg : Athena.localeModel.get("fail-msg"),
							showmeMsg : Athena.localeModel.get("showme-msg"),
							partialMessage : Athena.localeModel.get("partial-answer-msg"),
							successMsg : Athena.localeModel.get("correct-msg"),
							failMsg : Athena.localeModel.get("fail-msg")
						});
						this["dds_" + g].create();
					} else {
						d = this.getMarkup("interactivity-dds-sp", g);
						b = this.$el.find(".interactivity-dds-sp-" + g);
						b.attr("data-objectid", f.objectId);
						this["dds_" + g].options({
							modelData : f,
							allowMultiple : true,
							dndType : "dds",
							contentType : 1,
							dropClonable : false,
							resetAll : false,
							dropClass : "drop",
							innerContainer : "inner-container",
							dragClass : "drag",
							parentContainer : b,
							dragContainer : "drag-Area",
							dropContainer : "drop",
							enableClass : "active",
							correctClass : "correct-answer",
							incorrectClass : "wrong-answer",
							myanswerCorrectCls : "default-correct",
							activityType : "group",
							checkText : Athena.localeModel.get("button-check"),
							tryagainText : Athena.localeModel.get("button-tryagain"),
							showMeText : Athena.localeModel.get("button-showme"),
							myAnswerText : Athena.localeModel.get("button-myanswer"),
							imageSourcePath : Athena.settingsModel.getImagePath(),
							browser : Athena.browserDetails[0].Browser,
							pluginEvent : function(j, h, l, i, k) {
								return c.pluginEvent(j, h, l, i, k);
							},
							incorrectMsg : Athena.localeModel.get("fail-msg"),
							showmeMsg : Athena.localeModel.get("showme-msg"),
							partialMessage : Athena.localeModel.get("partial-answer-msg"),
							successMsg : Athena.localeModel.get("correct-msg"),
							failMsg : Athena.localeModel.get("fail-msg")
						});
						this["dds_" + g].create();
					}
					this.setEvents();
				} catch (e) {
					debug("Exception in DDS");
					debug(e);
				}
			},
			initDDO : function(g, f) {
				try {
					var a, d, b, c = this;
					a = new Athena.BaseClass();
					f = a.setMathFontSize(f);
//					debugger;
					this["ddo_" + g] = new Athena.DragDrop();
					if (new RegExp("ddo-ole").test(String(f.contents.type.value))) {
						d = this.getMarkup("interactivity-ddo-ole", g);
						b = this.$el.find(".interactivity-ddo-ole-" + g);
						b.attr("data-objectid", f.objectId);
						this["ddo_" + g].options({
							refId : g,
							modelData : f,
							dndType : f.contents.type.value,
							dropClonable : false,
							dragClass : "drag",
							parentContainer : b,
							dragContainer : "drag-Area",
							dropContainer : "drop-Area",
							enableClass : "active",
							checkText : Athena.localeModel.get("button-check"),
							tryagainText : Athena.localeModel.get("button-tryagain"),
							showMeText : Athena.localeModel.get("button-showme"),
							myAnswerText : Athena.localeModel.get("button-myanswer"),
							imageSourcePath : Athena.settingsModel.getImagePath(),
							browser : Athena.browserDetails[0].Browser,
							pluginEvent : function(j, h, l, i, k) {
								return c.pluginEvent(j, h, l, i, k);
							},
							incorrectMsg : Athena.localeModel.get("fail-msg"),
							showmeMsg : Athena.localeModel.get("showme-msg"),
							partialMessage : Athena.localeModel.get("partial-answer-msg"),
							successMsg : Athena.localeModel.get("correct-msg"),
							failMsg : Athena.localeModel.get("fail-msg")
						});
						this["ddo_" + g].create();
					} else {
						if (new RegExp("ddo-osp").test(String(f.contents.type.value))) {
							d = this.getMarkup("interactivity-ddo-osp", g);
							b = this.$el.find(".interactivity-ddo-osp-" + g);
							b.attr("data-objectid", f.objectId);
							if (new RegExp("1").test(String(f.contents.contentType.value))) {
								b.find(".ole_proof_table").detach();
								this["ddo_" + g].options({
									modelData : f,
									dndType : f.contents.type.value,
									dropClonable : true,
									dropClass : "drop",
									dragClass : "drag",
									parentContainer : b,
									dragContainer : "drag-container",
									dropContainer : "drop-container",
									correctClass : "correct-answer",
									incorrectClass : "wrong-answer",
									myanswerCorrectCls : "default-correct",
									checkText : Athena.localeModel.get("button-check"),
									tryagainText : Athena.localeModel.get("button-tryagain"),
									showMeText : Athena.localeModel.get("button-showme"),
									myAnswerText : Athena.localeModel.get("button-myanswer"),
									imageSourcePath : Athena.settingsModel.getImagePath(),
									browser : Athena.browserDetails[0].Browser,
									pluginEvent : function(j, h, l, i, k) {
										return c.pluginEvent(j, h, l, i, k);
									},
									incorrectMsg : Athena.localeModel.get("fail-msg"),
									showmeMsg : Athena.localeModel.get("showme-msg"),
									partialMessage : Athena.localeModel.get("partial-answer-msg"),
									successMsg : Athena.localeModel.get("correct-msg"),
									failMsg : Athena.localeModel.get("fail-msg")
								});
							} else {
								if (new RegExp("2").test(String(f.contents.contentType.value))) {
									b.find(".ole_list").detach();
									b.attr("data-objectid", f.objectId);
									this["ddo_" + g].options({
										modelData : f,
										dndType : f.contents.type.value,
										dropClonable : true,
										dropClass : "drop-Content",
										dragClass : "drag",
										parentContainer : b,
										dragContainer : "drag-Area",
										dropContainer : "prooftable",
										correctClass : "correct-answer",
										incorrectClass : "wrong-answer",
										myanswerCorrectCls : "default-correct",
										checkText : Athena.localeModel.get("button-check"),
										tryagainText : Athena.localeModel.get("button-tryagain"),
										showMeText : Athena.localeModel.get("button-showme"),
										myAnswerText : Athena.localeModel.get("button-myanswer"),
										imageSourcePath : Athena.settingsModel.getImagePath(),
										browser : Athena.browserDetails[0].Browser,
										pluginEvent : function(j, h, l, i, k) {
											return c.pluginEvent(j, h, l, i, k);
										},
										incorrectMsg : Athena.localeModel.get("fail-msg"),
										showmeMsg : Athena.localeModel.get("showme-msg"),
										partialMessage : Athena.localeModel.get("partial-answer-msg"),
										successMsg : Athena.localeModel.get("correct-msg"),
										failMsg : Athena.localeModel.get("fail-msg")
									});
								}
							}
							this["ddo_" + g].create();
						}
					}
					this.setEvents();
				} catch (e) {
					debug("Exception in DDO");
					debug(e);
				}
			},
			initSR : function(a, f) {
				try {
					var h, e, c, i, g = this;
					h = new Athena.BaseClass();
					f = h.setMathFontSize(f);
					e = this.getMarkup("interactivity-sr", a);
					c = this.$el.find(".interactivity-sr-" + a);
					c.attr("data-objectid", f.objectId);
					i = {
						model : f,
						parentContainer : c.find(".slider-container-all"),
						contentContainerDiv : "text-area",
						contentContainerClass : "content-container",
						contentTextDiv : "text-content",
						instructionTextDiv : "instruction-content",
						contentPrefix : "content-",
						sliderContainerDiv : "drag-bar",
						sliderContentDiv : "drop-content",
						sliderPrefix : "slider-",
						imageContent : "image-content",
						draggablePointer : "drag-pointer",
						imageContainerDiv : "image-frame",
						imageContentDiv : "image-area",
						sliderPanelDiv : "slider-panel",
						imagePath : Athena.settingsModel.getImagePath(),
						pluginEvent : function(l, j, n, k, m) {
							return g.pluginEvent(l, j, n, k, m);
						}
					};
					if (String(f.contents.state.value) === "fullframe") {
						c.find(".text-area").hide();
					} else {
						c.find(".image-frame").hide();
					}
					this["sr_" + a] = new Athena.SliderRevealItems();
					this["sr_" + a].options(i);
					this["sr_" + a].init();
					this["sr_" + a].create();
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-sr-" + a) ]);
					} catch (d) {
						debug(d);
					}
					this.setEvents();
				} catch (b) {
					debug("Exception in SR");
					debug(b);
				}
			},
			initCR : function(b, g) {
				try {
					var j, f, d, h, a, i = this;
					j = new Athena.BaseClass();
					g = j.setMathFontSize(g);
					f = this.getMarkup("interactivity-cr-acc", b);
					d = this.$el.find(".interactivity-cr-acc-" + b);
					d.attr("data-objectid", g.objectId);
					this["cr_" + b] = new Athena.Accordion();
					h = typeof g.contents.accordion_type === "undefined" ? "Collapsed" : this["cr_" + b].removeLineBreakandTrim(g.contents.accordion_type.value);
					if (h === "Collapsed") {
						a = 2;
						h = true;
					} else {
						a = 3;
						h = false;
					}
					this["cr_" + b].options({
						activityContainer : d,
						showOneAtTime : h,
						showInitialType : 1,
						activityType : a,
						modelContent : g,
						imageSourcePath : Athena.settingsModel.getImagePath(),
						pluginEvent : function(m, k, o, l, n) {
							return i.pluginEvent(m, k, o, l, n);
						}
					});
					this["cr_" + b].init();
					this.setEvents();
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-cr-acc-" + b) ]);
					} catch (e) {
						debug(e);
					}
				} catch (c) {
					debug("Exception in CR");
					debug(c);
				}
			},
			initCH : function(b, g) {
				try {
					var i, f, d, a, h = this;
					i = new Athena.BaseClass();
					g = i.setMathFontSize(g);
					f = this.getMarkup("interactivity-ch", b);
					d = this.$el.find(".interactivity-ch-" + b);
					d.attr("data-objectid", g.objectId);
					d.find(".question").html(g.contents.question);
					this["ch_" + b] = new Athena.ClickToHighlight();
					if (String(g.contents.content_type.value) === "text") {
						d.find(".interactivity-ch-image").hide();
						this["ch_" + b].options({
							imageSourcePath : Athena.settingsModel.getImagePath(),
							activityContainer : d,
							modelContent : g,
							activityType : 2,
							replaceClickedText : g.lists.button_text,
							originalClickedText : g.lists.button_clciked_text,
							clickClass : "button-text",
							clickClickedClass : "button-clicked",
							changeContentType : "different",
							type : "texthighlight",
							revealClass : "imageclass",
							highlightColors : [ "#fa6b74", "#FAE058", "#fa6b74", "#fa6b74", "#fa6b74" ],
							highlightColorsRgb : [ "250, 107, 116", "59, 254, 250", "232, 147, 250", "254, 174, 80", "250, 107, 116" ],
							pluginEvent : function(l, j, n, k, m) {
								return h.pluginEvent(l, j, n, k, m);
							}
						});
					} else {
						d.find(".interactivity-ch-text").hide();
						this["ch_" + b].options({
							imageSourcePath : Athena.settingsModel.getImagePath(),
							activityContainer : d,
							modelContent : g,
							activityType : 1,
							clickClass : "button-text",
							clickClickedClass : "button-clicked",
							changeContentType : "same",
							type : "show",
							revealClass : "imageclass",
							showOneAtTime : true,
							pluginEvent : function(l, j, n, k, m) {
								return h.pluginEvent(l, j, n, k, m);
							}
						});
					}
					this["ch_" + b].init();
					this.setEvents();
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-ch-" + b) ]);
					} catch (e) {
						debug(e);
					}
				} catch (c) {
					debug("Exception in CH");
					debug(c);
				}
			},
			initIH : function(b, h) {
				try {
					var n = this, o, g, d, m, k, f, l, a, j, i;
					o = new Athena.BaseClass();
					h = o.setMathFontSize(h);
					g = this.getMarkup("interactivity-ih", b);
					d = this.$el.find(".interactivity-ih-" + b);
					d.attr("data-objectid", h.objectId);
					this["ih_" + b] = new Athena.ImageHotSpot();
					if (String(h.contents.image_hotspot_area.value.trim()) === "intersection") {
						if (String(h.contents.image_hotspot_type.value.trim()) === "multiple") {
							this["ih_" + b].options({
								model : h,
								resetAll : false,
								hotspotAnswers : h.lists.hotspotAnswers,
								question_container : "question_container",
								imageContent : "image-content",
								activityContainer : d,
								button : "button",
								parentDom : "content-Area",
								dragContainer : "drag-container",
								checkText : Athena.localeModel.get("button-check"),
								tryagainText : Athena.localeModel.get("button-tryagain"),
								showMeText : Athena.localeModel.get("button-showme"),
								myAnswerText : Athena.localeModel.get("button-myanswer"),
								imagesPath : Athena.settingsModel.getImagePath(),
								correctAnswerFeedback : Athena.localeModel.get("showme-msg"),
								incompleteAnswerFeedback : Athena.localeModel.get("incomplete-msg"),
								partialAnswer : Athena.localeModel.get("partial-answer-msg"),
								successMsg : Athena.localeModel.get("correct-msg"),
								firstAttemptFailMsg : Athena.localeModel.get("fail-msg"),
								secondAttemptFailMsg : Athena.localeModel.get("fail-msg"),
								pluginEvent : function(r, p, t, q, s) {
									return n.pluginEvent(r, p, t, q, s);
								}
							});
						} else {
							this["ih_" + b].options({
								minimumClickCount : 1,
								model : h,
								resetAll : false,
								button : "button",
								imageContent : "image-content",
								question_container : "question_container",
								hotspotAnswers : h.lists.hotspotAnswers,
								activityContainer : d,
								parentDom : "content-Area",
								dragContainer : "drag-container",
								checkText : Athena.localeModel.get("button-check"),
								tryagainText : Athena.localeModel.get("button-tryagain"),
								showMeText : Athena.localeModel.get("button-showme"),
								myAnswerText : Athena.localeModel.get("button-myanswer"),
								imagesPath : Athena.settingsModel.getImagePath(),
								correctAnswerFeedback : Athena.localeModel.get("showme-msg"),
								incompleteAnswerFeedback : Athena.localeModel.get("incomplete-msg"),
								partialAnswer : Athena.localeModel.get("partial-answer-msg"),
								successMsg : Athena.localeModel.get("correct-msg"),
								firstAttemptFailMsg : Athena.localeModel.get("fail-msg"),
								secondAttemptFailMsg : Athena.localeModel.get("fail-msg"),
								pluginEvent : function(r, p, t, q, s) {
									return n.pluginEvent(r, p, t, q, s);
								}
							});
						}
						this["ih_" + b].init();
						this["ih_" + b].createIntersectionImageHotSpots();
					} else {
						if (String(h.contents.image_hotspot_area.value.trim()) === "specific") {
							if (String(h.contents.image_hotspot_type.value.trim()) === "multiple") {
								this["ih_" + b].options({
									model : h,
									resetAll : false,
									hotspotAnswers : h.lists.hotspotAnswers,
									question_container : "question_container",
									imageContent : "image-content",
									activityContainer : d,
									button : "button",
									parentDom : "content-Area",
									dragContainer : "drag-container",
									hotspotAtIntersection : false,
									checkText : Athena.localeModel.get("button-check"),
									tryagainText : Athena.localeModel.get("button-tryagain"),
									showMeText : Athena.localeModel.get("button-showme"),
									myAnswerText : Athena.localeModel.get("button-myanswer"),
									imagesPath : Athena.settingsModel.getImagePath(),
									correctAnswerFeedback : Athena.localeModel.get("showme-msg"),
									incompleteAnswerFeedback : Athena.localeModel.get("incomplete-msg"),
									partialAnswer : Athena.localeModel.get("partial-answer-msg"),
									successMsg : Athena.localeModel.get("correct-msg"),
									firstAttemptFailMsg : Athena.localeModel.get("fail-msg"),
									secondAttemptFailMsg : Athena.localeModel.get("fail-msg"),
									pluginEvent : function(r, p, t, q, s) {
										return n.pluginEvent(r, p, t, q, s);
									}
								});
							} else {
								this["ih_" + b].options({
									minimumClickCount : 1,
									model : h,
									resetAll : false,
									button : "button",
									imageContent : "image-content",
									question_container : "question_container",
									hotspotAnswers : h.lists.hotspotAnswers,
									activityContainer : d,
									parentDom : "content-Area",
									dragContainer : "drag-container",
									hotspotAtIntersection : false,
									checkText : Athena.localeModel.get("button-check"),
									tryagainText : Athena.localeModel.get("button-tryagain"),
									showMeText : Athena.localeModel.get("button-showme"),
									myAnswerText : Athena.localeModel.get("button-myanswer"),
									imagesPath : Athena.settingsModel.getImagePath(),
									correctAnswerFeedback : Athena.localeModel.get("showme-msg"),
									incompleteAnswerFeedback : Athena.localeModel.get("incomplete-msg"),
									partialAnswer : Athena.localeModel.get("partial-answer-msg"),
									successMsg : Athena.localeModel.get("correct-msg"),
									firstAttemptFailMsg : Athena.localeModel.get("fail-msg"),
									secondAttemptFailMsg : Athena.localeModel.get("fail-msg"),
									pluginEvent : function(r, p, t, q, s) {
										return n.pluginEvent(r, p, t, q, s);
									}
								});
							}
							this["ih_" + b].init();
							this["ih_" + b].createImageHotSpots();
						}
					}
					this.setEvents();
					try {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-ih-" + b) ]);
					} catch (e) {
						debug(e);
					}
				} catch (c) {
					debug("Exception in IH");
					debug(c);
				}
			},
			handleRequestPalette : function(a, b) {
				if (b) {
					this.dispatch(Athena.PALETTE_UPDATE, a);
				} else {
					this.dispatch(Athena.PALETTE_SHOW, a);
				}
			},
			setScrollTo : function() {
				var a = this;
				this.interactivities.css("opacity", 0);
				$(this.interactivities[0]).css("opacity", 1);
				this.$el.scroll(function() {
					a.checkInteractivity();
				});
			},
			checkInteractivity : function() {
				var b, d = this.interactivities.length, a = this.$el.scrollTop(), e = Math.floor(a / this.itemPosition) + 1, c;
				c = $(this.interactivities[Number(e)]);
				if (c.length > 0) {
					Athena.transitions.animate(c, {
						opacity : 1
					}, this.scrollSpeed);
				}
				if (e >= d) {
					this.$el.unbind("scroll");
					for (b = 0; b < d; b++) {
						$(this.interactivities[b]).show().css("opacity", 1);
					}
					debug("ScrollTo: Disabled - For Activity " + this.ref);
				}
				for (b = 0; b > e; b++) {
					c = $(this.interactivities[b]);
					if (c.length > 0) {
						if (c.css("opacity") === 0) {
							c.css("opacity", 1);
						}
					}
				}
			},
			handleScrollEnabled : function() {
				var a = this, b;
				if (this.useScroll) {
					b = function() {
						a.activityHeight = a.$el.find(".activity").outerHeight();
						a.headerHeight = a.header.outerHeight();
						a.containerHeight = a.$el.outerHeight();
						a.scrollMax = a.activityHeight - a.containerHeight;
						a.itemPosition = Math.ceil((a.scrollMax + a.headerHeight) / a.interactivities.length);
						debug(a.activityHeight, a.headerHeight, a.containerHeight, a.scrollMax, a.itemPosition, a.interactivities.length, a.interactivities);
						a.checkInteractivity();
					};
					b();
					if (!a.$el.hasClass("visited") && typeof MathJax !== "undefined") {
						MathJax.Hub.Queue([ "Typeset", MathJax.Hub, a.$el.toArray()[0].querySelector(".activity"), function() {
							b();
							a.$el.addClass("visited");
						} ]);
					}
				}
			},
			prerender : function() {
				this.header = this.$el.find(".start-screen");
				this.title = this.$el.find("header.activity-header h1");
				this.subtitle = this.$el.find("header.activity-header h2");
				this.intro = this.$el.find("header.activity-header h3");
				this.$el.addClass(this.model.option.page.value);
				if (this.model.option.page.value === "engage") {
					this.header.height(Athena.WINDOW_HEIGHT);
				}
				this.title.html((this.model.contents.title.value === undefined) ? this.model.contents.title : this.model.contents.title.value);
				this.subtitle.html((this.model.contents.subtitle.value === undefined) ? this.model.contents.subtitle : this.model.contents.subtitle.value);
				this.intro.html((this.model.contents.intro.value === undefined) ? this.model.contents.intro : this.model.contents.intro.value);
				if (this.model.contents.title.objectId !== undefined) {
					this.title.attr("data-objectid", this.model.contents.title.objectId);
				}
				if (this.model.contents.subtitle.objectId !== undefined) {
					this.subtitle.attr("data-objectid", this.model.contents.subtitle.objectId);
				}
				if (this.model.contents.intro.objectId !== undefined) {
					this.intro.attr("data-objectid", this.model.contents.intro.objectId);
				}
				if (this.model.contents.background !== undefined) {
					var a;
					a = $('<div class="bg"></div>');
					a.css({
						"background-image" : "url(" + Athena.settingsModel.getImagePath() + this.model.contents.background.value + ")"
					});
					this.$el.append(a);
				}
			},
			reset : function() {
				var a;
				for (a in this) {
					if (this.hasOwnProperty(a)) {
						if (a.match(/ih_/g) || a.match(/ch_/g) || a.match(/cr_/g) || a.match(/sr_/g) || a.match(/ddo_/g) || a.match(/dds_/g) || a.match(/ddm_/g)
								|| a.match(/ftq_/g) || a.match(/fiq_/g) || a.match(/mdq_/g) || a.match(/mcq_/g) || a.match(/msq_/g) || a.match(/player_/g)) {
							this[a].reset();
						}
					}
				}
			},
			preset : function() {
				var c, b;
				for (c in this) {
					if (this.hasOwnProperty(c)) {
						try {
							if (c.match(/sr_/g) || c.match(/ftq_/g) || c.match(/ih_/g)) {
								if (Athena.xapiCollection.objectIds !== undefined && Athena.xapiCollection.objectIds[this.model.objectId] !== undefined) {
									b = Athena.xapiCollection.objectIds[this.model.objectId][this[c].model.objectId];
								}
								if (b !== undefined) {
									this[c].preset(b);
								}
							} else {
								if (c.match(/cr_/g) || c.match(/ch_/g)) {
									if (Athena.xapiCollection.objectIds !== undefined && Athena.xapiCollection.objectIds[this.model.objectId] !== undefined) {
										b = Athena.xapiCollection.objectIds[this.model.objectId][this[c].modelContent.objectId];
									}
									if (b !== undefined) {
										this[c].preset(b);
									}
								} else {
									if (c.match(/dd/g)) {
										if (Athena.xapiCollection.objectIds !== undefined && Athena.xapiCollection.objectIds[this.model.objectId] !== undefined) {
											b = Athena.xapiCollection.objectIds[this.model.objectId][this[c].modelData.objectId];
										}
										if (b !== undefined) {
											this[c].preset(b);
										}
									} else {
										if (c.match(/fiq_/g)) {
											if (Athena.xapiCollection.objectIds !== undefined && Athena.xapiCollection.objectIds[this.model.objectId] !== undefined) {
												b = Athena.xapiCollection.objectIds[this.model.objectId][this[c].input.objectId];
											}
											if (b !== undefined) {
												this[c].preset(b);
											}
										} else {
											if (c.match(/mdq_/g) || c.match(/mcq_/g) || c.match(/msq_/g)) {
												if (Athena.xapiCollection.objectIds !== undefined && Athena.xapiCollection.objectIds[this.model.objectId] !== undefined) {
													b = Athena.xapiCollection.objectIds[this.model.objectId][this[c].data.objectId];
												}
												if (b !== undefined) {
													this[c].preset(b);
												}
											}
										}
									}
								}
							}
						} catch (a) {
							debug("exception in preset method for " + c);
							debug(a);
						}
					}
				}
			},
			render : function() {
				HsmActivity_1_View.__super__.render.call(this);
				this.$el.attr("data-objectid", this.model.objectId);
				var a = this;
				this.initPlugins();
				this.useScroll = Athena.settingsModel.get("scroll-interactivity-show");
				this.scrollSpeed = Athena.settingsModel.get("scroll-interactivity-speed");
				if (this.useScroll) {
					this.interactivities = this.$el.find(".interactivity-container");
					this.handleScrollEnabled();
					this.setScrollTo();
				}
				Athena.xapiCollection.get(a.model.objectId, this.objectIds, function() {
					a.preset();
				});
			}
		});
var HsmActivity_2_View = Athena.CoreActivityView.extend({
	title : null,
	subtitle : null,
	intro : null,
	frame : null,
	initialize : function(b, a, c) {
		HsmActivity_2_View.__super__.initialize.call(this, b, a, c);
		this.prerender();
	},
	disableFunctions : function() {
		HsmActivity_2_View.__super__.disableFunctions.call(this);
	},
	enableFunctions : function() {
		HsmActivity_2_View.__super__.enableFunctions.call(this);
	},
	timedAudioHandlers : function(a) {},
	resetAudioHandlers : function(a) {},
	completedAudioHandlers : function(a) {},
	setEvents : function() {},
	clearEvents : function() {},
	startActivity : function() {},
	setContent : function() {
		HsmActivity_2_View.__super__.setContent.call(this);
	},
	setStep : function(a) {},
	changeStep : function(b, a) {},
	replayAction : function() {},
	prerender : function() {
		this.title = this.$el.find("header.activity-header h1");
		this.subtitle = this.$el.find("header.activity-header h2");
		this.intro = this.$el.find("header.activity-header h3");
		this.frame = this.$el.find(".content-frame");
		this.$el.addClass(this.model.option.page.value);
		this.title.html((this.model.contents.title.value === undefined) ? this.model.contents.title : this.model.contents.title.value);
		this.subtitle.html((this.model.contents.subtitle.value === undefined) ? this.model.contents.subtitle : this.model.contents.subtitle.value);
		this.intro.html((this.model.contents.intro.value === undefined) ? this.model.contents.intro : this.model.contents.intro.value);
		if (this.model.contents.title.objectId !== undefined) {
			this.title.attr("data-objectid", this.model.contents.title.objectId);
		}
		if (this.model.contents.subtitle.objectId !== undefined) {
			this.subtitle.attr("data-objectid", this.model.contents.subtitle.objectId);
		}
		if (this.model.contents.intro.objectId !== undefined) {
			this.intro.attr("data-objectid", this.model.contents.intro.objectId);
		}
	},
	render : function() {
		HsmActivity_2_View.__super__.render.call(this);
		var c, e, f, d, a = 1, g, j = Athena.settingsModel.hostName, h, b = Athena.settingsModel.get("pmt-path-" + Athena.settingsModel.locale);
		if (!String(j).match(/http:/g)) {
			j = "http://" + j;
		}
		if (Athena.settingsModel.param) {
			h = Athena.dlolocaleModel.data.customValues[this.ref];
			if (h !== undefined) {
				a = h[Athena.settingsModel.region + "-" + Athena.settingsModel.param.category + "-" + a];
			}
		}
		if (a === undefined) {
			a = this.model.contents.resource.value;
		}
		if (queryparams.OL !== "0") {
			g = "/HRWHoap/iseServlet.htm?&wid=" + a + ((queryparams.platform_assignment_id !== undefined) ? ("&platform_assignment_id=" + queryparams.platform_assignment_id) : "")
					+ ((queryparams.sif_assignment_id !== undefined) ? ("&sif_assignment_id=" + queryparams.sif_assignment_id) : "")
					+ ((queryparams.assignmentId !== undefined) ? ("&assignmentId=" + queryparams.assignmentId) : "");
		} else {
			if ((queryparams.platform_assignment_id === undefined && queryparams.assignmentId === undefined) && Athena.settingsModel.edition === "SE") {
				queryparams.wftype = 19;
			}
			g = "../../../../../../../wwtb/offlinedata/" + a + "/index.html?wid=" + a
					+ ((queryparams.platform_assignment_id !== undefined) ? ("&platform_assignment_id=" + queryparams.platform_assignment_id) : "")
					+ ((queryparams.sif_assignment_id !== undefined) ? ("&sif_assignment_id=" + queryparams.sif_assignment_id) : "")
					+ ((queryparams.pmtuid !== undefined) ? ("&uid=" + queryparams.pmtuid) : "") + ((queryparams.cid !== undefined) ? ("&cid=" + queryparams.cid) : "1")
					+ ((queryparams.classid !== undefined) ? ("&classid=" + queryparams.classid) : "")
					+ ((queryparams.RegistrationId !== undefined) ? ("&RegistrationId=" + queryparams.RegistrationId) : "")
					+ ((queryparams.LearningInstanceId !== undefined) ? ("&LearningInstanceId=" + queryparams.LearningInstanceId) : "")
					+ ((queryparams.knowledge_graph_id !== undefined) ? ("&knowledge_graph_id=" + queryparams.knowledge_graph_id) : "")
					+ ((queryparams.tr4iv !== undefined) ? ("&tr4iv=" + queryparams.tr4iv) : "") + ((queryparams.tr4en !== undefined) ? ("&tr4iv=" + queryparams.tr4en) : "")
					+ ((queryparams.file !== undefined) ? ("&file=" + queryparams.file) : "")
					+ ((queryparams.assignmentId !== undefined) ? ("&assignmentId=" + queryparams.assignmentId) : "")
					+ ((queryparams.wftype !== undefined) ? ("&wftype=" + queryparams.wftype) : "")
					+ ((queryparams.viewer_style !== undefined) ? ("&viewer_style=" + queryparams.viewer_style) : "");
			if (Athena.settingsModel.edition !== "SE") {
				g += "&role=teacher";
			}
		}
		this.frame.attr("src", g);
		if (typeof MathJax !== "undefined") {
			if (document.querySelector("header") !== null) {
				MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("header"), function() {
					var i;
				} ]);
			}
		}
	}
});
var HsmActivity_3_View = Athena.CoreActivityView.extend({
	title : null,
	subtitle : null,
	intro : null,
	buttonEnd : null,
	objectIds : [],
	initialize : function(b, a, c) {
		HsmActivity_3_View.__super__.initialize.call(this, b, a, c);
		this.prerender();
	},
	disableFunctions : function() {
		HsmActivity_3_View.__super__.disableFunctions.call(this);
		this.dispatch(Athena.PALETTE_HIDE);
	},
	enableFunctions : function() {
		HsmActivity_3_View.__super__.enableFunctions.call(this);
	},
	timedAudioHandlers : function(a) {},
	resetAudioHandlers : function(a) {},
	completedAudioHandlers : function(a) {},
	setEvents : function() {
		var a = this;
		this.buttonEnd.click(function() {
			a.dispatch(Athena.PALETTE_HIDE, 1);
			Athena.dlolocaleModel.setSelected(0);
			a.dispatch(Athena.ACTIVITY_CHANGED, 1);
			a.dispatch(Athena.MENUARROW_UPDATE);
			a.dispatch(Athena.MENU_CHANGE, 0);
			Athena.eventBus.trigger(Athena.NAVIGATION_CLICKED, {
				verb : "launched",
				result : "moveToFirst",
				intType : "navigation"
			}, null);
		});
	},
	clearEvents : function() {},
	startActivity : function() {},
	pluginEvent : function(d, a, f, c, e) {
		var b;
		if (typeof e !== "object") {
			Athena.eventBus.trigger(Athena.EVENT_TRACK, {
				verb : f,
				result : a,
				intType : c
			}, d);
		} else {
			debug("DP Value: " + e.value);
			e.activity = this.ref;
			debug("data for FTQ Object ID: " + this.$el.attr("data-objectid") + " data id: " + e.id + " value: " + e.value);
			b = Athena.xapiCollection.set(this.$el.attr("data-objectid"), e.id, e.value, function(g) {
				debug("SET COMPLETE", g);
			});
		}
		return b;
	},
	getMarkup : function(b, f, c) {
		var a, e = Athena.shellView.ahtml, d = $(e).filter(function() {
			var i, g, h;
			i = $(this).attr("class");
			if (i !== undefined) {
				if (i.split(" ")[0] === b) {
					h = i.split(" ")[0];
				}
			}
			return h;
		});
		a = b + "-" + f;
		d.addClass(a);
		c.append(d);
		return d;
	},
	setContent : function() {
		HsmActivity_3_View.__super__.setContent.call(this);
		if (this.model.contents.taskTitle !== undefined) {
			this.$el.find("h1.task-title").html(this.model.contents.taskTitle.value);
		} else {
			this.$el.find("h1.task-title").hide();
		}
		this.$el.find("h1.journal-title").html(this.model.contents.journalTitle.value);
		this.$el.find("h1.journal-eval-title").html(this.model.contents.evalTitle.value);
		this.$el.find("span.journal-summary").html(this.model.contents.evalSummaryDescription.value);
		this.$el.find("ul.journal-summary").html(this.model.contents.evalSummary.value);
		this.buttonEnd.find("span.label").html(this.model.contents.buttonComplete.value);
		try {
			MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id")) ]);
		} catch (b) {
			debug(b);
		}
		var a = this.model.interactivities;
		if (a.FTQ_2 === undefined) {
			this.initFTQ(0, a.FTQ_1, "journal");
			this.$el.find(".task-ftq").hide();
			this.objectIds.push(a.FTQ_1.objectId);
		} else {
			this.initFTQ(0, a.FTQ_1, "task");
			this.initFTQ(1, a.FTQ_2, "journal");
			this.objectIds.push(a.FTQ_1.objectId);
			this.objectIds.push(a.FTQ_2.objectId);
		}
		this.initMCQ(0, a.MCQ_1);
		this.objectIds.push(a.MCQ_1.objectId);
	},
	initFTQ : function(a, f, g) {
		var e, c, i, b, h = this;
		e = this.getMarkup("interactivity-ftq", a, this.$el.find("." + g + "-ftq"));
		c = this.$el.find(".interactivity-ftq-" + a);
		c.attr("data-objectid", f.objectId);
		i = {
			parentContainer : c,
			questionClass : "ftq_content",
			qImageClass : "image",
			headerClass : "questionHeader",
			saveBtnClass : "save-btn",
			saveBtnTxt : Athena.localeModel.get("save-button-text"),
			sendBtnClass : "sendToNoteBook-btn",
			sendToNoteBookTxt : "Send To NoteBook",
			symbolBtnContainerClass : "symbolContainer2",
			symbolBtnClass : "formula-btn",
			inputBoxClass : "overview",
			textAreaComp : "text-area-comp",
			scrollbarClass : "scrollbar",
			pluginEvent : function(l, j, n, k, m) {
				return h.pluginEvent(l, j, n, k, m);
			},
			imageSourcePath : Athena.settingsModel.getImagePath(),
			paletteCallback : function(j, k) {
				h.handleRequestPalette(j, k);
			},
			autoScrollTo : true
		};
		this["ftq_" + a] = new Athena.FreeTextQuestion();
		this["ftq_" + a].options(i, f, this);
		this["ftq_" + a].init();
		try {
			MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-ftq-" + a) ]);
		} catch (d) {
			debug(d);
		}
	},
	initMCQ : function(l, t) {
		var k = this, s, u, e, n, f, r, b, g, m, j, p, q, d, o, a, h, i;
		u = this.getMarkup("interactivity-mcq", l, this.$el.find(".journal-questions"));
		e = this.$el.find(".interactivity-mcq-" + l);
		e.attr("data-objectid", t.objectId);
		o = "choice-element-";
		h = false;
		n = {
			answerBoxSelect : "answer-box-select",
			answerBoxUnSelect : "answer-box-unselect",
			answerBoxCorrect : "answer-box-correct",
			answerBoxWrong : "answer-box-wrong",
			answerBoxShowMe : "answer-box-showme",
			select : "radio-btn-select",
			unselect : "radio-btn-unselect",
			correct : "radio-btn-correct",
			wrong : "radio-btn-wrong",
			showme : "radio-btn-showme"
		};
		if (String(t.contents.content_type.value) === "text") {
			e.find(".single-image-container").hide();
			i = e.find(".single-container");
		} else {
			e.find(".single-container").hide();
			i = e.find(".single-image-container");
		}
		if (Athena.settingsModel.getImagePath() !== null && Athena.settingsModel.getImagePath() !== undefined) {
			g = Athena.settingsModel.getImagePath();
		} else {
			g = "images/";
		}
		if (Athena.localeModel.get("button-check") !== null && Athena.localeModel.get("button-check") !== undefined) {
			m = Athena.localeModel.get("button-check");
		} else {
			m = "Check";
		}
		if (Athena.localeModel.get("button-tryagain") !== null && Athena.localeModel.get("button-tryagain") !== undefined) {
			j = Athena.localeModel.get("button-tryagain");
		} else {
			j = "Try again";
		}
		if (Athena.localeModel.get("button-showme") !== null && Athena.localeModel.get("button-showme") !== undefined) {
			p = Athena.localeModel.get("button-showme");
		} else {
			p = "Show me";
		}
		if (Athena.localeModel.get("button-myanswer") !== null && Athena.localeModel.get("button-myanswer") !== undefined) {
			q = Athena.localeModel.get("button-myanswer");
		} else {
			q = "My answer";
		}
		b = {
			imagePath : g,
			buttonCheckText : m,
			buttonTryAgainText : j,
			buttonShowMeText : p,
			buttonMyAnswerText : q,
			pluginEvent : function(x, v, z, w, y) {
				return k.pluginEvent(x, v, z, w, y);
			}
		};
		r = {
			container : i,
			mainContainer : e.find(".main-Container"),
			cloneContainer : "interaction_container",
			defaultItem : "choice-element",
			checkButtonObject : "check-btn",
			tryButtonObject : "try-again-btn",
			showMeButtonObject : "showme-btn",
			myAnswersButtonObject : "myanswer-btn",
			choiceBtnClassName : "choice-btn",
			choiceTextClassName : "choice-text",
			choiceIdPrefix : "choice-btn-id-",
			checkBtnActive : "active",
			questionObject : "question_container",
			feedBackTextClass : "feedback",
			styleClassNames : n,
			hint : "hint",
			replaceHTML : null,
			persistOnSelect : true,
			successMsg : Athena.localeModel.get("correct-msg"),
			failMsg : Athena.localeModel.get("fail-msg"),
			tryAgainMsg : Athena.localeModel.get("fail-msg"),
			showmeMsg : Athena.localeModel.get("showme-msg")
		};
		this["mcq_" + l] = new Athena.MultipleChoiceQuestion();
		this["mcq_" + l].options(t, r, b, o, "append", h, null);
		try {
			MathJax.Hub.Queue([ "Typeset", MathJax.Hub, document.querySelector("#" + this.$el.attr("id") + " .interactivity-mcq-" + l) ]);
		} catch (c) {
			debug(c);
		}
	},
	setStep : function(a) {},
	changeStep : function(b, a) {},
	replayAction : function() {},
	handleRequestPalette : function(a, b) {
		if (b) {
			this.dispatch(Athena.PALETTE_UPDATE, a);
		} else {
			this.dispatch(Athena.PALETTE_SHOW, a);
		}
	},
	reset : function() {
		debug("RESET");
		var a;
		for (a in this) {
			if (this.hasOwnProperty(a)) {
				if (a.match(/mcq_/g) || a.match(/ftq_/g)) {
					this[a].reset();
					if (a.match(/mcq_/g)) {
						this[a].checkButton.hide();
					}
				}
			}
		}
	},
	preset : function() {
		var b, a;
		for (b in this) {
			if (this.hasOwnProperty(b)) {
				if (b.match(/ftq_/g)) {
					if (Athena.xapiCollection.objectIds !== undefined && Athena.xapiCollection.objectIds[this.model.objectId] !== undefined) {
						a = Athena.xapiCollection.objectIds[this.model.objectId][this[b].model.objectId];
						if (a !== undefined) {
							this[b].preset(a);
						}
					}
				} else {
					if (b.match(/mcq_/g)) {
						if (Athena.xapiCollection.objectIds !== undefined && Athena.xapiCollection.objectIds[this.model.objectId] !== undefined) {
							a = Athena.xapiCollection.objectIds[this.model.objectId][this[b].data.objectId];
							if (a !== undefined) {
								this[b].preset(a);
							}
						}
					}
				}
			}
		}
	},
	prerender : function() {
		this.title = this.$el.find("header.activity-header h1");
		this.subtitle = this.$el.find("header.activity-header h2");
		this.intro = this.$el.find("header.activity-header h3");
		this.buttonEnd = this.$el.find(".button-end");
		this.$el.addClass(this.model.option.page.value);
		this.title.html((this.model.contents.title.value === undefined) ? this.model.contents.title : this.model.contents.title.value);
		this.subtitle.html((this.model.contents.subtitle.value === undefined) ? this.model.contents.subtitle : this.model.contents.subtitle.value);
		this.intro.html((this.model.contents.intro.value === undefined) ? this.model.contents.intro : this.model.contents.intro.value);
		if (this.model.contents.title.objectId !== undefined) {
			this.title.attr("data-objectid", this.model.contents.title.objectId);
		}
		if (this.model.contents.subtitle.objectId !== undefined) {
			this.subtitle.attr("data-objectid", this.model.contents.subtitle.objectId);
		}
		if (this.model.contents.intro.objectId !== undefined) {
			this.intro.attr("data-objectid", this.model.contents.intro.objectId);
		}
	},
	render : function() {
		var a = this;
		HsmActivity_3_View.__super__.render.call(this);
		this.$el.attr("data-objectid", this.model.objectId);
		this.setEvents();
		this.buttonEnd.show();
		Athena.xapiCollection.get(a.model.objectId, this.objectIds, function() {
			a.preset();
		});
		debug(this.objectIds);
	}
});