/**
 * @author Lakshay Ahuja
 */
MG.LearnosityOperator = function () {
    this.superClass = MG.BaseOperator.prototype;
    this.lnsBtnReference = null;
    this.totalNavigatorWidth = 0;
    this.learnosityPanelRef = null;
    this.panel = $( "#learnosityPanel" );
    this.panelScrollableDiv = $( "#learnosityPanelScrollableDiv" );
    this.footer = $( "#lnsQuestionFooter" );
    this.header = $( "#learnosityPanelHeader" );
    this.questionNavigator = $( "#lnsQuestionNavigator" ); 
}

MG.LearnosityOperator.prototype = new MG.BaseOperator();

MG.LearnosityOperator.prototype.attachComponent = function ( objComp ) 
{
	var objThis = this;
    var strType = objComp.element.attr( AppConst.ATTR_TYPE ).toString();
    
    if ( objComp == null )
        return;

    
    switch ( strType ) 
    {
        case "learnosityPanel":
            objThis.learnosityPanelRef = objComp;
            objThis.addEventListeners( objComp );
            break;
            
        case AppConst.SCROLL_VIEW_CONTAINER:
            $( objComp ).bind( objComp.events.PAGE_RENDER_COMPLETE, function () 
            {
                objThis.scrollViewRenderCompleteHandler( objComp );
            });
            
            break;

        default:
            //console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
            break;
    }

    return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
};

MG.LearnosityOperator.prototype.addEventListeners = function ( objComp )
{
	var objThis = this;
	
	/* Close annotation bar */
	$( objComp.element ).unbind( 'click' ).bind( 'click', function () 
	{
        $( "#annotation-bar" ).css( 'display', 'none' );
    });

	/* Resize */
    $( window ).bind( 'resize', function ( e ) 
    {
        if( objThis.footer.css( "display" ) != "block" )
 	  	{
 	  		return;
 	  	}
 	  	$( document ).trigger( "updateLearnosityViewSize" );
    });

	/* Orientation change */
    $( window ).bind( 'orientationchange', function () 
    {
 	  	 setTimeout( function ()
 	  	 {
 	  	  	$( "#lnsQuestionNavigator" ).width( objThis.totalNavigatorWidth + 10 );
 	  	  	$( document ).trigger( "updateLearnosityViewSize" );
 	  	 }, 1500 );  
 	});
 	
 	/* Closing the Question Answer Panel */
    $( objComp.element ).find( '[ id = learnosityPanelCloseBtn ]' ).unbind( 'click' ).bind( 'click', function () 
    {
    	 $($( "#learnosityPanel iframe" )[0]).attr( "src", "" );
    	 objThis.panel.slideUp( 500, "easeInOutExpo" );
    });

    /* Minimize , Maximize Panel */
    $( objComp.element ).find( '[ id = learnosityPanelMinimizeIcon ]' ).unbind( 'click' ).bind( 'click', function () 
    {
    	if( $( this ).hasClass( "minimizeQAPanel" ) )
    	{
    		$( this ).removeClass( "minimizeQAPanel" ).addClass( "maximizeQAPanel" );
    		objThis.panelScrollableDiv.css( "display", "none" );
    		objThis.footer.css( "display", "none" );
    	}
    	else
    	{
    		$( this ).removeClass( "maximizeQAPanel" ).addClass( "minimizeQAPanel" );
    		objThis.panelScrollableDiv.css( "display", "block" );
    		objThis.footer.css( "display", "block" );
    	}
    });

	/* Navigate to next  */
    $( objComp.element ).find( "#lnsNextQuestionArrow" ).bind( "click", function ()
    {
		var i = $( "#lnsQuestionNavigator #lnsQuestionDots .quesCurrentCircle" ).index();
		if( $( "#lnsQuestionNavigator #lnsQuestionDots .quesCurrentCircle" ).siblings().length > i )
		{
			i = i + 1;
		}
		else
		{
			i = 0;
		}
		
		$( "#lnsQuestionNavigator #lnsQuestionDots .quesCurrentCircle" ).removeClass( "quesCurrentCircle" );
		var idStr = "#lnsQuestionNavigator #lnsQuestionDots #lnsPanelques" + ( i + 1 );
		$( idStr ).trigger( "click" );
		$( idStr ).addClass( "quesCurrentCircle" );
	});
    
    /* Navigate to previous */    	
	$( objComp.element ).find( "#lnsPrevQuestionArrow" ).bind( "click", function ()
	{
		var j = $( "#lnsQuestionNavigator #lnsQuestionDots .quesCurrentCircle" ).index();
		if( j > 0 )
		{
			j = j - 1;
		}
		else
		{
			j = $( "#lnsQuestionNavigator #lnsQuestionDots .quesCurrentCircle" ).siblings().length;
		}
		
		$( "#lnsQuestionNavigator #lnsQuestionDots .quesCurrentCircle" ).removeClass( "quesCurrentCircle" );
		var idStr = "#lnsQuestionNavigator #lnsQuestionDots #lnsPanelques" + ( j + 1 );
		$( idStr ).trigger( "click" );
		$( idStr ).addClass( "quesCurrentCircle" );
	});
};

MG.LearnosityOperator.prototype.scrollViewRenderCompleteHandler = function( objComp ) 
{
    var objThis = this;
    var arrPageComps = $( objComp.element ).find( '[ data-role="pagecomp" ]' );
    var objPageRole = null;
    for ( var i = 0; i < arrPageComps.length; i++ ) 
    {
        objPageRole = $( arrPageComps[i] ).data( "pagecomp" );
        try {
        	$( objPageRole ).bind( objPageRole.events.PAGE_CONTENT_LOAD_COMPLETE, function( event1 ) 
        	{
        	    if (($(event1.currentTarget.element).find('[ type="widget:learnosity" ]')[0] != undefined) ||
        		 	($(event1.currentTarget.element).find('[ type="widget:learnositystatus" ]')[0] != undefined))
        		{
                    var _page = $(event1.currentTarget.element);
                    objThis.lnsBtnReference = $(event1.currentTarget.element).find('[ type="widget:learnosity" ]');
        			if (EPubConfig.LearnosityGroupAsQTI == true) {
        			    $(objComp.element).find('[type = "widget:learnosity"]').attr('type', 'learnosityblank');
        			    $(objComp.element).find('[type = "widget:learnositystatus"]').removeClass().addClass('learnosityqtigroup');
        			}
                     var clickHandler = function( e) 
                     {
                            /* Header( in format of 'Activities : Page { page number }' ) */
                            $("#learnosityPanel #learnosityPanelPageNumber").html('<span class="questionPanelPageSpan">' +
                            GlobalModel.localizationData["LEARNOSITY_PANEL_PAGE_PREFIX"] +
                            '</span> ' + $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagebreakvalue'));

                            /* keep height as 50% if less then 35% or greater then 100% */
                            var _height = parseInt(EPubConfig.Learnosity_PanelHeight);
                            if (isNaN(_height) || _height < 35 || _height > 100) {
                                _height = 50;
                            }
                             

                            /*This event is handled in QuestionAnswerPanelComp to close the QAPanel if it is already opened.
                            Same event will be used if any panel is required to be closed on opening of LearnostiyPanel */
                            $(document).trigger("learnosityPanelOpening");
                            objThis.footer.css("display", "block");
                            /* remove all question dots and create new ones */
                            $("#lnsQuestionNavigator #lnsQuestionDots div").remove();

                            /* remove iframe too */
                            $("#learnosityPanel iframe").remove();
                            objThis.panel.css("height", window.height * _height / 100);
                            $("#learnosityPanelScrollableDiv").css("height", window.height * _height / 100 -
                                                                                $("#lnsQuestionFooter").height() -
                                                                                $("#learnosityPanelHeader").height());
                            var learnosityType = '[ type="widget:learnosity" ]';
                            if (EPubConfig.LearnosityGroupAsQTI == true) {
                                learnosityType = '[ type="learnosityblank" ]';
                            }
                            if (e.attr("type") == "widget:learnositystatus")/* single hotspot */ {
                                var len = 1;
                            }
                            else /* group */ {
                                var len = e.siblings(learnosityType).length + 1;

                                var _totalItems = $(e.parent()).find(learnosityType);
                                var _sibItems = e.siblings(learnosityType);

                                var _itemArr = [];
                                _sibItems.push(e[0]);
                                for (var k = 0; k < _totalItems.length; k++) {
                                    for (var m = 0; m < _sibItems.length; m++) {
                                        if (_sibItems[m] == _totalItems[k]) {
                                            _itemArr.push(_sibItems[m]);
                                        }
                                    }
                                }
                            }

                            if (len == 1)/* do not show next/back buttons( on footer ) if only one hotspot */ {
                                $("#lnsNextQuestionArrow").css("display", "none");
                                $("#lnsPrevQuestionArrow").css("display", "none");
                            }
                            else/* else show */ {
                                $("#lnsNextQuestionArrow").css("display", "block");
                                $("#lnsPrevQuestionArrow").css("display", "block");
                            }
                            /* each dot at the bottom( aka footer ) will represent a hotspot. adding them here */
                            for (var i = 0; i < len; i++) {
                                if (e.attr("type") == "widget:learnositystatus") {
                                    var _item = e;
                                }
                                else {
                                    var _item = $(_itemArr[i]);
                                }
                                $("#lnsQuestionNavigator #lnsQuestionDots").append('<div data-role="buttonComp" type="lnsQuesDot" data-id="' +
                                                                                    _item.attr("interactionid") + '" id="lnsPanelques' + (i + 1) +
                                                                                    '" class="questionPanelquesDots quesPendingCircle" style="margin-left:' +
                                                                                    5 + 'px;margin-right:' + 5 +
                                                                                    'px "><div class="questionBulletInnerShadow"></div></div>');
                                if (e.index() == i || len == 1) {
                                    $("#lnsQuestionNavigator #lnsQuestionDots .questionPanelquesDots").last().addClass("quesCurrentCircle");
                                }
                            }
                            /* height, width etc calculations */
                            var val = (EPubConfig.ReaderType != '6TO12' ? 14 : 10);
                            var totalNavigatorWidth = (i + 1) * ($("#lnsPanelques" + (i + 1)).outerWidth() + 2 * val);

                            $("#lnsQuestionNavigator").width(totalNavigatorWidth + 10);
                            objThis.totalNavigatorWidth = totalNavigatorWidth;
                            var ref = $("#bookContentContainer").data('scrollviewcomp');
                            ref.updateDesignForPageView("panelPosition", GlobalModel.currentScalePercent);
                            var activityFileExtension = EPubConfig.LearnosityActivityFormat == "json" ? ".json" : ".xml";
                            /* just replace the URL of the current iFrame on click on any hotspot dot at the footer */
                            $("#lnsQuestionNavigator #lnsQuestionDots").find("[ type='lnsQuesDot' ]").bind("click", function (event) {
                                $("#lnsQuestionNavigator #lnsQuestionDots").find(".quesCurrentCircle").removeClass("quesCurrentCircle");
                                $(event.currentTarget).addClass("quesCurrentCircle");
                                $("#learnosityPanel  iframe").css("height", "0px");
                                var str = EPubConfig.LearnosityActivityURL + "?Actxml=" + $(this).attr('data-id') + activityFileExtension;
                                $("#learnosityPanel iframe").attr("src", getPathManager().getLearnosityURL() + $(this).attr('data-id') + activityFileExtension);
                            });

                            /* adding iframe to div */
                            $("#learnosityPanel #learnosityPanelScrollableDiv").append("<iframe class='lnsIFrame' src='" +
                                                                                        getPathManager().getLearnosityURL() +
                                                                                        e.attr('interactionid') + activityFileExtension + "'></iframe>");
                            objThis.panelScrollableDiv.css("display", "block");
                            $("#learnosityPanelMinimizeIcon").removeClass("maximizeQAPanel").addClass("minimizeQAPanel");

                            /* set height, width etc on ifame load.*/
                            var _iFrame = $("#learnosityPanel iframe").last();
                            _iFrame.load(function (event1) {
                                setTimeout(function () {
                                    if ($("#learnosityPanel .lnsIFrame").css("display") != "none") {
                                        /* This event is handled in learnosityPanelComp.js */
                                        $(document).trigger("updateLearnosityViewSize");
                                    }
                                    setTimeout(function () {
                                        if ($("#learnosityPanel .lnsIFrame").css("display") != "none") {

                                            $(document).trigger("updateLearnosityViewSize");
                                        }
                                    }, 2000);
                                }, 1000);

                                /*_iFrame.contents().bind( "resize" , function ()
                                {
                                    _iFrame.contents().unbind( "resize" );
                                    setTimeout( function ()
                                    {
                                        if( $( "#learnosityPanel .lnsIFrame" ).css( "display" ) != "none" )
                                        {
                                             
                                            $( document ).trigger( "updateLearnosityViewSize" );
                                        }
                                    }, 2000 );
                                })*/
                            });
                            objThis.panel.slideDown(500, "easeInOutExpo");
                    };
                     
                     /* click handler on learnosity hotspots on page */
                     $(event1.currentTarget.element).find('[ type="widget:learnositystatus" ]').bind("click", function (evt)
                     {
                    	if( $( evt.target ).attr( "interactionId" ) == undefined && $( evt.target ).attr( "id" ) != undefined )
	                	{
	                	 	 // group type
                    	    // $( _page.find( '[ interactiongroupid="' +  $( evt.target ).attr( "id" ) + '_interaction" ]' ).children()[0] ).trigger( "click" );
                    	    clickHandler($($(_page.find('[ interactiongroupid="' + $(evt.target).attr("id") + '_interaction" ]')).children()[0]));
	                	 	 return;
	                	}
	                	else if( $( evt.target ).attr( "interactionId" ) != undefined )
	                	{
                            clickHandler($(evt.target));
	                	}
                    });
        		}
        		else
        		{
        			/**
                     * There is no learnosity on the page. So return;
                     */
                    return;
        		}
        		$(objThis.lnsBtnReference).bind('click', function (e) {
        		    e.preventDefault();
        		    e.stopPropagation();
        		    clickHandler($(e.target));
        		} );
        	});
        	
        }
        catch( e ) 
        {

        }
      }
     };