/**
 * @author Magic s/w pvt ltd
 */

/* Init function */
( function ()
  {
    
  } 
)();

MG.BookmarkOperator = function ( bookmarkTitle, callback )
{
	
	this.currentBookmarkRef = "";
    this.timerCreateBookMark = null;

    this.createBookmarkObject = function ( bookmarkTitle )
	{
		var _date = new Date();
	    var _month = _date.getMonth() + 1;
	    var _bookmarkID = $( this.currentBookmarkRef ).attr( 'id' );
	    var _dateOfCreation = ( ( '' + _month ).length < 2 ? '0' : '' ) + 
	    					  _month + '/' + ( ( '' + _date.getDate() ).length < 2 ? '0' : '' ) +
	    					  _date.getDate() + '/' + _date.getFullYear().toString().substr( 2, 2 );
	 	var _arrObj = {};
	    _arrObj.title = bookmarkTitle;
	    _arrObj.id = _bookmarkID;
	    _arrObj.date = _dateOfCreation;
	    _arrObj.timeMilliseconds = _date;
	    _arrObj.tempID = $.now();
	    _arrObj.tempStatus = "insert";
	    _arrObj.page = _bookmarkID.replace( 'bookmarkStrip_', '' );

		if (GlobalModel.HMHPlayerEmbedded) {
			_arrObj.pageIndex = GlobalModel.pageBrkValueArr.indexOf(_arrObj.page);
		}

	    return _arrObj;
	}    
}


/*----------------------------------------------------------------------------------------------------------------------------------------------
 *
 * 														Prototype functions starts
 *  
 *---------------------------------------------------------------------------------------------------------------------------------------------*/

MG.BookmarkOperator.prototype = new MG.BaseOperator();


/*
 * This function will handle all the events related to bookmark components.
 */
MG.BookmarkOperator.prototype.attachComponent = function ( objComp )
{
	var objThis = this;
	var _type = objComp.element.attr( AppConst.ATTR_TYPE ).toString();
    
    if ( objComp == null ) 
    {
    	return;
    }
    
    switch ( _type )
    {
     
        case AppConst.SCROLL_VIEW_CONTAINER:
            
            var _bookContentContainer2 = $( objComp.element[ 0 ] ).find( '[ id = "bookContentContainer2" ] ' )[ 0 ];
            
            /* 
             * PAGE_RENDER_COMPLETE ( pagerendercomplete ) is triggerd from scrollviewcomp when the page rendering completes.
             */
            $( objComp ).bind( objComp.events.PAGE_RENDER_COMPLETE, function ()
            {             
            	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
            	{
            		$( _bookContentContainer2 ).find( '[ type = addRemoveBookmarkStrip ]' ).addClass('ui-disabled').attr( "aria-disabled", true );
            	}
            	
            	$( objComp ).bind( 'showBookmarkStrip' , function(){
            		$( _bookContentContainer2 ).find( '[ type = addRemoveBookmarkStrip ]' ).removeClass('ui-disabled').attr( "aria-disabled", false );
            	});
            	
            	$( objComp ).bind( 'hideBookmarkStrip' , function(){
            		$( _bookContentContainer2 ).find( '[ type = addRemoveBookmarkStrip ]' ).addClass('ui-disabled').attr( "aria-disabled", true );
            	});
            	
                $( _bookContentContainer2 ).find( '[ type = addRemoveBookmarkStrip ]' ).bind( 'click', function( e )
                {
                	if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
                	{
                		e.preventDefault();
                		return;
                	}
                	e.stopPropagation();
                    objThis.currentBookmarkRef = this;
                    
                    /*
                     * Check if page is not bookmarked. If so then create a new one.
                     */
                     
                    if ( $( this ).hasClass( 'btnBookmarkComp-off' ) ) 
                    {
                    	/* 
                    	 * For 2to5 and prekto1 only 5 bookmarks will be saved
                    	 * Remove oldest bookmark and create new one 
                    	 */
                    	if ( GlobalModel.bookMarks.length >= 5 && EPubConfig.ReaderType.toUpperCase() != AppConst.GRADE_6TO12 ) 
                    	{
                            var _arrFirstElementId = GlobalModel.bookMarks[ 0 ].id;
                            objThis.deleteBookmarkforPage( _arrFirstElementId );
                    	}
                        objThis.saveBookmarkForPage( GlobalModel.localizationData[ 'PAGE_TEXT_FREE' ] + 
                        							 $( this ).attr( 'id' ).replace( 'bookmarkStrip_', '' ) );
                    }
                    else /* Delete if bookmark exist */
                    {
                        objThis.deleteBookmarkforPage( $( this ).attr( "id" ) );
                    }
                });
            });
            break;
            
        case AppConst.BOOKMARK_SECTION_PANEL:
			
            _bookmarkSectionPanel = $( objComp.element );
            
            /* 
             * Triggered from bookmarkComp from function onPanelOpen. 
             * This block adds the bookmark list into the panel. 
             */
            $( objComp ).bind( Event.Bookmark.AddDataToBookmarkSectionPanel, function( e, bookmarkData )
            {
                $( _bookmarkSectionPanel.find( '[ id = "addedBookmarks" ]' )[ 0 ] ).html( bookmarkData );
                if( $.trim( _bookmarkSectionPanel.find('[ id = "addedBookmarks" ]' ).html() ) == "" )
                {
					$( _bookmarkSectionPanel.find( '[ id = "blankBookmarkPanelText" ]' ) ).css( "display", "block" );
					$( _bookmarkSectionPanel.find( '[ id = "bookMarkSortBy" ]' ) ).css( "display", "none" );
				}
				else
				{
					$( _bookmarkSectionPanel.find( '[ id = "blankBookmarkPanelText" ]' ) ).css( "display", "none" );
					
					if( EPubConfig.ReaderType.toLowerCase() == "6to12" )
					{
						$( _bookmarkSectionPanel.find( '[ id = "bookMarkSortBy" ]' ) ).css( "display", "block" );
					}
               }
               $( $( objComp.element ).find( '[ type = bookmarkData ]' ) ).bind( "click", function()
               {
                	if( EPubConfig.pageView != AppConst.SCROLL_VIEW_CAMEL_CASING )
                    	objThis.doFunctionCall( AppConst.SCROLL_VIEW_CONTAINER, 'resetZoom' );
                    
                    $( objComp.element ).trigger( Event.Bookmark.LoadPageThroughBookmarkPanel, this );
                });
            });
            
            break;
            
          default:
  				// do nothing      
            break;
    }
    
    return MG.BaseOperator.prototype.attachComponent.call( this, objComp );
}

/*
 * Creates new bookmark and sends call to server via ServiceManager
 */
MG.BookmarkOperator.prototype.saveBookmarkForPage = function( bookmarkTitle )
{
    var objThis = this;
    var _arrObj = objThis.createBookmarkObject ( bookmarkTitle );
    var _len = GlobalModel.bookMarks.length;
    
    for( var i = 0; i < _len; i++ )
    {
    	if( GlobalModel.bookMarks[ i ].id == _arrObj.id )
    	{
    		GlobalModel.bookMarks[ i ].tempStatus = "insert";
    		break;
    	}
    }
    GlobalModel.bookMarks.push( _arrObj );
    
   	objThis.timerCreateBookMark = setTimeout( function () 
   	{
   		/*
   		 * Send call to server via ServiceManager for new bookmark creation.
   		 * In callback update the annotationID.
   		 */
        ServiceManager.Annotations.create( _arrObj, 1, function( _data ) 
        {
            var _len = GlobalModel.bookMarks.length;
            var _flag = -1;
            for ( var j = 0; j < _len; j++ ) 
            {
                if ( _arrObj.id == GlobalModel.bookMarks[ j ].id ) 
                {
                    _flag = 1;
                    GlobalModel.bookMarks[ j ].annotation_id = _data.annotation_id;
                    GlobalModel.bookMarks[ j ].tempID = -1;
                    break;
                }
            }
            if( _flag == -1 )
            {
                ServiceManager.Annotations.remove( _data.annotation_id, 1 );
            }
        });
    }, 500 );
    
    $( objThis.currentBookmarkRef ).trigger( Event.Bookmark.SaveBookmarkForPage );
}


MG.BookmarkOperator.prototype.deleteBookmarkforPage = function( bkmarkRef )
{
    var objThis = this;
    var _bookmarkID = bkmarkRef;
    var _annotationID = 0;
    /* 
     * Remove the object from GlobalModel.Bookmark array 
     */
    for ( var i = 0; i < GlobalModel.bookMarks.length; i++ ) 
    {
        if ( GlobalModel.bookMarks ) 
        {
            if ( GlobalModel.bookMarks[ i ].id == _bookmarkID ) 
            {
            	_annotationID = GlobalModel.bookMarks[ i ].annotation_id;
                GlobalModel.bookMarks.splice( i, 1 );
            }
        }
    }
    
    if( objThis.timerCreateBookMark )
    {
        clearTimeout( objThis.timerCreateBookMark );
    }
    
    if ( ( _annotationID != undefined ) && ( _annotationID != 0 ) ) 
    {
        ServiceManager.Annotations.remove( _annotationID, 1 );//ServiceManager.deleteAnnotation( _annotationID, 1 );
    }
    
    try
    {
        if( $( "#" + bkmarkRef ).length >= 1 )
        {
            $( "#" + bkmarkRef ).trigger( Event.Bookmark.DeleteBookmarkForPage );
        }
    }
    catch( e )
    {
        console.log( e )
    }
}

/*----------------------------------------------------------------------------------------------------------------------------------------------
 *
 * 															Prototype functions ends
 *  
 *---------------------------------------------------------------------------------------------------------------------------------------------*/
