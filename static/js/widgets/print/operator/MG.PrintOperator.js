/**
 * @author yogendra.chauhan
 */

$(document).ready(function(){

MG.PrintOperator = function() {
	this.superClass = MG.BaseOperator.prototype;
	this.appModal=null;
	this.objPrintComp = null;
	this.objPrintPanelComp = null;
};

MG.PrintOperator.prototype = new MG.BaseOperator();

MG.PrintOperator.prototype.attachComponent = function(objComp){

	var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
	var objThis = this;
	switch (strType) {
	
		case AppConst.PRINT_COMP:
			objThis.objPrintComp = (objComp.element);
			$(objComp.element).bind("clicked", function(e){
				e.stopPropagation();
				objThis.doFunctionCall(AppConst.PRINT_COMP, "setJournalData",objThis.appModal.getJournalData());
				//objThis.doFunctionCall(AppConst.PRINT_PANELCOMP, "showPanel");
				$(objComp.element).removeClass().addClass("btnPrintCompSelected");
				PopupManager.addPopup(objThis.objPrintPanelComp, objComp, {isModal:false, hasPointer: true, isCentered: false, positionAt: "bottom",isSetPosition: true});
			});
			break;
		case AppConst.PRINT_PANELCOMP:
				objThis.objPrintPanelComp = objComp.element;
				$(objComp.element).bind("print_clicked", function(e , printOption){
					e.stopPropagation();
					objThis.doFunctionCall(AppConst.PRINT_COMP, "doPrint", printOption);
				});
				
				$(objComp.element).bind("panel_hidden", function(e){
					PopupManager.removePopup();
				});
				case AppConst.CURR_PAGE:
				objThis=this;
				    $(objComp.element).find("[type='radio']").parent().bind("click", function(e){
						e.stopPropagation();
						$(this).removeClass().addClass('radioselected');
						var next_radio = $($($(this).parent().parent().siblings('div')[0]).children()[0]).children();
						$(next_radio).removeClass().addClass('radio')
						
						});
			break;
		default:
			//console.error('### ERROR: [' + strType + '] -- Either "type" is not defined or Invalid operator is assigned');
			break;
	}
	
	return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
};

MG.PrintOperator.prototype.initialize = function(objOperatorManager){
	this.objOperatorManager = objOperatorManager;
	this.appModal = this.objOperatorManager.getAppModel();
//	var objAppModel = objThis.doFunctionCall(AppConst.PAGE_SHOWROOM, '_getAppModel');
				
};

});