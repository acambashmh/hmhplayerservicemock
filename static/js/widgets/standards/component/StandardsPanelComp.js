/**
 * @author shashank.shrivastava
 * This widget manages the standards panel view
 */

var standardScroll;

(function($, undefined) {
	$.widget("magic.StandardsPanelComp", $.magic.tabViewComp, {
		objPreviousNodeDetails : null,

		events : {
			STANDARD_ITEM_CLICK : "standardItemClick",
			ON_MEDIA_STANDARD : "onMediaStandard",
			ON_WEB_STANDARD : "onWebStandard"
		},

		options : {
			page_arr_standards : [],
			isSetArr : false,
			standardsData : null,
			standardsDescData : null,
		},

		member : {
			standardsData : null,
			standardsDataDesc : null,
			pageData: null,
			standardPopulateCheckEnabled : true
		},

		/**
		 * This function is called upon widget creation
		 * @param none
		 * @return void
		 */
		_create : function() {
			$.extend(this.options, this.element.jqmData('options'));
		},

		/**
		 * This function is called upon widget initialization
		 * @param none
		 * @return void
		 */
		_init : function() {
			$.magic.tabViewComp.prototype._init.call(this);
			var objClassRef = this;
			var objPathManager = getPathManager();
			var strSrcId = objClassRef.options.src;
			var strSrcPath = objPathManager.getFilePath(strSrcId);

			$(window).resize(function() {
				objClassRef.updateViewSize();
			});

			$(window).orientationchange(function() {
				objClassRef.updateViewSize();
			});

			if(EPubConfig.Standards_isAvailable) {
				$(this.element).unbind("tabDisplayChanged panelOpened").bind("tabDisplayChanged panelOpened", function() {
					$("#StandardsListContainer").html('<div id="loaderContainer" style="width: 100%;" align="center"><div class="pagePreloader"></div></div>');
					if(objThis.member.pageData == null ) {
						setTimeout(function() {
							var objPathManager = getPathManager();
							var descFileName = EPubConfig.standardsDescriptionXMLPath;
							var urlDesc = objPathManager.getStandardsPath(descFileName);
							objThis.member.standardPopulateCheckEnabled = true;
							$.ajax({
							    type: "GET",
							    url: urlDesc,
							    contentType: "plain/text",
							    dataType: 'text',
							    success: function (objData1) {
							        objData1 = objData1.replace(/xhtml:/gi, "");
							        objClassRef.options.standardsDescData = objData1;
							        objClassRef.onPanelOpen();
							    },
							    error: function (e) {
							        $("#StandardsListContainer").html('<div style="margin-top:5px;">' + GlobalModel.localizationData["ERROR_IN_FETCHING_DATA"] + '</div>')
							    }
							})
							},500);
						
					} else {
						objClassRef.onPanelOpen();					
					}
				});
			}

			$(this.element).unbind("panelClosed").bind("panelClosed", function() {
				objThis.options.standardsData = null;
				objClassRef.options.standardsDescData = null;
				objClassRef.onPanelClosed();
			});

			if(EPubConfig.Standards_isAvailable) {
				objClassRef.loadStandardsContent();
			}
		},

		/**
		 * This function is to handle the enable/disable status of standards-launcher button.
		 */
		toggleBtnState : function() {
			var objThis = this;
			var iPageIndex = parseInt($($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence'));
			var currPage = GlobalModel.pageBrkValueArr[iPageIndex];
			var nextPage = GlobalModel.pageBrkValueArr[iPageIndex + 1];
			var standardArr = this.options.page_arr_standards;
			if (EPubConfig.pageView != 'doublePage') {
				if (standardArr.indexOf(currPage) != -1) {
					$("#btnStandardsPanel").data('buttonComp').enable();
					$("#btnStandardsPanel").removeClass('ui-disabled');
					$("#btnStandardsPanel").attr("disabled", false);
				} else {

					$("#btnStandardsPanel").addClass('ui-disabled');
					$("#btnStandardsPanel").attr("disabled", true);
					$("#btnStandardsPanel").data('buttonComp').disable();
				}
			} else {
				if (standardArr.indexOf(currPage) != -1 || standardArr.indexOf(nextPage) != -1) {
					$("#btnStandardsPanel").data('buttonComp').enable();
					$("#btnStandardsPanel").removeClass('ui-disabled');
					$("#btnStandardsPanel").attr("disabled", false);
				} else {

					$("#btnStandardsPanel").addClass('ui-disabled');
					$("#btnStandardsPanel").attr("disabled", true);
					$("#btnStandardsPanel").data('buttonComp').disable();
				}
			}
		},

		/**
		 * This function loads the content of standards.json in an object through ajax call.
		 */
		loadStandardsContent : function() {
			var objThis = this;
			var objPathManager = getPathManager();
			var fileName = EPubConfig.standardsXMLPath;
			var descFileName = EPubConfig.standardsDescriptionXMLPath;
			var strUri = objPathManager.getStandardsPath(fileName);
			var type = "GET";
			var url = strUri;
			var urlDesc = objPathManager.getStandardsPath(descFileName);
			if (objThis.options.standardsData == null) {
				$.getJSON(url, function(objData) {
					objThis.options.standardsData = objData;
					if (objData.standards.pages) {
						if (objData.standards.pages.page) {
							if (objData.standards.pages.page.length) {
								for (var i = 0; i < objData.standards.pages.page.length; i++) {
									objThis.options.page_arr_standards.push(objData.standards.pages.page[i].number);
								}
							} else {
								objThis.options.page_arr_standards.push(objData.standards.pages.page.number);
							}
						}
					}
					//console.log("Shashank " + objThis.options.page_arr_standards)
				})
				.error(function() { 
				          $("#StandardsListContainer").html('<div style="margin-top:5px;">'+GlobalModel.localizationData["ERROR_IN_FETCHING_DATA"]+'</div>')
				});
			}
		},

		standardsLoadComplete : function(standardsData, standardsDescData) {
			var objThis = this;
			var strXMLStandards;
			var strXMLStandardsDesc;
			if(objThis.member.pageData == null ) {		// First time panel opened.
				objThis.member.pageData = new Object();
				try {
					if (standardsData != undefined && standardsDescData != undefined) {
						//var standardsList = objThis.createStandardsList(standardsData, standardsDescData);
	
						/******/
						for (var i = 0; i < standardsData.standards.pages.page.length; i++) {
							var pgNo = standardsData.standards.pages.page[i].number;
							objThis.member.pageData[pgNo] = new Array();
							var objStndrd = objThis.getObjectsRef(standardsData, 'number', standardsData.standards.pages.page[i].number);
							if (objStndrd[0].standard !== 'undefined') {
								if ( typeof objStndrd[0].standard.length !== 'undefined') {// More than one standard references are available
								    for (var j = 0; j < objStndrd[0].standard.length; j++) {
								        if (EPubConfig.StandardsFormat == "text/xml") {
								            var descArr = objThis.getObjectsDescXML($($(standardsDescData)[1])[0], "-value", objStndrd[0].standard[j].ref);
								            
								        }
								        else {
								            var descArr = objThis.getObjectsDesc(standardsDescData, "-value", objStndrd[0].standard[j].ref);
								        }
										
										 objThis.member.pageData[pgNo].push(objStndrd[0].standard[j].ref + "__" + descArr);
										
									}
								} else {// Single standard reference is available
									var descArr = objThis.getObjectsDesc(standardsDescData, "-value"  , objStndrd[0].standard.ref);
									objThis.member.pageData[pgNo].push(objStndrd[0].standard.ref + "__" + descArr);
								}
							}
						}
						/*****/
					}
				} catch(e) {
					objThis.displayErrorMessage("4");
				}
				objThis.standardPanelCreate(objThis.member.pageData);
			} else {
				objThis.standardPanelCreate(objThis.member.pageData);
			}
		},

		getObjectsRef : function(obj, key, val) {
			var objThis = this;
			var objects = [];
			for (var i in obj) {
				if (!obj.hasOwnProperty(i)) {
					continue;
				}
				if ( typeof obj[i] == 'object') {
					objects = objects.concat(objThis.getObjectsRef(obj[i], key, val));
				} else if (i == key && obj[key] == val) {
					objects.push(obj);
				}
			}
			return objects;
		},

		getObjectsDesc : function(obj, key, val) {
			var objThis = this;
			var objects = [];
			for (var i in obj) {
				if (!obj.hasOwnProperty(i)) {
					continue;
				}
				if ( typeof obj[i] == 'object') {
					try {
						if(obj[i].codes != undefined) {
							//console.log(obj[i].codes.code["-value"] , "==", val) 
							if(obj[i].codes.code["-value"] == val) {
								objects.push(obj[i].description["xhtml:div"]);
								break;
							} else {
								objects = objects.concat(objThis.getObjectsDesc(obj[i], key, val));
							}	
						} else {
							objects = objects.concat(objThis.getObjectsDesc(obj[i], key, val));
						}
					} catch(e) {}
				}
				// else if (i == key && obj[key] == val) {
					//objects.push(obj);
				//}
			}
			return objects;
		},

		getObjectsDescXML: function (obj, key, val) {
		    var objThis = this;
		    var objects = [];
		    for (var i in obj.childNodes) {
		        if (!obj.childNodes.hasOwnProperty(i)) {
		            continue;
		        }
		        if (typeof obj.childNodes[i] == 'object') {
		            try {
		                if (obj.childNodes[i].localName == 'codes') {
		                    //console.log(obj[i].codes.code["-value"] , "==", val) 
		                    if (obj.childNodes[i].childNodes[0].getAttribute("value") == val) {
		                        objects.push(obj.childNodes[1].innerHTML);
		                        break;
		                    } else {
		                        objects = objects.concat(objThis.getObjectsDescXML(obj.childNodes[i], key, val));
		                    }
		                } else {
		                    objects = objects.concat(objThis.getObjectsDescXML(obj.childNodes[i], key, val));
		                }
		            } catch (e) { }
		        }
		        // else if (i == key && obj[key] == val) {
		        //objects.push(obj);
		        //}
		    }
		    return objects;
		},
		
		/**
		 * Panel list creation
		 */
		standardPanelCreate: function(pgArr) {
			var objThis = this;
			var currPageIndex = GlobalModel.currentPageIndex;
			var iPageIndex = parseInt($($('[type=PageContainer]')[currPageIndex]).attr('pagesequence'));
			var pgBrkVal = GlobalModel.pageBrkValueArr[iPageIndex];
			var standardsListStr = '';
			if(typeof pgArr[pgBrkVal] !== 'undefined') {
				standardsListStr = '<div class="head-page-number">' + GlobalModel.localizationData["PAGE_TEXT_FREE"] + pgBrkVal + '</div>';
				for(var i=0; i<pgArr[pgBrkVal].length; i++) {
					var str = pgArr[pgBrkVal][i];
					var strArr = str.split("__");
					if(strArr[0] != 'undefined') {
					          standardsListStr += '<div class="description-container"><span class="pg-number-standard">' + strArr[0] + '</span> ' + strArr[1] + '</div>';
					}
				}
			}
			if(EPubConfig.pageView == AppConst.DOUBLE_PAGE_CAMEL_CASING) {
			          var nextPageIndex = GlobalModel.currentPageIndex + 1;
				var iPageIndex = parseInt($($('[type=PageContainer]')[nextPageIndex]).attr('pagesequence'));
				var nxtpgBrkVal = GlobalModel.pageBrkValueArr[iPageIndex];
				if(typeof pgArr[nxtpgBrkVal] !== 'undefined') {
					standardsListStr += '<div class="head-page-number"> ' + GlobalModel.localizationData["PAGE_TEXT_FREE"] + nxtpgBrkVal + '</div>';
					for(var j=0; j<pgArr[nxtpgBrkVal].length; j++) {
						var str = pgArr[nxtpgBrkVal][j];
						var strArr = str.split("__");
						if(strArr[0]) {
						    standardsListStr += '<div class="description-container"><span class="pg-number-standard">' + strArr[0] + '</span> ' + strArr[1] + '</div>';
						}
					}
				}
			}
			
			$("#StandardsListContainer").html(standardsListStr);
			
			
			
			/**
			 *  If standardPopulateCheckEnabled is true, then the following code will check if standards panel is populated correctly.
			 *  standardPopulateCheckEnabled is set to true on every panel open, and is set to false in the following code.
			 */
			if(objThis.member.standardPopulateCheckEnabled == true){
			    objThis.member.standardPopulateCheckEnabled = false;
			    
			    /**
			     * After 500ms, it will be checked if StandardsListContainer is empty or not.
			     * If empty, then standardsLoadComplete is called to populate the stardard panel.
			     */
			 setTimeout(function(){
                var standardPanelText = $("#StandardsListContainer").html();
                if(standardPanelText.length == 0){
                        objThis.standardsLoadComplete(objThis.options.standardsData,objThis.options.standardsDescData);
                    
                    }
                },500);
			}
			
			
			//create and update scroller;
			setTimeout(function() {
				standardScroll = new iScroll('StandardsList', {
					scrollbarClass : 'myScrollbar'
				});
				objThis.updateViewSize();
			}, 50);
			
		},
		
		onPanelOpen : function() {
		    var objThis = this;
			if ($("#StandardsListContainer").html() == "" || $("#StandardsListContainer").children("#loaderContainer").length > 0) {
				if (standardScroll)
					standardScroll.destroy();
				standardScroll = null
				removeiScrollClickHandler($(this.element).find("[class=myScrollbarV]"));
				setTimeout(function() {
				    objThis.standardsLoadComplete(objThis.options.standardsData, objThis.options.standardsDescData);
				},0);
			}
		},

		onPanelClosed : function() {
			if (standardScroll)
				standardScroll.destroy();
			standardScroll = null
			removeiScrollClickHandler($(this.element).find("[class=myScrollbarV]"));

			$("#StandardsListContainer").html("");
		},

		/**
		 * This function destroys the this object
		 * @param 	none
		 * @return 	none
		 */
		destroy : function() {
			$.widget.prototype.destroy.call(this);
		},

		/**
		 * This function launches a standard
		 * @param 	(String) url to launch/opem
		 * @return 	none
		 */
		launchStandard : function(strURL) {
			window.open(strURL, "_blank");
		},

		updateViewSize : function() {
			var objThis = this;
			var nHeight = ($("#header").css("display") == "block") ? window.innerHeight - $("#header").height() : window.innerHeight - 37;

			$("#StandardsList").height(nHeight - 50);
			if (standardScroll) {
				standardScroll.refresh();
			}

			//adjust the width of the ResourceList if scrollbar is visible
			if (ismobile == null) {
				//Handling the click event on scroll bar of iscroll calling the function of MyScroll.js
				removeiScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"));
				iScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"), standardScroll, "StandardsList");
				if ($(objThis.element).find("[class=myScrollbarV]").html()) {
					//$($(objThis.element).find("[id=StandardsListContainer]")[0]).width($($(objThis.element)).width() - 23);
					$($(objThis.element).find("[id=StandardsListContainer]")[0]).css({
						"width" : "99.2%"
					})
				} else {
					//$($(objThis.element).find("[id=StandardsListContainer]")[0]).width($($(objThis.element)).width() - 12);
					$($(objThis.element).find("[id=StandardsListContainer]")[0]).css({
						"width" : "100%"
					})
				}
			}
		}
	});

	/**
	 * This function launches a standard
	 * @param 	(String) url to launch/opem
	 * @return 	none
	 */
	launchStandard = function(strURL) {
		var objDOM = $('[data-role="StandardsPanelComp"]');
		var objComp = objDOM.data("StandardsPanelComp");

		objComp.launchStandard(strURL);
	}
})(jQuery);
