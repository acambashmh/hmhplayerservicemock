/**
 * @author abhishek.shakya
 */
( function( $, undefined ) {
$.widget( "magic.zoomPanelComp", $.magic.magicwidget, 
{
	options: 
	{
		strList:"",
		level:0,
		curriculumXml :""
	},
		////////////////////////NOTEPANEL
	_getPanel: function()
	{
		return  this.getVar('objPanel');
	},
	_create: function()
	{
				
	},
	_init: function()
	{
        $.magic.magicwidget.prototype._init.call(this);
		self = this;
		
		self.updatePosition(self.element);
		$(window).bind('resize', function(){
			self.updatePosition();
		});

		self.hideZoomButtons();
		self.hidePageViewButtons();
		//$(this.element).find(".viewSwitchBtnParent").css("display", "none");
	},
	
	hideZoomButtons: function()
	{
		if (ismobile || EPubConfig.Zoom_isAvailable == false) {
            $(".zoomTxtParent").css('display', 'none');
            $(".zoomSliderParent").css('display', 'none');
            $(this.element).css({"top":"0px"})
            $("#zoomBtnPanel").css({"margin-top":"-4px","height":"81px !important"})
            $("#zoomPanelArrow").css({"margin-top":"-10px"})
        }
	},
	
	hidePageViewButtons : function()
	{
		var _width = 0, _defaultView = "", _count = $( this.element ).find(".viewSwitchBtnParent").find(".changePageViewBtn").length;
		
		_defaultView = EPubConfig.Page_Navigation_Type;
				
		// set visibility
		if( !EPubConfig.SinglePageView_isAvailable && ( _defaultView != AppConst.SINGLE_PAGE ) )
		{
			$( this.element ).find('[type="singlePageLauncher"]').css( "display", "none" );
			_count = _count - 1;
		}
		
		if( !EPubConfig.ScrollPageView_isAvailable && ( _defaultView != AppConst.SCROLL_VIEW ) )
		{
			$( this.element ).find('[type="scrollPageLauncher"]').css( "display", "none" );
			_count = _count - 1;
		}
		
		if( !EPubConfig.DoublePageView_isAvailable && ( _defaultView != AppConst.DOUBLE_PAGE ) )
		{
			$( this.element ).find('[type="doublePageLauncher"]').css( "display", "none" );
			_count = _count - 1;
		}

		_width = _count * $( this.element ).find('[type="singlePageLauncher"]').width();
		$( this.element ).find(".viewSwitchBtnParent").css( "width", _width );
	},
	
	onPanelOpen: function() {
		//this.hideZoomButtons();
	},
	
	onPanelClose: function() {
		 
	},
	
	updatePosition:function(){
       $(this.element).parent().css('top', $(zoomBtn).offset().top);
       $(this.element).parent().css('left' , $($(zoomBtn).parent().find('[id="zoomBtn"]')).width());
       $(this.element).css('top', 0);
	}
	
})
})( jQuery );
