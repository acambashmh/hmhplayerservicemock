var AudioPlayer = function ( )
{
	this.playing = false;
	this.result = [];
	this.slowResult = [];
	this.hideHighlight = false;
	this.prevState = false;
	this.counterAudio = 0;
	this.audioPopup = $( '#audioPopUp' );
	this.audioSyncClass = "";
	this.pageLevelAudioPath = "";
	this.glossaryAudioPath = "";
	this.canPlay = false;
	this.playInterval = "";
	this.closeAudio = false;
	this.hasMultipleAudio = false;
	
	this.loadPageContent = function( autoPlay )
	{
		//Load the page content
		var objThis = this;
		if( objThis.mediaOverlayPath )
		{
			objThis.pageLevelAudioPath = "";
			objThis.loadSmilContent( autoPlay );
			objThis.loadSlowSmilContent( );
			if( ismobile )
			{
				$('#player')[0].play();
			}
		}
		else if ( objThis.pageLevelAudioPath != "" )
		{
			objThis.audioPopup.find( '#textContainer' ).css( "display" , "none" );
			this.constructAudioPlayer( true );
		}
		else
		{
			if( !ismobile )
			{
				$( 'body' ).find( 'audio' ).remove( );
				$( "<audio></audio>" ).attr( { 'id' : 'player' } ).appendTo( "body" );
			}
			$('#player').find('source').remove();
			$( "<source></source>" ).attr( { 'src' : objThis.glossaryAudioPath , 'type' : 'audio/ogg' } ).appendTo( "#player" );
			$( "<source></source>" ).attr( { 'src' : objThis.glossaryAudioPath , 'type' : 'audio/mpeg' } ).appendTo( "#player" );
			$('#player')[0].load();
			$('#player')[0].loop = false;
		}
	}

	this.constructAudioPlayer = function ( autoPlay )
	{
		//Construct HTML for player
		var objThis = this;

		if ( objThis.pageLevelAudioPath != "" )
		{
			objThis.result[0] = {};
			objThis.result[0].audiosrc = objThis.pageLevelAudioPath;
		}
		if( ismobile )
		{
			$( '#player' )[0].pause( );
		}
		
		objThis.audioPopup.find( ".audio-stop-container" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );
		objThis.audioPopup.find( ".audio-slider-container" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );
		objThis.audioPopup.find( ".audio-play-container" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );

		$( '#player' ).unbind( 'canplaythrough' ).bind( "canplaythrough" , function ( ) {
			$( '#player' ).unbind( 'canplaythrough' );
			objThis.audioPopup.find( ".audio-play-container" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
			if( autoPlay && objThis.canPlay )
			{
				objThis.canPlay = false;
				objThis.audioPopup.find( ".audio-play-container" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );
				if( navigator.userAgent.indexOf('Firefox') == -1 )
				{
					$( '#player' )[0].play( );
				}
				$( '#player' )[0].pause( );
				objThis.playInterval = setInterval (function(){
					if( objThis.closeAudio == true )
					{
						clearInterval(objThis.playInterval);
						return;
					}
					if( $( '#player' )[0].seekable.length )
					{
						objThis.audioPopup.find( ".audio-play-container" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
						$( objThis ).trigger( "enable_leftpanelbutton" );
						$( objThis ).trigger( "enable_hotspotbutton" );
						objThis.playing = true ;
						objThis.playAudioDependingOnType( false );	
						clearInterval(objThis.playInterval);
					}
				},500)
			}
		});

		$('#player').find('source').remove();
		$( "<source></source>" ).attr( { 'src' : objThis.result[0].audiosrc , 'type' : 'audio/ogg' } ).appendTo( "#player" );
		$( "<source></source>" ).attr( { 'src' : objThis.result[0].audiosrc , 'type' : 'audio/mpeg' } ).appendTo( "#player" );
		objThis.setVolume();
		
		$('#player')[0].load();
		if( objThis.hideControl )
		{
			objThis.audioPopup.css( 'visibility' , 'hidden' );
			return;
		}
		
		objThis.audioPopup.find( '.volume-control-container' ).css( 'display' , 'none' );
		if( !ismobile )
		{
			objThis.createProgressSlider();
		}
		$(window).bind('orientationchange resize', function() {
            setTimeout(function() {
                objThis.updatePosition();
            }, 600);
        });
            
		objThis.updatePosition();
		
		objThis.audioPopup.slideDown( 500, "easeOutBounce" );
		
		//Bind Play/Pause/Stop/Close event
		objThis.audioPopup.find( '.audio-stop-container' ).unbind( 'click' ).bind( 'click' ,function ( ){
  			if ( ! objThis.audioPopup.find( ".audio-stop-container" ).hasClass( 'ui-disabled' ) )
  			{
				objThis.stopAudio( );  				
  			}
		});
		
		objThis.audioPopup.find( '.audio-play-container' ).unbind( 'click' ).bind( 'click' , function ( event ){
			if(objThis.playing == undefined)
			{
				var smilContent = objThis.result;
				if( !objThis.audioPopup.find( '#slowerButton' ).hasClass( 'slowerAudio' ) )
				{
					smilContent = objThis.slowResult;
				}
				$('#player').find('source').attr('src',smilContent[objThis.counterAudio].audiosrc);
				$('#player')[0].load();
				if( navigator.userAgent.indexOf('Firefox') == -1 )
				{
					$( '#player' )[0].play( );
				}
				$( '#player' )[0].pause( );
				objThis.audioPopup.find( ".audio-play-container" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );
				objThis.playInterval = setInterval (function(){
					if( objThis.closeAudio == true )
					{
						clearInterval(objThis.playInterval);
						return;
					}
					if( $( '#player' )[0].seekable.length )
					{
						objThis.audioPopup.find( ".audio-play-container" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
						$( objThis ).trigger( "enable_leftpanelbutton" );
						$( objThis ).trigger( "enable_hotspotbutton" );
						objThis.playing = true ;
						objThis.playAudioDependingOnType( false );	
						clearInterval(objThis.playInterval);
					}
				},500)
			}
			else if ( !objThis.playing )
			{
				objThis.playing = true;
				objThis.playAudioDependingOnType( true );
			}
			else
			{
				objThis.playing = false;
				objThis.pauseAudio( );
			}
		});
		
		objThis.audioPopup.find( '.play-bar' ).unbind( 'click' ).bind( 'click' , function ( event ){
			if ( ! objThis.audioPopup.find( ".audio-slider-container" ).hasClass( 'ui-disabled' ) )
			{
				objThis.slideAudio( event );
			}
		});
		
		objThis.audioPopup.find( '#progressBarSlider' ).unbind( 'click' ).bind( 'click' , function ( event ){
			if ( ! objThis.audioPopup.find( ".audio-slider-container" ).hasClass( 'ui-disabled' ) )
			{
				objThis.slideAudio( event );
			}
		});
		
		objThis.audioPopup.find( '.audioPanelButton' ).unbind( 'click' ).bind( 'click' ,function ( ){
			objThis.playSlowAudio( );
		});
		
		objThis.audioPopup.find( '.closeContainer' ).unbind( 'click' ).bind( 'click' ,function ( ){
			objThis.closeAudioPanel( );
		});
		
		if( !ismobile )
		{
			objThis.audioPopup.find( '#progressBarSlider .ui-slider-handle' ).unbind( 'dragstart' ).bind( 'dragstart' , function ( event ) {
					objThis.audioPopup.find( '#progressBarSlider .ui-slider-handle' ).unbind( 'mouseup' );
			});
			
			objThis.audioPopup.find( '#progressBarSlider .ui-slider-handle' ).unbind( 'drag' ).bind( 'drag' , function ( event ) {
				if ( ! objThis.audioPopup.find( ".audio-slider-container" ).hasClass( 'ui-disabled' ) )
				{
					objThis.slideAudio( event.originalEvent );	
				}
					
			});
			
			objThis.audioPopup.find( '#progressBarSlider .ui-slider-handle' ).bind( 'mousedown' , function ( event ) {
				objThis.pauseAudio( );
				objThis.audioPopup.find( '#progressBarSlider .ui-slider-handle' ).unbind( 'mouseup' ).bind( 'mouseup' , function ( event ) {
					if ( ! objThis.audioPopup.find( ".audio-slider-container" ).hasClass( 'ui-disabled' ) )
					{
						objThis.playAudioDependingOnType( false );						
					}
				});
			});
			
			objThis.audioPopup.find( '#progressBarSlider .ui-slider-handle' ).unbind( 'dragend' ).bind( 'dragend' , function ( event ) {
				if ( ! objThis.audioPopup.find( ".audio-slider-container" ).hasClass( 'ui-disabled' ) )
				{
					objThis.showDuration( );
					objThis.slideAudio(event.originalEvent);
					if(objThis.playing)
				    {
				     objThis.playAudioDependingOnType( false );
				    }
				}
				
			});
			
			objThis.audioPopup.find( '.audio-slider-container' ).unbind( 'dragover' ).bind( 'dragover' , function ( event ) {
				if( navigator.userAgent.indexOf('Firefox') != -1 )
				{
					objThis.playing = false;
					objThis.slideAudio( event.originalEvent );
				}
			});
		}
		
		objThis.audioPopup.find( '#audioText' ).unbind( 'click' ).bind( 'click' ,function ( ){
			if( !objThis.hideHighlight )
			{
				objThis.hideHighlight = true;
				objThis.audioPopup.find( '#audioText' ).removeClass( 'audioText' );
				objThis.audioPopup.find( '#audioText' ).addClass( 'audioText-disabled' );
				objThis.pageBody.find( '.' + objThis.audioSyncClass ).removeClass( objThis.audioSyncClass );
			}
			else
			{
				objThis.hideHighlight = false;
				objThis.audioPopup.find( '#audioText' ).removeClass( 'audioText-disabled' );
				objThis.audioPopup.find( '#audioText' ).addClass( 'audioText' );
			}
		});
		
		objThis.audioPopup.find( '#audioText' ).text( 'TEXT' );
		
	}
	
	this.updatePosition = function ( )
	{
		var objThis = this;
		var innerWidth = window.width - ( $( '#annotationPanel' ).width( ) + $( '#prevPgBanner' ).width( ) * 2  );
		objThis.audioPopup.css( { 'left':( ( (innerWidth - objThis.audioPopup.width( ) ) / 2 ) + $( '#annotationPanel' ).width( ) + $( '#prevPgBanner' ).width( ) )+ 'px' } );
	}
	
	this.createProgressSlider = function ( )
	{
		var objThis = this;
		objThis.audioPopup.find( '.volume-control-container' ).css( 'display' , 'block' );
		//Bind Volume Control Events
		objThis.audioPopup.find( '.volume-control-container' ).unbind( 'click mouseenter' ).bind( 'click mouseenter mousemove' , function ( ) {
			objThis.audioPopup.find( '#volume-container' ).css('display', 'block');
		});
		
		objThis.audioPopup.find( '#volume-container' ).unbind( 'mouseenter' ).bind( 'mouseenter' , function ( ) {
			objThis.audioPopup.find( '#volume-container' ).css('display', 'block');
		});
		
		objThis.audioPopup.find( '#volume-container' ).unbind( 'mouseleave' ).bind( 'mouseleave' , function ( ) {
			objThis.audioPopup.find( '#volume-container' ).css('display', 'none');
		});
		
		objThis.audioPopup.find( '.volume-control-container' ).unbind( 'mouseleave' ).bind( 'mouseleave' , function ( ) {
			objThis.audioPopup.find( '#volume-container' ).css( 'display' , 'none' );
		});
		
		objThis.audioPopup.find( '.volume-bar' ).unbind( 'click' ).bind( 'click' , function ( event ) {
			objThis.setVolume( event )
	 	 });
		  
	    objThis.audioPopup.find( '#volumeSlider .ui-slider-handle' ).unbind( 'drag' ).bind( 'drag' , function ( event ) {
			objThis.setVolume( event )
		});
		
		objThis.audioPopup.find( '#volume-container' ).unbind( 'dragover' ).bind( 'dragover' , function( event ) {
			if( navigator.userAgent.indexOf( 'Firefox' ) != -1 )
			{
				objThis.setVolume( event )
			}
		});
		
	}
	
	this.setVolume = function( event )
	{
		var objThis = this;
		var volume;
		if( event == undefined )
		{
			volume = objThis.audioPopup.find( '#volumeSlider' ).find( '.ui-slider-range' ).height();
		}
		else
		{
			volume = event.originalEvent.pageY - objThis.audioPopup.find( '#volumeSlider' ).offset( ).top;	
		}
		var volumeLevel = ( ( volume - 50 ) * -1 ) / 50;
		if( volume > 0 && volume <= 50 )
		{
			 objThis.audioPopup.find( '#volumeSlider .ui-slider-handle' ).css( 'top' , volume + 'px')
			 objThis.audioPopup.find( '#volumeSlider .ui-slider-range' ).css( 'height' , volume + 'px')
			 objThis.audioPopup.find( '.volume-bar-value' ).css( 'height' , volumeLevel * 50 + "px");
			 $( '#player' )[0].volume = volumeLevel;
		}
	}
	this.playWithoutSmilAudio = function( )
	{
		//Play/Pause audio
		var objThis = this;
		
		objThis.audioPopup.find( "#toggle" ).removeClass( "audio-pause" );
		objThis.audioPopup.find( "#toggle" ).addClass( "audio-play" );
		objThis.audioPopup.find( ".audio-stop-container" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
		objThis.audioPopup.find( ".audio-slider-container" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
		var duration = ( $('#player')[0].duration - $('#player')[0].currentTime ) * 1000;
		
		function completeHandler ( )
		{
			objThis.stopAudio();
			$(objThis).trigger("audio_complete");
		} 
		$('#player')[0].play();
		
		objThis.audioPopup.find( "#progressBarSlider .ui-slider-handle" ).animate ( 
		{
			'margin-left': '147px'
		},  
		{
			duration: duration,
			queue: false,
			easing: 'linear',
			step: function ( currentLeft )
			{
				objThis.duration = Math.round( ( currentLeft * $('#player')[0].duration )/147 );
				objThis.showDuration();
			},
			complete: function() 
			{
				completeHandler( );
			}
		});
		
		objThis.audioPopup.find( ".play-bar" ).animate({
			'width': '155px'
		},  
		{
			duration: duration,
			easing: 'linear',
			queue: false,
			step: function( currentLeft ){
							},
			complete: function() {
			// Animation complete.
				objThis.audioPopup.find(".play-bar").css('width','0px')
			}
		});
	}

	
	this.playAudio = function( prev )
	{
		//Play/Pause audio
		var objThis = this;
		var prevTime;
		var smilContent = objThis.result;
		if( !objThis.audioPopup.find( '#slowerButton' ).hasClass( 'slowerAudio' ) )
		{
			smilContent = objThis.slowResult;
		}
		
		objThis.audioPopup.find( "#toggle" ).removeClass( "audio-pause" );
		objThis.audioPopup.find( "#toggle" ).addClass( "audio-play" );
		objThis.audioPopup.find( ".audio-stop-container" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
		objThis.audioPopup.find( ".audio-slider-container" ).removeClass( "ui-disabled" ).attr( "aria-disabled", false );
		
		var currentPos = objThis.audioPopup.find(".play-bar").css('width').replace(/[^-\d\.]/g, '') * smilContent[ smilContent.length - 1 ].totalduration / objThis.audioPopup.find('#progressBarSlider').css('width').replace(/[^-\d\.]/g, '');
		$('#player')[0].loop = true;
		var duration = ( smilContent[ smilContent.length - 1 ].totalduration - currentPos ) * 1000;
		
		if( prev == false )
		{
			if( objThis.hasMultipleAudio )
			{
				objThis.setCurrentTime(smilContent[objThis.counterAudio].clipbegin , false );
			}
			else
			{
				$('#player')[0].currentTime = smilContent[objThis.counterAudio].clipbegin;
			}
		}
		$('#player')[0].play();	
		
		function completeHandler ( )
		{
			
			objThis.stopAudio();
			$(objThis).trigger("audio_complete");
		} 

		
		
		objThis.audioPopup.find( "#progressBarSlider .ui-slider-handle" ).animate ( 
		{
			'margin-left': '147px'
		},  
		{
			duration: duration,
			queue: false,
			easing: 'linear',
			step: function ( currentLeft )
			{
				if( objThis.counterAudio < smilContent.length )
				{
					objThis.duration = Math.round( ( currentLeft * smilContent[ smilContent.length - 1 ].totalduration )/147 );
					var condition = false;
					if( smilContent[objThis.counterAudio + 1 ] )
					{
					    if (($('#player')[0].currentTime <= smilContent[objThis.counterAudio + 1].clipend && $('#player')[0].currentTime >= smilContent[objThis.counterAudio + 1].clipbegin) || smilContent[objThis.counterAudio].audiosrc != smilContent[objThis.counterAudio + 1].audiosrc)
					    {
					        condition = !($('#player')[0].currentTime < smilContent[objThis.counterAudio].clipend && $('#player')[0].currentTime >= smilContent[objThis.counterAudio].clipbegin);
                            //Fix for ie multiple audio load issue
					        if ($.browser.msie && $('#player')[0].currentTime == 0 && prevTime)
					        {
					            condition = true;
					        }
					        prevTime = $('#player')[0].currentTime;
					  	}
					}
					
					if( condition )
					{
						objThis.counterAudio++;
						if( objThis.counterAudio == smilContent.length )
						{
							completeHandler( );
							return;
						}
						if(smilContent[objThis.counterAudio - 1].audiosrc != smilContent[objThis.counterAudio].audiosrc )
						{
                            objThis.hasMultipleAudio = true;
							$( '#player' )[0].pause();
							objThis.audioPopup.find( ".#progressBarSlider .ui-slider-handle" ).stop( );
							objThis.audioPopup.find( ".play-bar" ).stop();
							$('#player').find('source').attr('src',smilContent[objThis.counterAudio].audiosrc);
							$('#player')[0].load();
							if( objThis.hasMultipleAudio )
								objThis.setCurrentTime(smilContent[objThis.counterAudio].clipbegin);
							else
							{
								$('#player')[0].currentTime = smilContent[objThis.counterAudio].clipbegin;
								$('#player')[0].play();	
							}
							
						}
					}	
					if( ismobile )
					{
						objThis.updateHighlightMobile( smilContent );
					}
					else
					{
						objThis.updateHighlight( smilContent );
					}
					objThis.showDuration();
				}
			},
			complete: function() 
			{
				completeHandler( );
			}
		});
		
		objThis.audioPopup.find( ".play-bar" ).animate({
			'width': '155px'
		},  
		{
			duration: duration,
			easing: 'linear',
			queue: false,
			step: function( currentLeft ){
							},
			complete: function() {
			// Animation complete.
				objThis.audioPopup.find(".play-bar").css('width','0px')
			}
		});
	}
	
	this.pauseAudio = function()
	{
		var objThis = this;
		$( '#player' )[0].pause();
		objThis.audioPopup.find( ".#progressBarSlider .ui-slider-handle" ).stop( );
		objThis.audioPopup.find( ".play-bar" ).stop();
		objThis.audioPopup.find( "#toggle" ).removeClass( "audio-play" )
		objThis.audioPopup.find( "#toggle" ).addClass( "audio-pause" );
	}
	
	this.updateHighlight= function( smilContent )
	{
		var objThis = this;
		var smilContent = objThis.result;
  	    if( !objThis.audioPopup.find( '#slowerButton' ).hasClass( 'slowerAudio' ) )
  		{
   			smilContent = objThis.slowResult;
  		}
  		if( !objThis.hideHighlight && objThis.audioSyncClass )
  		{
   			objThis.pageBody.find( '.' + objThis.audioSyncClass ).removeClass( objThis.audioSyncClass );
			if ( ! objThis.pageBody.find( '#' + smilContent[ objThis.counterAudio ].id ).hasClass( objThis.audioSyncClass ) )
			{
				objThis.pageBody.find( '#' + smilContent[ objThis.counterAudio ].id ).addClass( objThis.audioSyncClass );
				objThis.pageBody.find( '#' + smilContent[ objThis.counterAudio ].id ).find("*").addClass( objThis.audioSyncClass );
			}
  		}
	}
	
	this.updateHighlightMobile = function( smilContent )
	{
		var objThis = this;
		var smilContent = objThis.result;
  	    if( !objThis.audioPopup.find( '#slowerButton' ).hasClass( 'slowerAudio' ) )
  		{
   			smilContent = objThis.slowResult;
  		}
  		if( !objThis.hideHighlight && objThis.audioSyncClass )
  		{
   			objThis.pageBody.find( '.' + objThis.audioSyncClass ).removeClass( objThis.audioSyncClass );
   			for( var i = 0; i < smilContent.length; i++ )
   			{
   				if($( '#player' )[0].currentTime >= smilContent[ i ].clipbegin && $( '#player' )[0].currentTime < smilContent[ i ].clipend && $('#player').find('source').attr('src') == smilContent[ i ].audiosrc && $( '#player' )[0].readyState > 3 )
   				{
   					if ( ! objThis.pageBody.find( '#' + smilContent[ i ].id ).hasClass( objThis.audioSyncClass ) )
   					{
   						objThis.pageBody.find( '#' + smilContent[ i ].id ).addClass( objThis.audioSyncClass );
   						objThis.pageBody.find( '#' + smilContent[ i ].id ).find("*").addClass( objThis.audioSyncClass );
   						break;
   					}
   				}
   			}
  		}
	}
	
	this.slideAudio = function( e , currenttime )
	{
		//Called on click on Slide Bar 
		var objThis = this;
		var tempTime;
		$('#player')[0].pause();
		if( objThis.audioSyncClass )
		{
			objThis.pageBody.find( '.' + objThis.audioSyncClass ).removeClass( objThis.audioSyncClass );
		}
		objThis.audioPopup.find( "#progressBarSlider .ui-slider-handle" ).stop( );
		objThis.audioPopup.find( ".play-bar" ).stop( );
		objThis.audioPopup.find( "#toggle" ).removeClass( "audio-play" )
		objThis.audioPopup.find( "#toggle" ).addClass( "audio-pause" );
		var smilContent = objThis.result;
		if( !objThis.audioPopup.find( '#slowerButton' ).hasClass( 'slowerAudio' ) )
		{
			smilContent = objThis.slowResult;
		}
		
		setTimeout ( function ( ) 
		{
			if( !currenttime )
			{
				if( e )
				{
					var sliderMarginLeft = e.pageX - $('#progressBarSlider').offset().left - 8;
					var barWidth = e.pageX - $('#progressBarSlider').offset().left;
					
					if ( sliderMarginLeft < -8 ) 
					{
						objThis.counterAudio = 0;
						objThis.audioPopup.find('#progressBarSlider .ui-slider-handle').css('margin-left' , "-8px");
						objThis.audioPopup.find(".play-bar").css('width','0px');
					}
					else if( sliderMarginLeft <= '147' )
					{
			      		objThis.audioPopup.find('#progressBarSlider .ui-slider-handle').css('margin-left' , sliderMarginLeft);
			      		objThis.audioPopup.find(".play-bar").css('width' , barWidth);
			        }
									  	
					tempTime = (objThis.audioPopup.find(".play-bar").css('width').replace(/[^-\d\.]/g, '') * smilContent[smilContent.length -1].totalduration) / objThis.audioPopup.find('#progressBarSlider').css('width').replace(/[^-\d\.]/g, '');
				}
				if( objThis.pageLevelAudioPath != "" )
				{
					$('#player')[0].currentTime = (objThis.audioPopup.find(".play-bar").css('width').replace(/[^-\d\.]/g, '') * $('#player')[0].duration) / objThis.audioPopup.find('#progressBarSlider').css('width').replace(/[^-\d\.]/g, '');
					if( objThis.playing )
					{
						if( ismobile )
						{
							setTimeout ( function ( ) 
							{
								objThis.playWithoutSmilAudio( true );	
							},800)
						}
						else
						{
							objThis.playWithoutSmilAudio( true );	
						}
					}
					return;
				}
				for(var i = 0; i < smilContent.length ; i++)
				{
					if(tempTime >= smilContent[i].seekbarbegin && tempTime < smilContent[i].seekbarend){
						objThis.counterAudio = i;
						if($('#player').find('source').attr('src') != smilContent[objThis.counterAudio].audiosrc)
						{
							objThis.hasMultipleAudio = true;
							$('#player').find('source').attr('src',smilContent[objThis.counterAudio].audiosrc);
							$('#player')[0].load();
							objThis.setCurrentTime( Number(smilContent[i].clipbegin) + ((Math.round(tempTime * 1000) / 1000) - (Math.round(smilContent[i].seekbarbegin * 1000) / 1000)), false )
							break;
						}
						$( '#player' ).unbind( 'canplaythrough' );
						$('#player')[0].currentTime = Number(smilContent[i].clipbegin) + ((Math.round(tempTime * 1000) / 1000) - (Math.round(smilContent[i].seekbarbegin * 1000) / 1000)) ;
						
						break;
					}
				}
				if(e.originalEvent && (e.originalEvent.type == 'click' || e.originalEvent.type == 'dragend'))
				{
					if( objThis.playing )
					{
						if( ismobile )
						{
							setTimeout ( function ( ) 
							{
								objThis.playAudio( true );	
							},800)
						}
						else
						{
							objThis.playAudio( true );	
						}
					}
						
				}
			}
		},0)
	}

    this.setCurrentTime = function ( currenttime , playAudio )
    {
    	var objThis = this;
    	$( '#player' ).unbind( 'canplaythrough' ).bind( "canplaythrough" , function ( ) {
			$( '#player' ).unbind( 'canplaythrough' );
			if( playAudio != false )
			{
				objThis.playAudio();
			}
    		$('#player')[0].currentTime = currenttime;
    	});
    }
	this.showDuration = function ( )
	{
		var objThis = this;
		//Show the Audio Duration
		if( objThis.duration > 0 )
		{
			var min = Math.floor( ( objThis.duration ) / 60 );
			var sec = Math.floor( ( objThis.duration ) % 60 );
			if( min < 10 )
				min = "0" + min;
			if( sec < 10 )
				sec = "0" + sec;
			objThis.audioPopup.find('.audio-duration').text(min + ':' + sec);
		}
		else
		{
			objThis.audioPopup.find( '.audio-duration' ).text( '00:00' );
		}
		
	}

	this.showVolumeControl = function ( )
	{
		objThis.audioPopup.find( '.volume-container' ).css( 'display' , 'block' );	
	}

	this.hideVolumeControl = function ( )
	{
		objThis.audioPopup.find( '.volume-container' ).css( 'display' , 'none' );
	}

	
	this.stopAudio = function()
	{
		var objThis = this;
		if( !objThis.pageBody )
			return;
		if(objThis.audioSyncClass)
			objThis.pageBody.find( '.' + objThis.audioSyncClass ).removeClass( objThis.audioSyncClass );
		
		setTimeout( function ( )
		{
			objThis.playing = undefined;
			objThis.counterAudio = 0;
			
			if( $( '#player' ).length )
			{
				$( '#player' )[0].pause( );
			}
			objThis.audioPopup.find( "#progressBarSlider .ui-slider-handle" ).stop();
			objThis.audioPopup.find( ".play-bar" ).stop();
			objThis.audioPopup.find( "#toggle" ).removeClass( "audio-play" )
			objThis.audioPopup.find( "#toggle" ).addClass( "audio-pause" );
			objThis.audioPopup.find( ".play-bar" ).css( 'width' , '0px' );
			objThis.audioPopup.find( '.audio-duration' ).text( '00:00' );
			objThis.audioPopup.find( ".audio-stop-container" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );
  			objThis.audioPopup.find( ".audio-slider-container" ).addClass( "ui-disabled" ).attr( "aria-disabled", true );
			objThis.audioPopup.find( "#progressBarSlider .ui-slider-handle" ).css( 'margin-left' , '-8px' );
			if(objThis.audioSyncClass)
				objThis.pageBody.find( '.' + objThis.audioSyncClass ).removeClass( objThis.audioSyncClass );
			if( $( '#player' ).length && $( '#player' )[0].readyState == 4 )
			{
				$( '#player' )[0].currentTime = 0; 
			}
		},0 )
		
	}

	this.loadSmilContent = function ( autoPlay )
	{
		//Load the smil content
		var objThis = this;
		objThis.counterAudio = 0;
		objThis.result = [];
		$.ajax({
					type: "GET",
					url: this.mediaOverlayPath,
					dataType: "text",
					contentType: "plain/text",
					success: function ( objData )
					{
							objData = objData.replace( /text/ig , "txt" );
							var objTemp = $.xml2json( objData );
							var smilData = objTemp.body.par;
							
							if( smilData.length == undefined )
							{
								objThis.result[0] = {};
								objThis.result[0].id = smilData.txt.src.split( '#' )[1];
								objThis.result[0].clipbegin = smilData.audio.clipBegin;
								objThis.result[0].clipend = smilData.audio.clipEnd;
								objThis.result[0].seekbarbegin = 0;
								
								objThis.result[0].totalduration = objThis.result[0].seekbarend = smilData.audio.clipEnd;
								 
								if( !objThis.audiosrc )
								{
									objThis.result[0].audiosrc = getPathManager().getSMILAudioPath( smilData.audio.src );
								}
							}
							else
							{
								for( var i = 0,j = 0; i < smilData.length; i++ )
								{
									if( objThis.pageBody.find("#" + smilData[i].txt.src.split( '#' )[1] ).length )
									{
										objThis.result[j] = {};
										objThis.result[j].id = smilData[i].txt.src.split( '#' )[1];
										objThis.result[j].clipbegin = smilData[i].audio.clipBegin;
										objThis.result[j].clipend = smilData[i].audio.clipEnd;
										objThis.result[j].totalduration = 0;
										if( j )
										{
											objThis.result[j].seekbarbegin = objThis.result[j-1].seekbarend;
											var duration =  ( ( Math.round( smilData[i].audio.clipEnd * 1000 ) / 1000 ) - ( Math.round( smilData[i].audio.clipBegin * 1000 ) / 1000 ) );
											objThis.result[j].totalduration =  Math.round( objThis.result[j-1].totalduration * 1000 ) / 1000  + Math.round( duration * 1000 ) / 1000;
										}
										else
										{
											objThis.result[j].seekbarbegin = 0;
											var duration =  ( ( Math.round( smilData[i].audio.clipEnd * 1000 ) / 1000 ) - ( Math.round( smilData[i].audio.clipBegin * 1000 ) / 1000 ) );
											objThis.result[j].totalduration =  Math.round((Math.round( duration * 1000 ) / 1000) * 1000 ) / 1000;
										}
										
										
										/*if( i>0 && smilData[i].audio.src !=  smilData[i-1].audio.src )
										{
											objThis.result[j].seekbarbegin = 0;
										}*/
										objThis.result[j].seekbarend = ( Math.round(objThis.result[j].seekbarbegin * 1000) / 1000 ) + ( ( Math.round( smilData[i].audio.clipEnd * 1000 ) / 1000 ) - ( Math.round( smilData[i].audio.clipBegin * 1000 ) / 1000 ) );
										objThis.result[j].seekbarend = Math.round(objThis.result[j].seekbarend * 1000) / 1000;
										
										if( !objThis.result[j].audiosrc )
										{
											objThis.result[j].audiosrc = getPathManager().getSMILAudioPath( smilData[i].audio.src );
										}	
										j = j + 1;
									}
								}
								
							}	
							
							objThis.constructAudioPlayer( autoPlay );
							
					},
					error: function ( )
					{
						$( objThis ).trigger( "smil_error" );
					}
				});
		
	}

	this.loadSlowSmilContent = function ( )
	{
		//Load the smil content
		var objThis = this;
		objThis.slowResult = [];
		if( objThis.mediaOverlayPath.indexOf( '_alt' ) == -1)
		{
			objThis.mediaOverlayPath = objThis.mediaOverlayPath.substring( 0 , objThis.mediaOverlayPath.indexOf('.') ) + "_alt" + objThis.mediaOverlayPath.substring( objThis.mediaOverlayPath.indexOf('.') , objThis.mediaOverlayPath.length );
		}
		$.ajax({
					type: "GET",
					url: objThis.mediaOverlayPath,
					dataType: "text",
					contentType: "plain/text",
					success: function ( objData )
					{
						objData = objData.replace( /text/ig , "txt" );
						var objTemp = $.xml2json( objData );
						var smilData = objTemp.body.par;
						
						if( smilData.length == undefined )
						{
							objThis.slowResult[0] = {};
							objThis.slowResult[0].id = smilData.txt.src.split( '#' )[1];
							objThis.slowResult[0].clipbegin = smilData.audio.clipBegin;
							objThis.slowResult[0].clipend = smilData.audio.clipEnd;
							objThis.slowResult[0].seekbarbegin = 0;
							
							objThis.result[0].totalduration = objThis.slowResult[0].seekbarend = smilData.audio.clipEnd;
							
							if( !objThis.slowResult[0].audiosrc )
							{
								objThis.slowResult[0].audiosrc = getPathManager().getSMILAudioPath( smilData.audio.src );
							}
						}
						else
						{
							for( var i = 0,j = 0; i < smilData.length; i++ )
							{
								if( objThis.pageBody.find("#" + smilData[i].txt.src.split( '#' )[1] ).length )
								{
									objThis.slowResult[j] = {};
									objThis.slowResult[j].id = smilData[i].txt.src.split( '#' )[1];
									objThis.slowResult[j].clipbegin = smilData[i].audio.clipBegin;
									objThis.slowResult[j].clipend = smilData[i].audio.clipEnd;
									objThis.slowResult[j].totalduration = 0;
									if( j )
									{
										objThis.slowResult[j].seekbarbegin = objThis.slowResult[j-1].seekbarend;
										var duration =  ( ( Math.round( smilData[i].audio.clipEnd * 1000 ) / 1000 ) - ( Math.round( smilData[i].audio.clipBegin * 1000 ) / 1000 ) );
										objThis.slowResult[j].totalduration =  Math.round( objThis.slowResult[j-1].totalduration * 1000 ) / 1000  + Math.round( duration * 1000 ) / 1000;
									}
									else
									{
										objThis.slowResult[j].seekbarbegin = 0;
										var duration =  ( ( Math.round( smilData[i].audio.clipEnd * 1000 ) / 1000 ) - ( Math.round( smilData[i].audio.clipBegin * 1000 ) / 1000 ) );
										objThis.slowResult[j].totalduration =  Math.round((Math.round( duration * 1000 ) / 1000) * 1000 ) / 1000;
									}
									
									
									/*if( i>0 && smilData[i].audio.src !=  smilData[i-1].audio.src )
									{
										objThis.slowResult[j].seekbarbegin = 0;
									}*/
									objThis.slowResult[j].seekbarend = ( Math.round(objThis.slowResult[j].seekbarbegin * 1000) / 1000 ) + ( ( Math.round( smilData[i].audio.clipEnd * 1000 ) / 1000 ) - ( Math.round( smilData[i].audio.clipBegin * 1000 ) / 1000 ) );
									objThis.slowResult[j].seekbarend = Math.round(objThis.slowResult[j].seekbarend * 1000) / 1000;
									
									if( !objThis.slowResult[j].audiosrc )
									{
										objThis.slowResult[j].audiosrc = getPathManager().getSMILAudioPath( smilData[i].audio.src );
									}	
									j = j + 1;
								}
							}
								
						}
						objThis.audioPopup.find( '.audioPanelButton' ).css( { 'display' : 'block' } );	
						objThis.updatePosition( );
					},
					error: function ( )
					{
						
					}
				});
	}
	
	this.playSlowAudio = function(){
		var objThis = this;
		if ( objThis.pageLevelAudioPath != "" )
		{
			objThis.slowResult[0].audiosrc = objThis.pageLevelAudioPath.substr(0, objThis.pageLevelAudioPath.lastIndexOf(".")) + "_alt" + objThis.pageLevelAudioPath.substr(objThis.pageLevelAudioPath.lastIndexOf("."),objThis.pageLevelAudioPath.length);
		}
	    objThis.canPlay = true;
	    if($('#slowerButton').hasClass('slowerAudio'))
	    {
		    objThis.stopAudio();
		    objThis.audioPopup.find('#slowerButton').removeClass('slowerAudio');
	   	    objThis.audioPopup.find('#slowerButton').addClass('slowerAudio-enabled');
	   	    setTimeout(function(){
	   	    	var audioSource = objThis.slowResult[0].audiosrc.substr(0, objThis.slowResult[0].audiosrc.lastIndexOf(".")) + ".ogg";
	   	    	if( navigator.userAgent.indexOf('Firefox') != -1 )
				{
	   	    		$('#player').find('source').attr({'src' : audioSource, 'type' : 'audio/ogg' });
	   	    	}
	   	    	else
	   	    	{
	   	    		$('#player').find('source').attr('src' , objThis.slowResult[0].audiosrc);	
	   	    	}
			    $( '#player' ).unbind( 'canplaythrough' ).bind( "canplaythrough" , function ( ) {
			    	$( '#player' ).unbind( 'canplaythrough' );
			    	objThis.playing = true;
			    	objThis.playAudioDependingOnType( false );
			    });
			    $('#player')[0].load();
	   	    },0)
	     }
		 else
		 {
		    objThis.stopAudio();
		    objThis.audioPopup.find('#slowerButton').removeClass('slowerAudio-enabled');
	 	    objThis.audioPopup.find('#slowerButton').addClass('slowerAudio');
	 	    setTimeout(function(){
			    $('#player').find('source').attr('src',objThis.result[0].audiosrc);
			    $( '#player' ).unbind( 'canplaythrough' ).bind( "canplaythrough" , function ( ) {
			    	$( '#player' ).unbind( 'canplaythrough' );
			    	objThis.playing = true;
			    	objThis.playAudioDependingOnType( false );
			    });
			    $('#player')[0].load();
		    },0)
		 }
	}
}

AudioPlayer.prototype.setMediaElement = function( option )
{
	var objThis = this;
	objThis.audioPopup.find('#slowerButton').removeClass('slowerAudio-enabled');
    objThis.audioPopup.find('#slowerButton').addClass('slowerAudio');
	objThis.audioPopup.find( '.audio-play-container' ).unbind( 'click' );
	$( objThis ).unbind( "audio_complete" );
	objThis.pageLevelAudioPath = "";
	objThis.glossaryAudioPath = "";
	objThis.mediaOverlayPath = "";
	objThis.canPlay = true;
	objThis.closeAudio = false;
	
	if( option.mediaOverlayPath )
	{
		objThis.audioPopup.find( '#textContainer' ).css( "display" , "block" );
		objThis.mediaOverlayPath = option.mediaOverlayPath;	
	}
	if( option.hideAudioText )
	{
		objThis.audioPopup.find( '#textContainer' ).css( "display" , "none" );
	}
	if( option.pageLevelAudioPath )
	{
		objThis.pageLevelAudioPath = option.pageLevelAudioPath;	
	}
	
	if( option.glossaryAudioPath )
	{
		objThis.glossaryAudioPath = option.glossaryAudioPath;	
	}

	if( option.currentPageFrame )
	{
		objThis.pageBody = $( option.currentPageFrame );			
	} 
	else
	{
		objThis.pageBody = $( 'body' );
	}
	
	if( option.controls == false )
	{
		objThis.hideControl = true;
	}
	
	if( option.autoplay == undefined )
	{
		option.autoplay = true;
	}
	if( option.highlightClass != undefined )
	{
		objThis.audioSyncClass = option.highlightClass;
	}
	
	objThis.loadPageContent( option.autoplay );
	
}
AudioPlayer.prototype.playAudioDependingOnType = function( status )
{
	var objThis = this;
	if( objThis.pageLevelAudioPath != "" )
	{
		objThis.playWithoutSmilAudio( status );
	}
	else
	{
		objThis.playAudio( status );
	}
}
AudioPlayer.prototype.closeAudioPanel = function()
{
	var objThis = this;
	objThis.closeAudio = true;
	$( '#player' ).unbind( 'canplaythrough' );
	objThis.stopAudio();
	objThis.audioPopup.find('#slowerButton').removeClass('slowerAudio-enabled');
    objThis.audioPopup.find('#slowerButton').addClass('slowerAudio');
	$( objThis ).trigger( "enable_leftpanelbutton" );
	$( objThis ).trigger( "enable_hotspotbutton" );
	clearInterval(objThis.playInterval);
	$( '.audio-popup' ).slideUp( 500 , "easeInOutExpo" , function ( ){ objThis.audioPopup.find( '.audioPanelButton' ).css( { 'display' : 'none' } ); } );
	$(objThis).trigger( "change_audio_state" );
}

