/**
 * @author lakshay.ahuja
 * This class handles the functionality of stickynotepanel and stickynote
 */
var strList = "";
var strTOCList = "";
var level = 0;
var curriculumXml = "";
var isPopupLayoutChanged = false;
var isPopupFirstTime = true;
var androidLandscapeHeight = 768;
var CompModel = {
    xPos:0,
    yPos:0
};

var objToc = $.widget("magic.annotationPanelComp", $.magic.tabViewComp, {
    _strDefaultFontFamily : "Arial",
    _nDefaultFontSize : "3",
    options : {
        setEnabled : true,
        isEnabled : true
    },
    events : {
        STICKY_TEXT_FORMAT_CHANGE : 'stickyTextformatchange'

    },
    member : {
        notePanelScroll: null,
        teacherNotePanelScroll: null,
        isContainerScrollBool : false,
        sortBy: "pageASC",
        teacherNotesSortBy: "pageASC",
        bPlaceCaretAtEnd : "false",
        contentWidth : 820,
    },
    _create: function () {

        var teacherNotesTab = $(this.element).find('[ id = "annotationTeacherNotesTab" ]');

        $("#teacherNoteAlertleft").css("display", "none");

		if( EPubConfig.AnnotationSharing_isAvailable )
		{
	       /* if (GlobalModel.selectedStudentData) {
	            $(teacherNotesTab).css("display","block");
	        }
	        else
	        {
	            $(teacherNotesTab).css("display", "none");
	        }   */
	       $(teacherNotesTab).css("display","block");
	    }            
		else
		{
			$("#annotationTeacherNotesTab").css("display", "none");
		}

        //adding this check because these panels are not handled the way other tab panels are handled
        if (EPubConfig.QAPanel_isAvailable == true || EPubConfig.Notes_isAvailable == true) {
            $("#NotesPanel").css("display", "block");
        }

        var objThis = this;
        $("#bookContentContainer2").bind('scrollstop', function() {
            if (objThis.member.isContainerScrollBool == true) {
                $(objThis).trigger('openStickyNotePanel');
            }

        });
        $("#bookContentContainer").bind('scrollstop', function() {
            if (objThis.member.isContainerScrollBool == true) {
                $(objThis).trigger('openStickyNotePanel');
            }

        });
    },

    _init : function() {
        $.magic.tabViewComp.prototype._init.call(this);
        var objThis = this;

         $(window).bind('orientationchange', function() {
            if (navigator.userAgent.match(/(android)/i) && ($("#stickyNoteContainer").css("display") == "block")) {
                isPopupFirstTime = true;
            }

        });
         var ddOptiosClicked = false;
         var ddTeacherOptiosClicked = false;
        if ($(objThis.element).attr("id") == "NotesPanel") {
            objThis.member.notePanelScroll = new iScroll('addedStickyNotesList', {
                scrollbarClass : 'myScrollbar'
            });

            objThis.member.teacherNotePanelScroll = new iScroll('addedTeacherNotesList', {
                scrollbarClass: 'myScrollbar'
            });

            
            $(window).resize(function() {
                objThis.updateViewSize();
            });

            /*--------------------------------------------Left panel open event ---------------------------------------------------- */
	        $(objThis.element).unbind("panelOpened").bind("panelOpened", function() {
	            $("#teacherNoteAlertleft").css("display", "none");
	        	//Open Annotation Tab/My Question Tab depending on visible Tab 
	        	if ($(objThis.element).find("#annotationMyQuestionsTab")[0].className != "leftPanelTab") {
	        		
	        	    $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "none");
	        	    $($(objThis.element).find('[id="blankTeacherNotesPanelText"]')).css("display", "none");
	        		$(objThis.element).find('[id=annotationSortBy]').css('display', 'none');
	            	$(objThis.element).find('[id=externalURLLink]').css('display', 'none');
	            	objThis.removeAllNotificationsForQuestionTab();
	            	$(objThis).trigger('addDataToQuestionAnswerSectionPanel');
	        	}
	        	else if ($(objThis.element).find("#annotationTeacherNotesTab")[0].className != "leftPanelTab") {
	        	    $($(objThis.element).find('[id="blankTeacherNotesPanelText"]')).css("display", "none");
	        	    $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "none");
	        	    $("#addedTeacherNotes").html("");
	        	    objThis.initTeacherNodes();
	        	}
	        	else {
	        	    $($(objThis.element).find('[id="blankQuestionAnswerPanelText"]')).css("display", "none");
	        	    $($(objThis.element).find('[id="blankTeacherNotesPanelText"]')).css("display", "none");
	        	    objThis.onPanelOpen();
	        	}
	        });
			/*------------------------------------------------------------------------------------------------------------------------ */

            /*--------------------------------------------Left panel close event ---------------------------------------------------- */
	        $(objThis.element).unbind("panelClosed").bind("panelClosed", function() {
	            objThis.onPanelClose();
	        });
	        /*------------------------------------------------------------------------------------------------------------------------ */
	       
	        $(document).bind("stickyNotesDataLoadComplete", function () {	           
	           if ($(objThis.element).find("#annotationMyQuestionsTab")[0].className == "leftPanelTab" && $("#NotesSectionPanel").css("display") == "block")
	           {
	               objThis.onPanelOpen();
	           }
	       });
	       
	       $(document).bind("noteHighlightCommentsLoaded", function () {	           
	           if ($(objThis.element).find("#annotationMyQuestionsTab")[0].className == "leftPanelTab" && $("#NotesSectionPanel").css("display") == "block")
	           {
	           		setTimeout(function(){
	           			objThis.initTeacherNodes();
	           		},1000)
	               
	           }
	       });
	     }

        $(objThis.element).unbind('click').bind('click', function (e) {
           
            if (ddOptiosClicked == false) {
                $($(objThis.element).find("#dd-options")[0]).css("display", "none");               
            } else {
                ddOptiosClicked = false;
            }

            if (ddTeacherOptiosClicked == false) {
                $($(objThis.element).find("#teacherNotes-dd-options")[0]).css("display", "none");
            } else {
                ddTeacherOptiosClicked = false;
            }

            
        });

        $($(objThis.element).find("#dd-clickable")).unbind('click').bind("click", function() {
            ddOptiosClicked = true;
            if ($($(objThis.element).find("#dd-options")[0]).css("display") == "none") {
                $($(objThis.element).find("#dd-options")[0]).css("display", "block");
                $($(objThis.element).find("#dd-options li")).unbind("click").bind("click", function(e) {
                    if ( typeof (e.currentTarget.innerText) != "undefined")
                        $($(objThis.element).find("#dd-selected")).html(e.currentTarget.innerText);
                    else
                        $($(objThis.element).find("#dd-selected")).html(e.currentTarget.textContent);
                    $($(objThis.element).find("#dd-options")[0]).css("display", "none");
                    objThis.member.sortBy = $($(e.currentTarget).find("a")).attr("value");

                    //deleting and recreating data in annotations panel
                    objThis.onPanelOpen();
                    var firstElementId = $($('.panelData')[0]).attr('id');
                    objThis.member.notePanelScroll.scrollToElement("#" + firstElementId, 0);
                });
            } else {
                $($(objThis.element).find("#dd-options")[0]).css("display", "none");
            }
        });

        $($(objThis.element).find("#teacherNotes-dd-clickable")).unbind('click').bind("click", function () {

            ddTeacherOptiosClicked = true;
            if ($($(objThis.element).find("#teacherNotes-dd-options")[0]).css("display") == "none") {
                $($(objThis.element).find("#teacherNotes-dd-options")[0]).css("display", "block");
                $($(objThis.element).find("#teacherNotes-dd-options li")).unbind("click").bind("click", function (e) {
                    if (typeof (e.currentTarget.innerText) != "undefined")
                        $($(objThis.element).find("#teacherNotes-dd-selected")).html(e.currentTarget.innerText);
                    else
                        $($(objThis.element).find("#teacherNotes-dd-selected")).html(e.currentTarget.textContent);
                    $($(objThis.element).find("#teacherNotes-dd-options")[0]).css("display", "none");
                    objThis.member.teacherNotesSortBy = $($(e.currentTarget).find("a")).attr("value");

                    //deleting and recreating data in annotations panel
                    objThis.initTeacherNodes();
                    var firstElementId = $($('.panelData')[0]).attr('id');
                    objThis.member.teacherNotePanelScroll.scrollToElement("#" + firstElementId, 0);
                });
            } else {
                $($(objThis.element).find("#teacherNotes-dd-options")[0]).css("display", "none");
            }
        });

        var objMyNoteBookBtn = $(objThis.element).find("#externalURLLink")[0];
        if (EPubConfig.My_Notebook_isAvailable) {
            if (EPubConfig.My_Notebook_isAvailable == true) {
                $($(objThis.element).find("#externalURLLink")[0]).unbind('click').bind("click", function() {
                    window.open(EPubConfig.MY_NOTEBOOK_PATH, '_blank');
                });
            } else {
                $($(objThis.element).find("#externalURLLink")[0]).css("visibility", "hidden");
            }
        } else {
            $($(objThis.element).find("#externalURLLink")[0]).css("visibility", "hidden");
        }
        //setting the default markup color
        if ($("#markupcolor")[0] == null)
            $("head").append("<style id='markupcolor' type='text/css'>::-moz-selection { background: " + EPubConfig.Markup_default_Color + " !important;} ::selection{ background: " + EPubConfig.Markup_default_Color + " !important;}</style>")

    },
    
    removeAllNotificationsForQuestionTab : function ()
    {
    	for( var question in GlobalModel.questionPanelAnswers )
		{
            for(var annotations in GlobalModel.questionPanelAnswers[question] )
            {
            	GlobalModel.questionPanelAnswers[question][annotations].isNotified = false;
            }
		}
    	GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
    },

    _setOption : function() {

    },

    /**
     * This function updates the height of sticky note panel list & refresh the iscroll
     * return void
     */
    updateViewSize : function() {
        var objThis = this;
        setTimeout(function() {
            var headerHeight = ($("#header").css('display') == 'block') ? $("#header").height() : 1;
            var stickynoteTopHeight = parseInt($("#StickyNotesSectionPanelTop").height());
            $("#addedStickyNotesList").height($($.find("[id=NotesSectionPanel]")).height() - (stickynoteTopHeight + headerHeight + 10));
            $("#addedTeacherNotesList").height($($.find("[id=NotesSectionPanel]")).height() - (stickynoteTopHeight + headerHeight + 10));
            objThis.member.notePanelScroll.refresh();
            objThis.member.teacherNotePanelScroll.refresh();

            var teacherNotesTab = $(objThis.element).find('[ id = "annotationTeacherNotesTab" ]');

            if (ismobile == null) {
                //Handling the click event on scroll bar of iscroll calling the function of MyScroll.js

                if (teacherNotesTab.hasClass("leftPanelTabActive")) {
                    iScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"), objThis.member.teacherNotePanelScroll, "addedTeacherNotesList");
                }
                else {                    
                    iScrollClickHandler($(objThis.element).find("[class=myScrollbarV]"), objThis.member.notePanelScroll, "addedStickyNotesList");
                }
                
            }
        }, 200);

    },
    onhighlightAnnotationBtnClick : function(highlightObj, matchFound) {

        var objThis = this;
        var defaultcolor = highlightObj.defaultcolor;
        if (!ismobile) {
            PopupManager.removePopup();
        } else {
            $("#annotation-bar").css('display', 'none');
        }
        var currentHighlight = $($("#innerScrollContainer")[0]).find('[type=highlighterComp]')[highlightObj.numberofHighlights];
        //This will open the stickynote container panel
        objThis.openHighlightPanel(highlightObj, highlightObj.highlightContainer, highlightObj.rangeData, highlightObj.numberofHighlights, defaultcolor, matchFound);

    },

    setCaretPositionVar : function(boolVal) {
        this.member.bPlaceCaretAtEnd = boolVal;
    },
    
    openHighlightPanel : function(highlightObj, highlightcontainerRef, rangeData, numberofHighlights, defaultcolor, matchFound) {
        var objThis = this;
        PopupManager.removePopup();
        setTimeout(function() {
            objThis.openHighlightContainerPanel(highlightObj, highlightcontainerRef.element, rangeData, numberofHighlights, defaultcolor, matchFound);
        }, 0)
    },

    openHighlightContainerPanel : function(highlightObj, objPopupPanel, rangeData, numberofHighlights, defaultcolor, matchFound) {
        var objThis = this;
        var toCenter = false;
        var panelRef = $(objPopupPanel).find('[type=color]');
        var defaultnum = defaultcolor.replace('selectioncolor', '').replace("_underline", '');

        //making all colors unselected and selectable
        for (var i = 0; i < panelRef.length; i++) {
            $(panelRef[i]).removeClass("colorButtonSelected").addClass("colorButton")
            $(panelRef[i]).css("pointer-events", "auto")
        }

        //making required color selected and unselectable;
        $(panelRef[defaultnum - 1]).removeClass('colorButton').addClass('colorButtonSelected');
        $(panelRef[defaultnum - 1]).css("pointer-events", "none");

        var toppos = highlightObj.topPos + 20;

        //toppos = toppos * GlobalModel.currentScalePercent;
        //toppos = rangeData.commonAncestorContainer.offsetTop * GlobalModel.currentScalePercent;
        //console.log(toppos,rangeData);
        var leftpos = highlightObj.leftPos + 20;
        //console.log("1",leftpos);
        //console.log("2",leftpos);
        if ($(window).height() <= toppos + 110) {
            toppos = toppos - 10 - $(objPopupPanel).height();
        }
        if (leftpos <= 0) {
            leftpos = rangeData.getBoundingClientRect().left;
        } else if (leftpos + $(objPopupPanel).width() >= $(window).width()) {
            leftpos = $(window).width() - $("annotationPanel").width() - $(objPopupPanel).width() - 15;
        }
        if (toppos < 0) {

            toCenter = true;
        }

        $(objPopupPanel[0]).css("display", "none");

        if (toppos >= 0) {
            $(objPopupPanel).css('top', toppos);
            /*  for ff, we may need, X GlobalModel.currentScalePercent*/
            $(objPopupPanel).css('left', leftpos);
        }
        //        var rangeData2 = rangy.createRange();
        //        rangeData2 = rangeData.cloneRange();
        if (matchFound == 1) {
            
            if (navigator.userAgent.match(/(android)/i)) 
            {
            	var startRange = document.createRange();
	            startRange.setStart(rangeData.startContainer, rangeData.startOffset);
	            var $start = $("<span/>");
	            startRange.insertNode($start[0]);
	            $start.remove();
            }
            $(objPopupPanel).find('[type=saveBtn]').removeClass('ui-disabled');
            try{
            	rangy.init();
	            var cssClassApplier = rangy.createCssClassApplier("selection" + numberofHighlights);
	            var cssClassAppliercolor = rangy.createCssClassApplier(defaultcolor);
	            window.getSelection().addRange(rangeData);
	            cssClassApplier.toggleSelection();
	            cssClassAppliercolor.toggleSelection();
            }
            catch(e)
            {
            	bShowPopup = true;
            }
        	var count = 0;
            while ($(".selection" + numberofHighlights).length == 0) {
                if (count > 100) {
                    break;
                }
                cssClassApplier.toggleSelection();
                cssClassAppliercolor.toggleSelection();
                count++;
            }
            //            $(".selection"+numberofHighlights).attr('id', ("selection"+numberofHighlights));

            if (window.getSelection) {
                if (window.getSelection().empty) {// Chrome
                    window.getSelection().empty();
                } else if (window.getSelection().removeAllRanges) {// Firefox
                    window.getSelection().removeAllRanges();
                }
            } else if (document.selection) {// IE?
                document.selection.empty();
            }
        }
        PopupManager.addPopup($(objPopupPanel), $(".selection"+numberofHighlights)[0], {
            isModal : true,
            isCentered : toCenter,
            popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
            isTapLayoutDisabled : true
        });
        $(objPopupPanel[0]).css("display", "block");
        if (EPubConfig.pageView.toUpperCase() == 'SCROLL') {
            if ($(objPopupPanel).offset().top < $($(".selection"+numberofHighlights)[0]).offset().top)
                $(objPopupPanel).css('margin-top', "-2px");
            else
                $(objPopupPanel).css('margin-top', "8px");
        } else
            $(objPopupPanel).css('margin-top', "-2px");

        var annotationPanelWidth = $("#annotationPanel").width();
        if ($(objPopupPanel).offset().left < annotationPanelWidth) {
            $(objPopupPanel).css('left', (annotationPanelWidth + 10));
        }
        PageNavigationManager.addSwipeListner($($(objThis.element).parent().parent().parent()).find('[id="bookContentContainer2"]'), $("#bookContentContainer").data('scrollviewcomp'));

    },

    onPanelClose: function () {
        var objThis = this;
        //GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlertleft" );
        this.updateViewSize();
    },

	/*
     * @description	This method sets the carat index to end of text in a contenteditable div
     * @param {Object} HTML element (contenteditable div)
     * @return void
     */
    placeCaretAtEnd : function(el) {
        if (!ismobile) {
            el.focus();
        }

        if ( typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
            var range = document.createRange();
            range.selectNodeContents(el);
            range.collapse(false);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        } else if ( typeof document.body.createTextRange != "undefined") {
            var textRange = document.body.createTextRange();
            textRange.moveToElementText(el);
            textRange.collapse(false);
            textRange.select();
        }
    },

    switchSavetoNote : function(savetonoteChk, savetoNoteBookButtonRef) {

        if (savetonoteChk == 1) {
            $(savetoNoteBookButtonRef).addClass('savetoNoteBookBtnChk');
            $(savetoNoteBookButtonRef).removeClass('savetoNoteBookBtn');

            return 0;
        } else {
            $(savetoNoteBookButtonRef).removeClass('savetoNoteBookBtnChk');
            $(savetoNoteBookButtonRef).addClass('savetoNoteBookBtn');

            return 1;
        }
    },
   
    
    /**
     * This function is called from MG.PanelViewOperator.
     */
    onPanelOpen : function() {
        var objThis = this;
    	var todaysDate = getTodaysDate();
    	var yesterdaysDate = getYesterdaysDate();
    	var strListPart_2 = '<div style="margin-top:5px;"><div style="margin-top: 100px; width: 100%;" align="center"><div class="pagePreloader"></div></div></div>';
    	$("#teacherNoteAlertleft").css("display", "none");
    	objThis.annotationArrSort(objThis.member.sortBy);
    	var annotationData = "";

    	var teacherNotesTab = $(this.element).find('[ id = "annotationTeacherNotesTab" ]');

		if( EPubConfig.AnnotationSharing_isAvailable )
		{
			/* if (GlobalModel.selectedStudentData) {
    	    $(teacherNotesTab).css("display", "block");
	    	}
	    	else {
	    	    $(teacherNotesTab).css("display", "none");
	    	} */
	    	$(teacherNotesTab).css("display", "block");
		}
    	else
    	{
    		$(teacherNotesTab).css("display", "none");
    	}
    	var noteArr = $.grep(GlobalModel.annotations, function (obj) {
            return ( obj.type == "stickynote" && !( obj.shared_with ) ) || ( obj.type == "highlight" && !( obj.shared_with ) );
        });
    	if( noteArr.length == 0 )
    	{
    	    $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "block");
            $(objThis.element).find('[id=annotationSortBy]').css('display', 'none');
            $(objThis.element).find('[id=externalURLLink]').css('display', 'none');
            $("#addedStickyNotes").html("");
            //$("#teacherNoteAlert").css("display", "none");
            return;
    	}
    	else
    	{    	   
    	    $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "none");
            $(objThis.element).find('[id=annotationSortBy]').css('display', 'block');
            $(objThis.element).find('[id=externalURLLink]').css('display', 'block');
            $("#addedStickyNotes").html(strListPart_2);

            //GlobalModel.updateTeacherNotificationIcon( "teacherNoteAlert" );
    	}
        
        if ( $(objThis.element).find('[id="annotationTeacherNotesTab"]').hasClass('leftPanelTabActive') && GlobalModel.BookEdition == "TSE" )
    	{
    		$(objThis.element).find('[id=externalURLLink]').css('display', 'none');
    	}
	    	
       setTimeout( function ()
       {
       for (var i = 0; i < GlobalModel.annotations.length; i++) 
        {
        	var annotationID = GlobalModel.annotations[i].id;
        	if (GlobalModel.annotations[i]) {
        		var annoDate = GlobalModel.annotations[i].comments ? GlobalModel.annotations[i].comments : GlobalModel.annotations[i] ;
        		var dayData = "";
                if (annoDate.date == yesterdaysDate) {
                    dayData = GlobalModel.localizationData["DATE_YESTERDAY"];
                } else if (annoDate.date == todaysDate) {
                    dayData = GlobalModel.localizationData["DATE_TODAY"];
                } else {
                    dayData = annoDate.date;
                }
                var iPageNumber = GlobalModel.annotations[i].pageNumber;
        		if (GlobalModel.annotations[i].type == "highlight") 
        		{
        			if( GlobalModel.annotations[i].shared_with )
        		    {
        		        continue;
        		    }
        			var titleStr = GlobalModel.annotations[i].title;
                    var color = GlobalModel.annotations[i].selectioncolor.replace('_underline', '');
                    if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData &&
                    	GlobalModel.annotations[i].teachersSharedWith &&
                    	GlobalModel.annotations[i].teachersSharedWith.results )
         	        {
         	        	var className =  "highlightDataIcon highlightTeacherGrpSharingLeftPanelDataIcon";
         	        }
         	        else
         	        {
         	        	var className =  "highlightDataIcon " + color + "_icon";
         	        }
                    annotationData += "<div id='highlightData_" + annotationID + "' page='" + iPageNumber + "' class='panelData stickynoteData' type='highlightData' style='text-shadow: none;'><div class='annotationData'><span id='stickynoteDataDayData' class='dayData'>" + dayData + "</span><span class='panelPageNumber'>" + GlobalModel.localizationData["PAGE_TEXT_FREE"] + iPageNumber + "</span><div class='" + className + "'></div><span id='stickynoteDataTitle' class='dataTitle'>" + $(titleStr).text() + "</span><span id='stickynoteDataTitle_detail' class='dataTitle' style='display:none;'><span>" + $(titleStr).text() + "</span></span></div><div id='editDeleteAnnotation'></div><div id='expandCollapseNoteList_" + annotationID + "' class='expandCollapseNoteList showDetail' type='expandCollapseNoteList' >" + GlobalModel.localizationData["STICKY_NOTE_PANEL_SHOW_DETAILS_BUTTON"] + "</div></div>"
        		}
        		else
        		{
        		    if( GlobalModel.annotations[i].shared_with )
        		    {
        		        continue;
        		    }
        			var regex = /(<([^>]+)>)/ig;
                    var titleStr = GlobalModel.annotations[i].title;
                    
                    if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData && GlobalModel.annotations[i].teachersSharedWith &&
                     GlobalModel.annotations[i].teachersSharedWith.results )
                    {
                    	if( GlobalModel.annotations[i].type == "highlight" )
                    	{
                    		var classname  = "stickynoteDataIcon highlightTeacherGrpSharingLeftPanelDataIcon";
                    	}
                    	else
                    	{
                    		var classname  = "stickynoteDataIcon stickynoteTeacherGrpSharingLeftPanelDataIcon";
                    	}
                    	
                    }
                    else
                    {
                    	var classname  = "stickynoteDataIcon";
                    }

                    //if only images are present in a note, display alt text (preffered) or src if alt text is not available;
                    if ($.trim(titleStr.replace(regex, "")) == "") {
                        titleStr = "";

                        var strTemp = "<div>" + GlobalModel.annotations[i].title + "</div>";

                        var objTemp = $(strTemp).find("img");
                        for (var imgCounter = 0; imgCounter < objTemp.length; imgCounter++) {
                            if ($(objTemp[imgCounter]).attr("alt") && $(objTemp[imgCounter]).attr("alt") != "")
                                titleStr += $(objTemp[imgCounter]).attr("alt") + ", "
                            else
                                titleStr += $(objTemp[imgCounter]).attr("src") + ", "
                        }

                        titleStr = titleStr.substr(0, titleStr.length - 2);
                    }
                    annotationData += "<div id='stickynoteData_" + annotationID + "' noteRef='" + annotationID + "' page='" + iPageNumber + 
                    "'  class='panelData stickynoteData' type='stickynoteData' style='text-shadow: none;'><div class='annotationData'><span id='stickynoteDataDayData' class='dayData'>" + 
                    dayData + "</span><span class='panelPageNumber'>" + GlobalModel.localizationData["PAGE_TEXT_FREE"] + iPageNumber + 
                    "</span><div class='" + classname + "'></div><span id='stickynoteDataTitle' class='dataTitle'>" + titleStr.replace(regex, "") + 
                    "</span><span id='stickynoteDataTitle_detail' class='dataTitle' style='display:none;'>" + GlobalModel.annotations[i].title + 
                    "</span></div><div id='editDeleteAnnotation'></div><div id='expandCollapseNoteList_" + annotationID + 
                    "' class='expandCollapseNoteList showDetail' type='expandCollapseNoteList' >" + GlobalModel.localizationData["STICKY_NOTE_PANEL_SHOW_DETAILS_BUTTON"] + "</div></div>";
                    
        		}
        	}
        }

        

        $(objThis).trigger('addDataToStickyNoteSectionPanel', annotationData);
      
        
        //if (annotationData == "") 
       // {
     //       $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "block");
     //       $(objThis.element).find('[id=annotationSortBy]').css('display', 'none');
       //     $(objThis.element).find('[id=externalURLLink]').css('display', 'none');
       // } 
      //  else 
       // {
       //     $($(objThis.element).find('[id="blankNotesPanelText"]')).css("display", "none");
       //     $(objThis.element).find('[id=annotationSortBy]').css('display', 'block');
        //    $(objThis.element).find('[id=externalURLLink]').css('display', 'block');
        //}     
        objThis.updateViewSize();
        }, 500 );
    },

    initTeacherNodes: function ()
    {
    	var objThis = this;
        var teacherNotesData = "";
        var todaysDate = getTodaysDate();
        var yesterdaysDate = getYesterdaysDate();
        var strListPart_2 = '<div style="margin-top:5px;"><div style="margin-top: 100px; width: 100%;" align="center"><div class="pagePreloader"></div></div></div>';

        objThis.annotationArrSort(objThis.member.teacherNotesSortBy);

        var teacherNoteAlert = $("#teacherNoteAlert");
        var addedTeacherNotes = $("#addedTeacherNotes");
        var teacherNotesTab = $("#annotationTeacherNotesTab");
        $("#teacherNoteAlert").css("display", "none");
        
        var teacherNoteArr = $.grep(GlobalModel.annotations, function (obj) {
            return ( obj.comments || obj.shared_with );
        });
        if (teacherNoteArr.length > 0) {
            $("#addedTeacherNotes").html(strListPart_2);
            $($(objThis.element).find('[id="blankTeacherNotesPanelText"]')).css("display", "none");
            $(objThis.element).find('[id=teacherAnnotationSortBy]').css('display', 'block');
            $(objThis.element).find('[id=externalURLLink]').css('display', 'block');
        }
        else {
            $($(objThis.element).find('[id="blankTeacherNotesPanelText"]')).css("display", "block");
            $(objThis.element).find('[id=teacherAnnotationSortBy]').css('display', 'none');
            $(objThis.element).find('[id=externalURLLink]').css('display', 'none');
        }
		setTimeout( function ()
        {
            if (GlobalModel.annotations) {
            	// TODO HC proper icons and attributes
                for (var i = 0; i < GlobalModel.annotations.length; i++) {
                    if ( ( GlobalModel.annotations[i].comments || GlobalModel.annotations[i].shared_with ) ) {

                        var annotationID = GlobalModel.annotations[i].id;
                        var item = GlobalModel.annotations[i].comments ? GlobalModel.annotations[i].comments : GlobalModel.annotations[i] ;
                        var dayData = "";
                        if (item.date == yesterdaysDate) {
                            dayData = GlobalModel.localizationData["DATE_YESTERDAY"];
                        } else if (item.date == todaysDate) {
                            dayData = GlobalModel.localizationData["DATE_TODAY"];
                        } else {
                            dayData = GlobalModel.annotations[i].date;
                        }
                        var iPageNumber = GlobalModel.annotations[i].pageNumber;
						var regex = /(<([^>]+)>)/ig;
                        var titleStr = GlobalModel.annotations[i].comments ? GlobalModel.annotations[i].comments.body_text : GlobalModel.annotations[i].title ;
                        var title = titleStr;
                        //if only images are present in a note, display alt text (preffered) or src if alt text is not available;
                        if ($.trim(titleStr.replace(regex, "")) == "") {
                            titleStr = "";

                            var strTemp = "<div>" + title + "</div>";

                            var objTemp = $(strTemp).find("img");
                            for (var imgCounter = 0; imgCounter < objTemp.length; imgCounter++) {
                                if ($(objTemp[imgCounter]).attr("alt") && $(objTemp[imgCounter]).attr("alt") != "")
                                    titleStr += $(objTemp[imgCounter]).attr("alt") + ", ";
                                else
                                    titleStr += $(objTemp[imgCounter]).attr("src") + ", ";
                            }

                            titleStr = titleStr.substr(0, titleStr.length - 2);
                        }
                        
                        // classname
                        if( GlobalModel.annotations[i].comments )
                        {
                        	if( GlobalModel.annotations[ i ].type == "stickynote" )
                        	{
                        		var className =  " class='stickynoteDataIcon noteComment' ";
                        	}
                        	else
                        	{
                        		var className =  " class='stickynoteDataIcon highlightComment' ";
                        	}
                           
                           if( GlobalModel.annotations[i].isNotified )
                           {
                           	   var itemClassname = " class='panelData panelDataUnRead stickynoteData' ";
                           }
                           else
                           {
                           	   var itemClassname = " class='panelData stickynoteData' ";
                           }
                        }
                        else if( GlobalModel.annotations[i].shared_with )
                        {
                           if( GlobalModel.annotations[i].type == "stickynote" )
                           {
                           		var className =  " class='stickynoteDataIcon standalonecomment' ";
                           }
                           else
                           {
                           		var color = GlobalModel.annotations[i].selectioncolor.replace('_underline', '');
                           		if( GlobalModel.BookEdition == "TSE" && !GlobalModel.selectedStudentData && GlobalModel.annotations[i].teachersSharedWith &&
                     	        GlobalModel.annotations[i].teachersSharedWith.results )
                     	        {
                     	        	var className =  " class='highlightDataIcon highlightTeacherGrpSharingLeftPanelDataIcon' ";
                     	        }
                     	        else
                     	        {
                     	        	var className =  " class='highlightDataIcon " + color + "_icon' ";
                     	        }
                           		
                           }
                           
                           if( GlobalModel.annotations[i].isNotified )
                           {
                           	   var itemClassname = " class='panelData panelDataUnRead stickynoteData' ";
                           }
                           else
                           {
                           	   var itemClassname = " class='panelData stickynoteData' ";
                           }
                        }
                        else
                        {
                            var className =  " class='stickynoteDataIcon' ";
                        }
                        
                        // Type
                        if( GlobalModel.annotations[i].type == "stickynote" )
                        {
                        	var type = "type='stickynoteComment'";
                        }
						else
						{
							var type = "type='stickynoteCommentHighlight'";
						}
						if( GlobalModel.annotations[i].type == "highlight" )
                    	{
                    		var annotationsTitle  = $(GlobalModel.annotations[i].title).text();
                    	}
                    	else
                    	{
                    		var annotationsTitle  = GlobalModel.annotations[i].title;
                    	}
                        teacherNotesData += "<div id='stickynoteComment_" + (annotationID) + "' noteRef='" + (annotationID) + "' page='" + iPageNumber + "'" + itemClassname +
                                             type + " style='text-shadow: none;'><div class='teacherAnnotationData'><span id='stickynoteDataDayData' class='dayData'>" + 
                                            dayData + "</span><span class='panelPageNumber'>" + GlobalModel.localizationData["PAGE_TEXT_FREE"] + iPageNumber +
                                            "</span><div" +  className + "></div><span id='stickynoteDataTitle' class='dataTitle'>" +
                                            titleStr.replace(regex, "") + "</span><span id='stickynoteDataTitle_detail' class='dataTitle' style='display:none;'>" +
                                            ( GlobalModel.annotations[i].comments ? GlobalModel.annotations[i].comments.body_text : annotationsTitle ) + "</span></div><div id='editDeleteAnnotation'></div><div id='expandCollapseNoteList_" +
                                            annotationID + "' class='expandCollapseNoteList showDetail' type='expandCollapseNoteList' >" +
                                            GlobalModel.localizationData["STICKY_NOTE_PANEL_SHOW_DETAILS_BUTTON"] + "</div></div>";
                        

                    }
                    GlobalModel.annotations[ i ].isNotified = false;
                }
                $(objThis).trigger('addDataToTeacherNoteSectionPanel', teacherNotesData);
                objThis.updateViewSize();
                //addedTeacherNotes.html(teacherNotesData);
            }
        },500);
        
    },

    scrollToPanelElement : function(obj) {
        this.updateViewSize();
        this.member.notePanelScroll.scrollToElement(obj, 0);
    },

    annotationArrSort : function(option) {
    	var objThis = this;
		var teacherNotesTab = $(objThis.element).find('[ id = "annotationTeacherNotesTab" ]');
        switch (option) {
            case "dateASC":
                GlobalModel.annotations.sort(function(a, b) {
                	var firstGroupTime = a.timeMilliseconds;
                	var secondGroupTime = b.timeMilliseconds;
                	if (teacherNotesTab.hasClass("leftPanelTabActive")) {
	                	if ( a.comments )
	                	{
	                		firstGroupTime = a.comments.timeMilliseconds;
	                	}
	                	if ( b.comments )
	                	{
	                		secondGroupTime = b.comments.timeMilliseconds;
	                	} 
	                }
                    var c = new Date( firstGroupTime );
                    var d = new Date( secondGroupTime );
                    return c - d;
                });
                break;

            case "dateDSC":
                GlobalModel.annotations.sort(function(a, b) {
                	var firstGroupTime = a.timeMilliseconds;
                	var secondGroupTime = b.timeMilliseconds;
                	if (teacherNotesTab.hasClass("leftPanelTabActive")) {
	                	if ( a.comments )
	                	{
	                		firstGroupTime = a.comments.timeMilliseconds;
	                	}
	                	if ( b.comments )
	                	{
	                		secondGroupTime = b.comments.timeMilliseconds;
	                	} 
	                }
                    var c = new Date( firstGroupTime );
                    var d = new Date( secondGroupTime );
                    return d - c;
                });
                break;

            case "pageASC":
                GlobalModel.annotations.sort(function(a, b) {
                    var c = GlobalModel.pageBrkValueArr.indexOf(a.pageNumber);
                    var d = GlobalModel.pageBrkValueArr.indexOf(b.pageNumber);
                    return c - d;
                });
                break;

            case "pageDSC":
                GlobalModel.annotations.sort(function(a, b) {
                    var c = GlobalModel.pageBrkValueArr.indexOf(a.pageNumber);
                    var d = GlobalModel.pageBrkValueArr.indexOf(b.pageNumber);
                    return d - c;
                });
                break;
        }
    },

    enable : function() {
        this.element.attr("disabled", false);
        this.element.removeClass("ui-disabled").attr("aria-disabled", false);
        this.options.isEnabled = true;
        if (this.options.disabledstyle) {
            $(this.element).removeClass(this.options.disabledstyle);
        }
        return this._setOption("disabled", false);
    },

    disable : function() {
        this.element.attr("disabled", true);
        this.element.addClass("ui-disabled").attr("aria-disabled", true);
        this.options.isEnabled = false;
        if (this.options.disabledstyle) {
            $(this.element).addClass(this.options.disabledstyle);
        }
        return this._setOption("disabled", true);
    },

    destroy : function() {
        $.widget.prototype.destroy.call(this);
    }
});
