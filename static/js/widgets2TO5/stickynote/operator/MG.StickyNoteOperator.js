/**
 * @author lakshay.ahuja
 * This class will handle all the functionalities of stickynotes
 */
var defaultNoteHeight;
var preTopPos = 0;
var sticknotetext= "";
MG.StickyNoteOperator = function(){
    _objHotspot = null;
    this.stickynotecontainerRef = "";
    this.stickynoteRef = "";
    this.newElement = true;
    this.stickyNoteDataChanged = false;
    this.deleteConfirmationPanel = "";
    this.noteLimitPanel = "";
    this.scrollviewRef = "";
    this.stickynoteClicked = "";
    this.noteToDeleteThroughAnnotationPanel = "";
    this.noteToDeleteId = -1;
    this.getCurrentstickynote = "";
    this.savetoNote = 1;
    this.tagAddHeight = 44;
    this.tagListArray = [];
    this.temptagListArray = [];
    this.autoCompleteTagsList = [];
    this.tagError = 0;
    this.currentStickyEdit = -1;
}

MG.StickyNoteOperator.prototype = new MG.BaseOperator();

MG.StickyNoteOperator.prototype.attachComponent = function(objComp){
    if (objComp == null) 
        return;
    
    var objThis = this, strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    var isDeleteNote = false;
    switch (strType) {
    
        case AppConst.STICKYNOTE_LAUNCHER:
            break;
            
        // This will contain functionality of the stickynote icon
        case AppConst.STICKY_NOTE_COMP:
        
        	if($(objComp.element).parent().length == 0)
        	{
        		//done to add buttons in the page when created during retreive process from server;
        		$("#innerScrollContainer").find('[id="PageContainer_' + $(objComp.element).attr("associated-page-number") +'"]').append(objComp.element);
        	}
        	
            $("#stickyNoteContainer").height(defaultNoteHeight);
            $($('.tagBorder')[0]).css("display", "none");
            objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'setNewStickyNoteCreated', objComp);
            //when the stickynote icon is clicked
            $(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(){
            	if(GlobalModel.hasDPError("notes"))
            		return;
                var objClass = this;
                $($("#bookContentContainer").data('scrollviewcomp')).trigger('closeOpenedPanel'); // handled in panelviewoperator. To close the panel opened on stickynote click
                isDeleteNote = true; // The stickynote clicked is deletable.
                objThis.stickynoteRef = $(objClass.element)[0];
                //stickynotetxt will contain the value that will be displayed in the textarea of the stickynote panel.
                var stickynoteTxt = $(objClass.element).attr('value');
                setTimeout(function(){
                    objThis.savetoNote = objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'onStickyNoteHotSpotClick', stickynoteTxt, objClass);
                }, 0);
            });
            break;
            
        // This will define all the functionality of sticky note panel.    
        case AppConst.STICKYNOTE_CONTAINER:

           	$(document).bind("stickynoteEdit", function(e, editID)
			{
			                objThis.editStickyNoteData(editID);
			});
            
           	$(document).bind("annotationServiceInitiated", function(){
				ServiceManager.getAnnotations(3);
			});
            
            objThis.stickynotecontainerRef = objComp;
            var saveBtnRef = $(objComp.element).find('[type=saveBtn]')[0];
            var deleteBtnRef = $(objComp.element).find('[type=deleteBtn]')[0];
            var stickynoteTxtAreaRef = $(objComp.element).find('[type=stickynoteTextArea]')[0];
            var stickynoteClsBtnRef = $(objComp.element).find('[type=closeButton]')[0];
            var savetoNoteBookButtonRef = $(objComp.element).find('[type=savetoNoteBookButton]')[0];
            var addTagBtnRef = $(objComp.element).find('[type=tagAddBtn]')[0];
            var tagInputBoxRef = $(objComp.element).find('[id=tagInputBox]')[0];
            
            var savetonoteChk = 1;
            
            if(EPubConfig.My_Notebook_isAvailable)
            	$(".savetonotebook").css("display","block");
            
            $(savetoNoteBookButtonRef).addClass('ui-disabled');
            $(savetoNoteBookButtonRef).data('buttonComp').disable();
            
            var saveToNoteButtonRole = $(savetoNoteBookButtonRef).data('buttonComp');
            $(saveToNoteButtonRole).unbind(saveToNoteButtonRole.events.CLICK).bind(saveToNoteButtonRole.events.CLICK, function(){
            
                $(saveBtnRef).removeClass('ui-disabled');
                $(saveBtnRef).data('buttonComp').enable();
                //                objThis.newElement = false;
                savetonoteChk = objThis.savetoNote;
                if(document.activeElement.id == "stickynoteTextArea" )
                {
                	$('#toRemoveFocus').removeAttr("disabled").focus().blur().attr("disabled", "disabled");
                }
                objThis.savetoNote = objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'switchSavetoNote', savetonoteChk, savetoNoteBookButtonRef);
                /*if (savetonoteChk == 1) {
                    defaultNoteHeight = $(objComp.element).height();
                    var updatedHeight = defaultNoteHeight + objThis.tagAddHeight;
                    $(objComp.element).css("height", updatedHeight);
                    $($(objComp.element).find('[class=stickyNoteTags]')[0]).css("display", "block");
                    $($(objComp.element).find('[class=tagBorder]')[0]).css("display", "block");
                    PopupManager.positionPopup(true);
                }
                else {
                    $(objComp.element).css("height", defaultNoteHeight);
                    $($(objComp.element).find('[class=stickyNoteTags]')[0]).css("display", "none");
                    $($(objComp.element).find('[class=tagBorder]')[0]).css("display", "none");
                    PopupManager.positionPopup(true);
                    objThis.temptagListArray = [];
                    $($(objComp.element).find('[id=tagList]')[0]).html("");
                    $(tagInputBoxRef).val("");
                    $(addTagBtnRef).addClass('ui-disabled');
                }*/
            });
            
            $(tagInputBoxRef).keyup(function(e){
            
                if ($.trim($(tagInputBoxRef).val()) == "") {
                    $(addTagBtnRef).addClass('ui-disabled');
                    $(addTagBtnRef).data('buttonComp').disable();
                }
                else {
                    $(addTagBtnRef).removeClass('ui-disabled');
                    $(addTagBtnRef).data('buttonComp').enable();
                    jQuery.unique(objThis.autoCompleteTagsList)
                    $(tagInputBoxRef).autocomplete({
                        source: objThis.autoCompleteTagsList
                    //appendTo: ".searchSuggestionDiv"
                    });
                    if (e.keyCode == 13) {
                    	$(".ui-autocomplete").css("display", "none");
                        if ($.trim($(stickynoteTxtAreaRef).text()) != "") {
                            $(saveBtnRef).removeClass('ui-disabled');
                            $(saveBtnRef).data('buttonComp').enable();
                        }
                        
                        if (objThis.temptagListArray.length == EPubConfig.Tags_Per_Note) {
                            $(tagInputBoxRef).blur();
                            objThis.tagError = 1;
                            PopupManager.removePopup();
                            $("#alertTxt").html(GlobalModel.localizationData["MAXIMUM_TAG_ADDED"]);
                            setTimeout(function(){
                                PopupManager.addPopup($("#alertPopUpPanel"), null, {
                                    isModal: true,
                                    isCentered: true,
                                    popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
                                    isTapLayoutDisabled: true
                                });
                            }, 0);
                        }
                        else 
                            if (objThis.temptagListArray.indexOf($.trim($(tagInputBoxRef).val())) != -1) {
                                $(tagInputBoxRef).blur();
                                objThis.tagError = 1;
                                PopupManager.removePopup();
                                $("#alertTxt").html(GlobalModel.localizationData["SAME_TAG_ADDED"]);
                                setTimeout(function(){
                                    PopupManager.addPopup($("#alertPopUpPanel"), null, {
                                        isModal: true,
                                        isCentered: true,
                                        popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
                                        isTapLayoutDisabled: true
                                    });
                                }, 0);
                            }
                            else {
                                objThis.temptagListArray.push($.trim($(tagInputBoxRef).val()));
                                objThis.autoCompleteTagsList.push($.trim($(tagInputBoxRef).val()));
                                var tagDiv = "<div class='tagOuter'><div class='tag'>" + $.trim($(tagInputBoxRef).val()) + "<div id='tag' data-role='simpleButtonComp' class='tagClsBtn' type='closeButton'></div></div><span class='tagTweek'></span></div>";
                                $($(objComp.element).find('[id=tagList]')[0]).append(tagDiv);
                                objThis.updateTagHeight(objComp);
                                $(tagInputBoxRef).val("");
                                $(addTagBtnRef).addClass('ui-disabled');
                                $(addTagBtnRef).data('buttonComp').disable();
                                objThis.stickyNoteDataChanged = true;
                                $($(objComp.element).find("[id=tag]")).unbind('click').bind('click', function(){
                                    for (var j = 0; j < objThis.temptagListArray.length; j++) {
                                        if ($(this).parent().parent().find(".tag")[0].innerHTML == (objThis.temptagListArray[j] + '<div id="tag" data-role="simpleButtonComp" class="tagClsBtn" type="closeButton"></div>')) {
                                            objThis.temptagListArray.splice(j, 1);
                                            break;
                                        }
                                    }
                                    $(this).parent().parent().remove();
                                    objThis.updateTagHeight(objComp);
                                    objThis.stickyNoteDataChanged = true;
                                });
                            }
                    }
                }
            });
            
            // On clicking the close btn on the sticky note panel
            if(ismobile) {
            	var eventType = 'vclick';
            } else {
            	var eventType = 'click';
            }
            $(stickynoteClsBtnRef).unbind(eventType).bind(eventType, function(){
            	preTopPos = 0;
                isDeleteNote = false; // On close button click, delete functioanlity wont work.
                if(document.activeElement.id == "stickynoteTextArea" )
                {
                	$('#toRemoveFocus').removeAttr("disabled").focus().blur().attr("disabled", "disabled");
                }
                
                PopupManager.removePopup();
                $(tagInputBoxRef).val("");
                $($(objComp.element).find('[id=tagList]')[0]).html("");
                objThis.temptagListArray = [];
                $(addTagBtnRef).addClass('ui-disabled');
                $(addTagBtnRef).data('buttonComp').disable();
                $($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationTxt]')[0]).html(GlobalModel.localizationData["STICKY_NOTE_DISCARD_CONFIRMATION_TEXT"]);
                $($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationSuccess]')[0]).html(GlobalModel.localizationData["STICKY_NOTE_DISCARD_CONFIRMATION_CLOSE_BUTTON"]);
                /* When the note is new note or some data has been changed, 
             and the user doesn't save the note before closing the panel, this popup will appear.*/
                if (objThis.newElement == true || objThis.stickyNoteDataChanged == true) {
                        $(objThis.stickynotecontainerRef).trigger('deleteConfirmation');
                    
                }
                else {
                //                $(".noteselection").removeClass("noteselection").addClass("noteselectionSwitch");
                }
                if(ismobile)
               	 	objThis.scrollviewRef.options.rangedata = "";
            });
            
            // On clicking the add tag btn on the sticky note panel
            var addTagBtnRole = $(addTagBtnRef).data('buttonComp');
            $(addTagBtnRole).unbind(addTagBtnRole.events.CLICK).bind(addTagBtnRole.events.CLICK, function(){
            	if($(addTagBtnRef).hasClass('ui-disabled')) {
            		return;
            	}
                if (objThis.temptagListArray.length == EPubConfig.Tags_Per_Note) {
                    $(tagInputBoxRef).blur();
                    objThis.tagError = 1;
                    $("#alertTxt").html(GlobalModel.localizationData["MAXIMUM_TAG_ADDED"]);
                    PopupManager.removePopup();
                    setTimeout(function(){
                        PopupManager.addPopup($("#alertPopUpPanel"), null, {
                            isModal: true,
                            isCentered: true,
                            popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
                            isTapLayoutDisabled: true
                        });
                    }, 0);
                }
                else 
                    if (objThis.temptagListArray.indexOf($.trim($(tagInputBoxRef).val())) != -1) {
                        $(tagInputBoxRef).blur();
                        objThis.tagError = 1;
                        PopupManager.removePopup();
                        $("#alertTxt").html(GlobalModel.localizationData["SAME_TAG_ADDED"]);
                        setTimeout(function(){
                            PopupManager.addPopup($("#alertPopUpPanel"), null, {
                                isModal: true,
                                isCentered: true,
                                popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
                                isTapLayoutDisabled: true
                            });
                        }, 0);
                    }
                    else {
                        objThis.temptagListArray.push($.trim($(tagInputBoxRef).val()));
                        objThis.autoCompleteTagsList.push($.trim($(tagInputBoxRef).val()));
                        var tagDiv = "<div class='tagOuter'><div class='tag'>" + $.trim($(tagInputBoxRef).val()) + "<div id='tag' data-role='simpleButtonComp' class='tagClsBtn' type='closeButton'></div></div><span class='tagTweek'></span></div>";
                        $($(objComp.element).find('[id=tagList]')[0]).append(tagDiv);
                        objThis.updateTagHeight(objComp);
                        $(tagInputBoxRef).val("");
                        $(addTagBtnRef).addClass('ui-disabled');
                        $(addTagBtnRef).data('buttonComp').disable();
                        objThis.stickyNoteDataChanged = true;
                        if ($.trim($(stickynoteTxtAreaRef).text()) != "") {
                            $(saveBtnRef).removeClass('ui-disabled');
                            $(saveBtnRef).data('buttonComp').enable();
                        }
                        $($(objComp.element).find("[id=tag]")).unbind('click').bind('click', function(){
                            for (var j = 0; j < objThis.temptagListArray.length; j++) {
                                if ($(this).parent().parent().find(".tag")[0].innerHTML == (objThis.temptagListArray[j] + '<div id="tag" data-role="simpleButtonComp" class="tagClsBtn" type="closeButton"></div>')) {
                                    objThis.temptagListArray.splice(j, 1);
                                    break;
                                }
                            }
                            $(this).parent().parent().remove();
                            objThis.updateTagHeight(objComp);
                            objThis.stickyNoteDataChanged = true;
                        });
                    }
            });
            
            // The following block will enable the save btn whenevr something is entered into the textarea.
            $(stickynoteTxtAreaRef).unbind('click').bind('click', function(e){
                var objClass = this;
                
                if (this == e.target ) {
                    $($(objThis.stickynotecontainerRef.element).find('[type=stickynoteTextArea]')[0]).attr('contenteditable', 'true');
                    $($(objThis.stickynotecontainerRef.element).find('[type=stickynoteTextArea]')[0]).find('a').css('cursor', 'auto');
                    $($(objThis.stickynotecontainerRef.element).find('[type=stickynoteTextArea]')[0]).find('a').css('text-decoration', 'none');
                    
                // objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'placeCaretAtEnd', $(objThis.stickynotecontainerRef.element).find('[type=stickynoteTextArea]')[0]);
                } else if (jQuery.contains($(this), $(e.target))) {
                	setTimeout(function() {
                		$($(objThis.stickynotecontainerRef.element).find('[type=stickynoteTextArea]')[0]).attr('contenteditable', 'true');
                    $($(objThis.stickynotecontainerRef.element).find('[type=stickynoteTextArea]')[0]).find('a').css('cursor', 'auto');
                    $($(objThis.stickynotecontainerRef.element).find('[type=stickynoteTextArea]')[0]).find('a').css('text-decoration', 'none');
                	},200);
                }
            });
            $(stickynoteTxtAreaRef).unbind('keyup').bind('keyup', function(){
                objThis.stickyNoteDataChanged = true;
                if ($.trim($(stickynoteTxtAreaRef).text()) != "" || $(stickynoteTxtAreaRef).html().toString().indexOf("<img") > -1) {
                    $(saveBtnRef).removeClass('ui-disabled');
                    $(saveBtnRef).data('buttonComp').enable();
                    $(savetoNoteBookButtonRef).removeClass('ui-disabled');
                    $(savetoNoteBookButtonRef).data('buttonComp').enable();
                    
                }
                else {
                    $(saveBtnRef).addClass('ui-disabled');
                    $(saveBtnRef).data('buttonComp').disable();
                    $(savetoNoteBookButtonRef).addClass('ui-disabled');
                    $(savetoNoteBookButtonRef).data('buttonComp').disable();
                }
                
            });
            
            $(stickynoteTxtAreaRef).unbind('paste').bind('paste', function(e) {
                    if(ismobile) {
                              e.preventDefault();
                              var clipboardData = event.clipboardData.getData('text/plain');
                              if(clipboardData !== 'undefined' && clipboardData != "" ) {
                                    $(saveBtnRef).removeClass('ui-disabled');
                                    $(saveBtnRef).data('buttonComp').enable();
                                    var selObj = window.getSelection();
									var selRange = selObj.getRangeAt(0);
									selRange.extractContents();
								    
									var newElement = document.createTextNode(clipboardData);
								    selRange.insertNode(newElement);
									selObj.removeAllRanges();
									return;
                                    //var htmlCont = $(stickynoteTxtAreaRef).html()  + clipboardData;
                                    //$(stickynoteTxtAreaRef).html(htmlCont);
                         }
                     } else {
                              $(saveBtnRef).removeClass('ui-disabled');
                          $(saveBtnRef).data('buttonComp').enable();
               }
            });
            
            $("#pg").bind('keyup', function(e){
                if (!$(tagInputBoxRef).is(":focus")) 
                    if ($(objThis.stickynotecontainerRef.element).css('display') == "block") {
                        if (e.keyCode == 8) {
                            if ($.trim($(stickynoteTxtAreaRef).text()) != "") {
                                $(saveBtnRef).removeClass('ui-disabled');
                                $(saveBtnRef).data('buttonComp').enable();
                            }
                            
                        }
                    }
            });
            
            // This event is triggerd in the mg.stickynoteoperator.js. elementClicked is the reference to the stickynote icon clicked.
            $($(objComp.element)[0]).bind('elementTriggered', function(e, elementClicked, _tagArr){
                //So far no data has been changed and teh note is not the new element
                objThis.stickyNoteDataChanged = false;
                objThis.newElement = false;
                objThis.stickynoteClicked = (elementClicked);
                objThis.tagListArray = [];
                if(_tagArr)
                {
                	for (var i = 0; i < _tagArr.length; i++) 
                    	objThis.tagListArray.push(_tagArr[i]);
                	objThis.createTag(objComp);
                }
            });
            
            // The click functionality on the save btn
            var saveBtnRole = $(saveBtnRef).data('buttonComp');
            
            $("#stickynoteSaveBtn").bind("mousedown", function(){
            	sticknotetext = $("#stickynoteTextArea").html();
            });
            
            $(saveBtnRole).unbind(saveBtnRole.events.CLICK).bind(saveBtnRole.events.CLICK, function(){
            	if( navigator.userAgent.match(/(ipad)/i) )
            	{
            		$("#stickynoteTextArea").html( sticknotetext );
            	}
            	if($(saveBtnRef).hasClass('ui-disabled')) {
            		return;
            	}
            	if(document.activeElement.id == "stickynoteTextArea" )
                {
                	$('#toRemoveFocus').removeAttr("disabled").focus().blur().attr("disabled", "disabled");
                }
            	
                //whenever the save btn is clicked the stickynotedatachanged will be false as the data changed has been saved.
                try {
                    objThis.stickyNoteDataChanged = false;
                    
                    // If the stickynote is new and is to be saved.
                    var StickyContentTxt = $(stickynoteTxtAreaRef).text();
                    //                  $(".noteselection").removeClass("noteselection").addClass("noteselectionSwitch");
                    
                    if (isInteger(EPubConfig.maxCharactersInNotes) == false) 
                        throw new Error("Value of maxCharactersInNotes is not an integer");
                    
                    if (EPubConfig.maxCharactersInNotes < 1) 
                        throw new Error("Max character limit in notes is set to less than one");
                    
                    if (StickyContentTxt.length > EPubConfig.maxCharactersInNotes) {
                        PopupManager.removePopup();
                        
                        var strErrorMessage = GlobalModel.localizationData["STICKY_NOTE_LIMIT"].replace("$1", EPubConfig.maxCharactersInNotes)
                        
                        $($(objThis.noteLimitPanel).find('[id=noteLimitTxt]')[0]).html(strErrorMessage);
                        $($(objThis.noteLimitPanel).find('[id=noteLimitInfo]')[0]).html(GlobalModel.localizationData["STICKY_NOTE_OK_BUTTON"]);
                        objThis.openNoteLimitPanel(this);
                    }
                    else {
                    	$(deleteBtnRef).removeClass('ui-disabled');
                    	$(deleteBtnRef).data('buttonComp').enable();
                    	$(saveBtnRef).addClass('ui-disabled');
                        $(saveBtnRef).data('buttonComp').disable();
                    	$($(objComp.element).find('[id=tagList]')[0]).html("");
	                    $(tagInputBoxRef).val("");
	                    $(addTagBtnRef).addClass('ui-disabled');
	                    $(addTagBtnRef).data('buttonComp').disable();
                        if (! ($.browser.msie && $.browser.version == 10) && !isWinRT && $.browser.version != 11 ) {
                          $(stickynoteTxtAreaRef).html($(stickynoteTxtAreaRef)[0].innerText);
						  objThis.modifyContent($(stickynoteTxtAreaRef)[0])
						}
                        var StickyContent = $(stickynoteTxtAreaRef).html();
                        
                        if (objThis.newElement == true) {
                        
                             objThis.saveNewStickyNote(StickyContent);
                            
                        }
                        else { // if the already existing sticky note is modified
                            objThis.modifySavedStickyNote(StickyContent);
                        }
                        
                        PopupManager.removePopup();
                    }
                } 
                catch (e) {
                    console.error(e.message);
                }
                objThis.scrollviewRef.options.rangedata = "";
            });
            
            //Delete functionality of the note. Delete Button on the note panel
            var deletebtnRole = $(deleteBtnRef).data('buttonComp');
            $(deletebtnRole).unbind(deletebtnRole.events.CLICK).bind(deletebtnRole.events.CLICK, function(){
            	if($(deleteBtnRef).hasClass('ui-disabled')) {
            		return;
            	}
                isDeleteNote = true;
                if(document.activeElement.id == "stickynoteTextArea" )
                {
                	$('#toRemoveFocus').removeAttr("disabled").focus().blur().attr("disabled", "disabled");
                }
                
                PopupManager.removePopup();
                $($(objComp.element).find('[id=tagList]')[0]).html("")
                $(tagInputBoxRef).val("");
                $(addTagBtnRef).addClass('ui-disabled');
                $(addTagBtnRef).data('buttonComp').disable();
                if(EPubConfig.My_Notebook_isAvailable)
                	$($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationTxt]')[0]).html(GlobalModel.localizationData["STICKY_NOTE_DELETE_CONFIRMATION_TEXT_NOTEBOOK"]);
                else
                	$($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationTxt]')[0]).html(GlobalModel.localizationData["STICKY_NOTE_DELETE_CONFIRMATION_TEXT"]);
                	
                $($(objThis.deleteConfirmationPanel).find('[id=dltConfirmationSuccess]')[0]).html(GlobalModel.localizationData["STICKY_NOTE_DELETE_CONFIRMATION_YES_BUTTON"]);
                objThis.openDeleteConfirmationPanel(this);
                
            });
            
            /* This event is fired in mg.stickynoteoperator.js.*/
            $(objThis.stickynotecontainerRef).unbind('deleteConfirmation').bind('deleteConfirmation', function(e){
                PopupManager.removePopup();
                // If its a new element, this will remove the current stickynote from the screen
                var sticyNoteDeletedId;
                if (objThis.newElement == true || isDeleteNote == true) {
                    if (objThis.newElement == true) {
                        objThis.getCurrentstickynote = objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'getNewStickyNoteCreated');
                        var currentStickyNote = objThis.getCurrentstickynote.element[0];
                        $(".noteselection_" + GlobalModel.totalStickyNotes).removeClass("noteselection");
                        $(".noteselection_" + GlobalModel.totalStickyNotes).removeClass("noteselection_" + GlobalModel.totalStickyNotes);
                        if (ismobile) 
                        	objThis.scrollviewRef.options.rangedata = "";
                        else
                        	$(objThis.scrollviewRef).trigger("select_text_end"); // this event is handled in annotationbaroperator.
                    	$(currentStickyNote).remove();
                    }
                    else 
                        if (isDeleteNote == true) {
                            if (objThis.noteToDeleteThroughAnnotationPanel == undefined || objThis.noteToDeleteThroughAnnotationPanel == null || objThis.noteToDeleteThroughAnnotationPanel == "") {
                                currentStickyNote = objThis.stickynoteClicked.element;
                            }
                            else {
                                currentStickyNote = objThis.noteToDeleteThroughAnnotationPanel;
                                objThis.noteToDeleteThroughAnnotationPanel = null;
                            }
                            try {
                            	if (window.getSelection) {
                                	if (window.getSelection().empty) { // Chrome
                                    	window.getSelection().empty();
                                	}
                                	else { 
                                    	if (window.getSelection().removeAllRanges) { // Firefox
                                    	    window.getSelection().removeAllRanges();
                                    	}
                                    }
                            	}
                            	else { 
                                	if (document.selection) { // IE?
                                    	document.selection.empty();
                                	}
                                }
                            } catch(e) {}
                                
                            var noteHighlightID; 
	                       if(currentStickyNote != undefined)
	                       {
	                       		noteHighlightID = $(currentStickyNote).attr("id").split("_")[1];
								$(".noteselection_" + noteHighlightID).removeClass("noteselection");
		                        $(".noteselection_" + noteHighlightID).unbind('click');
		                        var len = $(".noteselection_" + noteHighlightID).length;
		                        if(len)
		                        	$($(".noteselection_" + noteHighlightID)[0].childNodes).unwrap();
		                        $(".noteselection_" + noteHighlightID).removeClass("noteselection_" + noteHighlightID);
		                        $(currentStickyNote).remove();
		                    }
	                       	else
	                       		noteHighlightID = objThis.noteToDeleteId;
	                       		
							var _len = GlobalModel.annotations.length;
							for (var counter = 0; counter < _len; counter++)
							{
								if ((GlobalModel.annotations[counter].type == "stickynote"))
								{
									if (GlobalModel.annotations[counter].id == parseInt(noteHighlightID))
									{
										if( ( GlobalModel.annotations[counter].annotationID != undefined ) && ( GlobalModel.annotations[counter].annotationID != 0 ))
										{
											ServiceManager.deleteAnnotation(GlobalModel.annotations[counter].annotationID, 3);
										}									
									/*if(GlobalModel.annotations[counter].range)
									{
									ServiceManager.deleteAnnotation(GlobalModel.annotations[counter].range, 3);
									}
									else
									{
									ServiceManager.deleteAnnotation(GlobalModel.annotations[counter].annotationID, 3);
									}*/
									
										if(currentStickyNote != undefined)
											sticyNoteDeletedId = objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'deleteStickyNote', currentStickyNote);
										else
											GlobalModel.annotations.splice(counter,1);
									
										break;
									}
								}
							}
                            
                        }
                }
                
            });
            
            break;
        /* This is the limit characters in note. Whenever the stickynote data is 
         * changed and more than 1000 characters added, this popup will appear*/
        case AppConst.NOTE_LIMIT_PANEL:
            
            objThis.noteLimitPanel = $(objComp.element);
            var noteLimitInfoRef = ($(objComp.element).find('[id=noteLimitInfo]')[0]);
            $(noteLimitInfoRef).unbind('click').bind('click', function(){
                PopupManager.removePopup();
                var currentStickyNote;
                currentStickyNote = objThis.stickynoteClicked;
                objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'openPopupPanel', currentStickyNote, objThis.stickynotecontainerRef);
            });
            
            break;
            
        /* This is the delete confirmation panel. Whenever the stickynote data is 
         * changed and user closes the panel withoout saving, this popup will appear*/
        case AppConst.DLT_CONFIRM_PANEL:
            objThis.deleteConfirmationPanel = $(objComp.element);
            var dltConfirmSuccessRef = ($(objComp.element).find('[id=dltConfirmationSuccess]')[0]);
            var dltConfirmFailureRef = ($(objComp.element).find('[id=dltConfirmationFailure]')[0]);
            
            $(dltConfirmSuccessRef).unbind('click').bind('click', function(){
                $(objThis.stickynotecontainerRef).trigger('deleteConfirmation'); // handled in stickynote operator
                $($(objThis.stickynotecontainerRef.element).find('[id=tagList]')[0]).html("");

                objThis.temptagListArray = [];
                $($(objThis.stickynotecontainerRef.element).find('[class=tagBorder]')[0]).css("display","none");
                $($("#bookContentContainer").data('scrollviewcomp')).trigger('closeOpenedPanel'); 
                objThis.scrollviewRef.options.rangedata = "";
//              $(".noteselection").removeClass("noteselection").addClass("noteselectionSwitch");

            });
            
            // If user cancels the stickynote deletion confirmation, the stickynote panel will reopen.
            $(dltConfirmFailureRef).unbind('click').bind('click', function(){
                PopupManager.removePopup();
                var currentStickyNote;
                if (objThis.noteToDeleteThroughAnnotationPanel == undefined || objThis.noteToDeleteThroughAnnotationPanel == null || objThis.noteToDeleteThroughAnnotationPanel == "") {
                    if (objThis.newElement == true) {
                        objThis.getCurrentstickynote = objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'getNewStickyNoteCreated');
                        currentStickyNote = objThis.getCurrentstickynote.element[0];
                        var objLauncher = $(currentStickyNote).data('stickyNoteComp');
                        setTimeout(function(){
                            objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'openPopupPanel', objLauncher, objThis.stickynotecontainerRef);
                        }, 0);
                    //objThis.openPanel(objLauncher);
                    }
                    else {
                        currentStickyNote = objThis.stickynoteClicked;
                        objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'openPopupPanel', currentStickyNote, objThis.stickynotecontainerRef);
                    //objThis.openPanel(currentStickyNote)
                    }
                //objThis.noteToDeleteThroughAnnotationPanel = null;
                
                }
                else {
                    objThis.noteToDeleteThroughAnnotationPanel = "";
                }
            });
            
            break;
            
        case AppConst.SCROLL_VIEW_CONTAINER:
            objThis.scrollviewRef = objComp;
            $("#bookContentContainer").bind('stickyNoteIconClicked', function(e, objClass){
                $($("#bookContentContainer").data('scrollviewcomp')).trigger('closeOpenedPanel'); // handled in panelviewoperator. To close the panel opened on stickynote click
                isDeleteNote = true; // The stickynote clicked is deletable.
                objThis.stickynoteRef = $(objClass.element)[0];
                //stickynotetxt will contain the value that will be displayed in the textarea of the stickynote panel.
                var stickynoteTxt = $(objClass.element).attr('value');
                setTimeout(function(){
                    objThis.savetoNote = objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'onStickyNoteHotSpotClick', stickynoteTxt, objClass);
                }, 0);
            });
            $(objComp).bind("alertokclick", function(){
                if(objThis.tagError)
                {
                    objThis.tagError = 0;
                    var currentStickyNote;
            		if (objThis.newElement == true) {
                        objThis.getCurrentstickynote = objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'getNewStickyNoteCreated');
                        currentStickyNote = objThis.getCurrentstickynote.element[0];
                        var objLauncher = $(currentStickyNote).data('stickyNoteComp');
                        setTimeout(function(){
                            objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'openPopupPanel', objLauncher, objThis.stickynotecontainerRef);
                        }, 0);
                    }
                    else {
                        currentStickyNote = objThis.stickynoteClicked;
                        objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'openPopupPanel', currentStickyNote, objThis.stickynotecontainerRef);
                    }
                }
            });
            
            $(objComp).bind(objComp.events.PAGE_RENDER_COMPLETE, function(){
				objThis.scrollViewRenderCompleteHandler(objComp);
            });
            break;
            
        case AppConst.BOOKMARK_DLT_CONFIRM_PANEL:
            
            break;
            
            
        // This contains the functionality of the elements of StickyNote Annotaion Panel (Left hand side panel)
        case AppConst.STICKYNOTE_SECTION_PANEL:
            
            $(objComp.element).bind('editStickynoteData', function(e, currentListId){
                objThis.editStickyNoteData(currentListId);
            });
            
            $(objComp.element).bind('deleteStickynoteData', function(e, currentListId){
                objThis.newElement = false;
                objThis.noteToDeleteId = currentListId;
                objThis.noteToDeleteThroughAnnotationPanel = $("#btnStickyNote_" + currentListId)[0];
                $("#stickynoteDeleteBtn").trigger('click');
            });
            
            $($(objComp.element).find('[id="annotationMyNotesTab"]')).unbind('click').bind('click', function () {
                console.log("annotationMyNotesTab click");
                //annotationAllTab
                $(objComp.element).find('[id=annotationAllTab]').addClass('leftPanelTab').removeClass('leftPanelTabActive');
                $(objComp.element).find('[id=annotationMyNotesTab]').addClass('leftPanelTabActive').removeClass('leftPanelTab');
                $(objComp.element).find('[id=annotationMyQuestionsTab]').addClass('leftPanelTab').removeClass('leftPanelTabActive');
                $(objComp.element).find('[id=annotationTeacherNotesTab]').addClass('leftPanelTab').removeClass('leftPanelTabActive');
                $(objComp.element).find('[id=addedStickyNotesList]').css('display','block');
                $(objComp.element).find('[id=addedQuestionsList]').css('display', 'none');

          		$($(objComp.element).find('[id="blankQuestionAnswerPanelText"]')).css("display","none");
          		if($.trim($(objComp.element).find('[id=addedStickyNotes]').html()) == "")
        		{
        			$($(objComp.element).find('[id="blankNotesPanelText"]')).css("display","block");
        			$(objComp.element).find('[id=annotationSortBy]').css('display','none');
          			$(objComp.element).find('[id=externalURLLink]').css('display','none');
        		}
        		else
        		{
        			$($(objComp.element).find('[id="blankNotesPanelText"]')).css("display","none");
          			$(objComp.element).find('[id=externalURLLink]').css('display','block');
        		}
        		objThis.doFunctionCall(AppConst.STICKYNOTE_SECTION_PANEL, 'updateViewSize');
            });
            //annotationMyNotesTab
            
            break;
            
            
        // The notes button on the markup tool
        case AppConst.NOTES_ANNOTATION_BTN:
            
            // When the notes btn is clicked
            if(ismobile) {
            	var eventType = 'vclick';
            } else {
            	var eventType = 'click';
            }

            var clickHandler = function(e){
            	if(GlobalModel.hasDPError("notes"))
            		return;
            	PageNavigationManager.removeSwipeListner(objThis);
            	if (ismobile)
                {
                	document.onselectionchange = null;
                	$("#annotation-bar").css("display","none");
	            	$("body").append('<input type="text" id="inputToRemove" style="width:0;height:0"/>')
		    		$("#inputToRemove").focus();
		    		$("#inputToRemove").blur();
		    		$("#inputToRemove").remove();	
                }
                var objInfo = objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'getInfoForSkickyNotes');
                var strID = objInfo.ancestoralParentObjectID;
                var iPageNum = objThis.scrollviewRef.options.pageIndexForAnnotation;
                
                var arrStickyNotesOnPage = $("#innerScrollContainer").find('[associated-page-number="' + iPageNum + '"]');
                
                /*var objStickyNotesContainer = $("#innerScrollContainer").find('[note-objectid=' + strID + ']')[0];
                
                //if no sticky note with given if exists, create it, else open it;
                if (objStickyNotesContainer == null)*/ 
                {
                    if ($($(objThis.stickynotecontainerRef.element).find('[class=stickyNoteTags]')[0]).css("display") == "block") {
                        var updatedHeight = $(objThis.stickynotecontainerRef.element).height() - objThis.tagAddHeight;
                        var addTagBtnRef = $(objThis.stickynotecontainerRef.element).find('[type=tagAddBtn]')[0];
                        var tagInputBoxRef = $(objThis.stickynotecontainerRef.element).find('[id=tagInputBox]')[0];
                        $(objThis.stickynotecontainerRef.element).css("height", updatedHeight);
                        $($(objThis.stickynotecontainerRef.element).find('[class=stickyNoteTags]')[0]).css("display", "none");
                        PopupManager.positionPopup(true);
                        $($(objThis.stickynotecontainerRef.element).find('[id=tagList]')[0]).html("");
                        $(tagInputBoxRef).val("");
                        $(addTagBtnRef).addClass('ui-disabled');
                        $(addTagBtnRef).data('buttonComp').disable();
                    }
                    
                    if (isInteger(EPubConfig.Notes_Per_Page) == false) 
                        throw new Error("Value of Notes_Per_Page is not an integer");
                    
                    if (isInteger(EPubConfig.Tags_Per_Note) == false) 
                        throw new Error("Value of Tags_Per_Note is not an integer");
                    //add a note only if the number of notes that can be added on a page have not exceeded.
                    if (arrStickyNotesOnPage.length < EPubConfig.Notes_Per_Page || EPubConfig.Notes_Per_Page < 1) {
                        var offset = $(e.currentTarget).offset();
                        //Adding note highlight
                        
                        
                        
                        /* This will define the position of the notes icon that will appear on the screen
                     *  Written in annotationPanelComp.js
                     */
                        objThis.newElement = true;
                        
                        objThis.savetoNote = 0;
                        var savetoNoteBookButtonRef = $(objThis.stickynotecontainerRef.element).find('[type=savetoNoteBookButton]')[0];
                        
                        objThis.savetoNote = objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'switchSavetoNote', objThis.savetoNote, savetoNoteBookButtonRef);
                        var arrPos = new Object();
                        arrPos.top = objThis.scrollviewRef.options.Icontop;
                        if((arrPos.top == preTopPos) && ismobile)
                        {
                      	   return;
                        }
                        if(arrPos.top < 0) {
	                    	arrPos.top = e.pageY;	
	                    }
                        arrPos.left = objThis.scrollviewRef.options.Iconleft;
                        arrPos.contentWidth = objThis.scrollviewRef.options.contentWidth;
                        objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'setElementPosition', offset, arrPos);
                        
                        /* This will place stickynote icon on screen and will open stickynote container panel.
                     * written in annotationPanelComp.js
                     */
                        setTimeout(function(){
 							//Adding note highlight
	                        rangy.init();
	                        var cssClassApplier = rangy.createCssClassApplier("noteselection");
	                        var cssIDClassApplier = rangy.createCssClassApplier("noteselection_" + GlobalModel.totalStickyNotes);
	                        
	                        if (objThis.scrollviewRef.options.rangedata) {
	                            window.getSelection().removeAllRanges();
	                            window.getSelection().addRange(objThis.scrollviewRef.options.rangedata);
	                        }
	                        
	                        cssIDClassApplier.toggleSelection();
	                        cssClassApplier.toggleSelection();                          
                            objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'onNotesAnnotationBtnClick', GlobalModel.totalStickyNotes, objThis.stickynotecontainerRef);
                            var currentStickyNote = $("#btnStickyNote");
							objThis.stickynoteClicked = $(currentStickyNote).data('stickyNoteComp');
                        }, 0);
                        // Reset the values of stickynote container elements
                        $($(objThis.stickynotecontainerRef.element).find('[type=stickynoteTextArea]')[0]).html('');
                        $($(objThis.stickynotecontainerRef.element).find('[type=saveBtn]')[0]).addClass('ui-disabled');
                        $($(objThis.stickynotecontainerRef.element).find('[type=saveBtn]')[0]).data('buttonComp').disable();
                        $($(objThis.stickynotecontainerRef.element).find('[type=savetoNoteBookButton]')[0]).addClass('ui-disabled');
                        $($(objThis.stickynotecontainerRef.element).find('[type=savetoNoteBookButton]')[0]).data('buttonComp').disable();
                        $($(objThis.stickynotecontainerRef.element).find('[type=deleteBtn]')[0]).addClass('ui-disabled');
                        $($(objThis.stickynotecontainerRef.element).find('[type=deleteBtn]')[0]).data('buttonComp').disable();
                    }
                    else {
                        //For opening the readers alert box
                        $("#alertTxt").html(GlobalModel.localizationData["MAX_NOTE_ANNOTATION_ALLOWED_PER_PAGE"]);
                        setTimeout(function(){
                            PopupManager.addPopup($("#alertPopUpPanel"), null, {
                                isModal: true,
                                isCentered: true,
                                popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
                                isTapLayoutDisabled: true
                            });
                        }, 0);
                    }
                }
                /*else {
                    //open the already created note;
                    if ((parseInt($(objStickyNotesContainer).css('top')) * GlobalModel.currentScalePercent) < $("#bookContentContainer2").scrollTop()) {
                        var scrollTopBy = parseInt($(objStickyNotesContainer).css('top')) * GlobalModel.currentScalePercent;
                        $("#bookContentContainer2").scrollTop(scrollTopBy)
                    //$("#bookContentContainer2").scrollTop(parseInt($(objStickyNotesContainer).css('left')))
                    }
                    setTimeout(function(){
                        $(objStickyNotesContainer).trigger("click")
                    }, 200);
                }*/
            };
            
            if( isWinRT )
            {
				var eventListener = function(evt)
				{
				    clickHandler( evt );
				}
			    var target = document.getElementById("notes-anno");
			    target.addEventListener(WinRTPointerEventType, eventListener);
            }
            else
            {
            	$(objComp.element).bind(eventType, clickHandler);
            }
            break;
            
        default:
            break;
            
    }
    
    return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}


/**
 * This fn will open the deleteconfirmation panel
 */
MG.StickyNoteOperator.prototype.openDeleteConfirmationPanel = function(launcher){
    var objThis = this;
    PopupManager.addPopup($(objThis.deleteConfirmationPanel), launcher, {
        isModal: true,
        isTapLayoutDisabled: true,
        popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
        isCentered: true
    
    });
}

MG.StickyNoteOperator.prototype.openNoteLimitPanel = function(launcher){
    var objThis = this;
    
    PopupManager.addPopup($(objThis.noteLimitPanel), launcher, {
        isModal: true,
        isTapLayoutDisabled: true,
        popupOverlayStyle: "popupMgrMaskStyleSemiTransparent",
        isCentered: true
    
    });
}

/**
 * This function will modify text part from content. It will convert url written by user into clickable links
 */
MG.StickyNoteOperator.prototype.modifyContent = function(el){

    var arrContentEditableDivChildren = []
    for (var startIndex = 0, endIndex = el.childNodes.length; startIndex < endIndex; startIndex++) {
        var item = el.childNodes[startIndex];
        
        //if node is of type text
        if (item.nodeType == 3) {
            var strModifiedText = this.modifyContentforRegExp(item.data)
            arrContentEditableDivChildren.push(strModifiedText)
        }
        else 
            if ($(item).html() != "") {
                var elTemp = this.modifyContent($(item)[0])
                
                //ensuring all links (including copied from other websites) open in new tab
                if(item.nodeName == "A")
                {
                	elTemp[0].target = "_blank";
                }
                
                arrContentEditableDivChildren.push(elTemp)
            }
            else {
                arrContentEditableDivChildren.push(item)
            }
    }
    
    $(el).html("")
    for (var i = 0; i < arrContentEditableDivChildren.length; i++) {
        $(el).append(arrContentEditableDivChildren[i]);
    }
    
    return $(el);
}

/**
 * This function will modify content to find regular Expression
 */
MG.StickyNoteOperator.prototype.modifyContentforRegExp = function(StickyContent){
    StickyContent = StickyContent.replace(/<script(.*?)>/gim, "");
    StickyContent = StickyContent.replace(/<\/script(.*?)>/gim, "");
    StickyContent = StickyContent.replace(/\&nbsp\;/gim, " ");
    StickyContent = StickyContent.replace(/<a([^<>]+)>/gim, "");
    StickyContent = StickyContent.replace(/<\/a>/gim, "");
    StickyContent = StickyContent.replace(/([^\/\'])(http\:\/\/[^ \,<>\']+.[^ \,\.<>]+.\.[^ \,<>]+)/gim, "$1<a href='$2' target='_blank' style='cursor:pointer'>$2</a>");
    StickyContent = StickyContent.replace(/([^\/\'])(https\:\/\/[^ \,<>\']+.[^ \,\.<>]+.\.[^ \,<>]+)/gim, "$1<a href='$2' target='_blank' style='cursor:pointer'>$2</a>");
    StickyContent = StickyContent.replace(/([^\/])(www\.[^ \,<>\.]+.\.[^ \,<>]+)/gim, "$1<a href='http:\/\/$2' target='_blank' style='cursor:pointer'>$2</a>");
    StickyContent = StickyContent.replace(/^(www\.[^ \,<>\.]+.\.[^ \,<>]+)/im, "<a href='http:\/\/$1' target='_blank' style='cursor:pointer'>$1</a>");
    StickyContent = StickyContent.replace(/^(http\:\/\/[^ \,\'<>]+.[^ \,<>\.]+.\.[^ \,<>]+)/im, "<a href='$1' target='_blank' style='cursor:pointer'>$1</a>");
    StickyContent = StickyContent.replace(/^(https\:\/\/[^ \,\'<>]+.[^ \,<>\.]+.\.[^ \,<>]+)/im, "<a href='$1' target='_blank' style='cursor:pointer'>$1</a>");
    
    return StickyContent;
}

/**
 * This function is called wto edit the stickynote
 */
MG.StickyNoteOperator.prototype.editStickyNoteData = function(currentListId){
    var objThis = this;
    var pageIndex;
    var range;
    var stickynoteTxt;
    var savetonote;
    objThis.currentStickyEdit = currentListId;
    objThis.newElement = false;
    
    for (var i = 0; i < GlobalModel.annotations.length; i++) {
        if (GlobalModel.annotations[i]) {
            if (GlobalModel.annotations[i].type == "stickynote") {
                if (GlobalModel.annotations[i].id == parseInt(currentListId)) {
                    pageIndex = GlobalModel.annotations[i].pageNumber;
                    range = GlobalModel.annotations[i].range;
                    stickynoteTxt = GlobalModel.annotations[i].title;
                    savetonote = GlobalModel.annotations[i].savetonote;
                    objThis.tagListArray = [];
                    for (var j = 0; j < GlobalModel.annotations[i].tagsLists.length; j++) 
                        objThis.tagListArray.push(GlobalModel.annotations[i].tagsLists[j]);
                	break;
                }
            }
        }
    }
    
    $($("#bookContentContainer").data('scrollviewcomp')).trigger('closeOpenedPanel');
    var scrollToPos = parseInt($("#btnStickyNote_" + currentListId).css('top'));
    
    objThis.savetoNote = savetonote;
    var currentStickyNoteToEdit = $("#btnStickyNote_" + currentListId);
    var objLauncher = $(currentStickyNoteToEdit).data('stickyNoteComp');
    objThis.stickynoteClicked = objLauncher;
    //var stickynoteTxt = $(currentStickyNoteToEdit)[0].value;
    $("#stickynoteTextArea").value = stickynoteTxt;
    $($("#stickynoteTextArea")[0]).html(stickynoteTxt);
    $(objThis.stickynotecontainerRef.element).find('[id=stickynoteDeleteBtn]').removeClass('ui-disabled');
    $(objThis.stickynotecontainerRef.element).find('[id=stickynoteDeleteBtn]').data('buttonComp').enable();
    $(objThis.stickynotecontainerRef.element).find('[type=savetoNoteBookButton]').removeClass('ui-disabled');
    $(objThis.stickynotecontainerRef.element).find('[type=savetoNoteBookButton]').data('buttonComp').enable();
    $(objThis.stickynotecontainerRef.element).find('[id=stickynoteSaveBtn]').addClass('ui-disabled');
    $(objThis.stickynotecontainerRef.element).find('[id=stickynoteSaveBtn]').data('buttonComp').disable();
    $(objThis.stickynotecontainerRef.element).find('[type=savetoNoteBookButton]').addClass('ui-enabled');
    $(objThis.stickynotecontainerRef.element).find('[type=savetoNoteBookButton]').data('buttonComp').disable();
    
    
    $("#stickynoteTextArea").attr('contenteditable', 'true');
    $("#stickynoteTextArea").find('a').css('cursor', 'auto');
    $("#stickynoteTextArea").find('a').css('text-decoration', 'none');
    var textareaRef = $("#stickynoteTextArea")[0];
    var pageNumValue = GlobalModel.pageBrkValueArr.indexOf(String(pageIndex));
    var scrollviewCompRefnce = $("#bookContentContainer").data('scrollviewcomp');
    var curPageIndex = scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf(String(pageIndex));
    objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'setCaretPositionVar', true);
	
	var isNoteCreated = false;
    if(EPubConfig.pageView == 'doublePage')
    	isNoteCreated = (curPageIndex == GlobalModel.currentPageIndex || curPageIndex == GlobalModel.currentPageIndex+1)?true:false;
    else
    	isNoteCreated = (curPageIndex == GlobalModel.currentPageIndex)?true:false;
    	
    if(isNoteCreated)
	{
		if(EPubConfig.My_Notebook_isAvailable)
			objThis.createTag(objThis.stickynotecontainerRef);
	
		var currentStickyNoteToEdit = $("#btnStickyNote_" + objThis.currentStickyEdit);
		var objLauncher = $(currentStickyNoteToEdit).data('stickyNoteComp');
		objThis.stickynoteClicked = objLauncher;
        var nextBannerWidth = 0;
        if ($("#nextPgBanner").css('display') == "block") {
            nextBannerWidth = $("#nextPgBanner").width();
        }
        
        // statement to check if left distance is greater than the window width - nextnavigationbar width (as icon can hiede under the navigation bar).
        if ($(currentStickyNoteToEdit).offset().left > (window.width - nextBannerWidth) || ($(currentStickyNoteToEdit).offset().left < $("#scrollMainContainer").offset().left)) {
            objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'resetZoom');
        }
	    objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'openPopupPanel', objLauncher, objThis.stickynotecontainerRef);
	    
		objThis.currentStickyEdit = -1;
	}
	else
    	objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', pageNumValue);
    
    if (savetonote == 0) {
	    $(objThis.stickynotecontainerRef.element).find('[type=savetoNoteBookButton]').addClass('savetoNoteBookBtnChk').removeClass('savetoNoteBookBtn');
	}
	else {
	    $(objThis.stickynotecontainerRef.element).find('[type=savetoNoteBookButton]').removeClass('savetoNoteBookBtnChk').addClass('savetoNoteBookBtn');
	}
    
}

/**
 * This function will save the newly created stickynote
 * @param {Object} StickyContent: the content of the stickynote
 * @param {Object} totalStickynotes: the total number of stickynotes. This value will be incremented by one in this function.
 */
MG.StickyNoteOperator.prototype.saveNewStickyNote = function(StickyContent){
    var objThis = this;
    objThis.newElement = false;
    objThis.getCurrentstickynote = objThis.doFunctionCall(AppConst.STICKYNOTE_CONTAINER, 'getNewStickyNoteCreated');
    var currentStickyNote = objThis.getCurrentstickynote.element[0];
    currentStickyNote.value = StickyContent;
    // This function is written in stickynotecomp.js
    var arrObj = objThis.doFunctionCall(AppConst.STICKY_NOTE_COMP, 'saveNewStickyNoteData', StickyContent, GlobalModel.totalStickyNotes);
    (GlobalModel.stickyNoteArrayTopPosition).push(CompModel.yPos);
    if (EPubConfig.pageView == 'doublePage') {
        //objThis.scrollviewRef.options.contentWidth
        var totalSinglePageWidth = (objThis.scrollviewRef.options.contentWidth + $("#scrollMainContainer").offset().left) * GlobalModel.currentScalePercent;
        var stickynotePageIndex = objThis.scrollviewRef.options.pageIndexForAnnotation;
        
        arrObj.pageNumber = (stickynotePageIndex);
    }
    else 
        if (EPubConfig.pageView == 'singlePage') {
            var totalSinglePageWidth = (objThis.scrollviewRef.options.contentWidth + $("#scrollMainContainer").offset().left) * GlobalModel.currentScalePercent;
            var stickynotePageIndex = objThis.scrollviewRef.options.pageIndexForAnnotation;
            
            arrObj.pageNumber = (stickynotePageIndex);
            
        //objThis.scrollviewRef.options.contentWidth
        }
        else {
            //var stickynotePageIndex = parseInt(CompModel.yPos / ($('[type=PageContainer]').height() + parseInt($('[type=PageContainer]').css('margin-bottom')))) + 1;
            var stickynotePageIndex = objThis.scrollviewRef.options.pageIndexForAnnotation;
            arrObj.pageNumber = stickynotePageIndex;
            
        }
    arrObj.tagsLists = [];
    for (var i = 0; i < objThis.temptagListArray.length; i++) 
        arrObj.tagsLists.push(objThis.temptagListArray[i]);
    
    objThis.tagListArray = [];
    objThis.temptagListArray = [];
    arrObj.savetonote = objThis.savetoNote;
    arrObj.range = objThis.scrollviewRef.options.rangeObject;
    
    
    var objInfo = objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'getInfoForSkickyNotes');
    var strID = objInfo.ancestoralParentObjectID;
    var iPageNum = objThis.scrollviewRef.options.pageIndexForAnnotation;
    //$(currentStickyNote).attr('id', 'btnStickyNote_' + (GlobalModel.annotations.length - 1));
    $(currentStickyNote).attr('id', 'btnStickyNote_' + GlobalModel.totalStickyNotes);
    $(currentStickyNote).attr('note-objectid', strID);
    $(currentStickyNote).attr('associated-page-number', iPageNum);
    
    var stickyIcon = $(currentStickyNote)[0].outerHTML;
    
    var icontop = $(stickyIcon)[0].style.top;
    var iconleft = $(stickyIcon)[0].style.left;
    icontop = parseInt(icontop.replace('px', ''));
    iconleft = parseInt(iconleft.replace('px', ''));
    arrObj.icontop = icontop;
    arrObj.iconleft = iconleft;
    var pagenum = arrObj.pageNumber;
    var contentid = GlobalModel.totalStickyNotes.toString();
    
    var objData = {
		id: contentid,
		icontop: icontop,
		iconleft: iconleft,
		title: "",
		pagenum: pagenum,
		savetoNote:objThis.savetoNote,
		stickyContent: StickyContent,
		rangeObject: arrObj.range,
		tagsLists: []
	};
	GlobalModel.annotations[GlobalModel.annotations.length] = (arrObj);
    if (EPubConfig.My_Notebook_isAvailable) {
                    ServiceManager.createAnnotationAndTags(objData, 3);
          } else {
                    ServiceManager.createAnnotation(objData, 3, function(data) {
                              for (var i = 0; i < GlobalModel.annotations.length; i++) {
                                        if ((contentid == GlobalModel.annotations[i].id)&&(GlobalModel.annotations[i].type == "stickynote")) {
                                                  GlobalModel.annotations[i].annotationID = data.annotation_id;
                                                  break;
                                        }
                              }
                    });
          }
    
    $(".noteselection_" + contentid).unbind('click').bind('click', function(e){
    	if(GlobalModel.hasDPError("notes"))
    		return;
    	if( ismobile ){ $("#stickynoteTextArea").css("display", "none"); };
    	 e.stopPropagation();
        objThis.editStickyNoteData(contentid);
    });
    
    GlobalModel.totalStickyNotes++;
}

/**
 * This function is used to modify saved stickynote
 * @param {Object} StickyContent: the content of the stickynote
 * @param {Object} totalStickynotes: the total number of stickynotes.
 */
MG.StickyNoteOperator.prototype.modifySavedStickyNote = function(StickyContent){
    var objThis = this;
    $(objThis.stickynoteClicked.element)[0].value = StickyContent;
    var arrObj = new Array();
    arrObj.title = StickyContent;
    arrObj.id = $(objThis.stickynoteClicked.element).attr('id').replace('btnStickyNote_', '');
    arrObj.totalStickyNotes = GlobalModel.totalStickyNotes;
    arrObj.savetonote = objThis.savetoNote;
    arrObj.tagsLists = [];
    for (var i = 0; i < objThis.temptagListArray.length; i++) 
        arrObj.tagsLists.push(objThis.temptagListArray[i]);
    objThis.tagListArray = [];
    objThis.temptagListArray = [];
    arrObj.range = objThis.scrollviewRef.options.rangeObject;
    
						for (var counter = 0; counter < GlobalModel.annotations.length; counter++)
						{
							if ((GlobalModel.annotations[counter].type == "stickynote"))
							{
								if (GlobalModel.annotations[counter].id == parseInt(arrObj.id))
								{
								var rangeObject = GlobalModel.annotations[counter].range;
								var annotationID = GlobalModel.annotations[counter].annotationID;
								var objectID = GlobalModel.annotations[counter].pageNumber;
								}
							}
						}

    var modifyObj = objThis.doFunctionCall(AppConst.STICKY_NOTE_COMP, 'modifyStickyNoteData', arrObj, objThis.savetoNote);
    
    var stickyIcon = $(objThis.stickynoteClicked.element)[0].outerHTML;
    var icontop = $(stickyIcon)[0].style.top;
    var iconleft = $(stickyIcon)[0].style.left;
    icontop = parseInt(icontop.replace('px', ''));
    iconleft = parseInt(iconleft.replace('px', ''));
    
    var pagenum = (modifyObj.pageNumber);
    
    //saving note and tags on server;
	var objData = {
		contentID: arrObj.id, 
        pagenum : pagenum,
		savetoNote: objThis.savetoNote,
		stickyContent: StickyContent,
        rangeObject : rangeObject,
		annotationid : annotationID,
		objectID : objectID,
		tagsLists: []
	};

    if (EPubConfig.My_Notebook_isAvailable) {
                    ServiceManager.updateAnnotationAndTags(objData, 3);
          } else {
          		if( ( annotationID != undefined ) &&  ( annotationID != 0 ) )
          		{
                    ServiceManager.Annotations.update(3, objData);
          		}
          }
    
}
/**
 * This function is used to create tag in stickynote
 * @param {Object} objComp
 */
MG.StickyNoteOperator.prototype.createTag = function(objComp){
    var objThis = this;
    var tagDiv = "";
    objThis.temptagListArray = [];
    for (var j = 0; j < objThis.tagListArray.length; j++) {
        objThis.autoCompleteTagsList.push(objThis.tagListArray[j]);
        tagDiv += "<div class='tagOuter'><div class='tag'>" + objThis.tagListArray[j] + "<div id='tag' data-role='simpleButtonComp' class='tagClsBtn' type='closeButton'></div></div><span class='tagTweek'></span></div>";
        objThis.temptagListArray.push(objThis.tagListArray[j]);
    }
    $($(objComp.element).find('[id=tagList]')[0]).html(tagDiv);
    objThis.updateTagHeight(objComp);
    $($(objComp.element).find("[id=tag]")).unbind('click').bind('click', function(){
    
        for (var j = 0; j < objThis.temptagListArray.length; j++) {
            if ($(this).parent().parent().find(".tag")[0].innerHTML == (objThis.temptagListArray[j] + '<div id="tag" data-role="simpleButtonComp" class="tagClsBtn" type="closeButton"></div>')) {
                objThis.temptagListArray.splice(j, 1);
            }
        }
        $(objComp.element).find('[id=stickynoteSaveBtn]').removeClass('ui-disabled');
        $(objComp.element).find('[id=stickynoteSaveBtn]').data('buttonComp').enable();
        $(this).parent().parent().remove();
        objThis.updateTagHeight(objComp);
        objThis.stickyNoteDataChanged = true;
    });
}
/**
 * This function is used to update height of tag in stickynote
 * @param {Object} objComp
 */
MG.StickyNoteOperator.prototype.updateTagHeight = function(objComp){
    var objThis = this;
    setTimeout(function(){
        if (Math.ceil($($(objComp.element).find('[id=tagList]')[0]).height()) == 0) {
            if (objThis.savetoNote == 1) 
                var updatedHeight = defaultNoteHeight;
            else 
                var updatedHeight = defaultNoteHeight + objThis.tagAddHeight;
            $(objComp.element).css("height", updatedHeight);
            PopupManager.positionPopup(true);
        }
        else {
            var updatedHeight = defaultNoteHeight + objThis.tagAddHeight + (32 * Math.ceil($($(objComp.element).find('[id=tagList]')[0]).height() / 32));
            $(objComp.element).css("height", updatedHeight);
            PopupManager.positionPopup(true);
        }
    }, 0);
    
    
}

MG.StickyNoteOperator.prototype.scrollViewRenderCompleteHandler = function(objComp) {
	var objThis = this;
	var arrPageComps = $(objComp.element).find('[data-role="pagecomp"]');
	var objPageRole = null;
	var objContShowroom = $("#bookContentContainer").data("scrollviewcomp");
	
	for (var i = 0; i < arrPageComps.length; i++) {
		objPageRole = $(arrPageComps[i]).data("pagecomp");

		$(objPageRole).bind("createHighlightOnPage", function(e) {
			var scrollviewCompRefnce = $("#bookContentContainer").data('scrollviewcomp');
			setTimeout(function()
			{
				for (var counter = 0; counter < GlobalModel.annotations.length; counter++) 
				{
	                if ((GlobalModel.annotations[counter].type == "stickynote")) {
						if(EPubConfig.pageView == 'doublePage')
						{
							if(scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf(GlobalModel.annotations[counter].pageNumber) == GlobalModel.currentPageIndex || scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf(GlobalModel.annotations[counter].pageNumber) == GlobalModel.currentPageIndex+1)
							{
								objThis.createNoteHighlight(GlobalModel.annotations[counter].id, GlobalModel.annotations[counter].range);
							}
						}
						else
						{
							if(scrollviewCompRefnce.member.curPageElementsPgBrk.indexOf(GlobalModel.annotations[counter].pageNumber) == GlobalModel.currentPageIndex)
							{
								objThis.createNoteHighlight(GlobalModel.annotations[counter].id, GlobalModel.annotations[counter].range);
							}
						}
	                }
	            }
				if(objThis.currentStickyEdit != -1)
				{
					if(EPubConfig.My_Notebook_isAvailable)
						objThis.createTag(objThis.stickynotecontainerRef);

					var currentStickyNoteToEdit = $("#btnStickyNote_" + objThis.currentStickyEdit);
   					var objLauncher = $(currentStickyNoteToEdit).data('stickyNoteComp');
					objThis.stickynoteClicked = objLauncher;
			        var nextBannerWidth = 0;
			        if ($("#nextPgBanner").css('display') == "block") {
			            nextBannerWidth = $("#nextPgBanner").width();
			        }
			        
			        // statement to check if left distance is greater than the window width - nextnavigationbar width (as icon can hiede under the navigation bar).
			        if ($(currentStickyNoteToEdit).offset().left > (window.width - nextBannerWidth) || ($(currentStickyNoteToEdit).offset().left < $("#scrollMainContainer").offset().left)) {
			            objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'resetZoom');
			        }
				    objThis.doFunctionCall(AppConst.NOTES_ANNOTATION_BTN, 'openPopupPanel', objLauncher, objThis.stickynotecontainerRef);
					objThis.currentStickyEdit = -1;	
				}
			},800);
			
		});
	}
}

MG.StickyNoteOperator.prototype.restoreSelection = function(containerEl, savedSel)
{
        var charIndex = 0, range = document.createRange();
        range.setStart(containerEl, 0);
        range.collapse(true);
        var nodeStack = [containerEl], node, foundStart = false, stop = false;
//        console.log(nodeStack);
        while (!stop && (node = nodeStack.pop())) {
            if (node.nodeType == 3) {
                var nextCharIndex = charIndex + node.length;

                if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                    range.setStart(node, savedSel.start - charIndex);
                    foundStart = true;

                }
                if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                    range.setEnd(node, savedSel.end - charIndex);

                    stop = true;
                }
                charIndex = nextCharIndex;
            } else {
                var i = node.childNodes.length;
                while (i--) {
                    nodeStack.push(node.childNodes[i]);
                }
            }
        }

        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
		return range;
}

MG.StickyNoteOperator.prototype.createNoteHighlight = function(strNoteSelectionID, objRange)
{

	var objThis = this;
	var rangedata;

  	setTimeout(function()
  	{

	var Obj = $.xml2json(objRange);
	var startC = $('[id="'+Obj.startContainer+'"]').find('#mainContent')[0];
	var startO = parseInt(Obj.startOffset);
	var endO = parseInt(Obj.endOffset);

	var savedSel = {start: startO, end: endO};
	rangedata = objThis.restoreSelection($(startC)[0], savedSel);


	rangy.init();
	
	var cssClassApplier = rangy.createCssClassApplier("noteselection_" + strNoteSelectionID);

	var cssClassAppliercolor = rangy.createCssClassApplier("noteselection");

	window.getSelection().removeAllRanges();
	window.getSelection().addRange(rangedata);

	if($(startC).find(".noteselection_" + strNoteSelectionID)[0] == undefined)
	{
    	cssClassApplier.toggleSelection();
    	cssClassAppliercolor.toggleSelection();
   }
	$(".noteselection_" + strNoteSelectionID).attr("note-id", strNoteSelectionID);

	$(".noteselection_" + strNoteSelectionID).unbind('click').bind('click', function() {
		if(GlobalModel.hasDPError("notes"))
    		return;
		if( ismobile ){ $("#stickynoteTextArea").css("display", "none"); };
		objThis.editStickyNoteData($(this).attr('note-id'));
	});
	 window.getSelection().removeAllRanges();

			                    }, 0);
}

MG.StickyNoteOperator.prototype.getElementsByXPath = function getElementsByXPath(doc, xpath) {
	var nodes = [];
	
	try {
		var result = doc.evaluate(xpath, doc, null, XPathResult.ANY_TYPE, null);

		for (var item = result.iterateNext(); item; item = result.iterateNext()) {
			nodes.push(item);
		}
	} catch (exc) {
		// Invalid xpath expressions make their way here sometimes.  If that happens,
		// we still want to return an empty set without an exception.
	}
	return nodes;
}