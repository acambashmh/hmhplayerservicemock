﻿/**
 * @author: lakshay.ahuja
 */
(function ($, undefined) {
    $.widget("magic.learnosityInteractivity", $.magic.magicwidget, {

        options: {
            userResponse: [],
            interactivityType: "learnosityInteractivity",
            parentgrpId: null,
            xmlContent: null,
            currentResponse: []
        },

        member: {
            questionArray: [],
            questionArrData: null,
            questionInnercontent: null
        },

        _init: function () {
            
        },

        _create: function () {
        },

        populateQAPanelInnerContent: function (questionInnerContent, fileName) {
            var objThis = this;
            try {
                objThis.member.questionInnercontent = questionInnerContent;
                var activityFileExtension = EPubConfig.LearnosityActivityFormat == "json" ? ".json" : ".xml";

                questionInnerContent.element.children().css('display', 'none');
                questionInnerContent.element.find('iframe').remove();
                questionInnerContent.element.append("<iframe class='lnsIFrame' src='" + getPathManager().getLearnosityURL() +
                                                        fileName + activityFileExtension + "'></iframe>");
                questionInnerContent.element.find('iframe').css('height', $("#questionanswerInnerContent").height() + 'px');
                $("#questionanswerInnerContent").css("display", "block");
                $("#questionanswerInnerContent").css("visibility", "visible");
                $("#quesWaitText").hide();
                $("#prevQuestionArrow").removeClass("ui-disabled");
                $("#nextQuestionArrow").removeClass("ui-disabled");
                $('.savetonotebook').css('display', 'none');
                //objThis.setQAPanelInnercontentLayout(strQAContent, questionInnerContent.element);
                //objThis.setSavedUserResponse();
            } catch (e) {

            }
        },

        saveCurrentResponse: function () {
            this.setUserResponse();
        },

        setUserResponse: function () {
            var objThis = this;
            objThis.options.userResponse = [];
            var numberOfQuestions = ($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput').length);
            for (var i = 0; i < numberOfQuestions; i++) {
                var fillUpResponse = $.trim($($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val());
                objThis.options.userResponse.push(fillUpResponse);
            }

            $(objThis.member.questionInnercontent.element).parent().parent().trigger('recentResponseSaved');

        },

        setSavedUserResponse: function () {
            var objThis = this;
            var indexofQues = parseInt($(objThis.element).attr('id').replace('questionPanelques', '')) - 1;
            var questionManagerRef = $("#questionanswerInnerContent").data('questionmanager');
            var questionsGrp = questionManagerRef.getCurrentGroupQuestionsArray();
            var quesid = questionsGrp[indexofQues];

            var objData = GlobalModel.questionPanelAnswers[objThis.options.parentgrpId][quesid];

            if (objData && $(objData.response).length > 0) {
                objThis.options.userResponse = $(objData.response).find("response").html().split(",");

                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    var fillUpResponse = $.trim($($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val($.trim(objThis.options.userResponse[i])));
                }
                var flag = -1;
                for (var i = 0; i < objThis.options.userResponse.length; i++) {
                    if ($.trim(objThis.options.userResponse[i]) != "") {
                        flag = 0;
                        break;
                    }
                }
                var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                if (flag != -1) {
                    //var indexElem = $(objThis.element).parent().children().index($(objThis.element));
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).addClass('quesCompleteCircle').removeClass('quesPendingCircle');
                } else {
                    $($(objThis.member.questionInnercontent.element).parent().parent().find('[type="questionPanelquesDots"]')[indexElem]).removeClass('quesCompleteCircle').addClass('quesPendingCircle');
                }
            }
        },

        saveCurrentResponseToServices: function () {
            
        },

        displayHideCorrectResponse: function (bVal) {
            var objThis = this;
            var xmlContent = objThis.options.xmlContent;
            var correctResponse = [];
            var correctResponseLength = objThis.options.xmlContent.responseDeclaration.correctResponse.value.length;
            if (bVal == 1) {
                for (var i = 0; i < correctResponseLength; i++) {
                    objThis.options.currentResponse[i] = $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val();
                    correctResponse[i] = objThis.options.xmlContent.responseDeclaration.correctResponse.value[i];
                    $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val(correctResponse[i]);
                    $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).attr('disabled', 'disabled');
                }
            } else {
                for (var i = 0; i < correctResponseLength; i++) {
                    $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).val(objThis.options.currentResponse[i]);
                    $($(objThis.member.questionInnercontent.element).find('.qaPanelTableInput')[i]).removeAttr('disabled');
                }
            }
        }
    });
})(jQuery);
