MG.ResourceListOperator = function() {
	_objResourcePanel = null;
	this.superClass = MG.BaseOperator.prototype;
}
// extend super class
MG.ResourceListOperator.prototype = new MG.BaseOperator();
/**
 * This function defines the functionality of components to be executed.
 */
MG.ResourceListOperator.prototype.attachComponent = function(objComp) {
	
	var objThis = this;
	var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
	
	switch(strType){
		case AppConst.RESOURCE_LIST_LAUNCHER:
			$(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e)
			{					 
				 objThis.onResourceButtonClick(e);	
				 		 
			});
			break;
		case AppConst.RESOURCE_PANEL:
			$(objComp).unbind(objComp.events.RESOURCE_ITEM_CLICK).bind(objComp.events.RESOURCE_ITEM_CLICK, function(e, resourceItemRef){
				PopupManager.removePopup($(objComp.element)[0].id)
				objThis.onResourceItemClick(objComp, resourceItemRef);
			})
			break;
		default:
			////console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
			return false;
			break;
	}

	return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}

MG.ResourceListOperator.prototype.onResourceButtonClick = function(e){
	if(e.currentTarget != null && e.currentTarget != undefined)
	{
		_objResourcePanel = e.currentTarget;
	}
    else
	{
		_objResourcePanel = e;
	}
	//this.doFunctionCall(AppConst.RESOURCE_PANEL, 'showHidePanel');
	//var objPanelViewComp = _objResourcePanel.getResourcePanelViewComp();
    var objPanelViewComp = this.doFunctionCall(AppConst.RESOURCE_PANEL, "getResourcePanelViewComp", _objResourcePanel);
	PopupManager.addPopup(objPanelViewComp, _objResourcePanel, {isModal:true, hasPointer:true});
	//PopupManager.positioningPopup(objPanelViewComp);
}

MG.ResourceListOperator.prototype.onResourceItemClick = function(objComp, resourceItemRef){
	switch($(resourceItemRef).attr('linktype'))
	{
		case "media":
			$(objComp).trigger(objComp.events.ON_MEDIA_RESOURCE, resourceItemRef);
			break;
		case "external":
			$(objComp).trigger(objComp.events.ON_WEB_RESOURCE, resourceItemRef);
			break;
	}
}