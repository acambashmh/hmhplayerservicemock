/**
 * @author Anand Kumar
 */

/**
 * Constructor
 */
OperatorManager = {
    //el: objElement,
    //this.arrOperatorsList = [];
	objOperatorsArray: {}
};

OperatorManager.initializeAllOperators = function(objElement){


    $(objElement)[$(objElement).attr('data-role')]();
    var arrWidgetClass = objElement.find($('[data-role]'));
    var i;
    for (i = 0; i < arrWidgetClass.length; i++)
    {
        var strMethod = $(arrWidgetClass[i]).attr("data-role");
        try
        {
            $(arrWidgetClass[i])[strMethod]();
        }
        catch(err)
        {
            //console.error("Error Unable to instantiate widget ", strMethod, " \n  ", err);

        }
    }


    var arrOperators = $(objElement).find('operator');
    for(i = 0; i < arrOperators.length; i++)
    {
        var strOperatorName = $(arrOperators[i]).attr("name");
        this.initilizeOperator(strOperatorName);
    }
    this.setOperators(arrOperators);
};

OperatorManager.initilizeOperator = function(strOperatorName)
{
    if(this.objOperatorsArray[strOperatorName]!=null){
        //console.warn("***** OperatorLog -> getOperator() [ "+ strOperatorName +" ] -- Operator already exists! *****");
        return this.objOperatorsArray[strOperatorName];
    }
    try{
        this.objOperatorsArray[strOperatorName] = new MG[strOperatorName]();
        this.objOperatorsArray[strOperatorName].setId(strOperatorName);
        return this.objOperatorsArray[strOperatorName];
    }
    catch(err){
        //console.error('### ERROR :: Operator ['+strOperatorName+'] not found!!! ###');
    }
};

OperatorManager.setOperators = function(arrOperators)
{
    //console.log("!!!!!!!!!!!!!!!!!!! setOperators called in MG.OperatorManager ")
    var objThis = this;
    var arrOperatorsList = arrOperators;

    var operatorsLen = arrOperatorsList.length;
    for (var i = 0; i < operatorsLen; i++)
    {
        var objOpElement = $(arrOperatorsList[i]);
        var objCompDom = objOpElement.parent();
        var strCompName = objCompDom.attr("data-role");
        if(strCompName != undefined){
            var strOperatorName = objOpElement.attr('name');
            var strOperatorValue = objOpElement.attr('value');

            var objComp = objCompDom.data(strCompName);
            var objOperator = objThis.objOperatorsArray[strOperatorName];
            if(objOperator!=undefined){
                var blnAdded = objOperator.attachComponent(objComp);
                if(blnAdded && objOperator.id == undefined){
                    objOperator.id = strOperatorValue;
                }
            }
        }
        else{
            //console.warn("!!! Warning :: Specified object is not a widget and thus it can't have operator!!!");
        }
    }
};
