/**
* @description Initilize the eBook widget and renderer it's first page.
* @param {String}
* @return {String}
*/
(function($, undefined){
    $.widget("magic.contentmall", $.magic.magicwidget, {

        options: {
            src: null,// player json data file path
            isAuthorable: false
        },
        member://member variables
        {

        },
        _create: function(){
            var objThis = this;
            objThis.initialize();
        },
        /**
         * Initialize the variables and initiate the flow
         */
        _init: function(){
            $.magic.magicwidget.prototype._init.call(this);

            //$.extend(this.options, this.options);

        },

        /*
         * This function is used to load html
         * After loading complete of html replaces image paths from IDs to path using PathManager
         */
        initialize: function(){
            var objThis = this;


            objThis.hideUnrequiredSidePanelComponents();
            objThis.manageTabsInMasterPanels();
        },

        /**
         * This method hides icons and their separators in the side panel if the feature is not required.
         * @param none
         * @return void
         */
        hideUnrequiredSidePanelComponents: function()
        {
            var objSidePanelComponents = EPubConfig.SidePanelComponents;

            for(var strKey in objSidePanelComponents) {
                var strValue = objSidePanelComponents[strKey];

                //checking if feature is required or not. If not required, then we will hide the icon and its separator in the side panel.
                if(strValue && EPubConfig.RequiredApplicationComponents.indexOf(strKey) == -1)
                {
                    $("#" + strValue).css("display", "none");
                    $("#" + strValue + "-separator").css("display", "none");
                }
            }

        },

        /**
         * This method hides sub-panels in the master panel if they are not required and adjust the other available panel's click-able area accordingly.
         * @param none
         * @return void
         */
        manageTabsInMasterPanels: function()
        {
            var k;
            for(strPanelType in EPubConfig.TabbedPanels)
            {
                var bFirstPanelSet = false;
                var objPanelSubPanels = EPubConfig.TabbedPanels[strPanelType];
                var objFirstSubPanel;
                var iNumAvailableSubPanels = 0;

                //if current panel is available
                if(EPubConfig[EPubConfig.Feature_isAvaliblePropertyMapping[strPanelType]])
                {
                    var iTotalSubPanels = 0;

                    var arrVisiblePanels = [];
                    var arrTotalPanels = [];
                    for(strFeature in objPanelSubPanels)
                    {
                        iTotalSubPanels++;

                        var strIsAvailableProperty = EPubConfig.Feature_isAvaliblePropertyMapping[strFeature];
                        var strClickableComp = objPanelSubPanels[strFeature];
                        var objClickableItem = $("#" + strClickableComp);
                        var strType = objClickableItem.attr("type");

                        //if any of the sub-panels is not available, hide it's tab and content area;
                        if(EPubConfig[strIsAvailableProperty] == false)
                        {
                            $("#" + strType).css("display", "none");
                            objClickableItem.css("display","block");
                            objClickableItem.attr( "disabled", true );
                            objClickableItem.attr( "aria-disabled", true );
                            objClickableItem.css( "cursor", "auto");
                            objClickableItem.find("span").css("visibility", "hidden");
                            objClickableItem.addClass("leftPanelTab").removeClass("leftPanelTabActive");
                            objClickableItem.css('pointer-events', 'none');

                            var objChild = objClickableItem;
                            var objParent = objClickableItem.parent();
                            objClickableItem.remove();

                            objParent.append(objChild);
                            arrTotalPanels.push(objChild);
                        }
                        else
                        {
                            objClickableItem.css("display", "block");

                            //if current panel is first to be set in the view, making sure it's contents are visible.
                            if(bFirstPanelSet == false)
                            {
                                bFirstPanelSet = true;
                                objFirstSubPanel = objClickableItem;
                                $("#" + strType).css("display", "block");
                                objClickableItem.removeClass("leftPanelTab").addClass("leftPanelTabActive");
                            }
                            else
                            {
                                $("#" + strType).css("display", "none");
                            }

                            arrVisiblePanels.push(objClickableItem);
                            arrTotalPanels.push(objClickableItem);
                        }
                    }

                    if(arrVisiblePanels.length == 1) //disabling click event on panel's only tab
                    {
                        /*if($(objFirstSubPanel).attr("show-heading") != "true")
                         objFirstSubPanel.parent().css("display", "none");*/

                        objFirstSubPanel.css('pointer-events', 'none');

                        //ensuring that the only tab visible takes uses half of the panel's width
                        var nWidthOfVisiblePanel = $(objFirstSubPanel).parent().width() / 2;
                        objFirstSubPanel.css("width", nWidthOfVisiblePanel);

                        var nWidthOfHiddenPanel = nWidthOfVisiblePanel/(arrTotalPanels.length - 1);
                        for(k = 0; k < arrTotalPanels.length; k++)
                        {
                            if(arrTotalPanels[k] != objFirstSubPanel)
                                arrTotalPanels[k].css("width", nWidthOfHiddenPanel);
                        }
                    }
                    else if(arrVisiblePanels.length > 1) //adjust width of each sub-panels tab area;
                    {
                        //console.log("tabwidth:", $($(arrVisiblePanels[0]).parent()).find('[tabwidth]').length)
                        
                        var nFixedElementsWidth = 0;
                        if( arrVisiblePanels.length == 2 )
                        {
                        	arrVisiblePanels[ 0 ].removeAttr( "tabWidth" );
                        	arrVisiblePanels[ 1 ].removeAttr( "tabWidth" );
                        }
                        var arrFixedWidthElements = $($(arrVisiblePanels[0]).parent()).find('[tabwidth]');
                        if (arrFixedWidthElements.length > 0)
                        {
                            for(k = 0; k < arrFixedWidthElements.length; k++)
                            {
                                if(arrFixedWidthElements.lenght == 2)
								{
									$(arrFixedWidthElements[k]).removeAttr("tabwidth")
								}
                                nFixedElementsWidth += parseInt($(arrFixedWidthElements[k]).attr("tabwidth"))
                            }
                        }
                        //console.log("nFixedElementsWidth: ", nFixedElementsWidth);
                        var nWidthOfEachPanel = ($(arrVisiblePanels[0]).parent().width() - nFixedElementsWidth) / (arrVisiblePanels.length - arrFixedWidthElements.length);

                        //setting width of hidden panels to 0px & dividing visible panels in equal width
                        for(k = 0; k < arrTotalPanels.length; k++)
                        {
                            if(arrVisiblePanels.indexOf(arrTotalPanels[k]) == -1)
                                arrTotalPanels[k].css("width", "0px");
                            else
                            {
                                var nTabWidth = $(arrTotalPanels[k]).attr("tabwidth");
                                if ((nTabWidth) != undefined && parseInt(nTabWidth) > 0)
                                {
                                    arrTotalPanels[k].css("width", parseInt(nTabWidth));
                                }
                                else{
                                    arrTotalPanels[k].css("width", nWidthOfEachPanel);
                                }
                            }
                        }
                    }
                    else
                    {
                        objClickableItem.parent().css("display", "none");
                        $("#" + strType).siblings().css("display", "none");
                    }
                }
            }
        }
    })
})(jQuery);
