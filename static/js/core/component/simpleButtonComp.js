var hiddenTrue=true;
var check;
( function( $, undefined ) {
$.widget( "magic.simpleButtonComp",  $.magic.magicwidget, 
{
events:
	{
		CLICK:'click',
		DBLCLICK: 'taphold',
	},
	data:
	{
		type:null
	},
	options: 
	{
		 setEnabled:true,
		 isEnabled: true
	},
	_create: function()
	{
		$.extend(this.options, this.element.jqmData('options'));
		//this.
		if(this.options.setEnabled==undefined)
		this.options.setEnabled=true;
		this.options.selected = false;
		if (this.options.toggle == undefined) {
			try
			{
				var strStyle = this.options.offstyle;
				strStyle = this.options.onstyle;
			}
			catch(e)
			{
				console.warn('Please set styles for offstyle and onstyle if button is toggle');
			}
			this.options.toggle = false;
		}
		
		if (this.options.setEnabled == true) {
		 	/*$(this).bind("orientationchange", function(){
//		 		console.log("Orienation Change");
		 	})*/
		 	this.data.type = this.element.attr("type");
		 	var objClass = this;
			
			
			
		 	this.element.bind('click', function(e){
				if (objClass.options.isEnabled == true) {
					
			 		objClass.onHandler();
			 		e.stopPropagation();
					
					if(objClass.options.toggle == true)
					{
						if(objClass.options.selected == true)
						{
							$(objClass.element).attr('class', objClass.options.offstyle)
							objClass.options.selected = false;
						}
						else
						{
							$(objClass.element).attr('class', objClass.options.onstyle)
							objClass.options.selected = true;
						}
					}
				}
		 	});
			
			this.element.bind('taphold', function(e){
				
		 		objClass.onTapHold();
		 		e.stopPropagation();
		 	});
		 }else{
		 	this.disable();
		 }
		 
		 if(typeof(EPubConfig.scrollNavType) == 'undefined' || EPubConfig.scrollNavType == '') {
		 	//this.hideElemNav();
		 } else {
		 	//this.showElemNav();
		 }
		 
	},
	_init: function()
	{
		$.magic.magicwidget.prototype._init.call(this);
	},
	
	showElemNav : function() {
		$("[type='goToNextElement']").show();
		$("[type='goToPreviousElement']").show();
	},
	
	hideElemNav : function() {
		$("[type='goToNextElement']").hide();
		$("[type='goToPreviousElement']").hide();
	},
	
	onHandler: function(e)
	{
		//this.disable();
		$(this).trigger(this.events.CLICK, this.data);
		 
	},
	
	onTapHold: function(e)
	{
		//this.disable();
		$(this).trigger('taphold', this.data);
		 
	},
	
	enable: function() 
	{
		this.element.attr( "disabled", false );
		this.element.removeClass( "ui-disabled" ).attr( "aria-disabled", false );
		this.options.isEnabled=true;
		if(this.options.disabledstyle)
		{
			$(this.element).removeClass(this.options.disabledstyle);
		}
		return this._setOption( "disabled", false );
	},

	disable: function() 
	{
		
		this.element.attr( "disabled", true );
		this.element.addClass( "ui-disabled" ).attr( "aria-disabled", true );
		this.options.isEnabled=false;
		if(this.options.disabledstyle)
		{
			$(this.element).addClass(this.options.disabledstyle);
		}
		return this._setOption( "disabled", true );
	},
	
	setSelected: function(blnSelected)
	{
		
		this.options.selected = blnSelected;
		if(blnSelected == true)
		{
			$(this.element).attr('class', this.options.onstyle);
		}
		else
		{
			$(this.element).attr('class', this.options.offstyle);
		}
	},
	
	getSelected: function()
	{
		return this.options.selected;
	}
});
})( jQuery );