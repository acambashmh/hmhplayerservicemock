/**
 *
 */
var isZoomInOut = false;
var objScrollCompData = {};
var doPageLoadingTimer = null;
var doPageLoadingTime = 500;
var lastDist = 0;
var innerScrollContainerPosOnTouchStart = {};
var isZooming = false;
var scroll = 0;
var prevTime = 0, prevScroll = [0, 0], speed, timer;
var now1;
var scrollY = 0;
var stopCompleteScroll = false;
var stopCompleteScrollTimer;
var enableDblTap = true;
var flag = false;
var nCurrentScrollTop = 0;
var nCurrentTouchY = 0;
var isAnimationScrolling = false;
var timeOutComp;
var isFrmScrollStop = false;
var stopPageLoading = null;
var isSearchClicked = false;
var lastDopageLoadingIndex = -1;
var isPageViewChange = false;
(function($, undefined) {
	$.widget("magic.scrollviewcomp", $.magic.magicwidget, {
		//style="text-align: left; overflow: hidden; width:100%;height:100%;"
		options : {
			contentWidth : 0,
			contentHeight : 0,
			totalContentHeight : 0,
			minZoom : 0.5,
			maxZoom : 2.5,
			pageWidth : 0,
			pageHeight : 0,
			objContentContainer : null,
			objScrollContainer : null,
			nPageGap : 10,
			arrPageElements : [],
			arrPageLinkList : null,
			nCSSLoadCounter : 0,
			arrPageNames : null,
			nScalePercent : 1,
			nCurrentPageIndex : 0,
			rangedata : null,
			rangeObject : null,
			noteInfo : {
				ancestoralParentObjectID : null,
				pageNum : null
			},
			toppos : 0,
			Icontop : 0,
			pagecompBorder : 1,
			paddingTop : 0,
			Iconleft : 0,
			gapInBarnBody : 10,
			pageIndexForAnnotation : 0,
			pageCompMarginLeft : 0,
			landScrollContainerSet : null,
			portraitScrollContainerSet : null,
			selectionTimer : null, //added to fix ipad double processing for annotation tool positioning
			mobileSelectionTimer : null,
			repositioningTimer : null,
			repositioningPopup : null,
			hideBrandBarTimeout : null
		},

		/**
		 * Page Index Event
		 * Event will trigger on page scroll.
		 */
		events : {
			PAGE_RENDER_COMPLETE : "pagerendercomplete",
			PAGE_INDEX_CHANGE : "pageindexchange",
			ALERT_OK_CLICK : "alertokclick",
			SELECT_TEXT_END : "select_text_end"
		},
		member : //member variables
		{
			_objPathManager : null,
			elementScrollToppos : 0,
			isOrientationChange : false,
			previousLandscapeState : "",
			isChangedToDualPageMode : false,
			_objPageManager : null,
			alertOkClickHandler : null,
			startingView : null,
			_objElement : null,
			_nTouchGap : null,
			_nDoubleTapTime : 350,
			lastTouch : 0,
			_nScaledContentWidth : 0,
			_nScaledContentHeight : 0,
			lowestIndexCreated : 0,
			highestIndexCreated : 50,
			lowestPgBrkValCreated : 0,
			highestPgBrkValCreated : 50,
			curPageElementsPgBrk : [],
			curPageExists : true,
			totalPagesOnCreation : 0
		},
		
		immutableValues : { scrollViewLeft : 0,
							annotationPanelWidth : 0, 
							imageGalleryNext : 0 },

		_create : function() {
			if (!ismobile) {
				$.event.special.swipe.durationThreshold = 250;
				doPageLoadingTime = 0;
				EPubConfig.totalPageContainers = 50;
			}
			$.event.special.swipe.scrollSupressionThreshold = 100;
			var objThis = this;
			$.extend(this.options, this.element.jqmData('options'));

            LocalizationManager.setTooltipText();

			if(EPubConfig.MediaActiveClass && EPubConfig.MediaActiveClass != "")
			{
				
			}
			else if (EPubConfig.TextHighlight_Color && EPubConfig.TextHighlight_Color != "" ) {
				var strCSS = '.-epub-media-overlay-active { background-color: ' + EPubConfig.TextHighlight_Color + ' !important;} ';
				var scrScript = '<style type="text/css">' + strCSS + "</style>";
				$('head').append(scrScript);
			}

			/*if (EPubConfig.Markup_default_Color && EPubConfig.Markup_default_Color != "") {
				$('head').append("<style>.noteselection{background:" + EPubConfig.Markup_default_Color + ";cursor:pointer}</style>");
			}*/

			var objScrollElement = $(this.element).find('[id=bookContentContainer2]');
			this.options.objContentContainer = objScrollElement;

			//To change the Program branding title
			// $('#logoImgDiv').find('img').attr('src', getPathManager().appendReaderPath(EPubConfig.logoImgDivSrc));
			//$('#logoImgDiv2').find('img').attr('src', getPathManager().appendReaderPath(EPubConfig.logoImgDiv2Src));
			this.updateDesignForPageView("setBrandingBarPos");
			this.options.objScrollContainer = objScrollElement;
			//stopping the TAB key Event
			$(document).keydown(function(objEvent) {
				//disabling selection of text using keyboard when annotation pop-up is open
				if (objEvent.keyCode == 9 || ($("#annotation-bar").css("display") == "block" && objEvent.keyCode >= 37 && objEvent.keyCode <= 40 && objEvent.shiftKey == true))
				{
					objEvent.preventDefault();
					if($(objEvent.target).is('textarea') || $(objEvent.target).is('input'))
					{
						HotkeyManager.setNextActiveElement()
					}
				}
			});

			//objThis.hideBrandBar();
			if ( typeof (EPubConfig.swipeNavEnabled) != 'undefined' && EPubConfig.swipeNavEnabled == true && (ismobile != null || isWinRT) && GlobalModel.currentScalePercent == GlobalModel.actualScalePercent) {
				if (EPubConfig.pageView != "SCROLL") {
					PageNavigationManager.addSwipeListner(objScrollElement, objThis);
				} else {
					PageNavigationManager.removeSwipeListner(objScrollElement);
				}
			} else {
				PageNavigationManager.removeSwipeListner(objScrollElement);
			}

			/**
			 * this event is triggered by the window when a div starts scrolling.
			 * This block will remove the bookMarkPanel popup through removePopup function in popupmanager.
			 * 'closeOpenedPanel' is handled in PanelViewOperator and will close any popup opened through that operator.
			 */
			$(objScrollElement).unbind("scrollstart").bind("scrollstart", function(e) {
				if (isZooming || objThis.TOUCHNUM >= 2) {
					return;
				}

				if (ismobile) {
					enableDblTap = false;
				}
				//objThis.hideBrandBar();
				if (doPageLoadingTimer) {
					clearTimeout(doPageLoadingTimer);
				}
				if (!navigator.userAgent.match(/(android)/i)) {
					objThis.removeDefaultSelection();
				}
				if (!isZoomInOut) {
					if (objThis.member.isOrientationChange == false) {
						$(objThis).trigger('closeOpenedPanel');
						//this is bind in panel view operator;
					}
					objThis.member.isOrientationChange = false;
				}
				isZoomInOut = false;
			});

			$(document).bind('click', function(e) {
				if (e.target && (e.target.id == "mainContent" || e.target.id == "mainShowroom")) {
					if (!ismobile) {
						$("#annotation-bar").css("display", "none")
					}

				}

				objThis.hideBrandBar();
			});

			$("body").mousedown(function(e) {
				objThis.hideBrandBar();
			});

			$(objScrollElement).bind("scrollstop", function(e) {
				if (ismobile) {
					enableDblTap = true;
				}

				if (isZooming || objThis.TOUCHNUM >= 2) {
					return;
				}

				if (EPubConfig.pageView != "singlePage" && EPubConfig.pageView != "doublePage") {
					isFrmScrollStop = true;
					$(objThis.element).trigger('pagescrollstop');
					// this can be removed and the actual content can be written
					objThis.member.elementScrollToppos = $(objScrollElement).scrollTop();
					// log("objThis.member.elementScrollToppos in scrollstop "+ objThis.member.elementScrollToppos);
					if (GlobalModel.currentPageIndex != GlobalModel.currentPageIndexByPos) {
						$("#audioLauncherBtn").addClass("ui-disabled");
						$("#audioLauncherBtn").data('buttonComp').disable();
						$( '#audioLauncherBtn' ).removeClass( 'audioLauncherBtn-on' ).addClass( 'audioLauncherBtn-off' );
						GlobalModel.currentPageIndex = GlobalModel.currentPageIndexByPos;
						if (objThis.member.isOrientationChange == false && EPubConfig.pageView != 'singlePage' && EPubConfig.pageView != 'doublePage') {
							//$(objThis).trigger(objThis.events.PAGE_INDEX_CHANGE, objThis.data);
						}
					}
					objThis.options.nCurrentPageIndex = GlobalModel.currentPageIndex;
					objThis.member.isOrientationChange = false;
				}
				doPageLoadingTimer = setTimeout(function() {
					//log("SCROLL STOP");
					//log("scrollstop");
					var iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
					//log("iPageIndex: "+iPageIndex);
					//log(GlobalModel.isScrolling +"   " + isAnimationScrolling);
					if (GlobalModel.isScrolling || isAnimationScrolling) {
						return;
					}
					if ($("#header").css("display") != "none" && isContentCSSLoaded == true) {
						objThis.hideBrandBar();
					}
					if (document.getElementById("annotation-bar").style.display == "block") {
						PopupManager.removePopup('annotation-bar');
					}
					objThis.stopScrolling();
					// }
					//log("SCROLL STOP2");
				}, doPageLoadingTime);
			});

			$('#alertOKBtn').click(function(e) {
				PopupManager.removePopup();

				$(objThis).trigger(objThis.events.ALERT_OK_CLICK);
				if (objThis.member.alertOkClickHandler != null) {
					objThis.member.alertOkClickHandler(e);
					objThis.member.alertOkClickHandler = null;
				}
			});

			this.member._objElement = this.element.children()[0];
			this.member._objElement.thisRef = this;
			this.addListeners();
			$("body").css({
				'height' : '100%',
				'width' : '100%'
			});

			//adding and removing certain classes from certain element to support CSS load if app is loaded from content
			$("body").addClass("ui-mobile-viewport")
			$("#pg").addClass("ui-page ui-body-c ui-page-active")
			$("html").removeClass("ui-mobile-rendering");

			$('#bookPromptOverlayMessageCloseButton').click(function() {
				$("#bookPromptOverlayMessage").css("display", "none");
				clearTimeout(bookPromptOverlayMessagetimeout);
			});

		},
		

/*--------------------------------------------------------------- Update display Starts -------------------------------------------------------------*/
	
		updateQAPanelDisplay : function ( obj )
		{
			var _qaPanel = $( "#questionAnswerPanel" );
			var _isScrollVisible = false;
			var _bottom  = "0px";
			
			_isScrollVisible = this.checkIfScrollBarsAreAppearing( obj );
			
			/*
			 * Position QA Panel above the horizontal scroll bar
			 */
			if( _isScrollVisible && !ismobile )
			{
				_bottom = "16px"
			}
			_qaPanel.css( 'bottom' , _bottom );
			
			if ( ismobile && ( this.getOrientation() == AppConst.ORIENTATION_PORTRAIT ) ) // Android and IOS in portrait mode.
			{
				_qaPanel.css( "width" , window.width - this.immutableValues.scrollViewLeft - 4 );
			}
			else // WinRT, Desktop and Android, IOS( both in Landscape mode )
			{
				if ( $( "#questionPanelMinimizeIcon" ).hasClass( 'minimizeQAPanel' ) )
				{
					var updatedQAPanelWidth = EPubConfig.pageWidth * obj;
					var scrollBarAssumedWidth = 30;
					var qAPanelMaxWidth = $( window ).width() - this.immutableValues.annotationPanelWidth -
										  2 * this.immutableValues.imageGalleryNext - scrollBarAssumedWidth;
					if ( updatedQAPanelWidth > EPubConfig.pageWidth && updatedQAPanelWidth < qAPanelMaxWidth ) 
					{
						_qaPanel.width(updatedQAPanelWidth);
					} 
					else if ( updatedQAPanelWidth > EPubConfig.pageWidth && updatedQAPanelWidth > qAPanelMaxWidth && qAPanelMaxWidth > EPubConfig.pageWidth )
					{
						_qaPanel.width(qAPanelMaxWidth);
					} 
					else 
					{
						_qaPanel.width(EPubConfig.pageWidth);
					}
					_qaPanel.css( "max-width", $("#bookContentContainer").width() );
				}
			}
			
			var qaScrollDiv = $( _qaPanel ).find( '[ id = qaPanelScrollableDiv ]' )[ 0 ];
			var qaHeader = $( _qaPanel ).find( '[ id = questionanswerHeader ]' )[ 0 ];
			var qaFooter = $( _qaPanel ).find( '[ id = questionanswerFooter ]' )[ 0 ];
			var maxHightForQaScrollDiv = $( window ).height() - 
										 parseInt( $( qaFooter ).css( 'height' ) ) - 
										 parseInt( $( qaHeader ).css( 'height' ) );
			if(  $( "#lnsQuestionNavigator" ).css( "display" ) != "block" )
			{
				$( qaScrollDiv ).css( 'height', 'auto' );
			}
			
			$(qaScrollDiv).css( 'max-height', maxHightForQaScrollDiv );
		},
		
		updateImageGalleryDisplay : function ( obj )
		{
			var _imageGalleryPanel = $( "#imageGalleryPanel" );
			var _isScrollVisible = false;
			var _bottom  = "0px";
			
			_isScrollVisible = this.checkIfScrollBarsAreAppearing( obj );
			
			/*
			 * Position Image Gallery Panel above the horizontal scroll bar
			 */
			_bottom = ( _isScrollVisible && !ismobile ) ? "16px" : _bottom;
			
			_imageGalleryPanel.css( 'bottom' , _bottom );
			
			if ( ismobile && ( this.getOrientation() == AppConst.ORIENTATION_PORTRAIT ) ) // Android and IOS in portrait mode.
			{
				_imageGalleryPanel.width( window.width - this.immutableValues.annotationPanelWidth - 4 );
			}
			
			_imageGalleryPanel.css("max-width", $("#bookContentContainer").width());
		},
		
		updateLearnosityPanelAndAudioPlayerDisplay : function ( obj )
		{
			var _lnsPanel = $( "#learnosityPanel" );
			var _isScrollVisible = false;
			var _bottom  = 0;
			var _audioPlayerVisible = ( $( ".players" ).css( "display" ) == "block" ) ? true : false;
			
			_isScrollVisible = this.checkIfScrollBarsAreAppearing( obj );
			
			/*
			 * Position Learnosity Panel and Audio Panel above the horizontal scroll bar
			 */
			if( _isScrollVisible && ismobile )
			{
				_bottom = 16;
			}
			
			if( _audioPlayerVisible ) // if Audio player is visible then show learnosity panel above it
			{
				$( ".players" ).css( "bottom", _bottom );
				_bottom += 40;
			}
			_lnsPanel.css( "bottom", _bottom );
			
			if ( ismobile && ( this.getOrientation() == AppConst.ORIENTATION_PORTRAIT ) ) // Android and IOS in portrait mode.
			{
				_lnsPanel.width( window.width - this.immutableValues.scrollViewLeft - 4 );
			}
			else
			{
				if ( $( "#learnosityPanelMinimizeIcon" ).hasClass( "minimizeQAPanel" ) ) 
				{
					var updatedQAPanelWidth = EPubConfig.pageWidth * obj;
					var scrollBarAssumedWidth = 30;
					var qAPanelMaxWidth = $( window ).width() - this.immutableValues.annotationPanelWidth -
									  	  2 * this.immutableValues.imageGalleryNext - scrollBarAssumedWidth;
									  	  
					if ( updatedQAPanelWidth > EPubConfig.pageWidth && updatedQAPanelWidth < qAPanelMaxWidth ) 
					{
						_lnsPanel.width( updatedQAPanelWidth );
					} 
					else if ( updatedQAPanelWidth > EPubConfig.pageWidth && updatedQAPanelWidth > qAPanelMaxWidth && qAPanelMaxWidth > EPubConfig.pageWidth ) 
					{
						_lnsPanel.width( qAPanelMaxWidth );
					} 
					else 
					{
						_lnsPanel.width( EPubConfig.pageWidth );
					}
					_lnsPanel.css( "max-width", $( "#bookContentContainer" ).width() );
				} 
			}
		},

/*--------------------------------------------------------------- Update display ends -------------------------------------------------------------*/
		
		getOrientation : function ()
		{
			var _orientation = AppConst.ORIENTATION_LANDSCAPE;
			
			if ( navigator.userAgent.match( /(android)/i ) ) 
			{
				if ( window.height > window.width ) 
				{
					_orientation = AppConst.ORIENTATION_PORTRAIT;
				} 
			} 
			else 
			{
				_orientation = getDeviceOrientation();
			}
			
			return _orientation;
		},
		
		checkIfScrollBarsAreAppearing : function ( obj )
		{
			var _isScrollVisible = false;
			
			if ( EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage' ) 
			{
				if ( ( $( "#scrollMainContainer" ).width() ) * obj > $( "#bookContentContainer" ).width() ) 
				{
					_isScrollVisible = true;
				}
			} 
			else 
			{
				if ( ( $( "[ type = 'PageContainer' ]" ).width() ) * obj > $( "#bookContentContainer2" ).width() ) 
				{
					_isScrollVisible = true;
				}
			}
			
			return _isScrollVisible;
		},
				
		
		
		stopScrolling : function(objScrollElement) {
			objThis = this;
			if (EPubConfig.pageView != "singlePage" && EPubConfig.pageView != "doublePage") {
				objThis.managePageElementsCreation(GlobalModel.currentPageIndexByPos);
				if (objThis.member.isOrientationChange == false && EPubConfig.pageView != 'singlePage' && EPubConfig.pageView != 'doublePage') {
					$(objThis).trigger(objThis.events.PAGE_INDEX_CHANGE, objThis.data);
				}
			}
		},
				
		/**
		 * This function changes the attributes based on the view type
		 * @param {Object} objType: it defines the type of attribute to be changed.
		 */
		updateDesignForPageView : function(objType, arrObj) {
			var objThis = this;
			var _val = "";
			var strOrient = "";

			switch (objType) {
				case "panelPosition":
				
					if( $( "#questionAnswerPanel" ).css( "display" ) == "block" )
					{
						objThis.updateQAPanelDisplay( arrObj );
					}
					else if( $( "#imageGalleryPanel" ).css( "display" ) == "block" )
					{
						objThis.updateImageGalleryDisplay( arrObj );
					}
					//else if ( ( $( "#learnosityPanel" ).css( "display" ) == "block" ) || ( $( ".players" ).css( "display" ) == "block" ) )
					//{
						objThis.updateLearnosityPanelAndAudioPlayerDisplay( arrObj );
					//}
				
					$( document ).trigger( 'updateLearnosityViewSize' );
					break;

				case "pageLayoutOnOrientationChange":
					var isResizing = arrObj;
					if ( ismobile || window.navigator.msPointerEnabled ) 
					{
						if ( getDeviceOrientation() == AppConst.ORIENTATION_LANDSCAPE ) 
						{
							$( ".doublePageLauncher" ).removeClass( "ui-disabled" );

						} else 
						{
							$( ".doublePageLauncher" ).addClass( "ui-disabled" );
						}
						
						if( EPubConfig.ReaderType.toUpperCase() == "PREKTO1" )
						{
							$( "#zoomBtn" ).removeClass( "ui-disabled" );
						}
					}

					if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') 
					{
						if (getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT) 
						{

							if ((EPubConfig.pageView == 'doublePage' && objThis.member.previousLandscapeState == 'doublePage')) 
							{
								$(".singlePageLauncher").removeClass('singlepagelauncherSelected');
								$(".singlePageLauncher").trigger('click');
							}
							
							if( EPubConfig.ReaderType.toUpperCase() == "PREKTO1" )
							{
								$( "#zoomBtn" ).addClass( "ui-disabled" );
							}
						} 
						else
						{
							if ((EPubConfig.pageView == 'singlePage' && objThis.member.previousLandscapeState == 'doublePage')) 
							{
								$(".doublePageLauncher").removeClass('dblpagelauncherSelected')
								$(".doublePageLauncher").trigger('click')
							}
						}
						
					}
					break;

				case "updateElemSize":
					if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
						var navigationArrowsWidth = 45;
						if ($("#prevPgBanner").css('display') == 'block') {
							navigationArrowsWidth = $("#prevPgBanner").outerWidth();
						} else {
							navigationArrowsWidth = 0;
						}

						$("#bookContentContainer2").css('height', '100%');
						var annotationPanelWidth = $("#annotationPanel").outerWidth();
						var bkContainerOffset = $("#bookContentContainer").offset().left;
						var nextPrevBannwerWidth = $('#nextPgBanner').outerWidth();
						if (ismobile && EPubConfig.swipeNavEnabled) {
							nextPrevBannwerWidth = 0;
						}
						$("#bookContentContainer").css('margin-left', annotationPanelWidth + nextPrevBannwerWidth);
						$("#programBranding").css('margin-left', annotationPanelWidth + nextPrevBannwerWidth);
						$("#bookContentContainer").css({
							'width' : window.width - annotationPanelWidth - 2 * navigationArrowsWidth
						});

						$("#bookContentContainer2").css('overflow', 'hidden');
						if (ismobile) {
							$("#programBranding").css('margin-left', annotationPanelWidth);
							$("#bookContentContainer2").css('overflow', 'hidden');
						}
						$('#innerScrollContainer').css({
							'width' : 'auto'
						});

						for (var k = 0; k < EPubConfig.totalPageContainers; k++) {
							
							if (ismobile) {

								var posLeft = (objThis.options.contentWidth * k) + 1;
								$($('[type="PageContainer"]')[k]).css('position', 'absolute');
								$($('[type="PageContainer"]')[k]).css('left', posLeft);

							} else {
								var updatedPosLeft = 0;
								if (k == 0) {
									updatedPosLeft = 0;
								} else {
									var posLeft = (EPubConfig.pageWidth) + 1;
									var prevElemLeft = parseInt($($('[type="PageContainer"]')[k - 1]).css('left'));
									updatedPosLeft = parseInt(prevElemLeft) + parseInt(posLeft);
								}
								$($('[type="PageContainer"]')[k]).css('position', 'absolute');
								$($('[type="PageContainer"]')[k]).css('left', updatedPosLeft);
							}

						}

						$('[type="PageContainer"]').css({
							'height' : objThis.options.contentHeight,
							'margin-bottom' : '0px',
							'float' : 'none',
							'border' : 'none',
							'overflow' : 'hidden'
						});
						
						$("#scrollMainContainer").css('border', '1px solid rgb(150, 149, 149)');
					} else {
						$("#bookContentContainer2").css({
							'overflow' : 'auto',
							'left' : 0
						});
						$('#innerScrollContainer').css({
							'width' : objThis.options.contentWidth
						});

						for (var k = 0; k < EPubConfig.totalPageContainers; k++) {
							var posLeft = objThis.options.contentWidth * k;
							$($('[type="PageContainer"]')[k]).css('position', 'relative');
							$($('[type="PageContainer"]')[k]).css('left', 0);
						}

						$('[type="PageContainer"]').css({
							'visibility' : 'visible',
							'height' : objThis.options.contentHeight,
							'margin-bottom' : '10px',
							'float' : 'none',
							'border' : '1px solid rgb(150, 149, 149)',
							'overflow' : 'hidden'
						});
						$("#scrollMainContainer").css({
							'width' : 'auto',
							'height' : 'auto'
						});
						var annotationPanelWidth = $("#annotationPanel").outerWidth();
						var bookContentConatinerUserMargin = 0;
						if (ismobile) {
							bookContentConatinerUserMargin = 38;
						}
						$("#bookContentContainer").css('margin-left', annotationPanelWidth + bookContentConatinerUserMargin);
						$("#programBranding").css('margin-left', annotationPanelWidth + bookContentConatinerUserMargin);
						if (ismobile) {
							$("#programBranding").css('left', annotationPanelWidth);
						}
						var bkContainerOffset = $("#bookContentContainer").offset().left;
						$("#bookContentContainer").css({
							'width' : window.width - bkContainerOffset
						});
						$("#scrollMainContainer").css('border', 'none');

					}
					objThis.updateViewSize();
					break;

				case "setBrandingBarPos":
					if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
						var annotationPanelWidth = $("#annotationPanel").outerWidth();
						var nextPrevBannwerWidth = $('#nextPgBanner').outerWidth();
						if (ismobile) {
							nextPrevBannwerWidth = 0;
						}
						$("#programBranding").css('margin-left', annotationPanelWidth + nextPrevBannwerWidth);
						if (ismobile) {
							$("#programBranding").css('margin-left', annotationPanelWidth);
						}
					} else {
						var annotationPanelWidth = $("#annotationPanel").outerWidth();
						var bookContentConatinerUserMargin = 0;
						$("#programBranding").css('margin-left', annotationPanelWidth + bookContentConatinerUserMargin);
						if (ismobile) {
							$("#programBranding").css('left', annotationPanelWidth);
						}
					}
					break;

			}
		},

		setAlertOkClickHandler : function(fnClickHandler) {
			this.member.alertOkClickHandler = fnClickHandler;
		},

		_init : function() {
			$.magic.magicwidget.prototype._init.call(this);
			if ((isNative) || (_objQueryParams.homeurl)) {
				$("#nativeCloseBtn").css('display', 'block');
			} else {
				$("#nativeCloseBtn").css('display', 'none');
			}

			var objThis = this;
			if ($("#annotationPanel").css("display") == "none") {
				$("#annotationPanel").width(0);
				$("#prevPgBanner").css("left", "0");
			}
			if (EPubConfig.ReaderType == '6TO12') {
				if(!EPubConfig.Index_isAvailable){
					$("#tocIndex").css("display","none");
					$("#tocContents").css("width","100%");
				}
				else{
					$("#tocContents").css("width","50%");
				}
				$("#bookContentContainer").unbind('mousedown').bind('mousedown', function(){
					if($("#searchPanel").css("display") == "block")
					{
				    	$("#searchPanel").css("display","none");
						$("#navSearchBtn").removeClass("active");
					}
				});
			}
			this.member.arrPageNames = new Array();
			var strPageName = "";
			
			$("#navigationBar").unbind('click').bind('click', function(e) {
				if($(e.target).attr("class") == "srhIcon"){
        			isSearchClicked = true;
        			
        		}
        		$(objThis).trigger('closeOpenedPanel');
        	});

			if (GlobalModel.HMHPlayerEmbedded) {
				topNavigationBarHeight = 0;
			}
		},

		setAndLoadPages : function(arrPagesList) {
			this.options.arrPageLinkList = arrPagesList;
            this.initializeView();
			this.loadCSS();
		},

		loadCSS : function() {
			var objThis = this;
			var url = getPathManager().getEPubCSSPath(EPubConfig.cssFileNames[objThis.options.nCSSLoadCounter]);
			var hasPageLevelCss = false;
			if (EPubConfig.Print_isAvailable) {
				var scrScriptPrint = '<link rel="stylesheet" href="' + url + '" type="text/css" media="print">';
				$('head').append(scrScriptPrint);

			}

			scrScriptPrint = '<link rel="stylesheet" href="' + url + '" type="text/css"/>';
			$('head').append(scrScriptPrint);
			
			//if (EPubConfig.Print_isAvailable) {
			    for (var k = 0; k < GlobalModel.printCSSPath.length; k++) {
			        var printCSS = getPathManager().appendReaderPath(GlobalModel.printCSSPath[k]);
			        //appendReaderPath
			        var cssForPrint = '<link rel="stylesheet" href="' + printCSS + '" type="text/css" media="print">';
                    $('head').append(cssForPrint);
                }
			//}


            var ss = document.createElement("link");
            ss.type = "text/css";
            ss.rel = "stylesheet";
            ss.href = url;
            ss.onload = function()
            {
                objThis.options.nCSSLoadCounter++;
                if (objThis.options.nCSSLoadCounter < EPubConfig.cssFileNames.length) {
                    objThis.loadCSS();
                } else {
                    try {

                        isContentCSSLoaded = true;

                        if(GlobalModel.pagesToLoadAfterLoadingOfCommonCSS != undefined)
                        {
                            for(var p = 0; p < GlobalModel.pagesToLoadAfterLoadingOfCommonCSS.length; p++)
                            {
                               var objPageRole =  GlobalModel.pagesToLoadAfterLoadingOfCommonCSS[p].dataRole;
                               if($(objPageRole.element).attr("pagename") == GlobalModel.pagesToLoadAfterLoadingOfCommonCSS[p].pageName)
                               {
                                   objPageRole.loadPage(GlobalModel.pagesToLoadAfterLoadingOfCommonCSS[p].pagePath);
                               }
                            }
                        }

                        var bAnnotationToolsRequired = false;

                        //see if any of the annotation tools are added in required components/features list
                        for (var strComponents in EPubConfig.AnnoToolNames) {
                            if (EPubConfig.RequiredApplicationComponents.indexOf(strComponents) > -1) {
                                bAnnotationToolsRequired = true;
                                break;
                            }
                        }

                        if (bAnnotationToolsRequired) {
                            objThis.getSelectioninfo();
                        }

                        //$("#mainShowroom").css("visibility", "visible");
                        //$( "#mainShowroom" ).addClass( "shellFadeInEffect" );
                        $("#mainShowroom").css("opacity", "1");
                        $("#appPreloaderContainer").css("display","none");
                        objThis.options.hideBrandBarTimeout = setTimeout(function() {
                            objThis.hideBrandBar();
                        }, EPubConfig.brandHideAfterDuration);
                    } catch (e) {
                        console.error(e.message)
                    }
                }
            }
            document.getElementsByTagName("head")[0].appendChild(ss);

		},

		appendCSSContent : function(strCSS) {

			//apply formatting as per content's body tag in loaded CSS in content pages
			var objReg = /body/g;
			strCSS = strCSS.replace(objReg, ".contentBody");

			objReg = /\.\.\/fonts/g;
			strCSS = strCSS.replace(objReg, getPathManager().getFontsPath());

			objReg = /\.\.\/images\//g;
			strCSS = strCSS.replace(objReg, getPathManager().getEPubImagePath());

			objReg = /epub\|/g;
			strCSS = strCSS.replace(objReg, "");

			var scrScript = '<style type="text/css">' + strCSS + "</style>";
			$('head').append(scrScript);

			//$(scrScript).appendTo('head:eq(0)');
		},

		hidePopupBeforeReposition : function() {
			var objThis = this;
			if (objThis.options.repositioningTimer) {
				clearTimeout(objThis.options.repositioningTimer);
			}
			objThis.options.repositioningTimer = null;
			if ($("#stickyNoteContainer").css("display") == "block") {
				objThis.options.repositioningPopup = "#stickyNoteContainer";
				$("#stickyNoteContainer").css("visibility", "hidden");
				$('.tagOuter').css('visibility', 'hidden');
				$('.stickyNoteTags').css('visibility', 'hidden');
				$("#popupArrowDiv").css("visibility", "hidden");
			} else if ($("#glossaryPanel").css("display") == "block") {
				objThis.options.repositioningPopup = "#glossaryPanel";
				$("#glossaryPanel").css("visibility", "hidden");
				$("#popupArrowDiv").css("visibility", "hidden");
			} else if ($("#footNotePanel").css("display") == "block") {
				objThis.options.repositioningPopup = "#footNotePanel";
				$("#footNotePanel").css("visibility", "hidden");
				$("#popupArrowDiv").css("visibility", "hidden");
			} else if ($("#highlightContainer").css("display") == "block") {
				objThis.options.repositioningPopup = "#highlightContainer";
				$("#highlightContainer").css("visibility", "hidden");
			} else if ($("#annotation-bar").css("display") == "block") {
				PopupManager.removePopup();
				$("#annotation-bar").css("display", "none");
				if (!navigator.userAgent.match(/(android)/i)) {
					objThis.options.rangedata = " ";
				}
			}

		},

		initializeView : function() {
			var objThis = this;
			/*if (navigator.userAgent.match(/(android)/i)) {
				EPubConfig.swipeNavEnabled = false;
			}*/
			objThis.updateViewSize();
			$(window).bind('touchmove', function(e) {
				e.stopPropagation();
			});
			if (GlobalModel.currentPageIndex == 0) {
				$("#prevPgBanner").addClass('ui-disabled');
				$("#prevBtnLeft").addClass('ui-disabled');
			}
			if ((EPubConfig.pageView == 'doublePage') && ((getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT))) {
				EPubConfig.pageView = 'singlePage';
				objThis.member.previousLandscapeState = 'doublePage';
			}

			$("#annotationPanel").unbind('click').bind('click', function() {
				PopupManager.removePopup('annotation-bar');
			});
			
			//for remove space ipad-landscape view
			if (getDeviceOrientation() == AppConst.ORIENTATION_LANDSCAPE && navigator.userAgent.match(/(ipad)/i))
			{
				$('#mainShowroom').css('height',window.innerHeight);
			}
			objThis.showHideSideNavigationBars();
			objThis.updateDesignForPageView("updateElemSize");

			/**
			 * To handle layouting when resizing the window
			 */
			$(window).bind('resize', function() {
				if (ismobile == null) {
					objThis.hidePopupBeforeReposition();

					var popupbar = "#annotation-bar";
					setTimeout(function() {
						objThis.updateViewSize();
					}, 0);

					setTimeout(function() {
						if ((GlobalModel.currentScalePercent.toFixed(3) == GlobalModel.actualScalePercent.toFixed(3)) && (ismobile || (EPubConfig.pageView == 'singlePage') || (EPubConfig.pageView == 'doublePage'))) {

							if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
								objThis.scaleContentToFit();
								objThis.updateViewSize();
								var bkContainerLeft = $("#bookContentContainer2").css('left');
								// $("#bookContentContainer2").css(bkContainerLeft*GlobalModel.currentScalePercent);
								var iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
								objThis.showPageAtIndex(iPageIndex, true);
							}
						}

						if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
							var navigationArrowsWidth = 45;
							if ($("#prevPgBanner").css('display') == 'block') {
								navigationArrowsWidth = $("#prevPgBanner").width();
							} else {
								navigationArrowsWidth = 0;
							}
							$("#bookContentContainer").width(window.width - parseInt(navigationArrowsWidth) - parseInt($("#bookContentContainer").css('margin-left')));
						}

					}, 100);

					var highlightPop = $("[type='highlightContainer']");
					if ($(popupbar).css('display') == 'block') {
						$(popupbar).hide();
						setTimeout(function() {
							if ($("#annotation-bar").css("display") == "block")
								objThis.setAnnotationPopup("annotation-bar");
						}, 0);
					}

					PageNavigationManager.adjustPageToScreen(GlobalModel.currentScalePercent);
					setTimeout(function() {
						objThis.rePositionEbookPopups();
						if (objThis.options.repositioningPopup) {
							$(objThis.options.repositioningPopup).css("visibility", "visible");
							$('.tagOuter').css('visibility', 'visible');
							$('.stickyNoteTags').css('visibility', 'visible');
							if (objThis.options.repositioningPopup != "#highlightContainer") {
								$("#popupArrowDiv").css("visibility", "visible");
							}
							objThis.options.repositioningPopup = null;
						}
					}, 500);
				}
			});

			/**
			 * To handle layouting when tab is changed and application recieves focus.
			 */
			$(window).bind('focus', function() {
				if (ismobile) {
					if (GlobalModel.currentDeviceOrientation != getDeviceOrientation()) {
						$(window).trigger('orientationchange');
						GlobalModel.currentDeviceOrientation = getDeviceOrientation();
					}
				}
			});

			/**
			 * To handle layouting when orientation is changed
			 */
			$(window).bind('orientationchange', function() {
				//document.activeElement.focus();
				/** While orientation change, make the page Content hidden, and make it visible after scrollTop gets reset **/
				if (navigator.userAgent.match(/(android)/i)) {
					$('[type=PageContainer]').children().css('visibility', 'hidden');
				}
				if (GlobalModel.currentScalePercent.toFixed(3) != GlobalModel.actualScalePercent.toFixed(3)) {
					if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
						objThis.resetZoom();
					}
				}
				// objThis.resetZoom();
				if (getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT) {
					objThis.member.previousLandscapeState = EPubConfig.pageView;
				}
				objThis.updateDesignForPageView("pageLayoutOnOrientationChange", true);
				objThis.updateDesignForPageView("updateElemSize");
				objThis.hidePopupBeforeReposition();

				var fnOnOrientationChange = function() {
					if (!navigator.userAgent.match(/(android)/i)) {
						if (document.activeElement)
							document.activeElement.blur();
						$(":input").blur();
					}
					objThis.member.isOrientationChange = true;
					setTimeout(function() {
						//   $('[type="PageContainer"]').children().css('visibility','hidden');
						objThis.updateViewSize();
						if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
							if (getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT) {
								if ($("#questionPanelMinimizeIcon").hasClass('maximizeQAPanel')) {
									var leftVal = $("#bookContentContainer").offset().left;
									$("#questionAnswerPanel").css({
										"left" : leftVal
									});
								}
							}
							var navigationArrowsWidth = 45;
							if ($("#prevPgBanner").css('display') == 'block') {
								navigationArrowsWidth = $("#prevPgBanner").width();
							} else {
								navigationArrowsWidth = 0;
							}
							$("#bookContentContainer").width(window.width - parseInt(navigationArrowsWidth) - parseInt($("#bookContentContainer").css('margin-left')));
						}
						if ((GlobalModel.currentScalePercent.toFixed(3) == GlobalModel.actualScalePercent.toFixed(3)) && (ismobile || isWinRT || (EPubConfig.pageView == 'singlePage') || (EPubConfig.pageView == 'doublePage'))) {
							objThis.scaleContentToFit();
							if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
								var iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
								objThis.showPageAtIndex(iPageIndex, true);
							}
						} else if (GlobalModel.currentScalePercent.toFixed(3) != GlobalModel.actualScalePercent.toFixed(3)) {
							if (EPubConfig.pageView.toLowerCase() == 'scroll') {
								objThis.resetZoom();
								objThis.scaleContentToFit();
							} else {
								//objThis.scaleContentToFit();
							}
						}
						//objThis.resetZoom();
						setTimeout(function() {
							objThis.rePositionEbookPopups();
							if (objThis.options.repositioningPopup) {
								$(objThis.options.repositioningPopup).css("visibility", "visible");
								$('.tagOuter').css('visibility', 'visible');
								$('.stickyNoteTags').css('visibility', 'visible');
								if (objThis.options.repositioningPopup != "#highlightContainer") {
									$("#popupArrowDiv").css("visibility", "visible");
								}

								objThis.options.repositioningPopup = null;
							}
							
							/** When everything resets after orientation change, make the page Content Visible **/
							if (navigator.userAgent.match(/(android)/i)) {
								$('[type=PageContainer]').children().css('visibility', 'visible');
							}
						}, 50);
					}, 500);
				}
				//adding a timeout to ensure that orientation change related changes take effect.
				setTimeout(fnOnOrientationChange, 200);
				//for remove space ipad-landscape view
				if ( navigator.userAgent.match(/(ipad)/i))
				{
					$('#mainShowroom').css('height',window.innerHeight);
				}
			});

			objThis.options.contentWidth = EPubConfig.pageWidth;
			objThis.options.contentHeight = EPubConfig.pageHeight;

			var i = 0;
			var floatVal;

			//checking if page width and page height are valid numbers
			if (isNaN(EPubConfig.pageWidth))
				throw new Error("Value of pageWidth is not a number");

			if (isNaN(EPubConfig.pageHeight))
				throw new Error("Value of pageHeight is not a number");

			var strPage = "";
			if (EPubConfig.pageView == 'doublePage') {
				strPage = '<div id="innerScrollContainer" style="width: 100%;">';
				//$('.nextPreviousPageInactive').css('display', 'block');
			} else {
				strPage = '<div id="innerScrollContainer" style="visibility:hidden; width:' + objThis.options.contentWidth + 'px;">';
			}

			var iLength = EPubConfig.totalPages;
			objThis.member.totalPagesOnCreation = iLength;
			if (EPubConfig.isPageOrderInSequence == false) {
				this.member.arrPageNames = this.options.arrPageLinkList;
				iLength = this.options.arrPageLinkList.length;
			}
			
			// creating PageBrks Array
			for ( i = 0; i < iLength; i++) {
				//////console.log(this.options.arrPageLinkList[i]);
				objPgNameReg = /.xhtml/gi;
				strPageName = this.options.arrPageLinkList[i];
				strPageName = strPageName.replace(objPgNameReg, "");
				var pageBrkValue = 0;
				var pageBreakId = "";

				if (GlobalModel.ePubGlobalSettings.PageBreaks) {
					if (GlobalModel.ePubGlobalSettings.PageBreaks[i]) {
						pageBrkValue = GlobalModel.ePubGlobalSettings.PageBreaks[i].pageBreakValue;
						pageBreakId = GlobalModel.ePubGlobalSettings.PageBreaks[i].pageBreakId;
						var objPageData = GlobalModel.ePubGlobalSettings.Page[i];
						for (var strProp in GlobalModel.ePubGlobalSettings.PageBreaks[i]) {
							objPageData[strProp] = GlobalModel.ePubGlobalSettings.PageBreaks[i][strProp];
						}
						objPageData["pagebreakname"] = objPageData["pageName"].replace(/.xhtml/gi, "");
						objPageData.pagesequence = i;
						GlobalModel.pageDataByPageBreakValue["pagebreakid_" + pageBrkValue] = objPageData;
						GlobalModel.pageDataByPageName[objPageData["pagebreakname"]] = objPageData;
						GlobalModel.pageDataBySequence["pagesequence_" + i] = objPageData;
					} else {
						pageBrkValue = i + 1;
					}
				} else {
					pageBrkValue = i + 1;
				}

				GlobalModel.pageBrkValueArr[i] = pageBrkValue + "";
				//Ensuring that the value is a string.
			}
			
			strPage += '</div>';
			$(objThis.options.objContentContainer).append(strPage);
			PageNavigationManager.adjustPageToScreen(GlobalModel.currentScalePercent);
			PageNavigationManager.addDomPageElements(objThis);
			$($('[type="PageContainer"]')[0]).css('visibility', 'visible');

			if (EPubConfig.pageView == AppConst.DOUBLE_PAGE_CAMEL_CASING) {
				$($('[type="PageContainer"]')[1]).css('visibility', 'visible');
			}

			objThis.options.totalContentHeight = $("#innerScrollContainer").height();
			var allArrElements = $(objThis.options.objContentContainer).find('[data-role]');
			if (GlobalModel.currentScalePercent.toFixed(3) == GlobalModel.actualScalePercent.toFixed(3) && (ismobile || EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage')) {
				objThis.scaleContentToFit(true);
			}

			$(objThis).trigger(objThis.events.PAGE_RENDER_COMPLETE, objThis.data);


			var iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
			if (!iPageIndex) {
				iPageIndex = 0;
			}
			this.managePageElementsCreation(iPageIndex);

			if ((EPubConfig.pageView == 'doublePage') && !(getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT)) {
				objThis.scaleContentToFit();
			}
			if ((getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT) && navigator.userAgent.match(/(ipad)/i)) {
				//objThis.member.previousLandscapeState = EPubConfig.pageView;
			}
			if ((getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT) && navigator.userAgent.match(/(android)/i)) {
				//objThis.member.previousLandscapeState = EPubConfig.pageView;
			}

			//If the page has changed from original view, then the transition won't be visible using this timeout.
			setTimeout(function() {
				$("#innerScrollContainer").css('visibility', 'visible');
			}, 100);
			objThis.updateDesignForPageView("pageLayoutOnOrientationChange");
			objThis.updateBarWidth(GlobalModel.currentScalePercent);
			$(document).bind('touchmove', function(e) {
				var stopTouchMove = false;
				if (EPubConfig.pageView.toLowerCase() != "scroll") {
					if (!(navigator.userAgent.match(/(ipad)/i))) {
						stopTouchMove = true;
					}
				}
				//log(stopTouchMove)
				if ((GlobalModel.currentScalePercent.toFixed(3) == GlobalModel.actualScalePercent.toFixed(3)) && (stopTouchMove == true)) {
					//log("here");
					if ($("#popupMaskDiv").length < 1 || $("#popupMaskDiv").css('display') == 'none') {
						e.preventDefault();
					}
				}
			});

			if (EPubConfig.PageNumberInBookmark_isAvailable == false) {
				$('.pageNumberBelowBookMark').css('display', 'none');
			}

			objThis.calculateAndPreserveImmutableValues();
		},
		
		/**
		 * @description : This function calculates and preserve all those values that will not change throughout the application
		 * This is done to avoid unnecessary calculation of these values.
		 */
		calculateAndPreserveImmutableValues : function ()
		{
			var _imageGalleryPanelQA = $( "#imageGalleryPanelQA" );
			
			this.immutableValues.scrollViewLeft = $( "#bookContentContainer" ).offset().left;
			this.immutableValues.annotationPanelWidth = parseInt( $( "#annotationPanel" ).outerWidth() );
			this.immutableValues.imageGalleryNext = _imageGalleryPanelQA.find( '.imageGalleryNext' ).width();
		},
		
		/**
		 * @description : This function displays and hides side navigation bars based on the device type.
		 * 
		 * This is called at application start and when page view is changed.( Both of them in this file )
		 */
		showHideSideNavigationBars : function ()
		{
			if ( EPubConfig.pageView == "singlePage" || EPubConfig.pageView == "doublePage" ) 
			{
				if( ( ismobile == null ) || ( ismobile && ( !EPubConfig.swipeNavEnabled ) ) )
				{
					$( '#prevPgBanner, #nextPgBanner' ).css( 'display', 'block' );
				}
				else
				{
					$( '#prevPgBanner, #nextPgBanner' ).css( 'display', 'none' );
				}
			}
			else 
			{
				$( '#prevPgBanner, #nextPgBanner' ).css( 'display', 'none' );
			}
		},

		scaleContentToFit : function(isAtStart, isViewChange) {
			//ensuring that value of scaleToFit is Boolean only
			if ((EPubConfig.scaleToFit != true && EPubConfig.scaleToFit != false) || $.isNumeric(EPubConfig.scaleToFit)) {
				throw new Error("Value of scaleToFit is not Boolean")
			}

			var objThis = this;
			if (EPubConfig.scaleToFit == true) {
				var navigationArrowsWidth = 45;
				if ($("#prevPgBanner").css('display') == 'block') {
					navigationArrowsWidth = $("#prevPgBanner").outerWidth();
				} else {
					 if (EPubConfig.swipeNavEnabled == true) {
				        navigationArrowsWidth = 5;
				       }else{
				        navigationArrowsWidth = 0;
				       }
				}

				var objParDiv = $("#innerScrollContainer").parent();
				var objDiv = $("#innerScrollContainer");

				var scaleFunc = "scale(1,1)";
				this.options.nScalePercent = 1;

				var cssObj = {};

				var nBookContentContainerWidth = $("#bookContentContainer2").width() - $("#annotationPanel").width();
				var nReqiredScale;
				if (EPubConfig.pageView == 'singlePage') {
					nBookContentContainerWidth = $(window).width() - 2 * navigationArrowsWidth - $("#annotationPanel").outerWidth();
					nReqiredScale = nBookContentContainerWidth / this.options.contentWidth;
					nReqiredScaleV = (window.height - (2 + topNavigationBarHeight)) / (this.options.contentHeight + 2);
					nReqiredScale = (nReqiredScale < nReqiredScaleV) ? nReqiredScale : nReqiredScaleV;
					//if required scale is less than minimum zoom scale, set the minimum zoom scale to currently calculated required scale;
					this.options.minZoom = (nReqiredScale < this.options.minZoom) ? nReqiredScale : this.options.minZoom;
					scaleFunc = "scale(" + nReqiredScale + "," + nReqiredScale + ")";
					this.options.nScalePercent = nReqiredScale;
					$("#innerScrollContainer").css({
						'width' : 'auto'
					});
					var currPageWidth = ($("[type='PageContainer']").width() + 2 * objThis.options.pagecompBorder) * nReqiredScale;
					var zoomedBookContainerWidth = currPageWidth * EPubConfig.totalPages;
					var bookContainerWidth = (objThis.options.contentWidth + 2 * objThis.options.pagecompBorder) * EPubConfig.totalPages;
					if (bookContainerWidth < zoomedBookContainerWidth) {
						$("#bookContentContainer2").width(currPageWidth * $("[type='PageContainer']").length);
					} else {
						$("#bookContentContainer2").width(bookContainerWidth);
					}
					$("#bookContentContainer").css("overflow", "auto");
					var currPageHeight = ($("[type='PageContainer']").height()) * nReqiredScale;
					$("#scrollMainContainer").width(($("[type='PageContainer']").width()) * nReqiredScale);
					$("#scrollMainContainer").height(Math.floor(currPageHeight));
					PageNavigationManager.adjustPageToScreen(nReqiredScale);
				} else if (EPubConfig.pageView == 'doublePage') {
					nBookContentContainerWidth = parseInt($(window).width()) - parseInt($("#annotationPanel").outerWidth()) - 2 * parseInt(navigationArrowsWidth);
					if (EPubConfig.ReaderType.toLowerCase() == 'prekto1')
						nReqiredScale = (nBookContentContainerWidth-20) / (2 * this.options.contentWidth + 3 * objThis.options.pagecompBorder + 2 * objThis.options.pageCompMarginLeft);
					else
						nReqiredScale = nBookContentContainerWidth / (2 * this.options.contentWidth + 3 * objThis.options.pagecompBorder + 2 * objThis.options.pageCompMarginLeft);
					nReqiredScaleV = (window.height - (4 + topNavigationBarHeight)) / (this.options.contentHeight);
					nReqiredScale = (nReqiredScale < nReqiredScaleV) ? nReqiredScale : nReqiredScaleV;
					//if required scale is less than minimum zoom scale, set the minimum zoom scale to currently calculated required scale;
					this.options.minZoom = (nReqiredScale < this.options.minZoom) ? nReqiredScale : this.options.minZoom;
					scaleFunc = "scale(" + nReqiredScale + "," + nReqiredScale + ")";
					this.options.nScalePercent = nReqiredScale;
					$("#innerScrollContainer").css({
						'width' : 'auto'
					});
					var currPageWidth = ($("[type='PageContainer']").width() + 2 * objThis.options.pagecompBorder) * nReqiredScale;
					var zoomedBookContainerWidth = currPageWidth * EPubConfig.totalPages;
					var bookContainerWidth = (objThis.options.contentWidth + 2 * objThis.options.pagecompBorder) * EPubConfig.totalPages;
					if (bookContainerWidth < zoomedBookContainerWidth) {
						$("#bookContentContainer2").width(currPageWidth * $("[type='PageContainer']").length);
					} else {
						$("#bookContentContainer2").width(bookContainerWidth);
					}
					$("#bookContentContainer2").width($("[type='PageContainer']").outerWidth() * ($("[type='PageContainer']").length + 2));
					var currPageHeight = (this.options.contentHeight) * nReqiredScale + 2 * objThis.options.pagecompBorder;
					var scrollPageWidth = (2 * $("[type='PageContainer']").width()) * nReqiredScale;
					$("#scrollMainContainer").width(2 * ($("[type='PageContainer']").width()) * nReqiredScale);
					$("#scrollMainContainer").height(currPageHeight);
					$("#bookContentContainer").css("overflow", "hidden");
					// to enable zoom in 2 page view
					PageNavigationManager.adjustPageToScreen(nReqiredScale);
				} else {
					nBookContentContainerWidth = parseInt($(window).width()) - parseInt($("#scrollMainContainer").offset().left);
					nReqiredScale = nBookContentContainerWidth / (this.options.contentWidth + 2 * objThis.options.pagecompBorder);
					//if required scale is less than minimum zoom scale, set the minimum zoom scale to currently calculated required scale;
					this.options.minZoom = (nReqiredScale < this.options.minZoom) ? nReqiredScale : this.options.minZoom;
					scaleFunc = "scale(" + nReqiredScale + "," + nReqiredScale + ")";
					this.options.nScalePercent = nReqiredScale;
					if (this.options.nScalePercent <= 1) {
						$("#innerScrollContainer").height(this.options.totalContentHeight * this.options.nScalePercent);
					}
					var currPageWidth = ($("[type='PageContainer']").width() + 2 * objThis.options.pagecompBorder) * this.options.nScalePercent;
					//$("#bookContentContainer").width(currPageWidth);
					PageNavigationManager.adjustPageToScreen(nReqiredScale);
					$("#bookContentContainer").css("overflow", "auto");
					var objScrollElement = $(this.element).find('[id=bookContentContainer2]');
					//log("this.member.elementScrollToppos in scaleToFit "+ this.member.elementScrollToppos);
					//log("objThis.options.nScalePercent / GlobalModel.actualScalePercent: "+objThis.options.nScalePercent + "  " +  GlobalModel.actualScalePercent);
					//log(objThis.member.onOrientationChange)
					//if (objThis.member.onOrientationChange == false) {
					if (isViewChange != true) {
						var scrollToVal = this.member.elementScrollToppos * objThis.options.nScalePercent / GlobalModel.actualScalePercent;
						//log("scrollToVal: "+scrollToVal);
						setTimeout(function() {
							$(objScrollElement).scrollTop(scrollToVal);
						}, 0);
					}
					//}
					//log("scrollToVal: "+scrollToVal);
					//log("$(objScrollElement).scrollTop() in scaleToFit "+ $(objScrollElement).scrollTop());
					//log("scrollToVal: "+scrollToVal);
				}
				this.updateDesignForPageView("panelPosition", this.options.nScalePercent);
				cssObj["-ms-transform"] = scaleFunc;
				cssObj["-moz-transform"] = scaleFunc;
				cssObj["-webkit-transform"] = scaleFunc;
				cssObj["-o-transform"] = scaleFunc;
				cssObj["transform"] = scaleFunc;
				cssObj["-ms-transform-origin"] = "0% 0%";
				cssObj["-moz-transform-origin"] = "0% 0%";
				cssObj["-webkit-transform-origin"] = "0% 0%";
				cssObj["-o-transform-origin"] = "0% 0%";
				cssObj["transform-origin"] = "0% 0%";
				GlobalModel.currentScalePercent = this.options.nScalePercent;
				this.updateBarWidth(nReqiredScale);
				// this.updateScrollBtnPos(nReqiredScale);
				$("#innerScrollContainer").css(cssObj);
				if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 && !ismobile)
				{
					$("#bookContentContainer2").css("-webkit-transform","translateZ(0)");					
				}
				GlobalModel.actualScalePercent = this.options.nScalePercent;
				objThis.rePositionEbookPopups();
			}
			if (EPubConfig.pageView.toUpperCase() == "DOUBLEPAGE") {
				var sliderElem = $("#rangeSlider");
				sliderElem.slider('disable');
			}
		},

		rePositionEbookPopups : function() {
			var objThis = this;
			/* Start : Modlog_27052013 : Changes done for MREAD-80*/
			//objThis.repositionPopUps('bookMarkPanel');
			objThis.repositionPopUps('stickyNoteContainer');
			objThis.repositionPopUps('glossaryPanel');
			objThis.repositionPopUps('highlightContainer');
			objThis.repositionPopUps('footNotePanel');
			objThis.repositionPopUps('dltConfirmPanel');
			if ($("#questionAnswerPrintPanel").css('display') != 'none') {
				var annotationPanelWidth = $("#bookContentContainer").offset().left + ($("#bookContentContainer").width() - $("#questionAnswerPrintPanel").width()) / 2;
				$("#questionAnswerPrintPanel").css({
					'position' : 'absolute',
					'bottom' : "0px",
					'top' : "auto",
					'left' : annotationPanelWidth + "px"
				});
			}

			if (navigator.userAgent.match(/(android)/i) && objThis.options.rangedata) {
				if (objThis.options.repositioningTimer) {
					clearTimeout(objThis.options.repositioningTimer);
				}
				objThis.options.repositioningTimer = setTimeout(function() {
					$(objThis).trigger(objThis.events.SELECT_TEXT_END, objThis.data)
				}, 1500)
			}
		},

		/* Start : Modlog_27052013 Aastha for refractoring of code*/
		repositionPopUps : function(id) {
			var objThis = this;
			try {
				if (document.getElementById(id) != null) {
					if ($("#" + id).css("display") == "block" || objThis.options.repositioningPopup == "#" + id) {
						if (((parseInt($("#" + id).offset().top) + parseInt($("#" + id).height())) > parseInt($(window).height())) || (parseInt($("#" + id).offset().top) < 0)) {
							if (EPubConfig.pageView == 'scroll' || EPubConfig.pageView == 'SCROLL') {
								var launcher = PopupManager.objLauncherHotspot;
								var bkContentContainer2scrollTop = $("#bookContentContainer2").scrollTop();
								var launcherOffsetTop = $(launcher.element).offset();
								if (!(launcherOffsetTop)) {
									launcherOffsetTop = $(launcher).offset();
								}
								//alert('***');
								setTimeout(function() {
									var navigationBarHeight = $("#navigationBar").height() + 10;
									$("#bookContentContainer2").scrollTop(launcherOffsetTop.top + bkContentContainer2scrollTop - navigationBarHeight);
									PopupManager.positionPopup(true);
								}, 0);

							} else {
								PopupManager.positionPopup(true);
							}
						} else {
							PopupManager.positionPopup(true);
						}
					}
				}
			} catch(e) {
				PopupManager.removePopup();
			}
		},
		/* End : Modlog_27052013 Aastha for refractoring of code*/
		/**
		 * Binding select text end event on page text selection
		 * @param1 none
		 * @param none
		 * @return none
		 */
		getSelectioninfo : function() {
			var objThis = this;

			var objScrollElement = $(this.element).find('[id=bookContentContainer2]');
			this.options.objContentContainer = objScrollElement;
			this.options.objScrollContainer = objScrollElement;

			var iNumAnnotatioToolsUsed = 0;
			for (var strComponents in EPubConfig.AnnoToolNames) {
				var strTool = EPubConfig.AnnoToolNames[strComponents];

				//display annotation tool in tool-box only if it is required functionality as per configuration;
				if (strTool && EPubConfig.RequiredApplicationComponents.indexOf(strComponents) > -1) {
					$("#" + strTool).css('display', 'block');
					$("." + strTool).css('display', 'block');
					iNumAnnotatioToolsUsed++;
				}
			}

			var fnRemoveAllRanges = function() {
				if (!ismobile && !isWinRT ) 
				{
					$(objThis).trigger('closeOpenedPanel');
					var selcheck = window.getSelection() + '';
					if (selcheck.length != 0) {// Allows double click selection on IE 10 / Win 8.
						window.getSelection().removeAllRanges();
					}
				} 
				else
				{
					
					if (objThis.options.rangedata != "") {
						document.onselectionchange = null;
						if(!navigator.userAgent.match(/OS 8_0/))
						{
							$("body").append('<input type="text" id="inputToRemove" style="width:0;height:0" readonly/>');
							$("#inputToRemove").focus();
							$("#inputToRemove").blur();
							$("#inputToRemove").remove();
						}
					} else {
						if (document.onselectionchange == null) {
							document.onselectionchange = fnOnSelectionEndMobile;
						}
					}
					$(objThis).trigger('closeOpenedPanel');
					PopupManager.removePopup('annotation-bar');
	
			    }
			    
				if($('body').find('.search_highlight').length >0){
					$('body').removeHighlight();
				}	
				objThis.options.rangedata = "";

			}
			var fnOnSelectionEnd = function(e) {
				
				var selected = "";
				var range = "";

				/**
				 *  Displays which all tools should be visible according to config file.
				 *  This is called here because, in case of image selection the highlight tool gets hidden, so every tool has to be made visible again
				 *  when next selection is made.
				 */
				$(objThis).trigger('closeOpenedPanel');
				for (var strComponents in EPubConfig.AnnoToolNames) {
					var strTool = EPubConfig.AnnoToolNames[strComponents];

					//display annotation tool in tool-box only if it is required functionality as per configuration;
					if (strTool && EPubConfig.RequiredApplicationComponents.indexOf(strComponents) > -1) {
						$("#" + strTool).css('display', 'block');
						$("." + strTool).css('display', 'block');
						iNumAnnotatioToolsUsed++;
					}
				}
				if ( typeof window.getSelection != "undefined") {
					/**
					 * selcheck is a string of current selection.
					 */
					var selcheck = window.getSelection() + '';
					var sel = window.getSelection();

					if (selcheck.length != 0) {
						for (var i = 0, len = sel.rangeCount; i < len; ++i) {
							range = sel.getRangeAt(i);
							//                            var txt = document.createElement('div');
							//                            txt.appendChild(range.cloneContents());
						}
						objThis.options.rangedata = range;
						//$(range.commomAncestorContainer).wrap('<a id="tempLink"></a>');
						var isandroid = navigator.userAgent.match(/(android)/i);
						var containerSet = 'PageContainer_' + objThis.options.pageIndexForAnnotation;
						var preSelectionRange = range.cloneRange();
						preSelectionRange.selectNodeContents($('[id="'+containerSet+'"]').find('#mainContent')[0]);
						preSelectionRange.setEnd(range.startContainer, range.startOffset);
						var start = preSelectionRange.toString().length;
						var startOffset = start;
						var endOffset = start + range.toString().length

						objThis.options.rangeObject = "<Container><startContainer>" + containerSet + "</startContainer><endContainer>" + containerSet + "</endContainer><startOffset>" + startOffset + "</startOffset><endOffset>" + endOffset + "</endOffset></Container>";
						var selectionrange = objThis.options.rangedata;
						startRange = document.createRange();
						if( isWinRT )
						{
							document.onselectionchange = null;
						}
						startRange.setStart(selectionrange.startContainer, selectionrange.startOffset);
						if ( ismobile && (!isandroid) ) {
							$("#annotation-bar").css("display", "none");
							window.getSelection().removeAllRanges();
							window.getSelection().addRange(startRange);
						}

						var $start = $("<span/>");
						// Restricted for android. Causing error on android. Reason still to be investigated.
						if (!navigator.userAgent.match(/(android)/i)) {
							startRange.insertNode($start[0]);
						}
						//                  objThis.options.Icontop = $start.offset().top;
						objThis.options.Icontop = (selectionrange.getBoundingClientRect().top) - 67 + 56;
						if (navigator.userAgent.match(/(MSIE 10.0)/i) || isWinRT || $.browser.version == 11) {
							objThis.options.Icontop = $start.offset().top - 67 + 56;
						}
						$start.remove();
						newRange = document.createRange();
						newRange.setStart(selectionrange.endContainer, selectionrange.endOffset);
						
						var $span = $("<span/>");
						newRange.insertNode($span[0]);
						objThis.options.toppos = $span.offset().top;
						objThis.options.Iconleft = $span.offset().left;
						if (navigator.userAgent.match(/(android)/i)) {
							objThis.options.Icontop = objThis.options.toppos;
						}
						var leftpos = $span.offset().left;
						$span.remove();

						
						if (EPubConfig.ReaderType == '2TO5') {
							$('#anno-sep').css('display', 'block');
							$('.colorContainer').css('display', 'block');
						}

						if (ismobile) {
							if (objThis.options.selectionTimer) {
								clearTimeout(objThis.options.selectionTimer);
							}
							objThis.options.selectionTimer = setTimeout(function() {
								$(objThis).trigger(objThis.events.SELECT_TEXT_END, objThis.data);
							}, 0);

						} else {
							$(objThis).trigger(objThis.events.SELECT_TEXT_END, objThis.data)
						}
												
						if( document.onselectionchange == null && isWinRT )
						{
							document.onselectionchange = fnOnSelectionEndMobile;
						} 
						
					} else {
						if ($("#annotation-bar").css("display") == "block") {
							if (!navigator.userAgent.match(/(android)/i)) {

								if (!ismobile) {
									PopupManager.removePopup('annotation-bar');
								}
							}
						}

					}

				}
			}
			

			var fnOnSelectionEndMobile = function(e) {
				
				if ($('#stickyNoteContainer').css('display') == 'block') {
					return;
				}
				
				if( isWinRT && window.MSPointerEvent )// IE10 win RT
				{
					// do nothing
				}
				else
				{
					e.preventDefault();
				}
				$("#annotation-bar").css("display", "none");
				if (objThis.options.mobileSelectionTimer) {
					clearTimeout(objThis.options.mobileSelectionTimer);
				}
				objThis.options.mobileSelectionTimer = setTimeout(function() {
					if ( typeof window.getSelection != "undefined") {
						/**
						 * selcheck is a string which consists of current window selection
						 * str2 containers the string which is the whole content of page container
						 * if current selection and whole page selection is equal, nothing will happen.
						 * This function then calls fnOnSelectionEnd
						 */
						var selcheck = window.getSelection() + '';
						var sel = window.getSelection();
						var str1 = window.getSelection().toString();
						var containerSet = 'PageContainer_' + objThis.options.pageIndexForAnnotation;
						var mc = $("#"+containerSet).find('#mainContent')[0];
						var str2 = $(mc).text();

						var objReg = /[\s]*/gi;
						str1 = str1.replace(objReg, "");
						str2 = str2.replace(objReg, "");
						if (str1.length == str2.length) {
							objThis.options.rangedata = " ";
							return;
						}
						if (selcheck.length != 0) {
							var curRange = sel.getRangeAt(0);
							var oldRange = objThis.options.rangedata;
							if (oldRange) {
								if (oldRange.toString() == curRange.toString()) {
									// return;
									$("#annotation-bar").css("display", "block");
								} else {

								}
							}
							$("#annotation-bar").css("display", "none");
							fnOnSelectionEnd(e);
						}
					}
				}, 1000);
			}
			var msGesture;
			var eventListener = function(evt)
			{
				if (evt.type == "pointerdown"  || evt.type == "pointerup")
			    {
			        msGesture.addPointer(evt.pointerId);
			    }
			    
			    if( evt.pointerType == "touch" || evt.pointerType == 2 )
			    {
			    	WinRTPointerType = "touch";
			    }
			    else
			    {
			    	WinRTPointerType = "non-touch";	
			    }
		    	fnOnSelectionEndMobile(evt);
			}
			
			var msGesture1;
			var eventListener1 = function(evt1)
			{
				if (evt1.type == "pointerup")
			    {
			        msGesture1.addPointer(evt1.pointerId);
			    }
			    
			    if( evt1.pointerType == "touch" || evt1.pointerType == 2 )
			    {
			    	WinRTPointerType = "touch";
			    }
			    else
			    {
			    	WinRTPointerType = "non-touch";	
			    }
		    	fnOnSelectionEndMobile(evt1);
			}
				
				
			
			/**
			 * This function is called at the beginning from initializeView.
			 * It binds events for further processing when selection is done.
			 * fnRemoveAllRanges is called to remove already created selection when bookContentConatiner2 is tapped or clicked
			 */
			if (!ismobile && !(navigator.userAgent.match(/(Tablet)/i))) {
				$(objScrollElement).bind("mousedown", fnRemoveAllRanges);
				$("#bookContentContainer").bind("mouseup", fnOnSelectionEnd);
			} else {
				if( isWinRT )
				{
					msGesture = new MSGesture();
					msGesture.target = document.getElementById("bookContentContainer2");
					
					msGesture1 = new MSGesture();
					msGesture1.target = document.getElementById("annotation-bar");
					
				    var target = document.getElementById("bookContentContainer2");
				    var target1 = document.getElementById("annotation-bar");
				    
				    target.addEventListener(WinRTPointerEventType, fnRemoveAllRanges);
				    target.addEventListener("MSGestureTap", eventListener);
				    target.addEventListener(WinRTPointerEventType_Up, eventListener);
				    target1.addEventListener(WinRTPointerEventType_Up, eventListener1);
				    target.addEventListener(WinRTPointerEventType, eventListener);
				}	
				else
				{
					$(objScrollElement).bind("touchstart", fnRemoveAllRanges);
					$(objScrollElement).bind("taphold", fnOnSelectionEndMobile);
				}			
				

			}

		},
		
		eventListener : function( evt ){
			
		},

		/**
		 * Returns current Range data value
		 * @param1 none
		 * @param none
		 * @return rangedata option value
		 */
		getRangeContent : function() {

			return this.options.rangedata;
		},

		/**
		 * Returns data required for notes creation
		 * @param none
		 * @return noteInfo option value
		 */
		getInfoForSkickyNotes : function() {
			return this.options.noteInfo;
		},

		/**
		 * set data required for notes creation
		 * @param Object noteInfo
		 * @return none
		 */
		setInfoForSkickyNotes : function(noteInfo) {
			this.options.noteInfo = noteInfo;
		},
		getTextToCopy : function() {
			var strSelectedText = this.options.rangedata.toString();

			if (strSelectedText == null)
				return '';

			return strSelectedText;
		},

		/**
		 * Verify Selection Range for more than one page
		 * @param1 selection range object
		 * @param none
		 * @return boolean
		 */
		verifySelection : function(selectionrange) {
			var txt = document.createElement('div');
			try {
				txt.appendChild(selectionrange.cloneContents());
			} catch(e) {
			}
			if ($(txt).find('[type=PageContainer]').length == 0) {
				return false;
			} else {
				return true;
			}

		},
		/**
		 * This function is the custom containsNode method.
		 * Since generic containsNode method is not supported in IE, we need to create this method via using
		 * compareBoundaryPoints method.
		 * @param1 html element
		 * @param2 boolean to ensure wheather to include partially selected element.
		 * @return Boolean
		 */
		myContainsNode : function(el, bAllowPartialSelection) {
			var selectionrange = this.options.rangedata;
			var elRange = document.createRange();
			elRange.selectNodeContents(el);
			var startPoints = selectionrange.compareBoundaryPoints(Range.START_TO_START, elRange);
			var endPoints = selectionrange.compareBoundaryPoints(Range.END_TO_END, elRange);
			if (startPoints <= 0 && endPoints >= 0) {
				return true;
			}

			if (startPoints < 0) {
				if (endPoints < 0) {
					//alert ("The selection is before the bold text but intersects it.");
					if (!bAllowPartialSelection) {
						return false
					}
					return true
				} else {
					//alert ("The selection contains the bold text.");
					return true;
				}
			} else {
				if (endPoints > 0) {
					//alert ("The selection is after the bold text but intersects it.");
					if (!bAllowPartialSelection) {
						return false
					}
					return true;
				} else {
					if (startPoints == 0 && endPoints == 0) {
						//alert ("The selected text and the bold text are the same.");
						return true;
					} else {
						//alert ("The selection is inside the bold text.");
						if (!bAllowPartialSelection) {
							return false
						}
						return true;
					}
				}
			}

			return false;
		},
		/**
		 * Find out selected text nodes.
		 * @param1 elements of commonAncestorContainer
		 * @param2 object containing array.
		 * @return recursive calls fills up the array confined in passed object.
		 */
		getSelectedTextNodes : function(el, obj) {
			var objThis = this;
			var selection = window.getSelection();
			if ($(el).context.nodeType == 3) {
				if (objThis.myContainsNode(el, false)) {
					obj.arrList.push(el);
				}

				return;
			} else {
				var arr = $(el).contents();
				for (var i = 0; i < arr.length; i++) {
					var el = arr[i];
					if (objThis.myContainsNode(el, true)) {
						objThis.getSelectedTextNodes(el, obj);
					}
				}
			}

		},

		/**
		 * Find out selected text nodes.
		 * @param1 elements of commonAncestorContainer
		 * @param2 object containing array.
		 * @return recursive calls fills up the array confined in passed object.
		 */
		getValidEndContainerForSelection : function(selectionrange) {

			var objThis = this;
			var selection = window.getSelection();
			//closing the popup for highlighting or stickynotes removes the selection.So here we have to select text again.
			if ((selection.rangeCount == 0) && !ismobile) {
				selection.removeAllRanges();
				selection.addRange(selectionrange);
			}

			var commonAncestorCont = selectionrange.commonAncestorContainer;
			while (commonAncestorCont.nodeType == 3) {
				commonAncestorCont = $(commonAncestorCont).parent()[0];
			}
			var allWithinRangeParent = $(commonAncestorCont).contents();
			var strSelected = selectionrange.toString();
			var allSelected = [];
			for (var i = 0, el; el = allWithinRangeParent[i]; i++) {
				// The second parameter says to include the element
				// even if it's not fully selected
				if (objThis.myContainsNode(el, true)) {
					objThis.getSelectedTextNodes(el, {
						arrList : allSelected
					});
				}
			}

			var strText = "";
			var strSelTemp = "";
			var objReg = /[\s]*/gi;
			strSelTemp = strSelected.replace(objReg, "");

			var ec;
			var arrValid = [];
			for (var i = 0; i < allSelected.length; i++) {
				ec = allSelected[i];
				if ($(ec).context.nodeValue.replace(objReg, "").length > 0) {
					arrValid.push(ec);
				}
				strText += $(ec).context.nodeValue.replace(objReg, "");
				if (strText.toLowerCase().indexOf(strSelTemp.toLowerCase()) > -1) {
					break;
				}
			}
			return {
				eCont : ec,
				arrValidEcs : arrValid
			};

		},

		/**
		 * Set position of annotation popup
		 * @param1 popupbar
		 * @param none
		 * @return none
		 */
		setAnnotationPopup : function(popupbar) {
		    if( $( popupbar ).css( "display" ) != "block" )
		    {
		        return;
		    }
			var objThis = this;
			var selectionrange = this.options.rangedata;
			var time = 0;
			if (ismobile || ( isWinRT && WinRTPointerType == "touch" )) {
				$(popupbar).css({
					'visibility' : "hidden"
				});
				var selectionTop = selectionrange.getBoundingClientRect().top;
				var selectionHeight = selectionrange.getBoundingClientRect().bottom - selectionrange.getBoundingClientRect().top
				var diff = 200//($(window).height() - selectionHeight - $(popupbar).height() -15)/2;
				
				if(  isWinRT && WinRTPointerType == "touch" )
				{
					// do nothing
				}
				else
				{
					if ((selectionTop > parseInt($(window).height())) || (selectionTop < 0)) {
					if (EPubConfig.pageView == 'scroll' || EPubConfig.pageView == 'SCROLL') {
						var bkContentContainer2scrollTop = $("#bookContentContainer2").scrollTop();
						$("#bookContentContainer2").scrollTop(selectionTop + bkContentContainer2scrollTop - 200);
						time = 1000;
					}
					}
				}
				
				var currPage = $("[id='PageContainer_" + objThis.options.pageIndexForAnnotation + "']");
				var acceptableWidth = (currPage.width() + 2 * objThis.options.pagecompBorder) * this.options.nScalePercent;
				var nPopupbarWidth = $(popupbar).width();
				var nTop = 15;
				var nLeft = (currPage.offset().left) + (acceptableWidth - nPopupbarWidth) / 2;
				if (acceptableWidth > window.innerWidth) {
					if (currPage.offset().left >= 0) {
						acceptableWidth = window.innerWidth - (currPage.offset().left);
						nLeft = (currPage.offset().left) + (acceptableWidth - nPopupbarWidth) / 2;
					} else {
						acceptableWidth = window.innerWidth;
						nLeft = (acceptableWidth - nPopupbarWidth) / 2;
					}

				}
				//var currPageWidth = (currPage.width() + 2 * objThis.options.pagecompBorder) * this.options.nScalePercent;
				$(popupbar).css({
					'top' : nTop + 'px',
					'left' : nLeft + 'px',
					'width' : nPopupbarWidth + "px"
				});
				$(".anno-triangle").css('display', 'none');
				$(popupbar).unbind('touchstart').bind('touchstart', function(e) {
					e.preventDefault();
				});

				setTimeout(function() {
					startRange = document.createRange();
					startRange.setStart(selectionrange.startContainer, selectionrange.startOffset);
					var $start = $("<span/>");
					// Restricted for android. Causing error on android. Reason still to be investigated.
					if (!navigator.userAgent.match(/(android)/i)) {
						startRange.insertNode($start[0]);
					}
					//                  objThis.options.Icontop = $start.offset().top;
					objThis.options.Icontop = (selectionrange.getBoundingClientRect().top) - 67 + 56;
					if (navigator.userAgent.match(/(MSIE 10.0)/i) || isWinRT || $.browser.version == 11) {
						objThis.options.Icontop = $start.offset().top - 67 + 56;
					}
					$start.remove();
					$(popupbar).css({
						'visibility' : "visible"
					});
					$(popupbar).show();

				}, time);

				return;
			}

			startRange = document.createRange();
			startRange.setStart(selectionrange.startContainer, selectionrange.startOffset);
			var $start = $("<span/>");
			// Restricted for android. Causing error on android. Reason still to be investigated.
			if (!navigator.userAgent.match(/(android)/i)) {
				startRange.insertNode($start[0]);
			}
			//                  objThis.options.Icontop = $start.offset().top;
			objThis.options.Icontop = (selectionrange.getBoundingClientRect().top) - 67 + 56;
			if (navigator.userAgent.match(/(MSIE 10.0)/i) || isWinRT || $.browser.version == 11) {
				objThis.options.Icontop = $start.offset().top - 67 + 56;
			}
			$start.remove();

			////////////////// Begin Off Changes made for positioning and appearance of annotation popup.//////////////////////////

			var obj = objThis.getValidEndContainerForSelection(selectionrange);
			var endCont = obj.eCont;
			var arrValid = obj.arrValidEcs;
			var $span = $("<span/>");
			if (endCont == undefined) {
				newRange = document.createRange();
				newRange.setStart(selectionrange.endContainer, selectionrange.endOffset);
				newRange.insertNode($span[0]);
			} else {
				var objReg = /[\n]/g;
				var nodeVal = $(endCont).context.nodeValue;
				if (objReg.test(nodeVal.substr(nodeVal.length - 1))) {
					endCont.nodeValue = nodeVal.trim();
					var nlChar = document.createTextNode("\n");
					$(endCont).after(nlChar);
					setTimeout(function() {

						var range = document.createRange();
						for (var n = 0; n < arrValid.length; n++) {
							range.selectNodeContents(arrValid[n]);
						}

						var sel = window.getSelection();
						sel.removeAllRanges();
						sel.addRange(range);
						objThis.options.rangedata = range;
					}, 10);
				}
				$(endCont).after($span);
			}
			////////////////// End Off Changes made for positioning and appearance of annotation popup.//////////////////////////
			objThis.options.toppos = $span.offset().top;
			objThis.options.Iconleft = $span.offset().left;
			if (navigator.userAgent.match(/(android)/i)) {
				objThis.options.Icontop = objThis.options.toppos;
			}
			var leftpos = $span.offset().left;
			$span.remove();

			var totalPadding = parseInt($(popupbar).css('padding-top')) + parseInt($(popupbar).css('padding-bottom'));
			var totalPadding = 0;
			//parseInt($(popupbar).css('padding-top')) + parseInt($(popupbar).css('padding-bottom'));
			//console.log();
			var nPopupbarHeight = $(popupbar).height();
			var nPopupbarWidth = $(popupbar).width();
			var nPopupbarOuterWidth = $(popupbar).outerWidth();
			var nWindowHeight = window.height;
			var nWindowWidth = $(window).width();
			var nWindowInnerWidth = window.innerWidth;

			if (GlobalModel.currentScalePercent / GlobalModel.actualScalePercent > 1) {
				var appliedTop = ((objThis.options.toppos - (nPopupbarHeight + totalPadding) + 10 * (GlobalModel.currentScalePercent / GlobalModel.actualScalePercent) + 5 ));
			} else {
				var appliedTop = objThis.options.toppos - (nPopupbarHeight + totalPadding);
			}
			if (appliedTop < 0) {
				appliedTop = 0;
			} else if (appliedTop > (nWindowHeight - nPopupbarHeight)) {
				appliedTop = nWindowHeight - nPopupbarHeight - 10;
			}

			var nLeftPos = ((leftpos) - (nPopupbarWidth / 2) - 6);
			if (nLeftPos + nPopupbarWidth > nWindowInnerWidth) {
				nLeftPos -= (nLeftPos + nPopupbarWidth) - nWindowInnerWidth;
			}
			// Popup position changed for android to stop overlapping of selection strip and annotation popup
			if (navigator.userAgent.match(/(android)/i)) {
				nLeftPos = nLeftPos - 45;
			}

			var scrollerWidth = 20;
			var navigationPanelWidth = 0;
			var annotationPanelWidth = $("#annotationPanel").width();
			if ($("#prevPgBanner").css('display') != 'none') {
				navigationPanelWidth = $("#prevPgBanner").width();
			}

			var minAvailLeft = annotationPanelWidth + navigationPanelWidth;
			var maxAvailLeft = nWindowWidth - nPopupbarWidth - navigationPanelWidth - scrollerWidth;

			if (nLeftPos < minAvailLeft) {
				nLeftPos = minAvailLeft;
			}

			if (nLeftPos > maxAvailLeft) {
				nLeftPos = maxAvailLeft;
			}

			if (!ismobile) {
				$(popupbar).css({
					'top' : appliedTop + 'px',
					'left' : nLeftPos + 'px',
					'width' : nPopupbarWidth + "px"
				});
				var triangleArraowTopPos = $(".annotation-div").height() + totalPadding - 1;
				var nArrowLeftPos = ($(".annotation-div").width() / 2) - 10;
				if (nArrowLeftPos < $span.offset().left) {
					nArrowLeftPos = $span.offset().left;
				}

				$(".anno-triangle").css('left', nArrowLeftPos);
				$(".anno-triangle").css('top', triangleArraowTopPos + 'px');

				if (navigator.userAgent.indexOf("Firefox") != -1) {
					objThis.options.Icontop = objThis.options.toppos - 67 + 56;
				}
				$(".anno-triangle").css('display', 'block');
			} else {
				$(popupbar).css({
					'top' : (appliedTop + 70) + 'px',
					'left' : nLeftPos + 'px',
					'width' : nPopupbarWidth + "px"
				});
				$(".anno-triangle").css('display', 'none');
			}

			if (navigator.userAgent.indexOf("Firefox") != -1) {
				objThis.options.Icontop = objThis.options.toppos - 67 + 56;
			}
			if (ismobile) {
				$(popupbar).unbind('touchstart').bind('touchstart', function(e) {
					e.preventDefault();
				});
			}
			//$(popupbar).show();

		},
		managePageElementsCreation : function(nIndex) {
			var objThis = this;
			PageNavigationManager.manageDomPageElements(nIndex, objThis);
			var nPagenum = PageNavigationManager.newPageIndex;
			GlobalModel.currentPageIndex = nPagenum;
			//doPageLoadingTimer = setTimeout(function() {
			if( isPageViewChange || lastDopageLoadingIndex != nIndex)
			{
				lastDopageLoadingIndex = nIndex;
				objThis.doPageLoading(nPagenum);
				isPageViewChange = false;
			}	
			//}, doPageLoadingTime)
			if (objThis.member.isOrientationChange == false) {
				$(objThis).trigger(objThis.events.PAGE_INDEX_CHANGE, objThis.data);
			}

		},
		doPageLoading : function(nIndex) {
			var objThis = this;
			if (stopPageLoading) {
				clearTimeout(stopPageLoading);
			}
			stopPageLoading = setTimeout(function() {

				var arrElements = objThis.options.arrPageElements;
				var nPagenum = parseInt(nIndex);
				if (nPagenum < 0 || nPagenum > (arrElements.length - 1)) {
					return;
				}

				var nIndexInPageIndex = parseInt($($('[type=PageContainer]')[nPagenum]).attr('pagesequence'));
				// var pageNumValue = parseInt($($('[type=PageContainer]')[nPagenum]).attr('pagesequence'));
				if (!nIndexInPageIndex) {
					nIndexInPageIndex = 0;
				}
				var objRole = objThis.getDataRole(arrElements[nPagenum]);
				if (objRole != undefined && (objThis.member.arrPageNames[nIndexInPageIndex] != "BlankPage")) {
					//log(objThis.member.arrPageNames[nIndexInPageIndex])
					objRole.loadPage(objThis.member.arrPageNames[nIndexInPageIndex]);
				}
				var nPagesToPrecache = (EPubConfig.pageView.toLowerCase() == "singlepage") ? EPubConfig.pagesToPrecache : (parseInt(EPubConfig.pagesToPrecache) * 2);
				var nPrecacheCounter = 1;
				if (EPubConfig.pagesToPrecache) {
					if (EPubConfig.pagesToPrecache > 0) {
						for (var i = 0; i < nPagesToPrecache; i++) {
							if ((nPagenum + nPrecacheCounter) < arrElements.length) {
								objRole = objThis.getDataRole(arrElements[nPagenum + nPrecacheCounter]);
								var arrPageName = objThis.member.arrPageNames[nIndexInPageIndex + nPrecacheCounter];
								if (objRole != undefined && (arrPageName != "BlankPage") && (arrPageName != undefined)) {
									//log("==> " + objThis.member.arrPageNames[nIndexInPageIndex + nPrecacheCounter]);
									objRole.loadPage(objThis.member.arrPageNames[nIndexInPageIndex + nPrecacheCounter]);

								}
							}
							nPrecacheCounter++;

						}
					}
				}
				$(document).trigger('pagechange');

			}, 0);
			//log("nIndex: "+nIndex);

		},

		checkForDualPageSpread : function(nIndex) {
			try {
				var objThis = this;
				nIndex = parseInt(nIndex);
				var indexForPagebrk = nIndex;
				var pageSeqValue = parseInt($($('[type="PageContainer"]')[nIndex]).attr('pagesequence'));
				var dualPageSpreadForCurrentPage = false;
				var dualPageSpreadForPreviousPage = false;
				var dualPageSpreadForNextPage = false;
				var pageBrkVal = $($('[type="PageContainer"]')[nIndex]).attr('pagebreakvalue');
				var nxtpageBrkVal = $($('[type="PageContainer"]')[nIndex + 1]).attr('pagebreakvalue');
				var prevpageBrkVal = nIndex > 0 ? $($('[type="PageContainer"]')[nIndex - 1]).attr('pagebreakvalue') : false;
				dualPageSpreadForCurrentPage = GlobalModel.pageDataByPageBreakValue["pagebreakid_" + pageBrkVal].HasDualPageSpread;
				switch(EPubConfig.pageView) {
					case "doublePage":
						// This if means that the page is at the right side of the book.
						if (pageSeqValue % 2 != 0) {
							dualPageSpreadForCurrentPage = GlobalModel.pageDataByPageBreakValue["pagebreakid_" + pageBrkVal].HasDualPageSpread;
							if (prevpageBrkVal && GlobalModel.pageDataByPageBreakValue["pagebreakid_" + prevpageBrkVal]) {
								dualPageSpreadForPreviousPage = GlobalModel.pageDataByPageBreakValue["pagebreakid_" + prevpageBrkVal].HasDualPageSpread;
							}

							indexForPagebrk = indexForPagebrk - 1;
						}
						break;

					case "singlePage":
						// This means that this page will be at the right side in double page view.
						if (pageSeqValue % 2 == 0) {
							indexForPagebrk = indexForPagebrk - 1;
							if (GlobalModel.pageDataByPageBreakValue["pagebreakid_" + pageBrkVal]) {
								dualPageSpreadForCurrentPage = GlobalModel.pageDataByPageBreakValue["pagebreakid_" + pageBrkVal].HasDualPageSpread;
							}

							if (prevpageBrkVal && GlobalModel.pageDataByPageBreakValue["pagebreakid_" + prevpageBrkVal]) {
								dualPageSpreadForPreviousPage = GlobalModel.pageDataByPageBreakValue["pagebreakid_" + prevpageBrkVal].HasDualPageSpread;
							}
						}
						break;
				}
				// If the property is present in the current page or the previous page then, double page view should be launched.
				if (dualPageSpreadForCurrentPage || dualPageSpreadForPreviousPage) {
					if (EPubConfig.pageView != 'doublePage') {
						GlobalModel.currentPageIndex = PageNavigationManager.newPageIndex;
						objThis.member.isChangedToDualPageMode = true;
						//log(!(ismobile && (getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT)));
						if (!(ismobile && (getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT))) {
							EPubConfig.pageView = "doublePage";
							$(".doublePageLauncher").removeClass('dblpagelauncherSelected');
							$(".doublePageLauncher").trigger('click', true);
							//objThis.changePageView(nIndex, objThis);
							objThis.member.previousLandscapeState = 'singlePage';
						} else {
							objThis.member.previousLandscapeState = 'doublePage';
						}
					}
				} else if (objThis.member.isChangedToDualPageMode) {
					GlobalModel.currentPageIndex = PageNavigationManager.newPageIndex;
					objThis.member.isChangedToDualPageMode = false;
					if (EPubConfig.pageView != "singlePage") {
						EPubConfig.pageView = "singlePage";
						$(".singlePageLauncher").removeClass('singlepagelauncherSelected');
						$(".singlePageLauncher").trigger('click');
						//objThis.changePageView(nIndex, objThis);

					}
					objThis.member.previousLandscapeState = 'singlePage';
				}
				if (prevBtnClick == true) {
					prevBtnClick = false;
				}
				// return (nIndex > GlobalModel.currentPageIndex ? GlobalModel.currentPageIndex : nIndex);
				//return PageNavigationManager.newPageIndex;
			} catch(e) {
				console.log("Error in checkforpageproperty");
			}
		},
		/**
		 * This function receives the index to which the page is to be directed.
		 * nIndex = It is the position(index) of page as found in GlobalModel.pageBrkArr
		 */
		showPageAtIndex : function(nIndex, isResizing) {
			var objThis = this;
			if ((GlobalModel.originalPageView == 'doublePage' && nIndex % 2 == 1) ||
                (EPubConfig.pageView == 'doublePage' && nIndex % 2 == 1 && GlobalModel.pageDataByPageBreakValue["pagebreakid_" + GlobalModel.pageBrkValueArr[nIndex]].HasDualPageSpread)) {
				nIndex = nIndex - 1;
			}
			//log("nIndex   >>>: " + nIndex);
			$("#audioLauncherBtn").addClass("ui-disabled");
			$("#audioLauncherBtn").data('buttonComp').disable();
			$( '#audioLauncherBtn' ).removeClass( 'audioLauncherBtn-on' ).addClass( 'audioLauncherBtn-off' );
			if (!navigator.userAgent.match(/(android)/i)) {
				$("#annotation-bar").css('display', 'none');
				objThis.removeDefaultSelection();
			}

			if ($("#zoomBtnPanel").css("display") != "block")
				isZoomInOut = false;

			if (!isZoomInOut && isResizing == null) {
				$(objThis).trigger('closeOpenedPanel');
			}
			//for single and doublepage
			if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {
				if (nIndex == undefined || nIndex == null || nIndex == -1) {
					// return;
					$("#alertTxt").html(GlobalModel.localizationData["INVALID_PAGE_NUMBER_ALERT_TEXT"]);
					objThis.invalidPagenumberPopup();
				}
				/**
				 * GlobalModel.pageBrkValueArr is the array of total pages in the book
				 * objThis.member.curPageElementsPgBrk is the array of current pages in the book
				 * Find pgBrkVal in the curPageElementsPgBrk.
				 * If it exists, then index(0 - maxpages) would be its position in curPageElementsPgBrk and call swipepage
				 * if it doesn't exist, then index would be its position in GlobalModel.pageBrkValueArr, and call manageDomPageElements to create new pages.
				 */
				var pgBrkVal = GlobalModel.pageBrkValueArr[nIndex];
				var tempIndex = objThis.member.curPageElementsPgBrk.indexOf(pgBrkVal);
				if (tempIndex > -1) {
					objThis.member.curPageExists = true;
					nIndex = tempIndex;
				} else {
					objThis.member.curPageExists = false;
				}
				PageNavigationManager.manageDomPageElements(nIndex, objThis);
				//log(!(ismobile && (getDeviceOrientation() = AppConst.ORIENTATION_PORTRAIT)))
				// log("GlobalModel.originalPageView: ", GlobalModel.originalPageView);
				if (GlobalModel.originalPageView == 'singlePage') {

					objThis.checkForDualPageSpread(PageNavigationManager.newPageIndex);
				}
				if (tempIndex > -1) {
					objThis.member.curPageExists = true;
					nIndex = tempIndex;
					GlobalModel.currentPageIndex = nIndex;
					//console.log("GlobalModel.currentPageIndex: ",GlobalModel.currentPageIndex);
					PageNavigationManager.swipePage(nIndex, objThis, isResizing);
				} else {
					objThis.member.curPageExists = false;
					nPagenum = PageNavigationManager.newPageIndex;
					GlobalModel.currentPageIndex = nPagenum;
					//console.log("GlobalModel.currentPageIndex1: ",GlobalModel.currentPageIndex);
					setTimeout(function() {
						objThis.doPageLoading(nPagenum);
					}, 500);
				}
			} else {
				if (nIndex < objThis.options.arrPageLinkList.length && nIndex > -1) {
					/**
					 * GlobalModel.pageBrkValueArr is the array of total pages in the book
					 * objThis.member.curPageElementsPgBrk is the array of current pages in the book
					 * Find pgBrkVal in the curPageElementsPgBrk.
					 * If it exists, then index(0 - maxpages) would be its position in curPageElementsPgBrk and scroll the page
					 * if it doesn't exist, then index would be its position in GlobalModel.pageBrkValueArr, and call manageDomPageElements to create new pages.
					 */
					var pgBrkVal = GlobalModel.pageBrkValueArr[nIndex];
					var tempIndex = objThis.member.curPageElementsPgBrk.indexOf(pgBrkVal);
					//log("tempIndex: " + tempIndex);
					if (tempIndex > -1) {
						objThis.member.curPageExists = true;
						nIndex = tempIndex;
						GlobalModel.currentPageIndex = nIndex;
						var nTopPos = objThis.options.nScalePercent * (nIndex * (objThis.options.contentHeight + objThis.options.nPageGap + (2 * objThis.options.pagecompBorder) + (objThis.options.paddingTop))) - objThis.options.pagecompBorder;
						//log("nTopPos : " + nTopPos);
						$(objThis.options.objScrollContainer).scrollTop(nTopPos);
						//alert(nTopPos);
						objThis.member.elementScrollToppos = nTopPos;
					} else {
						objThis.member.curPageExists = false;
						objThis.managePageElementsCreation(nIndex);
					}
				} else {
					$("#alertTxt").html(GlobalModel.localizationData["INVALID_PAGE_NUMBER_ALERT_TEXT"]);
					objThis.invalidPagenumberPopup();
				}
			}

			try {
				$("#StandardsPanel").data('StandardsPanelComp').toggleBtnState();
			} catch(e) {
			}

		},
		getDataRole : function(objElement) {
			var objRole = $(objElement).data('pagecomp');
			return objRole;
		},
		getScaleValue : function() {
			return this.options.nScalePercent;
		},
		setZoom : function(nVal) {
			var objThis = this;

			if (!navigator.userAgent.match(/(android)/i)) {
				objThis.removeDefaultSelection();
			}
			var _pageContainer = $("[type='PageContainer']");
			var _scrollMainContainer = $("#scrollMainContainer");

			this.options.nScalePercent = nVal;

			var scaleFunc = "scale(" + nVal + "," + nVal + ")";
			//this.options.nScalePercent = 1;
			if ($("#zoomBtnPanel").css("display") == "block")
				isZoomInOut = true;
			var cssObj = {};

			cssObj["-ms-transform"] = scaleFunc;
			cssObj["-moz-transform"] = scaleFunc;
			cssObj["-webkit-transform"] = scaleFunc;
			cssObj["-o-transform"] = scaleFunc;
			cssObj["transform"] = scaleFunc;
			cssObj["-ms-transform-origin"] = "0% 0%";
			cssObj["-moz-transform-origin"] = "0% 0%";
			cssObj["-webkit-transform-origin"] = "0% 0%";
			cssObj["-o-transform-origin"] = "0% 0%";
			cssObj["transform-origin"] = "0% 0%";

			$("#innerScrollContainer").css(cssObj);

			if (navigator.userAgent.indexOf('MSIE') != -1) {
				if (EPubConfig.pageView.toLowerCase() == "scroll") {
					var maxVLimit = $("#innerScrollContainer")[0].scrollHeight * nVal - $(window).height();
					$($("#innerScrollContainer").parent()).css("-ms-scroll-limit-y-max", maxVLimit);
				}
			}

			this.updateBarWidth(nVal);

			// this.updateScrollBtnPos(nVal);
			GlobalModel.currentScalePercent = this.options.nScalePercent;

			if (EPubConfig.pageView == 'singlePage') {
				var currPageWidth = (_pageContainer.width() + 2 * objThis.options.pagecompBorder) * nVal;
				var currPageHeight = (_pageContainer.height() ) * nVal;
				this.member._nScaledContentWidth = currPageWidth;
				this.member._nScaledContentHeight = currPageHeight;
				_scrollMainContainer.width(_pageContainer.width() * nVal);
				_scrollMainContainer.height(currPageHeight);
				$("#bookContentContainer2").width(currPageWidth * EPubConfig.totalPageContainers);
				
			} else if (EPubConfig.pageView == 'doublePage') {
				var currPageWidth = ($("[type='PageContainer']").width() + 2 * objThis.options.pagecompBorder) * nVal;
				var currPageHeight = ($("[type='PageContainer']").height() + 2 * objThis.options.pagecompBorder) * nVal;
				this.member._nScaledContentWidth = currPageWidth;
				this.member._nScaledContentHeight = currPageHeight;
				$("#scrollMainContainer").width(2 * $("[type='PageContainer']").width() * nVal);
				$("#scrollMainContainer").height(currPageHeight);
				$("#bookContentContainer2").width(currPageWidth * EPubConfig.totalPageContainers);
				
			} else {
				$("#scrollMainContainer").css('margin-left', 0);
			}

			if (EPubConfig.pageView == 'singlePage' || EPubConfig.pageView == 'doublePage') {

				PageNavigationManager.resetSinglePageContainerPos(GlobalModel.currentPageIndex, objThis, true);
			} else {
				var nTopPos = objThis.options.nScalePercent * (GlobalModel.currentPageIndex * (objThis.options.contentHeight + objThis.options.nPageGap + (2 * objThis.options.pagecompBorder) + (objThis.options.paddingTop))) - objThis.options.pagecompBorder;
				if (GlobalModel.currentPageIndex < objThis.options.arrPageLinkList.length && GlobalModel.currentPageIndex > -1) {
					$(objThis.options.objScrollContainer).scrollTop(nTopPos);
					objThis.member.elementScrollToppos = nTopPos;
				}
			}

			if (EPubConfig.pageView.toLowerCase() == "scroll") {
				if (this.options.nScalePercent <= 1)
					$("#innerScrollContainer").height(this.options.totalContentHeight * this.options.nScalePercent);
			}
			PageNavigationManager.adjustPageToScreen(nVal);
			var _qaPanel = $("#questionAnswerPanel"), _imageGalleryPanel = $("#imageGalleryPanel"), _imageGalleryVisible = (_imageGalleryPanel.css("display") == "block" ) ? true : false, _qaPanelVisible = (_qaPanel.css("display") == "block" ) ? true : false;

			if (_qaPanelVisible || _imageGalleryVisible || ($('.players').css("display") == "block" ) || ( $("#learnosityPanel").css("display") == "block" ) ) {
				this.updateDesignForPageView("panelPosition", nVal);
				$( document ).trigger( "updateLearnosityViewSize" );
			}
			var scalePercent = parseInt((GlobalModel.currentScalePercent / GlobalModel.actualScalePercent) * 100);
			$("#rangeSlider a").attr("title", scalePercent + "%");

		},
		updateBarWidth : function(nVal) {
		},

		getCurrentPageIndex : function() {
			return this.options.nCurrentPageIndex;
		},

		updateViewSize : function(hasTimeDelay) {
			var objThis = this;
			if ($("#bookContentContainerHeight")[0] == null) {
				$("head").append("<style id='bookContentContainerHeight' type='text/css'></style>")
			}
			var updatedHeight = window.innerHeight - topNavigationBarHeight;
			
			$("#bookContentContainerHeight").text("#bookContentContainer{height: " + updatedHeight + "px !important;}");

			if (EPubConfig.pageView == 'singlePage') {
				$("#bookContentContainer").css({
					'position' : 'absolute',
					'top' : topNavigationBarHeight + 'px',
					'left' : 0,
					'height' : window.height + ' !important'
				});
				//setTimeout($("#bookContentContainer").height(window.height+' !important'),600);
			} else if (EPubConfig.pageView == 'doublePage') {

				$("#bookContentContainer").css({
					'position' : 'absolute',
					'top' : topNavigationBarHeight + 'px',
					'left' : 0,
					'height' : window.height
				});
			} else {
				var height = $("#bookContentContainer").height();

				if (hasTimeDelay === true) {
					setTimeout(function() {
						$("#bookContentContainer2").height(updatedHeight);
						$("#bookContentContainer2").width($("#bookContentContainer").width());
						$("#bookContentContainer").css({
							'position' : 'absolute',
							'top' : topNavigationBarHeight + 'px',
							'left' : 0
						});
						//objThis.updatePanelPositionsOnZoom(objThis.options.nScalePercent);
					}, 550);
				} else {
					var bkContainerOffset = $("#bookContentContainer").offset().left;
					$("#bookContentContainer").width(window.width - bkContainerOffset)
					$("#bookContentContainer2").height(updatedHeight);
					$("#bookContentContainer2").width($("#bookContentContainer").width());
					$("#bookContentContainer").css({
						'position' : 'absolute',
						'top' : topNavigationBarHeight + 'px',
						'left' : 0
					});
				}
			}

			this.updateDesignForPageView("panelPosition", objThis.options.nScalePercent);
		},

		getCurrentPageName : function() {
			var strName = "";
			var nIndex = GlobalModel.currentPageIndex;
			var nIndexInPageIndex = parseInt($($('[type=PageContainer]')[nIndex]).attr('pagesequence'));
			if (this.member.arrPageNames) {
				if (nIndexInPageIndex < this.member.arrPageNames.length)
					strName = this.member.arrPageNames[nIndexInPageIndex];
					
				if (EPubConfig.pageView == AppConst.DOUBLE_PAGE_CAMEL_CASING) {
					strName += ",";
					if (GlobalModel.currentPageIndex >= 0)
						strName += this.member.arrPageNames[nIndexInPageIndex + 1];
				}

				
			}

			return strName;
		},
		/**
		 * This is function is called from 1.NavigationOperator. This will scroll the ebook to the top most point
		 */
		scrollEbookToTop : function() {
			var objScrollElement = $(this.element).find('[id=bookContentContainer2]');
			$(objScrollElement).scrollTop(0);
			this.member.elementScrollToppos = 0;
		},

		hideLeftSideAnnotationPanel : function() {
			$(this).trigger('closeOpenedPanel');
		},

		invalidPagenumberPopup : function() {

			setTimeout(function() {
				PopupManager.addPopup($("#alertPopUpPanel"), null, {
					isModal : true,
					isCentered : true,
					popupOverlayStyle : "popupMgrMaskStyleSemiTransparent",
					isTapLayoutDisabled : true
				});
			}, 0);
		},

		hideBrandBar : function() {
			var objThis = this;
			clearTimeout(objThis.options.hideBrandBarTimeout);
			if ($("#header").css("display") == "block") {
				$("#header").slideUp(EPubConfig.brandHideInDuration,function(){
					$("#navigationBar").slideDown(EPubConfig.brandHideInDuration);
				});
				
			}

		},
		resetZoom : function() {
			//  log("enableDblTap: " + enableDblTap);
			if (enableDblTap == true) {
				$("#rangeSlider").slider({
					value : 100
				});
				isZoomInOut = false;
				this.setZoom(GlobalModel.actualScalePercent);
				if (ismobile && EPubConfig.pageView.toUpperCase() != 'SCROLL') {
					var objScrollElement = $(this.element).find('[id=bookContentContainer2]');
					PageNavigationManager.addSwipeListner(objScrollElement, this);
				}
				$(this).trigger('closeOpenedPanel');
			}
		},

		removeOpenedPopups : function() {
			var objThis = this;
			if ($("#stickyNoteContainer").css('display') != "none") {
				$("#stickyNoteContainer").find('.outerPanelClsBtn').trigger('click');
				$("#stickyNoteContainer").find('.outerPanelClsBtn').trigger('vclick');
			}

			if ($("#highlightContainer").css('display') != "none") {
				$("#highlightContainer").find('.outerPanelClsBtn').trigger('click');
				$("#highlightContainer").find('.outerPanelClsBtn').trigger('vclick');
			}
			if ($("#dltConfirmPanel").css('display') != "none") {
				PopupManager.removePopup();
			}
			if ($("#imageGalleryPanel").css('display') == "none") {
				PopupManager.removePopup();
			}
			if ($("#glossaryPanel").css('display') != "none") {
				$("#glossaryPanel").find('#closeButton').trigger('click');
			}
			if ( $("#audioPopUp").css("display") == "block" ) 
			{
				$("#audioPlayerCloseBtn").trigger('click');
			}

		},
		changePageView : function(iPageIndex, objComp, isResizing) {

			var objThis = this;
			objThis.showHideSideNavigationBars ();
			objThis.removeOpenedPopups();
			isPageViewChange = true;
			if (iPageIndex == -1) {
				iPageIndex = 0;
			}
			$("#audioLauncherBtn").addClass("ui-disabled");
			$("#audioLauncherBtn").data('buttonComp').disable();
			$( '#audioLauncherBtn' ).removeClass( 'audioLauncherBtn-on' ).addClass( 'audioLauncherBtn-off' );
			var iPageIndex = $($('[type=PageContainer]')[iPageIndex]).attr('pagesequence');
			var sliderElem = $("#rangeSlider");
			objThis.updateDesignForPageView("updateElemSize");
			setTimeout(function() {
				objThis.resetZoom();
			}, 0);
			GlobalModel.isToAnimate = false;
			//GlobalModel.currentPageIndex = iPageIndex;
			$(objThis).trigger('closeOpenedPanel');
			sliderElem.slider({
				value : 100
			});
			var scrollMainContainerWidth = $("#scrollMainContainer").width();
			$("#bookContentContainer").height(window.height);
			var isPageLoaded = false;
			if (EPubConfig.pageView == 'doublePage') {
				// sliderElem.slider('disable');
				$("#scrollMainContainer").width(scrollMainContainerWidth * 2);
				$('#innerScrollContainer').scrollTop(0);
				setTimeout(function() {
					$("#bookContentContainer2").scrollTop(0);
				}, 50);
				$("#bookContentContainer").scrollTop(0);
				PageNavigationManager.adjustPageToScreen(GlobalModel.currentScalePercent);
				objThis.updateDesignForPageView("updateElemSize");
				objThis.updateViewSize();
				/* If the page index is ODD, then it should appear on the left hand side in the double page view of the ebook.*/
				//PageNavigationManager.addRemoveStartingLeftPage(true, objThis);
				objThis.scaleContentToFit();
				isPageLoaded = PageNavigationManager.addRemoveStartingLeftPage(true, objThis);
			} else if (EPubConfig.pageView == 'singlePage') {
				sliderElem.slider('enable');
				$("#scrollMainContainer").width(scrollMainContainerWidth / 2);

				$('#innerScrollContainer').scrollTop(0);
				$("#bookContentContainer2").scrollTop(0);
				$("#bookContentContainer").scrollTop(0);
				PageNavigationManager.adjustPageToScreen(GlobalModel.currentScalePercent);
				objThis.updateDesignForPageView("updateElemSize");
				objThis.updateViewSize();
				//$("#innerScrollContainer").css({'-webkit-transform': 'scale(1, 1)'});

				//PageNavigationManager.addRemoveStartingLeftPage(false, objThis);
				objThis.scaleContentToFit();
				//objThis.options.nCurrentPageIndex = GlobalModel.currentPageIndex;
				isPageLoaded = PageNavigationManager.addRemoveStartingLeftPage(false, objThis);
				setTimeout(function() {
					$("#bookContentContainer2").scrollTop(0);
				}, 50);
			} else {
				sliderElem.slider('enable');
				$("#bookContentContainer2").css({
					'transition' : 'left 0s',
					'-webkit-transition' : 'left 0s',
					'-moz-transition' : 'left 0s',
					'-o-transition' : 'left 0s'
				});
				//PageNavigationManager.adjustPageToScreen();
				objThis.updateDesignForPageView("updateElemSize");
				objThis.updateViewSize();
				if (ismobile || isWinRT) {
					objThis.scaleContentToFit(false, true);
				} else {
					$("#innerScrollContainer").css({
						'-webkit-transform' : 'scale(1, 1)',
						'-moz-transform' : 'scale(1, 1)',
						'-ms-transform' : 'scale(1, 1)',
						'-o-transform' : 'scale(1, 1)',
						'transform' : 'scale(1, 1)'
					});
					GlobalModel.actualScalePercent = 1;
					objThis.options.nScalePercent = 1;
					GlobalModel.currentScalePercent = 1;
					$("#innerScrollContainer").css('height', 'auto');
					$("#innerScrollContainer").css('overflow', '');
				}
				PageNavigationManager.adjustPageToScreen(GlobalModel.currentScalePercent);
				isPageLoaded = PageNavigationManager.addRemoveStartingLeftPage(false, objThis);
			
			if( ( getDeviceOrientation() == AppConst.ORIENTATION_PORTRAIT ) && isWinRT )	
			{
				setTimeout(function(){	$("#bookContentContainer2").width(objThis.options.contentWidth+20); }, 500);
			}
			}
			objThis.updateDesignForPageView("panelPosition", objThis.options.nScalePercent);
			var iPageIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagesequence');
			if (iPageIndex < 0) {
				iPageIndex = 0;
			}
			if (isPageLoaded != true) {
				objThis.showPageAtIndex(iPageIndex, isResizing);
			}

			setTimeout(function() {
				if (EPubConfig.pageView != 'singlePage' && EPubConfig.pageView == 'doublePage') {
					$(objThis).trigger(objThis.events.PAGE_INDEX_CHANGE, objThis.data);
				}
			}, 50);

			var objScrollElement = $(this.element).find('[id=bookContentContainer2]');
			PageNavigationManager.removeSwipeListner(objScrollElement);
			if ( typeof (EPubConfig.swipeNavEnabled) != 'undefined' && EPubConfig.swipeNavEnabled && ismobile != null && GlobalModel.currentScalePercent == GlobalModel.actualScalePercent) {
				if (EPubConfig.pageView != "SCROLL") {
					PageNavigationManager.addSwipeListner(objScrollElement, objThis);
				}
			}

			$(document).trigger(AppConst.PAGE_VIEW_CHANGED);
		},

		/**
		 * Pinch in/out starts here
		 * This function is to add different listners to zoom in/out using pinch gesture
		 */
		addListeners : function() {
			if (ismobile) {
				this.member._objElement.ontouchstart = this.onStart;
				this.member._objElement.ontouchmove = this.onMove;
				this.member._objElement.ontouchend = this.onEnd;

			} else {
				this.member._objElement.onmousedown = this.onStart;
				this.member._objElement.onmousemove = this.onMove;
				this.member._objElement.onmouseup = this.onEnd;
			}

		},

		/**
		 * Touch or mousedown events cause call to this function
		 */
		onStart : function(e) {
			this.thisRef.touchStart(e, $(this).find('#innerScrollContainer'));
		},

		/**
		 * if mouse or fingure is moved
		 */
		onMove : function(e) {
			this.thisRef.touchMove(e, $(this).find('#innerScrollContainer'));
		},

		/**
		 * on end of mouse-click/touch
		 */
		onEnd : function(e) {
			this.thisRef.touchEnd(e, $(this).find('#innerScrollContainer'));
			 if (navigator.userAgent.match(/(android)/i)) {
                  this.addListeners();
                }
		},

		/**
		 * Timer function to calculate speed of scroll.
		 */
		timerFn : function() {
			var objThis = this;
			scroll = scrollY;

			var dt = prevTime - now1;

			//log("dt widout 1: " + (prevTime - now1));
			//log("diff " + Math.abs(scroll - prevScroll) + " ----   dt " + dt + " ------ division " + Math.abs(scroll - prevScroll) / dt);
			speed = 2000 * (scroll - prevScroll) / dt;
			//log("speed: " + Math.abs(speed / 5));
			now1 = 0;
			prevTime = 0;

		},

		/**
		 * When the touch starts we add our sliding class a record a few
		 * variables about where the touch started.  We also record the
		 * start time so we can do momentum.
		 */
		touchStart : function(e, jContent) {
			var objThis = this;
			var thisMemVars = this.member;

			prevScroll = $("#bookContentContainer2").scrollTop();
			isAnimationScrolling = false;
			now1 = $.now();
			$("#bookContentContainer2").stop(true, false);

			stopCompleteScrollTimer = setTimeout(function() {
				stopCompleteScroll = true;
			}, 300);
			var nCurrentLeftofContent = objThis.getLeft(jContent);
			clearInterval(objScrollCompData.timer);
			var arrCoors = this.getCentroid(e);

			this.TOUCHNUM = arrCoors[2];
			//(ev.touches.length == ev.targetTouches.length
			// log("this.TOUCHNUM in start: " + this.TOUCHNUM);

			// log(this.TOUCHNUM);
			// let it move if the content is bigger than the container (width)
			var xlimit = nCurrentLeftofContent + thisMemVars._nScaledContentWidth - this.options.padding;

			if (xlimit <= thisMemVars._width) {
				this.bExtremeRight = true;
			} else {
				this.bExtremeRight = false;
			}
			if (nCurrentLeftofContent >= this.options.padding) {
				this.bExtremeLeft = true;
			} else {
				this.bExtremeLeft = false;
			}
			this.removeIpadKbd();
			// This is used to remove focus from all input fields while Pinching
			jContent.css({
				'-webkit-transition-duration' : '0'
			});

			//log("***********  " + this.TOUCHNUM + "    " +  + "    " + e.targetTouches);
			if (this.TOUCHNUM == 1) {
				// Reset zoom level if double tapped/clicked on touch devices
				if (ismobile || isWinRT) {
					var now = new Date().getTime();
					objThis.member.lastTouch = ( typeof objThis.member.lastTouch != 'undefined') ? objThis.member.lastTouch : now;
					var diff = now - objThis.member.lastTouch;
					//log("diff: "+diff);
					// If consecutive taps are with in 500 ms
					if (diff < 350 && diff > 0) {
						if ((GlobalModel.currentScalePercent.toFixed(3) != GlobalModel.actualScalePercent.toFixed(3))) {
							objThis.resetZoom();
						}
					} else {
						objThis.member.lastTouch = now;
						objScrollCompData.startX = arrCoors[0];
						objScrollCompData.startY = arrCoors[1];
						objScrollCompData.endX = -1;
						objScrollCompData.endY = -1;
						objScrollCompData.timeOutCompleted = false;
						timeOutComp = setTimeout(function(e) {
							objScrollCompData.timeOutCompleted = true;
							//////console.log('calling');
						}, 1000);
					}
					nCurrentScrollTop = $("#bookContentContainer2").scrollTop();
					nCurrentTouchY = arrCoors[1];
					// GlobalModel.isContentRenderingSuspended = true;
				}
				//e.stopPropagation();
			}
			objThis.startX = objThis.MouseX = arrCoors[0];
			objThis.startY = objThis.MouseY = arrCoors[1];
			objThis.startLeft = objThis.getLeft(jContent);
			objThis.startTop = objThis.getTop(jContent);

			if (objThis.TOUCHNUM >= 2) {
				try {
					if (e != undefined && e.touches != undefined) {
						if (GlobalModel.currentScalePercent >= GlobalModel.actualScalePercent && GlobalModel.currentScalePercent <= 3) {

							if (EPubConfig.pageView == 'singlePage') {
								GlobalModel.currentScrollableElement = $("#bookContentContainer2");
								GlobalModel.currentScrollingElement = $("#bookContentContainer");
							} else {
								GlobalModel.currentScrollableElement = $("#innerScrollContainer");
								GlobalModel.currentScrollingElement = $("#bookContentContainer2");
							}

							GlobalModel.currentBookContentContaineOverflow = $(GlobalModel.currentScrollableElement).css("overflow");

							var nScLeft = $(GlobalModel.currentScrollingElement).scrollLeft();
							var nScTop = $(GlobalModel.currentScrollingElement).scrollTop()
							//console.log("nScTop: ", nScTop);
							$(GlobalModel.currentScrollingElement).scrollLeft(0);
							$(GlobalModel.currentScrollingElement).scrollTop(0)

							$(GlobalModel.currentScrollableElement).css({
								'transition' : 'left 0s',
								'-webkit-transition' : 'left 0s',
								'-moz-transition' : 'left 0s',
								'-o-transition' : 'left 0s',
								'left' : ($(GlobalModel.currentScrollableElement).position().left - nScLeft) + 'px',
								'top' : ($(GlobalModel.currentScrollableElement).position().top - nScTop) + 'px'
							});
							if (EPubConfig.pageView == 'singlePage') {
								$("#bookContentContainer2").css("overflow", "visible");
							}
							isZooming = true;
							//log("calling isZooming");
							innerScrollContainerPosOnTouchStart = $(GlobalModel.currentScrollableElement).position();
							innerScrollContainerPosOnTouchStart.top = 0;
							//console.log(GlobalModel.currentPageIndex);
							if (EPubConfig.pageView == 'singlePage') {
								innerScrollContainerPosOnTouchStart.left = -1 * (GlobalModel.currentPageIndex * GlobalModel.actualScalePercent * EPubConfig.pageWidth);
							} else {
								innerScrollContainerPosOnTouchStart.top = $(GlobalModel.currentScrollableElement).position().top
								innerScrollContainerPosOnTouchStart.left = 0;
							}
							GlobalModel.bookContentContainerSize = {
								width : $("#bookContentContainer").width(),
								height : $("#bookContentContainer").height()
							};
							var objRepositionableElem = (EPubConfig.pageView == 'singlePage') ? $("#bookContentContainer2") : $("#innerScrollContainer");
							GlobalModel.objRepositionableObjectPosition = $(objRepositionableElem).position();
						}
					}
				} catch (e) {
					//console.warn("error : " + e.message);
				}
			}

		},

		/**
		 * Drag the content based on touch coordinate movement
		 */
		touchMove : function(e, jContent) {

			if (ismobile || ( isWinRT && WinRTPointerType == "touch" )) {
				// return;
				// e.preventDefault();
				var objThis = this;
				//flag = true;
				//nCurrentScrollTop = $("#bookContentContainer").scrollTop();
				//nCurrentTouchY = arrCoors[1];
				if (navigator.userAgent.match(/(ipad)/i)) {
					if (EPubConfig.pageView.toLowerCase() == "scroll") {
						if (objThis.TOUCHNUM == 1) {
							var touchY = e.touches[0].pageY;
							//log(touchY - nCurrentTouchY)
							var nSCTop = $("#bookContentContainer2").scrollTop();

							if (!GlobalModel.isScrolling) {
								if (Math.abs(((touchY - nCurrentTouchY))) > 15) {
									$("#bookContentContainer2").scrollTop(nCurrentScrollTop - (touchY - nCurrentTouchY));
									GlobalModel.isScrolling = true;
								}
							} else {
								$("#bookContentContainer2").scrollTop(nCurrentScrollTop - (touchY - nCurrentTouchY));
							}
							flag = true;
						}
					}
				}

				// This is used to remove focus from all input fields while Pinching
				var thisMemVars = objThis.member;
				var arrCoors = objThis.getCentroid(e);
				objThis.MouseX = arrCoors[0];
				objThis.MouseY = arrCoors[1];
				// log("objThis.TOUCHNUM in move: " + objThis.TOUCHNUM);
				// Two or more fingures are used
				if (objThis.TOUCHNUM >= 2) {
					try {
						e.preventDefault();
						//e.stopPropagation();

						if (e != undefined && e.touches != undefined && EPubConfig.pageView != AppConst.DOUBLE_PAGE_CAMEL_CASING && isZooming == true) {
							var x1 = e.touches[0].pageX;
							var y1 = e.touches[0].pageY;

							var x2 = e.touches[1].pageX;
							var y2 = e.touches[1].pageY;

							//var offset = $("#bookContentContainer").offset();

							if (x1 > 100) {

								var nDist = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
								if (!lastDist) {
									lastDist = nDist;
								}
								//GlobalModel.currentScrollableElement = $("#innerScrollContainer");
								//GlobalModel.currentScrollingElement

								// var nZoom = nAddlScalePct;
								var nZoom = objThis.options.nScalePercent * nDist / lastDist;
								//    log("nZoom: "+nZoom);
								if (nZoom < GlobalModel.actualScalePercent) {
									nZoom = GlobalModel.actualScalePercent;
								}

								if (nZoom > 3) {
									nZoom = 3;
								}
								// console.log(nZoom);
								if (nZoom >= GlobalModel.actualScalePercent && nZoom <= 3 && nZoom != GlobalModel.currentScalePercent) {

									//var objCurrentPageOffset = $(GlobalModel.getCurrentPageElement()).offset();
									var objCurrentPageOffset = (EPubConfig.pageView.toLowerCase() == "scroll") ? $(GlobalModel.currentScrollableElement).offset() : $(GlobalModel.getCurrentPageElement()).offset();
									var objDisplayAreaOffset = $("#bookContentContainer").offset();

									var nPageHeightBeforeScale = ((EPubConfig.pageView.toLowerCase() == "scroll") ? (objThis.options.contentHeight * EPubConfig.totalPageContainers) : EPubConfig.pageHeight) * GlobalModel.currentScalePercent;
									//EPubConfig.pageHeight * GlobalModel.currentScalePercent;((EPubConfig.pageView == "SCROLL") ? GlobalModel.getPageContainersSize().height : EPubConfig.pageHeight) * GlobalModel.currentScalePercent;
									var nPageWidthBeforeScale = EPubConfig.pageWidth * GlobalModel.currentScalePercent;

									objThis.setContentScale(nZoom);

									var objMinPosPoints = {};
									objMinPosPoints.xpos = ((x2 < x1) ? x2 : x1);
									// - $("#bookContentContainer").offset().left;
									objMinPosPoints.ypos = ((y2 < y1) ? y2 : y1);
									// - $("#bookContentContainer").offset().top;

									var objMaxPosPoints = {};
									objMaxPosPoints.xpos = ((x2 < x1) ? x1 : x2);
									// - $("#bookContentContainer").offset().left;
									objMaxPosPoints.ypos = ((y2 < y1) ? y1 : y2);
									// - $("#bookContentContainer").offset().top;

									var objCenterPosPoint = {};
									var objRepositionableElem = (EPubConfig.pageView == 'singlePage') ? $("#bookContentContainer2") : $("#innerScrollContainer");
									objCenterPosPoint.xpos = objMinPosPoints.xpos + ((objMaxPosPoints.xpos - objMinPosPoints.xpos) / 2);
									objCenterPosPoint.ypos = objMinPosPoints.ypos + ((objMaxPosPoints.ypos - objMinPosPoints.ypos) / 2);

									var nTchXDistanceForPage = objCenterPosPoint.xpos - objCurrentPageOffset.left;
									var nTchYDistanceForPage = objCenterPosPoint.ypos - objCurrentPageOffset.top;

									var nTchXDistanceForDispArea = objCenterPosPoint.xpos - objDisplayAreaOffset.left;
									var nTchYDistanceForDispArea = objCenterPosPoint.ypos - objDisplayAreaOffset.top;

									var nPageHeightAfterScale = ((EPubConfig.pageView.toLowerCase() == "scroll") ? (objThis.options.contentHeight * EPubConfig.totalPageContainers) : EPubConfig.pageHeight) * GlobalModel.currentScalePercent;
									var nPageWidthAfterScale = EPubConfig.pageWidth * GlobalModel.currentScalePercent;

									var nNewDist = 0;
									var objCurrentPos = {};
									//{xpos: -(EPubConfig.pageWidth * GlobalModel.currentPageIndex * GlobalModel.currentScalePercent), ypos: 0};
									var nPct = 0;
									if (EPubConfig.pageView.toLowerCase() == "scroll") {
										objCurrentPos = {
											xpos : 0,
											ypos : 0
										};
									} else {
										objCurrentPos = {
											xpos : -((EPubConfig.pageWidth + 0.25) * GlobalModel.currentPageIndex * GlobalModel.currentScalePercent),
											ypos : 0
										};
									}
									if (nPageWidthAfterScale > GlobalModel.bookContentContainerSize.width) {

										nPct = nTchXDistanceForPage / nPageWidthBeforeScale;
										nNewDist = nPct * (nPageWidthAfterScale - nPageWidthBeforeScale);
										if (EPubConfig.pageView.toLowerCase() == "scroll") {
											objCurrentPos.xpos = GlobalModel.objRepositionableObjectPosition.left - nNewDist;
										} else {
											objCurrentPos.xpos = (objCurrentPos.xpos - (objDisplayAreaOffset.left - objCurrentPageOffset.left)) - nNewDist;
										}

									}
									
									nPct = nTchYDistanceForPage / nPageHeightBeforeScale;
									nNewDist = nPct * (nPageHeightAfterScale - nPageHeightBeforeScale);
									objCurrentPos.ypos = GlobalModel.objRepositionableObjectPosition.top - nNewDist;

									$(objRepositionableElem).css("top", objCurrentPos.ypos);
									$(objRepositionableElem).css("left", objCurrentPos.xpos);
									GlobalModel.objRepositionableObjectPosition = {
										left : objCurrentPos.xpos,
										top : objCurrentPos.ypos
									};

								}

								lastDist = nDist;
							}
						}
					} catch (e) {
						//console.warn("error : " + e.message);
					}
				}
			}
		},

		setContentScale : function(nVal) {
			var objThis = this;
			GlobalModel.currentScalePercent = nVal;

			this.options.nScalePercent = nVal;

			var scaleFunc = "scale(" + nVal + "," + nVal + ")";

			if ($("#zoomBtnPanel").css("display") == "block")
				isZoomInOut = true;

			var cssObj = {};

			cssObj["-ms-transform"] = scaleFunc;
			cssObj["-moz-transform"] = scaleFunc;
			cssObj["-webkit-transform"] = scaleFunc;
			cssObj["-o-transform"] = scaleFunc;
			cssObj["transform"] = scaleFunc;
			cssObj["-ms-transform-origin"] = "0% 0%";
			cssObj["-moz-transform-origin"] = "0% 0%";
			cssObj["-webkit-transform-origin"] = "0% 0%";
			cssObj["-o-transform-origin"] = "0% 0%";
			cssObj["transform-origin"] = "0% 0%";

			$("#innerScrollContainer").css(cssObj);

			if (EPubConfig.pageView == 'singlePage') {
				var currPageHeight = (EPubConfig.pageHeight + 2 * objThis.options.pagecompBorder) * nVal;
				$("#scrollMainContainer").width(EPubConfig.pageWidth * nVal);
				$("#scrollMainContainer").height(currPageHeight);
			} else {
				//$("#scrollMainContainer").css('margin-left', 0);
			}

		},

		/**
		 * When the touch ends we need to adjust the grid for momentum
		 * and to snap to the grid.  We also need to make sure they
		 * didn't drag farther than the end of the list in either
		 * direction.
		 */
		touchEnd : function(e, jContent) {
			///////////////this is used for open sticky popup
			var objThis = this;
			/****************************************/

			if (navigator.userAgent.match(/(ipad)/i)) {
				if (EPubConfig.pageView.toLowerCase() == "scroll") {
					GlobalModel.isScrolling = false;
					if (stopCompleteScrollTimer) {
						clearTimeout(stopCompleteScrollTimer);
					}
					if (flag == true) {
						//log("touchend");
						//$("#bookContentContainer2").stop(true, false)
						prevTime = $.now();
						scrollY = $("#bookContentContainer2").scrollTop();
						objThis.timerFn();
						flag = false;
						if (objThis.TOUCHNUM == 1) {
							if (stopCompleteScroll == false) {
								isAnimationScrolling = true;
								if ((scroll - prevScroll) == 0) {
									isAnimationScrolling = false;
								}
								// log(stopCompleteScroll);
								var scrollToPos = (scroll + speed / 5);
								var nTopPos = objThis.options.nScalePercent * (objThis.options.totalPageCount * (objThis.options.contentHeight + objThis.options.nPageGap + (2 * objThis.options.pagecompBorder) + (objThis.options.paddingTop))) - objThis.options.pagecompBorder;
								if (scrollToPos < 0 || scrollToPos > nTopPos) {
									isAnimationScrolling = false;
								}

								$("#bookContentContainer2").animate({
									scrollTop : scroll + speed / 5
								}, 1000, "easeOutCubic", function() {
									//log("OnAnimationcomplete");
									isAnimationScrolling = false;
								});

							}

						}
					} else {
						stopCompleteScroll = true;
						isAnimationScrolling = false;
						$("#bookContentContainer2").stop(true, false);
					}
					stopCompleteScroll = false;
				}
			}

			/****************************************/
			lastDist = 0;
			clearInterval(GlobalModel.zoominterval);

			if (isZooming) {
				isZooming = false;
				var nBCCPos = $(GlobalModel.currentScrollableElement).position();
				var scLeft = $(GlobalModel.getCurrentPageElement()).offset().left - $("#bookContentContainer").offset().left;
				var scTop;
				if (EPubConfig.pageView == 'singlePage') {
					scTop = $(GlobalModel.getCurrentPageElement()).offset().top;
					$(GlobalModel.currentScrollableElement).css("left", -1 * EPubConfig.pageWidth * GlobalModel.currentPageIndex * GlobalModel.currentScalePercent);
				} else {
					scTop = $(GlobalModel.currentScrollableElement).offset().top;
					$(GlobalModel.currentScrollableElement).css("left", "0px");
				}

				$(GlobalModel.currentScrollableElement).css("top", "0px");
				if (scLeft > 0) {
					scLeft = 0;
				}
				$(GlobalModel.currentScrollingElement).scrollLeft(Math.abs(scLeft));
				if (scTop > 0) {
					scTop = 0;
				}
				$(GlobalModel.currentScrollingElement).scrollTop(Math.abs(scTop));
			}

			var userAgent = navigator.userAgent;
			var objScrollElement = $(this.element).find('[id=bookContentContainer2]');

			if ( !ismobile ) 
			{
				PopupManager.removePopup();
			}

			var objThis = this;
			var thisMemVars = this.member;
			if (thisMemVars._nPercentScale <= thisMemVars._nActualPercentScale) {
				
				// intentionally left blank

			} else {
				// if margin on left
				if (this.member._bFitToPage == true && thisMemVars._height >= thisMemVars._nScaledContentHeight) {
					return;
				}
				var nCurrentTopofContent = objThis.getTop(jContent);
				var top = thisMemVars._height - thisMemVars._nScaledContentHeight;
				var ylimit = nCurrentTopofContent + thisMemVars._nScaledContentHeight;
				var margin = thisMemVars._width - thisMemVars._nScaledContentWidth;
				var bHorzontalMargin = (margin > 0) ? true : false;
				if (bHorzontalMargin) {
					var left = margin / 2;
					jContent.css('left', left + 'px');
				}

				if (nCurrentTopofContent > 0) {
					top = this.options.padding;
					jContent.css('top', top + 'px');
				} else if (ylimit < thisMemVars._nAvailableContentAreaHeight) {
					jContent.css('top', top + 'px');
				}

				var nCurrentLeftofContent = objThis.getLeft(jContent);
				var xlimit = nCurrentLeftofContent + thisMemVars._nScaledContentWidth;
				var gap = thisMemVars._width - xlimit;
			}
			thisMemVars._nTouchGap = 0;
			clearInterval(objScrollCompData.timer);

			if (objScrollCompData.timeOutCompleted == false) {

				if (objScrollCompData.startY - objThis.MouseY > 50) {
					var nYDiff = (objScrollCompData.startY - objThis.MouseY) * 50;
					objScrollCompData.timer = setInterval(function(e) {
						nYDiff = nYDiff / 2;
						if (nYDiff < 1) {
							clearInterval(objScrollCompData.timer);
						}
					}, 100);
				}

				if (objScrollCompData.startY - objThis.MouseY < -50) {
					var nYDiff = Math.abs(objScrollCompData.startY - objThis.MouseY) * 50;
					objScrollCompData.timer = setInterval(function(e) {
						nYDiff -= (nYDiff / 2);
						if (nYDiff < 1) {
							clearInterval(objScrollCompData.timer);
						}
					}, 100);
				}
				// The swipe events should be triggered only when touchnum is 1.

				if (objThis.TOUCHNUM == 1) {
					if ((objScrollCompData.startX - objThis.MouseX > 50) && (EPubConfig.pageView.toUpperCase() != 'SCROLL')) {
						if (GlobalModel.currentScalePercent.toFixed(3) <= GlobalModel.actualScalePercent.toFixed(3)) {
							clearTimeout(timeOutComp);
							$(objThis.element).trigger("swipepageleft");
						}

					}

					if ((objScrollCompData.startX - objThis.MouseX < -50) && (EPubConfig.pageView.toUpperCase() != 'SCROLL')) {
						if (GlobalModel.currentScalePercent.toFixed(3) <= GlobalModel.actualScalePercent.toFixed(3)) {
							clearTimeout(timeOutComp);
							$(objThis.element).trigger("swipepageright");
						}
					}
				}
			}

		},

		/**
		 * To calculate centroid of touches
		 */
		getCentroid : function(e) {
			var objThis = this;
			if (e.touches == undefined) {
				if( isWinRT )
				{
					return [e.pageX, e.pageY, 1];
				}
				return [e.pageX, e.pageY, 0];
			}
			var iTouches = e.touches.length;
			var x = 0;
			var y = 0;
			for (var i = 0; i < iTouches; i++) {
				x = x + e.touches[i].pageX;
				y = y + e.touches[i].pageY;
			}
			x = x / iTouches;
			y = y / iTouches;
			return [x, y, iTouches];
		},

		/**
		 * To calculate left of touches
		 */
		getLeft : function(/*JQuery*/jContent) {
			var jContent = $("#bookContentContainer2")
			var leftval = parseInt(jContent.css('left'), 10);
			if (isNaN(leftval)) {
				leftval = 0;
			}
			return leftval;
		},

		/**
		 * To calculate centroid of touches
		 */
		getTop : function(/*JQuery*/jContent) {
			var jContent = $("#bookContentContainer2")
			var topval = parseInt(jContent.css('top'), 10);
			if (isNaN(topval)) {
				topval = 0;
			}
			return topval;
		},

		/**
		 * To close keyboard on touch devices
		 */
		removeIpadKbd : function() {
			$(":input").blur();
		},

		/**
		 * If double tapped
		 */
		doubleTap : function(ctr, startX, startY) {
			if (ctr >= 2) {
				this.resetZoom();
			}
		},

		getElementTreeXPath : function(element) {
			var paths = [];

			// Use nodeName (instead of localName) so namespace prefix is included (if any).
			for (; element && element.nodeType == 1; element = element.parentNode) {
				var index = 0;
				for (var sibling = element.previousSibling; sibling; sibling = sibling.previousSibling) {
					// Ignore document type declaration.
					if (sibling.nodeType == Node.DOCUMENT_TYPE_NODE)
						continue;

					if (sibling.nodeName == element.nodeName)
						++index;
				}

				var tagName = element.nodeName.toLowerCase();
				var pathIndex = ( index ? "[" + (index + 1) + "]" : "");
				paths.splice(0, 0, tagName + pathIndex);
			}

			return paths.length ? "/" + paths.join("/") : null;
		},

		getElementXPath : function(element) {
			if (element && element.id)
				return '//*[@id="' + element.id + '"]';
			else
				return this.getElementTreeXPath(element);
		},

		/**
		 * To zoom in/out based on fraction value
		 */
		zoomInOut : function(fractionZoom) {
			///////////// for remove swipe functionality at the time of zoom
			if (ismobile) {
				var objScrollElement = $(this.element).find('[id=bookContentContainer2]');
				PageNavigationManager.removeSwipeListner(objScrollElement);
			}
			var objThis = this, thisMemVars = this.member;
			var nZoom = this.options.nScalePercent * (1 + fractionZoom);
			if (this.getScaleValue() > this.options.minZoom && this.getScaleValue() < this.options.maxZoom) {
				this.setZoom(nZoom);
			} else if (nZoom > this.options.maxZoom) {
				nZoom = this.options.maxZoom;
			} else if (nZoom < this.options.minZoom) {
				nZoom = this.options.minZoom;
			} else {
				this.setZoom(nZoom);
			}

		},

		removeDefaultSelection : function() {
			/**
			 * This block of code is used for removing selection while changinf the page.
			 * Starts
			 */
			if (window.getSelection) {
				if (window.getSelection().empty) {// Chrome
					window.getSelection().empty();
				} else if (window.getSelection().removeAllRanges) {// Firefox
					window.getSelection().removeAllRanges();
				}
			} else {
				if (document.selection) {// IE?
					document.selection.empty();
				}
			}
			if(!navigator.userAgent.match(/OS 8_0/))
			{
				$("body").append('<input type="text" id="inputToRemove" style="width:0;height:0" readonly/>');
				$("#inputToRemove").focus();
				$("#inputToRemove").blur();
				$("#inputToRemove").remove();
			}
			/****** Ends **/
		},

		setCurrentPageIndexForAnnotation : function(val) {
			this.options.pageIndexForAnnotation = val;
		}
		
	})
})(jQuery);
