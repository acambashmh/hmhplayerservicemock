
var bHidingHeader = false;
var hiddenTrue=true;
var check;
var disabledLeftMenuItems = [];
( function( $, undefined ) {
$.widget( "magic.buttonComp",  $.magic.magicwidget, 
{
events:
	{
		CLICK:'click',
		DBLCLICK: 'taphold'
	},
	data:
	{
		type:null
	},
	options: 
	{
		 setEnabled:true,
		 isEnabled: true
	},
	_create: function()
	{
		var _str = this.element.attr("id");
		
		$("#maskDiv").css("display", "block");

		$.extend(this.options, this.element.jqmData('options'));
 		this.data.type = this.element.attr("type");
		if(this.options.setEnabled==undefined)
		this.options.setEnabled=true;
		this.options.selected = false;
		if (this.options.toggle == undefined) {
			try
			{
				var strStyle = this.options.offstyle;
				strStyle = this.options.onstyle;
			}
			catch(e)
			{
				//console.log('Please set styles for offstyle and onstyle if button is toggle');
			}
			this.options.toggle = false;
		}
		
		if (this.options.setEnabled == true) { 
			/*$(this).bind("orientationchange", function(){
		 		//console.log("Orienation Change");
		 	})*/
		 	this.data.type = this.element.attr("type");
		 	var objClass = this;
			var ele = this.element;
				this.element.bind('click', function(e){
					
					if (objClass.options.isEnabled == true) {
					//alert(objClass.options.setEnabled);
                    /*
					 * Removes all popups on any button click
					 * same functionality implemented in butonComp, 
					 */
					//PopupManager.removePopup();
					HotkeyManager.findAndSetFocus($(objClass.element));
					objClass.onHandler();
					$(document).trigger("buttonCompClicked")
			 		e.stopPropagation();
					
					//ensuring that the branding bar is hidden despite event's propagation is stopped.
					if($("#header").css("display") == "block")
					{
						$(document).trigger("click")
					}
					
					if(objClass.options.toggle == true)
					{
						if(objClass.options.selected == true)
						{
							$(objClass.element).attr('class', objClass.options.offstyle)
							objClass.options.selected = false;
						}
						else
						{					
							$(objClass.element).attr('class', objClass.options.onstyle)
							objClass.options.selected = true;
						}
					}
				}
			 	});
			 	
			 	
			
			
			this.element.bind('taphold', function(e){
				
		 		objClass.onTapHold();
		 		e.stopPropagation();
		 	});
		 }else{
		 	this.disable();
		 }
	},
	_init: function()
	{
		$.magic.magicwidget.prototype._init.call(this);
	},
	onHandler: function(e)
	{
		$(this).trigger(this.events.CLICK, this.data);
		 
	},
	onTapHold: function(e)
	{
		//this.disable();
		$(this).trigger('taphold', this.data);
		 
	},
	
	enable: function() 
	{
		this.element.attr( "disabled", false );
		this.element.removeClass( "ui-disabled" ).attr( "aria-disabled", false );
		this.options.isEnabled=true;
		if(this.options.disabledstyle)
		{
			$(this.element).removeClass(this.options.disabledstyle);
		}
		return this._setOption( "disabled", false );
	},

	disable: function() 
	{
		this.element.attr( "disabled", true );
		this.element.addClass( "ui-disabled" ).attr( "aria-disabled", true );
		this.options.isEnabled=false;
		if(this.options.disabledstyle)
		{
			$(this.element).addClass(this.options.disabledstyle);
		}
		return this._setOption( "disabled", true );
	},
	
	setSelected: function(blnSelected)
	{
		
		this.options.selected = blnSelected;
		if(blnSelected == true)
		{
			$(this.element).removeClass(this.options.offstyle).addClass(this.options.onstyle);
			//$(this.element).attr('class', this.options.onstyle);
		}
		else
		{
			$(this.element).removeClass(this.options.onstyle).addClass(this.options.offstyle)
			//$(this.element).attr('class', this.options.offstyle);
		}
	},
	
	getElement: function()
	{
		return this.element;
	},
	
	getSelected: function()
	{
		return this.options.selected;
	},
	
	destroy: function()
	{
		/*for(var strEvent in this.events)
		{
			this.element.unbind(strEvent);
			$(this).unbind(strEvent);
		}*/
	}
});
})( jQuery );