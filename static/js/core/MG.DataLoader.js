/**
* @name DataLoader.js.
* @description this class is used to load a file.
* @author Magic
*/

MG.DataLoader = function(){

}

MG.DataLoader.prototype.load = function(strDataPath){
	// TO-DO: write code to load data
	var objClass = this;
	$.getJSON(strDataPath, function(objData){
		$(objClass).trigger('load_complete', objData);
	});
}
