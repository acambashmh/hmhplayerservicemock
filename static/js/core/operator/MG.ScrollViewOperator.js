/**
 * @author lakshay.ahuja
 */
var isAutoBookMarkLoaded = false;
MG.ScrollViewOperator = function() {
	this.updateBookmark = null
}

MG.ScrollViewOperator.prototype = new MG.BaseOperator();

MG.ScrollViewOperator.prototype.attachComponent = function(objComp) {
	if (objComp == null)
		return;

	var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();

	var objThis = this;
	switch(strType) {

		case AppConst.NOTES_ANNOTATION_BTN:
			objThis.notesBtnRef = $(objComp.element);
			$(objThis.notesBtnRef).bind('stickyNoteCreated', function(e) {
				//alert("scrollviewoperator called");
				var offset = $(e.currentTarget).offset();
				objThis.addNewComponentOnStage(objThis.scrollViewRef, AppConst.STICKYNOTE);
			});

			break;

		case AppConst.SCROLL_VIEW_CONTAINER:
			objThis.scrollViewRef = objComp;

			$(document).bind("autoBookmarkDataLoadComplete", function() {
			    isAutoBookMarkLoaded = true;
				if(!_objQueryParams.page){
				    if( $( "#stickyNoteContainer").css( "display" ) == "block" )
				    {
				       $( "#stickyNoteContainer").find( "[type=closeButton]" ).trigger( "click" ); 
				    }
				    objThis.doFunctionCall(AppConst.SCROLL_VIEW_CONTAINER, 'showPageAtIndex', GlobalModel.currentPageIndex, true);
				}
				if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
				{     
					$( $( '[ id = "bookContentContainer2" ] ' )[ 0 ] ).find( '[ type = addRemoveBookmarkStrip ]' ).addClass('ui-disabled').attr( "aria-disabled", true );      	
				}	
			});
			//objThis.initAuthorablePageShowroom();
			
			$(objComp).bind(objComp.events.PAGE_INDEX_CHANGE, function() {
			    if(isAutoBookMarkLoaded == true && !( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData ) ){
                    if (objThis.updateBookmark) {
                        clearTimeout(objThis.updateBookmark);
                    }
                    objThis.updateBookmark = setTimeout(function() {
                        var nIndex = $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagebreakvalue');
                        ServiceManager.Annotations.update(8, nIndex);
                    }, 1500); 
			    }
				
				if( GlobalModel.BookEdition == "TSE" && GlobalModel.selectedStudentData )
				{     
					$( $( '[ id = "bookContentContainer2" ] ' )[ 0 ] ).find( '[ type = addRemoveBookmarkStrip ]' ).addClass('ui-disabled').attr( "aria-disabled", true );      	
				}
				var _objPagedata = {};
	            _objPagedata.type = AppConst.PAGE_DATA;
	            _objPagedata.minTime = "true";
	            setTimeout(function(){
			           $(document).trigger( AppConst.SEND_TRACKING_STATEMENT, [_objPagedata] );
			           //AnalyticsManager.setPageData(strPageID, strPageLabel, iTimeSpentOnPage + "");
			    }, 500);

				objThis.doFunctionCall(AppConst.STANDARD_PANEL, 'toggleBtnState');
			});

			break;

		case AppConst.SEC_PANEL_CLOSE_BTN:
			// On close btn click, the opened panel will close
			$(objComp).unbind('click').bind('click', function() {
				$(objThis.scrollViewRef).trigger('closeOpenedPanel')
			});
			break;

		default:
			//console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
			break;
	}

	return MG.BaseOperator.prototype.attachComponent.call(this, objComp);
}

MG.ScrollViewOperator.prototype.addNewComponentOnStage = function(objContentshowroom, strCompName) {
	//objContentshowroom.addNewComponentOnStage(strCompName);
    var newComponentDiv = $('<div id="btnStickyNote" type="stickyNoteComp" style="position:absolute;" class="btnStickyNoteHotspot" moveable="true" authorable="true"  data-options={"expandedWidth":"51","expandedHeight":"50","width":"43","height":"41"}></div>');
    var strCompMethod = $(newComponentDiv).attr("data-role");
    var scrollviewcompRef = $("#bookContentContainer").data('scrollviewcomp');
    var pageNum = $($(newComponentDiv)[0].outerHTML).attr("associated-page-number");
    if (pageNum == undefined)
        pageNum = scrollviewcompRef.options.pageIndexForAnnotation;

    $("#bookContentContainer2").find('[id="PageContainer_' + pageNum + '"]').append(newComponentDiv);

    OperatorManager.initializeAllOperators($(newComponentDiv));
}

