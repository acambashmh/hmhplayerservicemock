/**
 * @author Hapreet S. Saluja
 * This operator handles communication between panel's button and panel itself
 */
MG.PanelViewOperator = function(){
    this.objPreviousPanel = null;
    this.superClass = MG.BaseOperator.prototype;
    this.panelTobeClosed = null;
    this.objMaskDiv = null;
    this.arrOpenablePanels = [];
    this.currentPanel = null;
    this.scrollViewRef = null;
    this.panelClickEnabled = true;
    
    GlobalModel.panelViewOperator = this;
}

// extend super class
MG.PanelViewOperator.prototype = new MG.BaseOperator();

/**
 * This function defines the functionality of components to be executed.
 * @param1 {Object} Object of data-role an element is binded with
 * @return {Object}
 */
MG.PanelViewOperator.prototype.attachComponent = function(objComp){
    if (objComp == null) 
        return;
    
    var objThis = this;
    var strType = objComp.element.attr(AppConst.ATTR_TYPE).toString();
    
    switch (strType) {
    
        case AppConst.EXTERNAL_TOC_LAUNCHER:
        
            this.arrOpenablePanels.push(objComp);
            
            //add click handlers to tool-bar buttons
            $(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e, strId){
                //$("#tocMainPanel").trigger('tocPanelLaunched');
                $("#tocGotoBtn").addClass('ui-disabled');
                objThis.panelClickHandler(objComp);
            });
            break;
        case AppConst.RESOURCE_PANEL_LAUNCHER:
        case AppConst.BOOKMARK_SECTION_LAUNCHER:
            this.arrOpenablePanels.push(objComp);
            
            //add click handlers to tool-bar buttons
            $(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e, strId){
            
                objThis.panelClickHandler(objComp);
            });
            break;
            
        case AppConst.STICKYNOTE_LAUNCHER:
            
            this.arrOpenablePanels.push(objComp);
            
            //add click handlers to tool-bar buttons
            $(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e, strId){
            
                /*
             if(objComp.options.selected == true){
             $("#stickyNoteNumberDiv").css('color', '#537c87');
             }
             else{
             $("#stickyNoteNumberDiv").css('color', '#fff');
             }
             */
                objThis.panelClickHandler(objComp);
            });
            break;
            
        case AppConst.SCROLL_VIEW_CONTAINER:
            objThis.scrollViewRef = objComp;
            $(objComp).bind('closeOpenedPanel', function(){
                objThis.panelClickHandler();
            });
            
            break;
            
        case AppConst.ZOOM_PANEL_LAUNCHER_BUTTON:
            this.arrOpenablePanels.push(objComp);
            $(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e, strId){
            	if( $(objComp.element).hasClass("zoom-on") )
            	{
            		HotkeyManager.pageViewFocusInHandler();
            	}
            	else
            	{
					HotkeyManager.currentActiveElementGroup = $("#annotationPanel")[0];
            	}
                $(objThis.scrollViewRef).trigger('resetZoomButtons',GlobalModel.currentScalePercent);
                objThis.doFunctionCall(AppConst.ZOOM_PANEL_COMP, "updatePosition")
                objThis.panelClickHandler(objComp);
            });
            break;
        case AppConst.ZOOM_PANEL_COMP:
            //DO NOTHING
            break;
        case AppConst.TOC_MAIN_PANEL:
            //DO NOTHING
            break;
        case "helpPanelLauncherBtn":
        	this.arrOpenablePanels.push(objComp);
            $(objComp).unbind("click").bind("click", function(){
                objThis.doFunctionCall("helpPanelComp", "launchPanel")
                objThis.panelClickHandler(objComp);
            });
        	break;
        case AppConst.RESOURCES_MAIN_PANEL:
            //DO NOTHING
            break;    
        case AppConst.STANDARDS_MAIN_PANEL:
            
            break;
        case AppConst.STANDARD_PANEL_LAUNCHER:
              this.arrOpenablePanels.push(objComp);
            $(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e, strId){
                objThis.panelClickHandler(objComp);
                
            });
            break;
        case AppConst.PRINT_PANEL_LAUNCHER_BUTTON:
            this.arrOpenablePanels.push(objComp);
            $(objComp).unbind(objComp.events.CLICK).bind(objComp.events.CLICK, function(e, strId){
            	$(".currentPageinfo").html("Current Page: <span class='currentPageNumber'>" + $($('[type=PageContainer]')[GlobalModel.currentPageIndex]).attr('pagebreakvalue') + "</span>");
            	objThis.panelClickHandler(objComp);
                
            });
            
            break;
        case AppConst.PRINT_PANEL_COMP:
        	$(objComp).bind('closeOpenedPanel', function(){
                objThis.panelClickHandler();
            });
            //DO NOTHING
            break;
        default:
            //console.error('### ERROR: ['+ strType +'] -- Either "type" is not defined or Invalid operator is assigned');
            //return false;
            break;
    }
    
    return this.superClass.attachComponent.call(this, objComp);
}

/**
 * This function handles when a panel's button is clicked in the sidebar.
 * @param1 {Object} Object of data-role an element is binded with
 * @return void
 */
MG.PanelViewOperator.prototype.panelClickHandler = function(objComp){
    var objThis = this;

	if($("#studentPanel").css("display") == "block" && EPubConfig.ReaderType == '6TO12' && isMyClassClicked == false )
	{
		$("#studentPanel").css("display","none");
		$(".teacherStudentArrow").removeClass("teacherStudentArrowExpanded");
		$(".teacherStudentArrow").removeClass("teacherArrowUp");
	}
    if($("#searchPanel").css("display") == "block" && EPubConfig.ReaderType == '6TO12' && isSearchClicked == false)
	{
    	$("#searchPanel").css("display","none");
		$("#navSearchBtn").removeClass("active");
	}
	isSearchClicked = false;
	isMyClassClicked = false;
	
    if(EPubConfig.QAPanel_isAvailable == true || EPubConfig.Notes_isAvailable == true)
    	{
    		$("#NotesPanel").css("display", "block");
    	}
    var slideTimeout = 500;
    if(EPubConfig.ReaderType == '2TO5' || EPubConfig.ReaderType == 'PREKTO1'){
        slideTimeout = 750;
    }
    
    if(objThis.panelClickEnabled == false)
    {
    	if(objComp)
    		objComp.setSelected(!objComp.getSelected())
    	return;
    }
    
    objThis.panelClickEnabled = false;
    setTimeout(function(){
    	objThis.panelClickEnabled = true;
    }, 200);
    	
    $("ul.dd-options").css("display", "none");
    $("#tocGotoInputBox").val('');
    var fnBookSectionClickHandler = function(event){
        if (objComp.getSelected() == true) {
            objThis.panelClickHandler();
        }
    }
    
    for (var i = 0; i < this.arrOpenablePanels.length; i++) {
        var objItem = this.arrOpenablePanels[i];
        
        var strType = $("#" + objItem.options.panelViewComp).attr("type");
        
        
        
        if (objComp == objItem) {
            var strVisibility = (objItem.getSelected() == true) ? "none" : "block";
           
            //$("#" + objItem.options.panelViewComp).css("display", strVisibility);
            //$("#" + objItem.options.panelViewComp).toggle("slide",500);
            if(strVisibility == "block") {
				 
                objThis.addBoxShadowToPanel($("#" + objItem.options.panelViewComp));

                if (strType == 'NotesSectionPanel')
                {
                    GlobalModel.updateTeacherNotificationIcon('teacherNoteAlert');
                }

            	objThis.doFunctionCall($("#" + objItem.options.panelViewComp).attr('id'), "onPanelOpen");
            	$("#" + objItem.options.panelViewComp).stop(false,true).animate({width: "show"}, slideTimeout, "easeInOutExpo", function()
            	{
            		objThis.addBoxShadowToPanel($(this));
            		
            	})
            	   $("#bookContentContainer").unbind('mousedown', fnBookSectionClickHandler).bind('mousedown', fnBookSectionClickHandler);
            	   $("#questionAnswerPanel").unbind('mousedown', fnBookSectionClickHandler).bind('mousedown', fnBookSectionClickHandler);
                if($("#annotation-bar").css("display") == "block")
                	PopupManager.removePopup('annotation-bar');
            }
            else {
            	//adding try catch because all panels are not extending TabViewComp
            	try
	            {
            	    $("#bookContentContainer").unbind('mousedown', fnBookSectionClickHandler);
            	    $("#questionAnswerPanel").unbind('mousedown', fnBookSectionClickHandler)
            	    if (strType == 'NotesSectionPanel') {
            	        GlobalModel.updateTeacherNotificationIcon('teacherNoteAlertleft');
            	    }

	            	objThis.doFunctionCall(strType, "beforePanelClose");
	            }
	            catch(e) {}
            	
            	$("#" + objItem.options.panelViewComp).stop(false,true).animate({width: "hide"}, slideTimeout, "easeInOutExpo", function(e)
            	{	
            		objThis.removeBoxShadowFromPanel($(this));
            		objThis.doFunctionCall(this.id, "onPanelClose");
            	})
            }
            	
           	if(this.currentPanel == null)
           		this.currentPanel = objItem;
           	else
           	{
           		this.currentPanel = null;
           	}
           	
        }
        else {
        
            var strType = $("#" + objItem.options.panelViewComp).attr("type");
            objItem.setSelected(false);
            
            if (strType == 'NotesSectionPanel') {
                GlobalModel.updateTeacherNotificationIcon('teacherNoteAlertleft');
            }

            //adding try catch because all panels are not extending TabViewComp
            try
            {
            	objThis.doFunctionCall(strType, "beforePanelClose");
            }
            catch(e) {}
            if( !objComp && !$("#zoomBtn").hasClass("zoom-on") && EPubConfig.ReaderType.toUpperCase() != "PREKTO1")
        	{
        		HotkeyManager.pageBodyFocusInHandler();
        	}
            if(objComp == undefined)
            {               

            	$("#" + objItem.options.panelViewComp).stop(false,true).animate({width: "hide"}, slideTimeout, "easeInOutExpo", function()
            	{            	    
            		objThis.removeBoxShadowFromPanel($(this));
            		objThis.doFunctionCall(this.id, "onPanelClose");
            	})
            	
            }
            else
            {
            	$("#" + objItem.options.panelViewComp).css("display", "none");
            	objThis.removeBoxShadowFromPanel($("#" + objItem.options.panelViewComp));            	
            	objThis.doFunctionCall(strType, "onPanelClose");
            }

            

            if($("#annotation-bar").css("display") == "block")
            	PopupManager.removePopup('annotation-bar');
            	/* */
        }
    }
}

MG.PanelViewOperator.prototype.addBoxShadowToPanel = function(objPanel)
{
	//adding box shadow on panel open
	if (objPanel.attr("hasBoxShadow") == "") {
		objPanel.addClass("panel-box-shadow");
	} else {
		objPanel.find("[hasBoxShadow='']").addClass("panel-box-shadow");
	}
}

MG.PanelViewOperator.prototype.removeBoxShadowFromPanel = function(objPanel)
{
	if (objPanel.attr("hasBoxShadow") == "") {
		objPanel.removeClass("panel-box-shadow");
	} else {
		objPanel.find("[hasBoxShadow='']").removeClass("panel-box-shadow");
	}
}