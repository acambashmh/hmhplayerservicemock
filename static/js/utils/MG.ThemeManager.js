/**
 * @author magic.softwate
 */
var ThemeManager = {
 	
	grade: AppConst.GRADE_K2,
	deviceType: "",
	
	getLayoutType: function()
	{
		var strGradeType = "";
		
		
		switch(EPubConfig.ReaderType)
		{
			case AppConst.GRADE_2TO5:
                strGradeType = AppConst.GRADE_2TO5;
				break;
			
			case AppConst.GRADE_PREKTO1:
                strGradeType = AppConst.GRADE_PREKTO1;
				break;
			
		}
		
		//alert(strDeviceType);
		return strGradeType;
	}
	
};
