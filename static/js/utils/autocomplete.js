/**
 * @author harpreet.saluja
 * This file overrides some methods of auto-complete feature
 */

$.ui.autocomplete.prototype._renderMenu = function(ul, items) {
	$('.ui-autocomplete').css("padding", "0px");
	$(ul).append('<div class="searchPopupShadow"><div class="searchPopupBG"><div class="searchSuggestionDiv"><span class="leftFloat SearchHeading clear">' + GlobalModel.localizationData["AUTO_COMPLETE_SUGGESTION_TEXT"] + '</span><div class="seperator clear"></div><div class="autocomplete-items-container"></div></div></div><div>')
	$(ul).append('<div class="autocomplete-container-arrow"><div>')
	var list = $('.autocomplete-items-container');
	var self = this;

	//adding auto-complete items in auto-complete items container
	$.each(items, function(index, item) {
		self._renderItem(list, item);
	});

	//because the auto complete structure has been changed, the click on items does not automatically work. Therefore, handling the click explicitly;
	list.find("a").unbind("click").bind('click', function() {
		$(self.element).val(this.innerHTML);
		$('.ui-autocomplete').css("display", "none");
		$(self.element).focus();
		
		//unbinding click event from current list items;
		//list.find("a").unbind("click");
	})

	$('.searchPopupShadow').css("width", ($(this.element).width() + 45));
	
	var setAutocompleteContainerPosition = function()
	{
		$('.ui-autocomplete').css("top", $(self.element).offset().top + 10);
		$('.ui-autocomplete').css("left", $(self.element).offset().left);
	}
	
	$(window).bind('resize', setAutocompleteContainerPosition);
	$(window).bind('orientationchange', setAutocompleteContainerPosition);
}

$.ui.autocomplete.prototype._renderItem = function(ul, item) {
	return $('<li class="ui-menu-item"></li>').append("<a>" + item.label + "</a>").appendTo(ul);
}; 