/**
 *  Version 2.1
 *      -Contributors: "mindinquiring" : filter to exclude any stylesheet other than print.
 *  Tested ONLY in IE 8 and FF 3.6. No official support for other browsers, but will
 *      TRY to accomodate challenges in other browsers.
 *  Example:
 *      Print Button: <div id="print_button">Print</div>
 *      Print Area  : <div class="PrintArea"> ... html ... </div>
 *      Javascript  : <script>
 *                       $("div#print_button").click(function(){
 *                           $("div.PrintArea").printArea( [OPTIONS] );
 *                       });
 *                     </script>
 *  options are passed as json (json example: {mode: "popup", popClose: false})
 *
 *  {OPTIONS} | [type]    | (default), values      | Explanation
 *  --------- | --------- | ---------------------- | -----------
 *  @mode     | [string]  | ("iframe"),"popup"     | printable window is either iframe or browser popup
 *  @popHt    | [number]  | (500)                  | popup window height
 *  @popWd    | [number]  | (400)                  | popup window width
 *  @popX     | [number]  | (500)                  | popup window screen X position
 *  @popY     | [number]  | (500)                  | popup window screen Y position
 *  @popTitle | [string]  | ('')                   | popup window title element
 *  @popClose | [boolean] | (false),true           | popup window close after printing
 *  @strict   | [boolean] | (undefined),true,false | strict or loose(Transitional) html 4.01 document standard or undefined to not include at all (only for popup option)
 */
(function($) {
    var counter = 0;
    var modes = { iframe : "iframe", popup : "popup" };
    var defaults = { mode     : modes.iframe,
                     popHt    : 500,
                     popWd    : 400,
                     popX     : 200,
                     popY     : 200,
                     popTitle : '',
                     popClose : false };

    var settings = {};//global settings

    $.fn.printArea = function( options )
    {
        $.extend( settings, defaults, options );

        counter++;
        var idPrefix = "printArea_";
        $( "[id^=" + idPrefix + "]" ).remove();
        var ele = getFormData( $(this) );

        settings.id = idPrefix + counter;

        var writeDoc;
        var printWindow;

        switch ( settings.mode )
        {
            case modes.iframe :
                var f = new Iframe();
                writeDoc = f.doc;
                printWindow = f.contentWindow || f;
                break;
            case modes.popup :
                printWindow = new Popup();
                writeDoc = printWindow.doc;
        }

        writeDoc.open();
        
        if(settings.cssfile) {
            writeDoc.write( docType() + "<html>" + getHeadLand() + getBody(ele) + "</html>" );          
        } else {
            writeDoc.write( docType() + "<html>" + getHead() + getBody(ele) + "</html>" );  
        }
        
        
       // $(writeDoc).find("#" + $(ele).attr('id')).width(EPubConfig.pageWidth * widthRatio);
       // $(writeDoc).find("#" + $(ele).attr('id')).height(EPubConfig.pageHeight * heightRatio);
        
        
       //console.log(EPubConfig.pageWidth * widthRatio,$(writeDoc).find("#" + $(ele).attr('id')).width());
        //$.mobile.loadingMessage = 'prepare for printing';
        //$.mobile.showPageLoadingMsg("b", 'message');
        var timeOut = setTimeout(function(){
              //$.mobile.hidePageLoadingMsg("b", 'prepare for printing',false);  
              //$.mobile.loadingMessage = 'Loading'; 
             writeDoc.close();
        
            printWindow.focus();
             printWindow.print();

    },1000)

       

        if ( settings.mode == modes.popup && settings.popClose )
            printWindow.close();
    }

    function docType()
    {
        if ( settings.mode == modes.iframe || !settings.strict ) return "";

        var standard = settings.strict == false ? " Trasitional" : "";
        var dtd = settings.strict == false ? "loose" : "strict";
        return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01' + standard + '//EN" "http://www.w3.org/TR/html4/' + dtd +  '.dtd">';
    }

    function getHead()
    {
        var head = "<head><title>" + settings.popTitle + "</title>";
        $(document).find("link")
            .filter(function(){
                    return $(this).attr("rel").toLowerCase() == "stylesheet";
                })
            .filter(function(){ // this filter contributed by "mindinquiring"
                    var media = $(this).attr("media");
                    if (media != undefined) {
                        return (media.toLowerCase() == "all" || media.toLowerCase() == "print")
                    }
                })
            .each(function(){
                    head += '<link type="text/css" rel="stylesheet" href="' + $(this).attr("href") + '" >';
                });
        head += "</head>";
        return head;
    }
    
    function getHeadLand()
    {
        var head = "<head><title>" + settings.popTitle + "</title>";
        $(document).find("link")
            .filter(function(){
                    return $(this).attr("rel").toLowerCase() == "stylesheet";
                })
            .filter(function(){ // this filter contributed by "mindinquiring"
                    var media = $(this).attr("media");
                    if (media != undefined) {
                        return (media.toLowerCase() == "all" || media.toLowerCase() == "print")
                    }
                })
            .each(function(){
                    head += '<link type="text/css" rel="stylesheet" href="' + $(this).attr("href") + '" >';
                });
                var cssHref = getPathManager().appendReaderPath('css/print_land.css'); // to solve print preview problem
                head += '<link type="text/css" rel="stylesheet" href='+cssHref+'>';
                 head += "</head>";
                 return head;
    }

    function getBody( printElement )
    {     
              //alert('<body><div id= "'+ $(printElement).attr("id") +'"class="' + $(printElement).attr("class") + '">' + $(printElement).html() + '</div></body>');
              //return false;
             // marginLeft = 0;
              var pgWidth = currentPageWidth;
              var marginLeft;
              var marginTop = 0;
              var isChrome = !!window.chrome;
              marginLeft = (frameWidth - (currentPageWidth * finalRatio))/2;
               if(EPubConfig.pageView == "doublePage"){
                  pgWidth = 2 * (currentPageWidth + 10);
                  marginLeft = 0;
                  marginTop = (frameHeight - (pgWidth * finalRatio))/2;
                    if (isChrome) {
                        marginLeft = 0;
                        marginTop = 0;
                        var temp = frameWidth;
                        frameWidth = frameHeight;
                        frameHeight = temp - 100;
                    }
              }
              if(marginLeft < 0){
                  marginLeft = 1;
              }
              if(marginTop < 0){
                  marginTop = 1;
              }
              //console.log(currentPageHeight * finalRatio,frameHeight)
        if (isChrome) {      
        	return '<body style="overflow:hidden;width: ' + frameWidth +'px; height: ' + frameHeight +'px;" align="center"><div style="margin-top: '+ marginTop +'px; margin-left: '+ marginLeft + 'px;" id= "'+ $(printElement).attr("id") +'" class="' + $(printElement).attr("class") + '">' + $(printElement).html() + '</div></body>';
        }
        else{
        	return '<body style="width: ' + frameWidth +'px; height: ' + frameHeight +'px;" align="center"><div style="margin-top: '+ marginTop +'px; margin-left: '+ marginLeft + 'px; width: ' + pgWidth * finalRatio + 'px; height: ' +currentPageHeight * finalRatio  +'px;" id= "'+ $(printElement).attr("id") +'" class="' + $(printElement).attr("class") + '">' + $(printElement).html() + '</div></body>';
        }
    }

    function getFormData( ele )
    {
              $(ele).find("textarea").each(function(){
                        var v = $(this).val();
                        if ($.browser.mozilla)
                        {
                              if (this.firstChild) {
                                        this.firstChild.textContent = v;
                              }
                              else
                              { 
                                        this.textContent = v;
                              }
                        }
                        else this.innerHTML = v;
              })
              
              $(ele).find("[type=text],[type=select],textarea").each(function(){
                    // In cases where radio, checkboxes and select elements are selected and deselected, and the print
                    // button is pressed between select/deselect, the print screen shows incorrectly selected elements.
                    // To ensure that the correct inputs are selected, when eventually printed, we must inspect each dom element
                    
                    var type = $(this).attr("type");
                    if ( type == "radio" || type == "checkbox" )
                    {
                              if ( $(this).is(":not(:checked)") ) {
                                        this.removeAttribute("checked");
                              }
                              else 
                              {
                                        this.setAttribute( "checked", true );
                               }
                       }
                       else if ( type == "text" ) {
                                 this.setAttribute( "value", $(this).val() );
                       }
                       else if ( type == "select-multiple" || type == "select-one" )
                       {
                                 $(this).find( "option" ).each( function() {
                                           if ( $(this).is(":not(:selected)") ) 
                                           {
                                                     this.removeAttribute("selected");
                                           }
                                           else 
                                           {
                                                     this.setAttribute( "selected", true );
                                           }
                                  });
                       }
                       else if ( type == "textarea" )
                       {
                              var v = $(this).attr( "value" );
                              if ($.browser.mozilla)
                              {
                                        if (this.firstChild) this.firstChild.textContent = v;
                                        else this.textContent = v;
                               }
                               else this.innerHTML = v;
                       }
               });
               return ele;
    }

    function Iframe()
    {
        var frameId = settings.id;
        var iframeStyle = 'border:0;position:absolute;width:0px;height:0px;left:0px;top:0px;';
        var iframe;

        try
        {
            iframe = document.createElement('iframe');
            document.body.appendChild(iframe);
            $(iframe).attr({ style: iframeStyle, id: frameId, src: "" });
            iframe.doc = null;
            iframe.doc = iframe.contentDocument ? iframe.contentDocument : ( iframe.contentWindow ? iframe.contentWindow.document : iframe.document);
        }
        catch( e ) { 
        throw e + ". iframes may not be supported in this browser."; 
        }

        if ( iframe.doc == null ) throw "Cannot find document.";
        return iframe;
    }

    function Popup()
    {
        var windowAttr = "location=yes,statusbar=no,directories=no,menubar=no,titlebar=no,toolbar=no,dependent=no";
        windowAttr += ",width=" + settings.popWd + ",height=" + settings.popHt;
        windowAttr += ",resizable=yes,screenX=" + settings.popX + ",screenY=" + settings.popY + ",personalbar=no,scrollbars=no";

        var newWin = window.open( "", "_blank",  windowAttr );

        newWin.doc = newWin.document;
        return newWin;
    }
})(jQuery);
