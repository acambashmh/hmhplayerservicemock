
    window.annServiceTokenGenerator = window.annServiceTokenGenerator || {};
    var annServiceTokenGenerator = window.annServiceTokenGenerator;

    annServiceTokenGenerator.getToken = function(params, successHandler, errorHandler) {
        //userId, role,
        
        if (!params.userId || !params.role || !params.platform) {
            var msg = "Error: UserId, role and platfrom need to be specified in order to use service token generator.";
            if (errorHandler) errorHandler(msg);
            else consoleLog(msg);
            return;
        }
        if (!params.userName) {
            params.userName = "userName";
        }
        if (!params.issuer) {
            params.issuer = "https://my.hrw.com";
        }
        if (!params.expires) {
            params.expires = 86400;
        }
        if (!params.audience) {
            params.audience = "http://www.hmhco.com";
        }
        
		if (!$.support.cors) $.support.cors = true;
        $.ajax({
            url: "http://dubv-dpdiweb01.dubeng.local:234/TokenGenerator/GetJwt?userName=" + params.userName + "&userId=" + params.userId + "&orgId=undefined&role=" + params.role + "&platform=" + params.platform + "&audience=" + params.audience + "&issuer=" + params.issuer + "&expiresIn=" + params.expires + "&_=136413832935310",
            contentType: 'text/plain',
            dataType: 'text',
            type: 'GET',
            cache: false,
            crossDomain: true,
            success: function (data) {
                successHandler(data);
            },
            error: function (xhr, textStatus, error) {
                errorHandler(textStatus);
                //alert("Error while getting security token for user " + userId);
            }
        });
    };