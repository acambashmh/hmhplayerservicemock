#!/usr/bin/env bash
scp -r ./app/* hmhCertNode1:/var/www/mockElaServer/app/
scp -r ./app/* hmhCertNode2:/var/www/mockElaServer/app/

scp -r ./static/js/* hmhCertNode1:/var/www/mockElaServer/static/js/
scp -r ./static/js/* hmhCertNode2:/var/www/mockElaServer/static/js/

scp -r ./static/mdsData/* hmhCertNode1:/var/www/mockElaServer/static/mdsData/
scp -r ./static/mdsData/* hmhCertNode2:/var/www/mockElaServer/static/mdsData/

ssh hmhCertNode1 forever restartall
ssh hmhCertNode2 forever restartall
