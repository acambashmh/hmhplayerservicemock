/**
 * Created by cambasa on 25/03/2015.
 */
var assert = require("assert");
var _ = require('lodash');
var requiredPropertiesRule =  require('../app/RMI/MetadataValidation/requiredPropertiesRule');
var resourceItemLevelRule = require('../app/RMI/MetadataValidation/resourceItemLevelRule');
var resourceItem = require('./helperFiles/validationFiles/resourceItem1.json');
var resourceItemValidator = require('../app/RMI/MetadataValidation/validateFlatRow');
var validateResourceList = require('../app/RMI/MetadataValidation/validateResourceList');

describe('Resource validation rules', function(){

    it('should validate true for resource item for required properties', function(){

        var resourceItemValidationResult = {
            messages:[],
            valid: true
        };

        var result = requiredPropertiesRule(resourceItem, resourceItemValidationResult);
        assert.equal(true, result.valid);

    });

    it('should validate false for  resource item for required properties ', function(){

        var resourceItemValidationResult = {
            messages:[],
            valid: true
        };
        var testResourceItem = _.clone(resourceItem, true);

        testResourceItem.HMH_ID = 1;
        testResourceItem.id = null;

        var result = requiredPropertiesRule(testResourceItem, resourceItemValidationResult);
        assert.equal(false, result.valid);
        assert.equal(2, result.messages.length);
    });

    it('should positively validate resource item for level rules', function(){

        var resourceItemValidationResult = {
            messages:[],
            valid: true
        };
        var testResourceItem = _.clone(resourceItem, true);

        var result = resourceItemLevelRule(testResourceItem, resourceItemValidationResult);
        assert.equal(true, result.valid);
    });

    it('should fail validation of resource item for level rules', function(){

        var resourceItemValidationResult = {
            messages:[],
            valid: true
        };
        var testResourceItem = _.clone(resourceItem, true);
        testResourceItem.L5_title = null;
        testResourceItem.L2_type = null;
        var result = resourceItemLevelRule(testResourceItem, resourceItemValidationResult);
        assert.equal(false, result.valid);
        assert.equal(2, result.messages.length);
    });

    it('should succeed to validate resource item with all rules', function(){

        var testResourceItem = _.clone(resourceItem, true);
        var result = resourceItemValidator(testResourceItem);
        assert(result.resourceId);
        assert.equal(true, result.valid);
    });

    it('should fail to validate resource item with all rules', function(){

        var testResourceItem = _.clone(resourceItem, true);
        testResourceItem.L5_title = null;
        testResourceItem.HMH_ID = true;
        var result = resourceItemValidator(testResourceItem);
        assert.equal(false, result.valid);
        assert.equal(2, result.messages.length);

    });

    it('should succeed to validate resource items with all rules', function(){

        var testResourceItem = _.clone(resourceItem, true);
        var result = validateResourceList([testResourceItem]);
        assert.equal(true, result.valid);
    });

    it('should fail to validate resource items with all rules, because the list is empty', function(){
        var result = validateResourceList([]);
        assert.equal(false, result.valid);
    });

    it('should fail to validate resource items with all rules, because not all items are valid', function(){
        var testResourceItem = _.clone(resourceItem, true);
        testResourceItem.HMH_ID = 123;

        var result = validateResourceList([testResourceItem]);
        assert.equal(false, result.valid);
        assert.equal(1, result.resourceItemsValidationResults.length);
    });

});

