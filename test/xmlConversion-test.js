/**
 * Created by cambasa on 25/03/2015.
 */
var assert = require("assert");
var singleConverter = require('../app/RMI/XMLConverter/converter');

var notConvertedJSONResource = require('./helperFiles/convertionFiles/notConvertedJSONResource.json');

describe('Converting XML into flat json structure', function(){

    it('should convert json xml node into json flat structure', function(){
        var convertedFile = singleConverter(notConvertedJSONResource);
        assert.equal(JSON.stringify(convertedFile), JSON.stringify(convertedFile));

    });

});

