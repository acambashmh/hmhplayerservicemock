var rmiInitClass = require('../app/RMI/rmiInit');
var assert = require("assert");
var chai = require('chai');
var chaiAsPromised = require("chai-as-promised");
var q = require('q');
var fs = require('fs');

chai.use(chaiAsPromised);
chai.should();


var svnTestPath = 'https://DUB-LP013651.pubedu.hegn.us:8443/svn/metadata-load';


describe('Handling parsing message from svn', function () {
    var svnConfig = {
        svnServer: 'https://DUB-LP013651.pubedu.hegn.us:8443/svn/metadata-load'
    };
    var rmiInit = new rmiInitClass(svnConfig);



    it('parse the message from svn successfuly 3 paths ', function () {

        var messageFromSvn = {'$msg': 'A   trunk/config.txt,U   trunk/dd.txt,U   trunk/fortesting.txt'};
        var svnLocation = 'https://DUB-LP013651.pubedu.hegn.us:8443/svn/metadata-load';

        var result = rmiInit.parseCommitMessage(messageFromSvn.$msg);
        var compare = [
            svnLocation + '/trunk/config.txt',
            svnLocation + '/trunk/dd.txt',
            svnLocation + '/trunk/fortesting.txt'
        ];

        for (var i = 0; i < compare.length; i++) {
            assert.equal(compare[0], result[0]);
        }

    });

    it('parse the message from svn successfuly one path ', function () {

        var messageFromSvn = {'$msg': 'A   trunk/config.txt'};
        var svnLocation = 'https://DUB-LP013651.pubedu.hegn.us:8443/svn/metadata-load';

        var result = rmiInit.parseCommitMessage(messageFromSvn.$msg);
        var compare = [
            svnLocation + '/trunk/config.txt'
        ];

        for (var i = 0; i < compare.length; i++) {
            assert.equal(compare[0], result[0]);
        }

    });

    it('parse the message from svn successfuly one path , with added ,', function () {
        var messageFromSvn = {'$msg': 'A   trunk/config.txt,'};

        var result = rmiInit.parseCommitMessage(messageFromSvn.$msg);
        var compare = [
            svnTestPath + '/trunk/config.txt'
        ];

        for (var i = 0; i < compare.length; i++) {
            assert.equal(compare[0], result[0]);
        }

    });

    it('parsing the empty message will return empty array', function () {
        var messageFromSvn = {'$msg': ''};
        var result = rmiInit.parseCommitMessage(messageFromSvn.$msg);
        assert.equal(0, result.length);
    });

    it('parsing nothing will return empty array', function () {
        var messageFromSvn = {'$msg': ''};
        var result = rmiInit.parseCommitMessage();
        assert.equal(0, result.length);
    });

});

describe('Checking if received files from svn are needed', function () {

    it('should successfuly compare the list of files with the ones in the config', function () {
        var compare = [
            '/trunk/config.txt',
            '/trunk/dd.txt',
            '/trunk/fortesting.txt'
        ];

        var svnTestFiles = [
            {
                id: 'config',
                properties: {
                    test: 'sasa'
                }
            },
            {
                id: 'dd',
                properties: {
                    test: 'sasa'
                }
            }
        ];
        var rmiInitForNeededFileCheck = new rmiInitClass('', svnTestFiles);
        var result = rmiInitForNeededFileCheck.checkForNeededFiles(compare);

        assert.equal(result.length, 2);
        assert.equal(result[0].svnPath, '/trunk/config.txt');
        assert.equal(result[1].svnPath, '/trunk/dd.txt');
    });

    it('should return empty list of needed files if no files were passed to the compare function', function () {
        var svnTestFiles = [
            'config',
            'dd'
        ];
        var rmiInitForNeededFileCheck = new rmiInitClass('', svnTestFiles);
        var result = rmiInitForNeededFileCheck.checkForNeededFiles([]);
        assert.equal(result.length, 0);
    });

});


describe('Check the integrated functionality of svn message handling', function () {
    this.timeout(15000);

   function mockgetFileFromSvn() {
        var defer = q.defer();
        setTimeout(function () {
            defer.resolve({
                svnPath: './test/helperFiles/handleCommitFiles/mds_resources_9780544087675.xml',
                filePath: './test/helperFiles/handleCommitFiles/mds_resources_9780544087675.xml',
                id: 'config',
                properties: {
                    test: 'sasa'
                }
            })
        }, 1000);

        return defer.promise;
    }

    var svnTestFiles = [
        {
            id: 'config',
            properties: {
                test: 'sasa'
            }
        }
    ];
    var rmiInit = new rmiInitClass(svnTestPath, svnTestFiles);
    rmiInit.getFileFromSvn = mockgetFileFromSvn;

    var messageFromSvn = {'$msg': 'A   trunk/config.txt'};

    it('should successfuly handle the svn commit message', function (done) {
        rmiInit.handleCommitMessage(messageFromSvn)
            .then(function (result) {

                for (var i = 0; i < result.length; i++) {
                    assert.equal(true, result[i].valid);
                }

                return true;
            }).should.eventually.equal(true).notify(done);
    });

    it('should fail to handle empty svn message', function (done) {
        rmiInit.handleCommitMessage('').should.be.rejected.notify(done);
    });

    it('should fail to handle svn message with a list of files that are not needed', function (done) {
        rmiInit.handleCommitMessage( {'$msg': 'A   trunk/con'}).should.be.rejected.notify(done);
    });

});