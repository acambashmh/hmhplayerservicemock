/**
 * Created by cambasa on 09/04/2015.
 */
var chai = require('chai');
var chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);
chai.should();


var libraryDashboardConverter = require('../app/RMI/Converters/libraryDashboardConverter');
var flatData = require('./helperFiles/convertionFiles/flatData.json');

describe('testing of conversion of flat json to dashboardLibrary structure of data', function(){

    it('should successfully convert data', function(){
        var result = libraryDashboardConverter.convertData(flatData);
        result.should.not.be.undefined;
        result.length.should.be.equal(1);
        result[0].categorization.should.exist;
        result[0].card.should.exist;
        result[0].id.should.exist;
        result[0].display_title.should.exist;
        result[0].uri.should.exist;
        result[0].uri.relative_url.should.exist;
        result[0].card.should.equal(1);
    });

    it('should fail to convert empty array', function(){
        var result = libraryDashboardConverter.convertData([]);
        result.length.should.equal(0);
    });

});