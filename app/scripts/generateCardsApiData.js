var http = require('http');
var https = require('https');
var fs = require('fs');
var Q = require('q');
var _ = require('underscore');
var querystring = require('querystring');
var mkdirp = require('mkdirp');

var proxy = require("../helpers/proxy.js");
var auth = require("../helpers/auth.js");
var staticDir = __dirname + "/../../static";

http.globalAgent.maxSockets = 100;
https.globalAgent.maxSockets = 100;

var doAjaxRequest = function(path, cookies, headers, host, ssl) {
	var defer = Q.defer();
	try {

		var cookie = "";
		if (cookies) {
			_.each(_.keys(cookies), function(k) {
				cookie += k + "=" + cookies[k] + "; "
			});
		}

		var ssl = ssl !== false;
		var options = {
			host : host || "my-review-cert.hrw.com",
			port : ssl ? 443 : 80,
			path : path,
			method : "GET",
			headers : {
				"Cookie" : cookie,
				"Accept" : "application/json",
				"Accept-Language" : "en-US,en;q=0.8,ca;q=0.6,nl;q=0.4",
				"Host" : host || "my-review-cert.hrw.com"
			},
			agent : false
		};

		if (headers) {
			_.each(_.keys(headers), function(header) {
				options.headers[header] = headers[header];
			});
		}

		var req = (ssl ? https : http).request(options, function(res) {
			var data = "";
			res.on("data", function(chunk) {
				data += chunk;
			});

			res.on("end", function() {
				if (res.statusCode !== 200) {
					defer.reject(new Error(res.statusCode + " " + path + "\n\n" + data));
				} else {
					Q.fcall(JSON.parse, data).then(defer.resolve, defer.reject);
				}
			});

			res.on("error", function(err) {
				defer.reject(err);
			});
		});

		req.on("error", function(err) {
			defer.reject(err);
		});

		req.end();

	} catch (e) {
		defer.reject(e);
	}
	return defer.promise;
};

var getDashboardProgramsGrades = function(cookies, platform) {
	var server = platform === "tc" ? "www-review-cert-tc1.thinkcentral.com" : "my-review-cert.hrw.com";
	var path = "/dashboard/search/planner/programs-grades?program=&outputFormat=json&version=3";
	return doAjaxRequest(path, cookies, null, server).then(function(data) {
		var mappings = [];
		_.each(data.programs.program, function(program) {
			_.each(program.grade || [], function(g) {
				mappings.push({
					program : program.name,
					grade : g
				});
			});

			_.each(program.nongrade || [], function(ng) {
				mappings.push({
					program : program.name,
					ngLevel : ng.non_grade_level,
					ngTitle : ng.non_grade_title
				});
			});
		});
		return mappings;
	});
};

var _uriPlusEncode = function(str) {
	return encodeURIComponent(str || "").replace(/%20/ig, "+");
};

var getDashboardTocDataFor = function(cookies, program, grade, nonGradeLevel, nonGradeTitle) {
	//https://my-review-cert.hrw.com/dashboard/search/planner/toc-resources?program=NL+Go+Math!+2014&non_grade_level=&non_grade_title=Accelerated+Grade+7&outputFormat=json&showSegments=false

	var path = "/dashboard/search/planner/toc-resources?program=" + _uriPlusEncode(program);
	if (grade) {
		path += "&grade=" + grade + "&outputFormat=json&showSegments=false";
	} else {
		path += "&non_grade_level" + _uriPlusEncode(nonGradeLevel) + "&non_grade_title=" + _uriPlusEncode(nonGradeTitle) + "&outputFormat=json&showSegments=false";
	}
	return doAjaxRequest(path, cookies).then(function(data) {
		console.log(JSON.stringify(data, null, 2));
	});
};

var getMdsSearchData = function(cookies, program, grade) {
	//http://my-test.hrw.com/api/search/v1/toc/HMH%20Collections/6?outputFormat=json&view=player&showResources=true&showGradeResources=true
	program = _uriPlusEncode(program);
	var path = "/api/search/v1/toc/" + program + "/" + grade + "?outputFormat=json&view=player&showResources=true&showGradeResources=true";
	return doAjaxRequest(path, cookies).then(function(data) {
		console.log(JSON.stringify(data, null, 2));
	});
};

var requestAllProgramGradeDashboardTocs = function(mappings) {
	var promises = _.map(mappings, function(mapping) {
		return getDashboardTocDataFor(mapping.program, mapping.grade, mapping.ngLevel, mapping.ngTitle).then(function(data) {
			return {
				mapping : mapping,
				data : data
			};
		});
	});
	return Q.all(promises);
};

var getDashboardBasalFilters = function(cookies) {
	var path = "/Basal/Resources";
	return doAjaxRequest(path, cookies).then(function(data) {
		return data.Filters;
	});
};

var getBasalResourceData = function(cookies, filter) {
	//	https://my-review-cert.hrw.com/Basal/Resources?program=National+Collections+-+Grade+8
	var path = "/Basal/Resources?program=" + _uriPlusEncode(filter);
	return doAjaxRequest(path, cookies).then(function(data) {
		return data.Resources;
	});
};

var processBasalResources = function(rawResources) {
	/**
	  types:
		1	Teacher Edition
		2	Student Edition
		3	DLO
		4	Teacher Resources
		5	Ancillary eBook
		6	Notebook
		7	PDF
		8	Learnosity
		9	PMT
		10 	myWriteSmart
		11 	Other Assessment
		12 	URL
		13 	Video
		14 Student Resources (TBD Placeholder)
		15 Math DLO (TBD Placeholder)
	**/
	
	if (!rawResources.length) {
		return [];
	}

	var teRegex = /\/_?ete_[0-9]+_?/ig;
	var seRegex = /\/_?ese_[0-9]+_?/ig;
	var myWSRegex = /\/MWSTeacherDashboardLoader/ig;
	var teResourcesRegex = /teacher_resources/ig;
	var myNBRegex = /\/mnb_[0-9]+_\//ig;
	var pdfRegex = /.pdf$/ig;

	var dloTERegex = /interactive_teacher|ise_te|(dlo_landing[^\/]+\/Teacher)/ig;
	var dloSERegex = /interactive_student|ise_se|(dlo_landing[^\/]+\/Student)/ig;

	var resources = [];

	_.each(rawResources, function(resource) {
		var resUrl = resource.ResourceUrl || "";
		var resTitle = resource.Title
		if (resTitle === "Student Resources") {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Core Components",
				component_type : "SE",
				card : 14,
				cardOrder : 2,
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resTitle === "Teacher Resources") {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Core Components",
				component_type : "TE",
				card : 4,
				cardOrder : 1,
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resTitle === "ExamView") {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Core Components",
				component_type : "TE",
				card : 11,
				cardOrder : 10,
				ISBN : [ {
					type : "primary",
					value : (/(?:interactive_[^0-9]*)([0-9]+)(?:_)/ig.exec(resUrl) || [])[1]
				} ],
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resUrl.match(dloTERegex)) {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Core Components",
				component_type : "TE",
				card : resUrl.toLowerCase().indexOf("math") > 0 ? 15 : 3,
				cardOrder : 3,
				ISBN : [ {
					type : "primary",
					value : (/(?:_[^0-9]*)([0-9]+)(?:_)/ig.exec(resUrl) || [])[1]
				} ],
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resUrl.match(dloSERegex)) {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Core Components",
				component_type : "SE",
				card : resUrl.toLowerCase().indexOf("math") > 0 ? 15 : 3,
				cardOrder : 4,
				ISBN : [ {
					type : "primary",
					value : (/(?:_[^0-9]*)([0-9]+)(?:_)/ig.exec(resUrl) || [])[1]
				} ],
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resUrl.match(teRegex)) {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Core Components",
				component_type : "TE",
				card : 1,
				cardOrder : 2,
				ISBN : [ {
					type : "primary",
					value : (/(?:\/_ete_)([0-9]+)(?:_)/ig.exec(resUrl) || [])[1]
				} ],
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resUrl.match(seRegex)) {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Core Components",
				component_type : "SE",
				card : 2,
				cardOrder : 1,
				ISBN : [ {
					type : "primary",
					value : (/(?:\/_ese_)([0-9]+)(?:_)/ig.exec(resUrl) || [])[1]
				} ],
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resUrl.match(myWSRegex)) {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "English Learner",
				card : 10,
				ISBN : [ {
					type : "primary",
					value : (/(?:isbn=)([0-9]+)(?:&)/ig.exec(resUrl) || [])[1]
				} ],
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resUrl.match(teResourcesRegex)) {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Teacher Resources",
				card : 4,
				component_type : "TE",
				ISBN : [ {
					type : "primary",
					value : (/(?:teacher_resources_)([0-9]+)(?:_)/ig.exec(resUrl) || [])[1]
				} ],
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resUrl.match(myNBRegex)) {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Core Components",
				card : 6,
				cardOrder : 4,
				ISBN : [ {
					type : "primary",
					value : (/(?:mnb_)([0-9]+)(?:_)/ig.exec(resUrl) || [])[1]
				} ],
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+\//ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else if (resUrl.match(pdfRegex)) {
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Other Resources",
				card : 7,
				cardOrder : 4,
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+(\/|$)/ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		} else {
			//URL
			resources.push({
				id : resource.Id,
				HMH_ID : resource.Id,
				display_title : resource.Title,
				categorization: "Other Resources",
				card : 12,
				cardOrder : 4,
				uri : {
					relative_url : resUrl.replace(/https?:\/\/[^/]+(\/|$)/ig, "/"),
					platform_url : (resUrl.match(/https?:\/\/[^/]+/ig) || [])[0]
				}
			});
		}
	});
	

	var categorizationOrders = {
		"Core Components" : 1,
		"Teacher Resources" : 2,
		"English Learner" : 3,
		"Other Resources" : 4
	};
	
	_.each(resources, function (res) {
		res.categoryOrder = categorizationOrders[res.categorization] || 5;
	});
	return resources;
};

var getAuthData = function(username, password, platform) {
	return auth.authenticate(username, password, platform);
};

var determinProgramGradeBasalMapping = function(programGrades, basalFilter) {
	if (!programGrades || !basalFilter) {
		return null;
	}

	var standardize = function(str) {
		return (str || "")
		/***/
		.replace(/(\s|^)CA\s/ig, " CALIFORNIA ")
		/***/
		.replace(/(\s|^)FL\s/ig, " FLORIDA ")
		/***/
		.replace(/(\s|^)TX\s/ig, " TEXAS ")
		/***/
		.replace(/(\s|^)MATHEMATICS\s/ig, " MATH ")
		/***/
		.replace(/(\s|^)HS\s/ig, " HIGH SCHOOL ")
		/***/
		.replace(/(\s|^)MS\s/ig, " MIDDLE SCHOOL ")
		/***/
		.replace(/GO MATH!/ig, "GO MATH")
		/***/
		.replace(/NL Go Math/ig, "GO MATH")
		/***/
		.replace(/GO MATH Spanish/ig, "GOMATHSPANISH")
		/***/
		.replace(/National Collections/ig, "HMH Collections")
		/***/
		.replace(/Kindergarten/ig, "KG")
		/***/
		.replace(/Grade KG/ig, "KG")
		/***/
		.replace(/\s+/ig, " ");
	};

	var bFilterLwr = standardize(basalFilter).toLowerCase();
	for (var i = 0; i < programGrades.length; i++) {
		var pg = programGrades[i];
		var nameMatch;
		if (pg.grade) {
			nameMatch = pg.program + " Grade " + pg.grade;
		} else {
			nameMatch = pg.ngTitle;
		}

		if (nameMatch && bFilterLwr.indexOf(standardize(nameMatch).toLowerCase()) > -1) {
			return pg;
		}
	}

	var gradeRegex = /(?:Grade\s*)([0-9]{1,2})/ig;
	var yearRegex = /201[0-9]/;

	var collections = /Collections/ig;
	var goMath = /Go Math/ig;
	var hsMath = /Geometry|Algebra|Math/ig;
	var grade = (gradeRegex.exec(basalFilter) || [])[1] || -1;

	var collectionsProgramGrades = _.filter(programGrades, function(pg) {
		return !!pg.program.match(collections);
	});

	var goMathProgramGrades = _.filter(programGrades, function(pg) {
		return !!pg.program.match(goMath);
	});

	var hsMathProgramGrades = _.filter(programGrades, function(pg) {
		return !pg.program.match(goMath) && !!pg.program.match(hsMath);
	});

	if (basalFilter.match(collections)) {
		var matches = _.filter(collectionsProgramGrades, function(cpg) {
			return cpg.grade == grade;
		});
		return matches.length === 1 ? matches[0] : null;
	} else if (basalFilter.match(goMath)) {
		var matches = _.filter(goMathProgramGrades, function(gmpg) {
			return gmpg.grade == grade;
		});
		return matches.length === 1 ? matches[0] : null;
	} else if (basalFilter.match(hsMath)) {
		var matches = _.filter(hsMathProgramGrades, function(hspg) {
			if (bFilterLwr.indexOf())

				var ngtGrade = (gradeRegex.exec(hspg.ngTitle || "") || [])[1] || -2;
			return (hspg.grade || ngtGrade) == grade;
		});
		return matches.length === 1 ? matches[0] : null;
	}

	return null;
};

var screenScrapeTCStudentDashboard = function(authData) {

	//	https://www-review-cert-tc1.thinkcentral.com/ePC/studentClassProducts.do
	var defer = Q.defer();
	try {
		var cookies = authData.cookies;
		var cookie = "";
		if (cookies) {
			_.each(_.keys(cookies), function(k) {
				cookie += k + "=" + cookies[k] + "; "
			});
		}

		var ssl = true;
		var options = {
			host : "www-review-cert-tc1.thinkcentral.com",
			port : ssl ? 443 : 80,
			path : "/ePC/studentClassProducts.do",
			method : "GET",
			headers : {
				"Cookie" : cookie,
				"Accept" : "*/*",
				"Accept-Language" : "en-US,en;q=0.8,ca;q=0.6,nl;q=0.4",
				"Host" : "www-review-cert-tc1.thinkcentral.com"
			},
			agent : false
		};

		var req = (ssl ? https : http).request(options, function(res) {
			var data = "";
			res.on("data", function(chunk) {
				data += chunk;
			});

			res.on("end", function() {
				if (res.statusCode !== 200) {
					defer.reject(new Error(res.statusCode + " " + path + "\n\n" + data));
				} else {
					defer.resolve(data);
				}
			});

			res.on("error", function(err) {
				defer.reject(err);
			});
		});

		req.on("error", function(err) {
			defer.reject(err);
		});

		req.end();

	} catch (e) {
		defer.reject(e);
	}
	return defer.promise.then(function(html) {
		var links = html.match(/event.returnValue=launchOnClick.+[^>]+<\/a>/ig);
		return _.map(links, function(lnk) {
			var lnkParts = (/(?:[^\(]+\(")([^\)]+(?:\)))/ig).exec(lnk)[1].replace(/"/ig, "").replace(/\)/ig, "").split(",");
			//["/content/hsp/math/gomath2015/ca/grk/ese_9780544275423_/html5/index.html"", ""DEF536F57EFD119EE04400144F250978"", ""9780544275423")"]
			var url = lnkParts[0];
			var resId = lnkParts[1];
			var isbn = lnkParts[2];
			var title = (/(?:>)([^<]+)(?:<\/a>)/ig).exec(lnk)[1].trim();

			var gradeMatch = (/(?:\/gr)(k|[0-9]+)(?:\/)/ig).exec(url);
			if (gradeMatch) {
				gradeMatch = "Grade " + gradeMatch[1];
			}

			return {
				ResourceUrl : "https://www-review-cert-tc1.thinkcentral.com" + url,
				Id : resId,
				ISBN : isbn,
				GradeId : -1,
				Grade : gradeMatch,
				Title : title
			};
		});
	});
};

var processTCStudent = function(authData) {
	var host = "certrv.gateway.api.hmhco.com";
	var authHeader = {
		Authorization : authData.authToken
	};
	var sectionsPath = "/api/identity/v1/students/student/" + authData.user.sifRef + "/section;contextId=tc";
	return doAjaxRequest(sectionsPath, null, authHeader, host, false).then(function(data) {
		var sectionRefs = _.pluck(data.sections, "refId");
		var gbPath = "/api/caes/v1/resourceAssociation;contextId=tc?sectionRefIds=" + sectionRefs.join("%2C");
		return doAjaxRequest(gbPath, null, authHeader, host, false).then(function(data) {
			//			console.log(JSON.stringify(data, null, 2));
			// deduplicate
			var programGrades = _.map(_.values(_.groupBy(data, function(pg) {
				return pg.program + " / " + pg.grade + " / " + pg.nonGradeTitle;
			})), function(array) {
				return {
					program : array[0].program,
					grade : array[0].grade,
					ngTitle : array[0].nonGradeTitle,
					ngLevel : array[0].nonGradeLevel
				};
			});
			if (programGrades.length > 1) {
				throw "TC Student with multiple Program / Grade mappings not supported yet!";
			}
			return programGrades[0];
		}).then(function(programGrade) {
			// screen scrape dashboad for resources
			return screenScrapeTCStudentDashboard(authData).then(function(resources) {
				var processed = processBasalResources(resources);
				return [ {
					pg : programGrade,
					resources : processed
				} ];
			});
		});
	});
};

var writeProgramGradeDataToDisk = function(authData, dataSets) {
	var promises = _.map(dataSets, function(data) {

		var pg = data.pg;
		var fileName = "cards_" + (authData.user.isTeacher ? "teacher" : "student") + ".json";
		var dir = "/mdsData/" + pg.program + "/" + (pg.grade || pg.ngTitle) + "/";
		dir = staticDir + dir.replace(/\s/ig, "_").replace(/[^A-Za-z0-9_.\/]/ig, " ");
		var fullPath =  dir + fileName.replace(/\s/ig, "_").replace(/[^A-Za-z0-9_.\/]/ig, " ");
		console.log(fullPath);
		return Q.nfcall(mkdirp, dir).then(function() {
			return Q.nfcall(fs.writeFile, fullPath, JSON.stringify(data.resources, null, 2));
		});
	});
	return Q.all(promises);
};

var buildMdsData = function(username, password, platform) {
	return getAuthData(username, password, platform).then(function(authData) {
		console.log(username + " logged in");

		if (platform === "tc" && !authData.user.isTeacher) {
			return processTCStudent(authData).then(function(data) {
				return writeProgramGradeDataToDisk(authData, data);
			});
		}

		return getDashboardProgramsGrades(authData.cookies, platform).then(function(programGradeMappings) {
			console.log(username + " got program / grades");

			return getDashboardBasalFilters(authData.cookies).then(function(filters) {

				console.log(username + " got basal filters");

				return Q.all(_.map(filters, function(filter) {

					return getBasalResourceData(authData.cookies, filter).then(function(rawBasalResources) {
						var basalResourcesByGradeId = _.groupBy(rawBasalResources, function(rbr) {
							return rbr.Grade;
						});

						var gradeIds = _.keys(basalResourcesByGradeId);
						var pg = determinProgramGradeBasalMapping(programGradeMappings, filter);

						if (pg && gradeIds.length === 1) {
							// assume single grade style
							if (pg) {
								var resources = processBasalResources(rawBasalResources);
								return [ {
									pg : pg,
									resources : resources
								} ];
							} else {
								console.log("could not find program grade mapping for " + filter + ": " + authData.user.username);
								return [];
							}
						} else {
							var values = [];
							// assume grade style
							_.each(gradeIds, function(grade) {
								var pg = determinProgramGradeBasalMapping(programGradeMappings, filter + " " + grade);
								if (pg) {
									var resources = processBasalResources(basalResourcesByGradeId[grade]);
									values.push({
										pg : pg,
										resources : resources
									});
								} else {
									console.log("could not find program grade mapping for " + filter + " -/- " + grade + ": " + authData.user.username);
									return [];
								}
							});
							return values;
						}
					}).then(function(data) {
						return writeProgramGradeDataToDisk(authData, data);
					});
				})).then(function() {
					console.log(username + " finished");
				});
			});
		}).fail(function(err) {
			console.log(authData.user.username + "failed");
			return Q.reject(err);
		});

	}).fail(function(err) {
		console.log(err.stack);
	});
};

var logins = [];

logins.push({
	user : "Ent7701",
	pass : "Ent7701",
	platform : "hmof"
});
logins.push({
	user : "scotter20",
	pass : "password",
	platform : "hmof"
});
logins.push({
	user : "Gmnate1234",
	pass : "Gmnate1234",
	platform : "hmof"
});
logins.push({
	user : "ECLASS5",
	pass : "password",
	platform : "hmof"
});
logins.push({
	user : "STUDENTGA7",
	pass : "password",
	platform : "hmof"
});
logins.push({
	user : "STUDENTALG11",
	pass : "password",
	platform : "hmof"
});
logins.push({
	user : "STUDENTG72",
	pass : "password",
	platform : "hmof"
});
logins.push({
	user : "STUDENTG82",
	pass : "password",
	platform : "hmof"
});
logins.push({
	user : "STUDENTG62",
	pass : "password",
	platform : "hmof"
});

/** These are not supported yet (student TC logins) **/

logins.push({
	user : "crmyoda",
	pass : "password",
	platform : "tc"
});

logins.push({
	user : "dfoundryk",
	pass : "password",
	platform : "tc"
});

logins.push({
	user : "dfoundry1",
	pass : "password",
	platform : "tc"
});

logins.push({
	user : "dfoundry2",
	pass : "password",
	platform : "tc"
});

logins.push({
	user : "dfoundry3",
	pass : "password",
	platform : "tc"
});

logins.push({
	user : "dfoundry4",
	pass : "password",
	platform : "tc"
});

logins.push({
	user : "dfoundry5",
	pass : "password",
	platform : "tc"
});

logins.push({
	user : "dfoundry6",
	pass : "password",
	platform : "tc"
});

/** HMOF Logins **/

logins.push({
	user : "nsharma22",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "scotter21",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "ialgt",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "aintb",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "ialgx",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "thenry117",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "Gobrien1234",
	pass : "Gobrien1234",
	platform : "hmof"
});

logins.push({
	user : "studentg81",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "studentg82",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "studentg71",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "studentg61",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "studentalg1",
	pass : "password",
	platform : "hmof"
});
logins.push({
	user : "bbooke",
	pass : "password",
	platform : "hmof"
});

logins.push({
	user : "cebook",
	pass : "password",
	platform : "hmof"
});

var chain = logins.reduce(function(previous, item) {
	return previous.then(function(previousValue) {
		return buildMdsData(item.user, item.pass, item.platform);
	})
}, Q.resolve());

chain.fail(function(err) {
	console.log(err.stack);
}).fin(function() {
	console.log("done");
});
