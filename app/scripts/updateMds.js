var fs = require('fs');
var staticDir = __dirname + "/../../static";
var _ = require('underscore');

_.each(fs.readdirSync(staticDir + "/mdsData/"), function(folderPath) {
	var fpath = staticDir + "/mdsData/" + folderPath;
	_.each(fs.readdirSync(fpath), function(gradePath) {
		var gpath = staticDir + "/mdsData/" + folderPath + "/" + gradePath;
		_.each(fs.readdirSync(gpath), function(filGradePath) {
			var filePath = staticDir + "/mdsData/" + folderPath + "/" + gradePath + "/" + filGradePath;
			console.log(filePath);

			var content = JSON.parse(fs.readFileSync(filePath));

			var categorizationOrders = {
				"Core Components" : 1,
				"Teacher Resources" : 2,
				"English Learner" : 3,
				"Other Resources" : 4
			};
			
			var cardOrders = [1,10,2,3,12,13,14,8,9,4,18,19,20,22,3];
			
			_.each(content, function (card) {
				card.categoryOrder = categorizationOrders[card.categorization] || 5;
				card.cardOrder = cardOrders[card.card] || 25;
			});
			
			fs.writeFileSync(filePath, JSON.stringify(content, null, 2));

		});
	});
});
//console.log(JSON.stringify(fs.readdirSync(staticDir + "/mdsData/")));

//return Q.nfcall(fs.writeFile, fullPath, JSON.stringify(data.resources, null, 2));
