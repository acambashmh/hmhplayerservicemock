var express = require('express');
var http = require('http');
var https = require('https');
var url = require('url');
var path = require('path');
var fs = require('fs');
var cors = require('cors');
var requireDir = require('require-dir');
var winston = require('winston');

var auth = require("./helpers/auth.js");
var proxy = require("./helpers/proxy.js");


winston.add(winston.transports.File, { filename: 'somefile.log' });

winston.info('App has started');

var app = express();
module.exports.app = app;
module.exports.staticDir = __dirname + "/../static";

app.set('port', process.env.port || 8123);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());

// authenticate using sif Authorization header if possible
app.use(function(req, res, next) {
	auth.getUser(req).then(function (user) {
		req.user = user;
	}).fin(next);
});

app.use(express.static(__dirname + '/../static')); // this needs to come before router for ebook content!
app.use(app.router);

app.listen(app.get('port'), function () {
    winston.info('Server has started, app is listening on port ' + app.get('port'));
});


app.get("/", function(req, res) {
	res.json(200, {
		"Status" : "ok"
	});
});

app.options("/**", function(req, res) {
	res.send(200);
});

requireDir('./routes', {
	recurse : true
});

app.all("/api/**", function(req, res) {
	proxy.proxyToApiCert(req, res);
});

