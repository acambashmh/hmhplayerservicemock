/**
 * Created by cambasa on 16/03/2015.
 */
var fs = require('fs');
var q = require('q');
var xml2js = require('xml2js');
var converter = require('./converter');
var _ = require('lodash');
var winston = require('winston');

function convertXMLFileToJSONMetadata(fileData) {
    var parser = new xml2js.Parser();
    var mainDefer = q.defer();

    q.nfcall(fs.readFile, fileData.filePath, 'utf-8')
        .then(function (data) {
            winston.info('xml file loaded and ready for convert to flat json');
            var defer = q.defer();
            parser.parseString(data, function (err, result) {
                var res = [];
                winston.info('xml file converted to json representation');
                _.forEach(result.MDS.resources[0].resource, function (item) {
                    var convertedMetadata = converter(item);
                    res.push(convertedMetadata);
                });

                //var res = converter.convertResourceXMLItemToJson(result.resource);
                fileData.convertedData = res;
                defer.resolve(fileData);
            });
            return defer.promise;
        }).then(function(data) {
            mainDefer.resolve(data);
        })
        .catch(function (e) {
            winston.error('error with converting xml file to flat json:' + e.toString());
            mainDefer.reject(e);
        });

    return mainDefer.promise;
}

module.exports = convertXMLFileToJSONMetadata;