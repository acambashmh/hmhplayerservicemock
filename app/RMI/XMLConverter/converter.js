/**
 * Created by cambasa on 24/03/2015.
 */
function convertResourceXMLItemToJson(item) {
    var flatRow = {};
    flatRow.id = item.$ ? item.$.id : null;

    flatRow.HMH_ID = item.HMH_ID ? item.HMH_ID[0] : null;
    flatRow.IWB_Compatible = item.IWB_Compatible ? convertBoolean(item.IWB_Compatible[0]) : null;
    flatRow.active = item.active ? convertBoolean(item.active[0]) : null;
    flatRow.aditional_text = item.aditional_text ? item.aditional_text[0] : null;
    flatRow.assignable = item.assignable ? convertBoolean(item.assignable[0]) : null;
    flatRow.author = item.author ? item.author[0] : null;
    flatRow.categorization = item.categorization ? item.categorization[0] : null;
    flatRow.component = item.component ? item.component[0] : null;
    flatRow.component_type = item.component_type ? item.component_type[0] : null;
    flatRow.diff_inst = item.diff_inst ? convertBoolean(item.diff_inst[0]) : null;

    flatRow.display_title = item.display_title ? item.display_title[0] : null;
    flatRow.done_owner = item.done_owner ? item.done_owner[0] : null;
    flatRow.download_url = item.download_url ? item.download_url[0] : null;
    flatRow.enrich = item.enrich ? convertBoolean(item.enrich[0]) : null;
    flatRow.ext = item.ext ? item.ext[0] : null;
    flatRow.freeplay = item.freeplay ? convertBoolean(item.freeplay[0]) : null;
    flatRow.genres = item.genres ? item.genres[0] : null;
    flatRow.hierarchy = item.hierarchy ? convertNumber(item.hierarchy[0]) : null;
    flatRow.language = item.language ? item.language[0] : null;
    flatRow.meaningful_description = item.meaningful_description ? item.meaningful_description[0] : null;
    flatRow.media_type = item.media_type ? item.media_type[0] : null;
    flatRow.non_grade_level = item.non_grade_level ? item.non_grade_level[0] : null;
    flatRow.non_grade_title = item.non_grade_title ? item.non_grade_title[0] : null;
    flatRow.parent_resource_id = item.parent_resource_id ? item.parent_resource_id[0] : null;
    flatRow.persistent = item.persistent ? convertBoolean(item.persistent[0]) : null;
    flatRow.resource_panel_se = item.resource_panel_se ? item.resource_panel_se[0] : null;
    flatRow.resource_panel_te = item.resource_panel_te ? item.resource_panel_te[0] : null;
    flatRow.resource_type = item.resource_type ? item.resource_type[0] : null;
    flatRow.reteach = item.reteach ? convertBoolean(item.reteach[0]) : null;
    flatRow.schedualable = item.schedualable ? convertBoolean( item.schedualable[0]) : null;
    flatRow.searchable = item.searchable ? convertBoolean(item.searchable[0] ) : null;
    flatRow.se_facing = item.se_facing ? convertBoolean(item.se_facing[0]) : null;
    flatRow.sort_id = item.sort_id ? item.sort_id[0] : null;
    flatRow.strand = item.strand ? convertNumber(item.strand[0]) : null;
    flatRow.teacher_managed = item.teacher_managed ? convertBoolean(item.teacher_managed[0]) : null;
    flatRow.text_complexity = item.text_complexity ? item.text_complexity[0] : null;
    flatRow.theme = item.theme ? item.theme[0] : null;
    flatRow.title = item.title ? item.title[0] : null;
    flatRow.tool_type = item.tool_type ?  convertNumber(item.tool_type[0]) : null;
    flatRow.viewable = item.viewable ? convertBoolean(item.viewable[0]) : null;

    //special





    //ISBN
    if(item.setOfISBNs){
        flatRow.ISBN = {};
        if(item.setOfISBNs[0].ISBN[0] ){
            if(item.setOfISBNs[0].ISBN[0].$ && item.setOfISBNs[0].ISBN[0].$.type){
                flatRow.ISBN.type =  item.setOfISBNs[0].ISBN[0].$.type;

            }

            flatRow.ISBN.value = item.setOfISBNs[0].ISBN[0]._ ? item.setOfISBNs[0].ISBN[0]._ : null;
        }

    }

    //icon url
    if (item.icon_url && item.icon_url[0].relative_url) {
        flatRow.icon_url = item.icon_url[0].relative_url[0];
    }

    //standards
    if (item.standards) {
        extractStandards(flatRow, item);
    }

    //Uri
    if (item.uri && item.uri[0].relative_url) {
        flatRow.relative_url = item.uri[0].relative_url[0];
    }

    extractLevel(flatRow, item.level);

    //flatRow.discipline = item.discipline ? item.discipline[0] : null;
    //flatRow.keywords = item.keywords ? item.keywords[0] : null;
    //flatRow.lab = item.lab ? item.lab[0] : null;
    return flatRow;
}


function extractStandards(flatRow, item) {
    if (item.standards && item.standards[0].standard && item.standards[0].standard[0] != '') {
        var standardsString = '';
        var standardsArray = item.standards[0].standard;

        for (var i = 0, len = standardsArray.length; i < len; i++) {

            standardsString += standardsArray[i];
            if (i + 1 != len) {
                standardsString += ', ';
            }
        }
        flatRow.standards = standardsString;
    }

}

function extractLevel(flatRow, levels) {
    flatRow.L1 = null;
    flatRow.L2 = null;
    flatRow.L3 = null;
    flatRow.L4 = null;
    flatRow.L5 = null;
    flatRow.L6 = null;
    flatRow.L7 = null;
    flatRow.L1_type = null;
    flatRow.L2_type = null;
    flatRow.L3_type = null;
    flatRow.L4_type = null;
    flatRow.L5_type = null;
    flatRow.L6_type = null;
    flatRow.L7_type = null;
    flatRow.L1_title = null;
    flatRow.L2_title = null;
    flatRow.L3_title = null;
    flatRow.L4_title = null;
    flatRow.L5_title = null;
    flatRow.L6_title = null;
    flatRow.L7_title = null;


    createLs(levels[0], flatRow);

    handleCardValue(flatRow);
    handleGradeLvlValue(flatRow, levels[0]);
}

function createLs(level, xlsRow) {
    if (level.level) {
        xlsRow = createLs(level.level[0], xlsRow);
    }
    else {
        //xlsRow.instructionalSegment = level.instructional_segment[0].title[0];
    }
    xlsRow['L' + level.$.level_number] = + convertNumber(level.$.hierarchy);
    xlsRow['L' + level.$.level_number + '_title'] = level.title ? level.title[0] : '';
    xlsRow['L' + level.$.level_number + '_type'] = level.$.type;
    return xlsRow;
};

function handleCardValue(flatRow) {
    if (flatRow && flatRow.L7) {
        flatRow.card = flatRow.L7;
    }

    delete flatRow.L7;
    delete flatRow.L7_type;
    delete flatRow.L7_title;
}

function handleGradeLvlValue(flatRow, level) {
    if (flatRow.L0 && level.grades) {
        flatRow.L0 = level.grades[0].grade[0];
    }
}

function convertBoolean(bool){
    if(bool === 'true'){
        return true;
    }

    if(bool === 'false'){
        return false
    }

    return null;
}

function convertNumber(number){
    if(!isNaN(number)){
        return parseInt(number);
    }
    return null;
}

module.exports = convertResourceXMLItemToJson;