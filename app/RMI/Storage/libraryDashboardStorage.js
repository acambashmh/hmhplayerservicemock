var q = require('q');
var fs = require('fs');
var winston = require('winston');
var Datastore = require('nedb');


var db = new Datastore({filename: __dirname + '/db/dashboardLibrary.db', autoload: true});


module.exports.getById = getById;


module.exports.searchByProgramGrade = function (program, grade) {
    var grade = parseInt(grade);

    return q.fcall(function(){
        var defer = q.defer();

        var findObj = {
            "properties.program": program,
            "properties.grade": grade
        };

        db.find(findObj, function (err, docs) {
            if (docs && docs.length > 0) {
                defer.resolve(docs[0].filePath);
            } else {
                defer.resolve(null);
            }

        });

        return defer.promise;

    }).then(function(path){
        var defer1 = q.defer();

        fs.readFile(path, {encoding:'utf-8'},function(err, data){
            if(err){
                defer1.resolve([]);
            }else{
                var data = JSON.parse(data);
                defer1.resolve(data);
            }
        });

        return defer1.promise;
    });

};


module.exports.insertOrUpdate = function (fileInfo, data) {
    var path = __dirname + '/ConvertedMetadata/' + fileInfo.id + '_libraryDashboard' + '.json';
    return q.nfcall(fs.writeFile, path, JSON.stringify(data))
        .then(function(ress){
            return getById(fileInfo.id)
        })
        .then(function (result) {
        var defer = q.defer();
            var now = new Date();
            var forSave = {
                id: fileInfo.id,
                properties: fileInfo.properties,
                filePath: path,
                type: 'libraryDashboard',
                date: now
            };
        if (result) {
            forSave._id = result._id;
            db.update(forSave, function(err,result){
                winston.info('the dashboard library data was converted and saved successfully');

                defer.resolve({done:true});
            })
        } else {
            db.insert(forSave, function(err,result){
                winston.info('the dashboard library data was converted and saved successfully');

                defer.resolve({done:true});
            });
        }
        return defer.promise;
    })
};


function getById(id) {
    var defer = q.defer();

    db.find({
        id: id
    }, function (err, docs) {
        if (docs && docs.lenght > 0) {
            defer.resolve(docs[0]);
        } else {
            defer.resolve(null);
        }

    });

    return defer.promise;
}
