var q = require('q');
var fs = require('fs');
var convertXmlMetadata = require('./XMLConverter/xmlMetadataConverter');
var metadataValidate = require('./MetadataValidation/validateResourceList');
var svn = require('svn-interface');
var winston = require('winston');


//RmiInit class, used to initialize and configure RMI functionil
function RmiInit(svnConfig, filesNeeded) {
    this.svnConfig = svnConfig || {
        svnServer: ''
    };
    this.filesNeeded = filesNeeded || [];
}


RmiInit.prototype.parseCommitMessage = function (msg) {
    winston.info('parsing commit message: ', msg);
    var result = [];

    if (msg && msg !== '') {
        var splitPathArray = msg.split(',');
        for (var i = 0; i < splitPathArray.length; i++) {

            if (splitPathArray[i] && splitPathArray[i] !== '') {
                var tempString = this.svnConfig.svnServer + '/' + splitPathArray[i].substring(1, splitPathArray[i].length).trim();
                result.push(tempString);
            }
        }
    }

    return result;
};

RmiInit.prototype.checkForNeededFiles = function (files) {
    var result = [];
    winston.info('checking for needed files: ', files);
    if (files && files.length > 0) {
        for (var i = 0; i < files.length; i++) {

            for (var j = 0; j < this.filesNeeded.length; j++) {
                if (files[i].indexOf(this.filesNeeded[j].id) > -1) {
                    var temp = {
                        svnPath: files[i],
                        fileInfo: this.filesNeeded[j]
                    };
                    result.push(temp);
                }
            }
        }
    }
    return result;
};

RmiInit.prototype.getFileFromSvn = function (fileProperties) {
    var defer = q.defer();
    var paths = [fileProperties.svnPath, __dirname + '/SvnFiles'];
    winston.info('file to export from svn ' + fileProperties.svnPath);
    svn.export(paths, {
        username: this.svnConfig.username,
        password: this.svnConfig.password,
        force: true
    }, function (err, res) {
        winston.info('Message from SVN export ' + res);
        if (err) {
            defer.reject(err);
        } else {
            var pathStart = res.indexOf('SvnFiles');
            var pathEnd = res.indexOf('.xml');
            fileProperties.filePath = __dirname + '/' + res.substring(pathStart, (pathEnd + 4));
            winston.info('File received from SVN  ' + fileProperties.filePath);
            defer.resolve(fileProperties);
        }
    });

    return defer.promise;
};

RmiInit.prototype.handleCommitMessage = function (msg) {
    //Parse message
    var result = {
        done: false
    };
    var self = this;
    winston.info('commit message parse start');
    var promise = q.fcall(function () {
        //Parsing svn message/////////////////////////////////////////////////////////////////////////////
        if (!msg && !msg.$msg) {
            throw new Error('message received from svn is empty');
        }

        var fileListFromSvnMessage = self.parseCommitMessage(msg.$msg);
        var neededFiles = self.checkForNeededFiles(fileListFromSvnMessage);

        if (!neededFiles || neededFiles.length === 0) {
            throw new Error('There is no files that need to be downloaded from SVN');
        }
        return neededFiles;
    })
        .then(function (svnFilePaths) {
            //Getting the files from SVN//////////////////////////////////////////////////
            var promiseArray = [];

            for (var i = 0; i < svnFilePaths.length; i++) {
                var tempPromise = self.getFileFromSvn(svnFilePaths[i]);
                promiseArray.push(tempPromise);
            }

            return q.all(promiseArray);
        })
        .then(function (data) {
            //Converting the files received from SNV/////////////////////////////////////////////////////
            winston.info('converting from xml to flat json');
            var promiseArray = [];
            for (var i = 0; i < data.length; i++) {
                promiseArray.push(convertXmlMetadata(data[i]));
            }

            return q.all(promiseArray);
        })
        .then(function (convertedReourceLists) {
            //Validate data
            winston.info('validating data');
            for (var i = 0; i < convertedReourceLists.length; i++) {
                var validationResult = metadataValidate(convertedReourceLists[i].convertedData);

                if (validationResult.valid) {
                    convertedReourceLists[i].valid = true;
                } else {
                    convertedReourceLists[i].valid = false;
                    convertedReourceLists[i].validationResult = validationResult;
                }
                delete convertedReourceLists[i].filePath;
                delete convertedReourceLists[i].svnPath;

            }
            winston.info('commit message handling end');
            return convertedReourceLists;
        })
        .catch(function (err) {
            winston.error('error with handling commit message: ', err.toString());
            throw new Error(err.toString());
        });

    return promise;
};

module.exports = RmiInit;

