/**
 * Created by cambasa on 30/03/2015.
 */

var _ = require('lodash');
var q = require('q');
var libraryDashboardStorage = require('../Storage/libraryDashboardStorage');
var winston = require('winston');


module.exports = {
    convertAndSave : convertAndSave,
    convertData : convertData
};




function convertAndSave(resourcesAndFileInfo) {
    return q.fcall(function () {
        winston.info('converting flat json to library dashboard data');
        return convertData(resourcesAndFileInfo.convertedData);
    }).then(function (convertRes) {
        winston.info('saving the converted library data')
        return libraryDashboardStorage.insertOrUpdate(resourcesAndFileInfo.fileInfo, convertRes)
    }).then(function () {
        return resourcesAndFileInfo.fileInfo.id;
    });
}


function convertData(resources) {
    var result = [];

    if (resources && resources.length > 0) {
        _.forEach(resources, function (resource) {
            if (resource.card) {
                var convertedItem = {
                    card: resource.card,
                    categorization: resource.categorization,
                    id: resource.id,
                    display_title: resource.display_title,
                    uri: {
                        relative_url: resource.relative_url,
                        platform_url: 'http://my-test.hrw.com'
                    }
                };
                result.push(convertedItem);
            }
        });
    }
    return result;
}