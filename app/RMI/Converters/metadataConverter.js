/**
 * Created by cambasa on 31/03/2015.
 */
var q = require('q');
var libraryDashboardConverter = require('./libraryDashboardConverter');
var _ = require('lodash');
var winston = require('winston');

function convertMetadata(listOfData){

    return q.fcall(function(){
        var promises = [];

        _.forEach(listOfData, function(data){
            var promise = libraryDashboardConverter.convertAndSave(data);
            promises.push(promise);
        });

        return q.all(promises);

    }).catch(function(err){
        winston.info(err);
    });
}



function convertSingleMetadata(data){
    return libraryDashboardConverter(data);
}



module.exports = convertMetadata;
