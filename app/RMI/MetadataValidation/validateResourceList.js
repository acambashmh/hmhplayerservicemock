/**
 * Created by cambasa on 26/03/2015.
 */
var validateResourceItem = require('./validateFlatRow');
var _ = require('lodash');
var winston = require('winston');


module.exports  = function(resourceItems){
    var result = new ResourceValidationResult();
    winston.info();
    if(resourceItems && resourceItems.length == 0){
        result.valid = false;
        result.messages.push('list of resources is empty');
    }

    try{

        _.forEach(resourceItems, function(item) {
            var itemValidResult = validateResourceItem(item);
            if(!itemValidResult.valid){
                result.valid = false;
                winston.error(result);
            }
            result.resourceItemsValidationResults.push(itemValidResult);
        })

    } catch(e){
        result.valid = false;
        result.messages.push('system error while validating resource items');
        winston.error('system error while validating resource items', e);
    }

    return result;
};

function ResourceValidationResult(isbn, resourceItemsValidationResults){
    this.isbn = isbn || null;
    this.valid = true;
    this.messages = [];
    this.resourceItemsValidationResults = resourceItemsValidationResults || [];
}
