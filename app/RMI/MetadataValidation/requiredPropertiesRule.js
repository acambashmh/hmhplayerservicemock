/**
 * Created by cambasa on 25/03/2015.
 */
var validatorFactory = require('jsonschema').Validator;
var validator = new validatorFactory();

module.exports = function (resourceItem, resourceItemValidationResult) {
    var schemaResult = validator.validate(resourceItem, schema);

    if(schemaResult.errors && schemaResult.errors.length > 0){
        resourceItemValidationResult.valid = false;
        for(var i =0; i< schemaResult.errors.length; i++){
            resourceItemValidationResult.messages.push(schemaResult.errors[i].toString());
        }

    }

    return resourceItemValidationResult;
};


var schema = {
    "id": "basicSchema",
    "title": "toc schema",
    "type": "object",
    "properties": {
        "HMH_ID": {"type": "string", "required": true, minLength: 10, maxLength:32},
        "resource_type": {"type": "string", maxLength:50},
        "id": {"type": "string"},
        "display_title": {"type": "string", "required": true, minLength: 1, maxLength:200},
        "meaningful_description": {"type": "string", maxLength:1000},
        "relative_url": {"type": "string", "required": true},
        "tool_type": {"type": "number", "required": true},
        "assignable": {"type": "boolean", "required": true},
        "freeplay": {"type": "boolean", "required": true},
        "active": {"type": "boolean", "required": true},
        "component_type": {"type": "string", "required": true},
        "media_type": {"type": "string", "required": true},
        "standards": {"type": "string"},
        "categorization": {"type": "string", "required": true},
        "strand": {"type": "number", minimum:0, maximum:999},
        "reteach": {"type": "boolean"},
        "diff_inst": {"type": "boolean"},
        "viewable": {"type": "boolean"},
        "schedulable": {"type": "boolean"},
        "searchable": {"type": "boolean"},
        "teacher_managed": {"type": "boolean"},
        "se_facing": {"type": "boolean"},
        "enrich": {"type": "boolean"},
        "component": {"type": "string", "required": true},
        "hierarchy": {"type": "number", minimum:0, maximum:100},
        "ISBN": {
            "type": "object",
            "properties": {
                "type": {"type": "string", "required": true},
                "value": {"type": "string", "required": true}
            }
        }
    }
};