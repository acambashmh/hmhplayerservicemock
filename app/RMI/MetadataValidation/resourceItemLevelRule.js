/**
 * Created by cambasa on 26/03/2015.
 */


module.exports = function(resourceItem, resourceItemValidationResult) {

    if(resourceItem.L0){
        if(!resourceItem.L0_type){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L0 type is missing');
        }
    }

    if(resourceItem.L1){
        if(!resourceItem.L1_type){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L1 type is missing');
        }
        if(!resourceItem.L1_title){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L1 title is missing');
        }
    }

    if(resourceItem.L2){
        if(!resourceItem.L2_type){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L2 type is missing');
        }
        if(!resourceItem.L2_title){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L2 title is missing');
        }
    }

    if(resourceItem.L3){
        if(!resourceItem.L3_type){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L3 type is missing');
        }
        if(!resourceItem.L1_title){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L3 title is missing');
        }
    }

    if(resourceItem.L4){
        if(!resourceItem.L4_type){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L4 type is missing');
        }
        if(!resourceItem.L4_title){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L4 title is missing');
        }
    }

    if(resourceItem.L5){
        if(!resourceItem.L5_type){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L5 type is missing');
        }
        if(!resourceItem.L5_title){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L5 title is missing');
        }
    }

    if(resourceItem.L6){
        if(!resourceItem.L6_type){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L6 type is missing');
        }
        if(!resourceItem.L6_title){
            resourceItemValidationResult.valid = false;
            resourceItemValidationResult.messages.push('L6 title is missing');
        }
    }

    return resourceItemValidationResult;

};