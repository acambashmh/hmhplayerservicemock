var _ = require('lodash');
var requiredPropertiesRule = require('./requiredPropertiesRule');
var resourceItemLevelRule = require('./requiredPropertiesRule');



//--------------------Validate Single resource item--------------------------//

function validateSingleResourceItem(resourceItem){
    var result = new ResourceItemValidationResult();

    //Resource item base validation
    if(!resourceItem){
        result.id = 'Unknown';
        result.messages.push('resource item was not converted properly');
        return result;
    }

    if(!resourceItem.id){
        result.id = 'Unknown';
        result.messages.push('resource item id doesnt exist');
        return result;
    }

    result.resourceId = resourceItem.id;
    requiredPropertiesRule(resourceItem, result);
    resourceItemLevelRule(resourceItem, result);

    return result;
}


//--------------------Factory classes for validation results--------------------------//


function ResourceItemValidationResult() {
    this.valid = true;
    this.messages = [];
    this.resourceId = null;
}


module.exports = validateSingleResourceItem;
