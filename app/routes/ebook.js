var fs = require('fs');
var Q = require('q');

var downloader = require("../helpers/downloader.js");
var app = require('../index.js').app;
var staticDir = require("../index.js").staticDir;

var readFile = function(path) {
	var defer = Q.defer();
	fs.readFile(path, function(err, fdata) {
		if (err || !fdata) {
			defer.reject(err || "no data");
		} else {
			defer.resolve(fdata);
		}
	});
	return defer.promise;
};

app.get("/content/ePubPOC/", function(req, res) {
	res.redirect(req.originalUrl + "index.html");
});

app.get("/content/ePubPOC/index.html", function(req, res) {
	readFile(staticDir + "/ebookIndex.html").then(function(data) {
		res.contentType('text/html');
		res.send(data, 200);
	}, function(err) {
		res.send(err, 500);
	});
});

		
app.get("/content/hmof/math/hsm/common/ite/f01.1/**", function(req, res) {
	downloader.downloadFile("https://my-review-cert.hrw.com", req.originalUrl).then(function(data) {
		// force it to reload, this makes it so the content type header gets set correctly through static file handler
		res.redirect(req.originalUrl);
	}, function(err) {
		res.send(err, 404);
	});
});

app.get("/content/hmof/:platform/:course/na/:grade/:ebookId/:html5?/", function(req, res) {
	res.redirect(req.originalUrl + "index.html");
});

app.get("/content/hmof/:platform/:course/na/:grade/:ebookId/:html5?/index.html", function(req, res) {
	readFile(staticDir + "/ebookIndex.html").then(function(data) {
		res.contentType('text/html');
		res.send(data, 200);
	}, function(err) {
		res.send(err, 500);
	});
});

//		 /content/hmof/math/gomath/na/gr7/ese_9780544146815_/html5/OPS/config.txt
app.get("/content/hmof/:platform/:course/na/:grade/:ebookId/:html5?/OPS/**", function(req, res) {
	downloader.downloadFile("https://my-review-cert.hrw.com", req.originalUrl).then(function(data) {
		// force it to reload, this makes it so the content type header gets set correctly through static file handler
		res.redirect(req.originalUrl);
	}, function(err) {
		res.send(err, 404);
	});
});

///content/hsp/reading/journeys2014/na/grk/ese_9780547909134_/vol1/launch.html
app.get("/content/hsp/reading/journeys2014/na/:grade/:ebookId/*/launch.html", function(req, res) {
	readFile(staticDir + "/ebookIndex.html").then(function(data) {
		res.contentType('text/html');
		res.send(data, 200);
	}, function(err) {
		res.send(err, 500);
	});
});

///content/hsp/reading/journeys2014/na/grk/ese_9780547909134_/vol1/launch.html
app.get("/content/hsp/reading/journeys2014/na/:grade/:ebookId/**", function(req, res) {
	console.log("downloading file ");
	downloader.downloadFile("http://www-k6.thinkcentral.com", req.originalUrl).then(function(data) {
		// force it to reload, this makes it so the content type header gets set correctly through static file handler
		res.redirect(req.originalUrl);
	}, function(err) {
		res.send(err, 404);
	});
});