var fs = require('fs');
var Q = require('q');

var downloader = require("../helpers/downloader.js");
var app = require('../index.js').app;
var staticDir = require('../index.js').staticDir;

var readFile = function(path) {
	var defer = Q.defer();
	fs.readFile(path, function(err, fdata) {
		if (err || !fdata) {
			defer.reject(err || "no data");
		} else {
			defer.resolve(fdata);
		}
	});
	return defer.promise;
};

//Athena stuff, for POC

////list of url prefixes to proxy to the real server
var proxyUrls = [ '/api/identity/', '/api/data/', '/api/ugen/', '/api/caes/', '/api/reporting/', '/api/xapi', '/api/adaptive', '/hrwhoap' ];

var checkFileForProxy = function(url) {
	url = (url || "").toLowerCase();
	for (var i = 0; i < proxyUrls.length; i++) {
		if (url.indexOf(proxyUrls[i].toLowerCase()) === 0) {
			return true;
		}
	}
	return false;
};

//app.get("/content/hmof/math/hsm/common/ite/:grade/js/**", function (req, res) {
//	var scriptName = req.url.match("[^/]+$")[0];
//	res.contentType('application/javascript');
//	readFile(staticDir + "/modifiedAthenaScripts/" + scriptName).then(function(data) {
//		if (scriptName === "init.athena-min.js") {
//			readFile(staticDir + "/modifiedAthenaScripts/Collab.js").then(function(collab) {
//				res.send(collab + "\n" + data, 200);
//			});
//		} else {
//			res.send(data, 200);
//		}
//	}, function(err) {
//		res.send(err, 500);
//	});
//});

module.exports.middleware = function(req, res, next) {
	if (checkFileForProxy(req.url)) {
		console.log("proxying: " + req.originalUrl);
		var host = "my-review-cert.hrw.com"; // the host to proxy to
		var isSSL = false; // use ssl on the proxied endpoint? prod uses ssl...
		var path = req.originalUrl;
		for (var i = 0; i < hsmUrlReplacements.length; i++) {
			path = path.replace(hsmUrlReplacements[i], "").replace("redirect_uri=" + encodeURIComponent(hsmUrlReplacements[i]), "redirect_uri=http%3A%2F%2F" + host);
		}
		proxyToLiveServer(host, path, isSSL, req, res);
	} else if (req.url.match(/\/content\/hmof\/math\/hsm\/common\/ite\/[^\/]+\/js\//)) {
		var scriptName = req.url.match("[^/]+$")[0];
		res.contentType('application/javascript');
		readFile(__dirname + "/../../static/modifiedAthenaScripts/" + scriptName).then(function(data) {
			if (scriptName === "init.athena-min.js") {
				readFile(__dirname + "/modifiedAthenaScripts/Collab.js").then(function(collab) {
					res.send(collab + "\n" + data, 200);
				});
			} else {
				res.send(data, 200);
			}
		}, function(err) {
			res.send(err, 500);
		});
	} else {
		next();
	}
};

app.get("/content/hmof/math/hsm/common/ite/:grade/**", function(req, res) {
	downloader.downloadFile("https://my.hrw.com", req.originalUrl).then(function(data) {
		// force it to reload, this makes it so the content type header gets set correctly through static file handler
		res.redirect(req.originalUrl);
	}, function(err) {
		res.send(err, 404);
	});
});