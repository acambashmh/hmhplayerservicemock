var fs = require('fs');
var Q = require('q');

var proxy = require("../helpers/proxy.js");
var auth = require("../helpers/auth.js");
var app = require('../index.js').app;
var staticDir = require("../index.js").staticDir;
var mwsDir = staticDir + "/mwsPOCData/";

var fsExists = function(filePath) {
	try {
		var defer = Q.defer();
		fs.exists(filePath, function(exists) {
			defer.resolve(exists);
		});
		return defer.promise;
	} catch (e) {
		return Q.reject(e);
	}
};

Q.nfcall(fs.mkdir, staticDir + "/mwsPOCData");

app.get("/mwsPoc/getAssignmentInfo/:assignmentId", function(req, res) {
	var user = req.user;
	if (!user) {
		res.send(401, "Authorization header not set");
		return;
	}
	Q.fcall(function() {
		var filePath = mwsDir + user.sifRef + "/" + req.params.assignmentId + ".json";
		return fsExists(filePath).then(function(exists) {
			if (exists) {
				var r = fs.createReadStream(filePath);
				r.on("error", function(err) {
					console.log(err.stack);
				});
				res.status(200);
				r.pipe(res);
			} else {
				console.log("no data for " + filePath);
				res.send("NOT FOUND", 404); // no data
			}
		});
	}).fail(function(err) {
		console.log(err);
		console.log(err.stack);
		res.json(500, {
			err : err.stack
		});
	});
});

app.post("/mwsPoc/saveAssignmentInfo/:assignmentId", function(req, res) {
	var user = req.user;
	if (!user) {
		res.send(401, "Authorization header not set");
		return;
	}
	Q.fcall(function() {
		var filePath = mwsDir + user.sifRef + "/" + req.params.assignmentId + ".json";

		return fsExists(mwsDir + user.sifRef).then(function(exists) {
			var mkDirPromise;
			if (exists) {
				mkDirPromise = Q.resolve();
			} else {
				mkDirPromise = Q.nfcall(fs.mkdir, mwsDir + user.sifRef);
			}
			return mkDirPromise.then(function() {
				return Q.nfcall(fs.writeFile, filePath, JSON.stringify(req.body, null, 2)).then(function() {
					res.json(200, {
						err : null
					});
				});
			});
		});

	}).fail(function(err) {
		console.log(err);
		console.log(err.stack);
		res.json(500, {
			err : err.stack
		});
	});
});
