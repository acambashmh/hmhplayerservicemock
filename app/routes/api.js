var fs = require('fs');
var Q = require('q');

var proxy = require("../helpers/proxy.js");
var app = require('../index.js').app;
var staticDir = require("../index.js").staticDir;

app.all("/api/identity/v1/authorize", function(req, res) {
	// all calls will be proxied to cert by default, if you wish to handle any specific call, add a route here
	// if we need to conditionally proxy to the live server just call proxy.proxyToApiCert
	proxy.proxyToApiCert(req, res);
});

