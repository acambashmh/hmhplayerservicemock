var fs = require('fs');
var Q = require('q');
var proxy = require("../helpers/proxy.js");
var app = require('../index.js').app;
var rmiConfig = require('../RMI/rmiConfig.json');
var rmiClass = require('../RMI/rmiInit');
var rmi = new rmiClass(rmiConfig.svnData, rmiConfig.fileDefinition);
var convertMetadata = require('../RMI/Converters/metadataConverter');
var winston = require('winston');


app.post('/rmi/commit', function(req, res){
    winston.info('commit message received');
    if(req.body.$msg){
        winston.info('commit message: ' + req.body.$msg);
        var respondedToCommit = false;
        rmi.handleCommitMessage(req.body)
            .then(function(data) {
                var result = [];

                for(var i = 0; i < data.length; i++){
                    var responseItem = {
                        valid:data[i].valid,
                        fileId:data[i].fileInfo.id
                    };

                    result.push(responseItem);
                }

                winston.info('response to the svn server for commit message' + result.toString());
                res.json(result);
                respondedToCommit = true;
                return data;
            })
            .then(function(result){
                winston.info('Converting the received data');
                return convertMetadata(result);
            })
            .catch(function(err){
                winston.error(err);
                if(!respondedToCommit){
                    res.json({done:false, err:err});
                }
            });

    }else{
        winston.error('commit message is empty');
        res.json({message:'$msg param is missing'});
    }

});