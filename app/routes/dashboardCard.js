var fs = require('fs');
var Q = require('q');

var proxy = require("../helpers/proxy.js");
var auth = require("../helpers/auth.js");
var app = require('../index.js').app;
var staticDir = require("../index.js").staticDir;
var mdsDir = staticDir + "/mdsData/";

var fsExists = function(filePath) {
	try {
		var defer = Q.defer();
		fs.exists(filePath, function(exists) {
			defer.resolve(exists);
		});
		return defer.promise;
	} catch (e) {
		return Q.reject(e);
	}
};

app.all("/api/search/v1/toc/:program/:grade?", function(req, res) {
	var user = req.user;
	if (!user) {
		res.send(401, "Authorization header not set");
		return;
	}
	Q.fcall(function() {
		var fileName = user.isTeacher ? "cards_teacher.json" : "cards_student.json";
		var filePath = req.params.program + "/"
		
		
		if (req.params.grade) {
			filePath += req.params.grade + "/" + fileName;
		} else if (req.query.nonGradeTitle) {
			filePath += req.query.nonGradeTitle + "/" + fileName;
		} else {
			throw "Grade or Non Grade Title must be specified";
		}

		filePath = mdsDir + filePath.replace(/\s/ig, "_").replace(/[^A-Za-z0-9_.\/]/ig, " ");
		
		return fsExists(filePath).then(function(exists) {
			if (exists) {
				var r = fs.createReadStream(filePath);
				r.on("error", function(err) {
					console.log(err.stack);
				});
				res.status(200);
				r.pipe(res);
			} else {
				console.log("no data for " + filePath);
				res.send("NOT FOUND", 404); // no data
			}
		});
	}).fail(function(err) {
		console.log(err);
		console.log(err.stack);
		res.json(500, {
			err : err.stack
		});
	});
});
