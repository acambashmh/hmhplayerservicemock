var Q = require('q');
var fs = require('fs');
var http = require('http');
var https = require('https');
var _ = require('underscore');
var xml2js = require('xml2js');

var getUrl = function(url) {
	var defer = Q.defer();
	var req = http.get(url, function(response) {
		if (response.statusCode === 200) {
			var data = "";
			response.on('data', function(chunk) {
				data += chunk;
			});
			response.on("end", function() {
				defer.resolve(data);
			});
		} else {
			defer.reject(new Error("invalid status code:" + response.statusCode + " - " + url));
		}
	});
	req.on("error", function(err) {
		console.log("ERR:" + url);
		defer.reject(err.stack);
	});
	return defer.promise;
};

var findDloLandingPageItems = function(tocNode) {
	// tocNode has (optionally) an ol
	var results = [];
	try {
		if (tocNode.a && tocNode.a[0] && tocNode.a[0]["$"]["href"].match(/pageDL[0-9]+.xhtml/)) {
			results.push({
				url : tocNode.a[0]["$"]["href"],
				text : tocNode.a[0]["_"]
			});
		}
		if (tocNode.ol) {
			_.each(tocNode.ol[0].li, function(node) {
				results = results.concat(findDloLandingPageItems(node));
			});
		}
	} catch (err) {
		console.log(err.stack);
	}
	return results;
};

var xmlToJson = function(xmlStr) {
	return Q.nfcall(xml2js.parseString, xmlStr);
};

var filterTocToDloPages = function(tocJson) {
	var dloNodes = findDloLandingPageItems(tocJson.html.body[0].section[0].nav[0]);
	return dloNodes;
};

var getAnchorTagsFromXhtml = function(pageHtml) {
	var anchorTags = pageHtml.match(/<a\s+href="[^"]+"[^>]+.+<\/a>/ig);
	return Q.resolve(_.map(anchorTags, function(aTagHtml) {
		var matches = (/(?:href=")([^"]+)(?:"[^>]+>)([^<]+)(?:<\/a>)/ig).exec(aTagHtml);
		var href = matches[1];
		var text = matches[2];
		return {
			text : text,
			href : href
		};
	}));
};

var parseLessonOverviewLandingPages = function(baseUrl, dloPage) {
	//http://my.hrw.com/content/hmof/language_arts/hmhcollections/fl/gr9/dlo_landing_page_9780544398719_/Teacher_Landing_Page/target9.html
	var contentId = baseUrl.match(/_[0-9]+_/ig)[0];
	var rootDir = baseUrl.match(/.+gr[0-9]+/ig)[0].replace("/ga/", "/na/");
	rootDir += "/dlo_landing_page" + contentId + "/Student_Landing_Page/target";
	return getUrl(rootDir + dloPage + ".html").then(function(pageHtml) {
		return getAnchorTagsFromXhtml(pageHtml).then(function(anchorTags) {
			return {
				aTags : anchorTags,
				section : (/(?:<div class="writing">)([^<]+)(?:<\/div>)/ig.exec(pageHtml) || [])[1]
			}
		});
	});
};

var chainLandingPageRequest = function(baseUrl, page) {
	return parseLessonOverviewLandingPages(baseUrl, page).then(function(data) {
		return chainLandingPageRequest(baseUrl, page + 1).then(function(data2) {
			data2[data.section] = data;
			return data2;
		});
	}, function(err) {
		return {};
	});
};

var getDlosForEbook = function(ebookUrl) {
	var opsDir = ebookUrl.replace(/[^/]+$/, "OPS/");
	var tocUrl = opsDir + "toc.xhtml";
	chainLandingPageRequest(opsDir, 1).then(function(data) {
		console.log('!');
	});

	//	Q(tocUrl).then(getUrl).then(xmlToJson).then(filterTocToDloPages).then(function(nodes) {
	//		var promises = [];
	//		_.each(nodes, function(node, idx) {
	//			promises.push(parseLessonOverviewLandingPages(opsDir, idx + 1));
	//		});
	//		Q.allSettled(promises).then(function(results) {
	//			console.log('!');
	//		});
	//
	//		//		return _.map(nodes, function(node) {
	//		//			return Q(opsDir + node.url).then(getDlosFromLandingPage);
	//		//		});
	//
	//	}).fail(function(err) {
	//		console.log(err.stack);
	//	});
};

//var testMdsData = function () {
//var ebookUrl = "http://my.hrw.com/content/hmof/language_arts/hmhcollections/na/gr6/ete_9780544061002_/index.html"; //"http://my.hrw.com/content/hmof/language_arts/hmhcollections/ga/gr10/ese_9780544157620_/index.html";
//require('./content/mds.js').getDlosForEbook(ebookUrl);
//};
//
//testMdsData();


//http://my-test.hrw.com/api/search/v1/toc/HMH%20Collections/6?outputFormat=json&view=player&showResources=true&showGradeResources=true





exports.getDlosForEbook = getDlosForEbook;