var jwt = require('jwt-simple');
var Q = require('q');
var _ = require('underscore');
var http = require('http');
var https = require('https');
var querystring = require('querystring');

var getUser = function(req) {
	return parseUserToken(req.headers.authorization);
};

var parseUserToken = function(authorizationToken) {
	return Q.fcall(function() {

		authorizationToken = authorizationToken || "";
		var secret = process.env.SIF_SECRET;

		var decoded;
		if (!secret) {
			decoded = {
				iss : 'https://identity.api.hmhco.com',
				aud : 'http://www.hmhco.com',
				iat : 1403050762,
				sub : 'cn=Walter White,uid=USER:' + authorizationToken.substr(authorizationToken.length - 5) + ',uniqueIdentifier=123456,o=00014647,dc=02845985',
				'http://www.imsglobal.org/imspurl/lis/v1/vocab/person' : [ 'Administrator', 'Instructor' ],
				client_id : 'HMH Player',
				PlatformId : 'HMOF',
				exp : 1403054362
			};
		} else if (authorizationToken) {
			decoded = jwt.decode(new Buffer(authorizationToken.substring("SIF_HMACSHA256 ".length), 'base64').toString('ascii'), secret, "SIF_HMACSHA256");
		} else {
			throw "Not Authorized";
		}

		var subs = _.object(_.map(decoded.sub.split(','), function(kv) {
			return kv.split("=");
		}));

		var roles = decoded["http://www.imsglobal.org/imspurl/lis/v1/vocab/person"] || [];

		return {
			isTeacher : _.contains(roles, "Administrator") || _.contains(roles, "Instructor"),
			username : subs.uid,
			name : subs.cn,
			sifRef : subs.uniqueIdentifier
		};
	});
};

var _parseAuthPayload = function(data) {
	return Q.fcall(function() {
		return JSON.parse(data).access_token
	});
};

var _parseAuthnCookieHeader = function(res) {
	return (/(?:Authn=)([^;]+)(?:;)/ig.exec(res.headers["set-cookie"]) || [])[1];
};

var getTcJSession = function() {
	//	https://www-review-cert-tc1.thinkcentral.com/ePC/login.do;jsessionid=C6C42BE30E9CEC09499D239A6EC3B445
	var defer = Q.defer();
	var options = {
		host : "www-review-cert-tc1.thinkcentral.com",
		port : 80,
		path : "/ePC/login.do"
	};
	var req = http.request(options, function(resp) {
		try {
			defer.resolve((/(?:JSESSIONID=)([^;]+)(?:;)/ig.exec(resp.headers["set-cookie"]) || [])[1]);
		} catch (e) {
			defer.reject(e);
		}
		resp.on("error", defer.reject);
	});
	req.on("error", defer.reject);
	req.end();

	return defer.promise;
};

var doTCLogin = function(username, password) {
	return getTcJSession().then(function(jsessionId) {
		var defer = Q.defer();

		var ssl = false;

		var postBody;

		var postBody = querystring.stringify({
			organizationId : "",
			REQUEST_TYPE : "",
			openToken : "",
			country : "US",
			internationalUserLoginEnabled : true,
			state : "CA",
			district : 88200009,
			school : 88200010,
			userName : username,
			password : password,
			login2 : "Log In",
			districtNameEval : "",
			selectedSchool : "",
			loginpage : true
		});

		var options = {
			host : "www-review-cert-tc1.thinkcentral.com",
			port : ssl ? 443 : 80,
			path : "/ePC/login.do;jsessionid=" + jsessionId,
			method : "POST",
			headers : {
				"Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
				"Accept-Language" : "en-US,en;q=0.8,ca;q=0.6,nl;q=0.4",
				"Origin" : "http://www-review-cert-tc1.thinkcentral.com",
				"Content-Length" : postBody.length,
				"Content-Type" : "application/x-www-form-urlencoded",
				"Host" : "www-review-cert-tc1.thinkcentral.com"
			}
		};

		var req = (ssl ? https : http).request(options, function(res) {
			//my-review-cert.hrw.com/
			try {
				var jsession = (/(?:JSESSIONID=)([^;]+)(?:;)/ig.exec(res.headers["set-cookie"]) || [])[1];
				var epc = (/(?:ePC_SESSION=)([^;]+)(?:;)/ig.exec(res.headers["set-cookie"]) || [])[1];
				defer.resolve({
					jsession : jsession,
					epc : epc
				});
			} catch (e) {
				defer.reject(e);
			}
			var data = "";
			res.on("data", function(chunk) {
				data += chunk;
			});
			res.on("end", function() {});
			res.on("error", function(err) {
				defer.reject(err);
			});
		});

		req.on("error", function(err) {
			defer.reject(err);
		});

		req.write(postBody);

		req.end();

		return defer.promise.then(function(cookies) {
			return getTCSecurityAuthCookie({
				"JSESSIONID" : cookies.jsession,
				"ePC_SESSION" : cookies.epc
			}).then(function(authCookies) {
				return _.extend(cookies, authCookies);
			});
		});
	});
};

var authenticate = function(username, password, platform) {
	return getAuthorizationToken(username, password, platform === "tc").then(function(authToken) {
		return parseUserToken(authToken).then(function(user) {
			return getAuthNCookie(authToken).then(function(authnCookie) {
				if (platform === "tc") {
					return doTCLogin(username, password).then(function(authCookies) {
						return {
							user : user,
							cookies : authCookies,
							authToken : authToken
						};
					});
				} else {
					return getHmofSecurityAuthCookie(authnCookie).then(function(securityCookie) {
						return {
							user : user,
							cookies : {
								"Authn" : authnCookie,
								"HMH_AUTH=SecurityToken" : securityCookie
							},
							authToken : authToken
						};
					});
				}
			});
		});
	});
};

var getTCSecurityAuthCookie = function(cookies) {
	//	https://my-review-cert.hrw.com/dashboard/lti
	//https://www-review-cert-tc1.thinkcentral.com/ePC/psssecurity.do?method=teacherLandingDashboardFwd

	var cookieStr = "s_fid=48971470F56EC969-3A1C39E04657A930; s_getNewRepeat=1424387985460-New; s_sq=hmhtckstaging%3D%2526pid%253Dhttps%25253A%25252F%25252Fwww-review-cert-tc1.thinkcentral.com%25252Fepc%25252Fstart.do%2526pidt%253D1%2526oid%253DLog%252520In%2526oidt%253D3%2526ot%253DSUBMIT; ";
	if (cookies) {
		for ( var k in cookies) {
			if (cookies.hasOwnProperty(k)) {
				cookieStr += k + "=" + cookies[k] + "; ";
			}
		}
	}

	var defer = Q.defer();
	var options = {
		host : "www-review-cert-tc1.thinkcentral.com",
		port : 443,
		path : "/ePC/psssecurity.do?method=teacherLandingDashboardFwd",
		headers : {
			"Cookie" : cookieStr,
			host : "www-review-cert-tc1.thinkcentral.com"
		}
	};
	var req = https.request(options, function(res) {
		var authn = (/(?:Authn=)([^;]+)(?:;)/ig.exec(res.headers["set-cookie"]) || [])[1];

		var data = "";
		res.on("data", function(chunk) {
			data += chunk;
		});

		res.on("end", function() {
			defer.resolve({
				authn : authn,
				html : data
			});
		});

		res.on("error", function(err) {
			defer.reject(err);
		});
	});

	req.on("error", function(err) {
		defer.reject(err);
	});
	req.end();
	return defer.promise.then(function(data) {
		var loginPageHtml = data.html;
		var newPostPayload = {};

		if (!loginPageHtml) {
			return Q.reject(new Error("unable to login"));
		}
		var matches = loginPageHtml.match(/<input\stype="hidden"\sname="[^?]+"\svalue="[^"]+"\/>/mig)[0].split("\n");
		_.each(matches, function(match) {
			var kv = (/(?:name=")([^"]+)(?:"\svalue=")([^"]*)(?:")/ig).exec(match);
			if (kv && kv.length === 3) {
				var k = kv[1];
				var val = kv[2];
				newPostPayload[k] = val;
			} else {
				console.log("failed to match " + match);
			}
		});

		var defer2 = Q.defer();
		var ssl = true;

		var postBody = querystring.stringify(newPostPayload);

		var options = {
			host : "www-review-cert-tc1.thinkcentral.com",
			port : ssl ? 443 : 80,
			path : "/dashboard/lti",
			method : "POST",
			headers : {
				"Cookie" : cookieStr + "; Authn=" + data.authn,
				"Accept" : "*/*",
				"Accept-Language" : "en-US,en;q=0.8,ca;q=0.6,nl;q=0.4",
				"Content-Length" : postBody.length,
				"Content-Type" : "application/x-www-form-urlencoded; charset=UTF-8",
				"Host" : "www-review-cert-tc1.thinkcentral.com"
			}
		};

		var req = (ssl ? https : http).request(options, function(res) {

			var body = "";

			res.on("data", function(chunk) {
				body += chunk;
			});

			res.on("end", function() {
				console.log(body);
				try {
					defer2.resolve({
						Authn : data.authn,
						"HMH_AUTH=SecurityToken" : (/(?:HMH_AUTH=SecurityToken=)([^;&]+)/).exec((res.headers["set-cookie"] || [])[0])[1]
					});
				} catch (e) {
					defer2.reject("Auth Cookie Not Found");
				}
			});

			res.on("error", function(err) {
				defer2.reject(err);
			});
		});

		req.on("error", function(err) {
			defer2.reject(err);
		});
		req.write(postBody);
		req.end();
		return defer2.promise;
	});
};

var getHmofSecurityAuthCookie = function(authnCookie, otherCookies) {
	//	https://my-review-cert.hrw.com/dashboard/lti

	var defer = Q.defer();
	var ssl = true;

	var cookieStr = "Authn=" + authnCookie + "; ";
	if (otherCookies) {
		for ( var k in otherCookies) {
			if (otherCookies.hasOwnProperty(k)) {
				cookieStr += k + "=" + otherCookies[k] + "; ";
			}
		}
	}

	var options = {
		host : "my-review-cert.hrw.com",
		port : ssl ? 443 : 80,
		path : "/HRWHoap/ltiLaunch/workbench/login",
		method : "GET",
		headers : {
			"Cookie" : cookieStr,
			"Accept" : "*/*",
			"Accept-Language" : "en-US,en;q=0.8,ca;q=0.6,nl;q=0.4",
			"Content-Type" : "application/x-www-form-urlencoded",
			"Host" : "my-review-cert.hrw.com"
		}
	};

	var req = (ssl ? https : http).request(options, function(res) {
		//my-review-cert.hrw.com/

		var data = "";
		res.on("data", function(chunk) {
			data += chunk;
		});

		res.on("end", function() {
			defer.resolve(data);
		});

		res.on("error", function(err) {
			defer.reject(err);
		});
	});

	req.on("error", function(err) {
		defer.reject(err);
	});
	req.end();
	return defer.promise.then(function(loginPageHtml) {
		var newPostPayload = {};

		if (!loginPageHtml) {
			return Q.reject(new Error("unable to login"));
		}
		var matches = loginPageHtml.match(/<input\stype="hidden"\sname="[^?]+"\svalue="[^"]+"\/>/mig)[0].split("\n");
		_.each(matches, function(match) {
			var kv = (/(?:name=")([^"]+)(?:"\svalue=")([^"]*)(?:")/ig).exec(match);
			if (kv && kv.length === 3) {
				var k = kv[1];
				var val = kv[2];
				newPostPayload[k] = val;
			} else {
				console.log("failed to match " + match);
			}
		});

		var defer2 = Q.defer();
		var ssl = true;

		var postBody = querystring.stringify(newPostPayload);

		var options = {
			host : "my-review-cert.hrw.com",
			port : ssl ? 443 : 80,
			path : "/dashboard/lti",
			method : "POST",
			headers : {
				"Cookie" : "Authn=" + authnCookie,
				"Accept" : "*/*",
				"Accept-Language" : "en-US,en;q=0.8,ca;q=0.6,nl;q=0.4",
				"Content-Length" : postBody.length,
				"Content-Type" : "application/x-www-form-urlencoded; charset=UTF-8",
				"Host" : "my-review-cert.hrw.com"
			}
		};

		var req = (ssl ? https : http).request(options, function(res) {
			try {
				defer2.resolve((/(?:HMH_AUTH=SecurityToken=)([^;&]+)/).exec((res.headers["set-cookie"] || [])[0])[1]);
			} catch (e) {
				defer2.reject("Auth Cookie Not Found");
			}

			res.on("error", function(err) {
				defer2.reject(err);
			});
		});

		req.on("error", function(err) {
			defer2.reject(err);
		});
		req.write(postBody);
		req.end();
		return defer2.promise;
	});
};

var getAuthNCookie = function(authToken) {
	return Q
			.fcall(function() {
				var defer = Q.defer();
				var ssl = true;

				var options = {
					host : "my-review-cert.hrw.com",
					port : ssl ? 443 : 80,
					path : "/api/identity/v1/authorize;contextId=hmof?response_type=code&scope=openid+profile&client_id=HMHPlayer+Id&state=HMHPlayerRedirect%7B%7Bhttp%3A%2F%2Fmy-review-cert.hrw.com%2Fcontent%2Fhmof%2Fmath%2Fhsm%2Fcommon%2Fite%2Fg01.1%2Findex.html%3FU%3D1efbbef0f49ec5327ffd389831cbe95e%26C%3Dgeo%26S%3DCA%26U%3D1efbbef0f49ec5327ffd389831cbe95e%26debug%3Dtrue%26console%3Dall%7D%7D&redirect_uri=http%3A%2F%2Fmy-review-cert.hrw.com%2FHMHPlayer_AuthCallback",
					method : "GET",
					headers : {
						"Authorization" : authToken,
						"Accept" : "*/*",
						"Accept-Language" : "en-US,en;q=0.8,ca;q=0.6,nl;q=0.4",
						"Content-Type" : "application/x-www-form-urlencoded",
						"Host" : "my-review-cert.hrw.com"
					}
				};

				var req = (ssl ? https : http).request(options, function(res) {
					//my-review-cert.hrw.com/

					var data = "";
					res.on("data", function(chunk) {
						data += chunk;
					});

					res.on("end", function() {
						Q.fcall(_parseAuthnCookieHeader, res).then(defer.resolve, defer.reject);
					});

					res.on("error", function(err) {
						defer.reject(err);
					});
				});

				req.on("error", function(err) {
					defer.reject(err);
				});
				req.end();
				return defer.promise;
			});
};

/**
 * Returns a promise with the authn cookie for the username / password 
 **/
var getAuthorizationToken = function(username, password, isTC) {
	var defer = Q.defer();
	try {

		var ssl = true;

		var postBody;

		if (isTC) {
			// for TC we build up a special username parameter
			postBody = "username=uid=" + encodeURIComponent(username) + ",o=88200010,dc=88200009,st=CA&password=" + encodeURIComponent(password) + "&grant_type=password";
		} else {
			postBody = querystring.stringify({
				username : username,
				password : password,
				grant_type : "password"
			});
		}

		var options = {
			host : "my-review-cert.hrw.com",
			port : ssl ? 443 : 80,
			path : "/api/identity/v1/token;contextId=" + (isTC ? 'tc' : 'homof'),
			method : "POST",
			headers : {
				"Accept" : "*/*",
				"Accept-Language" : "en-US,en;q=0.8,ca;q=0.6,nl;q=0.4",
				"Content-Length" : postBody.length,
				"Content-Type" : "application/x-www-form-urlencoded; charset=UTF-8",
				"Host" : "my-review-cert.hrw.com"
			}
		};

		var req = (ssl ? https : http).request(options, function(res) {
			//my-review-cert.hrw.com/

			var data = "";
			res.on("data", function(chunk) {
				data += chunk;
			});

			res.on("end", function() {
				if (res.statusCode === 200) {
					_parseAuthPayload(data).then(defer.resolve, defer.reject);
				} else {
					defer.reject(new Error("invalid statucs code " + res.statusCode + " " + data));
				}
			});

			res.on("error", function(err) {
				defer.reject(err);
			});
		});

		req.on("error", function(err) {
			defer.reject(err);
		});

		req.write(postBody);

		req.end();

	} catch (e) {
		console.log(e.stack);
		defer.reject(e);
	}
	return defer.promise;
};

/** 
 * Gets the user data object from a request
 * @param req - the express request object (that has the Authorization header)
 * @returns a promise that returns { isTeacher (bool), username (string), name (string), sifRef (string)}
 **/
module.exports.getUser = getUser;

/**
 * Authenticates a user to the desired platform and returns the user data as well as authentication cookies needed for that user to make requests
 * @param username - the username
 * @param password - the password
 * @param platform - either tc or hmof
 * @returns a promise that returns { user (same object returned by getUser), cookies (map of cookies), authToken: (sif auth header)}
 */
module.exports.authenticate = authenticate;
