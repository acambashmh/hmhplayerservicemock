var http = require('http');
var https = require('https');
var fs = require('fs');
var Q = require('q');
var querystring = require('querystring');

var hsmUrlReplacements = [ "http://127.0.0.1:8080", "http://localhost:8123", "http://localhost:8080", "http://10.0.1.97:8080", "http://wse1.cert.collab.api.hmhco.com" ];

var proxyToLiveServer = function(host, path, ssl, req, res) {

	var options = {
		host : host,
		port : ssl ? 443 : 80,
		path : path,
		method : req.method,
		headers : {}
	};
	for ( var header in req.headers) {
		if (req.headers.hasOwnProperty(header) && header && header.toLowerCase() !== 'host') {
			if (header.toLowerCase() === "referer") {

				var val = req.headers[header];
				for (var i = 0; i < hsmUrlReplacements.length; i++) {
					val = val.replace(hsmUrlReplacements[i], "http://" + host);
				}

				options.headers["Referer"] = val; //req.headers[header].replace("http://localhost:8080", "http://" + host);
			} else {
				options.headers[header] = req.headers[header];
			}
		}
	}
	options.headers["Host"] = host;

	//console.log("PROXYING: " + JSON.stringify(options, null, 2));

	var proxyReq = (ssl ? https : http).request(options, function(proxyRes) {
//		console.log("HEADERS: " + JSON.stringify(proxyRes.headers));
		res.status(proxyRes.statusCode);
		for ( var h in proxyRes.headers) {
			if (h === "set-cookie") {
				var cookies = proxyRes.headers[h];
				if (!Array.isArray(proxyRes.headers[h])) {
					cookies = [ proxyRes.headers[h] ];
				}
				for (var c = 0; c < cookies.length; c++) {

					var cookie = cookies[c];

					for (var i = 0; i < hsmUrlReplacements.length; i++) {
						cookie = cookie.replace(/Domain=[^;]+;/ig, hsmUrlReplacements[i].replace("http://", ""));
					}

					cookies[c] = cookie;// cookies[c].replace(/Domain=[^;]+;/ig, 'localhost:8080');
				}
				res.setHeader('set-cookie', cookies);
			} else {
				res.setHeader(h, proxyRes.headers[h]);
			}
		}
		proxyRes.pipe(res);
	});

	proxyReq.on('error', function(e) {
		console.log('problem with request: ' + e.message);
		console.log(e.stack);
		res.send("", 500);
	});

	// write data to request body
	if (req.rawBody) {
		proxyReq.write(req.rawBody);
	}

	if (req.body) {
		var cType = (req.headers["content-type"] || "");
		if (cType.toLowerCase().indexOf("application/x-www-form-urlencoded") > -1) {
			// urel form encoded data
			proxyReq.write(querystring.stringify(req.body));
		} else {
			proxyReq.write(JSON.stringify(req.body));
		}
	}
	// post_req.write(req.rawBody);

	proxyReq.end();
};

var readFile = function(path) {
	var defer = Q.defer();
	fs.readFile(path, function(err, fdata) {
		if (err || !fdata) {
			defer.reject(err || "no data");
		} else {
			defer.resolve(fdata);
		}
	});
	return defer.promise;
};

module.exports.proxyToApiCert = function(req, res) {
	var host = "my-review-cert.hrw.com"; // the host to proxy to
	var isSSL = true; // use ssl on the proxied endpoint? prod uses ssl...
	var path = req.originalUrl;
	for (var i = 0; i < hsmUrlReplacements.length; i++) {
		path = path.replace(hsmUrlReplacements[i], "").replace("redirect_uri=" + encodeURIComponent(hsmUrlReplacements[i]), "redirect_uri=http" + (isSSL ? "s" : "") + "%3A%2F%2F" + host);
	}
	console.log("proxying " + path);
	proxyToLiveServer(host, path, isSSL, req, res);
};

module.exports.proxyTo = proxyToLiveServer;
