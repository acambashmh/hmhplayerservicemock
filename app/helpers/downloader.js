var Q = require('q');
var fs = require('fs');
var http = require('http');
var https = require('https');
var mkdirp = require('mkdirp');

var basePath = __dirname + "/../../static";

var readFile = function (path) {
	var defer = Q.defer();
	fs.readFile(path, function (err, fdata) {
		if (err || !fdata) {
			defer.reject(err || "no data");
		}
		else {
			defer.resolve(fdata);
		}
	});
	return defer.promise;
};

var downloadFile = function (url) {
	var defer = Q.defer();

	var ssl = url.toLowerCase().indexOf('https://') === 0;
	
	var domain = url.match(new RegExp("https?://[^/]+"))[0];
	var relativePath = url.substring(domain.length);
	var relPathMatch = relativePath.match(new RegExp("(.+?)(/[^/]+)$"));
	if (!relPathMatch) {
		return Q.reject();
	}
	mkdirp(basePath + relPathMatch[1], function (err) {
		if (err) {
			defer.reject(err);
			console.log(err);
			return;
		}
		// console.log("downloading " + url);
		var request = (ssl ? https : http).get(url, function (response) {
			if (response.statusCode === 200) {
				var file = fs.createWriteStream(basePath + relativePath);
				response.pipe(file);

				file.on('finish', function () {
					file.close(function () {
						defer.resolve();
					});
				});
			}
			else {
				defer.reject("invalid status code");
			}
		});
		request.on('error', function (e) {
			console.log('problem with request: ' + e.message + " " + url);
			defer.reject("timed out");
		});
		request.setTimeout(15000, function () {
			request.abort();
			console.log("connection timed out: " + url);
			defer.reject("timed out");
		});
	});

	return defer.promise;
};

exports.downloadFile = function (server, relativePath) {
	
	if (relativePath.indexOf("?") > -1) {
		relativePath = relativePath.substring(0, relativePath.indexOf("?"));
	}
	
	return downloadFile(server + relativePath).then(function () {
		return readFile(basePath + relativePath);
	});
};

exports.ensureIndexAndToc = function (grade, ebookId) {

	var relPath = "/content/hmof/language_arts/hmhcollections/na/" + grade + "/" + ebookId + "/OPS/toc.xhtml";
	var tocPath = basePath + relPath;

	var ensureTocDefer = Q.defer();

	fs.exists(tocPath, function (exists) {
		if (exists) {
			ensureTocDefer.resolve();
		}
		else {
			// need to download it
			downloadFile("http://my.hrw.com" + relPath).then(function () {
				ensureTocDefer.resolve();
			}, function (err) {
				ensureTocDefer.reject(err);
			});
		}
	});

	return ensureTocDefer.promise.then(function () {
		return readFile(__dirname + '/ebookIndex.html');
	});
};
